// Karma configuration
// Generated on Tue Sep 03 2013 19:03:47 GMT+0300 (EEST)

module.exports = function(config) {
    config.set({
        basePath: '',
        frameworks: ['jasmine'],
        files: ['node_modules/es5-shim/es5-shim.js',
                '_out/sockjs*.js',
                '_out/react-with-addons.js',
                '_out/main.js',
                '_out/tests.js'
                ],
        exclude: [],


        proxies:  {
        '/': 'http://localhost:9999'
        },
        urlRoot: '/__karma',

        // test results reporter to use
        // possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
        reporters: ['dots'],

        // web server port
        port: 9876,

        // enable / disable colors in the output (reporters and logs)
        colors: true,

        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR ||
        // config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,

        // enable / disable watching file and executing tests whenever any file
        // changes
        autoWatch: true,

        // Start these browsers, currently available:
        // - Chrome, ChromeCanary, Firefox, Opera, Safari (only Mac), PhantomJS,
        // IE (only Windows)
        browsers: ['PhantomJS'],

        // If browser does not capture in given timeout [ms], kill it
        captureTimeout: 600000,

        browserDisconnectTimeout: 20000,

        browserNoActivityTimeout: 30000,

        // Continuous Integration mode
        // if true, it capture browsers, run tests and exit
        singleRun: true
    });
};
