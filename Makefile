BROWSERIFY = ./node_modules/.bin/browserify -t coffeeify -t [ reactify -x coffee ]
WATCHIFY = ./node_modules/.bin/watchify -t coffeeify -t [ reactify -x coffee ]
KARMA ?= ./node_modules/karma/bin/karma
LESSC ?= ./node_modules/.bin/lessc
UGLIFY ?= ./node_modules/.bin/uglifyjs
CSSMIN ?= ./node_modules/.bin/cleancss
AUTOPF ?= ./node_modules/.bin/autoprefixer -b "last 2 versions"


SRC = $(shell find src -type f)
LESS_SRC = $(shell find static -type f -name '*.less')
STATIC_SRC = $(shell find static -type f \
                     -not -name '*.less' \
                     -not -name 'react-with-addons.min.js')
STATIC = $(STATIC_SRC:static/%=_out/%) _out/application.css
LOCKED_LESS_RUN = test ! -f .less-lock && touch .less-lock && make _out/application.css && rm -f .less-lock

.PHONY: watch clean

watch: node_modules
	@gulp || echo 'ERROR running gulp'

node_modules: package.json
	npm install
	@touch $@

optimize:
	optipng -o7 $(shell find static -name '*.png')
