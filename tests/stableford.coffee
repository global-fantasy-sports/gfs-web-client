_ = require 'underscore'
{findByDisplayName} = require '../src/lib/utils.coffee'
{goTo, elsByClass, hashUrl, $, click} = require './utils.coffee'

describe 'Stableford', ->
    describe 'Incomplete', ->

    describe 'Complete', ->
        it 'navigates to stableford', ->
            goTo '/stableford'
            waits 100

        describe 'Root', ->
            it 'should have root', ->
                expect(window.root_).toBeDefined()

            it 'still should not have login', ->
                expect(findByDisplayName('Login')).toBeNull()

        describe 'Wizard', ->
            game = null
            component = null

            it 'should be in the wizard', ->
                expect(hashUrl()).toEqual("/stableford/select")

            it 'should have StablefordCandidates', ->
                component = findByDisplayName('StablefordCandidates')
                expect(component).toBeMounted()
                game = component.props.model

            it 'should have created new Stableford game', ->
                expect(game).not.toBeNull()
                expect(game.isNew()).toBe(true)

            waits 10000
            it 'should render participants', ->
                expect(elsByClass('game-table').length).toBeGreaterThan(0)

            waits 1000
            it 'should give possibility to choose candidate', ->
                candidates = $('.game-table .btn_select')
                expect(candidates.length).toBeGreaterThan(0)
                click(candidates[0])
                actionbar = $('.actionbar button')
                waits 100
                expect(actionbar.length).toBeGreaterThan(0)
                click(actionbar[0])

            waits 1000
            it 'should be able to go through handicap application', ->
                component = findByDisplayName('Handicap')
                expect(component).toBeMounted()
                submitButton = $('.actionbar button')
                expect(submitButton.length).toBeGreaterThan(0)
                click(submitButton[0])

            waits 1000
            it 'should give possibility to choose stake', ->
                component = findByDisplayName('Stableford/Stake')
                expect(component).toBeMounted()
                stakes = $('.btn_stake-token')
                expect(stakes.length).toBeGreaterThan(0)
                click(stakes[0])
                submitButton = $('.actionbar button')[0]
                expect(submitButton).toBeDefined()
                click(submitButton)

            waits 1000
            it 'should save game after choosing stake', ->
                expect(game.isNew()).toBe(false)

            waits 1000
            it 'should render challenge step', ->
                component = findByDisplayName('Challenge')
                expect(component).toBeMounted()
                closeButton = $('.ui-close')
                expect(closeButton.length).toBeGreaterThan(0)
                click(closeButton[0])

            waits 1000
            it 'should be able to pay for games', ->
                component = findByDisplayName('Basket')
                expect(component).toBeMounted()
                submitButton = $('.actionbar button')
                expect(submitButton.length).toBeGreaterThan(0)
                click(submitButton[0])

            waits 1000
            it 'should save game payment status', ->
                expect(game.paid()).toBe(true)
