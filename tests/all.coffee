xhr = require '../src/lib/xhr.coffee'
config = require '../src/config.js'
{getSession, goTo} = require './utils.coffee'
{findByDisplayName} = require '../src/lib/utils.coffee'

window.onerror = (message, url, linenumber )->
    console.error "---------ERR", message, url, linenumber


describe 'App', ->

    it 'can be instantiated', ->
        expect(window.root_).toBeUndefined()
        window.startApp()
        expect(window.root_).toBeDefined()

    it 'waits for you to login', ->

        expect(Object.keys(root_.refs)).toEqual(['login'])


    # Should we use Test Facebook users in the future?
    describe 'Logging in', ->
        xhr
            url: config.server + 'auth/login'
            method: 'post'
            data: JSON.stringify({email: 'test@test.com', password: 'test'})
            responseType: 'json'
            callback: (err, data) ->
                @err = err
                @data = data

        waits 100

        # it 'does not return error response', ->
        #     expect(err).toBeNull()

        # it 'returns data object', ->
        #     expect(data).toBeDefined()

        # it 'has "success" attribute in data object', ->
        #     expect(data.success).toBeDefined()

        it 'has session cookie', ->
            session = getSession()
            expect(session).toBeDefined()

        it 'successfully sends session to the server over socket', ->
            window.root_.connect()
            waits 2000
            expect(Object.keys(window.root_.refs)).toEqual([])
            expect(findByDisplayName('Login')).toBeNull()

    require "./fancy18.coffee"

    require "./stableford.coffee"

#    require "./liteford.coffee"

