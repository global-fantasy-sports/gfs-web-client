{COOKIE_RE}  = require '../src/lib/const.coffee'

exports.getSession = ->
    document.cookie.match(COOKIE_RE)?[1]

exports.click = (el) ->
    # Not really a click hahaha
    evs = ['mousedown', 'mouseup']
    for name in evs
       ev = document.createEvent("MouseEvent");
       ev.initMouseEvent(
           name,
           true, # bubble
           true, # cancelable
           window, null,
           0, 0, 0, 0, # coordinates
           false, false, false, false, # modifier keys
           0,# left
           null
           )
       el.dispatchEvent(ev)


exports.goTo = (url) -> window.location.hash = url

exports.hashUrl = -> window.location.hash[1..-1]

exports.elsByClass = (classname) ->
    document.getElementsByClassName(classname)

exports.$ = (q, base) ->
    els = (base or document).querySelectorAll(q)

beforeEach ->
    @addMatchers
        toBeMounted: ->
            @actual?._lifeCycleState == "MOUNTED"
