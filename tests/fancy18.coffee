_ = require 'underscore'
{findByDisplayName} = require '../src/lib/utils.coffee'
{goTo, elsByClass, hashUrl, $, click} = require './utils.coffee'

describe 'Fancy18', ->

    describe 'Incomplete', ->

    describe 'Complete', ->
        it 'navigates to fancy18', ->
            goTo "/fancy18"
            waits 100

        describe 'Root', ->
            it 'should have root', ->
                expect(window.root_).toBeDefined()

            it 'still should not have login', ->
                expect(findByDisplayName('Login')).toBeNull()

        describe 'Wizard', ->
            card = null

            it 'should be in the wizard', ->
                expect(hashUrl()).toEqual("/fancy18/select")

            it 'should have FancyCandidates', ->
                component = findByDisplayName('FancyCandidates')
                expect(component).toBeMounted()
                card = component.props.model

            it 'should have created new Fancy18 card', ->
                expect(card).not.toBeNull()
                expect(card.isNew()).toBe(true)

            waits 1000
            it 'should render participants', ->
                expect(elsByClass('game-table').length).toBeGreaterThan(0)

            waits 1000
            it 'should give possibility to choose candidate', ->
                candidates = $('.game-table .btn_select')
                expect(candidates.length).toBeGreaterThan(0)
                click(candidates[0])
                actionbar = $('.actionbar button')
                waits 100
                expect(actionbar.length).toBeGreaterThan(0)
                click(actionbar[0])

            waits 1000
            it 'should be able to make prediction', ->
                component = findByDisplayName('FancyPrediction')
                expect(component).toBeMounted()

                buttons = $('.actionbar button')
                expect(buttons.length).toBeGreaterThan(1)
                fillInButton = buttons[0]
                click(fillInButton)
                menuButtons = $('.prediction__head button')
                expect(menuButtons.length).toBeGreaterThan(0)
                click(menuButtons[0]) # random
                submitButton = buttons[buttons.length - 1]
                click(submitButton)

            waits 2000
            it 'should give possibility to choose stake', ->
                component = findByDisplayName('Fancy/Stake')
                expect(component).toBeMounted()
                stake = $('.btn_stake-token')
                expect(stake).toBeDefined()
                click(stake[0])
                submitButton = $('.actionbar button')
                expect(submitButton).toBeDefined()
                click(submitButton[0])

            waits 2000
            it 'should save card after choosing stake', ->
                expect(card.isNew()).toBe(false)

            waits 1000
            it 'should render challenge step', ->
                component = findByDisplayName('Challenge')
                expect(component).toBeMounted()
                closeButton = $('.ui-close')
                expect(closeButton.length).toBeGreaterThan(0)
                click(closeButton[0])

            waits 1000
            it 'should be able to pay for cards', ->
                component = findByDisplayName('Basket')
                expect(component).toBeMounted()
                submitButton = $('.actionbar button')
                expect(submitButton.length).toBeGreaterThan(0)
                click(submitButton[0])

            waits 1000
            it 'should save card payment status', ->
                expect(card.paid()).toBe(true)
