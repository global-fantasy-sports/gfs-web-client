_ = require 'underscore'
{findByDisplayName} = require '../src/lib/utils.coffee'
{goTo, elsByClass, hashUrl, $, click} = require './utils.coffee'

describe 'Liteford', ->
    describe 'Incomplete', ->

    describe 'Complete', ->
        it 'navigates to liteford', ->
            goTo '/liteford'
            waits 100

        describe 'Root', ->
            it 'should have root', ->
                expect(window.root_).toBeDefined()

            it 'still should not have login', ->
                expect(findByDisplayName('Login')).toBeNull()

        describe 'Wizard', ->
            game = null
            component = null

            it 'should be in the wizard', ->
                expect(hashUrl()).toEqual("/liteford/select")

            it 'should have LitefordCandidates', ->
                component = findByDisplayName('Top50Candidates')
                expect(component).toBeMounted()
                game = component.props.model

            it 'should have created new Liteford game', ->
                expect(game).not.toBeNull()
                expect(game.isNew()).toBe(true)

            waits 10000
            it 'should render participants', ->
                expect(elsByClass('game-table').length).toBeGreaterThan(0)

            waits 1000
            it 'should give possibility to choose candidate', ->
                lists = $('.candidate-list')
                expect(lists.length).toBe(5)
                for list in lists
                    candidates = $('.game-table .btn_select', list)
                    expect(candidates.length).toBeGreaterThan(0)
                    click(candidates[0])
                expect(component.state.participants.length).toBe(5)
                button = $('.actionbar button')
                click(button[0])

            waits 1000
            it 'should give possibility to choose stake', ->
                component = findByDisplayName('Stableford/Stake')
                expect(component).toBeMounted()
                stakes = $('.btn_stake-token')
                expect(stakes.length).toBeGreaterThan(0)
                click(stakes[0])
                submitButton = $('.actionbar button')[0]
                expect(submitButton).toBeDefined()
                click(submitButton)

            waits 1000
            it 'should save game after choosing stake', ->
                expect(game.isNew()).toBe(false)

            waits 1000
            it 'should render challenge step', ->
                component = findByDisplayName('Challenge')
                expect(component).toBeMounted()
                closeButton = $('.ui-close')
                expect(closeButton.length).toBeGreaterThan(0)
                click(closeButton[0])

            waits 1000
            it 'should be able to pay for games', ->
                component = findByDisplayName('Basket')
                expect(component).toBeMounted()
                submitButton = $('.actionbar button')
                expect(submitButton.length).toBeGreaterThan(0)
                click(submitButton[0])

            waits 1000
            it 'should save game payment status', ->
                expect(game.paid()).toBe(true)
