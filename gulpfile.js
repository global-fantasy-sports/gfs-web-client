/**
 * Created by serdiuk on 06.11.2014.
 */
const _ = require('underscore');
const autoprefixer = require('gulp-autoprefixer');
const del = require('del');
const gulp = require('gulp');
const gutil = require('gulp-util');
const less = require('gulp-less');
const minifyCSS = require('gulp-minify-css');
const rename = require('gulp-rename');
const sequence = require('gulp-sequence');
const teamcity = require('gulp-teamcity-reporter');
const watch = require('gulp-watch');
const webpack = require('webpack');


teamcity.wireTaskEvents();


const PROD = (
    process.env.NODE_ENV === 'production' ||
    process.env.TEAMCITY_SERVER
);

const DEST = './_out';

const webpackDevConfig = require('./webpack.dev.config.js');
const webpackProdConfig = require('./webpack.config.js');

const webpackGOLFConfig = require('./webpack.golf.config.js');
const webpackNBAConfig = require('./webpack.nba.config.js');
const webpackNFLConfig = require('./webpack.nfl.config.js');

const AUTOPREFIXER_CONFIG = {
    browsers: ['last 2 versions'],
    cascade: false,
};

gutil.log('Running in ' + (PROD ? 'PRODUCTION' : 'DEBUG') + ' mode');

gulp.task('clean', function () { return del([DEST]); });
gulp.task('clean:dev', function () { return del(['./.happypack/**/*']); });

function compileLESS(source, target) {
    return function lessTask() {
        const path = './less/' + source;
        const pipeline = gulp.src(path)
            .pipe(less())
            .pipe(autoprefixer(AUTOPREFIXER_CONFIG));

        if (PROD) {
            pipeline.pipe(minifyCSS());
        }

        return pipeline
            .pipe(rename(target))
            .pipe(gulp.dest(DEST));
    };
}

gulp.task('less:about', compileLESS('about/about.less', 'about-web.css'));
gulp.task('less:landing', compileLESS('landing/main.less', 'landing-web.css'));
gulp.task('less:selector', compileLESS('selector/main.less', 'selector-web.css'));
gulp.task('less:common', compileLESS('common/styles.less', 'common-web.css'));

gulp.task('less', [
    'less:about',
    'less:landing',
    'less:selector',
    'less:common',
]);

gulp.task('watch:less', ['less'], function () {
    watch('./less/**/*.less', function () {
        gulp.start('less');
    });
});

gulp.task('static', ['clean'], function () {
    return gulp.src(['./static/**/*'])
        .pipe(gulp.dest(DEST));
});

function compileCoffee(config, action) {
    return function gulpTask(callback) {
        var compiler = webpack(config);
        var gulpCallback = _.once(callback);

        function compilerCallback(err, stats) {
            if (err) {
                throw new gutil.PluginError('webpack', err);
            }

            gutil.log('[webpack]\n', stats.toString(true));
            gulpCallback();
        }

        if (action === 'watch') {
            compiler.watch(null, compilerCallback);
        }
        else {
            compiler.run(compilerCallback)
        }
    };
}

gulp.task('coffee', ['clean', 'clean:dev'], compileCoffee(webpackProdConfig));

gulp.task('watch:coffee', ['clean:dev'], compileCoffee(webpackDevConfig, 'watch'));
gulp.task('watch:coffee:golf', ['clean:dev'], compileCoffee(webpackGOLFConfig, 'watch'));
gulp.task('watch:coffee:nfl', ['clean:dev'], compileCoffee(webpackNFLConfig, 'watch'));
gulp.task('watch:coffee:nba', ['clean:dev'], compileCoffee(webpackNBAConfig, 'watch'));

gulp.task('watch', ['clean'], sequence([
    'watch:coffee',
    'watch:less',
    'static',
]));

gulp.task('watch:golf', ['clean'], sequence([
    'watch:coffee:golf',
    'watch:less',
    'static',
]));

gulp.task('watch:nba', ['clean'], sequence([
    'watch:coffee:nba',
    'watch:less',
    'static',
]));

gulp.task('watch:nfl', ['clean'], sequence([
    'watch:coffee:nfl',
    'watch:less',
    'static',
]));

gulp.task('build', ['clean'], sequence([
    'coffee',
    'less',
    'static',
]));

gulp.task('default', ['watch']);
