const webpack = require('webpack')
const WebpackNotifier = require('webpack-notifier')
const HappyPack = require('happypack')


const configure = require('./webpack.common.js')

const vendorModules = [
    './vendor/sockjs-0.3.4.js',
    'blueimp-md5',
    'core-js',
    'eventemitter3',
    'fixed-data-table',
    'immutable',
    'minimal-flux',
    'moment',
    'moment-timezone/builds/moment-timezone-with-data-2010-2020.js',
    'rc-slider',
    'react-addons-css-transition-group',
    'react-addons-linked-state-mixin',
    'react-addons-pure-render-mixin',
    'react-addons-shallow-compare',
    'react-addons-transition-group',
    'react-dom',
    'react-motion',
    'react-overlays',
    'react-router',
    'react-tap-event-plugin',
    'react-virtualized',
    'recharts',
    'strftime',
    'underscore',
]


const conf = configure({
    debug: true,

    entry: {
        selector: './main.selector.coffee',
        nfl: './main.nfl.coffee',
        nba: './main.nba.coffee',
        golf: './main.golf.coffee',
        vendor: vendorModules,
    },

    filename: '[name]-dev.js',

    devtool: process.env.WEBPACK_SOURCE_MAP,

    plugins: [
        new HappyPack({id: 'coffee', cache: false}),
        new webpack.EnvironmentPlugin([
            'NODE_ENV',
            'BLOCK_GEOIP',
            'PROFILE',
        ]),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            filename: 'vendor-dev.js',
            minChunks: Infinity,
        }),
        new WebpackNotifier({
            title: 'webclient',
            contentImage: './static/favicon/nfl-icon-192.png',
            alwaysNotify: true,
            excludeWarnings: true,
        }),
    ],
})


module.exports = conf
