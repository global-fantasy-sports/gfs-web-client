const webpack = require('webpack')
const WebpackNotifier = require('webpack-notifier')

const configure = require('./webpack.common.js')

const vendorModules = [
    './vendor/sockjs-0.3.4.js',
    'blueimp-md5',
    'core-js',
    'fixed-data-table',
    'immutable',
    'minimal-flux',
    'moment',
    'rc-slider',
    'react-addons-css-transition-group',
    'react-addons-linked-state-mixin',
    'react-addons-pure-render-mixin',
    'react-dom',
    'react-motion',
    'react-overlays',
    'react-router',
    'react-tap-event-plugin',
    'recharts',
    'strftime',
]


const conf = configure({
    debug: true,

    entry: {
        selector: './main.selector.coffee',
        nba: './main.nba.coffee',
        vendor: vendorModules,
    },

    filename: '[name]-dev.js',

    devtool: process.env.WEBPACK_SOURCE_MAP,

    plugins: [
        new webpack.EnvironmentPlugin([
            'NODE_ENV',
            'BLOCK_GEOIP',
        ]),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            filename: 'vendor-dev.js',
            minChunks: Infinity,
        }),
        new WebpackNotifier({
            title: 'webclient',
            contentImage: './static/favicon/nfl-icon-192.png',
            alwaysNotify: true,
            excludeWarnings: true,
        }),
    ],
})


module.exports = conf
