.segment.players {
    .app__container {
        min-height: 0;
        padding-top: 100px;
    }
}

// Cell mixin
.cell-mix (@color, @bgColor, @fontWeight: 300, @fontSize: 12px, @fontFamily: @latoMedium) {
    color: @color;
    background-color: @bgColor;
    text-align: center;
    font: @fontWeight @fontSize @fontFamily;
    &.fixedDataTableCellLayout_wrap1 {
        border-bottom: 1px solid #dbdcde;
        border-collapse: collapse;
    }
}
.head-cell-mix (@color, @bgColor, @fontWeight: 300, @fontSize: 12px, @fontFamily: @latoBlack) {
    background-color: @bgColor;
    color: @color;
    font: @fontWeight @fontSize @fontFamily;
    height: 100%;
    width: 100%;
    text-align: center;
    text-transform: uppercase;
    border-top: 1px solid #b6b8ba;
    border-bottom: 1px solid #b6b8ba;
    border-collapse: collapse;
    .icon {
        margin: 0 10px 0 0;
    }
}


.players-list {
    max-width: 1200px;
    margin: 0 auto;

    .fixedDataTableLayout_header,
    .fixedDataTableLayout_hasBottomBorder {
        border-bottom: 1px solid #dbdcde;
    }
    .fixedDataTableCellLayout_main {
        border: none !important;
    }
    .fixedDataTableLayout_hasBottomBorder {
        border-bottom: 1px solid #dbdcde;
    }
    &__row {
        &:hover {
            .players-list__cell {
                background-color: #344e89;
                color: @white;
                border-color: #344e89;
            }
            .players-list__cell-right {
                background-color: #344e89;
                color: @white;
                border-color: #344e89;
            }
            .players-list__cell-highlight {
                background-color: #3b5693;
                color: @white;
                border-color: #344e89 #5e73a2;

            }
        }
    }
    &__group-head {
        .head-cell-mix(#4d68a7, @bgColor:#e1e1e1);
        .public_fixedDataTableCell_cellContent {
            .flex-container(row, space-between, center);
            padding: 0 5px;
        }
    }
    &__cell {
        .cell-mix(#707070, @bgColor:#f3f3f3, 300, 12px, @latoBlack);

        &--highlight {
            .cell-mix(#707070, @bgColor:#f2f5f9, 300, 12px, @latoBlack);
        }
        &--right {
            .cell-mix(#707070, @bgColor: @white, 300, 12px, @latoMedium);
        }
        &--left-align {
            text-align: left;
            .public_fixedDataTableCell_cellContent {
                padding-left: 10px;
            }
        }
        &--right-align {
            text-align: right;
            .public_fixedDataTableCell_cellContent {
                padding-right: 10px;
            }
        }
    }
    &__head-cell {
        .head-cell-mix(#4d68a7, @bgColor:#e1e1e1);

        &-right {
            .head-cell-mix(#797777, @bgColor:#e1e1e1, 300, 9px);
        }
        &-highlight {
            .head-cell-mix(#797777, @bgColor:#e1e1e1, 300, 9px, @latoMedium);
            text-align: center;
        }
        &-left {
            .head-cell-mix(#797777, @bgColor:#e1e1e1, 300, 9px);
        }
        &--left-align {
            text-align: left;
            .public_fixedDataTableCell_cellContent {
                padding-left: 10px;
            }
        }
        &--right-align {
            text-align: right;
            .public_fixedDataTableCell_cellContent {
                padding-right: 10px;
            }
        }
    }
}

.players-list-filters {
    max-width: 1200px;
    margin: 0 auto;

    &__top {
        background: #34373b url("/static/img/players-list-filter-bg.jpg") no-repeat center center;
        background-size: contain;
        padding: 25px 25px;
        border: 1px solid #00293a;
        .flex-container(row, space-between, center);
    }
    &__search {
        .flex-container(row, space-between, center);
    }
    &__bottom {
        background-color: #344e89;
        padding: 10px 25px;
        border: 1px solid #46619e;
        .flex-container(row, space-between, center);
    }
    &__sorting {
        .flex-container(row, space-between, center);
        padding: 5px 25px 17px;
        background-color: #394257;
        position: relative;
    }
    &__players-count {
        font: 300 10px @latoMedium;
        color: #9a9ea5;
        text-transform: uppercase;
    }
    &__sorting-underline {
        display: block;
        height: 1px;
        position: absolute;
        bottom: 10px;
        left: 0;
        right: 0;
        background: linear-gradient(to right, rgba(96, 103, 119, .2), rgb(96, 103, 119) 50%, rgba(96, 103, 119, .2) 100%);
    }
    &__applied-filters {
        .flex-container(row, space-between, center);
    }
    &__add-buttons {
        .flex-container(row, space-between, center);
        position: relative;
        .btn {
            margin: 0 5px;
        }
    }
    &__mode-buttons {
        .flex-container(row, space-between, center);
        .btn {
            margin: 0 5px;
        }
    }
    &__row {
        .flex-container(row, space-between, center);
        margin: 15px auto 0;
        height: auto;
        max-height: 1000px;
        overflow: hidden;
        opacity: 1;
        transition:
            max-height .5s ease-in,
            opacity .5s ease-in;

        &--collapsed {
            max-height: 0;
            opacity: 0;

            transition:
                opacity .2s ease-in,
                max-height .2s ease-out;
        }
    }
    &__toggle {
        min-width: 160px;
    }
    &__search-input {
        width: 342px;
    }
    .range-filter {
        &__label {
            .stroke;
            .white;
        }
    }

    .cards-sorting {
        position: relative;
    }
}

.applied-filter {
    position: relative;
    font: 300 10px @latoBlack;
    text-transform: uppercase;
    border-radius: 3px;
    background-color: #223766;
    margin: 0 5px;

    &__in {
        padding: 15px 10px;
        cursor: pointer;
        .flex-container(row, space-between, center);
    }

    &__type {
        color: #6f8ac8;
    }
    &__value {
        color: @white;
        padding: 0 10px 0 5px;
    }
    &__tooltip {
        position: absolute;
        display: none;
        top: 51px;
        left: 0;
        background-color: #33405b;
        border-radius: 7px;
        padding: 15px 20px;
        width: 270px;
        z-index: 10;
        transition: all .3s ease-in-out;
        &:before {
            content: '';
            position: absolute;
            top: -6px;
            left: 50px;
            width: 0;
            height: 0;
            border-style: solid;
            border-width: 0 8.5px 7px 8.5px;
            border-color: transparent transparent #33405b transparent;

        }
        &--visible {
            display: block;
        }

        .btn {
            margin: 0 auto 20px;
            display: block;
        }
    }
}


.favorite-players {
    max-height: 320px;
    overflow: hidden;

    .Grid {
        overflow-y: hidden;
    }

    &__empty-text {
        box-sizing: border-box;
        width: 300px;
        height: 320px;
        margin: 100px auto 0;
        color: @white;
        font: 300 16px @latoRegular;
        text-transform: uppercase;
    }
}

.favorite-players-enter {
    opacity: 0;
    max-height: 0;
}
.favorite-players-enter.favorite-players-enter-active {
    opacity: 1;
    max-height: 260px;
}
.favorite-players-leave {
    opacity: 1;
    max-height: 260px;
}
.favorite-players-leave.favorite-players-leave-active {
    opacity: 0;
    max-height: 0;
}
.favorite-players-enter,
.favorite-players-leave {
    transition: opacity 0.3s ease-out, max-height 0.3s ease-out;
}

.favorite-player {
    margin: 30px auto;
    width: 230px;
    height: 260px;
    background: ghostwhite;
    border-radius: 5px;
    overflow: hidden;
    text-align: center;

    &__head {
        position: relative;
        text-align: center;
        height: 120px;
        background-color: #e1e1e1;
    }

    &--remove {
        float: right;
        cursor: pointer;
        padding: 10px;
    }

    &__name {
        padding: 23px 20px;
        background: url("/static/img/players-card-bg.jpg") no-repeat center top;
        background-size: cover;
    }

    &__stats {
        .flex-container(row, space-between, stretch);
        height: 55px;
        padding: 12px 10px 5px;
        background-color: #414851;
    }
}
