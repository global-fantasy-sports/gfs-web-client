require('core-js/es6')

global.logger = require('./env/logger')
global.logger.init()

require('./lib/setupMoment')

## setup our shim over React
React = require('react')
global.React = require('./lib/setupReact')(React)

require('./lib/setupAnimationFrame')(window)

_ = require('underscore')
{Router} = require('react-router')

window.immutable = require('immutable')
window.flux = require('./flux')

routes = require('./routes')
{history} = require('./world')


getAppContainer = ->
    container = document.getElementById('app')
    return container if container
    document.body.insertAdjacentHTML('beforeend', '<div id="app" class="app-wrapper"></div>')
    document.getElementById('app')


ROUTE_PROPS = [
    'component'
    'getComponent'
    'childRoutes'
    'getChildRoutes'
    'onEnter'
    'onExit'
]


merge = (a, b) ->
    Object.assign(a, _.omit(b, ROUTE_PROPS))


createElement = (Component, props) ->
    routeProps = props.branch.reduce(merge, {})
    props = Object.assign(routeProps, props.routeParams, props)
    React.createElement(Component, props)


onUpdate = ->
    if not _.last(@state.branch).ignoreScroll
        flux.actions.state.scrollToTop({duration: 0})


window.startApp = ->
    flux.actions.connection.init()

    React.render(
        React.createElement(Router, {history, routes, createElement, onUpdate}),
        getAppContainer()
    )

    # TODO: use router for this
    if typeof window._ga is 'function'
        window._ga("send", "pageview", {page: window.location.href[1..]})


if process.env.NODE_ENV != 'production'
    global.__debug = require('./env/debug')
