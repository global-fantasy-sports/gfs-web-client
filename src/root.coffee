_ = require 'underscore'

{cx} = require './lib/utils'
{popupCell} = require './world'
Preloader = require './lib/ui/Preloader'
BlockScreen = require './components/BlockScreen'
PopupContainer = require './lib/ui/PopupContainer'
Flash = require './lib/ui/Flash'
{STATE, DISABLED_MESSAGES}  = require './lib/const'


exports.Root = React.create
    displayName: 'Root'

    contextTypes:
        router: React.PropTypes.object

    mixins: [
        flux.Connect('connection', (store)-> store.getState())
    ]

    componentDidMount: ->
        @update = => _.defer(=> @forceUpdate)
        popupCell.on(@update)
        if process.env.NODE_ENV != 'production'
            global.__router = @context.router

        React.findDOMNode(this).parentElement.className = 'app-wrapper'

    componentWillUnmount: ->
        popupCell.off(@update)

    onChallenge: ->
        @forceUpdate()

    onWarningClick: ->
        if @state.state is STATE.DISCONNECTED
            flux.actions.connection.reconnect()

    render: ({children}, {state, processingMessage}) ->
        if state is STATE.BLOCKED
            return `<BlockScreen />`
        if state is STATE.WAITING
            return `<section className={cx('loading', SPORT)}>
                <Preloader mod="main" />
            </section>`

        disabled = state in [STATE.DISCONNECTED, STATE.DISABLED]
        isProcessing = state == STATE.PROCESSING
        viewProps = Object.assign({disabled, onChallenge: @onChallenge}, @state)

        `<section className="app-wrapper__in">
            <Flash />
            <PopupContainer />
            {React.cloneElement(children, viewProps)}

            {disabled &&
            <div className="disabled-warning modal-content"
                 onTap={this.onWarningClick}>
                <p className="lead"
                   dangerouslySetInnerHTML={{__html: DISABLED_MESSAGES[state]}} />
            </div>
            }

            {isProcessing &&
            <div className='modal-overlay'>
                <div className='modal-overlay__background'></div>
                <div className='modal-overlay__content'>
                    <img className="preloader__image" src="/static/img/rolling.svg" />
                    {!!processingMessage &&
                    <span className='modal-overlay__message'>{processingMessage}</span>
                    }
                </div>
            </div>
            }
        </section>`
