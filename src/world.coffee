{history} = require('react-router/lib/BrowserHistory')
{cell} = require('./lib/signal')


fakeTour = cell({'started': 0})
flashCell = cell([])
popupCell = cell()


module.exports = {
    fakeTour
    flashCell
    history
    popupCell
}
