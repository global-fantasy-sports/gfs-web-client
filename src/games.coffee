{Pick18} = require './games/pick18/main.coffee'
{HoleInOne} = require './games/holeinone/main.coffee'
{FiveBall} = require './games/fiveball/main.coffee'
{Football} = require './games/nfl/main.coffee'
{BasketBall} = require './games/nba/main.coffee'


exports.GAMEDEF =
    'hole-by-hole': Pick18
    'handicap-the-pros': HoleInOne
    'five-ball': FiveBall
    'football': Football
    'basketball': BasketBall
