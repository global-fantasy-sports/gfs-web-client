_ = require 'underscore'

{Flag} = require 'lib/ui/Flag'
CandidateStats = require '../../../components/CandidateStats'
{sum} = require 'lib/utils'
TournamentLegend = require '../../../components/tournament/TournamentLegend'
GuideHint = require "components/guide/GuideHint"
HintTrigger = require "components/guide/HintTrigger"
PredictionHead = require 'components/games/PredictionHead'

BOUNDARIES =
    4: 400
    5: 550

exports.Handicap = React.create
    displayName: 'Handicap'
    mixins: [flux.Connect("wizard", (store) ->
        handicap: store.getCurrentHandicap()
        bonus: store.getCurrentBonus()
        pars: store.getCurrentPars()
        yardages: store.getCurrentYardages()
        participant: store.getCurrentParticipant()
    )]

    getInitialState: ->
        showStats: false
        animate: undefined
        frog: false

    componentDidMount: ->
        @scrollToTop()

    scrollToTop: ->
        dom = @refs.handicap.getDOMNode()
        dom.scrollIntoView()

    toggleHandicap: (n) ->
        (event)->
            event.preventDefault()
            flux.actions.wizard.toggleHandicap(n)

    canBeApplied: (i) ->
        boundary = BOUNDARIES[@state.pars[i]]
        (boundary and boundary <= @state.yardages[i]) or false

    isFilled: (index) ->
        possibleToFill = sum(@canBeApplied(i) for i in [0..17])
        if @state.handicap > possibleToFill
            handicap = possibleToFill

        handicap == sum(@state.bonus)

    getTd: (n) ->
        can = @canBeApplied(n)
        handicap = sum(@state.bonus)

        if not can
            return `<td className="disabled">&mdash;</td>`

        if @state.bonus[n]
            `<td className="predict__target" onTap={this.toggleHandicap(n)}>
                <span className={
                    this.isFilled(
                        this.props.index) ?
                        "predict__selected blink"
                        : "predict__selected"}>
                    -1
                </span>
            </td>`
        else if @state.handicap > handicap
            return `<td className="predict__target" onTap={this.toggleHandicap(n)}></td>`
        else
            return `<td></td>`

    toggleStats: ->
        @setState(showStats: not @state.showStats)

    render: ({index}, {showStats, bonus, pars, yardages, participant}) ->

        getTd = @getTd
        els = [0..17].map (i) ->
            `<tr key={i}>
                <td>{i + 1}</td>
                <td>{yardages[i]}</td>
                <td>{pars[i]}</td>
                <HintTrigger group="prediction" id="predict">
                    {getTd(i)}
                </HintTrigger>
            </tr>`

        `<section className="prediction" ref="handicap">
            <PredictionHead
                activeIndex={index}
                toggleStats={this.toggleStats}
                showStats={showStats}
                handicap={true}/>
            {showStats ?
            <section className="frog static">
                <CandidateStats participant={participant}/>
            </section>
            : null}

            <TournamentLegend mod="handicap"/>

            <table className="predict with-handicap">
                <thead className="predict__head" ref="header">
                    <tr key={-1}>
                        <th>hole</th>
                        <th>yardage</th>
                        <th>par</th>
                        <th className="th-handicap">
                            Handicap
                            <span className="out-of">
                                <big> {sum(bonus)} </big>
                                of
                                <big> {participant.handicap}</big>
                            </span>
                            <GuideHint children="Click on the cells to make your prediction"
                                       title="Click on the cells"
                                       pointer="bottom"
                                       group="prediction"
                                       id="predict"
                                       top="-110px"
                                       pos="centered"/>
                        </th>
                    </tr>
                </thead>
                <tbody className="predict__body">
                    {els}
                </tbody>
            </table>
        </section>`
