_ = require('underscore')

HandicapStep = require('../../components/golf/HandicapStep')
{Handicap} = require('./create/handicap')
{TournamentLive} = require('../../components/tournament/TournamentLive')
SelectTournamentRound = require('../../components/wizard/SelectTournamentRound')
ParticipantSelectionStep = require('../../components/golf/ParticipantSelectionStep')
WizardCompetitionRoomList = require('../../components/stakerooms/WizardCompetitionRoomList')
ChallengeFriends = require('../../components/friends/ChallengeFriends')

HoleInOneResults = require('../../components/games/hole-in-one/HoleInOneResults')
HoleInOneGameTableInfo = require('../../components/games/hole-in-one/HoleInOneGameTableInfo')
HoleInOneStakeRoomGame = require('../../components/games/hole-in-one/HoleInOneStakeRoomGame')

SelectRoundInfoTooltip = require('../../components/tooltip/SelectRoundInfoTooltip')
SelectParticipantInfoTooltip = require('../../components/tooltip/SelectParticipantInfoTooltip')
HandicapCardInfoTooltip = require('../../components/tooltip/HandicapCardInfoTooltip')
StakeSelectInfoTooltip = require('../../components/tooltip/StakeSelectInfoTooltip')
FriendsInfoTooltip = require('../../components/tooltip/FriendsInfoTooltip')
HandicapParticipantFilters = require('../../components/golf/HandicapParticipantFilters')


exports.HoleInOne = HoleInOne =
    name: "handicap-the-pros"
    oneDayGame: false

    StakeRoomView: HoleInOneStakeRoomGame
    LiveView: TournamentLive
    ResultsView: HoleInOneResults
    GameTableInfo: HoleInOneGameTableInfo

    steps: ['round', 'select', 'handicap', 'stake', 'friends']
    disableEdit: ['round', 'stake']

    round:
        view: SelectTournamentRound
        title: 'Select round'
        wizardIcon: 'w1_golf_round'
        tooltip: SelectRoundInfoTooltip

        isDone: () ->
            Boolean(flux.stores.wizard.getTournamentId() and flux.stores.wizard.getRoundNumber())

        predicate: ->
            if flux.stores.wizard.getCompetitionRoomId() > 0
                return true

    select:
        view: ParticipantSelectionStep
        title: 'Select candidates'
        wizardIcon: 'w2_candidates'
        tooltip: SelectParticipantInfoTooltip
        ParticipantFilters: HandicapParticipantFilters

        isDone: () ->
            flux.stores.roster.isValid()

        isValid: () ->
            HoleInOne.round.isDone()

    handicap:
        view: HandicapStep
        title: 'Handicap card'
        wizardIcon: 'w3_golf_cards'
        tooltip: HandicapCardInfoTooltip

        isDone: () ->
            false

        isValid: () ->
            HoleInOne.select.isDone()

    stake:
        view: WizardCompetitionRoomList
        title: 'Entry fee'
        wizardIcon: 'w4_fee'
        tooltip: StakeSelectInfoTooltip

        isDone: ->
            Boolean(flux.stores.wizard.getSelectedRoomIds().count())

        isValid: ->
            HoleInOne.handicap.isDone()

        predicate: ->
            if Boolean(flux.stores.wizard.getSelectedRoomIds().count())
                flux.actions.wizard.saveGame()
                return true
            else return false

        next: ->
            flux.actions.wizard.saveGame()

    friends:
        view: ChallengeFriends
        title: 'Invite friends'
        wizardIcon: 'w5_friends'
        tooltip: FriendsInfoTooltip

        next: ->
            flux.actions.state.clearMode()
            flux.actions.wizard.challengeFriends()
            flux.actions.wizard.finish()
            return 'finish'
