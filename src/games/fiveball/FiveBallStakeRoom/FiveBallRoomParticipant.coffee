{Flag} = require 'lib/ui/Flag.coffee'

exports.FiveBallRoomParticipant = React.create
    displayName: 'FiveBallRoomParticipant'
    args:
        participant: 'object'

    mixins: [flux.Connect("state", (store, props) ->
        isActive: store.getExpandedFiveBallParticipantId(props.game.id) == props.participant.id
    )]

    onTap: ->
        flux.actions.tap.gameParticipantFiveBall(
            @props.game.id,
            @props.participant.id
        )

    render: ({participant}, {isActive}) ->
        lastScore = participant.lastScore

        if lastScore in [null, undefined]
            lastScore = 'N/A'

        klass = "profile"
        if isActive
            klass += ' opened'
            personInfo = `<div className="profile__person-info">
                                <div className="person-info">
                                    <div className="person-info__name">
                                        {participant.getFullName()}
                                    </div>
                                    <div className="address stroke small">
                                        <p>
                                            <span>World Rank: </span>
                                            <span>#{participant.rank}</span>
                                        </p>
                                    </div>
                                </div>
                            </div>`
        else
            klass += ' closed'
            openHandler = `<div className="profile__handler">
                                <div className="open-handler"></div>
                            </div>`

        `<div className={klass}  onTap={this.onTap}>
            <div className="profile__avatar">
               <div className="avatar">
                    <Flag country={participant.country}/>
                </div>
            </div>
            {personInfo}
            {openHandler}
        </div>`
