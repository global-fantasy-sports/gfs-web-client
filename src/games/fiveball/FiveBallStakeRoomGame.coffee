_ = require "underscore"
ItemWrapper = require '../../components/stakerooms/ItemWrapper'
StakeRoomProfile = require '../../components/stakerooms/StakeRoomProfile'
{FiveBallRoomParticipant} = require "./FiveBallStakeRoom/FiveBallRoomParticipant.coffee"
UserPoints = require "../../components/games/UserPoints"
{Btn} = require "lib/ui.coffee"
ActiveRoundSwitcher = require "../../components/ActiveRoundSwitcher"
ReactCSSTransitionGroup = require('react-addons-css-transition-group')


exports.FiveBallStakeRoomGame = React.create
    displayName: "FiveBallStakeRoomGame"

    mixins: [
        flux.Connect("participants", (store, props) ->
            participant1: store.getParticipant(props.game.participantId1)
            participant2: store.getParticipant(props.game.participantId2)
            participant3: store.getParticipant(props.game.participantId3)
            participant4: store.getParticipant(props.game.participantId4)
            participant5: store.getParticipant(props.game.participantId5)
        )
        flux.Connect("state", (store, props) ->
            gameId = store.getExpandedScorecardGameId()
            participantId = store.getExpandedScorecardParticipantId()

            isExpanded: gameId == props.game.id
            expandedParticipant: flux.stores.participants.getParticipant(participantId)
        )
    ]

    componentWillMount: ->
        _.defer (=> flux.actions.state.expandParticipantFiveBall(
            @props.game.id,
            @props.game.participantId1
        )), 1

    closeScorecard: ->
        flux.actions.tap.closeScorecard()

    openScorecard: ->
        flux.actions.tap.gameParticipantFiveBall(
            @props.game.id,
            @props.game.participantId1
        )

    render: ({game, menu, label, children, onParticipant, viewGame, idx, gameProperties},
             {user, participant1, participant2, participant3, participant4, participant5, isExpanded, expandedParticipant}) ->

        {LiveView} = gameProperties

        if not (participant1.id and participant2.id and participant3.id and participant4.id and participant5.id)

            return `<div />`

        row = `<ItemWrapper game={game}
                            mod={isExpanded ? 'dark multi-profile' :
                            label ? 'dark label' : 'dark'}
                            active={viewGame}>
            <section className="f-23">
                <StakeRoomProfile user={game.userData} label={label} idx={idx}
                                    mod="five-ball"/>
            </section>
            <section className="f-57 no-indent">
                <div className="multi-profile five-ball">
                    <FiveBallRoomParticipant participant={participant1}
                                             game={game}/>
                    <FiveBallRoomParticipant participant={participant2}
                                             game={game}/>
                    <FiveBallRoomParticipant participant={participant3}
                                             game={game}/>
                    <FiveBallRoomParticipant participant={participant4}
                                             game={game}/>
                    <FiveBallRoomParticipant participant={participant5}
                                             game={game}/>
                </div>
            </section>
            <section className="f-13">
                <div className="game-settings game-settings_row">
                    <UserPoints game={game} />
                </div>
            </section>
            <section className="f-8">
                <div className="game-settings row">
                    {isExpanded ?
                        <Btn mod ="white square regular small"
                            iconBg={true}
                            icon="close-red"
                            onTap={this.closeScorecard}>
                            Close
                        </Btn>
                        :
                        <Btn mod="blue square regular small"
                            iconBg={true}
                            icon="view"
                            onTap={this.openScorecard}>
                            Score card
                        </Btn>
                    }
                </div>
            </section>
        </ItemWrapper>`

        userName = user?.name
        participant = expandedParticipant.getFullName()

        liveEl = `<section>
            <div className="rounds-score-card">
                <div className="stroke white x2-medium">
                        {userName} score card for
                    <span className="stroke white x2-medium text-up"> {participant}</span>
                </div>
                <ActiveRoundSwitcher />
            </div>

            <LiveView game={game}
                participant={expandedParticipant}
                bonus={immutable.List()}
                gameProperties={gameProperties}/>
        </section>`

        `<div>
            {row}
            <ReactCSSTransitionGroup transitionName="slide-down"
                                     transitionEnterTimeout={1000}
                                     transitionLeaveTimeout={1000}>
                {isExpanded ? liveEl : null}
            </ReactCSSTransitionGroup>
        </div>`
