_ = require('underscore')

ParticipantSelectionStep = require('../../components/golf/ParticipantSelectionStep')
WizardCompetitionRoomList = require('../../components/stakerooms/WizardCompetitionRoomList')
{TournamentLive} = require('../../components/tournament/TournamentLive')
ChallengeFriends = require('../../components/friends/ChallengeFriends')

{FiveBallResults} = require('./games')
{FiveBallGameTableInfo} = require('./FiveBallGameTableInfo')
{FiveBallStakeRoomGame} = require('./FiveBallStakeRoomGame')
SelectTournamentRound = require('../../components/wizard/SelectTournamentRound')
SelectRoundInfoTooltip = require('../../components/tooltip/SelectRoundInfoTooltip')
FiveballSelectParticipantInfoTooltip = require('../../components/tooltip/FiveballSelectParticipantInfoTooltip')
StakeSelectInfoTooltip = require('../../components/tooltip/StakeSelectInfoTooltip')
FriendsInfoTooltip = require('../../components/tooltip/FriendsInfoTooltip')
FiveBallParticipantFilters = require('../../components/golf/FiveBallParticipantFilters')


exports.FiveBall = FiveBall =
    name: 'five-ball'
    title: 'TITLE'
    oneDayGame: false

    StakeRoomView: FiveBallStakeRoomGame
    LiveView: TournamentLive
    ResultsView: FiveBallResults
    GameTableInfo: FiveBallGameTableInfo

    steps: ['round', 'select', 'stake', 'friends']
    disableEdit: ['round', 'stake']

    round:
        view: SelectTournamentRound
        title: 'Select round'
        wizardIcon: 'w1_golf_round'
        tooltip: SelectRoundInfoTooltip

        isDone: () ->
            Boolean(flux.stores.wizard.getTournamentId() and flux.stores.wizard.getRoundNumber())

        predicate: ->
            if flux.stores.wizard.getCompetitionRoomId() > 0
                return true

    select:
        view: ParticipantSelectionStep
        title: 'Select candidates'
        wizardIcon: 'w2_candidates'
        tooltip: FiveballSelectParticipantInfoTooltip
        ParticipantFilters: FiveBallParticipantFilters

        isDone: () ->
            flux.stores.roster.isValid()

        isValid: () ->
            FiveBall.round.isDone()

    stake:
        view: WizardCompetitionRoomList
        title: 'Entry fee'
        wizardIcon: 'w4_fee'
        tooltip: StakeSelectInfoTooltip

        isDone: ->
            Boolean(flux.stores.wizard.getSelectedRoomIds().count())

        isValid: ->
            FiveBall.select.isDone()

        predicate: ->
            if Boolean(flux.stores.wizard.getSelectedRoomIds().count())
                flux.actions.wizard.saveGame()
                return true
            else return false

        next: ->
            flux.actions.wizard.saveGame()

    friends:
        view: ChallengeFriends
        title: 'Invite friends'
        wizardIcon: 'w5_friends'
        tooltip: FriendsInfoTooltip

        next: ->
            flux.actions.state.clearMode()
            flux.actions.wizard.challengeFriends()
            flux.actions.wizard.finish()
            return 'finish'
