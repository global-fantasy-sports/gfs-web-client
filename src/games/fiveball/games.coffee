{TournamentMember} = require "../../components/tournament/TournamentMember"
{TournamentResults} = require "../../components/tournament/TournamentResults"
{Helpers} = require "lib/Helpers"


exports.FiveBallResults = FiveBallResults = React.create
    displayName: "FiveBallResults"

    mixins: [flux.Connect("participantRoundResults", (store, props) ->
        participantRoundResult: store.getResult({
            "participantId": props.participant.id,
            "roundNumber": props.roundNumber
        })
    )]

    render: ({game}, {participantRoundResult}) ->
        {pars, strokes} = participantRoundResult

        strokeClass = (i) ->
            return ""

        points = Helpers.calculateRowPoints(strokes, pars, [])

        `<TournamentResults {...this.props}>
            <TournamentMember
                participantRoundResult={participantRoundResult}
                game={game}
                strokeClass={strokeClass}
                points={points}/>
        </TournamentResults>`
