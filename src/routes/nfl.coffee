_ = require('underscore')

{Root} = require('../root')
GameWrapper = require('../components/GameWrapper')

ChallengeRoom = require('../components/ChallengeRoom')
GameWizard = require('../components/wizard/GameWizard')
{GAMEDEF} = require('../games')
PlayersListHandler = require('../components/players/PlayersListHandler')

{Leaderboard} = require('../components/leaderboard/leaderboard')

CompetitionRoom = require('../components/competitionRoom')
Lineups = require('../components/roster/Lineups')
ImportLineups = require('../components/roster/ImportLineups')
RosterContainer = require('../components/roster/RosterContainer')
DesktopLanding = require('../components/landing/DesktopLanding')
HeaderNav = require('../components/HeaderNav')
{Footer} = require('../components/landing')
Page404 = require('../components/Page404')

{commonRoutes, redirect} = require('./common')

if SPORT == 'nfl'
    gameCreate = {
        path: 'salarycap(/:step)/'
        component: GameWizard
        gameType: 'salarycap'
        title: 'Football'
        gameDef: GAMEDEF['football']
        subheader: false
        footer: false
    }
if SPORT == 'nba'
    gameCreate = {
        path: 'salarycap(/:step)/'
        component: GameWizard
        gameType: 'salarycap'
        title: 'Basketball'
        gameDef: GAMEDEF['basketball']
        subheader: false
        footer: false
    }


routes = [
    {
        path: '', component: DesktopLanding, page: 'home', ignoreScroll: true, onEnter: (state, transition) ->
            nextUrl = localStorage.getItem('nextUrl')
            if nextUrl
                localStorage.removeItem('nextUrl')
                transition.to(nextUrl)
    }
    {path: 'games/', component: DesktopLanding, page: 'mygames', ignoreScroll: true}
    {path: 'schedule/', component: DesktopLanding, page: 'schedule', ignoreScroll: true}
    {path: 'competitions/', component: DesktopLanding, page: 'competition-rooms', ignoreScroll: true}

    {path: 'lineups/', component: Lineups}
    {path: 'lineups/import/', component: ImportLineups}
    {
        path: 'lineups/:id/', component: RosterContainer, onEnter: ->
            _.defer(->
                flux.actions.lineup.init()
            )
    }

    gameCreate

    {path: 'challenge/:challengeId/', component: ChallengeRoom, title: 'Challenge'}

    {path: 'competition/:roomId/', component: CompetitionRoom, title: 'Competition Room'}

    {path: 'leaderboard/', component: Leaderboard, title: 'Leaderboard'}

    {path: 'players/', component: PlayersListHandler}
]

for route in routes
    route.path = "/#{SPORT}/#{route.path}"


gameRoutes = {
    component: GameWrapper
    subheader: HeaderNav
    footer: Footer
    childRoutes: _.flatten([
        commonRoutes.map((el) -> Object.assign({}, el))
        routes
    ])
}

module.exports = {
    component: Root
    childRoutes: [
        gameRoutes
        redirect('#_=_', '/')
        {path: '*', component: Page404}
    ]
}
