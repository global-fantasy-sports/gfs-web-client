if SPORT is 'golf'
    module.exports = require('./golf')

else if SPORT in ['nfl', 'nba']
    module.exports = require('./nfl')

else
    module.exports = require('./selector')