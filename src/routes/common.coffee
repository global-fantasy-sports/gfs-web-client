_ = require('underscore')

TermsOfUse = require('../components/legal/TermsOfUse')
PrivacyPolicy = require('../components/legal/PrivacyPolicy')
ContestRules = require('../components/ContestRules')
{StatusPopupHandler, checkEmailConfirmation, unsubscribe} = require('../components/emails')
{CONFIRM_EMAIL_MESSAGES, UNSUBSCRIBE_MESSAGES} = require('../lib/const')
{UserAccount} = require('../components/UserAccount/UserAccount')
{Deposit} = require('../components/payment/index')


exports.redirect = redirect = (from, to) ->
    path: from
    onEnter: (props, transition) ->
        transition.to(to)


exports.commonRoutes = [
    {path: '/policy/', component: PrivacyPolicy}
    {path: '/terms/', component: TermsOfUse}
    {path: '/contest-rules/', component: ContestRules}

    {
        path: '/confirm-email(/:status(/:message))/'
        component: StatusPopupHandler
        func: checkEmailConfirmation
        messages: CONFIRM_EMAIL_MESSAGES
    }
    {
        path: '/unsubscribe(/:token)/'
        component: StatusPopupHandler
        func: unsubscribe
        messages: UNSUBSCRIBE_MESSAGES
    }
    redirect('/subscription/', '/useraccount/')
    {path: '/useraccount/', component: UserAccount}

    {path: '/deposit/', component: Deposit}
    {path: '/withdraw/', component: Deposit, page: 'withdraw'}

    {
        path: '/payment/', onEnter: ({location}, transition) ->
            {query} = location
            flux.actions.payment.processDepositResponse(query) if query
            transition.to("/deposit/")
    }
]
