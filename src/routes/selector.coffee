_ = require('underscore')
{commonRoutes, redirect} = require('./common')
{Root} = require('../root')
SelectorPage = require('../selector/SelectorPage')
GameWrapper = require('../components/GameWrapper')
Page404 = require('../components/Page404')




module.exports = {
    component: Root
    childRoutes: [
        {
            component: GameWrapper
            childRoutes: _.flatten([
                commonRoutes.map((el) -> Object.assign({}, el))
                {path: '/', component: SelectorPage}
            ])
        }
        redirect('/#_=_', '/')
        {path: '*', component: Page404}
    ]
}
