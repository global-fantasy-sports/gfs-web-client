_ = require('underscore')

{Root} = require('../root')
GameWrapper = require('../components/GameWrapper')

Tutorial = require('../components/Tutorial')
ChallengeRoom = require('../components/ChallengeRoom')
GameWizard = require('../components/wizard/GameWizard')
{GAMEDEF} = require('../games')
HeaderNav = require('../components/HeaderNav')

{commonRoutes, redirect} = require('./common')

{Leaderboard} = require('../components/leaderboard/leaderboard')
{UserAccount} = require('../components/UserAccount/UserAccount')
CompetitionRoom = require('../components/competitionRoom')
GolfDesktopLanding = require('../components/landing/GolfDesktopLanding')
{Rules} = require('../components/Rules')


golfCommonRoutes = [
    {
        path: '/golf/rules/', component: Rules, ignoreScroll: true, title: 'Rules', childRoutes: [
            {path: 'hole-by-hole/', ignoreScroll: true, title: 'Rules'}
            {path: 'handicap-the-pros/', ignoreScroll: true, title: 'Rules'}
            {path: 'five-ball/', ignoreScroll: true, title: 'Rules'}
        ]
    }
]

commonRoutes = commonRoutes.concat(golfCommonRoutes)

golfRoutes = [
    {path: '', component: GolfDesktopLanding, page: 'home', ignoreScroll: true}
    {path: 'games/', component: GolfDesktopLanding, page: 'mygames', ignoreScroll: true}
    {path: 'schedule/', component: GolfDesktopLanding, page: 'schedule', ignoreScroll: true}
    {path: 'competitions/', component: GolfDesktopLanding, page: 'competition-rooms', ignoreScroll: true}

    redirect('/subscription/', '/useraccount/')

    {
        path: 'hole-by-hole(/:step)/'
        component: GameWizard
        gameType: 'hole-by-hole'
        title: 'Hole By Hole'
        gameDef: GAMEDEF['hole-by-hole']
        subheader: false
    }

    {
        path: 'handicap-the-pros(/:step)/'
        component: GameWizard
        gameType: 'handicap-the-pros'
        title: 'Handicap The Pros'
        gameDef: GAMEDEF['handicap-the-pros']
        subheader: false
    }

    {
        path: 'five-ball(/:step)/'
        component: GameWizard
        gameType: 'five-ball'
        title: '5 Ball'
        gameDef: GAMEDEF['five-ball']
        subheader: false
    }

    {path: 'challenge/:challengeId/', component: ChallengeRoom, title: 'Challenge'}

    {path: 'competition/:roomId/', component: CompetitionRoom, title: 'Competition Room'}

    {
        path: 'tutorial/', component: Tutorial, childRoutes: [
            {path: 'hole-by-hole/', title: 'Tutorial'}
            {path: 'handicap-the-pros/', title: 'Tutorial'}
            {path: 'five-ball/', title: 'Tutorial'}
        ]
    }

    {path: 'leaderboard/:tournamentId/', component: Leaderboard, title: 'Leaderboard'}
    {path: 'useraccount/', component: UserAccount, title: 'Account page'}
]

for route in golfRoutes
    route.path = "/golf/#{route.path}"


gameRoutes = {
    component: GameWrapper
    subheader: HeaderNav
    childRoutes: _.flatten([
        commonRoutes.map((el) -> Object.assign({}, el))
        golfRoutes
    ])
}


module.exports = {
    component: Root
    childRoutes: [
        gameRoutes
        redirect('#_=_', '/')
    ]
}
