EMPTY = {}


exports.formatSalary = (value, emptyValue = EMPTY) ->
    if value
        "$#{value.toFixed( 0 ).replace( /\B(?=(\d{3})+(?!\d))/g, ',' )}"
    else if emptyValue is EMPTY
        value
    else
        emptyValue


exports.formatPoints = (value, emptyValue = EMPTY) ->
    if typeof value is 'number'
        value.toFixed(2)
    else if emptyValue is EMPTY
        value
    else
        emptyValue


exports.formatHeight = (value) ->
    inches = value % 12
    feet = (value - inches) / 12
    "#{feet}'#{inches}\""


exports.formatWeight = (value) ->
    "#{value}"


exports.formatFPPG = (value) ->
    if typeof value is 'number'
        value.toFixed(2)
    else
        'N/A'


exports.formatName = (value) ->
    name = value.split(' ').map((v) ->
        `<p>{v}</p>`
    )
