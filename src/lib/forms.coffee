_ = require('underscore')
Connect = require('../flux/connectMixin')
{cx, capitalize} = require('./utils')
{Icon} = require('./ui')

ValidationInput = React.create
    displayName: "ValidationInput"

    mixins: [Connect("forms", (store, props) ->
        inProgress: store.isInProgress(props.validationId)
        isSuccess: store.isSuccess(props.validationId)
        isError: store.isError(props.validationId)
        error: store.getError(props.validationId)
        value: store.getValue(props.validationId)
    )]

    componentWillMount: ->
        @setDefault(@props)

    componentWillReceiveProps: (props) ->
        @setDefault(props)

    setDefault: (props) ->
        if props.defaultValue
            flux.actions.forms.setValue(props.validationId, props.defaultValue)

    onChange: (e) ->
        flux.actions.forms.setValue(@props.validationId, e.target.value)

    onKeyPress: (e) ->
        if e.which == 13 and @props.onEnter then @props.onEnter(flux.stores.forms.getValue(@props.validationId))

    onBlur: (e) ->
        if @props.onValidate
            flux.actions.forms.startProgress(@props.validationId)

            error = @props.onValidate(@state.value)

            if error
                flux.actions.forms.setError(@props.validationId, error)
            else
                flux.actions.forms.setSuccess(@props.validationId)

        if @props.onSave
            flux.actions.forms.startProgress(@props.validationId)
            @props.onSave(@state.value)

    render: ({className, label, name, hint, mod, theme}, {inProgress, isSuccess, isError, error, value})->
        className ||= ""

        cls = cx(
            {
                "ui-input__el": true
                "error-value": isError
                "success-value": isSuccess
            }
        )

        `<div className={mod ? "ui-input " + mod + " " + className : "ui-input " + className}>
            {label &&
            <label className={"ui-input__label" + (theme ? " ui-input__label--theme-" + theme : "")}
                   htmlFor={name}>
                {label}
            </label>}
            <div className="ui-input__in">
                <input {...this.props}
                    onChange={this.onChange}
                    onKeyPress={this.onKeyPress}
                    className={cls}
                    value={value}
                    onBlur={this.onBlur}/>
                {(!isSuccess && inProgress) && <img className="ui-input__loader" src="/static/img/rolling.svg" />}
                {isSuccess && <Icon i="success" />}
                {isError && <InputMessage message={error} />}
            </div>
            {hint ? <div className="ui-input__hint">{hint}</div> : null}
        </div>`

exports.InputMessage = InputMessage = React.create
    displayName: 'InputMessage'

    render: ({message, mod})->
        cls = if mod then "ui-input-message " + mod else "ui-input-message"

        `<div className={cls}>
            {message}
            <span className="ui-input-message__arrow"></span>
        </div>`

exports.Input = Input = React.create
    displayName: 'Input'

    getDefaultProps: ->
        type: 'text'
        className: ''
        mod: ''

    value: ->
        React.findDOMNode(@refs['input']).value

    focus: ->
        React.findDOMNode(@refs['input']).focus()

    render: ({className, name, label, error, isValid, hint, validationId, mod}) ->
        if validationId then return `<ValidationInput {...this.props} />`

        inputClass = cx("ui-input__el", mod, {
            "error-value": Boolean(error)
            "success-value": isValid
        })

        `<div className={"ui-input " + className}>
            {label &&
                <label className="ui-input__label" htmlFor={name}>
                    {label}
                </label>
            }
            <div className="ui-input__in">
                <input {...this.props} className={inputClass} ref="input" />
                {error && <InputMessage message={error} />}
            </div>

            {hint && <div className="ui-input__hint">{hint}</div>}
         </div>`


exports.Checkbox = React.create
    displayName: 'Checkbox'

    render: ({className, onChange, children, name, checkedLink, bigLabel}) ->
        cls = cx(className,
            "ui-checkbox__label": true
            "big": bigLabel
        )

        `<div className="ui-checkbox">
            <label className={cls}
                htmlFor={name}>
                <input {...this.props}
                        type="checkbox"
                        className="ui-checkbox__el"
                        id={name}
                        children={null}/>
                {children}
            </label>
        </div>`
