_ = require 'underscore'
validate = require 'validate.js'
{Map, OrderedMap, Set, Seq} = require 'immutable'

xhr = require 'lib/xhr'
countries = require './constants/countries'
states = require './constants/states'


validate.validators.presence.message = 'is required'
###
    Custom validators
###

PHONE_RE = /^\+?\d{10}$/
ZIP_RE = /^\d{5}(?:-\d{4})?$/

COUNTRIES = new Set(countries.map((c)-> c.value).filter(Boolean))
STATES = new Set(states.map((s)-> s.value).filter(Boolean))


Object.assign(validate.validators, {
    emailAvailable: (value)->
        return null if not value

        xhr.post('/auth/check-email', {email: value})
            .catch(-> Promise.resolve({error: 'check failed'}))
            .then(({error})-> Promise.resolve(error))

    terms: (value)->
        if value isnt true
            return '^Please accept Terms and Conditions'

    phone: (value)->
        if value and not value.replace(/[\()-]/g, '').match(PHONE_RE)
            return '^Please enter a valid phone number'

    zip: (value)->
        if value and not value.match(ZIP_RE)
            return '^Please enter a valid ZIP code'

    state: (value, options, name, attrs)->
        if attrs.country is 'US' and not value
            return '^Please select your state'

    country: (value)->
        if value and not COUNTRIES.has(value)
            return "^#{value} is not allowed"

    withdrawable: (value, options)->
        availableUsd = options.max()
        if availableUsd < 1
            return '^You have nothing to withdraw yet'
        if not value
            return '^Please enter an amount to withdraw'
        value = parseInt(value)
        if not value or value < 1
            return '^Incorrect amount'
        if value > availableUsd
            return "^You can withdraw up to #{availableUsd}"

    routingNum: (value)->
        unless value and value.match(/^\d{9}$/)
            return 'must be 9 digits long'

    accountNum: (value)->
        unless value and value.match(/^\d{1,17}$/)
            return 'must be up to 17 digits long'
})


###
    Helpers
###
wrapErrors = (errors)->
    new Seq(errors).map((error)-> _.first(error)).toMap()

runValidation = (values, fields, fieldName)->
    constraints = fields.toSeq().map((f)-> f.validation)

    if fieldName
        constraints = constraints.filter((v, k)-> k is fieldName)
        values = values.filter((v, k)-> k is fieldName)

    validate.async(values.toObject(), constraints.toObject(), {wrapErrors})


class Field

    constructor: (@name, props={})->
        @props = _.omit(props, 'default', 'validate', 'JSONName')
        @validation = Object.assign(props.validate ? {presence: true})
        @default = props.default ? null
        @JSONName = props.JSONName ? @name

    getValue: (form)->
        form.values.get(@name, @default)

    getError: (form)->
        form.errors.get(@name)

    fromEvent: (event)->
        return event unless event and event.target
        if @props.type in ['checkbox', 'radio']
            event.target.checked
        else
            event.target.value

    fromJSON: (data)->
        data[@JSONName] ? @default

    toJSON: (form, data={})->
        data[@JSONName] = @getValue(form)
        data

###
    Form
###

class Form

    constructor: (fields, options={})->
        @fields = new OrderedMap(Object.keys(fields).map((name)->
            [name, new Field(name, fields[name])]))

        initial = options.initial ? {}
        @values = new Map(@fields.map((f)-> initial[f.name] ? f.default))
        @errors = new Map()

        @_listeners = new Set()

    get: (fieldName, defaultValue)->
        @values.get(fieldName, defaultValue)

    set: (fieldName, value, options={})->
        return unless @fields.has(fieldName)
        @clearErrors(fieldName, {silent: true})
        @values = @values.set(fieldName, value)
        @notify() unless options.silent

    update: (values, options={})->
        newValues = new Seq(values).filter((value, key)=> @fields.has(key))
        @values = @values.merge(newValues)
        @notify() unless options.silent

    validate: (fieldName)->
        @clearErrors(fieldName)
        runValidation(@values, @fields, fieldName).catch((errors)=>
            @setErrors(errors)
            Promise.reject(errors)
        )

    getErrors: (fieldName)->
        if fieldName then @errors.get(fieldName) else @errors.toObject()

    setErrors: (errors, options={})->
        @errors =
            if options.merge then @errors.merge(errors)
            else new Map(errors)
        @notify() unless options.silent

    clearErrors: (fieldName, options={})->
        return if fieldName and not @errors.has(fieldName)
        @errors = if fieldName then @errors.delete(fieldName) else @errors.clear()
        @notify() unless options.silent

    props: (fieldName)->
        field = @fields.get(fieldName)
        Object.assign({
            name: field.name
            value: field.getValue(this)
            error: field.getError(this)
            onChange: (event)=> @set(field.name, field.fromEvent(event))
            onBlur: ()=> @validate(field.name).catch((errors)=> logger.log(errors, 'validation errors', 'Validation'))
            onFocus: ()=> @clearErrors(field.name)
        }, field.props)

    listen: (handler)->
        @_listeners = @_listeners.add(handler)
        return ()=> @_listeners = @_listeners.remove(handler)

    notify: ->
        @_listeners.forEach((handler)=> handler(this))

    toJSON: ->
        @fields.reduce(((res, f)=> f.toJSON(this, res)), {})

    fromJSON: (data={})->
        @update(@fields.toKeyedSeq().map((f)-> f.fromJSON(data)))


module.exports = {Form, validate}
