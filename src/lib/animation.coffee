exports.translate = ({x, y}, units='%')->
    x = if x then x + units else 0
    y = if y then y + units else 0
    transform = "translate3d(#{x}, #{y}, 0)"
    {transform, WebkitTransform: transform}
