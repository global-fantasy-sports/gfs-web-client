_ = require('underscore')

bemClassName = require('bem-classname')
{cx, prevent} = require('./utils')
{Titlebar} = require('./ui/Titlebar')
Logo = require('./ui/Logo')


ReactCSSTransitionGroup = require('react-addons-css-transition-group')

exports.LogoTitle = LogoTitle = React.create
    displayName: 'LogoTitle'
    args:
        type: 'string'
        title: 'string'

    render: ({type, title}) ->
        `<div className="logo-title">
            <Logo logo={type.toLowerCase() + '-small'}/>
            <Titlebar className="light no-indent" title={title} mod="h2"/>
        </div>`

exports.Tokens = Tokens = React.create
    displayName: 'Tokens'

    render: ({m, mod, symb, icon}) ->
        symb ?= ""
        cls = "tokens__symb "

        amount = if _.isObject(m) then m.token else m

        `<span className={!mod ? 'tokens' : 'tokens ' + mod}>
            {icon ? <Icon i="token"/> : <span className={cls + symb}>Ŧ</span>}
            <ReactCSSTransitionGroup
                transitionName="zoomIn"
                transitionLeave={false}
                transitionEnterTimeout={200}>
                <span key={amount}>
                    <span>{numFmt(amount)}</span>
                </span>
            </ReactCSSTransitionGroup>
        </span>`

exports.Dollars = Dollars = React.create
    displayName: 'Dollars'

    getDefaultProps: -> {
        symb: ''
        mod: ''
    }

    render: ({m, mod, symb, icon}) ->
        amount = if _.isObject(m) then m.usd else m
        `<span className={'dollars ' + mod}>
            {icon ? <Icon i="dollar"/> : <span className={'dollars__symb ' + symb}>$</span>}
            <span>{numFmt(amount)}</span>
        </span>`

exports.Icon = Icon = React.create(
    displayName: 'Icon'

    getDefaultProps: -> {
        mod: ''
        i: ''
    }

    render: ({i, className, children, mod, iconMod}) ->
        mods = mod.split(' ')
        icons = i.split(' ')
        iconClasses = icons.map((x) -> "icon-#{x}")
        iconClassesWithMods = []
        for iconClass in iconClasses
            iconClassesWithMods.push(iconClass)
            for mod in mods
                iconClassesWithMods.push("#{iconClass}-#{mod}")

        cls = cx("icon", className, iconClassesWithMods.join(" "), iconMod)

        `<i {...this.props} className={cls}>{children}</i>`
)

exports.Link = class Link extends React.Component

    @propTypes: {
        tag: React.PropTypes.string
        children: React.PropTypes.node.isRequired
        to: React.PropTypes.string.isRequired
        query: React.PropTypes.object
        state: React.PropTypes.object
    }

    @defaultProps: {
        tag: 'span'
    }

    @contextTypes:
        router: React.PropTypes.object

    onTap: (event)=>
        event.preventDefault() if event
        {to, query, state, children, disabled} = @props
        if disabled
            return
        if children.props
            {onTap} = children.props
            onTap(event) if typeof onTap is 'function'
        {router} = @context
        if to is '__back'
            router.goBack()
        else
            router.transitionTo(to)


    render: ->
        {children} = @props
        if React.isValidElement(children)
            React.cloneElement(children, {onTap: @onTap})
        else
            {tag, to, href} = @props
            if tag is 'a' and not href
                href = to
            newProps = Object.assign({}, @props, {onTap: @onTap, href})
            unless '#' in href
                newProps.onClick = prevent
            React.createElement(tag, newProps, children)


exports.Btn = Btn = React.create
    displayName: 'Btn'
    args:
        mod: 'string?'
        icon: 'string?'
        iconBg: 'bool?'

    render: ({className, children, mod, icon, iconBg, btnText, activeState}) ->
        className ||= ""
        cls = "#{className} btn"

        if mod
            cls += ' ' + mod.split(' ').map((x) -> ('btn--' + x)).join(' ')

        disabled = if mod then mod.split(' ').filter((x) -> x == "disabled") else ""
        showIcon = Boolean(icon)

        textClass = cx
            "btn__text": true
            "black": btnText

        `<button {...this.props} className={activeState ? cls + " " + activeState : cls}>
            {iconBg ?
            <i className="icon-bg"><Icon i={icon}/></i>
            :
            showIcon ? <Icon i={icon}/> : null
            }
            <span className={textClass}>
                {children}
            </span>
        </button>`

exports.BtnExpand = BtnExpand =React.create(
    displayName: "BtnExpand"

    render: ({mod}) ->

        `<button {...this.props} className={bemClassName("button-expand", [mod])}>
            <i className="button-expand__dot"/>
            <i className="button-expand__dot"/>
            <i className="button-expand__dot"/>
        </button>`
)

class exports.BtnLink extends React.Component

    @propTypes:
        to: React.PropTypes.string.isRequired
        query: React.PropTypes.object
        state: React.PropTypes.object

    render: ->
        linkProps = _.pick(@props, 'to', 'query', 'state')
        props = _.omit(@props, 'to', 'query', 'state')
        `<Link {...linkProps}>
            <Btn {...props} />
        </Link>`

exports.MenuButton = MenuButton = React.create
    displayName: 'MenuButton'
    args:
        onTap: 'func?'

    render: ({className, children, i, iclassName, active, onTap, disabled, name}) ->
        cls = cx('menu-button', {active, disabled})
        className = className or ""
        cls += " #{className}"

        `<span className={cls} onTap={disabled ? _.noop : onTap} name={name}>
            {i && <Icon i={i} className={iclassName}/>}
            {children}
        </span>`

exports.MenuLink = (props) ->
    buttonProps = _.omit(props, 'to')
    `<Link to={props.to}>
        <MenuButton {...buttonProps} />
    </Link>`


exports.ActionBar = ActionBar = React.create
    displayName: 'ActionBar'

    render: ({children}, S) ->
        els = React.Children.map(children, (item, index) ->
            `<div className="actionbar__el" key={index}>{item}</div>`
        )

        `<div {...this.props} className="actionbar">
            {els}
        </div>`

round = (x, int = false) ->
    f = Math.floor(x)
    dec = Math.round((x - f) * 10)
    if dec == 10
        f += 1
        dec = 0
    if not dec or int
        return f
    f + '.' + dec

exports.numFmt = numFmt = (n, int = false) ->
    if not n
        return 0
    # http://stackoverflow.com/questions/10809136
    round(n, int).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")

exports.Lock = React.create
    displayName: 'Lock'
    args:
        onFill: 'func?'
        locked: 'bool'

    render: ({children, locked, onFill}, S) ->
        if not locked
            return `<div>
                {children}
            </div>`

        mod = "green-linear regular big high"

        `<div className="lock-parent">
            <div className="lock" onTap={onFill}>
                <div className="lock__in">
                    <Btn mod={mod}>
                        Click to fill in
                    </Btn>
                </div>
            </div>
            {children}
        </div>`


exports.StatusBadge = StatusBadge = (object, size) ->
    return null unless object

    cls = 'status-badge '

    if object.isLive()
        `<span className={cls + 'red ' + size}>Live</span>`
    else if object.isPending()
        `<span className={cls + 'orange ' + size}>Soon</span>`
    else if object.isFinished()
        `<span className={cls + 'white ' + size}>Final</span>`

exports.RoundBadge = RoundBadge = (object) ->
    `<span className="round-badge">
        <span>Round</span>
        <b>{object.roundNumber}-{object.roundNumber + 1}</b>
    </span>`


exports.ProfileBadge = ProfileBadge = React.create
    displayName: 'ProfileBadge'

    render: ({children}) ->
        `<div {...this.props} className="profile-badge">
            <p className="profile-badge__text">{children}</p>
        </div>`


exports.Money = React.create
    displayName: 'Money'
    args:
        m: 'object'
        mod: 'string?'

    render: ({className, m, mod, symb}) ->
        className = className or ""
        if not m
            return `<span/>`

        `<span className={!mod ? 'money ' + className : 'money ' + mod + " " + className}>
            {m.usd ? <Dollars m={m} symb={symb}/> : null}
            {m.token ? <Tokens m={m} symb={symb}/> : null}
        </span>`
