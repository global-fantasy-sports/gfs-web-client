exports.getRoundTour = () ->
    tour = flux.stores.tournaments.getTournaments().find((t) -> t.isLive())
    tour or getNextTour()

exports.getNextTour = getNextTour = () ->
    tour = flux.stores.tournaments.getTournaments().find((t) -> t.isPending())
    tour or null
