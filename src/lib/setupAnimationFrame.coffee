# https://gist.github.com/paulirish/1579671
module.exports = (window) ->
    return if window.requestAnimationFrame
    for prefix in ['moz', 'webkit', 'ms', 'o']
        if window["#{prefix}RequestAnimationFrame"]
            window.requestAnimationFrame = window["#{prefix}RequestAnimationFrame"]
            window.cancelAnimationFrame = window["#{prefix}CancelAnimationFrame"]
            return

    lastTime = 0

    window.requestAnimationFrame = (cb) ->
        currTime = +new Date()
        timeToCall = Math.max(0, 16 - (currTime - lastTime))
        id = setTimeout((-> cb(currTime + timeToCall)), timeToCall)
        lastTime = currTime + timeToCall
        return id

    window.cancelAnimationFrame = (id) ->
        clearTimeout(id)

    return
