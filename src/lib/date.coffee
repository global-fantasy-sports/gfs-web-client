strftime = require('strftime')
moment = require('moment')


exports.fromUtcTs = (ts) ->
    if ts is null
        return null
    new Date(ts * 1000)

exports.fromUtcToLocal = (date) ->
    if moment.isMoment(date)
        moment(date).local()
    else
        moment(date)

exports.toEST = (date) ->
    if not moment.isMoment(date)
        date = moment(date)
    date.tz('America/New_York')

exports.toUtcTs = (date) ->
    parseInt((+date) / 1000, 10)

exports.weekStart = (date) ->
    d = new Date(date or now())
    day = d.getDay() || 7
    d.setDate(d.getDate() - day + 1)

exports.weekEnd = (date) ->
    d = new Date(date or now())
    day = d.getDay() || 7
    d.setDate(d.getDate() + (8 - day))

exports.now = now = ->
    flux.stores.time.getNow()

# Returns amount of full days between first and second date. So that if d2 is
# Thursday, and d1 is Friday, result is 1.
exports.daysBetween = (d1, d2) ->
    diff = (+d1) - (+d2)
    if diff > 0 then diff++ else diff--
    days = diff / (1000 * 60 * 60 * 24)
    return ~~days

exports.calculateDateInterval = (timespan) ->
    cd = 24 * 60 * 60 * 1000
    ch = 60 * 60 * 1000
    d = Math.floor(timespan / cd)
    h = Math.floor((timespan - d * cd) / ch)
    m = Math.round((timespan - d * cd - h * ch) / 60000)
    pad = (n) -> if n < 10 then '0' + n else n

    if m == 60
        h++
        m = 0

    if h == 24
        d++
        h = 0

    return {
        d: pad(d)
        h: pad(h)
        m: pad(m)
    }

exports.getStartOfWeek = (d) ->
    d = new Date(d)
    day = d.getDay()
    diff = d.getDate() - day + (if day is 0 then -6 else 1)
    d = new Date(d.setDate(diff))
    d.setHours(0, 0, 0, 0)

exports.dateRangeFormat = (from, to) ->
    return {
        time: strftime('%I:%M %p %Z %A', from)
        date: "#{strftime('%B %d', from)} - #{strftime('%d %Y', to)}"
    }
