class Signal
    constructor: ->
        @fns = []

    notify: (value) ->
        for fn in @fns
            fn(value)
        return # to prevent 'for' collecting in an array

    on: (fn) ->
        @fns.push(fn)

    off: (fn) ->
        if fn
            @fns.splice(@fns.indexOf(fn), 1)
        else
            @fns = []


class Cell extends Signal
    constructor: (@value) ->
        super

    notify: (value) ->
        for fn in @fns
            fn(value, @value)
        @value = value
        return

    set: (value) ->
        @notify(value)

    get: ->
        @value

    on: (fn) ->
        fn(@value)
        super


exports.signal = ->
    new Signal()


exports.cell = (value) ->
    new Cell(value)


exports.SignalMixin =
    onSignal: (signal, handler) ->
        @_watchingSignals or= []
        signal.on(handler)
        @_watchingSignals.push([signal, handler])

    componentWillUnmount: ->
        return unless @_watchingSignals
        for [signal, handler] in @_watchingSignals
            signal.off(handler)
        delete @_watchingSignals
