_ = require('underscore')
EventEmitter = require('eventemitter3')
config = require('config')

SockJS = require('../vendor/sockjs-0.3.4')
UUID = require('./UUID')

METHODMAP = {
    read: 'get'
    delete: 'delete'
    create: 'set'
    update: 'set'
}

SUBS = {}


class Connection extends EventEmitter

    constructor: (url, name) ->
        super()
        @url = url
        @name = name
        @callbacks = {}

    connect: (session, callback) =>
        @sockjs = new SockJS(@url + '/ws')

        @sockjs.onmessage = (event) =>
            _.defer(@handleMessage, event)

        @sockjs.onopen = =>
            @send('set', 'login', {session}, (error, value) =>
                @emit('connect', error, value)
                callback(error, value)
            )

        @sockjs.onclose = =>
            console.warn('connection lost: ', @name)
            @emit('disconnect')

    send: (action, channel, value, callback) =>
        id = UUID(true)
        console.log("<-[#{@name}]", id, channel + '/' + action, value)
        msg = {id, action, channel}
        if value
            msg.value = value
        @sockjs.send(JSON.stringify(msg))
        if callback
            @callbacks[id] = callback

    handleMessage: (e) =>
        {id, channel, error, value} = JSON.parse(e.data)
        if error
            console.warn("->[#{@name}]", id, channel, error)
            if error[0] == 'Invalid session.'
                {removeSession} = require('lib/utils')
                removeSession()
                window.location.reload()
                return
        else
            console.log("->[#{@name}]", id, channel, value)

        cb = @callbacks[id]
        if cb
            cb(error, value)
            delete @callbacks[id]
        else
            @handleBroadcast(id, channel, value)

    handleBroadcast: (id, channel, value) ->
        subs = SUBS[channel]
        return unless subs and subs.length
        for cb in subs
            setTimeout(cb.bind(this, value), 10)
        return

    subscribe: (channel, cb) ->
        SUBS[channel] or= []
        SUBS[channel].push(cb)

    unsubscribe: (channel, cb) ->
        subs = SUBS[channel]
        i = subs.indexOf(cb)
        subs.splice(i, 1)

conn = new Connection(window.api_url, SPORT)
connAuth = new Connection('', 'AUTH')
if config.debug
    window.conn = conn
    window.connAuth = connAuth
module.exports.conn = conn
module.exports.connAuth = connAuth
