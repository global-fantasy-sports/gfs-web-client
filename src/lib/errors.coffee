module.exports = {
get: (text) ->
    return this[text] or text

'No money in the bank.':
    'An unexpected error happened inside of transaction system.
     Please contact developers as soon as possible.'

'User has no tokens.':
    "Really sorry, but you've spent all of your tokens.
    We'll emit more tokens soon.
    Thanks for playing and if you have any feedback, please share!"

'Not enough tokens':
    "Really sorry, but you've spent all of your tokens.
    We'll emit more tokens soon.
    Thanks for playing and if you have any feedback, please share!"

'Not enough money':
    'We are sorry, but there is not enough money in your account to join selected games'

'Wrong schema of the "value" message.':
    'Oops. Something went wrong.'

500:
    'Unrecognized error. Our developers are already working on this error. Please try a little later.'

}
