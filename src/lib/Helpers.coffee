_ = require "underscore"
{strokeType} = require 'lib/const.coffee'


POINTS =
    'two-bogey': -3
    'bogey': -1
    'par': 0
    'birdie': 2
    'eagle': 5
    'albatross': 8

class Helpers
    statusName: (obj) ->
        if obj.isLive() then return "Live"
        if obj.isPending() then return "Soon"
        if obj.isFinished() then return "Final"
        return null

    calculateRowPoints: (strokes, pars, bonus) ->
        bonus or= []
        pars or= []
        strokes.map((stroke, i) =>
            if stroke
                diff = stroke - pars[i] - (bonus[i] || 0)
                POINTS[strokeType(diff)] ? -3
        )

module.exports.Helpers = new Helpers()
