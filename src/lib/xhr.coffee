{getCookie, parseTSV, parseJSON} = require('./utils')
config = require('../config')

# NOTE: progress is a callback, which will be passed information about progress
# it should be done better somehow, delayed to when it will be needed
module.exports = (options) -> new Promise((resolve, reject) ->
    {url, method, data, responseType, callback, progress} = options

    xhr = new XMLHttpRequest()
    xhr.open(method, url, true)

    xhr.onreadystatechange = ->
        if xhr.readyState == 4
            resp = xhr.responseText
            if responseType == 'json'
                resp = parseJSON(resp)
            else if responseType == 'tsv'
                resp = parseTSV(resp)
            if 200 <= xhr.status < 300
                if callback
                    callback(null, resp)
                resolve(resp)
            else
                if callback
                    callback(xhr.status, resp)
                reject(new Error((resp and resp.error) or xhr.status))

    if progress
        xhr.onprogress = (e) ->
            return unless e.lengthComputable
            progress({
                loaded: e.loaded
                total: e.total
                percentage: e.total and (e.loaded / e.total) * 100 or 0
            })

    XSRFToken = getCookie('_xsrf')
    if XSRFToken and method == 'post'
        xhr.setRequestHeader('X-Xsrftoken', XSRFToken)
    xhr.send(data)
)

shortcut = (method, url, data, options = {}) ->
    responseType = options.responseType ? 'json'
    params = {url, method, responseType}
    params.data = JSON.stringify(data) if data
    module.exports(params)


for method in ['get', 'post']
    module.exports[method] = shortcut.bind(null, method)
