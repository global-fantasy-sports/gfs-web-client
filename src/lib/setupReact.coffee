_ = require('underscore')
Immutable = require('immutable')
config = require('../config.js')


setupReact = (React) ->
    keyOf = require('fbjs/lib/keyOf')
    shouldRejectClick = require('react-tap-event-plugin/src/defaultClickRejectionStrategy')
    TapEventPlugin = require('react-tap-event-plugin/src/TapEventPlugin')(shouldRejectClick)

    TapEventPlugin.eventTypes.touchTap.phasedRegistrationNames = {
        bubbled: keyOf({onTap: null})
        captured: keyOf({onTouchTapCapture: null})
    }

    require('react/lib/EventPluginHub').injection.injectEventPluginsByName({
        TapEventPlugin
    })

    ReactDOM = require('react-dom')

    React.create = newCreate(React, React.createClass)
    React.findDOMNode = ReactDOM.findDOMNode
    React.render = ReactDOM.render

    return React


# better React.create

getPropType = (type) ->
    ptype = if type[type.length - 1] == '?'
        React.PropTypes[type.slice(0, type.length - 1)]
    else
        React.PropTypes[type].isRequired

    throw new Error("Type '#{type}' is not a valid PropType") unless ptype
    return ptype


newRender = (render) ->
    func = ->
        logger.log("<#{@constructor.displayName} />", "render component", "TrackComponents")
        render.call(this, @props, @state)

    func.name = 'newRender'
    return func


ShouldComponentUpdateMixin =
    {
        shouldComponentUpdate: (nextProps, nextState) ->
            for entity in [[nextProps, @props], [nextState, @state]]
                a = entity[0]
                b = entity[1]

                keys = _(a).keys().concat(_(b).keys())

                for key in keys
                    if not Immutable.is(a[key], b[key]) then return true
            false
    }


newCreate = (React, oldCreate) ->
    (spec) ->
        unless spec.shouldComponentUpdate
            spec.mixins or= []
            spec.mixins = [spec.mixins..., ShouldComponentUpdateMixin]

        if spec.render
            spec.render = newRender(spec.render)

        if spec.args
            spec.propTypes = {}
            for name, type of spec.args
                spec.propTypes[name] = getPropType(type)

        oldCreate.call(React, spec)


module.exports = setupReact
