moment = require('moment-timezone')


moment.tz.setDefault(moment.tz.guess())

serverNow = (offset) ->
    +new Date() - offset

localNow = () ->
    +new Date()

setServerDate = (unixTimestamp) ->
    if unixTimestamp
        moment.__serverDate = moment.unix(unixTimestamp)
        offset = +new Date() - moment.__serverDate
        moment.now = serverNow.bind(null, offset)
        return moment.__serverDate.clone()
    else
        delete moment.__serverDate
        moment.now = localNow
        return moment()


moment.now = localNow
moment.setServerDate = setServerDate


if process.env.NODE_ENV != 'production'
    window._moment = moment
