config = require '../config.js'
immutable = require 'immutable'
{now} = require('lib/date')


EMAIL_RE = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/


TYPES =
    '0': 'par'
    '-1': 'birdie'
    '-2': 'eagle'
    '-3': 'albatross' # unused
    '1': 'bogey'
    '2': 'two-bogey'
    '3': 'two-bogey'
    '4': 'two-bogey'
    '5': 'two-bogey'

exports.COMPARE_PLAYERS_MAX = 3

exports.strokeType = (diff) ->
    TYPES['' + Math.max(-3, Math.min(3, diff))]


exports.DAYS =
    'Sunday Monday Tuesday Wednesday Thursday Friday Saturday Sunday'.split(' ')

exports.roundDay = (n) ->
    return exports.DAYS[n + 3]

exports.MONTHS = ('January February March April May June July August ' +
    'September October November December').split(' ')

exports.COOKIE_RE = new RegExp("(?:^|; )#{config.cookiename}=([^;]+)")

exports.gameNameByType =
    'hole-by-hole': 'Hole By Hole'
    'handicap-the-pros': 'Handicap The Pros'
    'five-ball': 'Five Ball'
    'nfl': 'NFL'
    'nba': 'NBA'
    'football': 'Salary cap'
    'salarycap': 'Salary cap'
    'quickpick': 'Quick Pick'
    'bankroll': 'Bankroll manager'

exports.playButtonNameByType =
    'hole-by-hole': 'Hole By Hole'
    'handicap-the-pros': 'Handicap The Pros'
    'five-ball': '5 Ball'
    'football': 'Football'
    'basketball': 'Basketball'

exports.badgeByType =
    'hole-by-hole': 'Hole By Hole'
    'handicap-the-pros': 'Handicap'
    'five-ball': '5 Ball'
    'football': 'Football'
    'nfl': 'Football'
    'nba': 'Basketball'

exports.GAMES = [
    'hole-by-hole'
    'handicap-the-pros'
    'five-ball'
    'football'
    'basketball'
]

exports.STATE =
    START: 'start'
    WAITING: 'waiting'
    BLOCKED: 'blocked'
    DISABLED: 'disabled'
    CONNECTED: 'connected'
    DISCONNECTED: 'disconnected'
    PROCESSING: 'processing'

exports.DISABLED_MESSAGES =
    disabled:
        ''
    disconnected:
        'Ahoy matey! Looks like you have connection issues.
         Please check your internet link.'

exports.CONFIRM_EMAIL_MESSAGES =
    success: 'Your email was confirmed. Thank you.'
    fail: 'Bad confirmation link or email has already been confirmed'

exports.UNSUBSCRIBE_MESSAGES =
    success: 'You were unsubscribed successfully.'
    fail: 'Bad unsubscribe link'

exports.channels =
    DELETE_GAME_CHANNEL: 'game-delete'

    NOTIFICATIONS:
        get: 'user-notifications'
        set: 'user-notifications-read'
        push: 'user-notification'

    GAMES:
        'hole-by-hole':
            collection: 'hole-by-hole-games'
        'handicap-the-pros':
            collection: 'handicap-the-pros'
        'five-ball':
            collection: 'five-ball-games'

    ROOMS:
        get: 'stake-rooms'

    PARTICIPANTS:
        get: 'participant'
        collection:
            get: 'participants'

    STATISTICS:
        get: 'statistics'

    LINEUPS:
        get: 'get-lineups'
        set: 'set-lineup'
        remove: 'del-lineup'
        
    FAVORITES:
        get: 'get-favorites'
        set: 'set-favorites'
        remove: 'del-favorites'

exports.RoomsComparators =
    game: (room) ->
        -((room.pot * 1000) +
            (room.gamesCount * 10))

    entries: (room) ->
        n = -(room.gamesCount / room.maxEntries)
        dt = now() / room.startDate
        return n + dt

    entry: (room) ->
        -room.entryFee

    prizes: (room) ->
        -room.pot

    starts: (room) ->
        n = -(room.gamesCount / room.maxEntries)

        +room.startDate * 100 + n
