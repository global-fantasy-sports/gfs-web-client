_ = require 'underscore'
{Motion, spring} = require 'react-motion'


translateX = (x)-> {
    transform: "translate3d(-#{x}%, 0, 0)"
    WebkitTransform: "translate3d(-#{x}%, 0, 0)"
}


module.exports = class SwipeableViews extends React.Component

    @propTypes: {
        children: React.PropTypes.node.isRequired
        index: React.PropTypes.number
        peek: React.PropTypes.bool
        peekAmount: React.PropTypes.number
        onChangeIndex: React.PropTypes.func
    }

    @defaultProps: {
        index: 0
        peek: false
        peekAmount: 40
        onChangeIndex: _.noop
    }

    constructor: (props)->
        super(props)

        @state = {
            index: props.index
            isDragging: false
            isFirstRender: true
        }

    componentDidMount: ->
        @setState({isFirstRender: false})

    componentWillReceiveProps: ({index})->
        if typeof index is 'number' and index isnt @props.index
            @setState({index})

    handleTouchStart: (event)=>
        {pageX, pageY} = event.touches[0]

        @startWidth = React.findDOMNode(this).getBoundingClientRect().width
        @startIndex = @state.index
        @startX = pageX
        @lastX = pageX
        @deltaX = 0
        @startY = pageY
        @isScroll = undefined

    handleTouchMove: (event)=>
        {pageX, pageY} = event.touches[0]
        # This is a one time test
        if @isScroll == undefined
            @isScroll = Math.abs(@startY - (pageY)) > Math.abs(@startX - (pageX))

        return if @isScroll

        # Prevent native scrolling
        event.preventDefault()
        @deltaX = pageX - (@lastX)
        @lastX = pageX
        indexMax = React.Children.count(@props.children) - 1
        index = @startIndex + (@startX - (pageX)) / @startWidth

        if index < 0
            index = 0
            @startX = pageX
        else if index > indexMax
            index = indexMax
            @startX = pageX

        @setState({isDragging: true, index})

    handleTouchEnd: =>
        return if @isScroll

        indexNew = undefined
        # Quick movement
        if Math.abs(@deltaX) > 10
            if @deltaX > 0
                indexNew = Math.floor(@state.index)
            else
                indexNew = Math.ceil(@state.index)
        else
        # Some hysteresis with startIndex
            if Math.abs(@startIndex - (@state.index)) > 0.6
                indexNew = Math.round(@state.index)
            else
                indexNew = @startIndex
        @setState({
            index: indexNew
            isDragging: false
        })

        if indexNew != @startIndex
            @props.onChangeIndex(indexNew)

    renderContainer: ({x})=>
        {children, style} = @props
        {isFirstRender} = @state

        if isFirstRender
            childrenToRender = children[0]
        else
            childrenToRender = React.Children.map(children, (element)->
                `<div className="swipeable-views__slide">
                    {element}
                </div>`
            )

        `<div className="swipeable-views__container" style={translateX(x)}>
            {childrenToRender}
        </div>`

    render: ->
        {index, isDragging} = @state
        {peek, peekAmount} = @props

        springPosition = index * 100
        if peek and not isDragging
            springPosition += peekAmount

        springConfig = if isDragging then [3000, 50] else if peek then [45, 10] else [300, 50]

        `<div className="swipeable-views"
              onTouchStart={this.handleTouchStart}
              onTouchMove={this.handleTouchMove}
              onTouchEnd={this.handleTouchEnd}>
            <Motion style={{x: spring(springPosition, springConfig)}}>
                {this.renderContainer}
            </Motion>
        </div>`
