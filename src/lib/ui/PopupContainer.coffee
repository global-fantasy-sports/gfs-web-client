_ = require('underscore')
{popupCell} = require('../../world')


module.exports = React.create
    displayName: "PopupContainer"

    mixins: [flux.Connect("dialogs", (store) ->
        dialog: store.getDialog()
        component: store.getComponent()
        dialogClassname: store.getDialogClassname()
    )]

    componentDidMount: ->
        @update = => @forceUpdate()
        popupCell.on(@update)

    componentWillUnmount: ->
        popupCell.off(@update)

    render: ({}, {dialog, component, dialogClassname}) ->
        dialogClassname = dialogClassname ? "helping-text-wrapper"
        if _.isFunction(component)
            component = component()

        dialog = popupCell.get()

        ((dialog || component) && `<section className={dialogClassname}>
            <div className="helping-text-wrapper__splash"></div>
            <div id="PopupContainer" className="popup-container">
                {dialog || component}
            </div>
        </section>`) or null
