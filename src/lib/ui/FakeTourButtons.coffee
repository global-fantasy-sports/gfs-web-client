Checkbox = require('./Checkbox')
{Btn} = require('../ui')
{cx, stopPropagation} = require('../utils')

addInterval = (hours) ->
    return () ->
        flux.actions.time.addInterval(hours)

module.exports = React.create(
    displayName: 'FakeTourButtons'

    mixins: [
        flux.Connect('time', (store) -> {
            enabled: store.isFake()
            resetLocked: store.isLocked()
        })
    ]

    handleRestart: () ->
        unless @state.resetLocked
            flux.actions.time.restartFakeTour()

    render: (P, {enabled, resetLocked}) ->
        return null unless enabled

        buttonCls = cx('pageslide-slide__restart-block', {disabled: resetLocked})

        `<div onTap={stopPropagation}>
            <div className={buttonCls} onTap={this.handleRestart}>
                <img src="/static/img/lobby/car_front.png" />
                <span>Great Scott!</span>
                <img src="/static/img/lobby/car_back.png" />
            </div>

            <div className="pageslide-slide-current-switcher">
                <span>Simulation environment:</span>
                <Checkbox checked={resetLocked} />
            </div>

            <div className="aside-menu__fake-tour-btn">
                <Btn mod="fake-tour-wide" onTap={flux.actions.time.setAtKickOff}>Kick-off</Btn>&nbsp;
                <Btn mod="fake-tour-wide" onTap={flux.actions.time.setAtMidGame}>Mid</Btn>&nbsp;
                <Btn mod="fake-tour-wide" onTap={flux.actions.time.setAtResults}>Results</Btn>&nbsp;
            </div>

            <div className="aside-menu__fake-tour-btn">
                <Btn mod="fake-tour" onTap={addInterval(1)}>+1H</Btn>&nbsp;
                <Btn mod="fake-tour" onTap={addInterval(6)}>+6H</Btn>&nbsp;
                <Btn mod="fake-tour" onTap={addInterval(24)}>+1D</Btn>&nbsp;
                <Btn mod="fake-tour" onTap={addInterval(24 * 7)}>+1W</Btn>
            </div>
        </div>`
)
