GuideHint = require "../../components/guide/GuideHint"
HintTrigger = require "../../components/guide/HintTrigger"
{Btn} = require "../ui"
{TourDates} = require '../../components/tournament/TourDates'
{TourNames} = require '../../components/tournament/TourName'
{cx} = require '../utils'

module.exports = React.create
    displayName: 'TournamentSwitchItem'

    render: ({tournament, tour, onTap, active, single}) ->
        if @props.single
            btn = `<Btn mod="regular small select inherit"
                        icon="ball-selected"
                        btnText={true}>
                Selected
            </Btn>`
        else
            btn = `<Btn mod="regular small select inherit"
                        icon={this.props.active ? 'ball-selected' : 'ball-select-empty'}>
                {this.props.active ? 'Select' : 'Select'}
            </Btn>`

        classes = cx(
            "tour-switch__el": true
            "active": @props.active
        )

        `<HintTrigger group="wizard" id="tour">
            <div key={this.props.tour.id} className={classes} onTap={this.props.onTap}>
                <div className="tour-switch__info">
                    <TourNames tournament={this.props.tour} mod="inherit bold"/>
                    <TourDates tournament={this.props.tour} mod="inherit bold"/>
                </div>
                <div className="tour-switch__btn">
                    {btn}
                </div>
            </div>
        </HintTrigger>`
