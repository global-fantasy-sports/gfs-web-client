exports.Flag = React.create
    displayName: 'Flag'


    render: ({country, mod}) ->
        if not country
            return `<i/>`

        cls = 'flag ' + country.toLowerCase()

        `<span className="flag-wrap"><span {...this.props} className={cls}></span></span>`
