{ActionBar, Btn, BtnLink} = require 'lib/ui'


module.exports = class WizardActionBar extends React.Component

    @propTypes: {
        nextLabel: React.PropTypes.string
        isDone: React.PropTypes.bool.isRequired
        onNext: React.PropTypes.func.isRequired
    }

    @defaultProps: {
        nextLabel: 'Next'
    }

    render: ->
        {isDone, onNext, nextLabel} = @props

        `<ActionBar>
            <BtnLink mod="red wide" to={"/" + SPORT + "/"}>
                Exit
            </BtnLink>
            <Btn mod={isDone ? 'green blink' : ' not-active'}
                 onTap={onNext}>
                {nextLabel}
            </Btn>
        </ActionBar>`
