PureRenderMixin = require('react-addons-pure-render-mixin')
{bem, cx, prevent} = require('../utils')


class Close extends React.Component

    @defaultProps: {
        mod: 'light'
        label: '✕'
    }

    @mod: bem('ui-close')

    shouldComponentUpdate: PureRenderMixin.shouldComponentUpdate

    handleTap: (e) =>
        e.preventDefault()
        {onClose} = @props
        onClose() if onClose

    render: () ->
        `<a
            href="#"
            className={cx(Close.mod(this.props.mod), this.props.className)}
            onClick={prevent}
            onTap={this.handleTap}
        >{this.props.label}</a>`


module.exports = Close
