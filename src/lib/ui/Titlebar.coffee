{cx} = require '../utils'


exports.Titlebar = React.create
    displayName: 'Titlebar'

    render: ({title, children, mod, className}) ->
        cls = cx('title', className)

        if not mod?
            `<h1 {...this.props} className={cls}>
                <span className="title__small">{title}&nbsp;</span>
                <span className="title__big">{children}</span>
            </h1>`
        else if mod == "h2"
            `<h2 {...this.props} className={cls}>
                <span className="title__small">{title}&nbsp;</span>
                <span className="title__big">{children}</span>
            </h2>`
        else if mod == "h3"
            `<h3 {...this.props} className={cls}>
                <span className="title__small">{title}&nbsp;</span>
                <span className="title__big">{children}</span>
            </h3>`
        else if mod == "h4"
            `<h4 {...this.props} className={cls}>
                <span className="title__small">{title}&nbsp;</span>
                <span className="title__big">{children}</span>
            </h4>`
        else if mod == "h5"
            `<h5 {...this.props} className={cls}>
                <span className="title__small">{title}&nbsp;</span>
                <span className="title__big">{children}</span>
            </h5>`
        else if mod == "h6"
            `<h6 {...this.props} className={cls}>
                <span className="title__small">{title}&nbsp;</span>
                <span className="title__big">{children}</span>
            </h6>`
