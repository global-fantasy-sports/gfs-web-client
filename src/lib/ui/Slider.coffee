Slider = require('rc-slider')


class CustomSlider extends React.Component

    @propTypes: {
        min: React.PropTypes.number.isRequired
        max: React.PropTypes.number.isRequired
        onChange: React.PropTypes.func.isRequired
    }

    render: () ->
        {min, max, value, defaultValue, label} = @props

        [valueStart, valueEnd] = (value or defaultValue or [min, max])

        `<div className="customized-slider">
            {label && <div className="customized-slider__label">{label}</div>}
            <div className="customized-slider__value">
                <span className="customized-slider__num">{valueStart}</span>
                <span className="customized-slider__num">{valueEnd}</span>
            </div>
            <Slider
                range={true}
                allowCross={false}
                min={min}
                max={max}
                onChange={this.props.onChange}
                defaultValue={defaultValue}
                className="customized-slider__rc"
            />
        </div>`


module.exports = CustomSlider
