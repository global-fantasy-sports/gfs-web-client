module.exports = React.create
    displayName: 'Logo'

    args:
        logo: 'string?'
        alt: 'string?'
        onTap: 'func?'
        mod: 'string?'

    render: ({logo, mod, onTap, alt}) ->
        cls = 'logo '
        `<span className={mod ? cls + mod : cls} onTap={onTap}>
            {logo ?
            <img className="logo__el"
                 src={"/static/logos/" + logo.replace(' ', '-').toLowerCase() + '.png'}
                 alt={alt} />
            : null}
         </span>`
