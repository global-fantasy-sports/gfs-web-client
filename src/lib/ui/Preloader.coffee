{cx} = require('lib/utils')


module.exports = class Preloader extends React.Component

    @propTypes: {
        mod: React.PropTypes.string
    }

    @defaultProps: {
        mod: 'default'
    }

    render: ->
        mod = @props.mod.split(' ')
        fileName = if 'dotted' in mod then 'preloader.svg' else 'rolling.svg'
        className = cx('preloader', mod.map((variant)-> "preloader--#{variant}"))
        `<div className={className}>
            <img className="preloader__image" src={"/static/img/" + fileName} />
        </div>`
