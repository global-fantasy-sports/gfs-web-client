{Icon} = require('../ui')
{cx} = require('../utils')

module.exports = React.create(
    displayName: 'RoundSwitchItem'

    handleTap: () ->
        @props.onTap(@props.round)

    render: ({active, disabled, round}) ->
        className = cx('round-switch__el', {
            "round-switch__el--active": active
            "round-switch__el--inactive": disabled
        })

        `<div className={className} onTap={this.handleTap}>
            <div className="round-switch__content">
                <span className="round-switch__text">Round&nbsp;</span>
                <span className="round-switch__round">{round} &amp; {round + 1}</span>
            </div>
            <div className="round-switch__icon">
                <Icon i={active ? 'radio-checked' : 'radio'} />
            </div>
        </div>`
)
