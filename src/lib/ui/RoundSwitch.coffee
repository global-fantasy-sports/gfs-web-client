_ = require('underscore')
{prevent} = require('../utils')
RoundSwitchItem = require('./RoundSwitchItem')

exports.RoundSwitch = React.create(
    displayName: 'RoundSwitch'

    handleSelect: (roundNumber) ->
        flux.actions.wizard.setEvent(@props.selectedTournamentId, roundNumber)

    render: ({selectedRound, selectedTournamentId}) ->
        disabled = not selectedTournamentId

        els = [1..3].map((roundNumber) =>
            isRoundAvailable = flux.stores.state.isRoundAvailable(roundNumber)

            onTap = @handleSelect

            `<RoundSwitchItem
                key={roundNumber}
                onTap={onTap}
                round={roundNumber}
                active={roundNumber === selectedRound}
                disabled={!isRoundAvailable || !selectedTournamentId}
            />`
        )


        `<div className="round-switch">
            <div className="round-switch__head">
                <span className="round-switch__title">Rounds</span>
                <a href="#" className="round-switch__rules" onClick={prevent}>Rules</a>
            </div>
            <div className="round-switch__els">
                {els}
            </div>
        </div>`
)
