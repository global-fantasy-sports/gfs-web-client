class PlaceholderImage extends React.Component

    @propTypes: {
        placeholder: React.PropTypes.string.isRequired
        src: React.PropTypes.string.isRequired
    }

    constructor: (props)->
        super(props)
        @imageInstance = new Image()
        @state = {
            displayedImage: props.placeholder
        }

    componentDidMount: () ->
        @setDisplayedImage(@props.src)

    componentWillReceiveProps: ({src}) ->
        @setDisplayedImage(src) unless src is @props.src

    componentWillUnmount: () ->
        @imageInstance.onerror = null
        @imageInstance.onload = null

    setDisplayedImage: (src) ->
        @imageInstance.onerror = () =>
            @setState({displayedImage: @props.placeholder})

        @imageInstance.onload = () =>
            @setState({displayedImage: src})

        @imageInstance.src = src

    render: () ->
        {alt, name} = @props
        {displayedImage} = @state

        if name
            `<div className="placeholder-img">
                <img {...this.props} src={displayedImage}/>
                <p className="placeholder-img__name">{name}</p>
            </div>`
        else
            `<img {...this.props} src={displayedImage}/>`


module.exports = PlaceholderImage
