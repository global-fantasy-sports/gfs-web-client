{popupCell} = require('../../world')


module.exports = React.create
    displayName: 'Popup'

    componentDidMount: ->
        popupCell.set(@props.children)

    componentWillUnmount: ->
        popupCell.set(null)

    render: ->
        return null
 
