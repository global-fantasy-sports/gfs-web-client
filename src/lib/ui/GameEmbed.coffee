{Btn} = require('../ui')


module.exports = React.create
    displayName: 'GameEmbed'
    args:
        children: 'node'
        mod: 'string?'
        noClose: 'bool?'
        onClose: 'func?'

    render: ({children, mod, noClose, onClose}, S) ->
        cls = 'popup-wrapper dark-embed ' + (mod or '')

        `<section {...this.props} className={cls}>
            <div className="modal game-embed">
                <div className="modal-dialog game-embed__dialog">
                    <div className="modal-content game-embed__content">
                        <div className="modal-body game-embed__body">
                            {children}
                        </div>
                        {!noClose &&
                            <div className="game-embed__close">
                                <Btn mod="blue-linear" onTap={onClose}>Ok</Btn>
                            </div>
                        }
                    </div>
                </div>
            </div>
        </section>`
