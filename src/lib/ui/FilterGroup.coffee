_ = require('underscore')
{Btn} = require('../ui')


FilterButton = ({value, label, onClick, active}) ->
    mod = if active then 'blue-base-active' else 'blue-base'
    handleClick = () -> onClick(value)
    `<Btn mod={mod} onClick={handleClick}>
        {label || value}
    </Btn>`




class FilterGroup extends React.Component

    @propTypes: {
        values: React.PropTypes.array.isRequired
        activeIndex: React.PropTypes.number.isRequired
        onChange: React.PropTypes.func.isRequired
    }

    render: () ->
        {values, activeIndex, onChange} = @props

        filters = values.map((item, index) =>
            `<FilterButton
                key={index}
                label={item}
                value={index}
                active={activeIndex === index}
                onClick={onChange}
            />`
        )

        otherProps = _.omit(@props, 'values', 'activeIndex', 'onChange')

        `<span {...otherProps}>{filters}</span>`


module.exports = FilterGroup
