Stickyfill = require('exports?window.Stickyfill!vendor/stickyfill')


class StickyBlock extends React.Component

    @propTypes: {
        children: React.PropTypes.node.isRequired
    }

    componentDidMount: () ->
        Stickyfill.add(@refs.root)

    componentWillUnmount: () ->
        Stickyfill.remove(@refs.root)

    render: () ->
        {children} = @props

        `<div {...this.props} data-sticky key="root" ref="root">
            {children}
        </div>`



module.exports = StickyBlock
