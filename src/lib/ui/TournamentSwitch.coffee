_ = require('underscore')
{Btn, Icon} = require('../ui')
{TourName} = require('../../components/tournament/TourName')
TourInfo = require('../../components/tournament/TourInfo')
{TourCourseRating} = require('../../components/tournament/TourCourseRating')
{cx} = require('../utils')

exports.TournamentSwitch = React.create(
    displayName: 'TournamentSwitch'

    componentDidMount: () ->
        {tournaments, selected} = @props
        if tournaments.size and not selected
            _.defer(() => @onTap(tournaments.getIn([0, 'id'])))

    onTap: (tournamentId) ->
        flux.actions.wizard.setTournamentId(tournamentId)

    renderTour: (tour) ->
        tourId = tour.id
        active = tourId is @props.selected

        tourClass = cx(
            "tour-switch__el": true
            "tour-switch__el--active": active
            "tour-switch__el--one": @props.tournaments.size == 1
        )

        `<div className={tourClass} onTap={this.onTap.bind(this, tourId)} key={tourId}>
            <div className="tour-switch__info">
                <TourName tournament={tour} active={active ? true : ""}/>
                <TourInfo tournament={tour} mod={active ? 'tour-info--active' : ""}/>
            </div>
            <div className="tour-switch__icon">
                <Icon i={active ? 'radio-checked' : 'radio'} />
            </div>
        </div>`

    render: ({tournaments}) ->
        els = tournaments.toSeq().map(@renderTour)
        `<section className="tour-switch">
            <div className="tour-switch__head">
                Tournaments
            </div>
            <div className="tour-switch__els">
                {els}
            </div>
        </section>`
)
