Connect = require('../../flux/connectMixin')


if SPORT == 'nfl'
    CurrentWeekInfo = React.create(
        displayName: 'CurrentWeekInfo'

        mixins: [
            Connect('time', (store) ->
                {week} = store.getState()
                return {
                    week: week[0]
                    year: week[1]
                }
            )
        ]

        render: ->
            {week, year} = @state
            `<div className="pageslide-slide-current-date">
                <span className="stroke small">
                    <span>Week: {week}</span>&nbsp;
                    <span>Year: {year}</span>
                </span>
            </div>`
    )
else
    CurrentWeekInfo = React.create(
        render: ->
            null
    )


module.exports = CurrentWeekInfo
