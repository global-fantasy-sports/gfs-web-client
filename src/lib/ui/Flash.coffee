GameEmbed = require('../../lib/ui/GameEmbed')
Popup = require('../../lib/ui/Popup')
{flashCell} = require('../../world')


module.exports = React.create(
    displayName: 'Flash'

    componentDidMount: ->
        @update = @forceUpdate.bind(this, null)
        flashCell.on(@update)

    componentWillUnmount: () ->
        flashCell.off(@update)

    close: (message) ->
        messages = flashCell.get()
        messages.splice(messages.indexOf(message), 1)
        flashCell.set(messages)

    render: (P, S) ->
        messages = flashCell.get() or []
        flashMessages = messages.map((message, index) ->
            close = @close.bind(this, message)
            `<Popup key={index}>
                <GameEmbed mod={'flash-popup ' + message.mod} onTap={close}>
                    <p className="flash-popup__msg">{message.text}</p>
                </GameEmbed>
            </Popup>`
        )

        `<div className='flash'>
            {flashMessages}
         </div>`
)
