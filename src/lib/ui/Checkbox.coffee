_ = require('underscore')
bemClassName = require('bem-classname')


bem = bemClassName.bind(null, 'ui-checkbox-circle')


module.exports = React.create(
    displayName: 'Checkbox'

    handleChange: (e) ->
        e.preventDefault()
        flux.actions.time.toggleResetLock()

    render: ({checked}) ->
        `<div className={bem({checked: checked})}>
            <label className={bem('label', {checked: checked})}>
                <div className={bem('block', {checked: checked})}>
                    <span className={bem('text', {checked: checked})}>
                        {checked ? 'UNLOCK' : 'LOCK'}
                    </span>
                    <div className={bem('circle', {checked: checked})} />
                </div>
                <input
                    checked={checked}
                    type="checkbox"
                    className={bem('el', {checked: checked})}
                    onChange={this.handleChange}
                />
            </label>
        </div>`
)
