_ = require('underscore')
{cx} = require('lib/utils')
{InputMessage} = require('lib/forms')


exports.SelectBox = SelectBox = React.create
    displayName: "SelectBox"

    mixins: [flux.Connect("forms", (store, props) ->
        if props.validationId
            inProgress: store.isInProgress(props.validationId)
            isSuccess: store.isSuccess(props.validationId)
            isError: store.isError(props.validationId)
            error: store.getError(props.validationId)
            value: store.getValue(props.validationId)
        else
            {}
    )]

    getDefaultProps: -> {
        mod: ''
        options: []
    }

    getInitialState: ->
        open: false
        id: @props.id or @props.name + _.uniqueId()

    changed: (ev) ->
        @props.onChange ev.target.value

    formValueChanged: (e) ->
        flux.actions.forms.setValue(@props.validationId, e.target.value)
        if @props.onValidate
            flux.actions.forms.startProgress(@props.validationId)

            error = @props.onValidate(@state.value)
            if error
                flux.actions.forms.setError(@props.validationId)
            else
                flux.actions.forms.setSuccess(@props.validationId)

    render: ({options, selected, disabled, hint, name, valueLink, mod, validationId, label}, {id}) ->
        if mod
            mod = mod.split(' ').map((x) -> "ui-select_#{x}").join(' ')

        cls = cx('ui-select', mod)

        opts = options.map(({value, name})->
            `<option value={value}>{name}</option>`
        )

        if valueLink
            `<div className={cls}>
                {label &&
                <label className="ui-input__label"
                       htmlFor={name}>
                    {label}
                </label>}
                <select disabled={disabled}
                        className="ui-select__el"
                        valueLink={valueLink}
                        name={name}>
                    {opts}
                </select>
                {hint ? <div className="ui-select__hint">{hint}</div> : null}
            </div>`
        else if validationId
            {error, isSuccess} = @state
            cls = cx(cls,
                "error-value": Boolean(error)
                "success-value": isSuccess
            )

            `<div className={cls}>
                {label &&
                <label className="ui-input__label"
                       htmlFor={name}>
                    {label}
                </label>}
                <select disabled={disabled}
                    className="ui-select__el"
                    onChange={this.formValueChanged}
                    value={this.state.value}>
                    {opts}
                </select>
                {error && <InputMessage message={error} />}
                {hint ? <div className="ui-select__hint">{hint}</div> : null}
            </div>`
        else
            {error, value, isValid} = @props
            cls = cx(cls,
                "error-value": Boolean(error)
                "success-value": isValid
            )

            `<div className={cls}>
                {label &&
                <label className="ui-input__label"
                       htmlFor={name}>
                    {label}
                </label>}
                <select disabled={disabled}
                        className="ui-select__el"
                        value={value}
                        name={name}
                        onChange={this.changed}>
                    {opts}
                </select>
                {error && <InputMessage message={error} />}
                {hint ? <div className="ui-select__hint">{hint}</div> : null}
            </div>`
