Ease =
    inOutQuad: (t, b, c, d) ->
        t /= d / 2
        return c / 2 * t * t + b if t < 1
        t--
        return -c / 2 * (t * (t - 2) - 1) + b

    inCubic: (t, b, c, d) ->
        tc = (t /= d) * t * t
        return b + c * (tc)

    inOutQuintic: (t, b, c, d) ->
        ts = (t /= d) * t
        tc = ts * t
        b + c * (6 * tc * ts + -15 * ts * ts + 10 * tc)


exports.scroll = scroll = (scrollParent, to, duration = 600) ->
    start = scrollParent.scrollTop
    startTime = +new Date()

    doAnimateScroll = () ->
        currentTime = +new Date() - startTime
        # find the value using easing function
        val = Ease.inOutQuad(currentTime, start, to - start, duration)
        # scroll the window
        scrollParent.scrollTop = val
        # do the animation unless its over
        if currentTime < duration
            requestAnimationFrame(doAnimateScroll)

    doAnimateScroll()


exports.getOffsetTop = getOffsetTop = (element) ->
    offset = 0
    while element
        offset += element.offsetTop
        element = element.offsetParent
    offset


exports.scrollToTop = (element, options = {}) ->
    {duration, offset, scrollParent} = options
    scrollParent or= document.body

    to = if element then getOffsetTop(element) else 0
    to += offset if offset

    if duration is 0
        scrollParent.scrollTop = to
    else
        scroll(scrollParent, to, duration)
