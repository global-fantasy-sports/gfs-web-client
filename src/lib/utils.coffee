_ = require('underscore')
md5 = require('blueimp-md5')
moment = require('moment')
{countrylist} = require('../vendor/countries.js')
{COOKIE_RE} = require('./const')
{localStorage} = require('../env/storage')
config = require('../config')


exports.splitQS = (qs) ->
    res = {}
    bits = qs.split('&')
    for bit in bits
        if bit.length
            i = bit.indexOf('=')
            res[bit.slice(0, i)] = decodeURIComponent(bit.slice(i + 1))
    return res

filterClassNames = (specs) ->
    filterFunc = (specs) ->
        for spec in specs when spec
            if typeof spec is 'string' then spec
            else if Array.isArray(spec) then filterFunc(spec)
            else key for key, value of spec when Boolean(value)

    _.flatten(filterFunc(specs))

exports.cx = (specs...) ->
    filterClassNames(specs).join(' ')

exports.bem = (el) ->
    (specs...) ->
        return el unless specs.length
        mods = filterClassNames(specs).map((mod) -> "#{el}--#{mod}")
        [el, mods...].join(' ')

exports.remove = (arr, i) ->
    arr.splice(i, 1)
    arr

exports.arrayToggle = (array, value) ->
    idx = array.indexOf(value)
    if ~idx
        array.splice(idx, 1)
    else
        array.push(value)
    return array

exports.choice = (arr) ->
    arr[Math.floor(Math.random() * arr.length)]

exports.randomNumber = (from, to) ->
    Math.floor(Math.random() * (to - from)) + from

exports.sum = (list) ->
    _.reduce(list, ((sum, n) -> sum + n), 0)

exports.fullOffset = (node) ->
    top = 0
    left = 0
    while node
        top += node.offsetTop
        left += node.offsetLeft
        node = node.offsetParent

    return {top, left}

# FIXME we only need 3-char codes, make them keys in the dictionary
exports.countryByCode = (code) ->
    country = _.find(countrylist, (c) -> c['alpha-3'] == code)

    if country then country.name else code

exports.isoCode = (longcode) ->
    if longcode.length == 2
        return longcode

    country = _.find(countrylist, (c) -> c['alpha-3'] == longcode)
    if country then country['alpha-2'] else longcode

# NB: uses localStorage right now, should use server in the future
# to be synchronized between devices
exports.settings = (key, value) ->
    getRoot = ->
        JSON.parse(localStorage.getItem("globalfantasysportsgolf")) || {}
    get = (k) ->
        root = getRoot()
        root[k] ? null
    set = (k, v) ->
        root = getRoot()
        root[k] = v
        localStorage.setItem("globalfantasysportsgolf", JSON.stringify(root))

    switch arguments.length
        when 2
            set(key, value)
        when 1
            get(key)
        else
            getRoot()

exports.capitalize = (s) ->
    s[0].toUpperCase() + s.slice(1)

exports.prevent = (e) ->
    e.preventDefault() if e

exports.stopPropagation = (e) ->
    e.stopPropagation() if e

lastNumber = (n) ->
    Math.floor(n / (Math.pow(10, 0)) % 10)

exports.numberEnding = (n) ->
    if n <= 0
        return n
    ending = switch lastNumber(n)
        when 1
            "st"
        when 2
            "nd"
        when 3
            "rd"
        when 4, 5, 6, 7, 8, 9, 0
            "th"

    return `<sup>{ending}</sup>`

exports.parseDec = (n) ->
    parseInt(n, 10)

exports.gravatar = (email) ->
    "//gravatar.com/avatar/#{md5(email)}.jpg?s=50"

exports.pad = (n) ->
    if 0 <= n < 10 then '0' + n else n

exports.dateFmt = (from) ->
    return "" unless +from

    dates = Array.from(arguments).map((date) ->
        moment(date).format('MMM D, YYYY')
    )

    dates.join('-')

exports.timeFmt = (date) ->
    moment(date).format('h:mm A')

exports.dateTimeFmt = (date) ->
    return "" unless +date
    moment(date).format('MMM D, YYYY h:mm A')

exports.clip = (n, min, max) ->
    return min if n < min
    return max if n > max
    return n

exports.getSession = ->
    match = document.cookie.match(COOKIE_RE)
    return unless match
    match[1]

exports.removeSession = ->
    date = new Date(0).toGMTString()
    if window.location.hostname.endsWith('howfargames.com')
        domain = 'howfargames.com'
    else if window.location.hostname.endsWith('globalfantasysports.com')
        domain = 'globalfantasysports.com'
    else
        domain = '127.0.0.1'
    document.cookie = "#{config.cookiename}=; path=/; domain=#{domain}; expires=#{date}"
    console.log('session cookie removed')

exports.getPosition = (target, fixed) ->
    if not target
        return 0
    rect = target.getBoundingClientRect()
    winHeight = window.innerHeight
    widWidth = window.innerWidth

    rectHeight = rect.height
    rectWidth = rect.width / 2

    targetParentWidth = target.parentNode.getBoundingClientRect().width

    top = (winHeight - rectHeight) + "px"
    centered =
        if fixed
            (widWidth * 0.5 - rectWidth) + "px"
        else
            (targetParentWidth * 0.5 - rectWidth) + "px"

    return {top: top, centered: centered}

exports.getCookie = (name) ->
    matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([.$?*|{}()[]\/+^])/g, '\\$1') + "=([^;]*)"))
    if matches
        decodeURIComponent(matches[1])
    else
        null

if typeof _ga is 'undefined'
    _ga = _.noop

exports.eventTracker = (eventCategory, eventAction) ->
    _ga('send', 'event', eventCategory, eventAction)

exports.pageTracker = (page, location) ->
    _ga('send', 'pageview', page, location)

exports.countTracker = (fieldName, fieldValue) ->
    _ga('set', fieldName, fieldValue)

exports.parseMoney = (x) ->
    if Array.isArray(x)
        [token, usd] = Array.from(x)
        return {token, usd}
    if immutable.Map.isMap(x)
        return x.toJS()
    return x

parseValue = (value) ->
    try
        return JSON.parse(value)
    catch e
        return value

exports.parseTSV = (data) ->
    return [] unless data

    data.split('\n')
        .filter((row) -> Boolean(row))
        .map((row) -> row.split('\t').map(parseValue))

exports.parseJSON = (data) ->
    try
        JSON.parse(data)
    catch e
        null
