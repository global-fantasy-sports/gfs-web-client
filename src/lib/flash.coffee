{flashCell} = require('../world')

flash = ({type, text, alwaysKeep, mod, timeout}) ->
    push = ->
        state = flashCell.get() or []
        state.push(
            type: type or 'notification'
            text: text
            mod: mod or ''
            alwaysKeep: alwaysKeep
        )
        flashCell.set(state)
    pop = ->
        state = flashCell.get() or []
        state.pop()
        flashCell.set(state)

    push()
    if not timeout
        timeout = 8000
    setTimeout((-> pop()), timeout)

module.exports = flash
