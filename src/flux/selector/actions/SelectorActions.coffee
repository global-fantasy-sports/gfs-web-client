{Actions} = require('minimal-flux')

class SelectorActions extends Actions

    setCurrentSport: (sport) ->
        if @stores.pageData.getCurrentSport() isnt sport
            @dispatch('setCurrentSport', sport)
            @actions.connection.loadFeaturedRooms(sport)


module.exports = SelectorActions
