{Flux} = require('minimal-flux')


module.exports = new Flux(
    actions: {
        connection: require('../common/actions/ConnectionActions')
        data: require('../common/actions/DataActions')
        facebook: require('../common/actions/FacebookActions')
        forms: require('../common/actions/FormActions')
        news: require('../common/actions/NewsActions')
        notifications: require('../common/actions/NotificationsActions')
        payment: require('../common/actions/PaymentActions')
        state: require('../common/actions/StateActions')
        tap: require('../common/actions/TapActions')
        time: require('../common/actions/TimeActions')
        userAccount: require('../common/actions/UserAccountActions')
        selector: require('./actions/SelectorActions')
    }
    stores: {
        connection: require('../common/stores/ConnectionStore')
        dialogs: require('../common/stores/DialogsStore')
        forms: require('../common/stores/FormsStore')
        games: require('./stores/MyGamesStore')
        guide: require('../common/stores/GuideStore')
        news: require('../common/stores/NewsStore')
        notifications: require('../common/stores/NotificationsStore')
        pageData: require('./stores/PageDataStore')
        payments: [require('../common/stores/PaymentStore'), 'users']
        state: require('../common/stores/StateStore')
        subscription: require('../golf/stores/SubscriptionStore')
        time: [require('../common/stores/TimeStore'), 'connection']
        users: require('../common/stores/UsersStore')
    }
)
