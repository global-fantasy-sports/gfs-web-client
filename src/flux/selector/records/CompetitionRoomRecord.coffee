createRecord = require('../../createRecord')
{daysBetween, fromUtcTs, now} = require('../../../lib/date')
{gameNameByType} = require('../../../lib/const')

CompetitionRoomRecord = createRecord({
    id: null
    entryFee: null
    mode: null
    type: null
    gameType: null
    gamesCount: null
    maxEntries: null
    winners: null
    pot: null
    started: null
    finished: null
    startDate: [fromUtcTs, null]
    isForBeginners: ['beginner', false]
    isFeatured: ['featured', false]
    isGuaranteed: ['guaranteed', false]
    kwargs: [immutable.Map, immutable.Map()]
}, {

    diffText: ->
        diff = @getDaysLeft()
        if diff == 0
            diff = "TODAY"
        else if diff == 1
            diff = "TOMORROW"
        else if diff > 1
            diff = "in #{diff}\u00a0days from now"
        else
            diff = ""

    getDaysLeft: ->
        daysBetween(@startDate, now())

    fullName1: ->
        gameTypeTitle = gameNameByType[@gameType]

        if @winners == 1
            winnerText = '' + @winners + '\u00a0winner'
        else
            winnerText = '' + @winners + '\u00a0winners'

        @type + ' ' + gameTypeTitle + ' game ' + @diffText() + ' with\u00a0' + winnerText
})

module.exports = CompetitionRoomRecord