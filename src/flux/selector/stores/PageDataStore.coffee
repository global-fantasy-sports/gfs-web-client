_ = require('underscore')
Immutable = require('immutable')
{Store} = require('minimal-flux')
{fromUtcTs} = require('../../../lib/date')

CompetitionRoomRecord = require('../../selector/records/CompetitionRoomRecord')

EMPTY_ROOMS_LIST = Immutable.List()

class PageDataStore extends Store
    constructor: ->
        @state = {
            contests: Immutable.Map()
            overachievers: Immutable.Map(
                nfl: Immutable.List()
                nba: Immutable.List()
                golf: Immutable.List()
            )
            rooms: Immutable.Map(
                nfl: EMPTY_ROOMS_LIST
                nba: EMPTY_ROOMS_LIST
                golf: EMPTY_ROOMS_LIST
            )
            startTime: Immutable.Map(
                nfl: null
                nba: null
                golf: null
            )
            currentSport: 'nfl' # first page
        }

        @handleAction('data.loadSelectorRooms', @loadRooms)
        @handleAction('data.loadSelectorOverachievers', @loadOverachievers)
        @handleAction('data.setNextCompetitionStartTime', @setNextCompetitionStartTime)
        @handleAction('selector.setCurrentSport', @setCurrentSport)

    setCurrentSport: (currentSport) ->
        @setState({currentSport})

    getCurrentSport: ->
        @state.currentSport

    setNextCompetitionStartTime: (sport, time) ->
        startTime = @state.startTime.set(sport, fromUtcTs(time))
        @setState({startTime: startTime})

    loadRooms: (sport, data) ->
        rooms = @state.rooms
        list = _.map(data,
            (room) -> new CompetitionRoomRecord(room))
        rooms = rooms.set(sport, Immutable.List(list))
        @setState({rooms})

    loadOverachievers: (sport, data) ->
        overachievers = @state.overachievers
        overachievers = overachievers.set(sport, Immutable.List(_.map(data,
            (overachiever) -> Immutable.fromJS(overachiever))
        ))

        @setState({overachievers})

    getRooms: (sport) ->
        sport ?= @getCurrentSport()
        @state.rooms.get(sport)

    getContests: -> @state.contests

    getOverachievers: (sport) ->
        sport ?= @getCurrentSport()
        @state.overachievers.get(sport)

    getStartTime: (sport) ->
        sport ?= @getCurrentSport()
        @state.startTime.get(sport)

module.exports = PageDataStore
