_ = require('underscore')
Immutable = require('immutable')
CompetitionRoomRecord = require('flux/nfl/records/CompetitionRoomRecord')
{Store} = require("minimal-flux")

class CompetitionRoomStore extends Store
    constructor: ->
        @state = {
            allStarted: false
        }

        @handleAction('data.loadSelectorRooms', @loadRooms)
        @handleAction('data.updateCompetitionRooms', @updateCompetitionRooms)

    loadRooms: (sport, data) ->
        rooms = @state.rooms
        rooms.set(sport, immutable.List(_.map(data,
            (room) -> new CompetitionRoomRecord(room))
        ))

        allStarted = rooms.get(sport).every((r) -> r.started)
        @setState({rooms, allStarted})

    getRooms: (sport) ->
        rooms = @state.rooms.get(sport)
        return rooms


module.exports = CompetitionRoomStore
