{Store} = require('minimal-flux')
Immutable = require('immutable')


class MyGamesStore extends Store

    constructor: () ->
        @state = {
            games: Immutable.List()
        }

    getMyGames: () ->
        @state.games

    getActiveGamesCount: () ->
        @getMyGames().count((game) -> not game.isFinished())


module.exports = MyGamesStore
