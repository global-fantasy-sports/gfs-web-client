_ = require('underscore')
config = require('../config')
errors = require('../lib/errors')
xhr = require('../lib/xhr')


exports.checkAvailability = ->
    new Promise((resolve, reject) ->
        xhr(
            url: '/geoiplookup/'
            method: 'get'
            responseType: 'json'
            callback: (err, data) ->
                if data.blocked
                    reject(new Error('unavailable'))
                else
                    resolve('available')
        )
    )


exports.validateLogin = (data={}) ->
    for field in ['email', 'password']
        if not data[field]
            return "Please fill #{field}"


exports.validateRegister = (data={}) ->
    for field in ['email', 'name', 'password']
        if not data[field]
            return "Please fill #{field}"

    if data.country is "US" and not data.state
        return "Please choose state"

    unless data.terms
        return "Please accept Terms & Conditions"


exports.requestPasswordReset = (email) ->
    new Promise((resolve, reject) ->
        xhr(
            url: "/auth/password/reset/"
            method: 'post'
            responseType: 'json'
            data: JSON.stringify({email: email})
            callback: (err, data) =>
                error = data.error or err
                if error then reject(new Error(error))
                else resolve()
        )
    )


exports.checkResetPasswordLink = ({userId, token}) ->
    new Promise((resolve, reject) ->
        xhr(
            url: "/auth/password/reset/confirm/"
            method: 'post'
            responseType: 'json'
            data: JSON.stringify({token, user_secure_id: userId})
            callback: (err, data) =>
                error = data.error or err
                if error
                    reject(new Error(errors.get(error)))
                else
                    resolve()
        )
    )


exports.changePassword = ({token, userId, password1, password2}) ->
    new Promise((resolve, reject) ->
        if not password1
            throw new Error('Please enter the password')
        if password1 != password2
            throw new Error('Your new password and confirmation password do not match.')

        data = {token, user_secure_id: userId, password: password1}
        xhr(
            url: "/auth/password/change/"
            method: 'post'
            data: JSON.stringify(data)
            responseType: 'json'
            callback: (err, data) =>
                error = data.error or err
                if error
                    reject(new Error(errors.get(error)))
                else
                    resolve()
        )
    )


exports.updateProfile = ({showCountryAndTerms, terms, country, state, email}) ->
    new Promise((resolve, reject) ->
        if showCountryAndTerms
            if not terms
                throw new Error('Please accept Terms & Conditions')

            if not email
                throw new Error('Please provide your email')

            if country is 'US' and not state
                throw new Error('Please select a state')

        if not email
            throw new Error('Please provide your email')

        xhr(
            url: "/profile/update/"
            method: 'post'
            data: JSON.stringify({country, state, email})
            responseType: 'json'
            callback: (err, data) =>
                if data?.errors?.email == 'Email exists'
                    reject({email_exists: true})
                if err or data?.error
                    reject(new Error(errors.get(data?.error or err)))
                else
                    resolve(data)
        )
    )


exports.connectProfileToAnotherOne = ({email, password}) ->
    new Promise((resolve, reject) ->
        if not password
            throw new Error('Please enter a password')
        xhr(
            url: "/profile/connect/"
            method: 'post'
            data: JSON.stringify({email, password})
            responseType: 'json'
            callback: (err, data) ->
                if data?.error == 'Data is not valid'
                    reject(new Error(_.values(data.errors).join(', ')))
                else if err
                    reject(new Error('Unhandled exception'))
                else
                    resolve(data)
        )
    )


exports.uploadFile = ({file, url}) ->
    if not file
        return
    new Promise((resolve, reject) ->
        formData = new FormData()
        formData.append("file", file)
        xhr(
            url: config.server + url
            method: "post"
            data: formData
            responseType: "json"
            callback: (err, data) ->
                if err or data?.error
                    reject({err, data})
                else
                    resolve(data)
        )
    )
