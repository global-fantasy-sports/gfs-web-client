{Store} = require "minimal-flux"

class ChallengesStore extends Store
    constructor: ->
        @state =
            challenges: immutable.Map()

        @handleAction "data.setChallengeRequests", @setChallengeRequests
        @handleAction "data.newChallengeRequest", @setChallengeRequest

        connAuth.subscribe('challenge-request-removed', (data) => @removeChallengeRequests(data))
        connAuth.subscribe('new-challenge-request', (data) => @setChallengeRequest(data))

    setChallengeRequests: (data) ->
        challenges = @state.challenges
        for challenge in (data or [])
            challenges = challenges.set(challenge.uuid, immutable.fromJS(challenge))
        @setState({challenges})

    setChallengeRequest: (challenge) ->
        challenges = @state.challenges.set(challenge.uuid, immutable.fromJS(challenge))
        @setState({challenges})

    getChallenge: (challengeId) ->
        @state.challenges.get(challengeId, immutable.Map())

    getChallenges: (sport=SPORT, current=true) ->
        @state.challenges.valueSeq().filter((c) =>
            roomId = c.get('competitionRoomId')

            c.get('fromSport') == sport and
            @stores.rooms.getRoom(roomId).id > 0
        )

    removeChallengeRequests: (data) ->
        challenges = @state.challenges
        for challenge in (data or [])
            challenges = challenges.remove(challenge.uuid)
        @setState({challenges})

module.exports = ChallengesStore
