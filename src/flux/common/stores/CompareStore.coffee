Immutable = require('immutable')
{Store} = require('minimal-flux')
{ROLE_STATS} = require('../../../components/const')


class CompareStore extends Store

    constructor: () ->
        @state = {
            players: Immutable.OrderedMap()
            position: null
        }

        @handleAction('compare.add', @handleAddPlayer)
        @handleAction('compare.remove', @handleRemovePlayer)
        @handleAction('compare.clear', @handleClear)

    getNormalizedData: () ->
        {players, position} = @state

        if players.size > 0
            stats = players.map((p) -> p.getStatSummary())

            statNames = ['games', ROLE_STATS[position]...]
            if SPORT == 'golf'
                statNames = statNames.slice(1)

            realData = Immutable.OrderedMap(statNames.map((statName) ->
                [statName, players.map((player) -> stats.getIn([player.id, statName]))]
            ))

            normalizedData = realData.entrySeq().map(([statName, values]) =>
                min = @stores.participants.getMinStat(statName)
                max = @stores.participants.getMaxStat(statName)
                diff = (max - min) or 1
                values.map((v) -> (v - min) / diff).set('stat', statName)
            ).toList()

            data = normalizedData.toJS()

        return {players, position, data: data or null}

    handleAddPlayer: (player) ->
        @setState({
            players: @state.players.set(player.id, player)
            position: player.position || 1
        })

    handleRemovePlayer: (player) ->
        players = @state.players.delete(player.id)
        position = if players.size then (players.first().position || 1) else null
        @setState({players, position})

    handleClear: () ->
        @setState({
            players: @state.players.clear()
            position: null
        })


module.exports = CompareStore
