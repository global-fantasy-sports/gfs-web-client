_ = require("underscore")
{Store} = require("minimal-flux")
{Btn} = require('../../../lib/ui')
{Titlebar} = require('../../../lib/ui/Titlebar')


class DialogsStore extends Store
    constructor: ->
        @dialogs = {
            errors:
                NOT_ENOUGH_MONEY: ->
                    `<div className="tooltip-content">
                        <div>
                            <div className="tooltip-content__text">
                                Oops. You don&#39;t have enough money on your account to pay for the selected competitions. Please add funds.
                            </div>
                        </div>
                    </div>`

                NOT_ENOUGH_TOKENS: ->
                    `<div className="tooltip-content">
                        <div>
                            <div className="tooltip-content__text">
                                Oops. You don&#39;t have enough tokens on your account to pay for the selected competitions. Please add funds.
                            </div>
                        </div>
                    </div>`
            steps:
                'entry-fee': () ->
                    onTap = () ->
                        flux.actions.state.openNflRulesDialog()

                    `<div>
                        <div className="tooltip-content__header">Competiton room selection</div>
                        <div className="tooltip-content__text">
                            <p>Select competition room you like. GFS provides various types, what room to select is up to you.</p>
                            <p className="tooltip-content__p">For more details go to rules section.</p>
                            <p className="tooltip-content__p tooltip-content__p--centered">
                                <button className="btn btn--blue-base" onTap={onTap}>GAME RULES</button>
                            </p>
                        </div>
                    </div>`

                'friends': () ->
                    `<div>
                        <div className="tooltip-content__header">Friends invites</div>
                        <div className="tooltip-content__text">
                            <p>Challenge your friends from the Facebook!</p>
                        </div>
                    </div>`

            games:
                'hole-by-hole':
                    round: '
                        <ul class="disc-list">
                            <li>Choose a tournament</li>
                            <li>Pick two consecutive rounds to play in</li>
                            <li>Click “Next” to continue</li>
                        </ul>'

                    select: '
                        <ul class="disc-list">
                            <li>Select the three golfers for whom you will predict holes for</li>
                            <li>
                                Remember: It’s not about picking the best golfer,
                                but about predicting their performance!
                            </li>
                            <li>
                                Tip: Use the statistics for each golfer to see their past performances
                            </li>
                        </ul>'

                    predict: '
                        <ul class="disc-list">
                            <li>Predict the to-par for each of the 18 holes of the round</li>
                            <li>There are 3 options: “under par”, “par” and “over par”</li>
                            <li>Whoever gets the most correct predictions, wins!</li>
                            <li>Tip: See how your golfer did on this course the previous
                                round or last tournament
                            </li>
                        </ul>'

                    stake: '
                        <ul class="disc-list">
                            <li>Different size competition rooms let’s you play for more or less</li>
                            <li>Select your entry fee and pay to proceed</li>
                            <li>Remember: Higher stake sizes give bigger prizes</li>
                        </ul>'

                    friends: '
                        <ul class="disc-list">
                            <li>Challenge friends here and they’ll get a notification about it</li>
                            <li>Connect e.g. Facebook to invite friends to play against you</li>
                        </ul>'

                'handicap-the-pros':
                    round: '
                        <ul class="disc-list">
                            <li>Choose a tournament</li>
                            <li>Pick two rounds</li>
                            <li>Click “Next” to continue</li>
                        </ul>'

                    select: '
                        <ul class="disc-list">
                            <li>Select three golfers from the list of golfers</li>
                            <li>NB! Handicap are extra strokes you get for each golfer</li>
                            <li>Hint: How you well you apply those extra strokes helps you win</li>
                            <li>Remember: You win by scoring most modified stableford points</li>
                        </ul>'

                    handicap: '
                        <ul class="disc-list">
                            <li>Use all handicap strokes on available holes</li>
                            <li>Hint: Expending a handicap stroke will subtract 1 stroke (-1) from
                                the total stroke score of the hole.</li>
                            <li>Tip: Apply handicap strokes to holes your golfers are likely to
                                score birdies on and supercharge them into eagles
                            </li>
                            <li>Remember: In modified stableford an Eagle is worth +5 points,
                                a Birdie +2, a par +1.</li>
                        </ul>'

                    stake: '
                        <ul class="disc-list">
                            <li>Different size competition rooms let’s you play for more or less</li>
                            <li>Select your entry fee and pay to proceed</li>
                            <li>Remember: Higher stake sizes give bigger prizes</li>
                        </ul>'

                    friends: '
                        <ul class="disc-list">
                            <li>Challenge friends here and they’ll get a notification about it</li>
                            <li>Connect e.g. Facebook to invite friends to play against you.</li>
                        </ul>'

                'five-ball':
                    round: '<ul class="disc-list">
                        <li>Choose a tournament</li>
                        <li>Pick two rounds</li>
                        <li>Click “Next” to continue</li>
                    </ul>'

                    select: '
                        <ul class="disc-list">
                            <li>Win by scoring the most stableford points</li>
                            <li>Choose 5 golfers in total</li>
                            <li>
                                You don’t have to select 1 from each tier, you can select
                                2 from top 2 instead
                            </li>
                            <li>Scroll down to select from top 20, 30, 40 and 50</li>
                        </ul>'

                    stake: '
                        <ul class="disc-list">
                            <li>Different sized competition rooms let you play for higher or lower stakes</li>
                            <li>Select your entry fee and pay to proceed</li>
                            <li>Remember: The higher the stakes the bigger the prizes</li>
                        </ul>'

                    friends: '
                        <ul class="disc-list">
                            <li>Challenge friends here and they’ll get a notification about it</li>
                            <li>Connect e.g. Facebook to invite friends to play against you.</li>
                        </ul>'
                nfl:
                    select: () ->
                        onTap = () ->
                            flux.actions.state.openNflRulesDialog()

                        `<div>
                            <div className="tooltip-content__header">Drafting athletes</div>
                            <div className="tooltip-content__text">
                                <p>
                                    Draft your lineup (8 players and DST).
                                    You’re given $50,000 budget as a salary cap (because each player has own price).
                                    Spend it on players to create your best lineup.
                                </p>
                                <p className="tooltip-content__p">For more details go to rules section.</p>
                                <p className="tooltip-content__p tooltip-content__p--centered">
                                    <button className="btn btn--blue-base" onTap={onTap}>GAME RULES</button>
                                </p>
                            </div>
                        </div>`
        }


        @state = {
            dialog: null
            component: null
            modal: false
            dialogClassname: null
        }

        @handleCloseOutside()
        @handleAction("tap.hitAndTricks", @openHitAndTricksDialog)
        @handleAction("state.openResetPasswordDialog", @openResetPasswordDialog)
        @handleAction("tap.closeDialog", @closeDialog)
        @handleAction("state.passwordWasReset", @openPasswordResetInfo)
        @handleAction("state.passwordResetError", @openPasswordResetErrorInfo)
        @handleAction("state.openFinishRegistrationDialog", @openFinishRegistrationDialog)
        @handleAction("state.openAccountExistsDialog", @openAccountExistsDialog)
        @handleAction("state.openPasswordWasResetModalDialog", @openPasswordWasResetModalDialog)
        @handleAction("state.openInfoMessage", @openInfoMessage)
        @handleAction("tap.finishRegistration", @closeDialog)
        @handleAction("tap.openRemoveGameDialog", @openRemoveGameDialog)
        @handleAction("tap.openRemoveChallengeRequestDialog", @openRemoveChallengeRequestDialog)
        @handleAction("state.openPlayerDetailsDialog", @openPlayerDetailsDialog)
        @handleAction("state.openEFFDialog", @openEFFDialog)
        @handleAction('state.openNflRulesDialog', @openNflRulesDialog)
        @handleAction('state.openDeleteLineupDialog', @openDeleteLineupDialog)
        @handleAction('state.openSaveLineupDialog', @openSaveLineupDialog)
        @handleAction('state.openLineupSavedDialog', @openLineupSavedDialog)

    handleCloseOutside: ->
        document.onclick = (e) =>
            {dialog, component} = @state
            return unless dialog or component

            node = e.target
            while true
                unless node then break
                if node.id == "PopupContainer"
                    break
                else if node == document.body and not @state.modal
                    flux.actions.tap.closeDialog()
                    break
                else
                    node = node.parentNode

    openHitAndTricksDialog: (gameType, stepName) ->
        contentHtml = @dialogs.games[gameType][stepName]

        html =
            `<div className="helping-text">
                <div className="helping-text__title">Hints & Tips</div>
                <div className="helping-text__inner">
                    <div className="helping-text__content">
                        <div dangerouslySetInnerHTML={{__html: contentHtml}} />
                    </div>
                </div>
            </div>`

        @setState({dialog: html})

    openNflRulesDialog: ->
        CommonDialog = require('../../../components/dialogs/CommonDialog')
        NflRulesDialog = require('../../../components/dialogs/NflRulesDialog')

        component =
            `<CommonDialog mod="no-pad">
                <NflRulesDialog  />
            </CommonDialog>`

        @setState({dialog: null, component, modal: true})

    openDeleteLineupDialog: (lineup) ->
        DeleteLineupDialog = require '../../../components/dialogs/DeleteLineupDialog'
        component = `<DeleteLineupDialog lineup={lineup} />`
        @setState({dialog: null, component, modal: true})

    openLineupSavedDialog: (lineup) ->
        LineupSavedDialog = require '../../../components/dialogs/LineupSavedDialog'
        component = `<LineupSavedDialog lineup={lineup} />`
        @setState({dialog: null, component, modal: true})

    openSaveLineupDialog: (lineup) ->
        SaveLineupDialog = require '../../../components/dialogs/SaveLineupDialog'
        component = `<SaveLineupDialog lineup={lineup} />`
        @setState({dialog: null, component, modal: true})

    openRemoveGameDialog: (cb) ->
        setTimeout(() =>
            closeDialog = @closeDialog.bind(this)

            CommonDialog = require('../../../components/dialogs/CommonDialog')

            component =
                `<CommonDialog>
                    <Titlebar
                        mod="h3"
                        className="dark"
                        title="Are you sure you want to delete this game?"
                    />
                    <p className="stroke black">
                        If you will delete the game,
                        we will refund your entry fee.
                    </p>
                    <div className="btn-inline">
                        <Btn mod="red" onTap={cb}>
                            Yes, delete it
                        </Btn>
                        <Btn mod="green" onTap={closeDialog}>
                            No, leave it
                        </Btn>
                    </div>
                </CommonDialog>`

            @setState({dialog: null, component})
        , 1)

    openRemoveChallengeRequestDialog: (cb) ->
        setTimeout(() =>
            closeDialog = @closeDialog.bind(this)
            closeAndRun = () =>
                @closeDialog()
                cb()

            CommonDialog = require('../../../components/dialogs/CommonDialog')

            component =
                `<CommonDialog>
                    <Titlebar
                        mod="h3"
                        className="dark"
                        title="Are you sure you want to delete this challenge request?"
                    />
                    <div className="btn-inline">
                        <Btn mod="red" onTap={closeAndRun}>
                            Yes, delete it
                        </Btn>
                        <Btn mod="green" onTap={closeDialog}>
                            No, leave it
                        </Btn>
                    </div>
                </CommonDialog>`

            @setState({dialog: null, component})
        , 1)


    openResetPasswordDialog: ->
        setTimeout(() =>
            {Input} = require('lib/forms')
            closeDialog = @closeDialog.bind(this)

            sendResetLink = =>
                flux.actions.tap.sendResetLink(flux.stores.forms.getValue("RESET_PASSWORD_EMAIL"))

            html =
                `<section className="reset-password-dialog common-dialog">
                    <span className="common-dialog__title">
                        Please enter your email address
                    </span>
                    <Input
                        className="reset-password-dialog__input"
                        placeholder="Email"
                        validationId="RESET_PASSWORD_EMAIL"
                        name="RESET_PASSWORD_EMAIL"
                    />
                    <div className="button-group">
                        <Btn className="button-group__btn" mod="orange medium" onTap={sendResetLink}>
                            Reset
                        </Btn>
                        <Btn mod="white medium" onTap={closeDialog}>
                            Cancel
                        </Btn>
                    </div>
                </section>`

            @setState({dialog: html})
        , 1)

    openPasswordResetInfo: ->
        setTimeout(() =>
            email = @stores.forms.getValue("RESET_PASSWORD_EMAIL")
            html =
                `<section className="common-dialog">
                    <span className="common-dialog__title">Success</span>
                    <span>Password reset link was sent to {email}</span>
                </section>`

            @setState({dialog: html})
        , 1)

    openPasswordResetErrorInfo: (error) ->
        setTimeout(() =>
            CommonDialog = require('../../../components/dialogs/CommonDialog')

            component =
                `<CommonDialog title="Error">
                    <p>{error.message}</p>
                </CommonDialog>`

            @setState({dialog: null, component})
        , 1)

    openFinishRegistrationDialog: ->
        setTimeout(() =>
            user = @stores.users.getCurrentUser()
            email = user.get("email") || user.get("fb_email")
            flux.actions.forms.setValue("FINISH_REGISTRATION_EMAIL", email)
            flux.actions.forms.setValue("country", user.get("country") || "US")
            flux.actions.forms.setValue("state", user.get("state"))

            CommonDialog = require('../../../components/dialogs/CommonDialog')
            FinishRegistrationDialog = require('../../../components/dialogs/FinishRegistrationDialog')

            component = ->
                `<CommonDialog title="Finish your registration please">
                    <FinishRegistrationDialog />
                </CommonDialog>`

            @setState({dialog: null, modal: true, component})
        , 1)

    openAccountExistsDialog: ->
        setTimeout(() =>
            CommonDialog = require('../../../components/dialogs/CommonDialog')
            AccountExistsDialog = require('../../../components/dialogs/AccountExistsDialog')

            component = ->
                `<CommonDialog title="Account exists">
                    <AccountExistsDialog />
                </CommonDialog>`

            @setState({dialog: null, modal: true, component})
        , 1)

    openPasswordWasResetModalDialog: ->
        setTimeout(() =>
            CommonDialog = require('../../../components/dialogs/CommonDialog')

            onBack = =>
                flux.actions.state.openAccountExistsDialog()

            component = ->
                `<CommonDialog title="Password was reset">
                    <p>
                        Password reset link was sent to your email.
                        Check your inbox please.
                    </p>
                    <section className="common-dialog__action-bar">
                        <Btn mod="green medium" onTap={onBack}>
                            Back
                        </Btn>
                    </section>
                </CommonDialog>`

            @setState({dialog: null, modal: true, component})
        , 1)

    openPlayerDetailsDialog: (id) ->
        if not id
            return

        _.defer(() =>
            PlayerStatsDialog = require('../../../components/stats/PlayerStatsDialog')
            onClose = -> flux.actions.tap.closeDialog()

            component = ->
                `<PlayerStatsDialog participantId={id} onHide={onClose} />`

            @setState({dialog: null, modal: true, component})
        )

    openEFFDialog: () ->
        _.defer(() =>
            EFFDialog = require('../../../selector/EFFDialog')
            onClose = -> flux.actions.tap.closeDialog()

            component = -> `<EFFDialog onHide={onClose} />`
            @setState({dialog: null, modal: true, component})
        )

    openInfoMessage: (message) ->
        setTimeout( =>
            CommonDialog = require('../../../components/dialogs/CommonDialog')

            close = -> flux.actions.tap.closeDialog()

            component =
                `<CommonDialog>
                    <p>{message}</p>
                    <section className="common-dialog__action-bar">
                        <Btn mod="green medium" onTap={close}>OK</Btn>
                    </section>
                </CommonDialog>`

            @setState({dialog: null, modal: false, component})
        )

    getErrorTooltipContent: (errorCode) ->
        if @dialogs.errors[errorCode]
            @dialogs.errors[errorCode]()
        else `<div />`

    closeDialog: ->
        @setState({dialog: null, modal: false, component: null})

    getDialog: ->
        @state.dialog

    getComponent: ->
        @state.component

    getDialogClassname: ->
        @state.dialogClassname

    getText: (gameType, stepName) ->
        @dialogs.games[gameType][stepName]

    getStepText: (stepName) ->
        @dialogs.steps[stepName]


module.exports = DialogsStore
