_ = require "underscore"
{Store} = require "minimal-flux"

GUIDE_CONFIG =
    'home': [
        ['tokens'],
        ['tabs'],
        ['play'],
        ['seemore1'],
        ['games'],
        ['tour'],
        ['competitions']
    ]
    'wizard': [
        ['tour'],
        ['round'],
        ['next'],
        ['golfer'],
        ['seemore'],
        ['entryfee'],
        ['friends']
    ]
    'prediction': [
        ['predict'],
        ['next-predict'],
    ]


getInitialState = -> {
    guideActive: false
    passedSteps: immutable.Set()
}


class GuideStore extends Store
    constructor: ->
        @state = getInitialState()

        @handleAction "connection.connectSuccess", @onConnectSuccess
        @handleAction "tap.guideHintTap", @toggleSteps
        @handleAction "tap.closeGuide", @closeGuide
        @handleAction "tap.activateGuide", @activateGuide
        @handleAction "tap.resetGuide", @resetGuide

    isGuideHintActive: (group, id) ->
        if @state.guideActive
            {passedSteps} = @state
            stepsActive = (steps)-> steps.filter((id)-> not passedSteps.contains("#{group}:#{id}")).length > 0
            steps = GUIDE_CONFIG[group].find(stepsActive)
            if id is 'next-predict' and _.first(steps) is id
                @stores.wizard.getCurrentSubstep() == 2
            else
                steps and id in steps

    toggleSteps: (group, id) ->
        @setState(passedSteps: @state.passedSteps.add("#{group}:#{id}"))

    closeGuide: ->
        @setState guideActive: false

    activateGuide: ->
        @setState guideActive: true

    resetGuide: ->
        @setState(getInitialState())

    isGuideActive: ->
        @state.guideActive

    onConnectSuccess: ({user}) ->
        @setState({
            passedSteps: immutable.Set(user.guide_steps)
            guideActive: user.guide_mode
        })


module.exports = GuideStore
