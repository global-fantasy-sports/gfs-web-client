{Store} = require('minimal-flux')
Immutable = require('immutable')


class NewsStore extends Store

    constructor: ()->
        @state = {
            status: 'initial'
            news: Immutable.List(),
            error: null,
        }

        @handleAction('news.fetch', @handleFetch)
        @handleAction('news.fetchResponse', @handleFetchResponse)
        @handleAction('news.fetchError', @handleFetchError)

    handleFetch: (sport) ->
        @setState({status: 'busy'})

    handleFetchResponse: ({news}) ->
        @setState({
            status: 'done',
            news: Immutable.fromJS(news)
        })

    handleFetchError: (error) ->
        @setState({
            status: 'initial',
            error: error
        })


module.exports = NewsStore
