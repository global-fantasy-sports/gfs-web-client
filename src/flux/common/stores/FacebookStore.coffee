_ = require('underscore')
{Store} = require('minimal-flux')
Immutable = require('immutable')


FacebookFriendBase = Immutable.Record({
    id: null
    name: ''
    picture: ''
    invited: false
    registered: false
}, 'FacebookFriendBase')

class FacebookFriend extends FacebookFriendBase

    constructor: ({id, name, picture, registered, invited}) ->
        super({
            id, name
            picture: picture?.data.url
            registered: registered ? false
            invited: invited ? false
        })

    group: ->
        if @registered
            'registered'
        else
            'notRegistered'

STATUS = {
    IDLE: 'idle'
    BUSY: 'busy'
    ERROR: 'error'
    FETCHED: 'fetched'
    DECLINED: 'declined'
    NOT_AUTHORIZED: 'not_authorized'
    NOT_LOGGED_IN: 'not_logged_in'
}


class FacebookStore extends Store

    @STATUS: STATUS

    constructor: ->
        @handleAction('facebook.initSuccess', @handleInitSuccess)
        @handleAction('facebook.login', @handleRequestStart)
        @handleAction('facebook.loginSuccess', @handleLoginSuccess)
        @handleAction('facebook.loginFail', @handleLoginFail)
        @handleAction('facebook.fetchFriends', @handleRequestStart)
        @handleAction('facebook.requestError', @handleRequestError)
        @handleAction('facebook.fetchFriendsSuccess', @handleFetchFriendsSuccess)
        @handleAction('facebook.fetchFriendsFail', @handleFetchFriendsFail)
        @handleAction('facebook.checkRegisteredFriends', @handleCheckRegisteredFriends)
        @handleAction('facebook.invite', @handleInvite)
        @handleAction('facebook.search', @handleSearch)
        @handleAction('facebook.filter', @handleFilter)
        @handleAction('facebook.sort', @handleSort)

        @state = {
            FB: null
            permissions: null
            status: STATUS.IDLE
            error: null
            friends: new Immutable.OrderedMap()
            search: ''
            sorting: 'name'
            reversed: false
            filter: null
        }

    isInitialized: ->
        @state.FB?

    isIdle: ->
        {FB, status} = @state
        FB? and status is STATUS.IDLE

    getFilteredFriends: ->
        {friends, filter, search} = @state
        friends = friends.toIndexedSeq()

        if search
            searchRe = new RegExp(search.trim(), 'i')
            friends = friends.filter((f) -> searchRe.test(f.name))

        if filter is 'registered'
            friends = friends.filter((f) -> f.registered)
        else if filter is 'notRegistered'
            friends = friends.filterNot((f) -> f.registered)

        return {
            filter, search
            friends: friends.toList()
            sorting: @state.sorting
            reversed: @state.reversed
            status: @state.status
            error: @state.error
        }

    handleInitSuccess: (FB) ->
        @setState({FB})

    handleRequestStart: ->
        @setState({status: STATUS.BUSY, error: null})

    handleRequestError: (error) ->
        @setState({status: STATUS.ERROR, error})

    handleLoginSuccess: (authResponse) ->
        @setState({
            status: STATUS.IDLE
            permissions: authResponse.grantedScopes
        })

    handleLoginFail: ->
        @setState({
            status: STATUS.NOT_LOGGED_IN
            permissions: null
        })

    handleFetchFriendsSuccess: (friends) ->
        friends = _.sortBy(friends, 'name')
            .map((f) -> [f.id, new FacebookFriend(f)])
        @setState({
            friends: new Immutable.OrderedMap(friends)
            sorting: 'name'
            reversed: false
        })

    handleFetchFriendsFail: (error) ->
        status = switch error.message
            when 'declined' then STATUS.DECLINED
            when 'not_authorized' then STATUS.NOT_AUTHORIZED
            else STATUS.NOT_LOGGED_IN

        @setState({status})

    handleCheckRegisteredFriends: (registered) ->
        registered = registered.reduce(((obj, id) ->
            obj[id] = {registered: true}
            return obj
        ), {})
        @setState({
            friends: @state.friends.mergeDeep(registered)
            status: STATUS.FETCHED
        })

    handleInvite: (id) ->
        @setState({friends: @state.friends.setIn([id, 'invited'], true)})

    handleSearch: (search) ->
        @setState({search})

    handleFilter: (filter) ->
        @setState({filter})

    handleSort: ({sorting, reversed}) ->
        friends = @state.friends.toSeq().sortBy((f) ->
            value = f.get(sorting)
            if typeof value is "boolean"
                if value then -1 else 1
            else
                value
        )
        if reversed
            friends = friends.reverse()

        @setState({
            sorting, reversed
            friends: friends.toOrderedMap()
        })


module.exports = FacebookStore
