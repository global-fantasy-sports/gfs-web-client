{Store} = require 'minimal-flux'
{List, Map} = require 'immutable'

{Form} = require 'lib/validation'
states = require '../../../lib/constants/states'


createVerificationForm = ->
    new Form({
        firstName: {type: 'text'}
        lastName: {type: 'text'}
        address: {type: 'text'}
        city: {type: 'text'}
        state: {type: 'text', validate: {presence: false}}
        zip: {type: 'text', validate: {zip: true}}
    })


getVerificationData = (user)->
    [firstName, ..., lastName] = user.get('name', '').split(' ')
    return {
        firstName
        lastName
        address: user.get('address', '')
        city: user.get('city', '')
        state: user.get('state', '')
        zip: user.get('zip', '')
    }


createWithdrawalForm = (store)->
    new Form({
        amount: {
            type: 'text'
            validate: {
                withdrawable: {max: store.getAvailableForWithdrawUsd}
            }
        }
        firstName: {type: 'text', JSONName: 'firstname'}
        lastName: {type: 'text', JSONName: 'lastname'}
        street: {type: 'text', JSONName: 'billing_street'}
        city: {type: 'text', JSONName: 'billing_city'}
        state: {options: states, JSONName: 'billing_state'}
        zip: {
            type: 'text', JSONName: 'billing_zip'
            validate: {presence: true, zip: true}}
        phone: {
            type: 'text'
            validate: {presence:true, phone: true}}
        routingNum: {
            type: 'text', JSONName: 'routingnum'
            validate: {presence: true, routingNum: true}}
        accountNum: {
            type: 'text', JSONName: 'accountnum'
            validate: {presence: true, accountNum: true}}
        bankName: {type: 'text', JSONName: 'bankname'}
    })


class PaymentStore extends Store

    constructor: ->
        @verificationForm = createVerificationForm()
        @withdrawalForm = createWithdrawalForm(this)

        @state =
            status: 'idle'
            history: new List()
            depositRequest: null
            withdrawRequest: 'norequests'
            bankData: null
            balance:
                usd: 0
                withdrawable: 0

        @handleAction 'payment.deposit', @handleDeposit
        @handleAction 'payment.depositSuccess', @handleDepositSuccess
        @handleAction 'payment.depositFail', @handleDepositFail
        @handleAction 'payment.requireVerification', @handleRequireVerification
        @handleAction 'payment.verify', @handleVerify
        @handleAction 'payment.verifyFail', @handleVerifyFail
        @handleAction 'payment.verifySuccess', @handleVerifySuccess
        @handleAction 'payment.withdraw', @handleWithdraw
        @handleAction 'payment.withdrawFail', @handleWithdrawFail
        @handleAction 'payment.withdrawSuccess', @handleWithdrawSuccess
        @handleAction 'data.setHistory', @setPaymentsHistory
        @handleAction 'data.setWithdrawState', @setWithdrawState
        @handleAction 'data.setBalance', @setBalance
        @handleAction "data.setBankData", @setBankData

        @handleAction('data.setCurrentUser', => @emit('change'))

    isBusy: -> @state.status is 'busy'

    isVerified: -> @stores.users.getCurrentUser().get('verified', false) and @state.bankData

    getVerificationState: -> {
        status: @state.status
        form: @verificationForm
        verified: @isVerified()
    }

    getWithdrawalState: -> {
        status: @state.status
        form: @withdrawalForm
        history: @state.history
        totalUsd: @state.balance.usd
        availableUsd: @state.balance.withdrawable
    }

    handleDeposit: (depositRequest)->
        @setState({status: 'busy', depositRequest})

    handleDepositSuccess: ->
        @setState({status: 'idle'})

    handleDepositFail: (error)->
        @setState({
            status: 'idle',
            depositRequest: new Error(error)
        })

    handleRequireVerification: (questions)->
        user = @stores.users.getCurrentUser()
        @verificationForm.update(getVerificationData(user, questions))
        @setState({status: 'idle'})

    handleVerify: (amount)->
        @setState({status: 'busy'})

    handleVerifySuccess: ->
        @verificationForm.clearErrors()
        @setState({status: 'idle'})

    handleVerifyFail: (errorMessage)->
        @verificationForm.setErrors({'__form__': errorMessage})
        @setState({status: 'idle'})

    handleWithdraw: ->
        @setState({status: 'busy', withdrawRequest: null})

    handleWithdrawSuccess: (data)->
        @setState({status: 'idle', withdrawRequest: 'success'})

    handleWithdrawFail: (error)->
        @setState({status: 'idle', withdrawRequest: new Error(error)})

    setPaymentsHistory: (data) ->
        data = for row in data
            row.date = new Date(row.date * 1000)
            row
        @setState(history: immutable.fromJS(data).toList())

    setWithdrawState: (status) ->
        @setState(withdrawRequest: status)

    getPaymentsHistory: ->
        return @state.history

    getWithdrawState: ->
        return @state.withdrawRequest

    getTotalUsd: ->
        return @state.balance.usd

    getAvailableForWithdrawUsd: =>
        return @state.balance.usd

    setBalance: (balance) ->
        @setState({balance})

    setBankData: (data)->
        @withdrawalForm.fromJSON(data)
        @setState({bankData: data})

    getBankData: -> @state.bankData


module.exports = PaymentStore
