{Store} = require 'minimal-flux'
{Record, Map, OrderedMap, Set} = require 'immutable'

{fromUtcTs} = require 'lib/date'
{parseMoney} = require 'lib/utils'


IMPORTANT_MESSAGES = [
    'money_deposit'
    'deposit_confirmed'
    'money_deposit_declined'
    'game_cancelled'
    'money_withdrawal_request'
    'not_enough_money'
]


BaseNotification = Record(
    id: null
    title: ''
    name: ''
    message: ''
    read: false
    date: null
    important: false
)


class Notification extends BaseNotification

    constructor: ({id, name, title, read, created_at, message})->
        super({
            id, name, title, read, message
            important: name in IMPORTANT_MESSAGES
            date: fromUtcTs(created_at)
        })

class NotificationsStore extends Store

    constructor: ->
        @handleAction('connection.authConnectSuccess', @createNotifications)
        @handleAction('data.newParticipant', @handleNewParticipant)
        @handleAction('notifications.received', @handleReceived)
        @handleAction('notifications.markAsRead', @handleMarkAsRead)
        @handleAction('notifications.markAllRead', @handleMarkAllRead)
        @handleAction('notifications.markAllImportantRead', @handleMarkAllImportantRead)
        @handleAction('notifications.togglePanel', @handlePanelToggle)
        @handleAction('notifications.closePanel', @handlePanelClose)

        @state = {
            panelOpen: false
            notifications: new OrderedMap()
            showOnlyImportant: false
        }

    get: ->
        @state.notifications

    getState: ->
        {panelOpen, showOnlyImportant} = @state

        notifications = @state.notifications.toIndexedSeq()
        unread = notifications.filter((n)-> not n.read).cacheResult()

        important = unread.filter((n)->
            n.important
        )

        {notifications, important, unreadCount: unread.count(), panelOpen, showOnlyImportant}

    getUnread: ->
        @state.notifications.filter((n)-> not n.read)

    getImportant: ->
        unread = @state.notifications.filter((n)-> not n.read)
        unread.filter((n) -> n.important)

    createNotifications: (data)->
        notifications = new OrderedMap(data.notifications.map((item)->
            n = new Notification(item)
            [item.id, n]
        ))

        unread = notifications.filter((n)-> not n.read)
        important = unread.filter((n)->
            n.important
        )

        if important.count()
            setTimeout( () =>
                @setState({panelOpen: true, showOnlyImportant: true})
            , 6000)

        @setState({notifications})

    handlePanelToggle: ->
        if !@state.panelOpen
            @setState({showOnlyImportant: false})

        @setState({panelOpen: !@state.panelOpen})

    handlePanelClose: ->
        @setState({
            panelOpen: false
        })

    handleNewParticipant: (items)->
        @emit('change')

    handleReceived: (item) ->
        panelOpen = @state.panelOpen
        newNotification = new Notification(item)
        received = new OrderedMap([[item.id, newNotification]])

        if newNotification.important
            showOnlyImportant = if !panelOpen then true else false
            panelOpen = true
        else showOnlyImportant = false

        @setState({
            notifications: received.concat(@state.notifications),
            panelOpen,
            showOnlyImportant
        })

    handleMarkAsRead: (id)->
        @setState({
            notifications: @state.notifications.setIn([id, 'read'], true)
        })

    handleMarkAllRead: ->
        notifications = @state.notifications.withMutations((obj)=>
            @getUnread().forEach((n)-> obj.setIn([n.id, 'read'], true))
        )
        @setState({notifications})

    handleMarkAllImportantRead: ->
        notifications = @state.notifications.withMutations((obj)=>
            @getImportant().forEach((n)-> obj.setIn([n.id, 'read'], true))
        )
        @setState({notifications})

    isPanelOpen: -> @state.panelOpen

    isShowOnlyImportant: -> @state.showOnlyImportant


module.exports = NotificationsStore
