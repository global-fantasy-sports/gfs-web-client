_ = require("underscore")
{Store} = require("minimal-flux")
{RoomsComparators} = require('../../../components/const')


class CompetitionRoomStore extends Store

    getRooms: ->
        new Error('Not implemented')

    getFilteredRooms: ->
        {onlyBeginners,
        onlyFeatured,
        onlyGuaranteed,
        gameTypeFilters,
        paymentType,
        sortingField,
        roomTypeFilters,
        sortingDirection,
        startDate} = @stores.search.getState()

        options = {}

        [min, max] = @stores.search.getEntryFeeInterval()
        filters = [
            (room) -> min <= room.entryFee <= max
            (room) -> room.gamesCount != room.maxEntries
            (room) -> not room.started
        ]

        if roomTypeFilters.count()
            filters.push((room) -> roomTypeFilters.has(room.type))

        if gameTypeFilters.count()
            filters.push((room) -> gameTypeFilters.has(room.gameType))

        if paymentType
            options.mode = paymentType

        if onlyBeginners
            options.isForBeginners = true

        if onlyFeatured
            options.isFeatured = true

        if onlyGuaranteed
            options.isGuaranteed = true

        if startDate is 'nearest'
            options.startDate = @getNextGame()?.startDate
        else if startDate
            options.startDate = startDate

        comparator = RoomsComparators[sortingField]

        isReverse = sortingDirection == 'DESC'

        @getRooms(options, filters, (a, b) ->
            if comparator(a) > comparator(b)
                if isReverse then return -1 else return 1
            else if comparator(a) < comparator(b)
                if isReverse then return 1 else return -1
            else 0
        )

    getNextGame: ->
        @state.rooms.toIndexedSeq().sortBy((r) -> r.startDate).find((r) -> r.isPending())


module.exports = CompetitionRoomStore
