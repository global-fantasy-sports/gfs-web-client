_ = require('underscore')
{Store} = require('minimal-flux')

{GAMES, STATE} = require('lib/const')
{splitQS, countTracker} = require('lib/utils')
{Form} = require('lib/validation')
{fakeTour} = require('../../../world')
countries = require('../../../lib/constants/countries')
states = require('../../../lib/constants/states')


module.exports = class ConnectionStore extends Store

    constructor: ->
        @handleAction('connection.init', @onInit)
        @handleAction('connection.connect', @onConnectStart)
        @handleAction('connection.connectSuccess', @onConnectSuccess)
        @handleAction('connection.connectError', @onConnectError)
        @handleAction('connection.disconnect', @onDisconnect)
        @handleAction('connection.facebookLogin', @handleAuthStart)
        @handleAction('connection.googleLogin', @handleAuthStart)

        @handleAction('connection.loginError', @handleAuthError)
        @handleAction('connection.signupError', @handleAuthError)
        @handleAction('connection.logout', @handleLogout)

        @handleAction('connection.unavailable', @onUnavailable)
        @handleAction('connection.setWaitingState', @onSetWaitingState)
        @handleAction('connection.initialDataPushed', @onInitialDataPushed)
        @handleAction('connection.setProcessingState', @setProcessingState)
        @handleAction('connection.processed', @setConnectedState)

        fakeTour.on(=> @onFakeTourUpdate())

        {games, app_data} = splitQS(window.location.search[1..])

        @state =
            state: STATE.START
            loginForm: createLoginForm()
            signupForm: createSignupForm()
            oneUserMode: false
            fakeTourMode: false
            initialDataPushed: {}
            initialDataParsed: {}
            games: games?.split(',') or GAMES
            initial: if app_data then JSON.parse(app_data) else false
            processingMessage: ''

    setProcessingState: (message) ->
        @setState(state: STATE.PROCESSING, processingMessage: message)

    setConnectedState: ->
        @setState(state: STATE.CONNECTED)

    isAuthenticated: -> @state.state isnt STATE.START

    isDisabled: -> @state.state is STATE.DISABLED

    getLoginForm: -> @state.loginForm

    getSignupForm: -> @state.signupForm

    onInit: ({session}) ->
        @setState(state: if session then STATE.WAITING else STATE.START)

    onConnectStart: ->
        @setState(state: STATE.WAITING)

    onConnectSuccess: (data) ->

        @setState({
            info: data.info
            initial: data.initial
            oneUserMode: data.mode
            fakeTourMode: data.fake_tour_mode
        })

    onInitialDataPushed: (initialDataPushed) ->
        @setState({
            initialDataPushed
            state: STATE.CONNECTED
        })

    onConnectError: (error) ->
        @setState(state: STATE.START)

    onDisconnect: ->
        if @state.state isnt STATE.START
            @setState(state: STATE.DISCONNECTED)

    handleAuthStart: ->
        @setState(state: STATE.WAITING)

    handleAuthError: (error) ->
        @setState({state: STATE.START})

    handleLogout: ->
        @setState({state: STATE.WAITING})

    onUnavailable: ->
        @setState(state: STATE.BLOCKED)

    onSetWaitingState: ->
        @setState(state: STATE.WAITING)

    onFakeTourUpdate: ->
        if state is STATE.CONNECTED and fakeTour.value?
            user = @stores.users.getCurrentUser()
            {state, oneUserMode} = @state
            if fakeTour.value.user_id? isnt user.id and oneUserMode
                @setState(state: STATE.DISABLED)
            else
                @emit('change')


createSignupForm = ->
    new Form({
        email: {
            type: 'email'
            default: ''
            validate: {
                presence: {message: '^Please enter your email'}
                emailAvailable: true
            }
        }
        name: {
            type: 'text'
            default: ''
            validate: {presence: {message: '^Please enter your name'}}
        }
        password: {
            type: 'password'
            default: ''
            validate: {
                presence: {message: '^Please enter a password'}
                length: {minimum: 6, message: 'is too short'}
            }
        }
        country: {
            type: 'select'
            default: ''
            options: countries
            validate: {presence: {message: '^Please select your country'}}
        }
        state: {
            type: 'select'
            default: ''
            options: states
            validate: {state: {message: 'Please select your state'}}
        }
        terms: {
            type: 'checkbox'
            default: false
            validate: {terms: {message: '^Please accept terms and conditions'}}
        }
    }, {
        initial: {country: 'US'}
    })


createLoginForm = ->
    new Form({
        email: {
            type: 'email'
            default: ''
            validate: {presence: {message: '^Please enter your email'}}
        }
        password: {
            type: 'password'
            default: ''
            validate: {presence: {message: '^Please enter your password'}}
        }
    })
