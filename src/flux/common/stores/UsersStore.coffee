_ = require 'underscore'
{Store} = require 'minimal-flux'
{gravatar, parseMoney} = require 'lib/utils'
flash = require 'lib/flash'
xhr = require 'lib/xhr'
config = require 'config'
{connAuth} = require 'lib/connection'
API = require 'flux/API'


class UsersStore extends Store
    constructor: ->
        @state =
            users: immutable.Map()
            currentUser: immutable.Map()
            bankData: immutable.Map()

        connAuth.subscribe("user-info", (user) => @updateCurrentUser(user))
        connAuth.subscribe("user-error", (text) -> flash({type: "error", text}))

        @handleAction "data.newUser", @setUser
        @handleAction "data.newGame", @handleNewGame
        @handleAction "data.setCurrentUser", @setCurrentUser
        @handleAction "data.setBankData", @setBankData
        @handleAction "userAccount.closeFirstTime", @handleCloseFirstTime
        @handleAction "userAccount.changeName", @editUser.bind(this, "name")
        @handleAction "userAccount.changeEmail", @editUser.bind(this, "email")
        @handleAction "userAccount.changeAddress", @editUser.bind(this, "address")
        @handleAction "userAccount.changeCountry", @editUser.bind(this, "country")
        @handleAction "userAccount.changeState", @editUser.bind(this, "state")
        @handleAction "userAccount.changeNickname", @editUser.bind(this, "nickname")
        @handleAction "userAccount.changeAnonymousStatus", @changeAnonymousStatus
        @handleAction "userAccount.changeAvatar", @editUser.bind(this, "avatar")
        @handleAction "userAccount.changeAnonymAvatar", @editUser.bind(this, "anonymous_avatar")
        @handleAction "userAccount.save", @saveUser
        @handleAction "userAccount.changeField", @editUser.bind(this)
        @handleAction "userAccount.changePassword", @editPassword
        @handleAction "userAccount.checkEmail", @checkEmail
        @handleAction "tap.sendResetLink", @sendResetLink
        @handleAction "tap.resetPassword", @resetPassword
        @handleAction "tap.linkAccounts", @linkAccounts
        @handleAction "payment.depositSuccess", @updateBalance
        @handleAction "tap.finishRegistration", @setCurrentUser

    setUser: (user) ->
        user.avatar = user.avatar or gravatar(user.email)
        @setState({
            users: @state.users.set(user.id, immutable.fromJS(user))
        })

        @emit('change:' + user.id)

    handleNewGame: (game)->
        @setUser(game['user_data']) if 'user_data' of game

    setCurrentUser: (user) ->
        user.avatar = user.avatar or gravatar(user.email)
        currentUser = @state.currentUser.merge(user)

        @setState({currentUser})

    updateCurrentUser: (user) ->
        currentUser = @state.currentUser.merge(user)

        @setState({currentUser})

    getUser: (userId) ->
        @state.users.get(userId, immutable.Map())

    getUsers: ->
        @state.users

    getAvatarUrls: ->
        [
            "/static/img/anonym1.png"
            "/static/img/anonym2.png"
            "/static/img/anonym_golfer2.png"
            "/static/img/anonym_male.png"
            "/static/img/anonym_female.png"
        ]

    getCurrentUser: ->
        @state.currentUser

    getUserAvatar: ->
        if @state.currentUser.get("anonymous")
            @state.currentUser.get("anonymous_avatar")
        else
            @state.currentUser.get("avatar") or gravatar(@state.currentUser.get("email"))

    getUserBalance: ->
        parseMoney(@state.currentUser.get('balance', []))

    getUserName: ->
        if @state.currentUser.get("anonymous")
            @state.currentUser.get("nickname")
        else
            @state.currentUser.get("name")

    getAnonymAvatarUrl: ->
        @state.currentUser.get("anonymous_avatar") || "/static/img/anonym_golfer.png"

    changeAnonymousStatus: (status) ->
        if not status or @state.currentUser.get("nickname")
            @editUser("anonymous", status)

    editUser: (field, value) ->
        @setState
            currentUser: @state.currentUser.set(field, value)

        @saveUser(field)

    editPassword: () ->
        oldPassword = @stores.forms.getValue("old_password")
        newPassword = @stores.forms.getValue("password")

        setTimeout =>
            xhr
                url: "/auth/password/change/"
                method: "post"
                data: JSON.stringify({
                    old_password: oldPassword
                    password: newPassword
                })
                responseType: "json"
                callback: (err, data) ->
                    if err
                        flux.actions.state.setError("CHANGE_PASSWORD", err)
                    else if data?.error
                        if _.isObject(data.error)
                            flux.actions.forms.setError(data.error.field, data.error.message)
                        else
                            flux.actions.state.setError("CHANGE_PASSWORD", data.error)
                    else
                        flux.actions.state.clearError("CHANGE_PASSWORD")

                    flux.actions.forms.stopProgress(["old_password", "password", "confirmPassword"])
        , 1

    saveUser: (field = "save_user") ->

        setTimeout =>
            xhr
                url: "/profile/update/"
                method: "post"
                data: JSON.stringify(@state.currentUser.toJS())
                responseType: "json"
                callback: (err, data) ->
                    if err?
                        if err == 0
                            flux.actions.forms.setError(field, "Connection problem")
                        else
                            flux.actions.forms.setError(field, err)
                    else if data?.error
                        flux.actions.forms.setError(field, data.error)
                    else
                        flux.actions.forms.setSuccess(field)
        , 1

    checkEmail: (email) ->
        setTimeout =>
            xhr
                url: "/auth/check-email/"
                method: "post"
                data: JSON.stringify({email})
                responseType: "json"
                callback: (err, data) ->
                    if data?.success
                        flux.actions.forms.setSuccess("signup_email")

                    if err || data?.error
                        flux.actions.forms.setError("signup_email", err || data.error)
        , 1

    handleCloseFirstTime: ->
        @setState({
            currentUser: @state.currentUser.set('first_login', false)
        })

    setBankData: (data) ->
        @setState bankData: immutable.fromJS(data)

    getBankData: ->
        @state.bankData

    sendResetLink: (email) ->
        setTimeout =>
            if email
                API.requestPasswordReset(email)
                    .then ->
                        flux.actions.state.passwordWasReset()
                    .catch (err) ->
                        flux.actions.state.passwordResetError(err)

        , 1

    resetPassword: ({email}) ->
        setTimeout =>
            API.requestPasswordReset(email)
                .then =>
                    flux.actions.state.openPasswordWasResetModalDialog()
                .catch  (error) =>
                    flux.actions.forms.setError("PASSWORD_RESET_ERROR", error)
        , 1

    linkAccounts: ({email, password}) ->
        setTimeout =>
            API.connectProfileToAnotherOne({email, password})
                .then =>
                    window.location.reload()
                .catch (error) =>
                    flux.actions.forms.setError("LINK_ACCOUNTS_ERROR", error)
        , 1

    updateBalance: (balance) ->
        @setState currentUser: @state.currentUser.set('balance', balance)

module.exports = UsersStore
