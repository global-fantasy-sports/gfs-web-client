_ = require "underscore"
{Store} = require "minimal-flux"


class FormStore extends Store
    constructor: ->
        @state =
            errors: immutable.Map()
            elements: immutable.Map()
            elementsInProgress: immutable.Set()
            successElements: immutable.Set()
            validationComplete: false

        @handleAction("forms.init", @init)
        @handleAction("forms.loadSaved", @handleLoadSaved)
        @handleAction("forms.startProgress", @startProgress)
        @handleAction("forms.stopProgress", @stopProgress)
        @handleAction("userAccount.changePassword", @startProgress.bind(this, ["old_password", "password", "confirmPassword"]))
        @handleAction("state.clearError", @checkSuccess)
        @handleAction("state.setError", @checkError)
        @handleAction("forms.setValue", @setValue)
        @handleAction("forms.setSuccess", @setSuccess)
        @handleAction("forms.setError", @setError)
        @handleAction("forms.validatePassword", @validatePassword)
        @handleAction("forms.validateSignup", @validateSignup)
        @handleAction("userAccount.changeAnonymousStatus", @validateNickname)

    init: ->
        @setState {
            elementsInProgress: immutable.Set()
            successElements: immutable.Set()
            elements: immutable.Map()
            errors: immutable.Map()
            validationComplete: false
        }

    handleLoadSaved: (data)->
        data or= {country: 'US'}
        @setState({elements: @state.elements.withMutations((obj)->
            obj.set("email", data.signup_email)
            obj.set("name", data.name)
            obj.set("country", data.country or 'US')
            obj.set("state", data.state)
            obj.set("password", null)
        )})

    validatePassword: ->
        isValidated = true

        if !@state.elements.get("old_password")
            isValidated = false
            @setError "old_password", "Enter your current password"
        else
            @clearError("old_password")

        if !@state.elements.get("password")
            isValidated = false
            @setError "password", "Enter new password"
        else
            @clearError("password")

        if @state.elements.get("password") != @state.elements.get("confirmPassword")
            isValidated = false
            @setError "confirmPassword", "Passwords are not hte same"
        else
            @clearError("confirmPassword")

        if isValidated
            @setState validationComplete: true

    validateSignup: ->
        validationComplete = true

        if @isError "signup_email"
            validationComplete = false

        if !@state.elements.get("signup_password")
            @setError "signup_password", "Please fill in your password"
            validationComplete = false
        else
            @clearError "signup_password"

        if !@state.elements.get("name")
            @setError "name", "Please fill in your name"
            validationComplete = false
        else
            @clearError "name"

        if !@state.elements.get("terms_and_conditions")
            @setError "terms_and_conditions", "Please accept Terms and Conditions"
            validationComplete = false
        else
            @clearError "terms_and_conditions"

        country = @state.elements.get("country")
        if country == "US" && !@state.elements.get("state")
            validationComplete = false
            @setError "state", "Please choose your state"
        else
            @clearError "state"

        @setState {validationComplete}

    validateNickname: (isAnonym) ->
        if isAnonym and not @state.elements.get("nickname")
            @setError("nickname", "Please enter nickname")

    checkSuccess: (code) ->
        if code == "CHANGE_PASSWORD"
            {successElements} = @state
            successElements = successElements.add(element) for element in ["password", "old_password"]
            @setState {successElements}

    checkError: (key, value) ->
        if key == "CHANGE_PASSWORD"
            @setState errors: @state.errors.set(key, value)

    removeErrors: (elements) ->
        {errors} = @state
        errors = errors.remove(element) for element in elements

        @setState {errors}

    removeSuccess: (elements) ->
        {successElements} = @state
        successElements = successElements.remove(element) for element in elements

        @setState {successElements}

    startProgress: (elements) ->
        if !_.isArray(elements) then elements = [elements]
        @removeErrors(elements)
        @removeSuccess(elements)

        {elementsInProgress} = @state
        elementsInProgress = elementsInProgress.add(element) for element in elements
        @setState {elementsInProgress, validationComplete: false}

    stopProgress: (elements) ->
        if !_.isArray(elements) then elements = [elements]
        {elementsInProgress} = @state
        elementsInProgress = elementsInProgress.remove(element) for element in elements
        @setState {elementsInProgress}

    isValidationComplete: ->
        @state.validationComplete

    isInProgress: (key) ->
        @state.elementsInProgress.contains(key)

    isSuccess: (key) ->
        @state.successElements.contains(key)

    isError: (key) ->
        @state.errors.has(key)

    setError: (key, value) ->
        @setState {
            errors: @state.errors.set(key, value)
            successElements: @state.successElements.remove(key)
            elementsInProgress: @state.elementsInProgress.remove(key)
        }

    getError: (key) ->
        @state.errors.get(key)

    clearError: (key) ->
        if key
            @setState {
                errors: @state.errors.remove(key)
            }
        else
            @setState errors: immutable.Map()

    setSuccess: (key) ->
        if @state.elements.get(key)
            @setState {
                errors: @state.errors.remove(key)
                successElements: @state.successElements.add(key)
                elementsInProgress: @state.elementsInProgress.remove(key)
            }
        else
            @setState {
                errors: @state.errors.remove(key)
                elementsInProgress: @state.elementsInProgress.remove(key)
            }

    getValue: (key, defaultValue) ->
        @state.elements.get(key, defaultValue)

    getValues: (defaults)->
        Object.assign({}, defaults, @state.elements.toObject())

    setValue: (key, value) ->
        @setState elements: @state.elements.set(key, value)


module.exports = FormStore
