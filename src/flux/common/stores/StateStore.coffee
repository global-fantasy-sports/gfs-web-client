{Store} = require "minimal-flux"
flash = require "lib/flash"


class StateStore extends Store
    constructor: ->
        @state = {
            elementsInProgress: immutable.Set()
            serverProgress: false
            togglers: immutable.Map()
            expandedScorecard: immutable.List() # contains game id and participant id
            fiveBallRoomState: immutable.Map()
            selectedRounds: immutable.Set()
            activeRoundNumber: 1
            activeGameId: null
            activeParticipantId: null
            gameType: ''
            dialog: null
            errors: immutable.Map()
            steps: immutable.List()
            mode: null
            menuOpen: false
            gamesFilter: 'all'
            tooltipId: null
            appState: null
            selectorPageType: 'nfl'
        }

        @handleAction("state.throw", @throw)
        @handleAction("state.showMessageBox", @showMessageBox)
        @handleAction("state.serverProgressSuccess", @serverProgressSuccess)
        @handleAction("tap.participantStatistics", @toggleParticipantStatistics)
        @handleAction("state.routeChanged", @changeRoute)
        @handleAction("tap.gameParticipant", @toggleScorecard)
        @handleAction("tap.gameParticipantFiveBall", @toggleFiveBallScorecard)
        @handleAction("state.expandParticipantFiveBall", @expandParticipant)
        @handleAction("tap.closeScorecard", @closeScorecard)
        @handleAction("tap.toggleRound", @roundSelection)
        @handleAction("tap.toggleActiveRound", @toggleActiveRound)
        @handleAction("state.clearActiveRounds", @clearActiveRounds)
        @handleAction("state.setError", @setError)
        @handleAction("state.clearError", @clearError)
        @handleAction("tap.scorecardFlag", @setActiveParticipant)
        @handleAction("tap.closeDialog", @closeDialog)
        @handleAction("tap.forgotPassword", @openDialog.bind(this, "FORGOT_PASSWORD"))
        @handleAction("userAccount.changePassword", @clearError.bind(this, "CHANGE_PASSWORD"))
        @handleAction("state.scrollToTop", @handleScrollToTop)
        @handleAction("tap.nextStep", @addStep)
        @handleAction("tap.stepBack", @popStep)
        @handleAction("tap.clearSteps", @clearSteps)
        @handleAction("tap.editGame", @handleEditGame)
        @handleAction("tap.newGame", @handleNewGame)
        @handleAction("state.clearMode", @clearMode)
        @handleAction("state.toggleSideMenu", @handleToggleSideMenu)
        @handleAction("state.setActiveGameId", @setActiveGameId)
        @handleAction("state.setActiveParticipantId", @setActiveParticipantId)
        @handleAction("tap.finishRegistration", @finishRegistration)
        @handleAction("state.setGamesFilter", @setGamesFilter)
        @handleAction("state.tooltipHovered", @setTooltipId)
        @handleAction("state.hideTooltips", @clearTooltipId)
        @handleAction("state.switchSelectorPage", @switchSelectorPage)

        try
            @handleAction("lineup.init", @setAppState.bind(this, "lineup"))
        catch e
            if process.env.NODE_ENV isnt 'production'
                console.warn('"lineup.init" action cannot be handled')

    throw: ->
        throw new Error("Intentional exception")

    getTooltipId: -> @state.tooltipId

    clearTooltipId: ->
        @setState {tooltipId: null}

    setTooltipId: (id) ->
        @setState {tooltipId: id}

    showMessageBox: (error) ->
        flash
            type: "error"
            text: error

    addStep: (step) ->
        @setState {
            steps: @state.steps.push(step)
        }

    popStep: ->
        @setState {
            steps: @state.steps.pop()
        }

    clearSteps: ->
        @setState steps: immutable.List()

    getLastStep: ->
        @state.steps.last()

    serverProgressSuccess: ->
        @stopServerProgress()

    isInProgress: ->
        @state.serverProgress

    toggleParticipantStatistics: (participantId) ->
        value = @state.togglers.get("participantStatistics")

        if value != participantId
            @setState togglers: @state.togglers.set("participantStatistics", participantId)
        else
            @setState togglers: @state.togglers.remove("participantStatistics")

    isExpandedStatistics: (participantId) ->
        @state.togglers.get("participantStatistics") == participantId

    toggleScorecard: (gameId, participantId) ->
        currentGameId = @state.expandedScorecard.first()
        currentParticipantId = @state.expandedScorecard.last()

        if currentGameId == gameId and currentParticipantId == participantId
            @setState
                expandedScorecard: immutable.List()
                activeGameId: null
        else
            @setState
                expandedScorecard: immutable.List([gameId, participantId])
                activeGameId: gameId

    setActiveParticipant: (participantId) ->
        @setState
            expandedScorecard: @state.expandedScorecard.set(1, participantId)

    expandParticipant: (gameId, participantId) ->
        currentState = @state.fiveBallRoomState.get(gameId, immutable.Map())
        @setState fiveBallRoomState: @state.fiveBallRoomState.set(gameId, currentState.merge({opened: participantId}))

    toggleFiveBallScorecard: (gameId, participantId) ->
        currentGameId = @state.expandedScorecard.first()
        currentParticipantId = @state.expandedScorecard.last()

        @expandParticipant(gameId, participantId)
        if currentGameId == gameId and currentParticipantId == participantId
            @setState expandedScorecard: immutable.List()
        else
            @setState expandedScorecard: immutable.List([gameId, participantId])

    closeScorecard: ->
        @setState expandedScorecard: immutable.List()

    closeDialog: ->
        @setState dialog: null

    changeRoute: (route) ->
        @setState {route}

    getRoute: ->
        @state.route

    getExpandedScorecardGameId: ->
        @state.expandedScorecard.first()

    getExpandedScorecardParticipantId: ->
        @state.expandedScorecard.get(1)

    getExpandedFiveBallParticipantId: (gameId) ->
        @state.fiveBallRoomState.getIn([gameId, 'opened'])

    roundSelection: (roundNum) ->
        if @state.selectedRounds.has(roundNum)
            newState = @state.selectedRounds.remove(roundNum)
            @setState({selectedRounds: newState})
        else if @state.selectedRounds.size < 2
            newState = @state.selectedRounds.add(roundNum)
            @setState({selectedRounds: newState})

    getActiveRounds: () ->
        @state.selectedRounds

    toggleActiveRound: (roundNumber) ->
        @setState {activeRoundNumber: roundNumber}

    getActiveRoundNumber: ->
        @state.activeRoundNumber

    clearActiveRounds: ->
        @setState({selectedRounds: @state.selectedRounds.clear()})

    getActiveGameId: ->
        @state.activeGameId

    getActiveGame: ->
        @stores.games.getGame(@getActiveGameId()) || {}

    getOpenDialogCode: ->
        @state.dialog

    setError: (key, error) ->
        @setState
            errors: @state.errors.set(key, error)

    clearError: (key) ->
        @setState
            errors: @state.errors.remove(key)

    getError: (key) ->
        @state.errors.get(key)

    openDialog: (key) ->
        @setState dialog: key

    handleScrollToTop: ({el, offset, duration})->
        @emit('scroll:to', {el, offset, duration})

    handleEditGame: (game) ->
        @setMode('EDIT')
        @setAppState('wizard')

    handleNewGame: ->
        @setMode('CREATE')
        @setAppState('wizard')

    setMode: (mode) ->
        @setState {mode}

    getMode: ->
        @state.mode

    clearMode: ->
        @setState mode: null

    handleToggleSideMenu: (menuOpen)->
        @setState({menuOpen})

    getMenuState: ->
        @state.menuOpen

    setActiveGameId: (id) ->
        @setState activeGameId: id

    setActiveParticipantId: (id) ->
        @setState activeParticipantId: id

    getActiveParticipantId: -> @state.activeParticipantId

    isRoundAvailable: (n) ->
        activeRounds = @getActiveRounds()
        selectedTournamentId = @stores.wizard.getTournamentId()
        selectedTournament = @stores.tournaments.getTournament(selectedTournamentId)

        isRoundAvailable = false
        rounds = selectedTournament.rounds

        if not rounds.getIn([3, "started"], true)
            if activeRounds.size == 0
                isRoundAvailable = true
            else if activeRounds.size == 1
                firstSelectedRound = activeRounds.first()
                if n == firstSelectedRound + 1 or n == firstSelectedRound - 1 or n == firstSelectedRound
                    isRoundAvailable = true
            else if activeRounds.includes(n)
                isRoundAvailable = true

            isRoundAvailable = isRoundAvailable and not rounds.getIn([n, 'started'], false)

        isRoundAvailable

    finishRegistration: ->
        @clearError("FINISH_REGISTRATION")
        @closeDialog()

    setGamesFilter: (filter) ->
        @setState gamesFilter: filter

    getGamesFilter: -> @state.gamesFilter

    setAppState: (appState) ->
        @setState {appState}

    getAppState: -> @state.appState

    switchSelectorPage: (gameType) ->
        @setState selectorPageType: gameType

    getSelectorPageType: -> @state.selectorPageType


module.exports = StateStore
