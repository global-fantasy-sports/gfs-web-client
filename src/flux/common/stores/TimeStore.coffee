moment = require('moment')
{Store} = require('minimal-flux')


class TimeStore extends Store

    constructor: ->
        @state = {
            date: new Date()
            week: [0, 0]
            started: false
            locked: false
            fake: false
            shownSeason: null
        }

        @handleAction('time.start', @handleTimeStart)
        @handleAction('time.stop', @handleTimeStop)
        @handleAction('time.tick', @handleTimeTick)
        @handleAction('time.setCurrentDate', @handleSetCurrentDate)
        @handleAction('time.resetLockChanged', @handleResetLockChanged)
        @handleAction('time.setShownSeason', @handleSetShownSeason)

    getNow: ->
        new Date(@state.date)

    getDiffToNow: (date) ->
        moment(date).diff(@state.date)

    getSeason: () ->
        @state.week[1]

    getShownSeason: ->
        @state.shownSeason ? @state.week[1]

    isRunning: () ->
        @state.started

    isFake: () ->
        @state.fake

    isLocked: () ->
        @state.locked

    handleTimeStart: ({date, week, fake, locked}) ->
        moment.setServerDate(if fake then date else false)
        week ?= @state.week
        @setState({
            fake,
            locked: locked ? @state.locked
            date: moment().toDate()
            week: week
            shownSeason: if week[0] != 1 then week[1] else week[1] - 1
            started: true
        })

    handleTimeStop: ->
        @setState({started: false})

    handleTimeTick: (date) ->
        @setState({date})

    handleSetCurrentDate: (timestamp) ->
        serverDate = moment.setServerDate(timestamp)
        @setState({date: serverDate.toDate()})

    handleResetLockChanged: (locked) ->
        @setState({locked})

    handleSetShownSeason: (shownSeason) ->
        @setState({shownSeason})


module.exports = TimeStore
