{Actions} = require "minimal-flux"


class StateActions extends Actions

    startServerProgress: ->
        @dispatch("startServerProgress")

    stopServerProgress: ->
        @dispatch("stopServerProgress")

    showMessageBox: (message) ->
        @dispatch("showMessageBox", message)

    setError: (key, value) ->
        @dispatch("setError", key, value)

    clearError: (key) ->
        @dispatch("clearError", key)

    serverProgressSuccess: ->
        @dispatch("serverProgressSuccess")

    routeChanged: (route) ->
        @dispatch("routeChanged", route)

    expandParticipantFiveBall: (gameId, participantId) ->
        @dispatch("expandParticipantFiveBall", gameId, participantId)

    clearActiveRounds: ->
        @dispatch("clearActiveRounds")

    scrollToTop: (options)->
        @dispatch('scrollToTop', options)

    clearMode: ->
        @dispatch "clearMode"

    toggleSideMenu: (open)->
        open ?= not @stores.state.getState().menuOpen
        @dispatch('toggleSideMenu', open)

    openResetPasswordDialog: ->
        @dispatch "openResetPasswordDialog"

    passwordWasReset: ->
        @dispatch "passwordWasReset"

    openPasswordWasResetModalDialog: ->
        @dispatch "openPasswordWasResetModalDialog"

    passwordResetError: (err) ->
        @dispatch "passwordResetError", err

    openFinishRegistrationDialog: ->
        @dispatch "openFinishRegistrationDialog"

    openAccountExistsDialog: ->
        @dispatch "openAccountExistsDialog"

    openNflRulesDialog: ->
        @dispatch('openNflRulesDialog')

    throw: ->
        @dispatch "throw"

    setActiveGameId: (id) ->
        @dispatch "setActiveGameId", id
        @setActiveParticipantId(@stores.games.getGame(id).participantId1)

    setActiveParticipantId: (id) ->
        @dispatch "setActiveParticipantId", id

    openInfoMessage: (message) ->
        @dispatch "openInfoMessage", message

    setGamesFilter: (filter) ->
        @dispatch "setGamesFilter", filter

    selectParticipant: (participant) ->
        @dispatch "selectParticipant", participant

    tooltipHovered: (id) ->
        @dispatch "tooltipHovered", id

    hideTooltips: ->
        @dispatch "hideTooltips"

    openPlayerDetailsDialog: (id) ->
        @dispatch('openPlayerDetailsDialog', id)

    switchSelectorPage: (gameType) ->
        @dispatch 'switchSelectorPage', gameType

    openEFFDialog: () ->
        @dispatch('openEFFDialog')

    openDeleteLineupDialog: (lineup) ->
        @dispatch('openDeleteLineupDialog', lineup)

    openSaveLineupDialog: (lineup) ->
        @dispatch('openSaveLineupDialog', lineup)

    openLineupSavedDialog: (lineup) ->
        @dispatch('openLineupSavedDialog', lineup)

module.exports = StateActions
