_ = require 'underscore'
{Actions} = require 'minimal-flux'

{history} = require 'world'
flash = require 'lib/flash'
{getCookie} = require 'lib/utils'
xhr = require 'lib/xhr'


AMOUNT_ERROR = 'Please select an amount to deposit'


depositForm = _.template(
    '<form action="/<%= SPORT %>/payment/create/" method="post" id="deposit_form_auto">' +
    '    <input type="hidden" name="_xsrf" value="<%= xsrf %>" />' +
    '    <input type="hidden" name="amount" value="<%= amount %>" />' +
    '    <input type="hidden" name="sport" value="<%= SPORT %>" />' +
    '</form>'
)


makeDepositRequest = (data)->
    xhr.post('/payment/create/', data).then(({error, url})->
        if url then window.location = url
        else flash({type: 'error', text: error})
    )


class PaymentActions extends Actions

    fetchData: ->
        @actions.data.receiveBalance()
        @actions.data.receiveBankData()
        @actions.data.loadPaymentHistory()

    deposit: (amount)->
        if not amount
            return flash({type: 'error', text: AMOUNT_ERROR})

        @dispatch('deposit', amount)

        if @stores.payments.isVerified()
            request = Object.assign(@stores.payments.getBankData(), {amount})
            makeDepositRequest(request)
        else
            @requireVerification()

    requireVerification: (extraQuestions)->
        @dispatch('requireVerification', extraQuestions)
        history.pushState(null, "/#{SPORT}/deposit/verification")

    verify: (form)->
        return if @stores.payments.isBusy()

        form.validate().then(=>
            @dispatch('verify')

            xhr(
                url: '/idology/'
                method: 'post'
                data: JSON.stringify(form.toJSON())
                responseType: 'json'
                callback: (err, response)=>
                    if err
                        flash({type: "error", text: err})
                        @verifyFail(err)
                    if response.error == 'Data is not valid'
                        text = _.map(response.errors, (error, field) ->
                            "#{field}: #{error}"
                        )
                        flash({type: "error", text: text})
                        @verifyFail(response.error)
                    else if response.success is false
                        @verifyFail(response.message)
                    else if response.success is true
                        @verifySuccess(response)
                    else if response.questions
                        @requireVerification({questions: response.questions})
            )
        )

    verifyFail: (errorMessage)->
        @dispatch('verifyFail', errorMessage)

    verifySuccess: (response)->
        @actions.data.setCurrentUser({verified: true})
        @dispatch('verifySuccess', response)
        @deposit(@stores.payments.getState().depositRequest)

    processDepositResponse: (query)->
        @dispatch('processDepositResponse', query)

        if 'success' of query then @depositSuccess(query)
        else @depositFail(query)

    depositSuccess: (query)->
        xhr.post('/payment/redirect/success/', {id: query.id, status: 'success'})
            .then((response)=>
                if response.error then throw new Error(response.error)
                else @dispatch('depositSuccess', response.balance)
                @actions.data.loadPaymentHistory()
            )
            .catch((error)=>
                @depositFail(error)
            )

    depositFail: (query)->
        text =
            if query instanceof Error
                query.message
            else if typeof query is 'string'
                query
            else if 'timeout' of query
                'Operation timed out'
            else if 'declined' of query
                'Transaction declined'
            else if 'error' of query
                "Transaction error: '#{query['transaction.errorMessage']}'"
            else
                'Something went wrong'

        flash({type: 'error', text})

        @dispatch('depositFail', text)

    withdraw: (form)->
        return if @stores.payments.isBusy()
        form.validate().then(=>
            @dispatch('withdraw')
            xhr.post('/payment/withdraw/', form.toJSON())
                .then((response)=>
                    if response.success then @withdrawSuccess(response)
                    else throw new Error(response.error)
                )
                .catch(@withdrawFail)
        )

    withdrawSuccess: (data)->
        @actions.data.setBalance(data)
        @actions.data.loadPaymentHistory()
        @actions.data.receiveBankData()
        @dispatch('withdrawSuccess', data)

    withdrawFail: (error)->
        message = if error instanceof Error then error.message else error
        flash({type: 'error', message})
        @dispatch('withdrawFail', message)


module.exports = PaymentActions
