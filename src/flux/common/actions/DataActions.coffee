_ = require('underscore')
{Actions} = require('minimal-flux')

xhr = require('../../../lib/xhr')
flash = require('../../../lib/flash')
{conn} = require('../../../lib/connection')
{channels} = require('../../../lib/const')
API = require('../../API')


class DataActions extends Actions
    constructor: ->
        conn.subscribe('competition-room-changed', (data) =>
            updateRooms = []
            process = (room) =>
                if @stores.rooms.getState().rooms.has(room.id)
                    updateRooms.push(room)
                    if room.games
                        @fetchGamesByRoomId(room.id)
                else
                    @fetchRoom(room.id)

            if _(data).isArray()
                for room in data
                    process(room)
            else
                process(data)
            @updateCompetitionRooms(updateRooms)
        )

        conn.subscribe('strokes', (data) =>
            @newParticipantRoundResult(data)
        )

        conn.subscribe('participant', (data) =>
            if data.participant_round_results
                @newParticipantRoundResult(data.participant_round_results[0])
                @newParticipantRoundResult(data.participant_round_results[1])
                @newParticipantRoundResult(data.participant_round_results[2])
                @newParticipantRoundResult(data.participant_round_results[3])
            @newParticipant(data)
        )

        conn.subscribe('tournament-changed', (data) =>
            @updateTournament(data)
        )

        conn.subscribe('games-changed', (data) =>
            @updateGames(data)
        )

        conn.subscribe('fppg-changed', (data) =>
            @updateFPPG(data)
        )

        if SPORT == 'nfl'
            conn.subscribe('new-week', (data) =>
                @initialData(data)
            )

            conn.subscribe('contest-changed', (data) =>
                @updateContest(data)
            )

    initialData: (data)->
        @dispatch('initialData', data)

    updateContest: (data) ->
        @dispatch('updateContest', data)

    newChallengeRequest: (data) ->
        logger.log data, "new challenge request", "DataActions"
        @dispatch "newChallengeRequest", data

    newGame: (game, type) ->
        logger.log game, "newGame", "DataActions"
        @dispatch "newGame", game, type
        if not @stores.rooms.getRoom(game.competitionRoomId).id
            @fetchRoom(game.competitionRoomId)

    removeGameFromStore: (gameId) ->
        @dispatch "removeGameFromStore", gameId

    removeGame: (gameId) ->
        logger.log gameId, "removeGame", "DataActions"
        conn.send('delete', channels.DELETE_GAME_CHANNEL, {id: gameId}, =>
            @actions.tap.closeDialog()
            @removeGameFromStore(gameId)
        )

    updateGames: (data) ->
        @dispatch('updateGames', data)

    calculatePointsAndPlace: (game) ->
        @dispatch "calculatePointsAndPlace", game

    setCompetitionRoom: (room) ->
        logger.log room, "setCompetitionRoom", "DataActions"
        @dispatch "setCompetitionRoom", room

    updateCompetitionRoom: (room) ->
        logger.log room, "updateCompetitionRoom", "DataActions"
        @dispatch "updateCompetitionRoom", room

    updateCompetitionRooms: (rooms) ->
        @dispatch "updateCompetitionRooms", rooms

    addGameToCompetitionRoom: (gameId) ->
        logger.log gameId, "addGameToCompetitionRoom", "DataActions"
        @dispatch "addGameToCompetitionRoom", gameId
    setTournament: (tournament) ->
        logger.log tournament, "setTournament", "DataActions"
        @dispatch "setTournament", tournament

    updateTournament: (tournament) ->
        logger.log tournament, "updateTournament", "DataActions"
        @dispatch "updateTournament", tournament

    newUser: (user) ->
        logger.log user, "newUser", "DataActions"
        @dispatch "newUser", user

    newParticipant: (participant) ->
        logger.log participant, "newParticipant", "DataActions"
        @dispatch "newParticipant", participant
        # FIXME: load stats when needed
        #@fetchParticipantStats(participant)

    newParticipantRoundResult: (result) ->
        logger.log result, "newParticipantRoundResult", "DataActions"
        @dispatch "newParticipantRoundResult", result

    setCurrentUser: (user) ->
        @dispatch "setCurrentUser", user

    updateGame: (data) ->
        logger.log data, "updateGame", "DataActions"
        @dispatch "updateGame", data

    uploadAvatar: (avatar) ->
        API.uploadFile(
            url: "/media/upload/"
            file: avatar
        )
        .then(
            ((data) =>
                @actions.userAccount.changeAvatar("/media/#{data.filename}")
            ),
            ((err, data) =>
                logger.log err, "err", "Upload"
                if err == 413
                    @actions.state.openInfoMessage("Image is too large")
                else if err != 404
                    @actions.state.openInfoMessage(data?.error or err)
            )
        )

    newChallenge: (challenge) ->
        logger.log challenge, "newChallenge", "DataActions"
        @dispatch "newChallenge", challenge

    loadPaymentHistory: ->
        logger.log "loadPaymentHistory", "DataActions"
        xhr(
            url: '/payment/history/'
            responseType: 'json'
            method: 'get'
            callback: (err, data) =>
                if err or data?.error
                    flash(type: 'error', text: data?.error or err)
                    return
                @setHistory(data)
        )

    setHistory: (data) ->
        logger.log data, "setHistory", "DataActions"
        @dispatch("setHistory", data)

    sendWithdrawRequest: (data) ->
        logger.log data, "sendWithdrawRequest", "DataActions"
        if @stores.payments.getWithdrawState() != 'inprogress'
            @setWithdrawState('inprogress')
            xhr(
                url: '/payment/withdraw/'
                data: JSON.stringify(data)
                method: 'post'
                responseType: 'json'
                callback: (err, data) =>
                    @setWithdrawState('norequests')
                    if err or data?.error
                        flash(type: 'error', text: data?.error or err)
                        return
                    @loadPaymentHistory()
                    @setBalance(data)
                    @receiveBankData()
            )


    setWithdrawState: (status) ->
        logger.log status, "setWithdrawState", "DataActions"
        @dispatch("setWithdrawState", status)

    receiveBalance: ->
        logger.log "receiveBalance", "DataActions"
        xhr(
            url: '/payment/balance/'
            method: 'get'
            responseType: 'json'
            callback: (err, data) =>
                if err or data?.error
                    flash(type: 'error', text: data?.error or err)
                    return
                @setBalance(data)
        )


    setBalance: (balance) ->
        logger.log balance, "setBalance", "DataActions"
        @dispatch("setBalance", balance)

    receiveBankData: ->
        logger.log "receiveBankData", "DataActions"
        xhr(
            url: '/payment/bankdata/'
            method: 'get'
            responseType: 'json'
            callback: (err, data) =>
                if err or data?.error
                    flash(type: 'error', text: data?.error or err)
                    return
                @setBankData(data)
        )

    setBankData: (data) ->
        logger.log data, "setBankData", "DataActions"
        @dispatch("setBankData", data)

    newRound: (round) ->
        logger.log round, "newRound", "DataActions"
        @dispatch("newRound", round)

    fetchGamesByRoomId: (roomId) ->
        logger.log(roomId, "fetchGamesByRoomId", "DataActions")
        room = @stores.rooms.getRoom(roomId)
        return unless room.id
        conn.send('get', 'games', {competitionRoomId: roomId}, (err, response) =>
            if err
                logger.log(err, 'games fetching error', "DataActions")
                return

            {games, participants, teams} = response
            if games
                if SPORT == 'golf'
                    _.map(games, (game) => @newGame(game, room.gameType))
                else
                    @newGames(games)

            if participants or teams
                @initialData({participants, nflDefenseTeams: teams})

            return
        )

    fetchRoom: (roomId) ->
        logger.log roomId, "fetchRoom", "DataActions"
        if not @stores.rooms.getRoom(roomId).id
            conn.send('get', 'competition-room', {'id': roomId},
                (err, room)=>
                    if err
                        logger.log(err, 'competition room fetching error', "DataActions")
                    if room
                        @dispatch('setCompetitionRoom', room)
            )

    fetchParticipant: (id) ->
        logger.log id, "fetchParticipant", "DataActions"
        conn.send("get", channels.PARTICIPANTS.get, {id}, (err, participant) ->
            if err
                logger.log err, "participant fetch error, id is #{id}", "DataActions"

            if participant
                @dispatch "newParticipant", participant
        )

    fetchParticipants: ->
        logger.log "fetchParticipants", "DataActions"
        conn.send("get", channels.PARTICIPANTS.collection.get, {}, (err, participants) =>
            return if err
            for participant in participants
                @dispatch "newParticipant", participant
        )

    fetchParticipantStats: (participant)->
        id = participant.golfer_id
        conn.send('get', channels.STATISTICS.get, {golfer_id: id}, (err, response)=>
            @dispatch('fetchParticipantStats', id, response)
        )

    fetchLineups: ->
        logger.log 'fetchLineups', 'DataActions'
        conn.send('get', channels.LINEUPS.get, {}, (err, lineups) =>
            if err then @actions.state.openInfoMessage(err)
            return unless lineups.length

            for lineup in lineups
                @actions.lineup.newLineup(lineup)
        )

    clearParticipants: ->
        @dispatch "clearParticipants"

    setChallengeRequests: (data) ->
        @dispatch 'setChallengeRequests', data

    newContest: (contest) ->
        @dispatch 'newContest', contest

    toggleFavorite: (id) ->
        player = @stores.participants.getPlayer(id)
        if player?.isFavorite
            @deleteFavorite(id)
        else
            @addFavorite(id)

    addFavorite: (playerId) ->
        logger.log 'addFavorite', 'DataActions'
        conn.send('set', channels.FAVORITES.set, {playerId}, (err, message) =>
            if err then @actions.state.openInfoMessage(err)

            if message is 'OK'
                @dispatch 'addFavorite', playerId
        )

    deleteFavorite: (playerId) ->
        logger.log 'addFavorite', 'DataActions'
        conn.send('delete', channels.FAVORITES.remove, {playerId}, (err, message) =>
            if err then @actions.state.openInfoMessage(err)

            if message is 'OK'
                @dispatch 'deleteFavorite', playerId
        )

    updateFPPG: (data) ->
        @dispatch 'updateFPPG', data

    newGames: (games) ->
        @dispatch('newGames', games)

    loadSelectorRooms: (sport, data) ->
        @dispatch('loadSelectorRooms', sport, data)

    loadSelectorOverachievers: (sport, data) ->
        @dispatch('loadSelectorOverachievers', sport, data)

    setNextCompetitionStartTime: (sport, time) ->
        @dispatch('setNextCompetitionStartTime', sport, time)

module.exports = DataActions
