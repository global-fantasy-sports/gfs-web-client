{Actions} = require('minimal-flux')

class SearchActions extends Actions

    setName: (name) ->
        @dispatch('setName', name)

    setCountry: (country) ->
        @dispatch('setCountry', country)

    setOrderName: (orderName) ->
        @dispatch('setOrderName', orderName)

    toggleSortOrder: (sortOrder) ->
        @dispatch('toggleSortOrder', sortOrder)

    toggleRoomType: ->
        @dispatch('toggleRoomType')

    setGameTypeFilter: (gameType) ->
        @dispatch('setGameTypeFilter', gameType)

    setSortingField: (fieldName) ->
        @dispatch('setSortingField', fieldName)

    setEntryFeeInterval: (valueStart, valueEnd) ->
        @dispatch("setEntryFeeInterval", valueStart, valueEnd)

    toggleSortingDirection: ->
        @dispatch('toggleSortingDirection')

    toggleBeginners: ->
        @dispatch('toggleBeginners')

    toggleFeatured: ->
        @dispatch('toggleFeatured')

    toggleGuaranteed: ->
        @dispatch('toggleGuaranteed')

    toggleGameTypeFilter: (gameType) ->
        @dispatch('toggleGameTypeFilter', gameType)

    toggleRoomTypeFilter: (roomType) ->
        @dispatch('toggleRoomTypeFilter', roomType)

    setPaymentTypeFilter: (payment) ->
        @dispatch('setPaymentTypeFilter', payment)

    extendNumberToShow: ->
        @dispatch('extendNumberToShow')

    setStartDateFilter: (value) ->
        @dispatch('setStartDateFilter', value)


module.exports = SearchActions
