{Actions} = require('minimal-flux')
moment = require('moment')

flash = require('../../../lib/flash')
{conn} = require('../../../lib/connection')


class TimeActions extends Actions

    constructor: () ->
        super()

        conn.subscribe("current-date", (data) =>
            return if not data.current_date
            @actions.time.setCurrentDate(data.current_date)
            @actions.connection.processed()
        )

        conn.subscribe('fake-tour', () ->
            flash({
                text: 'Tournament has been restarted'
                alwaysKeep: true
            })

            setTimeout(() ->
                window.location.reload()
            , 3000)
        )

        conn.subscribe("#{SPORT}-restart-locking", (data) =>
            @resetLockChanged(data)
        )

    start: ({date, week, fake, locked}) ->
        @dispatch('start', {date, week, fake, locked})
        @tick()

    stop: () ->
        @dispatch('stop')

    tick: () ->
        clearTimeout(@_timeout)

        @dispatch('tick', moment().toDate())

        if @stores.time.isRunning()
            @_timeout = setTimeout(@tick, 1000)

    addInterval: (hours) ->
        now = moment(@stores.time.getNow())
        payload = {current_date: now.add(hours, 'hours').unix()}
        conn.send('set', 'set-current-date', payload, (err, data) =>
            if err
                @actions.state.openInfoMessage(err)
            else
                @setCurrentDate(data.current_date)
        )

    setCurrentDate: (timestamp) ->
        @dispatch('setCurrentDate', timestamp)

    restartFakeTour: () ->
        unless @stores.time.isLocked()
            @actions.connection.setWaitingState()
            conn.send('set', 'restart-fake-tournament', {})

    setAtKickOff: () ->
        unless @stores.time.isLocked()
            @actions.connection.setProcessingState('Setting time to the kick-off')
            conn.send('set', 'fake-set-at-kick-off', {})

    setAtMidGame: () ->
        unless @stores.time.isLocked()
            @actions.connection.setProcessingState('Setting time to the middle of game')
            conn.send('set', 'fake-set-at-mid-game', {})

    setAtResults: () ->
        unless @stores.time.isLocked()
            @actions.connection.setProcessingState('Setting time when game is finished')
            conn.send('set', 'fake-set-at-results', {})

    toggleResetLock: ->
        locked = @stores.time.isLocked()
        if not locked
            conn.send('set', 'lock-restarting', {})
        else
            conn.send('delete', 'unlock-restarting', {})

        @resetLockChanged(not locked)

    resetLockChanged: (value) ->
        @dispatch('resetLockChanged', value)

    setShownSeason: (season) ->
        @dispatch('setShownSeason', season)


module.exports = TimeActions
