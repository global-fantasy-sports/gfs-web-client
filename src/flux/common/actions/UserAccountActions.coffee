{Actions} = require "minimal-flux"


class UserAccountActions extends Actions

    closeFirstTime: ->
        @dispatch("closeFirstTime")

    save: ->
        @dispatch "save"

    changeName: (name) ->
        @dispatch "changeName", name

    changeEmail: (email) ->
        @dispatch "changeEmail", email

    changeAddress: (address) ->
        @dispatch "changeAddress", address

    changeCountry: (country) ->
        @dispatch "changeCountry", country

    changeState: (state) ->
        @dispatch "changeState", state

    changeNickname: (nickname) ->
        @dispatch "changeNickname", nickname

    changeAnonymousStatus: (status) ->
        @dispatch "changeAnonymousStatus", status

    changeSubscriptionOption: (option, value) ->
        @dispatch "changeSubscriptionOption", option, value

    changeAvatar: (path) ->
        @dispatch "changeAvatar", path

    changeField: (fieldName, value) ->
        @dispatch "changeField", fieldName, value

    changePassword: (password, newPassword) ->
        @dispatch "changePassword", password, newPassword

    changeAnonymAvatar: (url) ->
        @dispatch "changeAnonymAvatar", url

    checkEmail: (email) ->
        @dispatch "checkEmail", email

module.exports = UserAccountActions
