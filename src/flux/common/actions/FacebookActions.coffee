_ = require('underscore')
{Actions} = require('minimal-flux')

{connAuth} = require('../../../lib/connection')


extractError = (error) ->
    if error.code is 2500
        'not_logged_in'
    else
        error.message


API = {
    initClient: (appId) -> new Promise((resolve, reject) ->
        window.fbAsyncInit = (args...) =>
            FB.init({
                appId: appId
                xfbml: false
                version: 'v2.7'
                status: true
                frictionlessRequests: true
            })

            resolve(FB)

        if not document.getElementById('facebook-sdk')
            el = document.createElement('script')
            el.id = 'facebook-sdk'
            el.src = '//connect.facebook.net/en_US/all.js'
            el.onerror = reject
            document.head.appendChild(el)
    )

    checkLogin: -> new Promise((resolve, reject) ->
        FB.getLoginStatus((response) ->
            switch response.status
                when 'connected' then resolve(response)
                when 'not_authorized' then reject(new Error('not_authorized'))
                else reject(new Error('not_logged_in'))
        )
    )

    login: -> new Promise((resolve, reject) ->
        callback = ({status, authResponse}) =>
            if status is 'connected' then resolve(authResponse)
            else reject(new Error('not_logged_in'))

        FB.login(callback, {
            scope: 'email,user_friends'
            return_scopes: true
        })
    )

    logout: -> new Promise((resolve) ->
        FB.logout(() -> resolve())
    )

    checkPermissions: -> new Promise((resolve, reject) ->
        FB.api('/me/permissions', ({data, paging, error}) ->
            if error
                return reject(new Error(extractError(error)))

            permission = _.findWhere(data, {permission: 'user_friends'})

            if permission?.status isnt 'granted'
                reject(new Error('declined'))
            else
                resolve()
        )
    )

    requestPermissions: -> new Promise((resolve) ->
        FB.ui({
            method: 'permissions.request'
            perms: 'user_friends'
            display: 'iframe'
        }, resolve)
    )

    getInvitableFriends: -> new Promise((resolve, reject) ->
        FB.api('/me/invitable_friends?fields=id,name,picture.width(80).height(80)&limit=5000', ({data, error}) ->
            if error then reject(new Error(extractError(error)))
            else resolve(data)
        )
    )

    getAppFriends: -> new Promise((resolve, reject) ->
        FB.api('/me/friends?fields=id,name,picture.width(80).height(80)&limit=5000', ({data, error}) ->
            if error then reject(new Error(extractError(error)))
            else resolve(data)
        )
    )

    getFriends: ->
        Promise.all([API.getAppFriends(), API.getInvitableFriends()])
            .then(([app, fb]) -> [app..., fb...])

    invite: (id) -> new Promise((resolve, reject) =>
        FB.ui({
            method: 'apprequests'
            to: [id]
            message: 'Try to beat me'
            data: {invited: true}
        }, (response) =>
            #== this code removes blank dialog box, staying after invite window is closed
            nodes = document.getElementsByClassName('fb_dialog_mobile loading')
            if nodes && nodes.length
                nodes[0].remove()
            #==
            if response.error_code
                reject(new Error(response.error_code))
            else
                resolve()
        )
    )

}


class FacebookActions extends Actions

    init: (appId) ->
        @dispatch('init', appId)
        API.initClient(appId).then(@initSuccess, @initError)

    initSuccess: (FB) ->
        @dispatch('initSuccess', FB)

    initError: (error) ->
        @dispatch('initError', error)

    login: ->
        @dispatch('login')
        API.checkLogin().catch(API.login)
            .catch(@loginSuccess, @loginFail)
            .then(API.getFriends)
            .then(@fetchFriendsSuccess, @fetchFriendsFail)
            .catch(@requestError)

    loginSuccess: (authResponse) ->
        @dispatch('loginSuccess', authResponse)

    loginFail: ->
        @dispatch('loginFail')

    logout: ->
        API.logout().then(() => @dispatch('logout'))

    fetchFriends: (options = {}) ->
        return unless @stores.facebook.isIdle()

        @dispatch('fetchFriends')

        API.checkLogin()
            .then(API.checkPermissions)
            .then(API.getFriends)
            .then(@fetchFriendsSuccess, @fetchFriendsFail)
            .catch(@requestError)

    fetchFriendsSuccess: (friends) ->
        @dispatch('fetchFriendsSuccess', friends)
        @checkRegisteredFriends()

    fetchFriendsFail: (error) ->
        @dispatch('fetchFriendsFail', error)

    checkRegisteredFriends: ->
        {friends} = @stores.facebook.getState()
        data = {friends_ids: Array.from(friends.keys())}
        connAuth.send('get', 'friends-registered', data, (error, data) =>
            {registered_friends} = data if data
            if error
                @checkRegisteredFriendsFail(new Error(error))
            else
                @dispatch('checkRegisteredFriends', registered_friends)
        )

    checkRegisteredFriendsFail: (error) ->
        @dispatch('checkRegisteredFriendsFail', error)

    invite: (friendId) ->
        API.invite(friendId).then(() =>
            @dispatch('invite', friendId)
            @actions.wizard.addFriendToChallengeList(friendId)
        )

    requestError: (error) ->
        #TODO: check error type
        @dispatch('requestError', error)

    search: (query) ->
        @dispatch('search', query or '')

    filter: (query) ->
        unless query in ['registered', 'notRegistered']
            query = null
        @dispatch('filter', query)

    sort: (fieldName) ->
        unless fieldName in ['name', 'registered']
            fieldName = 'name'
        {sorting, reversed} = @stores.facebook.getState()
        @dispatch('sort', {
            sorting: fieldName
            reversed: if sorting is fieldName then not reversed else false
        })

module.exports = FacebookActions
