{Actions} = require('minimal-flux')

errors = require('lib/errors')
{conn, connAuth} = require('lib/connection')
flash = require('lib/flash')
{eventTracker} = require('lib/utils')
{history} = require('world')


class WizardActions extends Actions

    init: (game) ->
        @dispatch("init", game)

    finish: ->
        @dispatch("init", {})
        history.replaceState(null, "/#{SPORT}/games/")

    setSubstepView: (view) ->
        @dispatch("setSubstepView", view)

    unlockPrediction: ->
        @dispatch("unlockPrediction")

    setPrediction: (index, prediction) ->
        @dispatch("setPrediction", index, prediction)

    setBonus: (participantId, bonus) ->
        @dispatch("setBonus", participantId, bonus)

    setCurrentPrediction: (prediction) ->
        @dispatch("setCurrentPrediction", prediction)

    setCurrentSubstep: (index) ->
        @dispatch("setCurrentSubstep", index)

    setWizardGameType: (gameType) ->
        @dispatch("setWizardGameType", gameType)

    setGameModel: (gameModel) ->
        @dispatch("setGameModel", gameModel)

    unselectParticipants: ->
        @dispatch("unselectParticipants")

    selectParticipant: (id) ->
        @dispatch("selectParticipant", id)

    toggleHandicap: (n) ->
        @dispatch("toggleHandicap", n)

    setCompetitionRoomId: (competitionRoomId, options) ->
        if @stores.wizard.getTournamentId()
            room = @stores.rooms.getRoom(competitionRoomId)
            if room.tournamentId != @stores.wizard.getTournamentId()
                return
        @dispatch("setCompetitionRoomId", competitionRoomId, options)

    setEvent: (tournamentId, roundNumber) ->
        @dispatch("setEvent", tournamentId, roundNumber)

    setTournamentId: (tournamentId) ->
        @dispatch("setTournamentId", tournamentId)

    addFriendToChallengeList: (friendId) ->
        @dispatch("addFriendToChallengeList", friendId)

    removeFriendFromChallengeList: (friendId) ->
        @dispatch("removeFriendFromChallengeList", friendId)

    toggleFriendInChallengeList: (friendId) ->
        friends = @stores.wizard.getListOfFriendsToChallenge()
        if friends.has(friendId)
            @removeFriendFromChallengeList(friendId)
        else
            @addFriendToChallengeList(friendId)

    challengeFriends: ->
        friends = @stores.wizard.getListOfFriendsToChallenge()
        competitionRoomId = @stores.wizard.getCompetitionRoomId()
        competitionRoom = @stores.rooms.getRoom(competitionRoomId)
        if friends.size > 0
            data =
                competitionRoomId: competitionRoomId
                tournamentName: competitionRoom.getTournamentName()
                friendsIds: friends.toArray()
                sport: SPORT


            connAuth.send('set', 'challenge-request', data,
                ((err, data) =>
                    return unless err
                    logger.log(err, ['Challenge request', data], "WizardActions")
                    if Array.isArray(err)
                        err = err[0]
                    return flash({
                        type: 'error'
                        text: errors.get(err) or err
                    })
                )
            )

    saveGame: () ->
        roomIds = @stores.wizard.getSelectedRoomIds()

        saveIt = (wizardData, game) =>
            selectedParticipants = wizardData.selectedParticipants.toJS()
            if wizardData.gameType in ["handicap-the-pros", "hole-by-hole"]
                game.participant1_id = selectedParticipants[0]
                game.participant2_id = selectedParticipants[1]
                game.participant3_id = selectedParticipants[2]
            else if wizardData.gameType == "five-ball"
                game.participant1_id = selectedParticipants[0]
                game.participant2_id = selectedParticipants[1]
                game.participant3_id = selectedParticipants[2]
                game.participant4_id = selectedParticipants[3]
                game.participant5_id = selectedParticipants[4]

            if wizardData.gameType == "hole-by-hole"
                game.prediction1 = wizardData.predictions.get(game.participant1_id)
                game.prediction2 = wizardData.predictions.get(game.participant2_id)
                game.prediction3 = wizardData.predictions.get(game.participant3_id)

            if wizardData.gameType == "handicap-the-pros"
                game.handicap_bonus1 = wizardData.bonuses.get(game.participant1_id)
                game.handicap_bonus2 = wizardData.bonuses.get(game.participant2_id)
                game.handicap_bonus3 = wizardData.bonuses.get(game.participant3_id)


            @setGameState('saving')
            conn.send('set', wizardData.gameType + '-game', game,
                ((err, data) =>
                    if err
                        @setGameState('saving error')
                        logger.log(err, ['Game saving error', game], "WizardActions")
                        if Array.isArray(err)
                            err = err[0]
                        if err
                            return flash({
                                type: 'error'
                                text: errors.get(err) or err
                            })

                    @setGameState('saved')
                    eventTracker("buttons", wizardData.gameType + "-new-game")
                    eventTracker('buttons', wizardData.gameType + '-pay-game')
                )
            )

        wizardData = @stores.wizard.getState()
        if wizardData.gameModel
            saveIt(wizardData, {id: wizardData.gameModel.id})
        else
            roomIds.forEach((competitionRoomId) ->
                saveIt(wizardData, {competitionRoomId})
            )


    setGameState: (gameState) ->
        @dispatch('setGameState', gameState)

    toggleCompetitionRoom: (id) ->
        @dispatch('toggleCompetitionRoom', id)

    setInfoTooltipVisibility: (settings, isVisible) ->
        @dispatch('setInfoTooltipVisibility', settings, isVisible)


module.exports = WizardActions
