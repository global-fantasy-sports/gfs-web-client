{Actions} = require('minimal-flux')
{COMPARE_PLAYERS_MAX} = require('../../../lib/const')


class CompareActions extends Actions

    add: (player) ->
        {players, position} = @stores.compare.getState()
        if not players.size or (position is (player.position || 1) and players.size < COMPARE_PLAYERS_MAX)
            @dispatch('add', player)

    remove: (player) ->
        @dispatch('remove', player)

    toggle: (player) ->
        {players} = @stores.compare.getState()
        if players.has(player.id)
            @remove(player)
        else
            @add(player)

    clear: () ->
        @dispatch('clear')


module.exports = CompareActions
