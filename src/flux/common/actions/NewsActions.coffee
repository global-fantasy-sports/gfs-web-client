{Actions} = require('minimal-flux')
xhr = require('../../../lib/xhr')


class NewsActions extends Actions

    fetch: (sport) ->
        @dispatch('fetch', sport)
        # FIXME
        # xhr.get('/feeds/news/')
        #     .then(@fetchResponse, @fetchError)
        @fetchResponse(news: [{
            'date': 'TODAY',
            'headline': 'Eight under-the-radar NFL free-agent signings',
            'text': 'American football evolved in the United States, originating from the sports of association football and rugby football.',
            'imageSmall': '/static/img/selector/news-pre-img.jpg',
            'imageBig': '/static/img/selector/news-img.jpg',
            'link': '#'
        }, {
            'date': 'TODAY',
            'headline': 'Eight under-the-radar NFL free-agent signings',
            'text': 'American football evolved in the United States, originating from the sports of association football and rugby football.',
            'imageSmall': '/static/img/selector/news-pre-img.jpg',
            'imageBig': '/static/img/selector/news-img.jpg',
            'link': '#'
        }, {
            'date': 'TODAY',
            'headline': 'Eight under-the-radar NFL free-agent signings',
            'text': 'American football evolved in the United States, originating from the sports of association football and rugby football.',
            'imageSmall': '/static/img/selector/news-pre-img.jpg',
            'imageBig': '/static/img/selector/news-img.jpg',
            'link': '#'
        }])

    fetchResponse: (response) ->
        @dispatch('fetchResponse', response)

    fetchError: (error) ->
        @dispatch('fetchError', error)


module.exports = NewsActions
