_ = require 'underscore'
{Actions} = require "minimal-flux"
{localStorage} = require 'env/storage'

class FormActions extends Actions
    init: ->
        @dispatch "init"

    loadSaved: (name)->
        saved = localStorage.getItem(name)
        localStorage.removeItem(name)
        @dispatch('loadSaved', if saved then JSON.parse(saved) else null)

    save: (name)->
        values = _.omit(@stores.forms.getValues(), 'password')
        localStorage.setItem(name, JSON.stringify(values))
        @dispatch('save', values)

    validatePassword: ->
        @dispatch "validatePassword"

    validateSignup: ->
        @dispatch "validateSignup"

    startProgress: (elements) ->
        @dispatch "startProgress", elements

    stopProgress: (elements) ->
        @dispatch "stopProgress", elements

    setError: (key, value) ->
        @dispatch "setError", key, value

    setValue: (key, value) ->
        setTimeout =>
            @dispatch "setValue", key, value
        , 1

    setSuccess: (key) ->
        @dispatch "setSuccess", key



module.exports = FormActions
