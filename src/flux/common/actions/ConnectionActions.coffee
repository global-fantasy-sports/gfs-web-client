_ = require('underscore')
{Actions} = require('minimal-flux')
querystring = require('querystring')

{getCookie, getSession, countTracker} = require('../../../lib/utils')
flash = require('../../../lib/flash')
errors = require('../../../lib/errors')
config = require('../../../config')
xhr = require('../../../lib/xhr')
{conn, connAuth} = require('../../../lib/connection')
API = require('../../API')


checkError = (response = {}) ->
    if response.code is 'blocked_by_location' and not config.ignoreWrongLocation
        throw new Error('blocked_by_location')
    if response.error
        throw new Error(response.error)
    return response


class ConnectionActions extends Actions
    constructor: ->
        @timer = null

    init: ->
        if not (config.server == '')
            conn.on('disconnect', => @reconnect(conn))
            session = getSession()
            @dispatch('init', {conn, session})
            API.checkAvailability()
                .then(=> @connect(conn, session))
                .catch(=> @unavailable())

        connAuth.on('disconnect', => @reconnect(connAuth))
        session = getSession()
        @connect(connAuth, session)

        if SPORT is ''
            currentSport = @stores.pageData.getCurrentSport()
            if currentSport
                @loadFeaturedRooms(currentSport)

    loadFeaturedRooms: (sport) ->
        if sport == @stores.pageData.getCurrentSport()
            xhr.get("/#{sport}/api/selector/info/")
            .then(
                ((data) =>
                    if config.debug
                        console.log('ajax -> [selectorInfo, ' + sport + ']', data)
                    if data
                        if data.currentDate and not @stores.time.isRunning()
                            @actions.time.start({
                                fake: true
                                locked: true
                                date: data.currentDate
                                week: data.currentWeek
                            })
                        @actions.data.loadSelectorRooms(sport, data.rooms)
                        @actions.data.loadSelectorOverachievers(sport, data.overachievers)
                        @actions.data.setNextCompetitionStartTime(sport, data.nextStartingTime)
                        setTimeout((=> @loadFeaturedRooms(sport)), 5000)
                ),
                ((error) =>
                    console.log(error)
                )
            )

    connect: (connection, session) ->
        session ?= getSession()
        if not session
            @connectError(connection, 'Invalid session')
            @actions.forms.loadSaved('RegistrationForm')
        else
            if connection.name != 'AUTH'
                @dispatch('connect')
            connection.connect(session, (error, data) =>
                if error then @connectError(connection, error)
                else @connectSuccess(connection, data)
            )

    connectError: (connection, error) ->
        if connection.name != 'AUTH'
            @dispatch('connectError', error)
        setTimeout((=> @connect(connection)), 5000)


    connectSuccess: (connection, data) ->
        if connection.name == 'AUTH'
            @authConnectSuccess(data)
        else
            @gameConnectSuccess(data)

    authConnectSuccess: (data) ->
        @actions.data.setCurrentUser(data.user)
        @actions.data.setChallengeRequests(data.initial.challengeRequests)
        @actions.facebook.init(data.info['facebook_app_id'])
        @dispatch('authConnectSuccess', data.initial)

    gameConnectSuccess: (data) ->

        @actions.time.start({
            fake: data.fake_tour_mode
            week: data.current_week
            date: data.current_date
            locked: data.resetLocked
        })
        if data.initial
            @actions.data.initialData(data.initial)
            @actions.connection.initialDataPushed(
                roomsCount: data.initial.competitionRooms.length
                participantsCount: data.initial.participants.length
            )

            _.map(data.initial.games, (game) =>
                @actions.data.newGame(game)
            )

        @dispatch('connectSuccess', data)


    initialDataPushed: (data) ->
        @dispatch('initialDataPushed', data)

    reconnect: (connection) ->
        if connection.name != 'AUTH'
            @dispatch('reconnect')
        setTimeout((=> @connect(connection)), 5000)

    disconnect: ->
        @reconnect()

    facebookLogin: ->
        @dispatch('facebookLogin')
        window.location = '/auth/facebook/query/'

    googleLogin: ->
        @dispatch('googleLogin')
        window.location = '/auth/google/query'

    login: ->
        @stores.connection.getLoginForm().validate().then((user) =>
            xhr.post("/auth/login", user)
                .then(checkError)
                .then(@loginSuccess, @loginError)
            @dispatch('login')
        ).catch((error) ->
            logger.log(error, 'login error', 'ConnectionActions')
        )
    loginSuccess: ->
        @dispatch('loginSuccess')
        @connect()

    loginError: (error) ->
        if error.message is 'blocked_by_location'
            @unavailable()
        else
            flash(type: 'error', text: errors.get(error.message))
            @dispatch('loginError', error)

    signup: ->
        @stores.connection.getSignupForm().validate().then((user) =>
            if tap and tap.vid
                user['affiliate_code'] = tap.vid
            else if getCookie('jamcom')
                user['affiliate_code'] = getCookie('jamcom')

            xhr.post("/auth/register", user)
                .then(checkError)
                .then(@signupSuccess, @signupError)

            @dispatch('signup')
        ).catch((error) ->
            logger.log(error, 'signup error', 'ConnectionActions')
        )

    signupError: (error) ->
        if error.message is 'blocked_by_location'
            @unavailable()
        else
            flash(type: 'error', text: errors.get(error.message))
            @dispatch('signupError', error)

    signupSuccess: (data) ->
        @connect()
        @dispatch('signupSuccess', data)

    unavailable: () ->
        @dispatch('unavailable')

    setWaitingState: ->
        @dispatch('setWaitingState')

    logout: ->
        @actions.facebook.logout()
        xhr.post("/auth/logout/")
            .then(checkError)
            .then(=>
                sessionStorage.clear()
                @dispatch('logout')
                window.location.href = '/'
            )

    saveAffiliateCode: (code, callback) ->
        connAuth.send('set', 'affiliate-code', {affiliate_code: code}, (error, response) ->
            if error
                logger.error("Error saving affiliate code: #{error}")
            else if typeof callback is 'function'
                callback(response)
        )

    registerEFF: (data = {}) ->
        xhr.post('/user/eff-register/', data)
            .then(() => @actions.tap.closeDialog())

    setProcessingState: (message) ->
        @dispatch('setProcessingState', message)

    processed: ->
        setTimeout((=> @dispatch('processed')), 3000)


module.exports = ConnectionActions
