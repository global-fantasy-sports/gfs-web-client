_ = require('underscore')
{Actions} = require('minimal-flux')
xhr = require('../../../lib/xhr')
API = require('../../API')
config = require('../../../config')

class TapActions extends Actions
    selectParticipant: (participantId) ->
        @dispatch "selectParticipant", participantId

    unselectParticipant: (participantId) ->
        @dispatch "unselectParticipant", participantId

    participantStatistics: (participantId) ->
        @dispatch "participantStatistics", participantId

    gameParticipant: (gameId, participantId) ->
        @dispatch "gameParticipant", gameId, participantId
        @actions.state.setActiveGameId(gameId)

    gameParticipantFiveBall: (gameId, participantId) ->
        @dispatch "gameParticipantFiveBall", gameId, participantId
        @actions.state.setActiveGameId(gameId)

    closeScorecard: ->
        @dispatch "closeScorecard"

    toggleRound: (roundNum) ->
        @dispatch "toggleRound", roundNum

    toggleActiveRound: (roundNumber) ->
        @dispatch "toggleActiveRound", roundNumber

    scorecardFlag: (participantId) ->
        @dispatch "scorecardFlag", participantId

    guideHintTap: (group, id, scrollTo=null) ->
        return unless @stores.guide.isGuideActive()
        @dispatch "guideHintTap", group, id
        xhr
            url: config.server + "/api/guide/closetip/"
            method: 'post'
            data: JSON.stringify({
                step_id: "#{group}:#{id}"
            })
            callback: () ->
        if scrollTo
            _.defer(=>
                @actions.state.scrollToTop({el: document.getElementById(scrollTo)})
            )


    resetGuide: ->
        xhr(
            url: config.server + "/api/guide/reset/"
            method: 'post'
            callback: => @dispatch('resetGuide')
        )

    closeGuide: ->
        @dispatch "closeGuide"
        xhr
            url: config.server + "/api/guide/switch/"
            method: 'post'
            data: JSON.stringify({
                guide_mode: false
            })
            callback: () ->

    activateGuide: ->
        @dispatch "activateGuide"
        xhr
            url: config.server + "/api/guide/switch/"
            method: 'post'
            data: JSON.stringify({
                guide_mode: true
            })
            callback: () ->

    closeDialog: ->
        @dispatch "closeDialog"

    forgotPassword: ->
        @dispatch "forgotPassword"

    faqQuestion: (question) ->
        @dispatch "faqQuestion", question

    hitAndTricks: (gameType, stepName) ->
        @dispatch "hitAndTricks", gameType, stepName

    nextStep: (step) ->
        @dispatch "nextStep", step

    stepBack: ->
        @dispatch "stepBack"

    clearSteps: ->
        @dispatch "clearSteps"

    editGame: (game) ->
        @actions.facebook.fetchFriends({silent: true})
        @dispatch "editGame", game

    newGame: ->
        @actions.facebook.fetchFriends({silent: true})
        @dispatch "newGame"

    resetPassword: (opt) ->
        @dispatch "resetPassword", opt

    sendResetLink: (email) ->
        @dispatch "sendResetLink", email

    openRemoveGameDialog: (cb) ->
        @dispatch "openRemoveGameDialog", cb

    openRemoveChallengeRequestDialog: (cb) ->
        @dispatch "openRemoveChallengeRequestDialog", cb

    finishRegistration: ({terms, email, country, state}) ->
        API.updateProfile({terms, email, country, state, showCountryAndTerms: true})
            .then ({user}) =>
                @dispatch("finishRegistration", user)
            .catch (err) =>
                if err.email_exists
                    return @actions.state.openAccountExistsDialog()
                @actions.forms.setError("FINISH_REGISTRATION", err)

    linkAccounts: (opt) ->
        @dispatch "linkAccounts", opt


module.exports = TapActions
