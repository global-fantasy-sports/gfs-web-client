{Actions} = require 'minimal-flux'
{connAuth} = require 'lib/connection'
{channels} = require 'lib/const'


class NotificationsActions extends Actions

    constructor: ->
        connAuth.subscribe(channels.NOTIFICATIONS.push, (item)=> @received(item))

    togglePanel: ->
        if @stores.notifications.isPanelOpen()
            if @stores.notifications.isShowOnlyImportant()
                @markAllImportantRead()
            else
                @markAllRead()

        @dispatch('togglePanel')

    closePanel: () ->
        if @stores.notifications.isShowOnlyImportant()
            @markAllImportantRead()
        else
            @markAllRead()
        @dispatch('closePanel')

    received: (item)->
        @dispatch('received', item)

    markAsRead: (id)->
        @dispatch('markAsRead', id)
        connAuth.send('set', channels.NOTIFICATIONS.set, {ids: [id]})

    markAllRead: ->
        ids = Array.from(@stores.notifications.getUnread().keys())
        @dispatch('markAllRead')
        if ids.length
            connAuth.send('set', channels.NOTIFICATIONS.set, {ids})

    markAllImportantRead: ->
        ids = Array.from(@stores.notifications.getImportant().keys())
        @dispatch('markAllImportantRead')
        if ids.length
            connAuth.send('set', channels.NOTIFICATIONS.set, {ids})

    markAllUnread: ->
        @dispatch('markAllUnread')

    hidePopup: (id)->
        @dispatch('hidePopup', id)

module.exports = NotificationsActions
