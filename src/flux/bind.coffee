_ = require 'underscore'


getStore = (storeOrName)->
    if typeof storeOrName is 'string'
        flux.stores[storeOrName]
    else
        storeOrName


getValueOf = (param, stores)->
    if typeof param is 'function' then param(stores...) else param


defaultPropsGetter = (stores)->
    ->
        values = (getStore(store).getState() for store in stores)
        _.extend({}, values...)


class Subscriptions

    constructor: (stores, callback)->
        @callback = _.throttle(callback, 100)
        @stores = for store in stores
            store = getStore(store)
            store.addListener('change', @callback)
            store

    release: =>
        for store in @stores
            store.removeListener('change', @callback)
        true


module.exports = (Base, options)->

    boundStores = (options.stores ? []).map(getStore)
    boundProps = options.props ? defaultPropsGetter(boundStores)

    class Container extends React.Component
        @displayName: "Container(#{(Base.displayName or Base.name)})"

        componentDidMount: ->
            @subscriptions = new Subscriptions(boundStores, => @forceUpdate())

        componentWillUnmount: ->
            @subscriptions.release()

        render: ->
            props = getValueOf(boundProps, boundStores)
            `<Base {...props} {...this.props} />`
