_ = require('underscore')

Connect = (storeName, handler) ->
    store = flux.stores[storeName]

    if not store
        if process.env.NODE_ENV isnt 'production'
            console.warn("ConnectMixin: store '#{storeName}' does not exist")
        return {}

    return {
        getInitialState: ->
            handler(store, @props, {}, null)

        componentWillReceiveProps: (nextProps) ->
            chunk = handler(store, nextProps, @state, this)
            @setState(chunk)

        componentDidMount: ->
            @__onStoreChange or= []

            onChange = _.throttle(=>
                if @isMounted()
                    chunk = handler(store, @props, @state, this)
                    @setState(chunk)
            , 150) # 150 because Andrey said

            @__onStoreChange.push(onChange)
            store.addListener("change", onChange)

        componentWillUnmount: ->
            for onChange in @__onStoreChange
                store.removeListener("change", onChange)
            return
    }

module.exports = Connect
