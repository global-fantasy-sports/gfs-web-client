if SPORT is 'golf'
    flux = require('./golf/flux')
else if SPORT in ['nfl', 'nba']
    flux = require('./nfl/flux')
else
    flux = require('./selector/flux')


flux.Connect = require('./connectMixin')


module.exports = flux
