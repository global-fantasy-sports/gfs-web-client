{Flux} = require('minimal-flux')

CompareActions = require('../common/actions/CompareActions')
ConnectionActions = require('../common/actions/ConnectionActions')
DataActions = require('../common/actions/DataActions')
FacebookActions = require('../common/actions/FacebookActions')
FormActions = require('../common/actions/FormActions')
HandicapActions = require('./actions/HandicapActions')
LineupActions = require('../nfl/actions/LineupActions')
NotificationsActions = require('../common/actions/NotificationsActions')
PaymentActions = require('../common/actions/PaymentActions')
RosterActions = require('../nfl/actions/RosterActions')
SearchActions = require('../common/actions/SearchActions')
StateActions = require('../common/actions/StateActions')
TapActions = require('../common/actions/TapActions')
TimeActions = require('../common/actions/TimeActions')
UserAccountActions = require('../common/actions/UserAccountActions')
WizardActions = require('../common/actions/WizardActions')

ChallengeStore = require('../common/stores/ChallengeStore')
CompareStore = require('../common/stores/CompareStore')
CompetitionRoomStore = require('./stores/CompetitionRoomStore')
ConnectionStore = require('../common/stores/ConnectionStore')
DialogsStore = require('../common/stores/DialogsStore')
FacebookStore = require('../common/stores/FacebookStore')
FaqStore = require('./stores/FaqStore')
FormsStore = require('../common/stores/FormsStore')
GamesStore = require('./stores/GamesStore')
GuideStore = require('../common/stores/GuideStore')
HandicapStore = require('./stores/HandicapStore')
NotificationsStore = require('../common/stores/NotificationsStore')
ParticipantRoundResultsStore = require('./stores/ParticipantRoundResultsStore')
ParticipantsStore = require('./stores/ParticipantsStore')
PaymentStore = require('../common/stores/PaymentStore')
RosterStore = require('./stores/RosterStore')
RoundStore = require('./stores/RoundStore')
ScoreCardsStore = require('./stores/ScoreCardsStore')
SearchStore = require('./stores/SearchStore')
StateStore = require('../common/stores/StateStore')
SubscriptionStore = require('./stores/SubscriptionStore')
TimeStore = require('../common/stores/TimeStore')
TournamentsStore = require('./stores/TournamentsStore')
UsersStore = require('../common/stores/UsersStore')
WizardStore = require('./stores/WizardStore')


module.exports = new Flux(
    actions:
        compare: CompareActions
        connection: ConnectionActions
        data: DataActions
        facebook: FacebookActions
        forms: FormActions
        handicap: HandicapActions
        lineup: LineupActions
        notifications: NotificationsActions
        payment: PaymentActions
        roster: RosterActions
        search: SearchActions
        state: StateActions
        tap: TapActions
        time: TimeActions
        userAccount: UserAccountActions
        wizard: WizardActions

    stores:
        challenges: [ChallengeStore, 'rooms']
        compare: CompareStore
        connection: [ConnectionStore, 'forms', 'rooms', 'participants']
        dialogs: DialogsStore
        facebook: FacebookStore
        faq: FaqStore
        forms: [FormsStore, 'state']
        games: GamesStore
        guide: GuideStore
        handicap: [HandicapStore, 'roster', 'wizard']
        notifications: [NotificationsStore, 'participants', 'tournaments']
        participantRoundResults: ParticipantRoundResultsStore
        participants: ParticipantsStore
        payments: [PaymentStore, 'users']
        rooms: [CompetitionRoomStore, 'search']
        roster: [RosterStore, 'participants', 'tournaments']
        rounds: RoundStore
        scorecards: [ScoreCardsStore, "games", "participants", "users", "tournaments", "state", "participantRoundResults"]
        search: SearchStore
        state: StateStore
        subscription: SubscriptionStore
        time: [TimeStore, 'connection']
        tournaments: TournamentsStore
        users: [UsersStore, 'connection', 'forms']
        wizard: [WizardStore, 'participants']
)
