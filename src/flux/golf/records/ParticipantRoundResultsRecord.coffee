createRecord = require "flux/createRecord"


ParticipantRoundResultsRecord = createRecord({
    id: null
    participantId: ["participant_id", null]
    roundNumber: ["round_number", null]
    pars: []
    strokes: []
    yardages: []
}, {
    getLastHole: ->
        @strokes.findIndex((el) -> el != 0)

    getScore: ->
        score = 0
        @strokes.forEach((val, i) =>
            if val
                score += val - @pars[i]
        )
        return score
})

module.exports = ParticipantRoundResultsRecord
