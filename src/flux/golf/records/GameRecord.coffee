Immutable = require('immutable')
createRecord = require('../../createRecord')


commonFields =
    id: null
    userId: null
    competitionRoomId: null
    entryFee: null
    points: null
    place: null
    placeRange: null
    paid: null
    mode: null
    tournamentId: null
    roundNumber: null
    userData: {}
    mechanical: null


commonMethods =
    isMyGame: ->
        @userId == flux.stores.users.getCurrentUser().get('id')

    getTournament: ->
        flux.stores.tournaments.getTournament(@tournamentId)

    getCompetitionRoom: ->
        flux.stores.rooms.getRoom(@competitionRoomId)

    isPending: ->
        @getCompetitionRoom().isPending()

    isLive: ->
        @getCompetitionRoom().isLive()

    isFinished: ->
        @getCompetitionRoom().isFinished()

    isRecent: ->
        @getCompetitionRoom().isRecent()

    getPrize: ->
        room = @getCompetitionRoom()
        return false unless room.isFinished()
        room.getIn(['prizeList', @place]) or false

    delete: ->
        flux.actions.data.removeGame(@id)

    removeFromStore: ->
        flux.actions.data.removeGameFromStore(@id)

FiveBallGameRecord = createRecord(
    Object.assign({}, commonFields, {
        gameType: 'five-ball'
        participantId1: null
        participantId2: null
        participantId3: null
        participantId4: null
        participantId5: null
    }),
    Object.assign({}, commonMethods, {
        getParticipants: ->
            Immutable.List([
                @participantId1
                @participantId2
                @participantId3
                @participantId4
                @participantId5
            ]).map((id) -> flux.stores.participants.getParticipant(id))
    })
)


HandicapTheProsGameRecord = createRecord(
    Object.assign({}, commonFields, {
        gameType: 'handicap-the-pros'
        participantId1: null
        participantId2: null
        participantId3: null
        handicapBonus1: []
        handicapBonus2: []
        handicapBonus3: []
    }),
    Object.assign({}, commonMethods, {
        getParticipants: ->
            Immutable.List([
                @participantId1
                @participantId2
                @participantId3
            ]).map((id) -> flux.stores.participants.getParticipant(id))

    })
)


HoleByHoleGameRecord = createRecord(
    Object.assign({}, commonFields, {
        gameType: 'hole-by-hole'
        participantId1: null
        participantId2: null
        participantId3: null
        prediction1: []
        prediction2: []
        prediction3: []
    }),
    Object.assign({}, commonMethods, {
        getParticipants: ->
            Immutable.List([
                @participantId1
                @participantId2
                @participantId3
            ]).map((id) -> flux.stores.participants.getParticipant(id))

        getStats: ->
            correct = 0
            wrong = 0
            paramsArray = [
                {
                    participantId: @participantId1
                    prediction: @prediction1
                    roundNumber: @roundNumber
                }
                {
                    participantId: @participantId2
                    prediction: @prediction2
                    roundNumber: @roundNumber
                }
                {
                    participantId: @participantId3
                    prediction: @prediction3
                    roundNumber: @roundNumber
                }
                {
                    participantId: @participantId1
                    prediction: @prediction1
                    roundNumber: @roundNumber + 1
                }
                {
                    participantId: @participantId2
                    prediction: @prediction2
                    roundNumber: @roundNumber + 1
                }
                {
                    participantId: @participantId3
                    prediction: @prediction3
                    roundNumber: @roundNumber + 1
                }
            ]
            for params in paramsArray
                results = flux.stores.participantRoundResults.getResult({
                    participantId: params.participantId
                    roundNumber: params.roundNumber
                })

                if results
                    results.get("strokes", Immutable.List()).forEach((stroke, i) ->
                        par = results.pars[i]
                        if stroke - par > 0
                            boundary = 1
                        else if stroke - par < 0
                            boundary = -1
                        else
                            boundary = 0

                        if boundary == params.prediction[i]
                            correct++
                        else
                            wrong++
                    )
            {correct, wrong}
    })
)

GAME_RECORD_BY_TYPE =
    'five-ball': FiveBallGameRecord
    'handicap-the-pros': HandicapTheProsGameRecord
    'hole-by-hole': HoleByHoleGameRecord

module.exports = {
    FiveBallGameRecord
    HandicapTheProsGameRecord
    HoleByHoleGameRecord
    GAME_RECORD_BY_TYPE
}
