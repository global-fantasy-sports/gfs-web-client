_ = require('underscore')
Immutable = require('immutable')
moment = require('moment')
{fromUtcTs} = require("lib/date")
createRecord = require("flux/createRecord")
{SUMMARY_STATS} = require('../../../components/const')
{countryByCode} = require('lib/utils')
StatRecord = require('./StatRecord')


parseStats = (stats) ->
    return Immutable.List() unless stats

    Immutable.List(stats.map((item) ->
        new StatRecord(Object.assign({}, item))
    ))

parseStatInfo = (statInfo) ->
    stats = Object.assign({}, statInfo)
    feets = Math.trunc(stats.hole_proximity_avg)
    inches = Math.trunc(stats.hole_proximity_avg * 100 % 100)

    stats.hole_proximity_avg = "#{feets}' #{inches}''"
    stats.cuts_made = (stats.cuts_made or 0).toFixed(0)
    stats.events_played = (stats.events_played or 0).toFixed(0)
    stats

ParticipantRecord = createRecord({
    id: null
    firstName: null
    lastName: null
    height: null
    weight: null
    birthday: [fromUtcTs, null]
    smallPhoto: null
    largePhoto: null
    tournamentId: null
    country: null
    lastPosition: null
    lastScore: "N/A"
    rank: "N/A"
    handicap: null
    status: null
    golferId: null
    avgFPPR: null
    statInfo: [parseStatInfo, {}]
    statInfoHistory: [parseStats, Immutable.List()]
}, {
    getFullName: ->
        ((@firstName && "#{@firstName} ") or "") + "#{@lastName}"

    getName: -> @getFullName()

    getRoundResult: (roundNumber) ->
        flux.stores.participantRoundResults.getResult({
            participantId: @id
            roundNumber: roundNumber
        })

    getPhoto: (size = 'large') ->
        if size is 'small'
            @smallPhoto
        else
            @largePhoto

    getScore: ->
        [1..4].reduce(((score, n) => score + @getRoundResult(n).getScore()), 0)

    getScoreDisplay: ->
        score = @getScore()
        if score > 0 then "+#{score}"
        else if isNaN(score) then "-"
        else "#{score}"

    isDefault: ->
        Immutable.is(this, new ParticipantRecord())

    name: ->
        @firstName + ' ' + @lastName

    getTournament: ->
        flux.stores.tournaments.getTournament(@tournamentId)

    getFPPGHistory: () ->
        @statInfoHistory.map((item) -> item.score).toArray()

    formatFPPR: ->
        if typeof @avgFPPR is 'number'
            return {value: @avgFPPR.toFixed(2), label: 'FPPR'}
        else
            return {value: '', label: 'FPPR N/A'}

    formatRank: ->
        "World Rank #{@rank}"

    getStatSummary: ->
        Immutable.OrderedMap(SUMMARY_STATS.map((stat) =>
            [stat, @statInfo[stat]]
        ))

    getFullCountry: ->
        countryByCode(@country)

    getAge: -> moment().diff(@birthday, 'years')

    getCalculatedStats: ->
        @statInfoHistory.map((stat) ->
            if stat.tourStartDate.format('MM') == stat.tourEndDate.format('MM')
                start = stat.tourStartDate.format('DD')
                end = stat.tourEndDate.format('DD MMM, YY')
            else
                start = stat.tourStartDate.format('DD MMM ')
                end = stat.tourEndDate.format(' DD MMM, YY')

            range = "#{start}-#{end}"
            stat.set('dateRange', range)
        )

    getAverage: (type) ->
        if type is 'games'
            return @statInfoHistory.count((item) -> not item.statistics.isEmpty())

        values = @statInfoHistory.map((item) -> item.getStat(type) or 0)
        values.reduce((a, b) -> a + b) / values.size

})


module.exports = ParticipantRecord
