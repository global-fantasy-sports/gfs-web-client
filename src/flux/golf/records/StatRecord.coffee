createRecord = require 'flux/createRecord'
moment = require 'moment'



parseDate = (timestamp) -> moment.unix(timestamp)

StatRecord = createRecord({
    tournamentId: ['tournament_id', null]
    position: null
    score: 0
    moneyPrize: ['money_prize', 0]
    strokes: []
    avgFPPR: null
    pars: null
    birdies: null
    bogeys: null
    tourStartDate: [parseDate, null]
    tourEndDate: [parseDate, null]
    tourName: null
    dateRange: null
}, {
    getStat: (statName) ->
        this[statName]
})


module.exports = StatRecord
