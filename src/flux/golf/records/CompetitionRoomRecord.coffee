Immutable = require('immutable')
{daysBetween, fromUtcTs, now} = require('../../../lib/date')
createRecord = require('../../createRecord')
{gameNameByType} = require('../../../lib/const')
{Dollars, Tokens} = require('../../../lib/ui')

parsePrizes = (data) ->
    Immutable.Map(data).mapKeys((k) -> Number(k))

CompetitionRoomRecord = createRecord({
    id: null
    tournamentId: null
    roundNumber: null
    pot: null
    entryFee: null
    gameType: null
    type: null
    maxEntries: 0
    mode: null
    gamesCount: 0
    started: null
    finished: null
    startDate: [fromUtcTs, null]
    prizeList: [parsePrizes, Immutable.Map()]

}, {
    getTournament: ->
        flux.stores.tournaments.getTournament(@tournamentId)

    isFull: ->
        @maxEntries == @gamesCount

    getTournamentName: ->
        tournament = @getTournament()
        tournament.name

    getGames: ->
        flux.stores.games.getGames({competitionRoomId: +@id})

    fetchGames: (options = {}) ->
        force = options.force or false
        if @getGames().size > 0 and force == false
            return
        flux.actions.data.fetchGamesByRoomId(@id)

    isPending: ->
        @started == false

    isLive: ->
        @started == true and @finished == false

    isFinished: ->
        @finished == true

    isRecent: ->
        not @isFinished() or daysBetween(@startDate, now()) < 5

    isEmptyAndPending: ->
        @size == 0 and @started == false

    getDaysLeft: ->
        daysBetween(@startDate, now())

    fullName1: ->
        title = gameNameByType[@gameType]

        maxEntries = @maxEntries
        winners = @prizeList.size
        chances = winners / maxEntries

        dayDiff = @getDaysLeft()
        if dayDiff == 0
            dayDiff = "TODAY"
        else if dayDiff == 1
            dayDiff = "TOMORROW"
        else
            dayDiff = "#{dayDiff}-days from now"

        tournament = @getTournament()
        tournamentInfo = `<div>{tournament.name}</div>`

        if maxEntries == 2
            `<div>
                <div>{title}, {dayDiff}, head-to-head matchup 50% chance to win</div>
                {tournamentInfo}
            </div>`
        else if chances > 0.2
            `<div>
                <div>
                    <span>{title}, {dayDiff},</span><br />
                    <span>{winners} GUARANTEED winners!!</span>
                </div>
                {tournamentInfo}
            </div>`
        else if chances <= 0.2
            firstPrize = @prizeList.get("1")
            if @mode == "token"
                stakeView = `<Tokens m={firstPrize}/>`
            else
                stakeView = `<Dollars m={firstPrize}/>`

            `<div>
                <div>
                    <span>{title}, {dayDiff},</span><br />
                    <span>{stakeView} 1ST PRIZE!!</span>&nbsp;
                    <span>{winners} guaranteed winners.</span>
                </div>
                {tournamentInfo}
            </div>`

    fullName2: ->
        ''
})


module.exports = CompetitionRoomRecord
