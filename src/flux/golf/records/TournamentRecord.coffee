Immutable = require('immutable')
strftime = require('strftime')
moment = require('moment')

{fromUtcTs, getStartOfWeek} = require('../../../lib/date')
createRecord = require('../../createRecord')

EmptyRounds = Immutable.Map([
    [1, Immutable.Map()]
    [2, Immutable.Map()]
    [3, Immutable.Map()]
    [4, Immutable.Map()]
])

roundParser = (rounds) ->
    return EmptyRounds unless rounds
    Immutable.Map([1..4].map((index) ->
        [index, Immutable.Map(rounds[index])]
    ))

TournamentRecord = createRecord({
    id: null
    name: ''
    startDate: [fromUtcTs, null]
    endDate: [fromUtcTs, null]
    purse: null
    fedexPoints: null
    status: null
    courseRating: null
    courseName: ''
    country: ''
    rounds: [roundParser, EmptyRounds]
    winningShare: "1,000,000"
}, {
    isLive: ->
        @isPending() and not @isFinished()

    isPending: ->
        not @getIn(['rounds', 1, "started"], false)

    isFinished: ->
        @getIn(['rounds', 4, "finished"], false)

    isThisWeek: ->
        thisWeek = getStartOfWeek()
        thisWeek == getStartOfWeek(@startDate) or (thisWeek == getStartOfWeek(@endDate) and not @isFinished())

    isAllowedToStart: ->
        @isThisWeek() and not @getIn(['rounds', 3, "started"], true)

    getCurrentRoundNumber: ->
        if not @isPending()
            return 0
        for i in [1..4]
            if i < 4
                if @getIn(['rounds', i, "started"], false) and not @getIn(['rounds', (i + 1), "started"], false)
                    return i
            else
                if @getIn(['rounds', i, "started"], false) and not @getIn(['rounds', i, "finished"], false)
                    return i
                else if @getIn(['rounds', i, "started"], true)
                    return 5

    getTournamentPeriod: ->
        timezone = strftime("%Z", @startDate)
        date = moment(@startDate)
        startTime = date.format("h[:]mm A")
        startDate = date.format("MMMM DD")
        endDate = moment(@endDate).format("DD")
        period = {
            time: "#{startTime} [#{timezone}]"
            date: "#{startDate} - #{endDate}"
            month: date.format("MMMM")
        }
        return period

})


module.exports = TournamentRecord
