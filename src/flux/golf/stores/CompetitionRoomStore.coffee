_ = require('underscore')
Immutable = require('immutable')
CompetitionRoomRecord = require('flux/golf/records/CompetitionRoomRecord')
CommonCompetitionRoomStore = require('../../common/stores/CompetitionRoomStore')


class CompetitionRoomStore extends CommonCompetitionRoomStore
    constructor: ->
        @state =
            rooms: Immutable.Map()
            # TODO: distribute competition rooms by type on creation
#            roomsByGameType: Immutable.Map(
#                'hole-by-hole': Immutable.Set()
#                'handicap-the-pros': Immutable.Set()
#                'five-ball': Immutable.Set()
#            )

        @handleAction('data.initialData', @handleInitialData)
        @handleAction("data.setCompetitionRoom", @setCompetitionRoom)
        @handleAction("data.updateCompetitionRoom", @updateCompetitionRoom)

        @handleAction('search.setEntryFeeInterval', () => @emit('change'))
        @handleAction('search.setPaymentTypeFilter', () => @emit('change'))
        @handleAction('search.setSortingField', () => @emit('change'))
        @handleAction('search.toggleBeginners', () => @emit('change'))
        @handleAction('search.toggleFeatured', () => @emit('change'))
        @handleAction('search.toggleGuaranteed', () => @emit('change'))
        @handleAction('search.toggleRoomType', () => @emit('change'))
        @handleAction('search.toggleRoomTypeFilter', () => @emit('change'))
        @handleAction('search.toggleGameTypeFilter', () => @emit('change'))

    handleInitialData: (data) ->
        rooms = Immutable.Seq(data.competitionRooms or []).toKeyedSeq()
            .mapEntries(([k, v]) -> [v.id, new CompetitionRoomRecord(v)])
            .toMap()

        allStarted = rooms.every((r) -> r.started)
        @setState({rooms, allStarted})

    setCompetitionRoom: (data) ->
        room = new CompetitionRoomRecord(data)
        @setState({
            rooms: @state.rooms.set(room.id, room)
        })

    updateCompetitionRoom: (data) ->
        rooms = @state.rooms.update(data.id, (room) ->
            if room
                for key, value of data
                    if typeof room[key] != 'undefined'
                        room = room.set(key, value)
                room
        )

        allStarted = rooms.every((r) -> r.started)
        @setState({rooms, allStarted})

    getRooms: (options = {}, filters = [], sorting = false) ->
        rooms = @state.rooms.toIndexedSeq()

        if not _(options).isEmpty()
            rooms = rooms.filter((room) ->
                for key, value of options
                    if room.get(key) != value
                        return false
                return true
            )

        if filters.length
            for filter in filters when filter
                rooms = rooms.filter(filter)

        if sorting
            rooms = rooms.sort(sorting)

        rooms.toList()

    getTotalAvailableRooms: () ->
        @state.rooms.count((room) ->
            room.gamesCount < room.maxEntries and not room.started
        )

    getTournamentFilteredRooms: () ->
        tournamentId = @stores.wizard.getTournamentId()
        roundNumber = @stores.wizard.getRoundNumber()
        gameType = @stores.wizard.getWizardGameType()

        @getFilteredRooms().filter((r) ->
            r.tournamentId is tournamentId and
            r.roundNumber is roundNumber and
            r.gameType is gameType
        )

    getTournamentCompetitionRooms: (tour_id) ->
        @state.rooms.filter((room) -> room.tournamentId == +tour_id)

    getRoom: (id) -> @state.rooms.get(+id, new CompetitionRoomRecord({}))

    getRoomTypes: -> @state.rooms.toIndexedSeq().map((r) -> r.gameType).toSet()

    isAllStarted: -> @state.allStarted


module.exports = CompetitionRoomStore
