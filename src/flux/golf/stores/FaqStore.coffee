_ = require "underscore"
{Store} = require "minimal-flux"

class FaqStore extends Store
    constructor: ->
        @state =
            faq:
                sections: [
                    {
                        name: "Basics"
                        questions: [
                            {
                                question: "Can I access FanDuel from my phone or tablet?"
                                answer: "Yes! We have an iPhone app, Android app and an iPad app. We also have a mobile-optimized version of most pages on the site, including lobby, entry and edit, and live scoring. If you click through to pages which haven't been optimized yet you'll simply be shown the desktop site version. On smartphones the mobile site will be used by default. On tablets we stick to the desktop version by default. You can use the link at the foot of every page to change to the site which suits you best."
                            }
                            {
                                question: "I thought FanDuel was free. Why was I prompted to pay?"
                                answer: "FanDuel offers both free and pay to play games. You may have been prompted either to deposit money into your account (after sign-in) or to enter a cash entry game. Either of these are optional. You are welcome to continue playing in free contests on FanDuel for as long as you want."
                            }
                        ]
                    }
                    {
                        name: "Rules and Scoring"
                        questions: [
                            {
                                question: "The games are all over, and I won, so why is 'My Account' not showing my winnings?"
                                answer: "We have to wait until all the final fantasy points have been reported to ensure that all box scores are accurate. Because fantasy points are awarded manually by stats providers, there can be some last minute changes. If there is difficulty obtaining official results or issues with scoring, the contest may be settled the following day."
                            }
                            {
                                question: "What happens if any of the games are postponed or called off (cancelled)?"
                                answer: "For NFL contests, you will receive points for any postponed games that are played no later than Wednesday of that week. For one day MLB contests, if the postponed game is played on the same day then you will receive points for your players in those games as normal. If a game is postponed to a later date or called off for any reason, any players you have selected for that game will receive zero points. For one day NBA, CBB and NHL contests, you will receive points for any postponed games that are played no later than the next day. For any NBA, CBB, MLB, and NHL contests that are scheduled to include games from multiple days (such as some playoff contests), you will receive points for any games that are played no later than the day after the last game scheduled as part of the contest. If game cancellations and/or postponements mean there is only one game left in a contest, then the contest will be canceled and all entry fees will be refunded.<br /><br />NOTE: A game will be considered to be 'played' on a given day if it begins on that day and is completed no later than 6am Eastern on the following day."
                            }
                            {
                                question: "How are contest scores calculated?"
                                answer: "See the <strong>Rules and Scoring</strong> tab for a breakdown of FanDuel scoring for each sport and for our detailed contest rules. "
                            }
                        ]
                    }
                ]
            opened: immutable.Set()

        @handleAction "tap.faqQuestion", @toggleQuestion

    toggleQuestion: (question) ->
        if @state.opened.contains(question)
            @setState opened: @state.opened.remove(question)
        else
            @setState opened: @state.opened.add(question)

    getSectionNames: ->
        _.pluck(@state.faq.sections, "name")

    getQuestions: (section) ->
        section = _.findWhere(@state.faq.sections, {name: section})
        return null unless section

        section.questions or []

    isQuestionOpened: (question) ->
        @state.opened.contains(question)


module.exports = FaqStore
