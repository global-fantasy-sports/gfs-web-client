_ = require('underscore')
{Store} = require('minimal-flux')

config = require('config')
flash = require('lib/flash')
xhr = require('lib/xhr')

class SubscriptionStore extends Store
    constructor: ->
        super()
        @state = options: immutable.Map()

        @handleAction("userAccount.changeSubscriptionOption", @setOption)
        @handleAction("userAccount.save", @saveOptions)


    getSubscriptions: (section) ->
        if @state.options.isEmpty()
            xhr({
                url: config.server + "/account/subscriptions/"
                method: "get"
                responseType: "json"
                callback: (err, data) =>
                    if err or data.error
                        return flash({
                            type: "error"
                            text: data.error or err
                        })
                    @setState({
                        options: immutable.fromJS(data.subscription_settings)
                        description: immutable.fromJS(data.subscription_settings_titles)
                    })
            })
            return []
        else
            options = @state.options.map((value, optKey) =>
                contains = @state.description.get(section).find((description, key) =>
                    key is optKey
                )

                if contains
                    return {
                        key: optKey
                        value: value,
                        description: @state.description.getIn([section, optKey])
                    }
            )
            return options.filter((o) -> o?)

    saveOptions: (options) ->
        options = options or @state.options
        xhr({
            url: config.server + "/account/subscriptions/"
            method: "post"
            data: JSON.stringify(subscription_settings: options.toJSON())
            callback: (err, data) =>
                if err or data.error
                    return flash({
                        type: "error"
                        text: data.error or err
                    })
        })

    setOption: (option, value) ->
        options = @state.options.set(option, value)

        @setState({options})
        @saveOptions(options)


module.exports = SubscriptionStore
