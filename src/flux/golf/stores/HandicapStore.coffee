{Store} = require('minimal-flux')
Immutable = require('immutable')


class HandicapStore extends Store

    constructor: () ->
        @state = {
            progress: 0
            activeIndex: 0
            handicaps: Immutable.Map()
        }

        @handleAction('wizard.init', @handleInit)
        @handleAction('handicap.selectParticipant', @handleSelectParticipant)
        @handleAction('handicap.toggleHandicap', @handleToggleHandicap)

    getHandicapStep: () ->
        {handicaps, progress, activeIndex} = @state
        lineup = @stores.roster.getLineup()
        participant = lineup.get(activeIndex)
        return {
            progress, activeIndex, lineup, participant
            finished: lineup.map((p) -> handicaps.get(p.id, Immutable.Set()).size is participant.handicap)
            handicap: handicaps.get(participant.id, Immutable.Set())
            yardages: participant.getRoundResult(1).get('yardages')
            pars: participant.getRoundResult(1).get('pars')
        }

    handleInit: () ->
        @setState({
            progress: 0
            activeIndex: 0
            handicaps: Immutable.Map()
        })

    handleSelectParticipant: (index) ->
        @setState({activeIndex: index})

    handleToggleHandicap: (index) ->
        {handicaps, activeIndex} = @state
        lineup = @stores.roster.getLineup()
        participant = lineup.get(activeIndex)
        @setState({
            handicaps: handicaps.update(participant.id, Immutable.Set(), (value) ->
                if value.has(index)
                    value.delete(index)
                else if value.size < participant.handicap
                    value.add(index)
            )
        })


module.exports = HandicapStore
