_ = require "underscore"
{Store} = require "minimal-flux"
{gravatar} = require "lib/utils.coffee"
{GAME_RECORD_BY_TYPE} = require "../records/GameRecord"


class GamesStore extends Store
    constructor: ->
        @state =
            games: immutable.Map()

        @handleAction "data.newGame", @setGame
        @handleAction "data.updateGame", @updateGame
        @handleAction "data.removeGameFromStore", @removeGame

    updateGame: (game) ->
        if @state.games.has(game.id)
            @setState({games: @state.games.update(game.id, (value) -> value.merge(game))})

    setGame: (game) ->
        recordClass = GAME_RECORD_BY_TYPE[game.gameType]
        gameRecord = new recordClass(game)

        if not gameRecord.userData.avatar
            gameRecord.userData.avatar = gravatar ''
        @setState
            games: @state.games.set(game.id, gameRecord)

    removeGame: (gameId) ->
        @setState
            games: @state.games.remove(gameId)

    getGames: (options = {}) ->
        if _.isEmpty(options)
            @state.games
        else
            @state.games.filter (game) ->
                for key, value of options
                    if game.get(key) != value
                        return false

                return true
            .sort (game1, game2) ->
                game2.points - game1.points

    getGame: (id) ->
        return @state.games.get(id, immutable.Map())

    getMyGames: ->
        userId = @stores.users.getCurrentUser().get('id')
        @state.games.filter((game) -> game.userId == userId)

    getActiveGamesCount: () ->
        @getMyGames().count((game) -> not game.isFinished())


    isAllowedToPlay: (gameType)->
        @stores.tournaments.getTournaments().some(((tour)=>
            tour.getCurrentRoundNumber() < 3 and @stores.rooms.getTournamentCompetitionRooms(tour.id).some((room)-> room.gameType is gameType)
        ))


module.exports = GamesStore
