_ = require("underscore")
{Store} = require("minimal-flux")

{Helpers} = require("lib/Helpers")


class ScoreCardsStore extends Store
    constructor: ->
        @state = {
            activeGameId: null
            activeRoundNumber: null
            game: {}
            activeParticipantId: null
            activeParticipant: {}
            participants: immutable.Map()
            pars: []
            strokes: []
            yardages: []
            bonuses: []
            predictions: []
            total: null
        }

        @handleAction("state.setActiveGameId", @handleActiveGameId)
        @handleAction("data.newGame", @handleNewGame)
        @handleAction("state.setActiveParticipantId", @handleActiveParticipantId)
        @handleAction("data.newParticipant", @handleNewParticipant)
        @handleAction("data.newParticipantRoundResult", @handleNewParticipantRoundResult)
        @handleAction("tap.toggleActiveRound", @handleActiveRound)

    handleActiveGameId: (activeGameId) ->
        game = @stores.games.getGame(activeGameId)
        @setState({activeGameId, game})
        @updateParticipants()

    handleActiveRound: (roundNumber) ->
        @setState({activeRoundNumber: roundNumber})
        @updateStats()

    handleNewGame: ->
        game = @stores.games.getGame(@state.activeGameId)
        if game
            @setState({game})
            @updateParticipants()
            @updateStats()

    handleActiveParticipantId: (activeParticipantId) ->
        activeParticipant = @stores.participants.getParticipant(activeParticipantId)
        @setState(activeParticipantId, activeParticipant)
        @updateParticipants()
        @updateStats()

    handleNewParticipant: ->
        activeParticipant = @stores.participants.getParticipant(@state.activeParticipantId)
        if activeParticipant
            @setState({activeParticipant})
            @updateParticipants()
            @updateStats()

    handleNewParticipantRoundResult: ->
        @updateParticipants()
        @updateStats()

    getActiveParticipant: -> @state.activeParticipant

    getActiveRoundNumber: ->
        if @state.activeRoundNumber
            roundNumber = @state.activeRoundNumber
        else if @state.game.id
            roundNumber = @state.game.getTournament().getCurrentRoundNumber()
            if roundNumber < @state.game.roundNumber
                roundNumber = @state.game.roundNumber
            if roundNumber > @state.game.roundNumber + 1
                roundNumber = @state.game.roundNumber + 1

        roundNumber

    updateParticipants: ->
        participants = []
        {game} = @state
        if game.id
            switch game.gameType
                when "handicap-the-pros", "hole-by-hole"
                    participants = [
                        @stores.participants.getParticipant(game.participantId1)
                        @stores.participants.getParticipant(game.participantId2)
                        @stores.participants.getParticipant(game.participantId3)
                    ]
                when "five-ball"
                    participants = [
                        @stores.participants.getParticipant(game.participantId1)
                        @stores.participants.getParticipant(game.participantId2)
                        @stores.participants.getParticipant(game.participantId3)
                        @stores.participants.getParticipant(game.participantId4)
                        @stores.participants.getParticipant(game.participantId5)
                    ]

        @setState({participants: immutable.List(participants)})

    getStatus: ->
        if @state.game.id then Helpers.statusName(@state.game) else ''

    getPars: -> @state.pars

    getStrokes: -> @state.strokes

    getYardages: -> @state.yardages

    getBonuses: -> @state.bonuses

    getPredictions: -> @state.predictions

    getTotal: -> @state.total

    getGame: -> @state.game

    getParticipants: -> @state.participants

    updateStats: ->
        game = @state.game
        pars = strokes = bonuses = predictions = yardages = []
        activeRoundNumber = @getActiveRoundNumber()

        switch game.gameType
            when "handicap-the-pros"
                results = @stores.participantRoundResults.getResult({
                    "participantId": @state.activeParticipantId
                    "roundNumber": activeRoundNumber
                })

                {pars, strokes} = results
                if game.participantId1 == @state.activeParticipantId then bonuses = game.handicapBonus1
                if game.participantId2 == @state.activeParticipantId then bonuses = game.handicapBonus2
                if game.participantId3 == @state.activeParticipantId then bonuses = game.handicapBonus3
            when "five-ball"
                results = @stores.participantRoundResults.getResult({
                    "participantId": @state.activeParticipantId
                    "roundNumber": activeRoundNumber
                })

                {pars, strokes} = results
            when "hole-by-hole"
                allResults = immutable.Map()

                @state.participants.forEach((p) =>
                    allResults = allResults.set(
                        p.id,
                        @stores.participantRoundResults.getResult({
                            "participantId": p.id
                            "roundNumber": activeRoundNumber
                        })
                    )
                )
                allPars = allResults.map((r) => r.pars)
                allStrokes = allResults.map((r) => r.strokes)

                allPredictions = immutable.Map()
                allPredictions = allPredictions.set(game.participantId1, game.prediction1)
                allPredictions = allPredictions.set(game.participantId2, game.prediction2)
                allPredictions = allPredictions.set(game.participantId3, game.prediction3)

                results = allResults.get(@state.activeParticipantId) || {}
                pars = results.pars || []
                strokes = results.strokes || []
                yardages = results.yardages || []
                predictions = allPredictions.get(@state.activeParticipantId, [])

                total = allStrokes.reduce((memo, partStrokes, i) ->
                    partPars = allPars.get(i)
                    partPrediction = allPredictions.get(i)

                    memo + partStrokes.reduce((memo2, stroke, j) ->
                        diff = stroke - partPars[j]
                        prediction = partPrediction[j]
                        if diff == prediction then return memo2 + 1

                        # because under par could be -2, -5, -8, over par 1, 3, 5. All negative numbers should be equal. The same with positive
                        if diff != 0 and prediction != 0 and diff * prediction > 0 then return memo2 + 1
                        return memo2
                    , 0)
                , 0)

        @setState({pars, strokes, yardages, bonuses, predictions, total})


module.exports = ScoreCardsStore
