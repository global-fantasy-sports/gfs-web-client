_ = require("underscore")
{Store} = require("minimal-flux")
Immutable = require("immutable")
{sum, settings} = require("lib/utils")


class WizardStore extends Store
    constructor: ->
        @init()

        @stores.participants.addListener("change", => @emit("change"))

        @handleAction("wizard.init", @init)
        @handleAction('roster.update', => @emit('change'))
        @handleAction("tap.selectParticipant", @selectParticipant)
        @handleAction("tap.unselectParticipant", @unselectParticipant)
        @handleAction("wizard.addFriendToChallengeList", @addFriendToChallengeList)
        @handleAction("wizard.removeFriendFromChallengeList", @removeFriendFromChallengeList)
        @handleAction("wizard.selectParticipant", @selectParticipant)
        @handleAction("wizard.setBonus", @setBonus)
        @handleAction("wizard.setCompetitionRoomId", @setCompetitionRoomId)
        @handleAction("wizard.setCurrentPrediction", @setCurrentPrediction)
        @handleAction("wizard.setCurrentSubstep", @setCurrentSubstep)
        @handleAction("wizard.setEvent", @setEvent)
        @handleAction("wizard.setGameModel", @setGameModel)
        @handleAction("wizard.setGameState", @setGameState)
        @handleAction("wizard.setPrediction", @setPrediction)
        @handleAction("wizard.setSubstepView", @setSubstepView)
        @handleAction("wizard.setTournamentId", @setTournamentId)
        @handleAction("wizard.setWizardGameType", @setWizardGameType)
        @handleAction('wizard.toggleCompetitionRoom', @setCompetitionRoomId)
        @handleAction("wizard.toggleHandicap", @updateBonuses)
        @handleAction("wizard.unlockPrediction", @unlockPrediction)
        @handleAction("wizard.unselectParticipants", @unselectParticipants)
        @handleAction("wizard.setInfoTooltipVisibility", @setTooltipVisibility)


    init: (game) ->
        if game
            gameState = 'editing'
            gameType = game.gameType
            bonuses = {}
            predictions = {}
            selectedParticipants = Immutable.Set()

            if gameType == "hole-by-hole"
                predictions[game.participantId1] = game.prediction1
                predictions[game.participantId2] = game.prediction2
                predictions[game.participantId3] = game.prediction3

            if gameType == "handicap-the-pros"
                bonuses[game.participantId1] = game.bonus1
                bonuses[game.participantId2] = game.bonus2
                bonuses[game.participantId3] = game.bonus3

            if gameType in ["handicap-the-pros", "hole-by-hole"]
                selectedParticipants = Immutable.Set([
                    game.participantId1
                    game.participantId2
                    game.participantId3
                ])
            else if gameType == "five-ball"
                selectedParticipants = Immutable.Set([
                    game.participantId1
                    game.participantId2
                    game.participantId3
                    game.participantId4
                    game.participantId5
                ])
            else
                logger.log("Unexpected game_type in WizardStore.init")


            @state = {
                selectedParticipants
                substepView: null
                predictions: Immutable.Map(predictions)
                bonuses: Immutable.Map(bonuses)
                isPredictionLocked: true
                currentSubstep: 1
                gameType
                gameModel: game
                tournamentId: game.tournamentId
                roundNumber: game.roundNumber
                selectedRoomIds: Immutable.Set(game.competitionRoomId)
                friendsToChallenge: Immutable.Set()
                gameState
                tooltipsVisibility: Immutable.Map()
            }
        else
            @state =
                gameState: 'new'
                selectedParticipants: Immutable.Set()
                substepView: null
                predictions: Immutable.Map()
                bonuses: Immutable.Map()
                isPredictionLocked: true
                currentSubstep: 1
                gameType: null
                gameModel: null
                tournamentId: null
                roundNumber: null
                selectedRoomIds: Immutable.Set()
                friendsToChallenge: Immutable.Set()
                tooltipsVisibility: Immutable.Map()

    getState: (wizardProps = {}) ->
        Object.assign({}, @state, {
            isStepDone: @isStepDone(wizardProps)
            selectedParticipants: @stores.roster.getSelectedIds()
        })

    isStepDone: ({gameDef, step}) ->
        stepDef = gameDef?[step]
        if stepDef and typeof stepDef.isDone is 'function'
            return stepDef.isDone()
        return true

    selectParticipant: (participantId) ->
        @setState({
            selectedParticipants: @state.selectedParticipants.add(participantId)
        })

    unselectParticipant: (participantId) ->
        @setState({
            selectedParticipants: @state.selectedParticipants.remove(participantId)
        })

    unselectParticipants: ->
        @setState({
            selectedParticipants: Immutable.Set()
        })

    getSelectedParticipantIds: ->
        @state.selectedParticipants

    getSelectedRoomIds: ->
        @state.selectedRoomIds

    setSubstepView: (view) ->
        @setState({
            substepView: view
        })

    getSubstepView: ->
        @state.substepView

    unlockPrediction: ->
        @setState({
            isPredictionLocked: false
        })

    isPredictionLocked: ->
        @state.isPredictionLocked

    setCurrentSubstep: (index) ->
        @setState({
            currentSubstep: index
        })

    getCurrentSubstep: ->
        @state.currentSubstep

    setCurrentPrediction: (prediction) ->
        @setState({
            predictions: @state.predictions.set(@getCurrentParticipant().id, prediction)
        })

    setPrediction: (id, prediction) ->
        @setState({
            predictions: @state.predictions.set(id, prediction)
        })

    setCurrentBonus: (bonus) ->
        @setState({
            bonuses: @state.bonuses.set(@getCurrentParticipant().id, bonus)
        })

    setBonus: (participantId, bonus) ->
        @setState({
            bonuses: @state.bonuses.set(participantId, bonus)
        })

    getCurrentPrediction: ->
        @state.predictions.get(@getCurrentParticipant().id) || Array.apply(null, new Array(18))

    getPrediction: (id) ->
        @state.predictions.get(id) || Array.apply(null, new Array(18))

    getCurrentBonus: () ->
        @state.bonuses.get(@getCurrentParticipant().id) || [1..18].map(-> false)

    getBonus: (id) ->
        @state.bonuses.get(id) || [1..18].map(-> false)

    isSubstepComplete: ->
        if @state.gameType == "hole-by-hole"
            return !_.contains(@getCurrentPrediction(), undefined)

        if @state.gameType == "handicap-the-pros"
            return @getCurrentHandicap() == @getCurrentBonus().reduce((memo, n) -> memo + n)

        false

    updateBonuses: (n) ->
        if sum(@getCurrentBonus()) >= @getCurrentHandicap() and not @getCurrentBonus()[n]
            return

        bonus = @getCurrentBonus().slice()
        bonus[n] = !bonus[n]
        @setCurrentBonus(bonus)

    getCurrentPars: ->
        @getCurrentParticipant().getRoundResult(1).get("pars")

    getCurrentYardages: ->
        @getCurrentParticipant().getRoundResult(1).get("yardages")

    setWizardGameType: (gameType) ->
        @setState({gameType})

    setGameModel: (gameModel) ->
        @setState({gameModel})

    getCurrentHandicap: ->
        @getCurrentParticipant().handicap

    getWizardGameType: ->
        @state.gameType

    getCurrentParticipant: ->
        participantId = @state.selectedParticipants.toArray()[@getCurrentSubstep() - 1]
        @stores.participants.getParticipant(participantId)

    setTournamentId: (tournamentId) ->
        unless @state.tournamentId is tournamentId
            @setEvent(tournamentId, null)

    setEvent: (tournamentId, roundNumber) ->
        @setState({
            tournamentId
            roundNumber
            selectedParticipants: new Immutable.Set()
            bonuses: new Immutable.Map()
            predictions: new Immutable.Map()
        })

    getTournamentId: ->
        @state.tournamentId

    getRoundNumber: ->
        @state.roundNumber

    setCompetitionRoomId: (id) ->
        {selectedRoomIds} = @state

        selectedRoomIds =
            if selectedRoomIds.has(id)
                selectedRoomIds.delete(id)
            else
                selectedRoomIds.add(id)

        @setState({selectedRoomIds})

    getCompetitionRoomId: ->
        @state.selectedRoomIds.first()

    addFriendToChallengeList: (friendId) ->
        @setState({friendsToChallenge: @state.friendsToChallenge.add(friendId)})

    removeFriendFromChallengeList: (friendId) ->
        @setState({friendsToChallenge: @state.friendsToChallenge.delete(friendId)})

    getListOfFriendsToChallenge: ->
        return @state.friendsToChallenge

    setGameState: (gameState) ->
        @setState({gameState})

    getGameState: ->
        @state.gameState

    setTooltipVisibility: (settingsName, isVisible) ->
        settings("info-tooltip-#{settingsName}", isVisible)
        @setState tooltipsVisibility: @state.tooltipsVisibility.set(settingsName, isVisible)

    isInfoTooltipVisible: (settingsName) ->
        settings("info-tooltip-#{settingsName}") ? true


module.exports = WizardStore
