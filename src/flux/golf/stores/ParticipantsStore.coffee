{Store} = require('minimal-flux')
Immutable = require('immutable')
ParticipantRecord = require('../records/ParticipantRecord')
StatRecord = require('../records/StatRecord')
{ROLE_STATS} = require('../../../components/const/golf')


getValue = (map, key, empty) ->
    result = if typeof map[key] is 'function' then map[key]() else map.get(key)
    if not result? or result is 'N/A' then empty else result


BOUNDARIES_STATS = [
    ROLE_STATS[0]...
    ROLE_STATS[1]...
]


class ParticipantsStore extends Store
    constructor: ->
        @state = {
            boundaries: Immutable.Map()
            participants: Immutable.Map()
            stats: Immutable.Map()
        }

        @handleAction('data.initialData', @handleInitialData)
        @handleAction("data.newParticipant", @handleNewParticipant)
        @handleAction("data.clearParticipants", @handleClearParticipants)
        @handleAction("data.fetchParticipantStats", @handleFetchParticipantStats)

    handleInitialData: (data) ->
        boundaries = Immutable.Map()
        participants = Immutable.Seq(data.participants or [])
            .map((p) -> new ParticipantRecord(p)).cacheResult()

        participants.forEach((p) ->
            for stat in BOUNDARIES_STATS
                value = p.getAverage(stat)
                if value > boundaries.getIn(['max', stat], -Infinity)
                    boundaries = boundaries.setIn(['max', stat], value)
                if value < boundaries.getIn(['min', stat], Infinity)
                    boundaries = boundaries.setIn(['min', stat], value)
        )

        @setState({
            boundaries
            participants: participants.toKeyedSeq().mapKeys((k, p) -> p.id).toMap()
        })

    getParticipant: (id, emptyValue = new ParticipantRecord()) ->
        @state.participants.get(id, emptyValue)

    getParticipants: (params = {}) ->
        {country, filter, sort, reversed, limit, tournamentId} = params
        participants = @state.participants.toIndexedSeq()

        tournamentId ?= @stores.wizard.getTournamentId()
        if tournamentId
            participants = participants.filter((p) ->
                p.tournamentId is tournamentId
            )

        if country and country isnt 'all'
            participants = participants.filter((p) ->
                p.get('country') is country
            )

        if filter
            filter = filter.toLowerCase()
            participants = participants.filter((p) ->
                p.getFullName().toLowerCase().indexOf(filter) isnt -1
            )

        if sort
            participants = participants.sortBy((p) -> getValue(p, sort, +Infinity))
            participants = participants.reversed() if reversed

        if limit
            participants = participants.take(limit)

        participants.toList()

    getLeaderboard: (tournamentId) ->
        @getParticipants({
            tournamentId
            order: 'rank'
        })

    getCountryCodes: ->
        @getParticipants().reduce(((result, p) ->
            country = p.get('country')
            if country then result.add(country) else result
        ), Immutable.Set())

    getStats: (golferId) ->
        @state.stats.get(golferId, Immutable.List())

    getMinStat: (statName) ->
        @state.boundaries.getIn(['min', statName], 0)

    getMaxStat: (statName) ->
        @state.boundaries.getIn(['max', statName], 0)

    handleNewParticipant: (participant) ->
        b = @state.boundaries
        for statName in BOUNDARIES_STATS
            currentMin = b.getIn(['min', statName], Infinity)
            currentMax = b.getIn(['max', statName], -Infinity)
            value = participant.statInfoHistory[statName]

            if value < currentMin
                b = b.setIn(['min', statName], value)
            if value > currentMax
                b = b.setIn(['max', statName], value)

        @setState(
            participants: @state.participants.set(participant.id, new ParticipantRecord(participant))
            boundaries: b
        )

    handleClearParticipants: ->
        @setState(participants: Immutable.Map())

    handleFetchParticipantStats: (golferId, data) ->
        @setState({
            stats: @state.stats.set(golferId, new List(data.map((x) -> new StatRecord(x))))
        })


module.exports = ParticipantsStore
