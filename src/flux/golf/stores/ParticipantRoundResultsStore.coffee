_ = require('underscore')
{Store} = require('minimal-flux')
Immutable = require('immutable')
ParticipantRoundResult = require('../records/ParticipantRoundResultsRecord')


class ParticipantRoundResultsStore extends Store
    constructor: ->
        @state = {
            results: Immutable.Map()
        }

        @handleAction('data.initialData', @handleInitialData)
        @handleAction('data.newParticipantRoundResult', @handleNewParticipantRoundResult)

    handleInitialData: (data) ->
        results = _.chain(data.participants).pluck('participantRoundResults').flatten(true).value()

        results = Immutable.Seq(results).toKeyedSeq()
            .mapEntries(([k, v]) -> [v.id, new ParticipantRoundResult(v)])
            .toMap()

        @setState({results})

    handleNewParticipantRoundResult: (result) ->
        @setState(
            results: @state.results.update(result.id, (el) ->
                if el
                    el.merge(result)
                else
                    new ParticipantRoundResult(result)
            )
        )
    getResult: (param) ->
        if _.isNumber(param)
            id = param
            @state.results.get(id, new ParticipantRoundResult())

        if _.isObject(param)
            @state.results.find(
                ((el) ->
                    for key, value of param
                        if el.get(key) != param[key]
                            return false
                    true
                ), null, new ParticipantRoundResult()
            )


module.exports = ParticipantRoundResultsStore
