{Store} = require('minimal-flux')
CommonSearchStore = require('../../common/stores/SearchStore')


class SearchStore extends CommonSearchStore
    constructor: ->
        super
        state = {
            name: null
            country: null
        }

        Object.assign(@state, state)

        @handleAction('search.setName', @setName)
        @handleAction('search.setCountry', @setCountry)

    setName: (name) ->
        @setState({name})

    setCountry: (country) ->
        @setState({country})

    getName: -> @state.name

    getCountry: -> @state.country


module.exports = SearchStore
