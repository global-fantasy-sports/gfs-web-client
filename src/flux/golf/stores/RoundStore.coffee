_ = require "underscore"
{Store} = require "minimal-flux"
{fromUtcTs} = require "lib/date"


class RoundStore extends Store
    constructor: ->
        @state =
            rounds: immutable.Map()

        @handleAction "data.newRound", @setRound

    setRound: (round) ->
        round.start_time_utc = fromUtcTs(round.start_time_utc)
        @setState rounds: @state.rounds.set(round.id, immutable.fromJS(round))

    getRound: (id) ->
        @state.rounds.get(id.toString())

    getRounds: (options) ->
        unless options then return @state.rounds

        @state.rounds.filter (r) ->
            for key, value of options
                if r.get(key) != value
                    return false

            return true


module.exports = RoundStore
