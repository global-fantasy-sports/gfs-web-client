_ = require('underscore')
Immutable = require('immutable')
{Store} = require('minimal-flux')

TournamentRecord = require("../records/TournamentRecord")


class TournamentsStore extends Store
    constructor: ->
        @state = {
            tournaments: Immutable.Map()
        }

        @handleAction('data.initialData', @handleInitialData)
        @handleAction('data.setTournament', @setTournament)
        @handleAction('data.updateTournament', @updateTournament)

    handleInitialData: (data) ->
        tournaments = Immutable.Seq(data.tournaments or []).toKeyedSeq()
            .mapEntries(([k, v]) -> [v.id, new TournamentRecord(v)])
            .toMap()

        @setState({tournaments})

    setTournament: (tournament) ->
        @setState({
            tournaments: @state.tournaments.set(tournament.id, new TournamentRecord(tournament))
        })

    updateTournament: (tournament) ->
        @setState({
            tournaments: @state.tournaments.update(tournament.id, (tour) -> tour.merge(tournament) if tour)
        })

    getTournaments: ->
        @state.tournaments

    getTournamentsAllowedToStart: (gameType) ->
        tournaments = @state.tournaments.toIndexedSeq().filter((tour) => tour.isAllowedToStart())

        if gameType
            tournaments = tournaments.filterNot((tour) =>
                @stores.rooms.getTournamentCompetitionRooms(tour.id).filter((room) -> room.gameType is gameType).isEmpty()
            )

        tournaments.toList()

    getTournament: (id) ->
        @state.tournaments.get(id, new TournamentRecord())

    getTournamentsByMonth: () ->
        @state.tournaments.toIndexedSeq().sortBy((tour) ->
            tour.startDate
        ).groupBy((tour) ->
            tour.getTournamentPeriod().month
        )

module.exports = TournamentsStore
