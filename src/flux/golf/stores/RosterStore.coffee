{Store} = require('minimal-flux')
Immutable = require('immutable')


class RosterStore extends Store

    constructor: () ->
        @state = {
            lineup: Immutable.List.of(false)
        }
        @handleAction('wizard.setEvent', @init)
        @handleAction('roster.filterByTier', @handleFilterByTier)
        @handleAction('roster.toggleParticipant', @handleToggleParticipant)
        @handleAction('roster.removeParticipant', @handleRemoveParticipant)

    getRoster: () ->
        participants = @_getParticipants()
        {tier, lineup, sorting, reversed} = @state

        selected = lineup.toSeq().filter(Boolean).map((p) -> p.id).toSet()
        open =
            if @stores.wizard.getWizardGameType() is 'five-ball'
                not lineup.get(tier)
            else
                lineup.size isnt selected.size


        return {
            tier, lineup, selected, sorting, reversed
            participants, open, selected
        }

    isValid: () ->
        @state.lineup.every(Boolean)

    getSelectedIds: () ->
        @state.lineup.filter(Boolean).map((p) -> p.id)

    getLineup: () ->
        @state.lineup

    init: (tournamentId = null) ->
        size = if @stores.wizard.getWizardGameType() is 'five-ball' then 5 else 3
        lineup = Immutable.Range(0, size).map(-> false).toList()

        @setState({
            tier: 0
            sorting: 'rank'
            reversed: false
            lineup: lineup
            selected: Immutable.Set()
            _participants: null
        })

    _getParticipants: () ->
        {_participants} = @state
        {tournamentId, gameType} = @stores.wizard.getState()
        if not _participants
            _participants = @stores.participants.getParticipants({
                tournamentId
                sort: 'rank'
            })
            @setState({_participants}, {silent: true})

        if gameType is 'five-ball'
            _participants.toSeq().skip(@state.tier * 10).take(10).toList()
        else
            _participants

    handleFilterByTier: (tier) ->
        @setState({tier: Math.max(0, Math.min(tier, 4))})

    handleToggleParticipant: (participant) ->
        {lineup, tier} = @state

        currentPosition = lineup.findIndex((p) -> p.id is participant.id)
        openPosition = lineup.findIndex((p) -> not p)

        if currentPosition isnt -1
            lineup = lineup.set(currentPosition, false)
        else if @stores.wizard.getWizardGameType() is 'five-ball'
            lineup = lineup.set(tier, participant)
        else if openPosition isnt -1
            lineup = lineup.set(openPosition, participant)

        @setState({lineup})

    handleRemoveParticipant: (id) ->
        @setState({
            lineup: @state.lineup.map((p) ->
                if p.id is id then false
                else p
            )
        })

module.exports = RosterStore
