{Actions} = require('minimal-flux')


class HandicapActions extends Actions

    selectParticipant: (index) ->
        @dispatch('selectParticipant', index)

    toggleHandicap: (n) ->
        @dispatch('toggleHandicap', n)



module.exports = HandicapActions
