_ = require('underscore')
{Store} = require('minimal-flux')
Immutable = require('immutable')

DEFAULT_POSITIONS = ['All', 'QB', 'RB', 'TE', 'WR']


splitName = (name) ->
    index = name.indexOf('/')
    [name[0...index], name[(index + 1)..]]


ColumnRecordBase = Immutable.Record({
    index: null
    name: null
    category: ''
    column: ''
    label: ''
    abbr: ''
    ordering: 0
    important: false
    highlight: false
    positions: Immutable.Set(DEFAULT_POSITIONS)
}, 'ColumnRecord')


class ColumnRecord extends ColumnRecordBase
    constructor: (props) ->
        {category, column, name} = props
        if not column
            [category, column] = splitName(name)

        super(Object.assign(props, {
            name, category, column
            abbr: props.abbr or column.replace('_', ' ').toUpperCase()
            positions: Immutable.Set(props.positions or DEFAULT_POSITIONS)
        }))

        @getCellValue = @getCellValue.bind(this)

    getCellValue: (row) ->
        switch @name
            when 'player/salary'
                row.getPlayer().salary
            when 'player/fppg'
                row.getPlayer().FPPG
            when 'player/photo'
                row.getPlayer().smallPhoto
            when 'player/favorite'
                row.getPlayer().isFavorite
            else
                row.getIn(['values', @index])


ColumnGroupRecord = Immutable.Record({
    name: null
    label: ''
    ordering: 1
    items: Immutable.List()
    expanded: false
}, 'ColumnGroupRecord')

ColumnGroupRecord::getItems = (expanded) ->
    expanded ?= @expanded

    items = @items.toIndexedSeq()
    if not expanded
        items = items.filter((item) -> item.important)

    items.toList()

DataRowRecord = Immutable.Record({
    playerId: null,
    values: Immutable.List()
}, 'DataRowRecord')

DataRowRecord::getPlayer = () ->
    flux.stores.participants.getPlayer(@playerId)


Sorting = Immutable.Record({
    field: 'player/name'
    reversed: false
}, 'Sorting')

Sorting::getDirection = () ->
    if @reversed
        'desc'
    else
        'asc'

Sorting::setField = (field) ->
    if @field is field
        new Sorting({field: @field, reversed: not @reversed})
    else
        new Sorting({field: field})

Sorting::getDisplayLabel = () ->
    @field


FilterRecord = Immutable.Record({
    column: null
    start: 0
    end: 0
    max: 0
    min: 0
}, 'FilterRecord')

FilterRecord::initValues = (players) ->
    min = 0
    max = 0

    players.forEach((row) =>
        value = @column.getCellValue(row)
        if typeof value is 'number'
            max = Math.max(value, max)
            min = Math.min(value, min)
    )

    @merge({
        min, max
        start: min
        end: max
    })

FilterRecord::apply = (players) ->
    {column, start, end} = this
    if start isnt @min or end isnt @max
        players.filter((row) -> start <= column.getCellValue(row) <= end)
    else
        players


class StatisticsStore extends Store
    constructor: () ->
        @state = {
            status: 'idle'
            error: null
            viewMode: 'cards'
            showFavorites: false
            availableFilters: Immutable.List()
            filters: Immutable.List()
            allPlayers: Immutable.List()
            players: Immutable.List()
            columns: Immutable.Map()
            columnGroups: Immutable.Map()
            search: ''
            sorting: new Sorting()
            position: 'All'
            maxFPPG: 50
        }

        @handleAction('statistics.fetchPlayers', @handleFetchStart)
        @handleAction('statistics.processPlayersResponse', @handleProcessPlayers)
        @handleAction('statistics.error', @handleError)
        @handleAction('statistics.sort', @handleSort)
        @handleAction('statistics.addFilter', @handleAddFilter)
        @handleAction('statistics.removeFilter', @handleRemoveFilter)
        @handleAction('statistics.clearFilters', @handleClearFilters)
        @handleAction('statistics.filter', @handleFilter)
        @handleAction('statistics.filterPosition', @handleFilterPosition)
        @handleAction('statistics.search', @handleSearch)
        @handleAction('statistics.toggleCategory', @handleToggleCategory)
        @handleAction('statistics.toggleViewMode', @handleToggleViewMode)
        @handleAction('statistics.toggleFavorites', @handleToggleFavorites)

    getColumn: (name) ->
        @state.columns.get(name)

    getPlayersList: () -> {
        availableFilters: @state.availableFilters
        columnGroups: @state.columnGroups.valueSeq().rest().toList()
        columns: @state.columns
        error: @state.error
        filters: @state.filters
        inProgress: @state.status in ['idle', 'busy']
        maxFPPG: @state.maxFPPG
        minFPPG: @state.minFPPG
        players: @state.players
        position: @state.position
        search: @state.search
        sorting: @state.sorting
        viewMode: @state.viewMode
        showFavorites: @state.showFavorites
    }

    handleFetchStart: () ->
        @setState({status: 'busy', error: null})

    handleError: (error) ->
        @setState({status: 'error', error})

    handleProcessPlayers: ({categories, columns, data}) ->
        columns = _.indexBy(columns, (col) -> col.name)

        columns = Immutable.OrderedMap(data[0][1..].map((name, index) ->
            col = Object.assign({index, name}, columns[name])
            [name, new ColumnRecord(col)]
        ))
        columns = columns.set("player/photo", new ColumnRecord({name: "player/photo"}))

        columnGroups = Immutable.OrderedMap(categories.map((cat) ->
            items = columns.filter((col) -> col.category is cat.name).sortBy((col) -> col.ordering).toList()
            Object.assign(cat, {items})
            [cat.name, new ColumnGroupRecord(cat)]
        ))

        allPlayers = Immutable.List(data[1..].map(([id, row...]) ->
            new DataRowRecord({
                playerId: id
                values: Immutable.List(row)
            })
        ))

        salaryColumn = columns.get("player/salary")
        allPlayers = allPlayers.filter((row) -> salaryColumn.getCellValue(row) > 0)

        col = columns.get('player/position')
        allPlayers = allPlayers.filter((row) -> col.getCellValue(row) != 'FB')

        availableFilters = columns
            .filter((col) -> col.category isnt 'player' or col.column in ['salary', 'fppg'])
            .map((column) -> new FilterRecord({column}))

        fppgColumn = columns.get("player/fppg")

        fppg = allPlayers.map((row) -> fppgColumn.getCellValue(row))

        @setState({
            status: 'done'
            columns
            columnGroups
            availableFilters
            filters: Immutable.List()
            allPlayers
            players: allPlayers
            maxFPPG: fppg.max()
            minFPPG: fppg.min()
        })

    handleSort: (field) ->
        @_updateState({sorting: @state.sorting.setField(field.name)})

    handleSearch: (search) ->
        @_updateState({search: search or ''})

    handleAddFilter: (filter) ->
        @_updateState({
            filters: @state.filters.push(filter.initValues(@state.allPlayers))
        })

    handleRemoveFilter: (filter) ->
        @_updateState({filters: @state.filters.filterNot(({column}) ->
            Immutable.is(column, filter.column))
        })

    handleClearFilters: () ->
        @_updateState({
            filters: @state.filters.clear()
            position: 'All'
            search: ''
            sorting: new Sorting()
        })

    handleFilterPosition: (position) ->
        @_updateState({position})

    _updateState: (newState) ->
        @setState(newState, {silent: true})

        {allPlayers, columns, position} = @state

        players = allPlayers.toIndexedSeq()

        if position == 'FAV'
            col = columns.get('player/favorite')
            players = players.filter((row) -> col.getCellValue(row))
        else if position isnt 'All'
            col = columns.get('player/position')
            players = players.filter((row) -> col.getCellValue(row) is position)

        {search} = @state
        search = search.trim().toLowerCase()
        nameCol = columns.get('player/name')
        if search
            players = players.filter((row) ->
                nameCol.getCellValue(row).toLowerCase().indexOf(search) isnt -1
            )

        {filters} = @state
        if filters.size
            players = filters.reduce(((players, filter) -> filter.apply(players)), players)

        {sorting} = @state
        sortColumn = columns.get(sorting.field)
        players = players.sortBy((row) -> sortColumn.getCellValue(row))
        players = players.reverse() if sorting.reversed

        @setState({
            players: players.toList()
        })

    handleFilter: ({column}, [start, end]) ->
        {filters} = @state
        index = filters.findIndex((item) -> Immutable.is(item.column, column))
        @_updateState({filters: filters.mergeIn([index], {start, end})})

    handleToggleCategory: (name) ->
        @setState({
            columnGroups: @state.columnGroups.updateIn([name, 'expanded'], (value) -> not value)
        })

    handleToggleViewMode: (viewMode) ->
        @setState({viewMode})

    handleToggleFavorites: () ->
        @setState({showFavorites: not @state.showFavorites})

module.exports = StatisticsStore
