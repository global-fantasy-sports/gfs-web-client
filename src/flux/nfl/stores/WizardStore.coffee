{Store} = require('minimal-flux')
Immutable = require('immutable')
{settings} = require("lib/utils")


class WizardStore extends Store
    constructor: ->
        @state = {
            game: null
            gameState: 'idle'
            friendsToChallenge: Immutable.Set()
            selectedDate: null
            sportEventId: null
            selectedRoomIds: Immutable.Set()
            selectedRoomsTotalPrice: Immutable.Map({usd: 0, token: 0})
            isInitialRoom: false
            challengeRequestRoomId: null
            tooltipsVisibility: Immutable.Map()
        }

        @handleAction('wizard.init', @handleInit)
        @handleAction('roster.update', => @emit('change'))
        @handleAction('roster.init', =>
            setTimeout =>
                @emit('change')
            , 1
        )

        @handleAction('tap.editGame', @handleInitEdit)
        @handleAction('wizard.setCompetitionRoomId', @handleCompetitionRoomId)
        @handleAction('wizard.setGameState', @handleGameState)

        @handleAction('wizard.addFriendToChallengeList', @handleAddFriendToChallengeList)
        @handleAction('wizard.removeFriendFromChallengeList', @handleRemoveFriendFromChallengeList)
        @handleAction('wizard.selectEvent', @selectEvent)
        @handleAction('roster.toggleParticipant', () => @emit('change'))

        @handleAction('wizard.toggleCompetitionRoom', @toggleCompetitionRoom)
        @handleAction("wizard.setInfoTooltipVisibility", @setTooltipVisibility)

    handleInit: (props) ->
        @setState(Object.assign({}, {
            game: null
            gameState: 'idle'
            friendsToChallenge: Immutable.Set()
            selectedDate: null
            sportEventId: null
            isInitialRoom: false
            challengeRequestRoomId: null
            selectedRoomIds: Immutable.Set()
            selectedRoomsTotalPrice: Immutable.Map({usd: 0, token: 0})
        }, props))

    handleInitEdit: (game) ->
        room = @stores.rooms.getRoom(game.competitionRoomId)
        @setState({
            game: game
            selectedRoomIds: Immutable.Set([game.competitionRoomId])
            friendsToChallenge: Immutable.Set()
            selectedDate: null
            sportEventId: room.sportEventId
        })

    handleCompetitionRoomId: (roomId, options) ->
        state = {
            selectedRoomIds: @state.selectedRoomIds.add(roomId)
        }

        if options.init
            state.isInitialRoom = true

        @setState state

    handleGameState: (gameState) ->
        @setState({gameState})

    handleAddFriendToChallengeList: (friendId) ->
        @setState(friendsToChallenge: @state.friendsToChallenge.add(friendId))

    handleRemoveFriendFromChallengeList: (friendId) ->
        @setState(friendsToChallenge: @state.friendsToChallenge.delete(friendId))

    toggleCompetitionRoom: (id) ->
        room = @stores.rooms.getRoom(id)
        price = @state.selectedRoomsTotalPrice

        multiplier = if @state.selectedRoomIds.has(id) then -1 else 1

        if room.mode == 'usd'
            newPrice = price.set('usd', price.get('usd') + multiplier * room.entryFee)

        if room.mode == 'token'
            newPrice = price.set('token', price.get('token') + multiplier * room.entryFee)

        if @state.selectedRoomIds.has(id)
            @setState({
                selectedRoomIds: @state.selectedRoomIds.remove(id)
                selectedRoomsTotalPrice: newPrice
            })
        else
            @setState({
                selectedRoomIds: @state.selectedRoomIds.add(id)
                selectedRoomsTotalPrice: newPrice
            })

    getState: (wizardProps) ->
        Object.assign({
            isStepDone: @isStepDone(wizardProps)
            errorCode: @getErrorCode(wizardProps)
            nextStepLabel: @getNextStepLabel(wizardProps)
        }, @state)

    getNextStepLabel: (wizardProps) ->
        switch wizardProps?.step
            when 'friends'
                return 'Finish'
            when 'stake'
                if @state.selectedRoomIds.count() > 1
                    return "Pay for #{@state.selectedRoomIds.count()} games"
                else
                    return 'Pay'
            else
                return 'Next'

    isEnoughMoney: (wizardProps) ->
        balance = @stores.users.getUserBalance()
        if wizardProps?.step == 'stake'
            return @state.selectedRoomsTotalPrice.get('usd') <= balance.usd &&
                    @state.selectedRoomsTotalPrice.get('token') <= balance.token
        else return true

    isStepDone: (wizardProps = {}) ->
        stepDef = wizardProps.gameDef?[wizardProps.step]
        isEnoughMoney = @isEnoughMoney(wizardProps)

        if stepDef and typeof stepDef.isDone is 'function'
            return stepDef.isDone() && isEnoughMoney
        return true

    getErrorCode: (wizardProps) ->
        if wizardProps?.step == 'stake'
            balance = @stores.users.getUserBalance()

            if @state.selectedRoomsTotalPrice.get('usd') > balance.usd
                return 'NOT_ENOUGH_MONEY'

            if @state.selectedRoomsTotalPrice.get('token') > balance.token
                return 'NOT_ENOUGH_TOKENS'

    getSelectedRoomIds: ->
        @state.selectedRoomIds

    getCurrentSubstep: ->
        1

    getTournamentId: ->
        undefined

    getWizardGameType: ->
        'salarycap'

    getGameState: ->
        @state.gameState

    getCompetitionRoomId: ->
        @state.selectedRoomIds.first()

    getListOfFriendsToChallenge: ->
        @state.friendsToChallenge

    selectEvent: (date, id) ->
        @setState({selectedDate: date, sportEventId: id})

    getSelectedEvent: ->
        {date: @state.selectedDate, id: @state.sportEventId}

    getGameId: ->
        @state.game?.id

    isInitialRoom: ->
        @state.isInitialRoom

    setTooltipVisibility: (settingsName, isVisible) ->
        settings("info-tooltip-#{settingsName}", isVisible)
        @setState tooltipsVisibility: @state.tooltipsVisibility.set(settingsName, isVisible)

    isInfoTooltipVisible: (settingsName) ->
        settings("info-tooltip-#{settingsName}") ? true


module.exports = WizardStore
