_ = require('underscore')
Immutable = require('immutable')
{Store} = require('minimal-flux')
Participant = require('../records/ParticipantRecord')
Player = require('../records/PlayerRecord')
Team = require('../records/TeamRecord')
{ROLE_STATS} = require('components/const')


class ParticipantsStore extends Store

    constructor: ->
        @state = {
            teams: Immutable.Map()
            participants: Immutable.Map()
            players: Immutable.Map()
            defenseTeams: Immutable.Map()
            overachievers: Immutable.List()
            underachievers: Immutable.List()
            boundaries: Immutable.Map()
        }

        @handleAction('data.initialData', @handleInitialData)
        @handleAction('data.newContest', @recalculateOpponents)
        @handleAction('data.addFavorite', @setFavorite)
        @handleAction('data.deleteFavorite', @unsetFavorite)
        @handleAction('data.updateFPPG', @updateFPPG)

    handleInitialData: (initial) ->
        [players, participants, boundaries] = @parseParticipants(initial)

        teams =
            if initial.teams
                Immutable.Map(initial.teams.map((t) -> [t.id, new Team(t)]))
            else
                Immutable.Map()

        defenseTeams =
            if initial.nflDefenseTeams
                Immutable.Map(initial.nflDefenseTeams.map((t) ->
                    id = t.id
                    team = Object.assign({position: 'DST', id, playerId: t.team}, t)
                    [id, new Participant(team)]
                ))
            else
                Immutable.Map()

        overachievers =
            if initial.overachievers
                Immutable.fromJS(initial.overachievers)
            else
                @state.overachievers

        underachievers =
            if initial.underachievers
                Immutable.fromJS(initial.underachievers)
            else
                @state.underachievers

        @setState({
            players, boundaries
            overachievers, underachievers
            teams: @state.teams.merge(teams)
            defenseTeams: @state.defenseTeams.merge(defenseTeams)
            participants: participants.merge(defenseTeams)
        })

    parseParticipants: (initial) ->
        players =
            if initial.sportsmen
                Immutable.Map(initial.sportsmen.map((p) ->
                    [p.id, new Player(p)]
                ))
            else
                Immutable.Map()

        players = @state.players.merge(players)

        participants =
            if initial.participants
                Immutable.Map(initial.participants.map((p) ->
                    player = players.get(p.sportsmanId)
                    participant = new Participant(Object.assign({}, _(player.toJS()).omit('id'), p))
                    [p.id, participant]
                ))
            else
                Immutable.Map()

        boundaries = @state.boundaries
        statNames = _.chain(ROLE_STATS).values().push('games').flatten().value()

        participants.forEach((p) ->
            for stat in statNames
                value = p.getAverage(stat)
                if value > boundaries.getIn(['max', stat], -Infinity)
                    boundaries = boundaries.setIn(['max', stat], value)
                if value < boundaries.getIn(['min', stat], Infinity)
                    boundaries = boundaries.setIn(['min', stat], value)
        )

        participants = @state.participants.merge(participants)
        [players, participants, boundaries]

    recalculateOpponents: (contest) ->
        participants = @state.participants.map((p) ->
            if p.gameId == contest.id
                if p.team == contest.awayTeam
                    return p.set('opponent', contest.homeTeam)
                else
                    return p.set('opponent', contest.awayTeam)
            else return p
        )

        @setState({participants})

    getParticipants: (options) ->
        participants = @state.participants.toIndexedSeq()

        if !_.isEmpty(options)
            participants = participants.filter((p) ->
                for key, value of options
                    if value != -1 && p.get(key) != value
                        return false

                return true
            )

        participants.cacheResult()

    getFavorites: () ->
        @state.participants.toIndexedSeq().filter((p) -> p.isFavorite).toList()

    getPlayerParticipant: (id, sportEventId = -1, position = null) ->
        participants = @getParticipants({playerId: id, sportEventId})
        if participants.size > 0
            participant = participants.get(0)
        else
            if position == 'DST'
                t = @getTeam(id)
                if t
                    team = Object.assign({position: 'DST', id, playerId: t.id, team: id, smallPhoto: t.logo}, t)
                    participant = new Participant(team)
            else
                player = @getPlayer(id)
                participant = new Participant(player)

        participant

    getMissingParticipant: (id) ->
        player = @getPlayer(id)
        participant = new Participant(player)
        participant.set('playerId', participant.id)

    getParticipant: (id) ->
        @state.participants.get(id)

    getTeam: (id) ->
        @state.teams.get(id)

    getOverachievers: ->
        @state.overachievers.sortBy((el) -> -el.get('points'))

    getUnderachievers: ->
        @state.underachievers.sortBy((el) -> -el.get('points'))

    getPlayers: ->
        @state.players

    getPlayer: (id) ->
        @state.players.get(id)

    setFavorite: (playerId) ->
        {participants, players} = @state
        changed = participants.toIndexedSeq().filter((p) -> p.playerId is playerId).map((p) -> p.id).toArray()
        @setState({
            players: players.setIn([playerId, 'isFavorite'], true)
            participants: participants.withMutations((coll) ->
                changed.forEach((id) -> coll.setIn([id, 'isFavorite'], true))
            )
        })

    unsetFavorite: (playerId) ->
        {participants, players} = @state
        changed = participants.toIndexedSeq().filter((p) -> p.playerId is playerId).map((p) -> p.id).toArray()
        @setState({
            players: players.setIn([playerId, 'isFavorite'], false)
            participants: participants.withMutations((coll) ->
                changed.forEach((id) -> coll.setIn([id, 'isFavorite'], false))
            )
        })

    getMinStat: (statName) ->
        @state.boundaries.getIn(['min', statName], 0)

    getMaxStat: (statName) ->
        @state.boundaries.getIn(['max', statName], 0)

    updateFPPG: (data) ->
        participants = @state.participants
        for player_data in _.union(data.players or [], data.teams or [])
            playerID = player_data.playerId
            sportEventId = player_data.sportEventId
            participant = @getPlayerParticipant(playerID, sportEventId)
            if participant?.id
                participants = participants.update(participant.id, (p) ->
                    p = p.set('points', player_data.fppg)
                         .set('pointsDetails', player_data.calcInfo)
                    p
                )
        @setState({participants})

module.exports = ParticipantsStore
