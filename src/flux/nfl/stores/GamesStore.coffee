_ = require('underscore')
{Store} = require("minimal-flux")
GameRecord = require('../records/GameRecord')
Contest = require('../records/ContestRecord')


class GamesStore extends Store
    constructor: ->
        @state =
            games: immutable.Map()
            contests: immutable.OrderedMap()

        @handleAction('data.newGame', @handleNewGame)
        @handleAction('data.newGames', @handleNewGames)
        @handleAction('data.initialData', @handleProcessContests)
        @handleAction('data.removeGameFromStore', @removeGame)
        @handleAction('data.updateGame', @updateGame)
        @handleAction('data.updateGames', @updateGames)
        @handleAction('data.updateContest', @updateContest)

    removeGame: (id) ->
        @setState({games: @state.games.remove(id)})

    handleProcessContests: (data) ->
        if not data.realGames
            return

        res = immutable.OrderedMap()
        for contest in data.realGames
            res = res.set(contest.id, new Contest(contest))

        @setState({contests: res.sortBy((c) -> +c.startTimeUtc)})

    handleNewGame: (game) ->
        game.gameType = 'salarycap'
        @setState({games: @state.games.set(game.id, new GameRecord(game))})

    handleNewGames: (data) ->
        games = @state.games
        for game in data
            if games.has(game.id)
                games = games.update(game.id, (g) ->
                    for key, value of game
                        if typeof g[key] != 'undefined'
                            g = g.set(key, value)
                    g
                )
            else
                games = games.set(game.id, new GameRecord(game))
        @setState({games: games})

    updateContest: (contest) ->
        if @state.contests.has(contest.id)
            @setState({contests: @state.contests.update(contest.id, (value) -> value.merge(contest))})

    updateGame: (game) ->
        if @state.games.has(game.id)
            @setState({games: @state.games.update(game.id, (value) -> value.merge(game))})

    updateGames: (data) ->
        games = @state.games
        for game in data
            if game.deleted
                games = games.remove(game.id)
            else if games.has(game.id)
                games = games.update(game.id, (value) -> value.merge(game))
        @setState({games: games})

    getGames: (options = {}) ->
        {games} = @state
        unless _.isEmpty(options)
            games = games.filter((game) ->
                for key, value of options
                    if game.get(key) != value
                        return false

                return true
            )

        games.sort((game1, game2) -> game2.points - game1.points)

    getMyGames: ->
        currentUserId = @stores.users.getCurrentUser().get('id')
        @state.games.filter((game) -> game.userId == currentUserId)

    getActiveGamesCount: () ->
        @getMyGames().count((game) -> not game.isFinished())

    getGame: (id) ->
        @state.games.get(id)

    getContest: (id) ->
        @state.contests.get(id, new immutable.Map())

    getContests: ->
        @state.contests

    getContestsByDay: ->
        filter = (contest) ->
            contest.showInSchedule

#        if SPORT == 'nba'
#            sportEventId = flux.stores.wizard.getSelectedEvent().id
#            filter = (contest) ->
#                contest.showInSchedule and (contest.sportEventId == sportEventId or sportEventId is null)
        @state.contests.toIndexedSeq().filter(filter).groupBy((contest) -> contest.getDay())

    getCurrentContests: ->
        contests = @state.contests.toIndexedSeq().filter((contest) -> contest.showInSchedule)
        if SPORT == 'nba'
            sportEventId = @stores.wizard.getSelectedEvent().id
            contests = contests.filter((c) -> c.sportEventId == sportEventId)
        else if SPORT == 'nfl'
            if @stores.wizard.isInitialRoom()
                roomId = @stores.wizard.getSelectedRoomIds().first()
                startDate = @stores.rooms.getRoom(roomId)?.startDate
            else
                startDate = @stores.rooms.getNextGame()?.startDate
            contests = contests.filter((c) ->
                c.startTimeUtc >= startDate)

        contests.toList()

    getAvailableEvents: ->
        @state.contests.toIndexedSeq().filter((contest) -> contest.showInSchedule).map((contest) -> contest.getSportEvent()).toOrderedSet().map((event) -> event.toJS())

    getCurrentEventId: ->
        @getCurrentContests().first()?.sportEventId


module.exports = GamesStore
