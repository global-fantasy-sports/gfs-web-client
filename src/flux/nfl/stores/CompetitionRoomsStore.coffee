_ = require('underscore')
Immutable = require('immutable')
CompetitionRoomRecord = require('flux/nfl/records/CompetitionRoomRecord')
CommonCompetitionRoomStore = require('../../common/stores/CompetitionRoomStore')

class CompetitionRoomStore extends CommonCompetitionRoomStore
    constructor: ->
        @state = {
            rooms: Immutable.Map()
            allStarted: false
        }

        @handleAction('data.initialData', @handleInitialData)
        @handleAction('data.setCompetitionRoom', @setCompetitionRoom)
        @handleAction('data.updateCompetitionRoom', @updateCompetitionRoom)
        @handleAction('data.updateCompetitionRooms', @updateCompetitionRooms)

        @handleAction('search.setEntryFeeInterval', () => @emit('change'))
        @handleAction('search.setGameTypeFilter', () => @emit('change'))
        @handleAction('search.setPaymentTypeFilter', () => @emit('change'))
        @handleAction('search.setSortingField', () => @emit('change'))
        @handleAction('search.toggleBeginners', () => @emit('change'))
        @handleAction('search.toggleFeatured', () => @emit('change'))
        @handleAction('search.toggleGuaranteed', () => @emit('change'))
        @handleAction('search.toggleRoomTypeFilter', () => @emit('change'))

    handleInitialData: (data) ->
        if not data.competitionRooms
            return

        rooms = Immutable.Seq(data.competitionRooms).toKeyedSeq()
            .mapEntries(([k, v]) -> [v.id, new CompetitionRoomRecord(v)])
            .toMap()

        @_setRooms(@state.rooms.merge(rooms))

    setCompetitionRoom: (data) ->
        room = new CompetitionRoomRecord(data)
        @_setRooms(@state.rooms.set(room.id, room))

    updateCompetitionRoom: (data) ->
        rooms = @state.rooms.update(data.id, (room) ->
            if room
                for key, value of data
                    if typeof room[key] != 'undefined'
                        room = room.set(key, value)
                room
        )

        @_setRooms(rooms)

    updateCompetitionRooms: (data) ->
        rooms = @state.rooms
        for room in data
            rooms = rooms.update(room.id, (r) ->
                for key, value of room
                    if typeof r[key] != 'undefined'
                        r = r.set(key, value)
                r
            )
        @_setRooms(rooms)

    _setRooms: (rooms) ->
        allStarted = rooms.every((r) -> r.started)
        @setState({rooms, allStarted})

    getRooms: (options = {}, filters = [], sorting = false) ->
        rooms = @state.rooms.toIndexedSeq()

        if not _(options).isEmpty()
            rooms = rooms.filter((room) ->
                for key, value of options
                    if value instanceof Date
                        if +(room.get(key)) != +value
                            return false
                    else if room.get(key) != value
                        return false
                return true
            )

        if filters.length
            for filter in filters when filter
                rooms = rooms.filter(filter)

        if sorting
            rooms = rooms.sort(sorting)

        rooms.toList()

    getTotalAvailableRooms: () ->
        @state.rooms.count((room) ->
            room.gamesCount < room.maxEntries and not room.started
        )

    getTournamentFilteredRooms: () ->
        @getFilteredRooms()

    getRoom: (id) ->
        @state.rooms.get(+id, new CompetitionRoomRecord({}))

    getRoomTypes: ->
        @state.rooms.toIndexedSeq().map((r) -> r.gameType).toSet()

    isAllStarted: ->
        @state.allStarted


module.exports = CompetitionRoomStore
