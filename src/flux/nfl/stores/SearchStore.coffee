_ = require('underscore')
Immutable = require('immutable')
{Store} = require('minimal-flux')
CommonSearchStore = require('../../common/stores/SearchStore')


class SearchStore extends CommonSearchStore
    constructor: ->
        @state =
            entryFeeInterval: [0, 40]
            onlyBeginners: false
            onlyFeatured: false
            onlyGuaranteed: false
            roomTypeFilters: Immutable.Set()
            gameTypeFilters: Immutable.Set()
            sortingDirection: 'ASC'
            sortingField: 'starts'
            paymentType: 'usd'

        @handleAction("search.setEntryFeeInterval", @setEntryFeeInterval)
        @handleAction("search.toggleBeginners", @toggleBeginners)
        @handleAction("search.toggleFeatured", @toggleFeatured)
        @handleAction("search.toggleGuaranteed", @toggleGuaranteed)
        @handleAction("search.toggleGameTypeFilter", @toggleGameTypeFilter)
        @handleAction("search.toggleRoomTypeFilter", @toggleRoomTypeFilter)
        @handleAction("search.toggleSortingDirection", @toggleSortingDirection)
        @handleAction("search.setSortingField", @setSortingField)
        @handleAction("search.setPaymentTypeFilter", @setPaymentTypeFilter)

    toggleSortingDirection: ->
        @setState({
            sortingDirection: if @state.sortingDirection == 'ASC' then 'DESC' else 'ASC'
        })

    toggleBeginners: ->
        @setState({onlyBeginners: not @state.onlyBeginners})

    toggleFeatured: ->
        @setState({onlyFeatured: not @state.onlyFeatured})

    toggleGuaranteed: ->
        @setState({onlyGuaranteed: not @state.onlyGuaranteed})

    setGameTypeFilter: (gameType) ->
        @setState({gameTypeFilter: gameType})

    toggleRoomTypeFilter: (roomType) ->
        unless roomType
            return @setState({roomTypeFilters: Immutable.Set()})

        {roomTypeFilters} = @state
        if roomTypeFilters.has(roomType)
            @setState({roomTypeFilters: roomTypeFilters.remove(roomType)})
        else
            @setState({roomTypeFilters: roomTypeFilters.add(roomType)})

    toggleGameTypeFilter: (gameType) ->
        unless gameType
            return @setState({gameTypeFilters: Immutable.Set()})

        {gameTypeFilters} = @state
        if gameTypeFilters.has(gameType)
            @setState({gameTypeFilters: gameTypeFilters.remove(gameType)})
        else
            @setState({gameTypeFilters: gameTypeFilters.add(gameType)})

    setSortingField: (fieldName) ->
        if fieldName == @state.sortingField
            @toggleSortingDirection()
        else
            @setState({sortingField: fieldName})

    setPaymentTypeFilter: (paymentType) ->
        @setState({paymentType})

    getSortingField: ->
        @state.sortingField

    getRoomTypeFilters: ->
        @state.roomTypeFilters

    getGameTypeFilters: ->
        @state.gameTypeFilters

    isOnlyBeginners: ->
        @state.onlyBeginners

    isOnlyFeatured: ->
        @state.onlyFeatured

    isOnlyGuaranteed: ->
        @state.onlyGuaranteed

    getSortingDirection: ->
        @state.sortingDirection

    setEntryFeeInterval: (valueStart, valueEnd) ->
        @setState({entryFeeInterval: [valueStart, valueEnd]})

    getEntryFeeInterval: ->
        @state.entryFeeInterval

    getRoomType: ->
        @state.roomType

    getPaymentType: ->
        @state.paymentType


module.exports = SearchStore
