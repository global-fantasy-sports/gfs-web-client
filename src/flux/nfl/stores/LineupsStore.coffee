_ = require('underscore')
{history} = require('world')
{Store} = require('minimal-flux')
{conn} = require('lib/connection')
{channels} = require('lib/const')
LineupRecord = require('../records/LineupRecord')


class LineupStore extends Store
    constructor: () ->
        @state = {
            lineups: immutable.Map()
            isCurrentRosterAdded: false
            currentLineupId: null
            editedLineupId: null
            hoveredLineupId: null
        }
        @stores.participants.addListener('change', () =>
            @recalculateParticipants()
        )

        @handleAction('wizard.init', @init)
        @handleAction('lineup.newLineup', @handleNewLineup)
        @handleAction('lineup.toggleCurrentRoster', @toggleCurrentRoster)
        @handleAction('lineup.remove', @remove)
        @handleAction('lineup.editName', @editName)
        @handleAction("lineup.startEditLineupName", @handleLineupMode)
        @handleAction("lineup.finishEditLineupName", @handleLineupMode)
        @handleAction('lineup.onSaveLineup', @save)
        @handleAction('lineup.hoverLineupName', @handleHoverLineupName)
        @handleAction('lineup.blurLineupName', @handleBlurLineupName)
        @handleAction('lineup.setCurrentLineup', @handleCurrentLineup)


    init: ->
        @setState({isCurrentRosterAdded: false})

    handleCurrentLineup: (id) ->
        @setState({currentLineupId: id})

    handleHoverLineupName: (id) ->
        @setState({hoveredId: id})

    handleBlurLineupName: ->
        @setState({hoveredId: null})

    handleNewLineup: (lineup) ->
        newLineup = new LineupRecord()
        store = @stores.participants

        newLineup = newLineup.updateIn(['positions'], (coll) ->
            coll.map((p) ->
                id = lineup[p.key]
                participant = store.getPlayerParticipant(id, -1, p.position)
                p.set('participant', participant || id)
            )
        ).set('id', lineup.id)
        .set('name', lineup.name)

        @setState({lineups: @state.lineups.set(lineup.id, newLineup)})

    recalculateParticipants: ->
        store = @stores.participants

        lineups = @state.lineups.map((lineup) ->
            lineup.updateIn(['positions'], (coll) ->
                coll.map((p) ->
                    if _(p.participant).isNumber()
                        p.set('participant', store.getPlayerParticipant(p.participant))
                    else if _(p.participant).isString()
                        p.set('participant', store.getParticipant(p.participant))
                    else
                        p
                )
            )
        )

        @setState({lineups})

    toggleCurrentRoster: ->
        if @state.isCurrentRosterAdded
            @remove(@state.currentLineupId)
            @setState({isCurrentRosterAdded: false})
        else
            currentLineup = @stores.roster.getLineup()
            @save(currentLineup)
            @setState({isCurrentRosterAdded: true})

    remove: (id) ->
        return unless id

        conn.send('delete', channels.LINEUPS.remove, {id}, (err, data) =>
            if err then return flux.actions.state.openInfoMessage(err)

            if data.success
                @setState({lineups: @state.lineups.remove(id)})
                history.pushState(null, "/#{SPORT}/lineups/")
        )

    editName: (id, name) ->
        if id != 'new'
            lineup = @state.lineups.get(id)
            lineup = lineup.set('name', name)
            @setState({lineups: @state.lineups.set(id, lineup)})
            @save(lineup)

    save: (lineup) ->
        data = {
            name: lineup.name
        }

        if lineup.id
            data.id = lineup.id

        lineup.getKeys().forEach((key) ->
            data[key] = lineup.getPlayerId(key)
        )

        conn.send('set', channels.LINEUPS.set, data, (err, response) =>
            if err
                flux.actions.state.openInfoMessage(err)

            flux.actions.roster.clear()
            @setState({currentLineupId: response.id})
        )

    isCurrentRosterAdded: -> @state.isCurrentRosterAdded

    getLineups: -> @state.lineups

    getLineup: (id) -> @state.lineups.get(id) || new LineupRecord({name: 'NEW LINEUP'})

    handleLineupMode: (val) ->
        @setState({editedLineupId: val})

    getEditedLineupId: -> @state.editedLineupId

    getHoveredLineupId: -> @state.hoveredId

    getCurrentLineupId: -> @state.currentLineupId


module.exports = LineupStore
