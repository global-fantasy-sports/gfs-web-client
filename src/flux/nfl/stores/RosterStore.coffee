_ = require('underscore')
Immutable = require('immutable')
{Store} = require('minimal-flux')
errors = require('../../../lib/errors')
{POSITIONS, POSITION_VALUES} = require('../../../components/const')
LineupRecord = require('../records/LineupRecord')
RosterPosition = require('../records/RosterPosition')


ROSTER_BUDGET = 50000


class RosterStore extends Store
    constructor: () ->
        @state = @init()

        @state.lineup = new LineupRecord()

        @handleAction('wizard.init', @initRoster)
        @handleAction('roster.init', @initRoster)
        @handleAction('lineup.init', @clearClone)
        @handleAction('roster.clear', @clear)
        @handleAction('roster.search', @handleSearch)
        @handleAction('roster.filterByGame', @handleFilterByGame)
        @handleAction('roster.filterByPosition', @handleFilterByPosition)
        @handleAction('roster.filterByTeam', @handleFilterByTeam)
        @handleAction('roster.selectAllTeams', @handleAllTeams)
        @handleAction('roster.clearFilters', @handleClearFilters)
        @handleAction('roster.toggleParticipant', @handleToggleParticipant)
        @handleAction('roster.sort', @handleSorting)
        @handleAction('roster.cloneGame', @handleCloneGame)
        @handleAction('tap.editGame', @handleEditGame)
        @handleAction('roster.cloneLineup', @handleCloneLineup)
        @handleAction('data.addFavorite', @handleUpdateFavorite)
        @handleAction('data.deleteFavorite', @handleUpdateFavorite)
        @handleAction('data.initialData', @handleInitialLoading)

    setState: (state) ->
        if state.lineup
            userId = @stores.users.getCurrentUser().get('id')
            sportEventId = @stores.games.getCurrentEventId() or state.lineup.sportEventId

            positions = state.lineup.positions.map((p) ->
                if typeof p.participant in ['number', 'string']
                    id = p.participant
                else
                    id = p.participant?.playerId

                {
                    position: p.position
                    key: p.key
                    playerId: id
                }
            )

            localStorage.setItem("roster-#{userId}", JSON.stringify({
                sportEventId
                name: state.lineup.name
                positions
            }))

        super state

    init: () ->
        Object.assign({
            budget: ROSTER_BUDGET
            open: Immutable.Set(POSITIONS)
            selected: Immutable.Set()
            clone: null
        }, @initFilters())

    initFilters: () ->
        sorting: 'salary'
        reversed: true
        search: ''
        position: 'All'
        gameId: null
        teamIds: Immutable.Set()

    handleInitialLoading: ->
        @loadLineup()
        @recalculateParticipants()
        @checkLineupEvent()

    loadLineup: ->
        userId = @stores.users.getCurrentUser().get('id')
        cached = localStorage.getItem("roster-#{userId}")
        if cached
            parsed = JSON.parse(cached)

            positions = Immutable.Seq(parsed.positions).map((p) =>
                new RosterPosition({position: p.position, key: p.key, participant: p.playerId})
            ).toList()

            @state.lineup = new LineupRecord({sportEventId: parsed.sportEventId, name: parsed.name, positions})

    clear: ->
        @setState lineup: new LineupRecord()

    checkLineupEvent: ->
        if @state.lineup.sportEventId != @stores.games.getCurrentEventId()
            @clear()

    recalculateParticipants: ->
        store = @stores.participants

        lineup = @state.lineup.updateIn(['positions'], (coll) ->
            coll.map((p) ->
                if typeof p.participant in ['number', 'string']
                    p.set('participant', store.getPlayerParticipant(p.participant) || p.participant)
                else
                    p
            )
        )

        @setState({lineup})

    handleEditGame: (game) ->
        lineup = new LineupRecord()
        game.getParticipants().forEach((participant, index) ->
            lineup = lineup.setIn(['positions', index, 'participant'], participant)
        )

        @recalculateState(lineup)

    clearClone: ->
        @setState(@init())

    initRoster: () ->
        clone = @state.clone
        @setState(@init())

        if clone
            @recalculateState(clone)

    handleSearch: (query = '') ->
        @setState({search: query.trim().toLowerCase()})

    handleSorting: (sorting) ->
        reversed = if sorting is @state.sorting then not @state.reversed else false
        @setState({sorting, reversed})

    handleCloneGame: (game) ->
        lineup = new LineupRecord()
        game.getParticipants().forEach((participant, index) ->
            lineup = lineup.setIn(['positions', index, 'participant'], participant)
        )

        @setState({clone: lineup})

    handleCloneLineup: (lineup, opt) ->
        opt = Object.assign({edit: false, isAll: false}, opt)

        if !opt.edit
            lineup = lineup.remove('id').remove('name')

        if !opt.isAll
            participants = @getCandidates(false, true)
            lineup = @filterLineup(lineup, participants)

        @setState({clone: lineup})
        if opt.edit then @initRoster()

    handleUpdateFavorite: (playerId) ->
        {lineup} = @state

        updatedIndex = lineup.positions.findIndex((pos) ->
            pos.getIn(['participant', 'playerId']) is playerId
        )

        if updatedIndex > -1
            lineup = lineup.updateIn(['positions', updatedIndex, 'participant'], (p) =>
                @stores.participants.getParticipant(p.id)
            )

        @recalculateState(lineup)

    handleFilterByGame: (gameId) ->
        @setState({gameId})

    handleFilterByPosition: (position) ->
        @setState({position})

    handleFilterByTeam: (teamId) ->
        if @state.teamIds.has(teamId)
            @setState({teamIds: @state.teamIds.delete(teamId)})
        else
            @setState({teamIds: @state.teamIds.add(teamId)})

    handleAllTeams: () ->
        @setState({teamIds: @state.teamIds.clear()})

    handleClearFilters: () ->
        @setState(@initFilters())

    handleToggleParticipant: (participant) ->
        if @state.lineup.hasParticipant(participant)
            lineup = @state.lineup.removeParticipant(participant)
        else
            lineup = @state.lineup.addParticipant(participant)

        @recalculateState(lineup)

    recalculateState: (lineup) ->
        {open, filled} = lineup.positions.toSeq().groupBy((pos) -> if pos.isOpen() then 'open' else 'filled').toObject()

        budget = lineup.positions.reduce(((sum, pos) -> sum - pos.getSalary()), ROSTER_BUDGET)
        selected =  filled?.map(({participant}) -> participant.playerId).toSet() ? Immutable.Set()
        open = open?.map(({position}) -> POSITION_VALUES[position]).flatten().toSet() ? Immutable.Set()

        @setState({
            lineup
            open
            selected
            budget
        })

    getBudget: ->
        @state.budget

    filterLineup: (lineup, participants) ->
        lineup.update('positions', (positions) ->
            positions.map((p) ->
                if p.participant && participants.contains(p.participant)
                    return p
                else
                    return p.set('participant', null)
            )
        )

    getRoster: (allParticipants = false) ->
        {lineup, selected, budget} = @state
        selectedTeams = lineup.positions.map((p) -> p.participant).filter((p) -> p?).groupBy((p) -> p.team)

        disabledTeams = selectedTeams.map((team, key) ->
            if team.count() >= 4 then key else null
        ).filter((t) -> t?).toList()

        return {
            lineup, selected, budget, selectedTeams, disabledTeams,
            isDone: lineup.positions.size is selected.size and budget >= 0
            open: @state.open
            search: @state.search
            sorting: @state.sorting
            reversed: @state.reversed
            position: @state.position
            participants: @getCandidates(allParticipants)
            games: @stores.games.getCurrentContests()
            gameId: @state.gameId
            teamIds: @state.teamIds
        }

    getLineup: -> @state.lineup

    getRosterBudget: -> ROSTER_BUDGET

    getCandidates: (isAll, noFilters = false) ->
        filter = (p) -> p.salary > 0
        if SPORT == 'nba'
            selectedSportEventId = @stores.wizard.getSelectedEvent().id
            filter = (p) ->
                p.salary > 0 and p.sportEventId == selectedSportEventId
        else if SPORT == 'nfl'
            if @stores.wizard.isInitialRoom()
                roomId = @stores.wizard.getSelectedRoomIds().first()
                startDate = @stores.rooms.getRoom(roomId)?.startDate
            else
                startDate = @stores.rooms.getNextGame()?.startDate
            filter = (p) ->
                p.getGame().startTimeUtc >= startDate

        if isAll
            res = @stores.participants.getParticipants()
        else
            res = @stores.participants.getParticipants({readyToPlay: true}).filter(filter)

        {gameId, teamIds, position, search, sorting, reversed} = @state

        if !noFilters
            if position == 'FAV'
                res = res.filter((p) -> p.isFavorite)
            else if position and position isnt 'All'
                res = res.filter((p) -> p.canPlay(position))

            if gameId
                res = res.filter((p) -> p.gameId is gameId)
            if search
                res = res.filter((p) -> p.getName().toLowerCase().includes(search))
            if not teamIds.isEmpty()
                res = res.filter((p) -> teamIds.has(p.team))

        if sorting == 'position'
            ps = _(POSITIONS)
            res = res.sort((a, b) => ps.indexOf(a.position) - ps.indexOf(b.position))
        else if sorting == 'name'
            res = res.sortBy((p) => p.getName())
        else
            res = res.sortBy((p) => p[sorting])

        if reversed
            res = res.reverse()

        return res.toList()

    isValid: () ->
        @state.lineup.isValid()

    getSelectedIds: () ->
        @state.lineup.map((p) -> p.getIn(['participant', 'id']))


module.exports = RosterStore
