{Actions} = require('minimal-flux')
{history} = require('world')
{conn} = require('lib/connection')
{channels} = require('lib/const')

API = require('../../API')

class LineupActions extends Actions

    init: ->
        @dispatch('init')

    newLineup: (lineup) ->
        @dispatch('newLineup', lineup)

    toggleCurrentRoster: ->
        @dispatch('toggleCurrentRoster')

    remove: (id) ->
        @dispatch('remove', id)

    editName: (id, name) ->
        @finishEditLineupName()
        @dispatch('editName', id, name)

    startEditLineupName: (id) ->
        @dispatch("startEditLineupName", id)

    finishEditLineupName: ->
        @dispatch("finishEditLineupName")

    goToLineupEdit: (id) ->
        unless id
            id = 'new'
        history.pushState(null, "/#{SPORT}/lineups/#{id}/")

    goToImportLineups: ->
        history.pushState(null, "/#{SPORT}/lineups/import/")

    goToDownloadTemplate: ->
        window.location.href = "/#{SPORT}/lineups/template/"

    uploadLineupsFile: (file) ->
        API.uploadFile(
            url: "/lineups/upload/"
            file: file
        )
        .then(
            (=>
                @actions.data.fetchLineups()
                history.pushState(null, "/#{SPORT}/lineups/")
            )
            (({err, data}) =>
                logger.log(data or err, "err", "Upload")
                if err == 413
                    @actions.state.openInfoMessage("File is too large")
                else if err != 404
                    @actions.state.openInfoMessage(data and data.error or err)
            )
        )

    onSaveLineup: ->
        lineup = @stores.roster.getLineup()
        @dispatch('onSaveLineup', lineup)
        history.pushState(null, "/#{SPORT}/lineups/")

    saveLineup: (lineup) ->
        data = {
            name: lineup.name
        }

        if lineup.id
            data.id = lineup.id

        lineup.getKeys().forEach((key) ->
            data[key] = lineup.getPlayerId(key)
        )

        conn.send('set', channels.LINEUPS.set, data, (err, response) =>
            if err
                flux.actions.state.openInfoMessage(err)

            lineup = lineup.set('id', response.id)
            @setCurrentLineup(response.id)
            @actions.roster.cloneLineup(lineup, {edit: true})
            @actions.state.openLineupSavedDialog(lineup)
        )

    hoverLineupName: (id) ->
        @dispatch('hoverLineupName', id)

    blurLineupName: ->
        @dispatch('blurLineupName')

    setCurrentLineup: (id) ->
        @dispatch('setCurrentLineup', id)

module.exports = LineupActions
