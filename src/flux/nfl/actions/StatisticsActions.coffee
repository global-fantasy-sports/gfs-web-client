_ = require('underscore')
{Actions} = require('minimal-flux')
utils = require('../../../lib/utils')

xhr = require('../../../lib/xhr')
{conn} = require('lib/connection')


fetchPlayersData = () ->
    new Promise((resolve, reject) ->
        conn.send('get', 'season-stats-metadata', null, (err, response) =>
            return reject('Players data not available') unless response and response.dataURL

            xhr.get(response.dataURL, null, {responseType: 'tsv'}).then(
                ((data) => resolve(Object.assign(response, {data}))),
                ((error) => reject(error))
            )
        )
    )


class StatisticsActions extends Actions

    fetchPlayers: () ->
        @dispatch('fetchPlayers')

        fetchPlayersData()
            .then(@processPlayersResponse, @error)

    processPlayersResponse: (response) ->
        @dispatch('processPlayersResponse', response)

    error: (error) ->
        @dispatch('error', error)

    sort: (field) ->
        if typeof field is 'string'
            field = @stores.statistics.getColumn(field)

        @dispatch('sort', field)

    addFilter: (filter) ->
        @dispatch('addFilter', filter)

    removeFilter: (filter) ->
        @dispatch('removeFilter', filter)

    clearFilters: () ->
        @dispatch('clearFilters')

    filter: (fieldName, value) ->
        @dispatch('filter', fieldName, value)

    filterPosition: (position = 'All') ->
        @dispatch('filterPosition', position)

    search: (query = '') ->
        @dispatch('search', query)

    toggleCategory: (name) ->
        @dispatch('toggleCategory', name)

    toggleViewMode: () ->
        {viewMode} = @stores.statistics.getState()
        @dispatch('toggleViewMode', if viewMode is 'list' then 'cards' else 'list')

    toggleFavorites: () ->
        @dispatch('toggleFavorites')

module.exports = StatisticsActions
