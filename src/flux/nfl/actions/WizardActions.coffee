{history} = require('../../../world')
{conn} = require('../../../lib/connection')
flash = require('../../../lib/flash')
errors = require('../../../lib/errors')
CommonWizardActions = require('../../common/actions/WizardActions')

class WizardActions extends CommonWizardActions

    init: (props = {}) ->
        @dispatch('init', props)

    finish: ->
        @init()
        history.replaceState(null, "/#{SPORT}/games/")

    saveGame: () ->
        lineup = @stores.roster.getLineup()
        return if not lineup.isValid()

        saveIt = (lineup, params) =>
            params = lineup.positions.reduce(((obj, {key, participant}) ->
                if key == 'dst'
                    obj[key] = participant.team
                else
                    obj[key] = participant.playerId
                return obj
            ), params)

            @setGameState('saving', params)

            channel = ''
            if SPORT == 'nfl'
                channel = 'nfl-game'
            if SPORT == 'nba'
                channel = 'nba-game'

            conn.send('set', channel, params, (error, data) =>
                if error
                    @setGameState('saving error')
                    logger.log(error, ['Game saving error', SPORT], "WizardActions")
                    error = error[0] if Array.isArray(error)
                    flash({type: 'error', text: errors.get(error)})
                else
                    @setGameState('saved')
            )

        if @stores.state.getMode() == "EDIT"
            params = {id: @stores.wizard.getGameId()}
            if not params.id
                console.error('Game id is not set while wizard is in edit mode')

            saveIt(lineup, params)
        else
            roomIds = @stores.wizard.getSelectedRoomIds().toArray()
            params = {competitionRoomIds: roomIds}
            saveIt(lineup, params)

        flux.actions.roster.clear()

    setWizardGameType: () ->
        @dispatch('setWizardGameType', 'salarycap')

    selectEvent: (date, id) ->
        @dispatch('selectEvent', date, id)

module.exports = WizardActions
