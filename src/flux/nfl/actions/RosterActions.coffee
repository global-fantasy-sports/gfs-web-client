_ = require('underscore')
{Actions} = require('minimal-flux')
Immutable = require('immutable')


class RosterActions extends Actions

    init: ->
        @dispatch('init')

    update: ->
        @dispatch('update')

    toggleParticipant: (id) ->
        participant = @stores.participants.getParticipant(id)
        @dispatch('toggleParticipant', participant)
        @update()

    removeParticipant: (id) ->
        @dispatch('removeParticipant', id)

    filterByGame: (id) ->
        @dispatch('filterByGame', id)

    filterByTier: (tier) ->
        @dispatch('filterByTier', tier)

    filterByPosition: (position) ->
        @dispatch('filterByPosition', position)

    filterByTeam: (teamId) ->
        @dispatch('filterByTeam', teamId)

    selectAllTeams: () ->
        @dispatch('selectAllTeams')

    search: (query = '') ->
        @dispatch('search', query)

    sort: (sorting) ->
        @dispatch('sort', sorting)

    clearFilters: () ->
        @dispatch('clearFilters')

    cloneGame: (game) ->
        @dispatch('cloneGame', game)

    cloneLineup: (lineup, opt) ->
        @dispatch('cloneLineup', lineup, opt)

    clear: ->
        @dispatch('clear')

    autoFill: (isAll) ->
        getMostExpensiveParticipantId = () =>
            {lineup} = @stores.roster.getRoster(isAll)

            lineupData = lineup.positions.map((el) ->
                if el.participant
                    return Immutable.Map({id: el.participant.id, salary: el.participant.salary})
            )

            max = lineupData.map((el) -> el.get('salary')).max()
            index = lineupData.findIndex((el) -> el.get('salary') == max)
            lineupData.getIn([index, 'id'])

        replaceParticipant = (id) =>
            @toggleParticipant(id)
            fillLineup()

        fillLineup = () =>
            {lineup} = @stores.roster.getRoster(isAll)

            lineup.positions.filter((pos) -> pos.isOpen()).forEach((pos) =>
                {participants, selected} = @stores.roster.getRoster(isAll)
                participants = participants.sort(() -> _.random(-1, 1))

                candidate = participants.find((p) ->
                    pos.accepts(p) and not selected.has(p.playerId)
                )
                @toggleParticipant(candidate.id)
            )

        fillLineup()
        {lineup} = @stores.roster.getRoster(isAll)
        if not lineup.isValid()
            logger.log('reshuffle', 'DEBUG')

        while not lineup.isValid()
            id = getMostExpensiveParticipantId()
            replaceParticipant(id)
            {lineup} = @stores.roster.getRoster(isAll)



module.exports = RosterActions
