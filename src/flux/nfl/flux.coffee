{Flux} = require('minimal-flux')

CompareActions = require('../common/actions/CompareActions')
ConnectionActions = require('../common/actions/ConnectionActions')
DataActions = require('../common/actions/DataActions')
FacebookActions = require('../common/actions/FacebookActions')
FormActions = require('../common/actions/FormActions')
LineupActions = require('./actions/LineupActions')
NotificationsActions = require('../common/actions/NotificationsActions')
PaymentActions = require('../common/actions/PaymentActions')
RosterActions = require('./actions/RosterActions')
SearchActions = require('../common/actions/SearchActions')
StateActions = require('../common/actions/StateActions')
StatisticsActions = require('./actions/StatisticsActions')
TapActions = require('../common/actions/TapActions')
TimeActions = require('../common/actions/TimeActions')
UserAccountActions = require('../common/actions/UserAccountActions')
WizardActions = require('./actions/WizardActions')

ChallengeStore = require('../common/stores/ChallengeStore')
CommonSearchStore = require('../common/stores/SearchStore')
CompareStore = require('../common/stores/CompareStore')
CompetitionRoomStore = require('./stores/CompetitionRoomsStore')
ConnectionStore = require('../common/stores/ConnectionStore')
DialogsStore = require('../common/stores/DialogsStore')
FacebookStore = require("../common/stores/FacebookStore")
FormsStore = require('../common/stores/FormsStore')
GamesStore = require('./stores/GamesStore')
GuideStore = require('../common/stores/GuideStore')
LineupsStore = require('./stores/LineupsStore')
NotificationsStore = require('../common/stores/NotificationsStore')
ParticipantsStore = require('./stores/ParticipantsStore')
PaymentStore = require('../common/stores/PaymentStore')
RosterStore = require('./stores/RosterStore')
StateStore = require('../common/stores/StateStore')
StatisticsStore = require('./stores/StatisticsStore')
SubscriptionStore = require('../golf/stores/SubscriptionStore')
TimeStore = require('../common/stores/TimeStore')
UsersStore = require('../common/stores/UsersStore')
WizardStore = require('./stores/WizardStore')


module.exports = new Flux(
    actions:
        compare: CompareActions
        connection: ConnectionActions
        data: DataActions
        facebook: FacebookActions
        forms: FormActions
        lineup: LineupActions
        notifications: NotificationsActions
        payment: PaymentActions
        roster: RosterActions
        search: SearchActions
        state: StateActions
        statistics: StatisticsActions
        tap: TapActions
        time: TimeActions
        userAccount: UserAccountActions
        wizard: WizardActions

    stores:
        challenges: [ChallengeStore, 'rooms']
        compare: CompareStore
        connection: ConnectionStore
        dialogs: DialogsStore
        facebook: FacebookStore
        forms: FormsStore
        games: GamesStore
        guide: GuideStore
        lineups: [LineupsStore, 'participants', 'roster']
        notifications: NotificationsStore
        participants: ParticipantsStore
        payments: [PaymentStore, 'users']
        rooms: CompetitionRoomStore
        roster: [RosterStore, 'participants', 'games', 'users']
        search: CommonSearchStore
        state: StateStore
        statistics: StatisticsStore
        subscription: SubscriptionStore
        time: [TimeStore, 'connection']
        users: [UsersStore, 'connection']
        wizard: [WizardStore, 'roster']
)
