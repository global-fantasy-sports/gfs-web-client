Immutable = require('immutable')
createRecord = require('../../createRecord')


StatRecord = createRecord({
    gameId: null
    points: 0
    salary: 0
    statistics: Immutable.Map()
}, {
    getGame: () ->
        flux.stores.games.getContest(@gameId)

    getStat: (type, defaultValue, context = {}) ->
        value = switch type
            when 'date' then @getGame()?.formatTime('MMM DD, YYYY')
            when 'opponent' then @getGame()?.getOpponent(context.teamId)
            when 'result' then @getGame()?.getScore()
            when 'points' then @points
            else @statistics.getIn(type.split(':'))

        return value ? defaultValue
})

module.exports = StatRecord
