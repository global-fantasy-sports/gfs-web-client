Immutable = require('immutable')
moment = require('moment')

createRecord = require('../../createRecord')
{fromUtcTs} = require('../../../lib/date')


Contest = createRecord({
    id: null
    awayTeam: null
    homeTeam: null
    awayScore: null
    homeScore: null
    status: null
    showInSchedule: null
    sportEventId: null
    startTimeUtc: [fromUtcTs, null]
    awayTeamPhoto: null
    homeTeamPhoto: null
}, {
    formatTime: (formatStr = 'ddd h:mm A') ->
        moment(@startTimeUtc).format(formatStr)

    getDay: (formatDate = 'MMM D, YYYY') ->
        moment(@startTimeUtc).format(formatDate)

    getSportEvent: ->
        Immutable.Map({date: @getDay('ddd MMM Do YYYY'), id: @sportEventId})

    getScore: () ->
        "#{@homeScore} : #{@awayScore}"

    getOpponent: (teamId) ->
        if @homeTeam is teamId then @awayTeam else @homeTeam
})

module.exports = Contest
