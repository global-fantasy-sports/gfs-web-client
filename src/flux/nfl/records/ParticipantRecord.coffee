_ = require('underscore')
Immutable = require('immutable')
moment = require('moment')
createRecord = require('../../createRecord')
{POSITION_VALUES, ROLE_STATS} = require('../../../components/const')
StatRecord = require('./StatRecord')


parseStats = (stats) ->
    return Immutable.List() unless stats
    Immutable.List(stats.map((item) ->
        statistics = Immutable.fromJS(item.statistics || {})
        new StatRecord(Object.assign({}, item, {statistics}))
    ))


ParticipantRecord = createRecord({
    id: null
    gameId: null
    playerId: ['sportsmanId', null]
    position: null
    team: null
    opponent: null
    points: 0
    pointsDetails: ['calcInfo', []]
    injured: null
    statInfo: [Immutable.fromJS, Immutable.Map()]
    number: -1
    height: -1
    weight: -1
    age: -1
    college: '%ImyaKoledzha%'

    nameAbbr: null
    nameFull: null
    salary: 0
    FPPG: 0
    rank: 0
    smallPhoto: null
    largePhoto: null
    photo: null
    statInfoHistory: [parseStats, Immutable.List()]
    isFavorite: false
    sportEventId: null
    readyToPlay: ['showInList', false]
    oprk: null
}, {
    getGame: () ->
        flux.stores.games.getContest(@gameId)

    getTeam: () ->
        flux.stores.participants.getTeam(@team)

    formatPoints: () ->
        if @points > 0
            @points.toFixed(2)
        else
            'N/A'

    canPlay: (position) ->
        POSITION_VALUES[position].has(@position)

    isInjured: ->
        true if @injured in ['QST', 'PRO', 'DOU']

    getInjuredColor: ->
        switch @injured
            when 'QST'
                'yellow'
            when 'PRO'
                'orange'
            when 'DOU'
                'red'

    getName: () ->
        if @position is 'DST'
            @getTeam().getFullName()
        else
            @nameFull

    getPhoto: (size = 'large') ->
        if size is 'small'
            @smallPhoto
        else
            @largePhoto

    formatRank: () ->
        if @rank then @rank else 'N/A'

    formatSalary: () ->
        if @salary then "$#{@salary}" else "—"

    formatFPPG: () ->
        if SPORT == 'nba'
            fppg = @statInfoHistory.last().get('points')
        else
            fppg = @FPPG

        if typeof @FPPG is 'number'
            return {value: @FPPG.toFixed(2), label: 'FPPG'}
        else
            return {value: '', label: 'FPPG N/A'}

    formatFPPGstr: () ->
        obj = @formatFPPG()
        if obj.value
            obj.value
        else
            obj.label

    getStatsHistory: (season = null) ->
        history = @statInfoHistory.toSeq().filter((record) ->
            record.salary and not record.statistics.isEmpty()
        )

        if season
            history = history.filter((item) ->
                game = item.getGame(@gameId)
                return moment(game.startTimeUtc).get('year') is season
            )

        history

    getFPPGHistory: (season = null) ->
        @getStatsHistory(season).map((item) -> item.get('points', 0)).toArray()

    getAverage: (type, season = null) ->
        if type is 'games'
            return @getStatsHistory(season).count()

        values = @getStatsHistory(season).map((item) -> item.getStat(type) or 0).toList()
        if values.size
            values.reduce((a, b) -> a + b) / values.size
        else
            0

    getStatSummary: (season = null) ->
        Immutable.OrderedMap(['games', ROLE_STATS[@position]...].map((stat) =>
            [stat, @getAverage(stat, season)]))
})

module.exports = ParticipantRecord
