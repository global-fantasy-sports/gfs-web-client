_ = require('underscore')
Immutable = require('immutable')
createRecord = require('../../../createRecord')


GameRecord = createRecord({
    id: null
    competitionRoomId: null
    entryFee: null
    gameType: null
    mode: null
    paid: null
    place: null
    placeRange: null
    points: null
    mechanical: null
    userId: null
    userData: {}
    pg: null
    sg: null
    pf: null
    sf: null
    c: null
    g: null
    f: null
    util: null
}, {
    getCompetitionRoom: ->
        flux.stores.rooms.getRoom(@competitionRoomId)

    getSportEventId: ->
        room = @getCompetitionRoom()
        room and room.sportEventId

    remove: ->
        flux.actions.data.removeGame(@id)

    isPending: ->
        @getCompetitionRoom().isPending()

    isLive: ->
        @getCompetitionRoom().isLive()

    isFinished: ->
        @getCompetitionRoom().isFinished()

    isRecent: ->
        @getCompetitionRoom().isRecent()

    getPrize: ->
        room = @getCompetitionRoom()
        return false unless room.isFinished()
        room.getIn(['prizeList', @place]) or false

    getPlayerIds: ->
        Immutable.List([@pg, @sg, @pf, @sf, @c, @g, @f, @util])

    getPlayersBrief: ->
        sportEventId = @getSportEventId()
        @getPlayerIds().map((playerId) ->
            participant = flux.stores.participants.getPlayerParticipant(playerId, sportEventId)
            _(participant).pick('smallPhoto', 'position')
        )
    getParticipants: ->
        sportEventId = @getSportEventId()
        participants = @getPlayerIds().map((id) ->
            flux.stores.participants.getPlayerParticipant(id, sportEventId)
        )

        participants.filter(Boolean)

})

module.exports = GameRecord
