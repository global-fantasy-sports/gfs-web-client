_ = require('underscore')
Immutable = require('immutable')
createRecord = require('../../../createRecord')

GameRecord = createRecord({
    id: null
    competitionRoomId: null
    entryFee: null
    gameType: null
    mode: null
    paid: null
    place: null
    placeRange: null
    points: null
    mechanical: null
    userId: null
    userData: {}
    deleted: null
    prizeAmount: null
    prizeCurrency: null
    qb: null
    rb1: null
    rb2: null
    wr1: null
    wr2: null
    wr3: null
    te: null
    flex: null
    dst: null
}, {
    getCompetitionRoom: ->
        flux.stores.rooms.getRoom(@competitionRoomId)

    isMyGame: ->
        @userId == flux.stores.users.getCurrentUser().get('id')

    getSportEventId: ->
        room = @getCompetitionRoom()
        room and room.sportEventId

    remove: ->
        flux.actions.data.removeGame(@id)

    isPending: ->
        @getCompetitionRoom().isPending()

    isLive: ->
        @getCompetitionRoom().isLive()

    isFinished: ->
        @getCompetitionRoom().isFinished()

    isRecent: ->
        @getCompetitionRoom().isRecent()

    getPrize: ->
        room = @getCompetitionRoom()
        return false unless room.isFinished()
        room.getIn(['prizeList', @place]) or false


    getPlayerIds: ->
        Immutable.List([@qb, @rb1, @rb2, @wr1, @wr2, @wr3, @te, @flex])

    getPlayersBrief: ->
        sportEventId = @getSportEventId()
        @getPlayerIds().map((playerId) ->
            participant = flux.stores.participants.getPlayerParticipant(playerId, sportEventId)
            _(participant).pick('smallPhoto', 'position')
        )
    getParticipants: ->
        sportEventId = @getSportEventId()
        participants = @getPlayerIds().map((id) ->
            flux.stores.participants.getPlayerParticipant(id, sportEventId)
        )
        participants = participants.push(flux.stores.participants.getPlayerParticipant(@dst, sportEventId))

    placeStr: ->
        if @place == 1
            '1st'
        else if @place == 2
            '2nd'
        else if @place == 3
            '3rd'
        else
            '' + @place + 'th'
})

module.exports = GameRecord
