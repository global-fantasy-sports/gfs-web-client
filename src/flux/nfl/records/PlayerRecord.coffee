_ = require 'underscore'
Immutable = require('immutable')
createRecord = require "flux/createRecord"
{POSITION_VALUES, ROLE_STATS} = require '../../../components/const'
StatRecord = require "./StatRecord"


parseStats = (stats) ->
    return Immutable.List() unless stats
    Immutable.List(stats.map((item) ->
        statistics = Immutable.fromJS(item.statistics || {})
        new StatRecord(Object.assign({}, item, {statistics}))
    ))


PlayerRecord = createRecord({
    id: null
    participantId: null
    nameFull: null
    nameAbbr: null
    age: null
    salary: 0
    FPPG: 0
    rank: 0
    smallPhoto: null
    largePhoto: null
    photo: null
    college: null
    height: null
    weight: null
    statInfoHistory: [parseStats, Immutable.List()]
    isFavorite: false
    currentPosition: null
    currentTeamId: null
    currentOpponentTeamId: null
    currentNumber: -1
}, {
})

module.exports = PlayerRecord
