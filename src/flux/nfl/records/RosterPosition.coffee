_ = require('underscore')
createRecord = require('flux/createRecord')
{POSITION_VALUES} = require('../../../components/const')


RosterPosition = createRecord({
    key: null
    position: null
    participant: null
}, {
    accepts: (participant) ->
        POSITION_VALUES[@position].has(participant.position)

    isOpen: () ->
        !_(@participant).isObject()

    getSalary: () ->
        @getIn(['participant', 'salary'], 0)

    getFPPG: () ->
        @getIn(['participant', 'FPPG'], 0)
})

module.exports = RosterPosition
