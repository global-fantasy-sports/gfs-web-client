createRecord = require('../../createRecord')

module.exports = createRecord({
    id: null
    name: null
    city: null
    logo: null
}, {
    getFullName: () ->
        "#{@city} #{@name}"
})
