_ = require('underscore')
Immutable = require('immutable')
{now, daysBetween, fromUtcTs} = require('../../../lib/date')
createRecord = require('../../createRecord')
{gameNameByType} = require('../../../lib/const')


fakeGameType = ->
    'salarycap'

parsePrizes = (data) ->
    Immutable.Map(data).mapKeys((k) -> Number(k))

CompetitionRoomRecord = createRecord({
    id: null
    entryFee: null
    finished: null
    gameType: [fakeGameType, null]
    gamesCount: null
    maxEntries: null
    mode: null
    pot: null
    prizeList: [parsePrizes, Immutable.Map()]
    startDate: [fromUtcTs, null]
    endDate: [fromUtcTs, null]
    started: null
    isForBeginners: ['beginner', false]
    isFeatured: ['featured', false]
    isGuaranteed: ['guaranteed', false]
    type: null
    week: null
    year: null
    sportEventId: null
    tournamentId: null
    roundNumber: null
}, {
    isPending: ->
        @started == false

    isLive: ->
        @started == true and @finished == false

    isFinished: ->
        @finished == true

    isRecent: ->
        not @isFinished() or daysBetween(now(), @endDate) < 3

    isFull: ->
        @maxEntries == @gamesCount

    getTournamentName: ->
        "#{SPORT} tournament"

    getGames: (forceFetch=false)->
        flux.stores.games.getGames({competitionRoomId: +@id})

    fetchGames: (options = {}) ->
        force = options.force or false
        if @getGames().size > 0 and force == false
            return
        flux.actions.data.fetchGamesByRoomId(@id)

    getDaysLeft: ->
        daysBetween(@startDate, now())

    diffText: ->
        diff = @getDaysLeft()
        if diff == 0
            diff = "TODAY"
        else if diff == 1
            diff = "TOMORROW"
        else if diff > 1
            diff = "in #{diff}\u00a0days from now"
        else
            diff = ""

    fullName1: ->
        gameTypeTitle = gameNameByType[@gameType]

        winners = @prizeList.size
        if winners == 1
            winnerText = '' + winners + '\u00a0winner'
        else
            winnerText = '' + winners + '\u00a0winners'

        @type + ' ' + gameTypeTitle + ' game ' + @diffText() + ' with\u00a0' + winnerText

    fullName2: ->
        'Week ' + @week + ' - Regular Season'

    closeText: ->
        if @started
            return ''
        else
            return 'This competition is closed ' + @diffText()

})


module.exports = CompetitionRoomRecord
