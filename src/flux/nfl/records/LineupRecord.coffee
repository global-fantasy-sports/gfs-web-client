Immutable = require('immutable')
createRecord = require('../../createRecord')
RosterPosition = require('./RosterPosition')

LINEUP_POSITIONS = Immutable.List()

if SPORT == 'nfl'
    LINEUP_POSITIONS = Immutable.List([
        new RosterPosition({position: 'QB', key: 'qb'})
        new RosterPosition({position: 'RB', key: 'rb1'})
        new RosterPosition({position: 'RB', key: 'rb2'})
        new RosterPosition({position: 'WR', key: 'wr1'})
        new RosterPosition({position: 'WR', key: 'wr2'})
        new RosterPosition({position: 'WR', key: 'wr3'})
        new RosterPosition({position: 'TE', key: 'te'})
        new RosterPosition({position: 'FLEX', key: 'flex'})
        new RosterPosition({position: 'DST', key: 'dst'})
    ])
if SPORT == 'nba'
    LINEUP_POSITIONS = Immutable.List([
        new RosterPosition({position: 'PG', key: 'pg'})
        new RosterPosition({position: 'SG', key: 'sg'})
        new RosterPosition({position: 'PF', key: 'pf'})
        new RosterPosition({position: 'SF', key: 'sf'})
        new RosterPosition({position: 'C', key: 'c'})
        new RosterPosition({position: 'G', key: 'g'})
        new RosterPosition({position: 'F', key: 'f'})
        new RosterPosition({position: 'UTIL', key: 'util'})
    ])

LineupRecord = createRecord({
    id: null
    sportEventId: null
    name: ''
    positions: LINEUP_POSITIONS
}, {
    isValid: ->
        @positions.every((p) -> not p.isOpen()) and @getBudget() >= 0

    getKeys: ->
        @positions.map((p) -> p.key)

    getPlayerId: (key) ->
        index = @positions.findIndex((p) -> p.key == key)
        @getIn(['positions', index, 'participant', 'playerId'])

    addParticipant: (participant) ->
        index = @positions.findIndex((pos) -> pos.isOpen() and pos.accepts(participant))
        if index >= 0
            return @setIn(['positions', index, 'participant'], participant)

        return this

    removeParticipant: (participant) ->
        index = @positions.findIndex((pos) -> pos.getIn(['participant', 'id']) is participant.id)
        return @deleteIn(['positions', index, 'participant'])

    hasParticipant: (participant) ->
        Boolean(@positions.find((pos) -> pos.getIn(['participant', 'id']) is participant.id))

    getBudget: ->
        ROSTER_BUDGET = flux.stores.roster.getRosterBudget()
        @positions.reduce(((sum, pos) -> sum - pos.getSalary()), ROSTER_BUDGET)

    getCost: ->
        ROSTER_BUDGET = flux.stores.roster.getRosterBudget()
        ROSTER_BUDGET - @getBudget()

    getFPPG: ->
        selected = @positions.filterNot((p) -> p.isOpen())
        return 0 unless selected.size

        sum = selected.reduce((memo, p) ->
            memo + p.getIn(['participant', 'FPPG'], 0)
        , 0)

        return sum / selected.size
})

module.exports = LineupRecord
