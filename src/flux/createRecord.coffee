_ = require('underscore')
Immutable = require('immutable')


getParsers = (opt) ->
    parsers = {}
    for key, value of opt
        if _(value).isArray()
            if _(value[0]).isString() and _(value[1]).isFunction()
                parsers[key] = value[1]

            if _(value[0]).isFunction()
                parsers[key] = value[0]

    parsers

getMapping = (opt) ->
    mapping = {}
    for key, value of opt
        if _(value).isArray()
            if _(value[0]).isString()
                mapping[value[0]] = key

    mapping

getDefaults = (opt) ->
    defaultValues = {}
    for key, value of opt
        if _(value).isArray() and value.length in [2..3]
            defaultValues[key] = _(value).last()
        else
            defaultValues[key] = value

    defaultValues

noopParser = (value) -> value

createRecord = (opt, methods) ->
    BaseRecord = Immutable.Record(getDefaults(opt))

    mapping = getMapping(opt)
    parsers = getParsers(opt)

    class Record extends BaseRecord
        constructor: (attrs = {}) ->
            values = {}
            for key, value of attrs # keys are in backend style here, because they coming from websockets
                if value?
                    key = mapping[key] or key # translate into frontend style
                    parser = parsers[key] or noopParser
                    values[key] = parser(value)

            notParsed = _(_(parsers).keys()).difference(_(attrs).keys())
            for key in notParsed
                parser = parsers[key]
                values[key] = parser()

            super(values)

        isEmpty: () ->
            Immutable.is(this, new Record())

    _.extend(Record.prototype, methods)

    return Record


module.exports = createRecord
