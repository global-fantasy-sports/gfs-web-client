floodLight = require '../lib/ui/floodLight'
GameEmbed = require '../lib/ui/GameEmbed'
{Titlebar} = require '../lib/ui/Titlebar'
{eventTracker} = require '../lib/utils'


module.exports.FirstTime = React.create
    displayName: "FirstTime"
    args:
        onClose: 'func'

    getInitialState: ->
        error: false
        thankYou: false

    componentDidMount: ->
        floodLight("cnv_s917")
        user = @props.user.toJS()
        if user.first_login
            if user.fb_id
                eventTracker('register', 'facebook')
            else if user.google_id
                eventTracker('register', 'google')
            else
                eventTracker('register', 'email')

    submitDiscount: ->
        code = @refs.affDiscount.getDOMNode().children[0].value
        if code.length
            @setState(error: false)
            flux.actions.connection.saveAffiliateCode(code)
            code = null
            @setState(thankYou: true)
        else
            @setState(error: true)

    render: ({onClose}, {error, thankYou}) ->
        `<GameEmbed onClose={onClose}>
            <section className="first-time">
                <section className="first-time__text">
                    <Titlebar title="WELCOME! YOU'VE REGISTERED "/>
                    <p className="stroke white medium">
                        We&apos;ve accredited your account with 100 tokens
                        to play.
                    </p>
                    <p className="stroke white medium">
                        Compete head to head, or versus 7 or 40
                        players per competition.
                    </p>
                    <p className="stroke white medium">
                        There are 3 different games available, choose
                        a round, the prize you wish to compete for - and play!
                    </p>
                    <p className="stroke white medium">
                        Challenge friends or fans and compete for money
                    </p>
                </section>
            </section>
        </GameEmbed>`
