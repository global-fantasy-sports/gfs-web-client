
exports.START_CONFIG =
    'hole-by-hole':
        name: 'hole-by-hole'
        event: 'hole-by-hole-play'
        roundGame: true
        logo: 'hole-by-hole'
        difficulty: 'Easy'
        difficultyStyle: 'green'
        difficultyColor: 'green-diff'
        gameType: 'Daily'
        gameTypeStyle: 'light-violet'
        gameTypeColor: 'light-violet-type'
        counterMode: 'round'
        desc: 'Predict the outcome of each hole for your golfers! Will it be a birdie?
                A par? A double-bogey?'
        tutorial:
            story: 'The concept of the game is to predict how your chosen
                    golfer will play 18 holes of a round, and it is not about
                    who is the best golfer. Win by knowing how your golfer
                    selection will play.'
            steps:
                1:
                    img: 'golf-people.png'
                    title: '1. Choose golfer'
                    text: 'You can use detailed statistics, to find the
                        golfer
                        you would like to predict the round for, or you can
                        choose your favourite if you want.'
                2:
                    img: 'birdie-par-bogey.png'
                    title: '2. Select your prediction'
                    text: 'Analyse the length of the holes and their
                        respective
                        pars, consider how your golfer will approach each hole
                        and decide if it will be a bogey, a par or a birdie.'
                3:
                    img: 'ipad.png'
                    title: '3. Follow the competition live'
                    text: 'Now that the tournament has started, you can follow
                        your golfer on television, as well as inside the
                        game to get full satisfaction from watching
                        something live.'
    "handicap-the-pros":
        name: "handicap-the-pros"
        event: 'handicap-the-pros-play'
        roundGame: true
        logo: 'handicap-the-pros'
        difficulty: 'Hard'
        difficultyStyle: 'red'
        difficultyColor: 'red-diff'
        gameType: 'Daily'
        gameTypeStyle: 'dark-violet'
        gameTypeColor: 'dark-violet-type'
        counterMode: 'round'
        desc: 'All golfers are ranked and given a handicap. You choose on which holes to apply
               the extra strokes. Turn a birdie into an eagle and supercharge
               the score of your golfer! '
        tutorial:
            story: 'In this game we reintroduce the Handicap System to level
                    the playing field between between the various professional
                    golfers. Thus, less successful players gets extra strokes
                    that you get to apply for specific holes. Accompanying the
                    Handicap System is the Stableford points system. The object
                    of the game is to score as many Stableford points as
                    possible, but at the same time also to apply your golfer`s
                    extra handicap strokes to individual holes.
                    Apply your handicap points with care and even the worst
                    golfer will defeat Tiger Woods.'
            steps:
                1:
                    img: 'golf-people.png'
                    title: '1. Choose golfer'
                    text: 'You must only choose 1 golfer, but to help you
                        we`ve provided you with detailed statistics, to choose who
                        you believe will be the winner of the Stableford tournament.'
                2:
                    img: 'eagle.png'
                    title: '2. Apply handicap'
                    text: ' Depending on the handicap of your golfer, you’ll
                        have 1 or more extra strokes to apply for the 18 holes of a
                        round. Choose wisely and if you choose a hole that your
                        golfer scores a birdie on, this is awarded points as an
                        Eagle according to Stableford points, giving you a
                        significant points boost and potentially winning you the
                        tournament.'
                3:
                    img: 'ipad.png'
                    title: '3.Follow the competition live'
                    text: 'Now that the tournament has started, you can
                        follow your golfer throughout the whole tournament on
                        television, as well as inside the game to get full
                        satisfaction from watching this alternative game play'
    "five-ball":
        name: "five-ball"
        event: '5b-play'
        roundGame: true
        logo: 'five-ball'
        difficulty: 'Medium'
        difficultyStyle: 'orange'
        difficultyColor: 'orange-diff'
        gameType: 'Daily'
        gameTypeStyle: 'light-violet'
        gameTypeColor: 'light-violet-type'
        counterMode: 'round'
        desc: 'Build a team of 5 golfers and win by scoring the most points collectively.
               Points system in use: Modified Stableford Scoring.'
        tutorial:
            story: 'Win by scoring the most points by choosing 5 golfers and
                    combine their points from the alternative points scoring
                    system: Stableford. The Stableford rules favour aggressive
                    players who score many birdies, and removes some of the
                    disadvantage when they bogey or double bogey a stroke.
                    The game is a tournament length game.'
            steps:
                1:
                    img: 'golf-people-5.png'
                    title: '1. Choose golfers'
                    text: 'You must choose 5 candidates and you can use
                        detailed statistics, to choose who you believe will
                        be your best choice for a Stableford tournament.'
                2:
                    img: 'ipad.png'
                    title: '2. Follow the competition live'
                    text: 'Now that the tournament has started, you can
                        follow your golfers on television, as well as inside
                        the game to get full satisfaction from watching this
                        alternative game play.'
