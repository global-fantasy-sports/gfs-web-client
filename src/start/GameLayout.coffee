{cx} = require('../lib/utils')
{scrollToTop} = require('../lib/scroll')

Header = require('../components/header/Header')
GuideHeader = require('../components/guide/GuideHeader')
SideMenu = require('../components/SideMenu')
Logo = require('../lib/ui/Logo')
{FirstTime} = require('./FirstTime')



module.exports = React.create(
    displayName: 'GameLayout'

    mixins: [
        flux.Connect("guide", (store) -> guideActive: store.isGuideActive())
        flux.Connect("users", (store) -> user: store.getCurrentUser())
        flux.Connect('state', (store) -> menuOpen: store.getMenuState())
    ]

    componentDidMount: ->
        flux.stores.state.addListener('scroll:to', @updateScrollPosition)

    componentWillUnmount: ->
        flux.stores.state.removeListener('scroll:to', @updateScrollPosition)

    updateScrollPosition: ({el, duration, offset}) ->
        header = React.findDOMNode(@refs['header'])
        scrollToTop(el or header, {
            duration
            offset: offset ? -header.offsetHeight
        })

    onTap: ->
        flux.actions.state.toggleSideMenu(false) if @state.menuOpen

    render: ->
        {guideActive, user, menuOpen} = @state
        {currentState, info} = @props

        Subheader = @props.subheader
        Footer = @props.footer

        `<section className={cx('pageslide-body', {'is-blocked': flux.stores.connection.isDisabled()})}>
            <div className={"segment " + currentState}>
                <div className="app" onTap={this.onTap}>
                    <Header
                        ref="header"
                        sport={SPORT}
                        top={<GuideHeader />}
                        bottom={Subheader ? <Subheader active={currentState} /> : null}
                    />

                    <section className={cx({'app__container': true, 'guide': guideActive})}>
                        {this.props.children}
                    </section>

                    {Footer ? <Footer /> : null}
                </div>

                <SideMenu onTap={this.onTap}
                          open={menuOpen} />

                {user && user.get("first_login") && user.get("country") ?
                    <FirstTime onClose={flux.actions.userAccount.closeFirstTime} user={user}/>
                : null}
            </div>
        </section>`
)
