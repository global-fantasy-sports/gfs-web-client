PopupContainer = require('../lib/ui/PopupContainer')
CompetitionRooms = require('./CompetitionRooms')
LeaderboardBlock = require('./LeaderboardBlock')
EFFBlock = require('./EFFBlock')
PromoSchedule = require('./PromoSchedule')
Header = require('../components/header/Header')
HeaderNav = require('./HeaderNav')
SideMenu = require('../components/SideMenu')
{Footer} = require('../components/landing')
bemClassName = require('bem-classname')

SelectorPage = React.create(
    displayName: 'SelectorPage'

    mixins: [
        flux.Connect("pageData", (store) ->
            currentSport: store.getCurrentSport()
            rooms: store.getRooms()
            overachievers: store.getOverachievers()
        )
    ]

    handleMenuToggle: () ->
        flux.actions.state.toggleSideMenu()

    render: (P, {currentSport, rooms, startTime, overachievers}) ->
        `<div className="app">
            <div className="app__in">
                <Header
                    bottom={<HeaderNav />}
                    onMenu={this.handleMenuToggle}
                />

                <section className="app__content">
                    {currentSport == "nfl" ?
                        <div className="app__row">
                            <div className="app__left">
                                <PromoSchedule />
                                <CompetitionRooms rooms={rooms} sport={currentSport} />
                            </div>
                            <div className="app__right">
                                <EFFBlock />
                                <LeaderboardBlock overachievers={overachievers}/>
                            </div>
                        </div>
                    :
                        <div className={bemClassName("app__unavailable-games", [currentSport])}>
                            <div className="app__game-info">
                                <img src={"/static/img/selector/" + currentSport + "-ball.png"} alt={currentSport}/>
                                <p>
                                    {currentSport} daily fantasy game isn't available to play yet!
                                    Check back throughout the season for updates about the launch date.<br/>
                                    See you soon!
                                </p>
                            </div>
                        </div>
                    }
                    <Footer />
                </section>

                <SideMenu />
                <PopupContainer />
            </div>
        </div>`
)

module.exports = SelectorPage
