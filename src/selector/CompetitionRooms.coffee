{Btn, Icon} = require('lib/ui')

RoomBriefInfo = ({room, sport}) ->
    {isFeatured, isForBeginners, isGuaranteed, entryFee, pot} = room

    handleClick = (e) ->
        e.preventDefault()
        window.location.href = "/#{sport}/competition/#{room.id}/"

    `<div className="competition-rooms__game">
        {(isFeatured || isForBeginners || isGuaranteed) && <div className="competition-rooms__icons">
            {isGuaranteed && <Icon i="guarantee" />}
            {isForBeginners && <Icon i="begginer" />}
            {isFeatured && <Icon i="featured" />}
        </div>}

        <div className="competition-rooms__top">
            {room.fullName1()}
        </div>

        <div className="competition-rooms__bottom">
            <div className="competition-rooms__stakes">
                <div className="competition-rooms__entry">
                    <p className="text text--green text--size-14">$ {entryFee}</p>
                    <p className="text text--gray">ENTRY FEE</p>
                </div>
                <div className="competition-rooms__prize">
                    <p className="text text--green text--size-14">$ {pot}</p>
                    <p className="text text--gray">PRIZE POOL</p>
                </div>
            </div>
            <div className="competition-rooms__btn-enter">
                <Btn mod="red small" onClick={handleClick}>ENTER</Btn>
            </div>
        </div>
    </div>`



module.exports = React.create(
    displayName: 'CompetitionRooms'

    render: ({rooms, sport}) ->
        items = rooms.map((room) ->
            `<RoomBriefInfo key={room.id} room={room} sport={sport} />`
        )

        `<div className="competition-rooms">
            <h4 className="title title--blue title--h4">Featured competitions rooms</h4>
            <div className="competition-rooms__content">
                {items}
            </div>
        </div>`
)
