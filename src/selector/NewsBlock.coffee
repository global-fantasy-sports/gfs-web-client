Connect = require('../flux/connectMixin')


NewsItem = ({date, headline, text, imageSmall, imageBig, link}) ->
    `<div className="news__item">
        <div className="news__img"><img src={imageSmall} /></div>
        <div className="news__brief">
            <p className="text text--gray-light text--size-10 text--uppercase">{date}</p>
            <p className="text text--black text--size-14">{headline}</p>
        </div>
        <div className="news-popup">
            <div className="news-popup__img">
                <img src={imageBig} />
            </div>
            <div className="news-popup__content">
                <p className="text text--lato-medium">{text}</p>
                <a href={link} target="__blank" className="text text--link-arrow">READ MORE</a>
            </div>
        </div>
    </div>`


NewsList = ({news}) ->
    items = news.map((item, index) ->
        `<NewsItem key={index} {...item.toObject()} />`
    )

    `<section className="news">
        <div className="news__top">
            <h3 className="title title--h3 title--black">NEWS</h3>
            <a href="https://news.globalfantasysports.com" target="_blank"
                className="text text--link-arrow">VIEW ALL</a>
        </div>

        {items}
    </section>
    `



module.exports = React.create(
    displayName: 'NewsBlock'

    mixins: [
        Connect('news', (store) -> store.getState())
    ]

    componentDidMount: () ->
        flux.actions.news.fetch()

    render: () ->
        switch @state.status
            when 'done'
                `<NewsList news={this.state.news} />`
            when 'error'
                `<p>Error: {this.state.error}</p>`
            else
                `<p>Loading…</p>`

)
