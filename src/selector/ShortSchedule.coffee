ScheduleItem = ({contest}) ->
    time = contest.formatTime('h:mm A')
    day = contest.formatTime('ddd')

    `<div className="schedule__item">
        <div className="schedule__team schedule__team--left">{contest.homeTeam}</div>
        <div className="schedule__team-logo">
            <img src={contest.homeTeamPhoto} />
        </div>

        <div className="schedule__time">
            <p className="text text--blue text--size-10 text--uppercase">{day}</p>
            <p className="text text--size-10 text--uppercase">{time}</p>
        </div>

        <div className="schedule__team-logo">
            <img src={contest.awayTeamPhoto} />
        </div>
        <div className="schedule__team schedule__team--right">{contest.awayTeam}</div>
    </div>`

ScheduleItem.displayName = 'ScheduleItem'


module.exports = React.create(
    displayName: 'ShortSchedule'

    mixins: [flux.Connect('pageData', (store) ->
        contests: store.getContests().toList().take(3)
    )]

    render: ({}, {contests}) ->
        items = contests.map((contest) ->
            `<ScheduleItem key={contest.id} contest={contest} />`
        )

        `<div className="schedule__info">
            <h4 className="title title--h4 title--gray title--lato-black">
                SCHEDULE
            </h4>

            {items}
        </div>`
)
