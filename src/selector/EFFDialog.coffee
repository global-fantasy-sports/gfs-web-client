CommonDialog = require('../components/dialogs/CommonDialog')
Close = require('../lib/ui/Close')
{Btn} = require('../lib/ui')
{Input, InputMessage} = require('../lib/forms')


{EMAIL_RE} = require('../lib/const')

validateEmail = (email = '') ->
    email.trim().search(EMAIL_RE) is 0


module.exports = React.create(
    displayName: 'EFFDialog'

    getInitialState: () -> {
        name: ''
        email: ''
        error: false
    }

    handleName: (e) ->
        @setState({name: e.target.value})

    handleEmail: (e) ->
        @setState({email: e.target.value, error: false})

    handleSubmit: (e) ->
        e.preventDefault()

        {email, name} = @state

        if not validateEmail(email)
            @setState({error: 'Please enter a valid email'})
        else
            flux.actions.connection.registerEFF({name, email})
                .catch(@handleError)

    handleError: (error) ->
        alert(error.toString())

    render: () ->
        {name, email, error} = @state

        `<CommonDialog className="popup">
            <Close onClose={this.props.onHide} className="popup__close"/>
            <div className="popup__title"><h4 className="title title--h4 title--black">EQUITY FOR FANS</h4></div>
            <div className="">
                <form onSubmit={this.handleSubmit}>
                    <Input
                        name="name"
                        placeholder="Name"
                        value={name}
                        onChange={this.handleName}
                    />
                    <Input
                        name="email"
                        placeholder="Email"
                        value={email}
                        error={error}
                        onChange={this.handleEmail}
                    />

                    <Btn mod="red small" type="submit">
                        Register
                    </Btn>
                </form>
            </div>
        </CommonDialog>`
)
