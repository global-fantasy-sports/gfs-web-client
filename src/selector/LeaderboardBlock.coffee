{cx} = require('lib/utils')

LeaderboardItem = ({player, num}) ->

    playerClass = cx('leaderboard__player', {
        'leaderboard__player--left': num == 0
        'leaderboard__player--center': num == 1
        'leaderboard__player--right': num == 2
    })
    playerPhoto = player.get('photoSmall') or "/static/img/nfl-player-placeholder.png"

    `<div className={playerClass}>
        <img src={playerPhoto} alt={player.get('nameFull')} className="leaderboard__player-img"/>
        <div className="leaderboard__pedestal">
            <img src="/static/img/selector/selector-leader-block.png"
                 className="leaderboard__pedestal-img"/>
            <p className="leaderboard__points">{player.get('points')}</p>
            <p className="leaderboard__name">{player.get('nameFull')}</p>
            <p className="leaderboard__position">{player.get('playerPosition')}</p>
        </div>
    </div>`

module.exports = React.create(
    displayName: 'LeaderboardBlock'

    render: ({overachievers}) ->
        items = overachievers.map((overachiever, i) ->
            return null unless overachiever
            `<LeaderboardItem player={overachiever} key={i} num={i}/>`
        )

        `<div className="leaderboard">
            <h4 className="title title--h4 title--blue leaderboard__title">Last week overachievers</h4>
            <div className="leaderboard__content">
                {items}
            </div>
        </div>`
)
