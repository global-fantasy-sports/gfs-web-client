moment = require('moment')
{pad} = require('../lib/utils')
{now} = require('../lib/date')


class CountdownClock extends React.Component

    componentDidMount: () ->
        @_interval = setInterval((() => @forceUpdate()), 1000)

    componentWillUnmount: () ->
        clearInterval(@_interval)

    render: () ->
        timeLeft = moment.duration(@props.toUTCDate - now(), 'ms')
        if timeLeft < 0
            timeLeft = moment.duration(0, 'ms')

        months = timeLeft.get('months') or null

        `<div className="time-counter__clock">
            {months && <div className="time-counter__item">
                <p className="text text--size-24">{months}</p>
                <p className="text text--gray-dark text--size-12">MON</p>
            </div>}
            <div className="time-counter__item">
                <p className="text text--size-24">{timeLeft.get('days')}</p>
                <p className="text text--gray-dark text--size-12">DAYS</p>
            </div>
            <div className="time-counter__item">
                <p className="text text--size-24">{pad(timeLeft.get('hours'))}</p>
                <p className="text text--gray-dark text--size-12">HOURS</p>
            </div>
            <div className="time-counter__item">
                <p className="text text--size-24">{pad(timeLeft.get('minutes'))}</p>
                <p className="text text--gray-dark text--size-12">MIN</p>
            </div>
            <div className="time-counter__item">
                <p className="text text--size-24">{pad(timeLeft.get('seconds'))}</p>
                <p className="text text--gray-dark text--size-12">SEC</p>
            </div>
        </div>`


module.exports = CountdownClock
