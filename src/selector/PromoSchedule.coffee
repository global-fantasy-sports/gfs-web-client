moment = require('moment')
{cx} = require('lib/utils')
CountdownClock = require('./CountdownClock')
ShortSchedule = require('./ShortSchedule')
{fromUtcToLocal} = require('../lib/date')


PromoSchedule = React.create(
    displayName: 'PromoSchedule'

    mixins: [
        flux.Connect("pageData", (store) ->
            currentSport: store.getCurrentSport()
            startTime: store.getStartTime()
        )
    ]

    render: (P, {currentSport, startTime}) ->
        cls = "schedule schedule--#{currentSport}"
        gameURL = "/#{currentSport}/"

        title = null
        if currentSport == 'nfl'
            room = flux.stores.pageData.getRooms().first()
            if room
                week = room.getIn(['kwargs', 'week'])
                title = `<h1 className="title title--h1">WEEK {week}</h1>`

        startTimeLabel = if startTime
                fromUtcToLocal(startTime).format('h:mm A dddd | MMMM Do')
            else
                ''

        `<div className={cls}>
            <div className="schedule__left">
                <div className="schedule__top">
                    <p className="text text--gray-light text--size-12">
                        {startTimeLabel}
                    </p>
                    {title}
                    <h4 className="title title--h4 title--red">Regular season</h4>
                </div>
                <div className="schedule__bottom">
                    <a href={gameURL} className="btn btn--red btn--inline btn--size-16">ENTER</a>
                    <div className="time-counter">
                        <p className="text text--gray-light text--uppercase text--size-10">Next game begins in</p>
                        <CountdownClock toUTCDate={startTime} />
                    </div>
                </div>
            </div>

            <div className="schedule__right" />
        </div>`
)

module.exports = PromoSchedule
