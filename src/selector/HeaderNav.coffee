{cx, prevent} = require('../lib/utils')


HeaderNavItem = React.create({
    displayName: 'HeaderNavItem'
    mixins: [
        flux.Connect("pageData", (store, props) ->
                active: store.getCurrentSport() is props.name
        )
    ]

    switchSelectorPage: ->
        flux.actions.selector.setCurrentSport(@props.name)

    render: ({children, href, name, disabled}, {active}) ->
        className = cx('head__games-item', {
            'head__games-item--active': not disabled and active is name
            'head__games-item--disabled': disabled
        })

        if disabled
            `<span className={className}>{children}</span>`
        else
            `<a
                className={className}
                href={href || '#'}
                onTap={this.switchSelectorPage}
                onClick={prevent}
            >
                {children}
            </a>`

})

HeaderNav = React.create({
    displayName: 'HeaderNav'
    render: () ->
        `<div className="head__bottom">
            <div className="app__row">
                <div className="head__left"/>

                <nav className="head__games-list">
                    <HeaderNavItem name="nfl">
                        NFL
                    </HeaderNavItem>
                    <HeaderNavItem name="basketball">
                        BASKETBALL
                    </HeaderNavItem>
                    <HeaderNavItem name="soccer">
                        SOCCER
                    </HeaderNavItem>
                    <HeaderNavItem name="golf">
                        GOLF
                    </HeaderNavItem>
                    <HeaderNavItem name="baseball">
                        BASEBALL
                    </HeaderNavItem>
                </nav>
            </div>
        </div>`
})


module.exports = HeaderNav
