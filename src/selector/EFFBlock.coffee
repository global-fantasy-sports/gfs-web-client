{Btn} = require('../lib/ui')


class EFFBlock extends React.Component

    handleShowDialog: (e) =>
        e.preventDefault()
        flux.actions.state.openEFFDialog()

    render: () ->
        `<section className="news">
            <div className="news__top">
                <h3 className="title title--h3 title--black">Equity for Fans</h3>
            </div>
            <div className="news__content text text--size-12 text--black">
                GFS offers its new users a unique opportunity through its “Equity for Fans”-program (EFF),
                whereby new customers can take a slice of the company when they deposit money to their account.
                Once the deposit is verified GFS will return a balanced amount of shares to the new depositing user.
                Existing fans of DFS will not only get to play on the newest and best evolution of daily fantasy sports,
                but the money deposited also secures a share of the success of GFS.
                Whether a user deposits small or big, there’s a share option that fits. Click below to sign-up for
                our “I’m interested”-newsletter. And GFS will let you know when this option opens up.
            </div>

            <div className="news__btn">
                <Btn mod="red small" onTap={this.handleShowDialog}>
                    CLICK HERE TO SIGNUP
                </Btn>
            </div>
        </section>`


module.exports = EFFBlock
