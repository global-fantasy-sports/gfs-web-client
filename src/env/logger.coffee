config = require "../config.js"
{localStorage} = require './storage'
{allow, block} = config["log-channels"]
allowedGroups = []
blockedGroups = []


logMessage = (value, message, group) ->
    if value?
        console.log "### #{group} ###: #{message}:", value
    else
        console.log "### #{group} ###: #{message}"

module.exports =
    init: ->
        allow = localStorage.getItem("allow") or null
        block = localStorage.getItem("block") or "*"

        if allow and allow != "*"
            groups = allow.split ","
            allowedGroups = (group.trim() for group in groups)

        if block and block != "*"
            groups = block.split ","
            blockedGroups = (group.trim() for group in groups)

    clear: ->
        localStorage.removeItem "allow"
        localStorage.removeItem "block"
        @init()

    add: (group) ->
        unless group in allowedGroups
            allow = "#{allow ? ''}, #{group}"
            localStorage.setItem "allow", allow

        @init()


    passed: (group) ->
        return false unless allow?

        if (allow == "*" and block == "*") or (allow == "*" and not block?) or (not block? and allow?)
            return true

        if block == "*" and allow?
            if group in allowedGroups
                return true

        if (allow == "*" and block?) or (block? and block != "*" and allow?)
            if group not in blockedGroups
                return true

            return false


    return: (value, message, group) ->
        @log value, message, group

        return value

    log: (value, message, group) ->
        if arguments.length == 2
            if @passed(arguments[1])
                logMessage null, arguments[0], arguments[1]
            return

        if @passed(group)
            logMessage value, message, group
