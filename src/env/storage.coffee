class MockStorage
    constructor: ()->
        @_data = {}

    getItem: (key)->
        @_data[key] ? null

    setItem: (key, value)->
        @_data[key] = value

    removeItem: (key)->
        delete @_data[key]


getStorage = (type)->
    storage = global[type]
    try
        storage.setItem('storagetest', 1)
        storage.removeItem('storagetest', 1)
        return storage
    catch e
        return new MockStorage()


module.exports = {
    localStorage: getStorage('localStorage')
    sessionStorage: getStorage('sessionStorage')
}
