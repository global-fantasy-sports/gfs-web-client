_ = require "underscore"
{choice} = require 'lib/utils'


document.onkeydown = (e) ->
#    console.log('#################### e is', e)

    if e.which == 77 && e.ctrlKey
        flux.actions.roster.autoFill()
        return false

    if e.which == 188 && e.ctrlKey
        flux.actions.roster.autoFill(true)
        return false

    if e.which == 686 # Ctrl + D
        local = require('./local-debug')
        local()

sample = (coll, n) ->
    ids = coll.map( (el) -> el.get("id")).toList().toJS()
    id = _.sample(ids, n)

    if _.isArray(id)
        coll.filter((el) -> _(id).contains(el.get("id")))
    else
        coll.find((el) -> el.id == id)

pickTournamentAndRound = ->
    activeTournaments = flux.stores.tournaments.getTournamentsAllowedToStart().filter(
        (tour) ->
            not flux.stores.rooms.getTournamentCompetitionRooms(tour.get('id')).isEmpty()
    )

    tournament = sample(activeTournaments)
    flux.actions.wizard.setTournamentId(tournament.id)

    while true
        r = _.sample([1, 2, 3, 4])
        if flux.stores.state.isRoundAvailable(r) then break

    flux.actions.wizard.setEvent(tournament.id, r)


pickCandidates = (n = 3) ->
    tournamentId = flux.stores.wizard.getTournamentId()
    participants = flux.stores.participants.getParticipants({tournamentId})

    notCut = participants
            .toList()
            .filter((p) ->
                status = p.get("status")
                !(status == "cut" or status == "withdrawn")
            )

    participants = sample(notCut, n)

    participants.map (p, i) ->
        id = p.get("id")
        flux.actions.tap.selectParticipant(id)

pickPrediction = ->
    for i in [0, 1, 2]
        id = flux.stores.wizard.getSelectedParticipantIds().toList().get(i)
        prediction = ([1..18].map -> false).map(-> choice([-1, 0, 1]))
        flux.actions.wizard.setPrediction(id, prediction)

    flux.actions.wizard.setCurrentSubstep(3)


module.exports = {
    holeByHole: () ->
        router = global.__router
        path = router.state.location.pathname

        if path == "/"
            router.transitionTo("/#{SPORT}/hole-by-hole/")

        if path == "/hole-by-hole/round" or path == "/hole-in-one/round"
            pickTournamentAndRound()

        if path == "/hole-by-hole/select" or path == "/hole-in-one/select"
            pickCandidates()

        if path == "/hole-by-hole/predict"
            pickPrediction()

    fiveBall: ->
        router = global.__router
        path = router.state.location.pathname

        if path == "/golf/"
            router.transitionTo("/#{SPORT}/five-ball/")

        if path == "/golf/five-ball/round/"
            pickTournamentAndRound()

        if path == "/golf/five-ball/select/"
            pickCandidates(5)
}
