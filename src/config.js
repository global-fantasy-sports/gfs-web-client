var server = SPORT === '' ? '' : '/' + SPORT + '/api';
// api_url is got from index.html
module.exports = {
    "ignoreGeoIP": process.env.GEOIP_BLOCK !== 'block',
    "ignoreWrongLocation": false,
    "debug": true,
    "server": server,
    "cookiename": "session",
    "log-channels": {
        allow: '*'
        // block: "*"
    }
};
