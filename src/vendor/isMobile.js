/*eslint-env browser*/
/**
 * Forked from isMobile.js v0.3.9
 *
 * A minified version of this script is embedded into index.html
 *
 * @author: Kai Mallea (kmallea@gmail.com)
 * @license: http://creativecommons.org/publicdomain/zero/1.0/
 */
(function (ua, innerWidth) {
    var tests = [
        /(?:iPhone|iPad|iPod)/i,
        /Android/i,
        /IEMobile/i,
        /(?=.*\bWindows\b)(?=.*\bARM\b)/i, // Match 'Windows' AND 'ARM'
        /(?:BlackBerry|BB10)/i,
        /Opera Mini/i,
        /(CriOS|Chrome)(?=.*\bMobile\b)/i,
        /(?=.*\bFirefox\b)(?=.*\bMobile\b)/i // Match 'Firefox' AND 'Mobile'
    ];

    // Facebook mobile app's integrated browser adds a bunch of strings that
    // match everything. Strip it out if it exists.
    var tmp = ua.split('[FBAN');
    if (typeof tmp[1] !== 'undefined') {
        ua = tmp[0];
    }

    for (var i = 0, l = tests.length; i < l; i++) {
        if (tests[i].test(ua)) {
            return true;
        }
    }

    return innerWidth <= 800;

})(navigator.userAgent, window.innerWidth);
