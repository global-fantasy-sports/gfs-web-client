{getStakeRooms} = require '../stakerooms/stakerooms'
{StakesList} = require '../stakerooms/stakesize'
GuideHint = require '../guide/GuideHint'
WizardCompetitionRoomList = require '../stakerooms/WizardCompetitionRoomList'

module.exports = React.create
    displayName: 'StakeSelect'

    mixins: [
        flux.Connect('rooms', (store) ->
            filters = []
            if SPORT == 'nba'
                selectedEventId = flux.stores.wizard.getSelectedEvent().id
                filters = [
                    (r) -> r.sportEventId == selectedEventId
                ]
            {rooms: store.getRooms([], filters).toList()}
        )
        flux.Connect('wizard', (store) -> {
            selectedRoomId: store.getCompetitionRoomId()
            gameState: store.getGameState()
        })
    ]


    getInitialState: ->
        roomId = flux.stores.wizard.getCompetitionRoomId()

        if roomId is null
            preselected = false
            deactivateRooms = false
        else
            preselected = true
            deactivateRooms = true

        {
            deactivateRooms
            preselected
            entryFeeType: 'all'
        }

    componentWillUpdate: (nextProps, nextState) ->
        if nextState.gameState == 'saved'
            @props.onNext()

    onSelect: (roomId) ->
        flux.actions.wizard.setCompetitionRoomId(roomId)

    next: ->
        flux.actions.wizard.saveGame()

    toggleRooms: (t) ->
        (e) =>
            e.preventDefault()
            @setState(entryFeeType: t)

    onExpandFilter: ->
        @setState(expanded: not @state.expanded)

    render: (props, {selectedRoomId, rooms, deactivateRooms, preselected, entryFeeType, expanded}) ->
        filter = (room) -> true
        onSelect = @onSelect
        publicRooms = getStakeRooms(rooms, [filter])
        tokenRooms = rooms.filter (room) -> room.mode == 'token'
        usdRooms = rooms.filter (room) -> room.mode == 'usd'

        `<WizardCompetitionRoomList />`
