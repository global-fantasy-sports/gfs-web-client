_ = require 'underscore'
LinkedStateMixin = require('react-addons-linked-state-mixin')
{Input, Checkbox, InputMessage} = require '../lib/forms'
{SelectBox} = require '../lib/ui/SelectBox'
{Btn, Link} = require '../lib/ui'
{Titlebar} = require '../lib/ui/Titlebar'
Logo = require '../lib/ui/Logo'
{Footer} = require './landing'
{cx} = require '../lib/utils'
flash = require '../lib/flash'
{STATE} = require '../lib/const'
# FIXME: challenge requests are broken
# {challengeRequests} = require './data'
countries = require '../lib/constants/countries'
states = require '../lib/constants/states'
API = require '../flux/API'


HelloUser = React.create
    displayName: 'HelloUser'

    getInitialState: ->
        tab: 'aboutUs'

    toggle: (tab) ->
        (e) =>
            @setState(tab: tab)

    render: ({children}, {tab}) ->
        `<div className="hello-user">
            <div className="hello-user__top">
                <div className="hello-user__title">
                    <Link to={"/" + SPORT + "/"}>
                        <Logo
                            logo={SPORT === 'golf' ? 'gfg-logo' : 'football-logo'}
                            alt={SPORT === 'golf' ? 'Global Fantasy Sports Golf' : 'Global Fantasy Sports Football'}
                        />
                    </Link>
                    <Titlebar
                        mod="h2"
                        title={SPORT === 'golf' ? 'The leader in Fantasy Golf' : 'The leader in Fantasy Football'}
                        className="regular centered white"
                    />
                </div>
                <div className="hello-user__center">
                    <div className="hello-user__l">
                        <div className="hello-user__content">
                            {LOGIN_CONTENT['howToPlay']}
                            <div className="hello-user__diamond">
                                <img src="/static/img/diamond.png"/>
                            </div>
                        </div>
                    </div>
                    <div className="hello-user__r">
                        <div className="hello-user__sign-up">
                            {children}
                        </div>
                    </div>
                </div>
            </div>

            <div className="hello-user__bottom">
                <div className="hello-user__tab">
                    <div className="hello-user__tab-wrap">
                        <Titlebar title="About us" onTap={this.toggle('aboutUs')}
                                  className={cx('app__title hello', {'active': tab == 'aboutUs'})}/>
                        <Titlebar title="Why Global Fantasy Sports" onTap={this.toggle('whyHowFar')}
                                  className={cx('app__title hello', {'active': tab == 'whyHowFar'})}/>
                    </div>
                </div>
                <div className="hello-user__info">
                    {LOGIN_CONTENT[tab]}
                </div>
            </div>
        </div>`


exports.LoginWrapper = React.create
    displayName: 'LoginWrapper'

    componentDidMount: ->
        # FIXME
#        pathname = @props.location.pathname
#        parts = pathname.split('/')
#        if parts[1] == 'challenge'
#            challengeRequests.on 'sync', =>
#                id = pathname.slice(1).replace /\//g, ""
#                request = challengeRequests.get id
#
#                if request
#                    logger.log id, "challenge request id was received", "ChallengeRequest"
#                    localStorage['hfg-init'] = JSON.stringify
#                        action: 'stakeroom-challenge'
#                        roomId: request.get "stake_room_id"
#
#                    @props.onChallenge()

    render: ({children})->
        `<section className="app login-page">
            <section className="app__container">
                <section className="auth">
                    <div className="auth__in">
                        {children}
                    </div>
                </section>
            </section>

          <Footer />
        </section>`


exports.AuthBlock = React.create
    displayName: 'AuthBlock'

    getInitialState: -> {
        tab: 'sign-up'
    }

    toggleSignUp: (tab) ->
        (event)=>
            event.preventDefault()
            @setState(tab: tab)

    render: (P, {email, tab, passwordReset, termsAndConditions, checkboxError, isCheckboxError})->
        Form = if tab is 'sign-up' then AuthSignupForm else AuthLoginForm
        `<HelloUser>
            <div className={cx("sign-up", {"login": tab == "login"})}>
                <div className="sign-up__in" />
                <div className="sign-up__tabs">
                    <div onTap={this.toggleSignUp('sign-up')}
                         className={cx({'sign-up__tab': true, 'active': tab == 'sign-up'})}>
                        Sign Up
                    </div>
                    <div onTap={this.toggleSignUp('login')}
                         className={cx({'sign-up__tab': true, 'active': tab == 'login'})}>
                        Login
                    </div>
                </div>
                <Form />
            </div>
        </HelloUser>`


class AuthSocialButtons extends React.Component

    shouldComponentUpdate: ->
        false

    render: ->
        `<div className="sign-up__social">
            <Btn onTap={flux.actions.connection.facebookLogin}
                 icon="fb-white"
                 name="facebook"
                 mod="facebook">
                Facebook
            </Btn>
            <Btn onTap={flux.actions.connection.googleLogin}
                 mod="google"
                 icon="google">
                Google
            </Btn>
        </div>`


class AuthSignupForm extends React.Component

    constructor: (props)->
        super(props)
        @form = flux.stores.connection.getSignupForm()

    componentDidMount: ->
        @unsubscribe = @form.listen(=> @forceUpdate())

    componentWillUnmount: ->
        @unsubscribe()

    handleSubmit: (event)->
        event.preventDefault()
        flux.actions.connection.signup()

    render: ->
        form = @form
        termsError = form.getErrors('terms')
        `<form className="sign-up__form" onSubmit={this.handleSubmit}>
            <AuthSocialButtons />

            <div className="sign-up__email">
                <Input placeholder="Email"
                       hint="We will NOT spam your inbox!"
                       {...form.props('email')} />
            </div>
            <div className="sign-up__fields">
                <Input placeholder="Name" {...form.props('name')} />
                <Input placeholder="Password" {...form.props('password')} />
            </div>
            <div className="sign-up__select">
                <SelectBox mod="row" {...form.props('country')} />
                {form.get('country') !== 'US'
                    ? <SelectBox mod="row" disabled={true} />
                    : <SelectBox mod="row" {...form.props('state')} />}
            </div>
            <div className="sign-up__checkbox">
                {termsError && <InputMessage message={termsError} mod="checkbox" />}
                <Checkbox {...form.props('terms')} >
                    <div className="sign-up__checkbox-text">
                        I agree to the Global Fantasy Sports Golf™&nbsp;
                        <Link to="/contest-rules/">
                            <span className="stroke extra-small blue">Contest Rules</span>
                        </Link>,{' '}
                        <Link to="/terms/">
                            <span className="stroke extra-small blue">Terms &amp; Conditions</span>
                        </Link> and{' '}
                        <Link to="/policy/">
                            <span className="stroke extra-small blue">Privacy Policy</span>
                        </Link>,{' '}
                        and that I am of legal age to play Fantasy Sports in my
                        state of residency.
                    </div>
                </Checkbox>
            </div>

            <div className="sign-up__finish">
                <Btn mod="red-linear big high block" type="submit" name="register">
                    Sign Up
                </Btn>
            </div>
        </form>`


class AuthLoginForm extends React.Component

    constructor: (props)->
        super(props)
        @form = flux.stores.connection.getLoginForm()

    componentDidMount: ->
        @unsubscribe = @form.listen(=> @forceUpdate())

    componentWillUnmount: ->
        @unsubscribe()

    handleSubmit: (event)=>
        event.preventDefault()
        flux.actions.connection.login()

    handleResetPassword: =>
        flux.actions.state.openResetPasswordDialog(@form.get('email'))

    render: ->
        form = @form
        `<form className="sign-up__form" onSubmit={this.handleSubmit}>
            <AuthSocialButtons />

            <div className="sign-up__email">
                <Input placeholder="Email" {...form.props('email')} />
            </div>
            <div className="sign-up__fields">
                <Input placeholder="Password" {...form.props('password')} />
            </div>

            <div className="ui-input__pass-forgot" onTap={this.handleResetPassword}>
                Forgot password?
            </div>

            <div className="sign-up__finish">
                <Btn mod="red-linear big high block" type="submit">
                    Login
                </Btn>
            </div>
        </form>`



LOGIN_CONTENT =
    aboutUs: `<div className="about">
        <div className="about__l">
            <p>
                Global Fantasy Sports is a Florida based fantasy sports company which owns a
                license to operate the “Global Fantasy Sports Golf” brand in the U.S.
            </p>
            <p>
                We aim to create authentic sports fantasy games that focus on the
                strategic elements and insights of the sport, rather than just
                the outcome of a tournament.
            </p>
            <p>
                We all share the aim of creating authentic sports fantasy games that
                rely on more than the outcome of a sports tournament, but rather have
                an intense focus on the strategic understanding of game-play elements
                and insights into the sport. Our focus is on creating a fresh take on
                what fantasy games can be
            </p>
        </div>
        <div className="about__r">
            <section className="banner">
                <img src="/static/img/b-invite-friends.png"
                     alt="Play against your friends in one of our exciting competition rooms"/>
            </section>
        </div>
    </div>`

    howToPlay: `<div className="about list">
        <ul>
            <li>
                Select your preferred golfers and predict their play.
            </li>
            <li>
                Go head to head with a friend or play with a group of friends.
            </li>
            <li>
                No season-long commitment — simply play when you want.
            </li>
            <li>
                Play for real money against other fans. Immediate cash payouts.
            </li>
        </ul>
    </div>`

    whyHowFar: `<div className="about">
        <div className="about__l">
            <p>
                What could be more perfect than finishing 18 holes, hitting the couch and
                following the tournament on live TV while relaxing with fantasy golf? Nothing,
                we say! The tournament comes to life and the competition heats up. So kick back
                and enjoy the pros in the bunker - from your bunker!
            </p>
            <p>
                We’re dedicated to improving our games, and we’d love to hear any feedback
                you might have. Do you have a game idea? Suggestions for improvements?
                New features that could make the games more fun?
                Use the “feedback” snippet in the bottom right corner.
            </p>
        </div>
        <div className="about__r">
            <section className="banner">
                <img src="/static/img/b-hole-by-hole.png"
                     alt="You have a good chance with prizes for at least 7 participants"/>
            </section>
        </div>
    </div>`

    video: `<div className="video-frame">
        <iframe src=""
                width="100%"
                height="100%"
                frameBorder="0"
                allowFullScreen/>
    </div>`


exports.PasswordChange = React.create
    mixins: [LinkedStateMixin]

    getInitialState: ->
        linkValid: null
        password1: ''
        password2: ''
        tab: 'aboutUs'

    contextTypes:
        router: React.PropTypes.object

    componentDidMount: ->
        API.checkResetPasswordLink(@props.routeParams)
            .then(=> @setState(linkValid: true))
            .catch((error)=>
                flash(type: 'error', text: error.message)
                @goHome()
            )

    goHome: ->
        _.defer => @context.router.transitionTo("/#{SPORT}/")

    setPassword: ->
        {password1, password2} = @state
        API.changePassword(_.extend({password1, password2}, @props.routeParams))
            .then(=> @goHome())
            .catch((error)-> flash(type: 'error', text: error.message))

    render: () ->
        return null unless @state.linkValid

        `<HelloUser>
            <div className="sign-up password">
                <div className="sign-up__title"></div>
                <div className="sign-up__form">
                    <div className="sign-up__password">
                        <Input type="password"
                               placeholder="New password"
                               name="password1"
                               valueLink={this.linkState('password1')} />
                    </div>
                    <div className="sign-up__password">
                        <Input type="password"
                               placeholder="Confirm password"
                               name="password2"
                               valueLink={this.linkState('password2')} />
                    </div>

                    <div className="sign-up__finish">
                        <Btn mod="red-linear big high block"
                             onTap={this.setPassword}>
                            Ok
                        </Btn>
                    </div>
                </div>
            </div>
        </HelloUser>`
