{Btn} = require 'lib/ui'
{Flag} = require 'lib/ui/Flag'
ItemWrapper = require '../stakerooms/ItemWrapper'



exports.LeaderTableRow = React.create
    displayName: "LeaderTableRow"

    mixins: [flux.Connect("participantRoundResults", (store, props) ->
        participant_id = props.participant.id

        score1: store.getResult({participant_id, round_number: 1}).getScore()
        score2: store.getResult({participant_id, round_number: 2}).getScore()
        score3: store.getResult({participant_id, round_number: 3}).getScore()
        score4: store.getResult({participant_id, round_number: 4}).getScore()
    )]

    getScore: ->
        v = @props.participant.getScore()
        if v > 0
            "+#{v}"
        else
            "#{v}"

    render: ({participant, idx, onScore, onStats}, {score1, score2, score3, score4}) ->
        cls = "items-align-center dark bold leaderboard dark-small"
        currentRound = @props.currentRound
        if currentRound
            id = participant.id
            opt = {
                "participant_id": id
                "round_number": currentRound
            }

            currentRoundResults = flux.stores.participantRoundResults.getResult(opt)

        statsAndScore = `<div className="btn-inline">
            {onScore ?
            <Btn mod="blue square regular small"
                 iconBg={true}
                 icon="view"
                 onTap={onScore(participant)}>
                Score Card
            </Btn>
                : null }
            {onStats ?
            <Btn mod="blue square regular small"
                 iconBg={true}
                 icon="stats"
                 onTap={onStats(participant)}>
                Statistics
            </Btn>
                : null }
        </div>`

        `<ItemWrapper {...this.props} mod={cls}>
            <section className="f-8">
                <span className="place-badge yellow">
                    {idx + 1}
                </span>
            </section>
            <section className="f-7">
                <div className="avatar">
                    <Flag country={participant.country}/>
                </div>
            </section>
            <section className="f-23">
                <span className="stroke bold x2-medium">
                    {participant.getFullName()}
                </span>
            </section>
            <section className="f-17">
                <span className="stroke white x-big bold">
                    {participant.rank}
                </span>
            </section>
            <section className={"f-6" + (currentRound == 1 ? " bg-red" : "")}>
                <span className="stroke white x-big bold">
                    {score1}
                </span>
            </section>
            <section className={"f-7" + (currentRound == 2 ? " bg-red" : "")}>
                <span className="stroke white x-big bold">
                    {score2}
                </span>
            </section>
            <section className={"f-7" + (currentRound == 3 ? " bg-red" : "")}>
                <span className="stroke white x-big bold">
                    {score3}
                </span>
            </section>
            <section className={"f-7" + (currentRound == 4 ? " bg-red" : "")}>
                <span className="stroke white x-big bold">
                    {score4}
                </span>
            </section>
            <section className="f-18">
                <span className="data-item green">
                    <span className="stroke x-big bold">
                        {this.getScore()}
                    </span>
                </span>
            </section>
        </ItemWrapper>`
