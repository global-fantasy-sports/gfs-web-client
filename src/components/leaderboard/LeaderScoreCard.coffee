{StatusBadge} = require 'lib/ui'
{MemberTable} = require '../tournament/MemberTable'
{Titlebar} = require 'lib/ui/Titlebar'
TournamentCounter = require '../tournament/TournamentCounter'
{TourNames} = require '../tournament/TourName'
TournamentLegend = require '../tournament/TournamentLegend'

exports.LeaderScoreCard = React.create
    displayName: "LeaderScoreCard"

    mixins: [flux.Connect("participantRoundResults", (store, props) ->
        participantId = props.participant.id
        roundResults: [
            store.getResult({participantId, roundNumber: 1})
            store.getResult({participantId, roundNumber: 2})
            store.getResult({participantId, roundNumber: 3})
            store.getResult({participantId, roundNumber: 4})
        ]
    )]

    render: ({participant}, {roundResults}) ->
        tour = participant.getTournament()

        els = for roundResult in roundResults
            strokeClass = (i) ->
                return "" if roundResult.get("strokes", immutable.Map()).isEmpty()
                strokes = roundResult.get("strokes", immutable.List())
                pars = roundResult.get("pars", immutable.List())
                strokeType(strokes.get(i) - pars.get(i))

            `<div className="team-results" key={roundResult.get("id")}>
                <p className="team-results__title">Round {i+1}</p>
                <MemberTable
                    member={participant}
                    participantRoundResult={participantRoundResult}
                    roundNumber={i+1}
                    strokeClass={strokeClass} />
            </div>`

        `<section className="team-results">
            <div className="team-results__tour-info">
                <div className="team-results__row">
                    <Titlebar title={participant.getFullName()} className="red centered"/>
                    <div className="team-results__tour-info">
                        <TourNames tournament={tour}/>
                        <TournamentCounter tournament={tour} mod="tour"/>
                        {StatusBadge(tour, "big")}
                    </div>
                </div>
            </div>

            <div className="stats-table__legend">
                <TournamentLegend />
            </div>
            <div className="team-results__tables">
                {els}
            </div>
        </section>`
