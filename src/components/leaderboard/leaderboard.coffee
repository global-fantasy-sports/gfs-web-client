GameEmbed = require('../../lib/ui/GameEmbed')
Popup = require('../../lib/ui/Popup')
{BtnLink, StatusBadge} = require '../../lib/ui'
{Titlebar} = require '../../lib/ui/Titlebar'
TournamentCounter = require '../tournament/TournamentCounter'
{TourNames} = require '../tournament/TourName'
{TourDates} = require '../tournament/TourDates'
TourData = require '../tournament/TourData'
CandidateStats = require '../CandidateStats'
{LeaderScoreCard} = require './LeaderScoreCard'
{LeaderTable} = require './LeaderTable'


exports.Leaderboard = React.create
    displayName: "Leaderboard"
    mixins: [
        flux.Connect("participants", (store, props) ->
            participants: store.getParticipants({tournamentId: +props.tournamentId})
        )
    ]

    getInitialState: ->
        selected: null
        modal: null

    onScore: (participant) ->
        (e) =>
            @setState(modal: "scores", selected: participant)

    onStats: (participant) ->
        (e) =>
            @setState(modal: "stats", selected: participant)

    closeModal: ->
        @setState modal: null, selected: null

    render: ({tournamentId}, {selected, modal, participants})->
        if participants.isEmpty()
            return `<div/>`

        tournament = flux.stores.tournaments.getTournament(+tournamentId)

        popupView = switch modal
            when "scores"
                `<LeaderScoreCard participant={selected} />`
            when "stats"
                `<CandidateStats participant={selected}/>`
            else
                null

        `<div className="leader-board">

            {modal ?
            <Popup>
                <GameEmbed onClose={this.closeModal}>
                    {popupView}
                </GameEmbed>
            </Popup>
                : null}

            <div className="leader-board__head">
                <div className="leader-board__title">
                    <Titlebar className="" title="Live Leaderboard" mod="h2"/>
                    <BtnLink to="/golf/" mod="white-linear wide">Back</BtnLink>
                </div>
            </div>
            <div className="leader-board__tour">
                <TourNames tournament={tournament}/>
                <TourDates tournament={tournament}/>
                <TournamentCounter tournament={tournament} mod="tour"/>
                {StatusBadge(tournament, "big")}
                <TourData tournament={tournament}/>
            </div>
            <div className="leader-board__golfers">
                <LeaderTable participants={participants}
                             onStats={this.onStats}
                             onScore={this.onScore}
                             tournament={tournament}/>
            </div>
        </div>`
