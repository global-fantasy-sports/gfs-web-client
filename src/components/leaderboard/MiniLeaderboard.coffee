{LeaderTable} = require './LeaderTable'
{BtnLink} = require 'lib/ui'
{eventTracker} = require 'lib/utils'

exports.MiniLeaderboard = React.create
    displayName: 'MiniLeaderboard'

    mixins: [flux.Connect("participants", (store, props) ->
        participants: store.getParticipants({tournamentId: props.tournament.id})
    )]

    viewLeaderboard: ->
        eventTracker('buttons', 'leaderboard-view')

    render: ({tournament}, {participants}) ->
        if participants.isEmpty()
            return `<div/>`

        if not tournament.isLive()
            return `<div/>`

        parts = participants.sortBy (p) -> p.getScore()

        `<div className="leader-board mini">
            <div className="leader-board__golfers">
                <LeaderTable participants={parts.slice(0, 3)} tournament={tournament}/>
            </div>
            <div className="leader-board__bottom">
                <BtnLink to={"/golf/leaderboard/" + tournament.id}
                         mod="green-linear x-medium"
                         onTap={this.viewLeaderboard}>
                    View more
                </BtnLink>
            </div>
        </div>`
