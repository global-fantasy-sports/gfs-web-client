{LeaderTableRow} = require './LeaderTableRow'

exports.LeaderTable = React.create
    displayName: "LeaderTable"

    render: ({participants, onScore, onStats, tournament}, S) ->
        participants = participants or []
        if not participants.size
            return
        parts = participants.sortBy (p) -> p.getScore()
        currentRound = tournament.getCurrentRoundNumber()

        rows = parts.toList().map (p, i) ->
            `<LeaderTableRow participant={p} key={p.id} idx={i}
                             currentRound={currentRound}
                             onStats={onStats}
                             onScore={onScore}/>`

        # TODO get rid of the header
        `<section className="game-tables">
            <section className="game-table header leaderhead" ref="header">
                <section className="f-8 text-orange">
                    Pos
                </section>
                <section className="f-7">
                    Country
                </section>
                <section className="f-23">
                    Golfer
                </section>
                <section className="f-17">
                    World rank
                </section>
                <section className={"f-6" + (currentRound == 1 ? " bg-red" : "")}>
                    R1
                </section>
                <section className={"f-7" + (currentRound == 2 ? " bg-red" : "")}>
                    R2
                </section>
                <section className={"f-7" + (currentRound == 3 ? " bg-red" : "")}>
                    R3
                </section>
                <section className={"f-7" + (currentRound == 4 ? " bg-red" : "")}>
                    R4
                </section>
                <section className="f-8 text-green">
                    Hole
                </section>
                <section className="f-10">
                    To-par
                </section>
            </section>
            {rows}
        </section>`
