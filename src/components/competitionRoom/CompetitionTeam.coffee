_ = require('underscore')
{Btn} = require('lib/ui')
PlaceholderImage = require('../../lib/ui/PlaceholderImage')
Overlay = require('react-overlays/lib/Overlay')
PointsToolTip = require('./PointsToolTip')
Tooltip = require '../tooltip/Tooltip'

module.exports = React.create(
    displayName: "CompetitionTeam"

    mixins: [
        flux.Connect('state', (store) ->
            tooltipId: store.getTooltipId()
        )
        flux.Connect('participants', (store, props) ->
            participants: props.game.getParticipants()
        )
    ]

    onHover: (participantId) ->
        ->
            flux.actions.state.tooltipHovered(participantId)

    onBlur: ->
        flux.actions.state.hideTooltips()

    onPlayerClick: (participantId) ->
        (e) ->
            flux.actions.state.openPlayerDetailsDialog(participantId)

    getTooltipContent: (participant) ->
        if _(participant.pointsDetails).isEmpty() 
            return `<div key="PD" className='two-column-view'>Player has not played yet</div>`

        column1 = [`<div key='ACT' className='two-column-view__caption'>ACTIVITY</div>`]
        column1.push(
            _(participant.pointsDetails).map((el) ->
                `<div key={'title_' + el.name} className='two-column-view__title-row'>
                    <span className='two-column-view__title'>{el.name}</span>
                    {!!el.rules &&
                    <span>{el.rules}</span>
                    }
                </div>`
            )...
        )

        column2 = [`<div key="FP_TITLE" className='two-column-view__caption'>FANTASY POINTS</div>`]
        column2.push(
            _(participant.pointsDetails).map((el) ->
                `<div key={'value_' + el.name} className='two-column-view__value'>{el.points}</div>`
            )...
        )

        `<div className='two-column-view' key={participant.id}>
            <div className='two-column-view__column two-column-view__column--dark'>{column1}</div>
            <div className='two-column-view__column'>{column2}</div>
        </div>`

    render: ({game}, {tooltipId, participants}) ->
        {onBlur, onHover, onPlayerClick} = this
        getTooltipContent = @getTooltipContent
        if game.isPending() and not game.isMyGame()
            return null

        items = participants.map((participant, i) ->
            if not participant
                return `<div className="competition-team__item"
                             key={i}></div>`
            contest = flux.stores.games.getContest(participant?.gameId)

            `<div className="competition-team__item" onTap={onPlayerClick(participant.id)} key={participant.id}>
                <div className="competition-team__img">
                    <PlaceholderImage
                        src={participant.smallPhoto}
                        placeholder="/static/img/nfl-player-placeholder-small.png"
                    />
                </div>
                <div className="competition-team__data">
                    <div className="competition-team__name">{participant.getName()}</div>
                    <div className="competition-team__pos">{participant.position}</div>
                    <div className="competition-team__teams">
                        <div className="competition-team__row">
                            <img src={contest.homeTeamPhoto} className="competition-team__logo"/>
                            <p>VS</p>
                            <img src={contest.awayTeamPhoto} className="competition-team__logo"/>
                        </div>
                        <div className="competition-team__row">
                            <p className="stroke">{contest.homeScore}</p>
                            <p className="stroke small">&nbsp;</p>
                            <p className="stroke">{contest.awayScore}</p>
                        </div>
                    </div>
                    <div className="competition-team__points">
                        <p className="stroke small">TOTAL POINTS</p>
                        <p className="competition-team__total">{participant.points}</p>
                    <Tooltip content={getTooltipContent(participant)} mod="two-column" viewMod={_(participant.pointsDetails).isEmpty() ? 'texted' : ''}>
                        <span className="abbr">
                            Scoring
                        </span>
                    </Tooltip>
                    </div>
                </div>
            </div>`
        ).toArray()

        `<div className="competition-team">
            {items}
        </div>`
)
