PrizesDistribution = require('./PrizesDistribution')
{Btn, Icon, Tokens, Dollars} = require('lib/ui')
{COMPETITION_ROOM_TEXTS} = require('../const')
{dateTimeFmt, cx} = require('lib/utils')
RoomStartCounter = require('../stakerooms/RoomStartCounter')
RoomStatusIcon = require('./RoomStatusIcon')

module.exports = React.create(
    displayName: 'CompetitionRoomDescription'

    renderTags: (room) ->
        tags = [
            `<Btn key={'type-' + room.id} mod="offset-sides competition-filter">{room.type}</Btn>`
        ]

        if room.isForBeginners
            tags.push(`<Btn key={'beginners-' + room.id} mod="offset-sides competition-filter">Beginner</Btn>`)

        if room.isFeatured
            tags.push(`<Btn key={'featured-' + room.id} mod="offset-sides competition-filter">Featured</Btn>`)

        if room.isGuaranteed
            tags.push(`<Btn key={'guaranteed-' + room.id} mod="offset-sides competition-filter">Guaranteed</Btn>`)

        if room.mode == 'token'
            tags.push(`<Btn key={'token-' + room.id} mod="offset-sides competition-filter">Token</Btn>`)

        if room.mode == 'usd'
            tags.push(`<Btn key={'usd-' + room.id} mod="offset-sides competition-filter">Dollars</Btn>`)

        tags

    getGameStatus: ->
        {room} = @props
        text =
            if room.isFinished() then 'FINAL'
            else if room.isLive() then 'LIVE'
            else if room.isFull() then 'FULL'
            else 'DAYS LEFT'

        `<div className="competition-head__icon-item w-13">
            <div className="competition-head__icon">
                <RoomStatusIcon room={room} />
                {!room.started && !room.isFull() ?
                    <span className="stroke lato-black x-big">{room.getDaysLeft()}</span> : null}
            </div>
            <div className="stroke text-up extra-small">{text}</div>
        </div>`

    roomDescription: ->
        room = @props.room
        texts = COMPETITION_ROOM_TEXTS[room.type] || {description: -> ''}
        gameTypeText = COMPETITION_ROOM_TEXTS.gameTypes[room.gameType]

        if room.isFinished()
            `<div className="competition-head__text">
                <div className="competition-head__finished">
                    <p>The game is finished.</p>
                    <p>Congratulations to the winners!</p>
                </div>
                <p className="competition-head__text-bottom">
                    {gameTypeText}
                </p>
            </div>`
        else if not room.isLive() and room.getDaysLeft() < 1
            `<div className="competition-head__text">
                <div className="competition-head__full">
                    <p className="competition-head__full-title">The game begins in</p>
                    <div className="competition-head__timer">
                        <RoomStartCounter room={room} subtext/>
                    </div>
                </div>
                <p className="competition-head__text-bottom">
                    {gameTypeText}
                </p>
            </div>`
        else
            `<div className="competition-head__text">
                <p className="competition-head__text-top">
                    {texts.description(room.maxEntries, room.prizeList.size)}
                </p>
                <p className="competition-head__text-middle">
                    {texts.distribution}
                </p>
                <p className="competition-head__text-bottom">
                    {gameTypeText}
                </p>
            </div>`

    render: ({room, onPlay, isMyRoom}) ->
        return null if room.isEmpty()
        tags = @renderTags(room)

        mod =
            if room.isFinished() then 'finished'
            else if room.isLive() then 'live'
            else if room.isFull() then 'full'
            else false

        roomBackgroundImageCls = cx("competition-head__left", {
            "competition-head__left--#{mod}": mod
        })


        `<div className="competition-head">
            <div className="competition-head__title">
                <p className="stroke lato-medium small opacity-50">{dateTimeFmt(room.startDate)}</p>
                <div className="competition-head__name">
                    <div className="w-75">
                        <h1 className="title white size-30">{room.fullName1()}</h1>
                        <div>{tags}</div>
                    </div>
                    <div>
                        <RoomStatusIcon large room={room} height={85} />
                    </div>
                </div>
            </div>
            <div className="competition-head__content">
                <div className={roomBackgroundImageCls}>
                    <div className="competition-head__room-name">
                        <p className="stroke lato-black small text-up w-80">{room.fullName2()}</p>
                        <div className="flex-row-stretch w-10">
                            <Icon i="cup-white"/>
                            <Icon i="injured-white"/>
                            <Icon i="star-white"/>
                        </div>
                    </div>
                    <div className="competition-head__info">
                        <div className="competition-head__icons-row">
                            <div className="competition-head__icon-item w-13">
                                <div className="competition-head__icon">
                                    <img src="/static/img/competitionRoom/icn_status_room_player.png" alt="Player"/>
                                    <span className="stroke lato-black x-big">{room.maxEntries}</span>
                                </div>
                                <div className="stroke text-up extra-small">PLAYERS</div>
                            </div>
                            <div className="competition-head__line w-30"></div>
                            <div className="competition-head__icon-item w-13">
                                <div className="competition-head__icon">
                                    <img src="/static/img/competitionRoom/icn_status_room_winner.png" alt="Winner"/>
                                    <span className="stroke lato-black x-big">{room.prizeList.size}</span>
                                </div>
                                <div className="stroke text-up extra-small">WINNERS</div>
                            </div>
                            <div className="competition-head__line w-30"></div>
                            {this.getGameStatus()}
                        </div>
                        {this.roomDescription()}
                    </div>
                </div>
                <div className="competition-head__right">
                    <PrizesDistribution room={room} isMyRoom={isMyRoom} onPlay={onPlay}/>
                </div>
            </div>
        </div>`
)
