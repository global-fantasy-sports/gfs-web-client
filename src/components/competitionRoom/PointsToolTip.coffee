
PointsToolTip = React.create
    displayName: 'PointsToolTip'

    mixins: [flux.Connect('participants', (store, props) ->
        participant: store.getParticipant(props.id)
    )]

    render: ({}, {participant}) ->
        els = for key, value of participant.pointsDetails
            `<div key={key}>{key}:{'\u00a0'}{value}</div>`
        if els.length == 0
            els = `<div>No data</div>`

        `<div className='tooltip tooltip-position-fix'>
            <div className='tooltip__inner'>
                {els}
            </div>
        </div>`

module.exports = PointsToolTip
