{Tokens, Dollars, Btn} = require('lib/ui')
bemClassName = require('bem-classname')
{cx} = require '../../lib/utils'

bem = bemClassName.bind(null, 'prizes-distribution')

module.exports = React.create(
    displayName: 'PrizesDistribution'

    mixins: [
        flux.Connect('games', (store, props) ->
            prizeGames: props.room.getGames().filter((g) -> g.prizeAmount > 0)
        )
    ]

    onPlay: ->
        @props.onPlay(@props.room)

    render: ({room, isMyRoom}, {prizeGames}) ->
        tableCls = if isMyRoom then 'table-full' else ''

        disabled = room.started || room.finished || room.isFull()
        playButtonMod = cx('big', {
            disabled,
            red: !disabled
            gray: disabled
        })

        Money = if room.mode == 'usd'
            Dollars
        else
            Tokens

        if room.finished
            prizeInfo = prizeGames.sortBy((game) -> game.place).map((game, i) ->
                i += 1
                `<tr key={'row -' + i}>
                  <td className="prizes-distribution__cell-left">
                      {game.placeStr()} {game.userData.name}
                  </td>
                  <td className="prizes-distribution__cell-right">
                    <span className="prizes-distribution__money"><Money m={game.prizeAmount} mod="green"/></span>
                  </td>
                </tr>`
            ).toArray()
        else
            prizeInfo = room.prizeList.toIndexedSeq().map((prize, i) ->
                i += 1
                `<tr key={'row -' + i}>
                    <td className="prizes-distribution__cell-left">{i} place</td>
                    <td className="prizes-distribution__cell-right">
                        <span className="prizes-distribution__money"><Money m={prize} mod="green"/></span>
                    </td>
                </tr>`
            ).toArray()

        `<div className={bem([tableCls])}>
            <div className="prizes-distribution__entries-wrap">
                <div className="prizes-distribution__entry">
                    <p className="prizes-distribution__entry-title">ENTRIES</p>
                    <p>
                        <span className="prizes-distribution__games-count">{room.gamesCount}</span>
                        <span className="prizes-distribution__max"> / {room.maxEntries}</span>
                    </p>
                </div>
                <div className="prizes-distribution__entry">
                    <p className="prizes-distribution__entry-title">ENTRY SIZE</p>
                    <Dollars m={room.entryFee}/>
                </div>
            </div>
            <div className={bem("table-wrap", [tableCls])}>
                <table className="prizes-distribution__table">
                    <thead className="prizes-distribution__head">
                    <tr>
                        <th colSpan="3">Prizes distribution</th>
                    </tr>
                    </thead>
                    <tbody className="prizes-distribution__content">
                        {prizeInfo}
                    </tbody>
                </table>
            </div>
            {!isMyRoom &&
            <div className="prizes-distribution__btn">
                <Btn mod={playButtonMod} onTap={this.onPlay}>Play</Btn>
            </div>
            }
        </div>`
)
