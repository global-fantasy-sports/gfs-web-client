{cx} = require('lib/utils')
CompetitionRoomDescription = require('./CompetitionRoomDescription')
CompetitionItem = require('./CompetitionItem')
CompetitionTeam = require('./CompetitionTeam')
PredictionCardFiveBall = require('../golf/PredictionCardFiveBall')


EmptyItem = ({index}) ->
    `<div className="competition-item-empty">
        <div className="competition-item-empty__place">
            <p className="competition-item-empty__game">GAME</p>
            <p className="competition-item-empty__num">{index}</p>
        </div>
    </div>`


CommonCompetitionRoom = React.create(
    displayName: "CommonCompetitionRoom"

    mixins: [flux.Connect('rooms', (store, props) ->
        room: store.getRoom(+props.roomId)
    ), flux.Connect('games', (store, props) ->
        games: store.getGames({competitionRoomId: +props.roomId}).toList()
        isMyRoom: store.getMyGames().find((g) -> g.competitionRoomId == +props.roomId)?
    )]

    getInitialState: ->
        expanded: null

    onExpand: (id) ->
        @setState(expanded: if @state.expanded is id then null else id)

    fetchGames: (room) ->
        if room.id
            room.fetchGames({force: true})

    componentWillMount: ->
        @fetchGames(@state.room)

    componentWillUpdate: (nextProps, nextState) ->
        if @state.room.id != nextState.room.id
            @fetchGames(nextState.room)

    render: ({onPlay}, {room, games, expanded, isMyRoom}) ->
        els = games.setSize(room.maxEntries).toArray().map((game, index) =>
            if game
                onExpand = @onExpand
                `<div className="competition-room__item" key={game.id}>
                    <CompetitionItem
                        room={room}
                        game={game}
                        onExpand={onExpand}
                    />
                    {['nfl', 'nba'].includes(SPORT) && expanded === game.id && <CompetitionTeam game={game} />}
                    {SPORT == 'golf' ? <PredictionCardFiveBall /> : null}
                </div>`
            else
                `<EmptyItem key={index} index={index + 1} />`
        )

        `<section className="competition-room">
            <div className="competition-room__head">
                <div className="competition-room__row">
                    <CompetitionRoomDescription
                        room={room}
                        isMyRoom={isMyRoom}
                        onPlay={onPlay}
                    />
                </div>
            </div>
            <div className="competition-room__content">
                <div className="competition-room__region">
                    {els}
                </div>
            </div>
        </section>`
)

module.exports = CommonCompetitionRoom
