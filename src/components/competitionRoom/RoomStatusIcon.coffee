_ = require('underscore')
{capitalize} = require('../../lib/utils')

SMALL_ICONS = {
    'finished': '/static/img/competitionRoom/icn_status_room_min_finish.png'
    'full': '/static/img/competitionRoom/icn_status_room_min_full.png'
    'not started': '/static/img/competitionRoom/icn_status_room_min_time.png'
    'live': '/static/img/competitionRoom/icn_status_room_min_live.png'
}

LARGE_ICONS = {
    'finished': '/static/img/competitionRoom/icn_status_room_finish.png'
    'full': '/static/img/competitionRoom/icn_status_room_full.png'
    'not started': '/static/img/competitionRoom/icn_status_room_not-started.png'
    'live': '/static/img/competitionRoom/icn_status_room_live.png'
}

getStatus = (room) ->
    if room.isFinished() then 'final'
    else if room.isLive() then 'live'
    else if room.isFull() then 'full'
    else 'not started'


RoomStatusIcon = (props) ->
    {room, large} = props
    icons = if large then LARGE_ICONS else SMALL_ICONS
    status = getStatus(room)
    extraProps = _.omit(props, 'room', 'large')
    `<img
        {...extraProps}
        src={icons[status]}
        alt={capitalize(status)}
    />`

module.exports = RoomStatusIcon
