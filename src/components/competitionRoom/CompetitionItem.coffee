{Btn} = require('lib/ui')
bemClassName = require('bem-classname')
{Dollars, Tokens} = require('lib/ui')

CompetitionPlayers = require('./CompetitionPlayers')

competitionItem = bemClassName.bind(null, 'competition-item')

module.exports = React.create(
    displayName: "CompetitionItem"

    contextTypes:
        router: React.PropTypes.object

    getInitialState: ->
        open: false

    onClone: ->
        flux.actions.roster.cloneGame(@props.game)
        @context.router.transitionTo("/#{SPORT}/salarycap/select/")

    onExpand: ->
        @props.onExpand(@props.game.id)

    onRemove: ->
        flux.actions.tap.openRemoveGameDialog( () =>
            @props.game.remove()
        )

    onEditGame: ->
        flux.actions.tap.editGame(@props.game)

    toggleControlButtons: ->
        @setState(open: not @state.open)

    render: ({game, room}, {open}) ->
        {userData} = game
        onClone = @onClone
        onRemove = @onRemove
        onExpand = @onExpand
        onEditGame = @onEditGame

        isDisabled = if not room.isPending() then "disabled" else ""
        isMyGame = userData.id == flux.stores.users.getCurrentUser().get('id')
        isAllowedToEdit = isMyGame and game.isPending()
        isNFLAndAllowedToEdit = SPORT == 'nfl' and isAllowedToEdit

        if game.prizeAmount
            Money = if game.prizeCurrency == 'usd' then Dollars else Tokens
            prize = `<div className={competitionItem('cell-points')}>
              <p className={competitionItem('title')}>PRIZE</p>
              <p className={competitionItem('prize')}>
                 <Money m={game.prizeAmount} />
              </p>
            </div>`
        else
            prize = `<div className={competitionItem('cell-points')} />`

        placeRangeSmall = if (game.placeRange or '').length > 2 then "small"

        canExpand = isMyGame or game.isLive() or game.isFinished()

        expandBtn = `<div className={competitionItem('expand')} onTap={this.toggleControlButtons}>
            <i className={competitionItem('expand-dot')} />
            <i className={competitionItem('expand-dot')} />
            <i className={competitionItem('expand-dot')} />
        </div>`

        `<section className={competitionItem({my: isMyGame})}>
            <div className={competitionItem('cell-place')}>
                <p className={competitionItem('title')}>PLACE</p>
                <p className={competitionItem('range', [placeRangeSmall])}>{game.placeRange}</p>
            </div>
            <div className={competitionItem('cell')}>
                <img
                    src={userData.avatar}
                    alt={userData.name}
                    width="100"
                    height="100"
                />
            </div>
            <div className={competitionItem('name')}>
                {userData.name}
            </div>

            {prize}

            <div className={competitionItem('cell-points')}>
                <p className={competitionItem('title')}>POINTS</p>
                <p className={competitionItem('points')}>{game.points}</p>
            </div>
            <div className={competitionItem('players')}>
                <CompetitionPlayers game={game}/>
                {isMyGame ? expandBtn : null}
            </div>
            <div className={competitionItem('buttons', [open ? 'open' : ''])}>
                {isNFLAndAllowedToEdit && open ?
                    <Btn mod={"square"}
                         iconBg={true}
                         icon="clone"
                         onTap={onClone}>
                        Clone
                    </Btn>
                    : null
                }
                {isAllowedToEdit && open ?
                    <Btn mod={"square" + isDisabled}
                         iconBg={true}
                         icon="pencil"
                         onTap={!isDisabled ? onEditGame : null}>
                    Edit
                  </Btn>
                  : null}
                <Btn onTap={onExpand}
                     mod={canExpand ? "square" : "square disabled"}
                     iconBg={true}
                     icon="expand">
                    Expand
                </Btn>
                {isAllowedToEdit && open ?
                    <Btn mod={"square text-red" + isDisabled}
                         iconBg={true}
                         icon="trash"
                         onTap={!isDisabled ? onRemove : null}>
                        Delete
                    </Btn>
                    : null
                }
            </div>
        </section>`
)
