PlaceholderImage = require('../../lib/ui/PlaceholderImage')
{cx} = require('lib/utils')

{Icon} = require('../../lib/ui')


module.exports = React.create(
    displayName: "CompetitionPlayers"

    mixins: [
        flux.Connect('participants', (store, props) ->
            participants: props.game.getParticipants()
        )
    ]

    render: ({game, overlay}, {participants}) ->
        if game.isPending() and not game.isMyGame() and SPORT == 'nfl'
            photos = `<img className="competition-players__img-empty"
                           src="/static/img/empty-nfl-lineup.png" />`
        else
            photos = participants.map((participant, index) ->
                imgCls = "competition-players__img competition-players__img-" + "#{index}"
                photo = participant?.smallPhoto or ''
                name =
                    if SPORT is 'nfl' or SPORT is 'nba'
                        participant and participant.nameFull or ''
                    else
                        participant and participant.getFullName() or ''

                `<div className={imgCls} key={index}>
                    <PlaceholderImage
                        src={photo}
                        placeholder="/static/img/nfl-player-placeholder-small.png"
                        name={name}
                    />
                </div>`
            ).toArray()

        names = participants.map((participant, index) ->
            position = participant?.position or ''
            points = participant?.points or 0
            `<div className="competition-players__pos" key={index}>
                {game.isPending() ? position : points}
            </div>`
        ).toArray()

        imgWrapCls = cx(
            "competition-players__img-wrapper": true
            "competition-players__img-wrapper--five": participants.size <= 5
            "competition-players__img-wrapper--three": participants.size <= 3
            "competition-players__img-wrapper--empty": !game.isMyGame()
        )

        `<div className="competition-players">
            <div className="competition-players__top">
                <div className={imgWrapCls}>
                    {photos}
                </div>
            </div>
            <div className="competition-players__bottom">
                <div className="competition-players__pos-wrapper">{names}</div>
            </div>

            {overlay && <div className="competition-players__overlay">
                <span className="competition-players__overlay-text">
                    You&nbsp;&nbsp;<Icon i="cup-white-big" />&nbsp;&nbsp;won!
                </span>
            </div>}
        </div>`
)
