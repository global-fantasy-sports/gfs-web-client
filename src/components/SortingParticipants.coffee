{cx, bem} = require('lib/utils')
{Icon} = require('lib/ui')


module.exports = React.create
    displayName: "SortingParticipants"

    onSort: (sorting) ->
        (e) ->
            e.preventDefault()
            flux.actions.roster.sort(sorting)

    render: ({clickable, sorting, reversed}) ->
        className = bem('sorting-participant__table-item')
        onSort = @onSort

        if clickable
            opt = {
                position: [(if sorting == 'position' then className('active') + ' w-10' else className() + ' w-10'), 'Pos']
                nameFull: [(if sorting == 'nameFull' then className('active') + ' w-30' else className() + ' w-30'), 'Name']
                opponent: [(if sorting == 'opponent' then className('active') + ' w-15' else className() + ' w-15'), 'Opponent']
                rank: [(if sorting == 'rank' then className('active') + ' w-10' else className() + ' w-10'), 'OPRK']
                FPPG: [(if sorting == 'FPPG' then className('active') + ' w-10' else className() + ' w-10'), 'FPPG']
                salary: [(if sorting == 'salary' then className('active') + ' w-15' else className() + ' w-15'), 'Salary']
            }

            els = for key, value of opt
                `<div onTap={onSort(key)} className={value[0]} key={key}>
                    {value[1]}
                    {sorting == key &&
                        <span>
                            &nbsp;<Icon i='arrow-sort' />
                        </span>
                    }
                </div>`

            `<div className="sorting-participant">
                <div className="sorting-participant__table-head">
                    <div className={className() + " w-3"}></div>
                    {els}
                    <div className={className() + " w-10"} key="last"></div>
                </div>
            </div>`
        else
            `<div className="sorting-participant">
                <div className="sorting-participant__table-head">
                    <div className={className('with-photo') + " w-10"}>Pos</div>
                    <div className={className('with-photo', 'name') + " w-30"}>Name</div>
                    <div className={className('with-photo') + " w-15"}>Opponent</div>
                    <div className={className('with-photo') + " w-10"}>OPRK</div>
                    <div className={className('with-photo') + " w-10"}>FPPG</div>
                    <div className={className('with-photo') + " w-15"}>Salary</div>
                    <div className={className('with-photo') + " w-10"}></div>
                </div>
            </div>`
