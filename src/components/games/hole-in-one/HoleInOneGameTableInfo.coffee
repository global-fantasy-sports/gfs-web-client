strftime = require 'strftime'
{StatusBadge, RoundBadge} = require '../../../lib/ui'
CompetitionPlace = require '../CompetitionPlace'
CardPoints = require '../CardPoints'


module.exports = React.create
    displayName: "HoleInOneGameTableInfo"

    mixins: [
        flux.Connect('games', (store, props) => {game: store.getGame(props.gameId)})
        flux.Connect('rooms', (store, props) => {room: store.getRoom(props.roomId)})
    ]

    render: ({}, {game, room}) ->
        place = game.placeRange or ''
        points = game.points or 0

        `<div className="game-settings">
            <div className="game-settings__l wide">
                <p className="game-settings__badge">
                    {RoundBadge(room)}
                    {StatusBadge(room)}
                </p>
                <p className="game-settings__date">
                    {strftime("%B %d, %a", room.startDate)}
                </p>
            </div>
            <div className="game-settings__r wide">
                {!room.isPending() ?
                    <div>
                        <CompetitionPlace position={place}/>
                        <CardPoints points={points}/>
                    </div>
                : null }
            </div>
        </div>`
