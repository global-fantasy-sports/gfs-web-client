{Btn} = require "../../../lib/ui"
StakeRoomProfile = require '../../stakerooms/StakeRoomProfile'
ItemWrapper = require '../../stakerooms/ItemWrapper'
HoleInOneRoomParticipant = require "./HoleInOneRoomParticipant"
UserPoints = require "../UserPoints"
ActiveRoundSwitcher = require "components/ActiveRoundSwitcher"

ReactCSSTransitionGroup = require('react-addons-css-transition-group')

module.exports = React.create
    displayName: "HoleInOneStakeRoomGame"

    mixins: [
        flux.Connect("participants", (store, props) ->
            participant1: store.getParticipant(props.game.participantId1)
            participant2: store.getParticipant(props.game.participantId2)
            participant3: store.getParticipant(props.game.participantId3)
        )
        flux.Connect("state", (store, props) ->
            gameId = store.getExpandedScorecardGameId()
            participantId = store.getExpandedScorecardParticipantId()
            if participantId == props.game.participantId1
                expandedBonus = props.game.handicapBonus1

            if participantId == props.game.participantId2
                expandedBonus = props.game.handicapBonus2

            if participantId == props.game.participantId3
                expandedBonus = props.game.handicapBonus3

            isExpanded: gameId == props.game.id
            expandedParticipant: flux.stores.participants.getParticipant(participantId)
            expandedBonus: expandedBonus
        )
    ]

    closeScorecard: ->
        flux.actions.tap.closeScorecard()

    openScorecard: ->
        flux.actions.tap.gameParticipant(
            @props.game.id,
            @props.game.participantId1
        )

    render: ({game, menu, label, idx, children, viewGame, gameProperties},
        {participant1, participant2, participant3, expandedParticipant,
        expandedBonus, isExpanded}
    ) ->

        {LiveView} = gameProperties

        row = `<ItemWrapper {...this.props} game={game} active={viewGame} mod={label ? "dark label" : "dark"}>
            <section className="f-20">
                <StakeRoomProfile user={game.userData} idx={idx} label={label} mod="handicap-the-pros"/>
            </section>
            <section className="f-20">
                <HoleInOneRoomParticipant
                    participant={participant1}
                    game={game} />
            </section>
            <section className="f-20">
                <HoleInOneRoomParticipant
                    participant={participant2}
                    game={game} />
            </section>
            <section className="f-20">
                <HoleInOneRoomParticipant
                    participant={participant3}
                    game={game} />
            </section>
            <section className = "f-12">
                <UserPoints game={game} viewGame={viewGame}/>
            </section>
            <section className="f-12">
                <div className="game-settings row">
                    {isExpanded ?
                        <Btn mod ="white square regular small"
                            iconBg={true}
                            icon="close-red"
                            onTap={this.closeScorecard}>
                            Close
                        </Btn>
                        :
                        <Btn mod="blue square regular small"
                            iconBg={true}
                            icon="view"
                            onTap={this.openScorecard}>
                            Score card
                        </Btn>
                    }
                </div>
            </section>
        </ItemWrapper>`

        userName = game.userData.name
        participant = expandedParticipant.getFullName()

        liveEl = `<section>
            <div className="rounds-score-card">
                <div className="stroke white x2-medium">
                        {userName} score card for
                    <span className="stroke white x2-medium text-up"> {participant}</span>
                </div>
                <ActiveRoundSwitcher roundNumber={game.roundNumber} />
            </div>

            <LiveView game={game}
                participant={expandedParticipant}
                bonus={expandedBonus}
                gameProperties={gameProperties}/>
        </section>`

        `<div>
            {row}
            <ReactCSSTransitionGroup transitionName="slide-down"
                                     transitionEnterTimeout={1000}
                                     transitionLeaveTimeout={1000}>
                {isExpanded ? liveEl : null}
            </ReactCSSTransitionGroup>
        </div>`
