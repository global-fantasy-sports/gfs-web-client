{sum} = require "lib/utils.coffee"
{strokeType} = require "lib/const.coffee"

{TournamentResults} = require "../../tournament/TournamentResults.coffee"
{TournamentMember} = require "../../tournament/TournamentMember.coffee"
{Helpers} = require "lib/Helpers.coffee"


module.exports = React.create
    displayName: "HoleInOneResults"

    mixins: [flux.Connect("participantRoundResults", (store, props) ->
        participantRoundResult: store.getResult({
            participantId: props.participant.id
            roundNumber: props.roundNumber
        })
    )]

    render: ({game, roundNumber, bonus}, {participantRoundResult}) ->
        pars = participantRoundResult.pars
        strokes = participantRoundResult.strokes

        strokeClass = (i) ->
            return "" if not bonus[i]
            "handicap"

        pointClass = (i) ->
            return "" if not strokes[i]
            strokeType(strokes[i] - pars[i] - bonus[i])

        points = Helpers.calculateRowPoints(strokes, pars, bonus)

        `<TournamentResults {...this.props}>
              <TournamentMember
                  game={game}
                  participantRoundResult={participantRoundResult}
                  roundNumber={roundNumber}
                  pointClass={pointClass}
                  strokeClass={strokeClass}
                  points={points}
                  bonus={bonus}/>
        </TournamentResults>`
