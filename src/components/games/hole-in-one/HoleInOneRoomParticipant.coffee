{Flag} = require "lib/ui/Flag"
_ = require "underscore"
strftime = require "strftime"
{cx} = require 'lib/utils'


module.exports = React.create
    displayName: "HoleInOneRoomParticipant"

    mixins: [flux.Connect("state", (store, props) ->
        isActive: store.getExpandedScorecardGameId() == props.game.id \
            and store.getExpandedScorecardParticipantId() == props.participant.id
    )]

    onTap: ->
        flux.actions.tap.gameParticipant(
            @props.game.id,
            @props.participant.id
        )

    render: ({participant}, {isActive}) ->
        lastScore = participant.lastScore
        name = participant.getFullName()
        rank = participant.rank
        handicap = participant.handicap

        cls = cx(
            "profile two-golfers": true
            "active": isActive
        )

        `<div className={cls} onTap={this.onTap}>
            <div className="profile__avatar">
                <div className="avatar">
                    <Flag country={participant.country}/>
                </div>
            </div>
            <div className="profile__person-info">
                <div className="person-info">
                    <div className="person-info__name">
                        {name}
                    </div>
                    <div className="address">
                        <p className="stroke small">
                            <span>World Rank: </span>
                            <span>#{rank}</span>
                        </p>
                        <p className="stroke small">
                            <span>Handicap: </span>
                            <span>{handicap}</span>
                        </p>
                    </div>
                </div>
            </div>
         </div>`
