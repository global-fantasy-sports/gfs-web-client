module.exports = React.create
    displayName: "CardPoints"

    render: ({points}) ->
        `<div className="points-counter">
            <span className="points-counter__amount">{points} </span>
            <span>points</span>
        </div>`
