{Btn} = require('lib/ui')
{cx} = require('../../lib/utils')
Profile = require './Profile'
{TourNames} = require('../tournament/TourName')
{TourDates} = require('../tournament/TourDates')
{TourCourseRating} = require('../tournament/TourCourseRating')
GuideHint = require('../../components/guide/GuideHint')

module.exports = React.create
    displayName: 'PredictionHead'
    mixins: [flux.Connect("wizard", (store) ->
        participants: store.getSelectedParticipantIds().map((id) ->
            flux.stores.participants.getParticipant(id)
        )
        tournament: flux.stores.tournaments.getTournament(store.getTournamentId())
    )]

    render: ({fillRandom, toggleStats, showStats, handicap, activeIndex}, {participants, tournament}) ->
        participants = participants.toArray()
        participant1 = participants[0]
        participant2 = participants[1]
        participant3 = participants[2]

        cls = cx("golfer-with-tour__el f-22 r-border")

        `<header className="golfer-with-tour">
            <div className="golfer-with-tour__top">
                <div className="f-50">Tournament</div>
                <div className="f-40">Golfers</div>
                {!handicap ?
                <div className="f-10 centered">Statistics</div>
                    : null}
            </div>
            <div className="golfer-with-tour__content">
                <div className="golfer-with-tour__el f-25">
                    <div className="">
                        <TourNames tournament={tournament} mod="bold"/>
                        <TourDates tournament={tournament} mod="bold"/>
                    </div>
                </div>
                <div className={cls + (activeIndex == 1 ? " active" : "")}>
                    <Profile participant={participant1} />
                </div>
                <div className={cls + (activeIndex == 2 ? " active" : "")}>
                    <Profile participant={participant2}/>
                </div>
                <GuideHint key={activeIndex}
                           children="Fill in prediction card for the second golfer"
                           title="Next prediction card"
                           pointer="bottom"
                           group="prediction"
                           id="next-predict"
                           right="150px"
                           bottom="-170px"
                           pointer="top"/>
                <div className={cls + (activeIndex == 3 ? " active" : "")}>
                    <Profile participant={participant3}/>
                </div>
                <div className="golfer-with-tour__el f-9">
                    {!showStats ?
                    <Btn onTap={toggleStats}
                         iconBg={true} icon="stats"
                         mod="blue square regular small">
                        Statistics
                    </Btn>
                        :
                    <Btn onTap={toggleStats}
                         iconBg={true} icon="close-red"
                         mod="white square regular small">
                        Close
                    </Btn>
                        }
                </div>
            </div>
        </header>`
