{sum, cx} = require "lib/utils"
{Icon, Btn} = require "lib/ui"
{strokeType} = require "lib/const"
HintTrigger = require "components/guide/HintTrigger"

out = (prediction, target, result) ->
    unless result?
        if prediction == target
            logger.log prediction, "prediction == target", "TableCellOut"
            return `<span className="prediction-res filled">
                        <Icon i="cross-white"/>
                    </span>`
        return ''

    if target == prediction == result
        logger.log prediction, "prediction == target == result", "TableCellOut"
        return `<span className="prediction-res correct-yours">
                    <span className="prediction-res__el">
                        <Icon i="cross-white"/>
                    </span>
                </span>`

    if target == prediction != result
        logger.log prediction, "prediction == target != result", "TableCellOut"

        return `<span className="prediction-res wrong">
                    <span className="prediction-res__el">
                        <Icon i="cross-white"/>
                    </span>
                </span>`

    if target == result != prediction
        logger.log prediction, "target == result != prediction", "TableCellOut"
        return `<span className="prediction-res correct">
                    <span className="prediction-res__el"></span>
                </span>`

    logger.log [prediction, target, result], "No result", "TableCellOut"
    return ''


resultCompare = (diff) ->
    if diff < 0
        return -1
    if diff == 0
        return 0
    return 1


module.exports = React.create
    displayName: "Table"

    mixins: [flux.Connect("participantRoundResults", (store, props) ->
        result = store.getResult({
            "participantId": props.participant.id,
            "roundNumber": props.roundNumber
        })

        {
            pars: result.pars
            yardages: result.yardages
            strokes: result.strokes
        }
    )]

    componentDidUpdate: (prevProps, prevState) ->
        if prevProps?.highlight == null and @props.highlight != null
            @scrollToRow(@props.highlight)

    scrollToRow: (rowNumber) ->
        node = @refs[rowNumber].getDOMNode()
        offset = fullOffset(node)
        y = offset.top - (window.innerHeight - node.offsetHeight) / 2
        window.scrollTo(0, y)

    select: (target, i) ->
        return unless @props.editable
        (e) =>
            prediction = @props.prediction.slice()
            if prediction[i] == target
                prediction[i] = undefined
            else
                prediction[i] = target

            flux.actions.wizard.setCurrentPrediction(prediction)

    getTd: (prediction, target, result, i, animate) ->
        if animate != undefined and i == 0 and animate == target
            `<td className="predict__anim">
                <Icon i="up-hand"/>
            </td>`
        else
            `<td className="predict__target">
                <div>
                    <HintTrigger group="prediction" id="predict">
                        <Btn onTap={this.select(target, i)}>
                            {out(prediction, target, result)}
                        </Btn>
                    </HintTrigger>
                </div>
            </td>`

    disabledTd: ->
        `<td className="predict__disabled"></td>`

    render: ({prediction, highlight, animate, editable}, {strokes, pars, yardages}) ->
        hasStrokes = sum(strokes)

        cls = cx(
            "predict__body": true
            "disabled": not editable?
        )

        els = prediction.map (pred, i) =>
            if strokes[i]
                current = resultCompare(strokes[i] - pars[i])
                type = strokeType(strokes[i] - pars[i])
            else
                current = undefined
                type = ""

            egg = (if i == 17 then => @props.egg())

            `<tr key={i} ref={i} className={highlight == i ? "highlight" : ""}>
                <td onTap={egg}><div>{i + 1}</div></td>
                <td><div>{yardages[i]}</div></td>
                <td><div>{pars[i]}</div></td>

                {hasStrokes ?
                    <td className={"tr-gray-light " + type}>
                        <div>{strokes[i] || ""}</div>
                    </td>
                : null}

                {_this.getTd(pred, -1, current, i, animate)}
                {_this.getTd(pred, 0, current, i, animate)}
                {_this.getTd(pred, 1, current, i, animate)}
            </tr>`

        `<table className="predict">
            <thead className="predict__head" ref="header">
                <tr>
                    <th>hole</th>
                    <th>yards</th>
                    <th>par</th>
                    {hasStrokes ? <th>Score</th> : null}
                    <th>under par</th>
                    <th>par</th>
                    <th>over par</th>
                </tr>
            </thead>
            <tbody className={cls}>
                {els}
            </tbody>
        </table>`
