_ = require 'underscore'
{numFmt, Lock, Money} = require 'lib/ui'
{Flag} = require 'lib/ui/Flag'
{numberEnding} = require 'lib/utils'
{DAYS, MONTHS} = require 'lib/const'

Table = require "components/games/pick18/Table"


GameTable = React.create
    displayName: 'GameTable'

    getInitialState: ->
        locked: true

    startFill: ->
        @setState(locked: false)

    save: ->
        @setState(locked: true)
        @props.onSave()

    render: ({game, editable, participant, prediction, roundNumber}, {locked}) ->

        content = `<div className="card-table">
                        <Table
                            game={game}
                            roundNumber={roundNumber}
                            participant={participant}
                            prediction={prediction}
                            editable={editable} />
                   </div>`

        if editable
            `<div className="card-table">
                <Lock locked={locked} onFill={this.startFill}>
                    {content}
                 </Lock>
            </div>`
        else
            content


module.exports = GameTable
