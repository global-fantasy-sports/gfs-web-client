{Lock} = require 'lib/ui'
{choice} = require 'lib/utils'
{DAYS} = require 'lib/const'

CandidateStats = require '../../CandidateStats'
PredictionHead = require '../PredictionHead'
GuideHint = require '../../guide/GuideHint'

PredictionTable = require "components/games/pick18/Table"

Pick18Prediction = React.create
    displayName: 'Pick18Prediction'

    mixins: [flux.Connect("wizard", (store) ->
        isPredictionLocked: store.isPredictionLocked()
        prediction: store.getCurrentPrediction()
        participant: store.getCurrentParticipant()
        roundNumber: store.getRoundNumber()
    )]

    getInitialState: -> {
        showStats: false
    }

    componentDidMount: ->
        dom = @refs.prediction.getDOMNode()
        dom.scrollIntoView()

    startFill: ->
        flux.actions.wizard.unlockPrediction()

    allPredicted: ->
        @state.prediction.filter((x) -> x != undefined).length == 18

    fillRandom: ->
        prediction = @state.prediction.map(-> choice([-1, 0, 1]))
        flux.actions.wizard.setCurrentPrediction(prediction)

    toggleStats: ->
        @setState(showStats: not @state.showStats)


    render: ({index}, {showStats, isPredictionLocked, prediction, participant, roundNumber}) ->

        `<section className="prediction" ref="prediction">
            <PredictionHead
                activeIndex={index}
                toggleStats={this.toggleStats}
                showStats={showStats}/>
            <GuideHint children="Click on the cells to make your prediction"
                       title="Click on the cells"
                       pointer="bottom"
                       group="prediction"
                       id="predict"
                       top="0"
                       right="150px"/>
            {showStats &&
            <section className="frog static">
                <CandidateStats participant={participant}/>
            </section>
            }
            <Lock locked={isPredictionLocked}
                  onFill={this.startFill}>
                <PredictionTable ref="table"
                       egg={this.fillRandom}
                       editable={true}
                       roundNumber={roundNumber}
                       prediction={prediction}
                       participant={participant}/>
            </Lock>
        </section>`


module.exports = Pick18Prediction
