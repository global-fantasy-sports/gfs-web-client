strftime = require "strftime"
{RoundBadge, StatusBadge} = require "lib/ui"
CompetitionPlace = require "../CompetitionPlace"
Pick18GameStats = require "./Pick18GameStats"

module.exports = React.create
    displayName: "Pick18GameTableInfo"

    mixins: [
        flux.Connect('games', (store, props) => {game: store.getGame(props.gameId)})
        flux.Connect('rooms', (store, props) => {room: store.getRoom(props.roomId)})
    ]

    render: ({gameId}, {game, room}) ->
        place = game.placeRange or 0

        `<div className="game-settings">
            <div className="game-settings__l wide">
                <p className="game-settings__badge">
                    {RoundBadge(room)}
                    {StatusBadge(room)}
                </p>
                <p className="game-settings__date">
                    {strftime("%B %d, %a", room.startDate)}
                </p>
            </div>
            <div className="game-settings__r wide">
                {!room.isPending() ?
                    <div>
                        <CompetitionPlace position={place}/>
                        <Pick18GameStats gameId={gameId} />
                    </div>
                : null }
            </div>
        </div>`
