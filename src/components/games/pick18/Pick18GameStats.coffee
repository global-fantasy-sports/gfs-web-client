
module.exports = React.create
    displayName: "Pick18GameStats"

    mixins: [flux.Connect("games", (store, props) ->
        stats: store.getGame(props.gameId)?.getStats()
    )]

    render: ({}, {stats}) ->
        `<div className="par-counter">
            <span className="wins">{stats.correct}</span>
            <b>{" | "}</b>
            <span className="defeats">{stats.wrong}</span>
            <b>{" | "}</b>
            <span>{"108"}</span>
         </div>`
