{Flag} = require "lib/ui/Flag"
{cx} = require 'lib/utils'


module.exports = React.create
    displayName: "Pick18RoomParticipant"

    mixins: [flux.Connect("state", (store, props) ->
        isActive: store.getExpandedScorecardGameId() == props.game.get("id") \
            and store.getExpandedScorecardParticipantId() == props.participant.id
    )]

    onTap: ->
        flux.actions.tap.gameParticipant(
            @props.game.id,
            @props.participant.id
        )

    render: ({participant}, S) ->
        lastScore = participant.lastScore
        name = participant.getFullName()
        rank = participant.rank
        country = participant.country

        cls = cx(
            "profile two-golfers": true
            "active": @state.isActive
        )

        `<div className={cls} onTap={this.onTap}>
            <div className="profile__avatar">
                <div className="avatar">
                    <Flag country={country}/>
                </div>
            </div>
            <div className="profile__person-info">
                <div className="person-info">
                    <div className="person-info__name">
                        {name}
                    </div>
                    <div className="address">
                        <p className="stroke small">
                            <span>World Rank: </span>
                            <span>{rank}</span>
                        </p>
                    </div>
                </div>
            </div>
         </div>`
