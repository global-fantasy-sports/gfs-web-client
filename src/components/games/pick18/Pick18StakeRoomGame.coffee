StakeRoomProfile = require '../../stakerooms/StakeRoomProfile'
ItemWrapper = require '../../stakerooms/ItemWrapper'
{Btn} = require "lib/ui"

Pick18RoomParticipant = require "./Pick18RoomParticipant"
Pick18GameStats = require "./Pick18GameStats"
ReactCSSTransitionGroup = require('react-addons-css-transition-group')
ActiveRoundSwitcher = require "components/ActiveRoundSwitcher"


module.exports = React.create
    displayName: "Pick18StakeRoomGame"

    mixins: [
        flux.Connect("participants", (store, props) ->
            participant1: store.getParticipant(props.game.participantId1)
            participant2: store.getParticipant(props.game.participantId2)
            participant3: store.getParticipant(props.game.participantId3)
        )
        flux.Connect("state", (store, props) ->
            gameId = store.getExpandedScorecardGameId()
            participantId = store.getExpandedScorecardParticipantId()
            if participantId == props.game.participantId1
                expandedPrediction = props.game.prediction1

            if participantId == props.game.participantId2
                expandedPrediction = props.game.prediction2

            if participantId == props.game.participantId3
                expandedPrediction = props.game.prediction3

            isExpanded: gameId == props.game.id
            expandedParticipant: flux.stores.participants.getParticipant(participantId)
            expandedPrediction: expandedPrediction
            activeRoundNumber: store.getActiveRoundNumber()
        )
    ]

    getInitialState: ->
        openProfileNumber: 0

    componentWillReceiveProps: (nextProps) ->
        if nextProps.viewGame != @.props.viewGame
            @forceUpdate()

    closeResult: ->
        flux.actions.tap.closeScorecard()

    onView: ->
        flux.actions.tap.gameParticipant(@props.game.id, @props.game.participantId1)

    render: (
        {game, label, idx, onView, children, viewGame, closeResult, gameProperties},
        {participant1, participant2, participant3,
        openProfileNumber, isExpanded,
        expandedParticipant, expandedPrediction, activeRoundNumber}
    ) ->

        {LiveView} = gameProperties

        row = `<ItemWrapper {...this.props} game={game} mod={label ? "dark label" : "dark"}>
            <section className="f-24">
                <StakeRoomProfile idx={idx} label={label}
                                  user={game.userData} />
            </section>
            <section className="f-20">
                    <Pick18RoomParticipant
                        game={game}
                        participant={participant1} />
            </section>
            <section className="f-20">
                    <Pick18RoomParticipant
                        game={game}
                        participant={participant2} />
            </section>
            <section className="f-20">
                    <Pick18RoomParticipant
                        game={game}
                        participant={participant3} />
            </section>

            <section className="f-20">
                <div className="game-settings row">
                    <Pick18GameStats gameId={game.id} viewGame={viewGame}/>
                    {isExpanded ?
                        <Btn mod ="white square regular small"
                            iconBg={true}
                            icon="close-red"
                            onTap={this.closeResult}>
                            Close
                        </Btn>
                        :
                        <Btn mod="blue square regular small"
                            iconBg={true}
                            icon="view"
                            onTap={this.onView}>
                            Score card
                        </Btn>
                    }
                </div>
            </section>
        </ItemWrapper>`

        userName = game.userData.name
        participant = expandedParticipant.getFullName()

        if isExpanded
            liveEl = `<section>
                <div className="rounds-score-card">
                    <div className="stroke white x2-medium">
                        {userName} score card for
                        <span className="stroke white x2-medium text-up"> {participant}</span>
                    </div>
                    <ActiveRoundSwitcher />
                </div>
                <LiveView game={game}
                    roundNumber={activeRoundNumber}
                    participant={expandedParticipant}
                    prediction={expandedPrediction}
                    user={game.userData} />
            </section>`
        else
            liveEl = `<div />`

        `<div>
            {row}
            <ReactCSSTransitionGroup transitionName="slide-down"
                                     transitionEnterTimeout={1000}
                                     transitionLeaveTimeout={1000}>
                {liveEl}
            </ReactCSSTransitionGroup>
        </div>`
