GamePointsAndPosition = React.create(
    render: ({game}) ->
        place = game.placeRange or ''
        points = game.points or 0

        if game.gameType == 'hole-by-hole'
            {correct, wrong} = game.getStats()
            `<div className="data-items data-items--row-end data-items--border-right">
                <div className="data-item">
                    <p className="data-item__text">Correct</p>
                    <div className="data-item__num">
                        <span>{correct}</span>
                    </div>
                </div>
                <div className="data-item">
                    <p className="data-item__text">Wrong</p>
                    <div className="data-item__num">
                        <span>{wrong}</span>
                    </div>
                </div>
                <div className="data-item">
                    <p className="data-item__text">Position</p>
                    <div className="data-item__num">
                        <span>{place}</span>
                    </div>
                </div>
            </div>`
        else
            `<div className="data-items data-items--row">
                <div className="data-item">
                    <p className="data-item__text">Place</p>
                    <div className="data-item__num data-item__num--big">
                        <span>{place}</span>
                    </div>
                </div>
                <div className="data-item">
                    <p className="data-item__text">Points</p>
                    <div className="data-item__num data-item__num--big">
                        <span>{points}</span>
                    </div>
                </div>
            </div>`
)
module.exports = GamePointsAndPosition
