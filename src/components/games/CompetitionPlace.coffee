
module.exports = React.create
    displayName: "CompetitionPlace"

    render: ({position}) ->
        cls = "competition-place "
        if position
            `<div className={cls}>
                    <div className="competition-place__data">
                        <span className="competition-place__number">
                            {position}
                        </span>
                    </div>
                    <div className="competition-place__text">
                        <span>in competition</span>
                    </div>
            </div>`
        else
            `<div className={cls}></div>`
