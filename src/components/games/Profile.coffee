{Flag} = require '../../lib/ui/Flag'

Profile = React.create(
    displayName: 'Profile'

    render: ({participant, game, mod, nameColor, more}) ->
        cls = 'profile '
        if mod
            cls = cls + mod
        nameCls = 'person-info__name '
        if nameColor
            nameCls = nameCls + nameColor

        `<div className={cls}>
            <div className="avatar">
                <Flag country={participant.country}/>
            </div>

            <div className="person-info">
                <div className={nameCls}>
                    {participant.getFullName()}
                </div>
                {this.props.more ?
                <div className="person-info__more">and {more} more golfers</div>
                    : null}
            </div>
        </div>`
)


module.exports = Profile
