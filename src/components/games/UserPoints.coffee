{cx} = require('lib/utils')

module.exports = React.create
    displayName: "UserPoints"

    render: ({game, viewGame}) ->
        place = game.placeRange or ''
        if (game.placeRange or '').indexOf('-') > -1
            annotation = 'shared'
        else
            annotation = ''
        points = game.points or 0
        cls = cx(
            "user-point": true
            "active": viewGame
        )

        `<div className={cls}>
            <div className="user-point__number">
                <span className="user-point__position">{place}</span>
                <span>&nbsp;{annotation}</span>
            </div>
            <div className="user-point__info">in competition</div>
            <div className="user-point__amount">
                <span className="user-point__num">{points}</span> points
            </div>
        </div>`
