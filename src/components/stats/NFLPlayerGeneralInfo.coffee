{POSITION_NAMES} = require('../const')
InjuryStatusBadge = require('../InjuryStatusBadge')
{formatWeight, formatHeight} = require('../../lib/format')


NFLPlayerGeneralInfo = React.create(
    displayName: 'NFLPlayerGeneralInfo'

    render: ({participant}) ->
        team = participant.getTeam()
        game = participant.getGame()

        `<section className="player-info__general">
            <div className="player-info__game">
                {game.homeTeam} @ {game.awayTeam}, {game.formatTime('MMM DD, h:mm A')}
            </div>

            <h3 className="player-info__name">
                {participant.getName()} <InjuryStatusBadge player={participant} />
            </h3>
            {participant.position !== 'DST' &&
                <div>
                    <span className="player-info__team">
                        {team.getFullName()}{' '}&ndash;{' '}
                    </span>
                    <span className="player-info__num">
                        {POSITION_NAMES[participant.position]}{' '}
                        {'#'}{participant.number}
                    </span>
                </div>
            }
            {participant.position !== 'DST' &&
                <div className="player-info__data">
                    H:&nbsp;<strong>{formatHeight(participant.height)}</strong>,{' '}
                    W:&nbsp;<strong>{formatWeight(participant.weight)}</strong>,{' '}
                    Age:&nbsp;<strong>{participant.age}</strong>,{' '}
                    College:&nbsp;<strong>{participant.college}</strong>
                </div>
            }
        </section>`
)

module.exports = NFLPlayerGeneralInfo
