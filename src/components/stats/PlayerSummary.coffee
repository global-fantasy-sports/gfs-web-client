_ = require('underscore')
{ROLE_STATS} = require('../const')
StatLabel = require('./StatLabel')

class PlayerSummary extends React.Component

    @propTypes: {
        summary: React.PropTypes.object.isRequired
    }

    renderItem: ([type, value], index) =>
        if type != 'games' and _(value).isNumber()
            value = value.toFixed(2)

        `<div className="player-summary__item" key={index}>
            <StatLabel className="player-summary__label" type={type} />
            <span className="player-summary__value">{value}</span>
        </div>`

    render: () ->
        entries = Array.from(@props.summary.entries())
        `<div className="player-summary">
            {entries.map(this.renderItem)}
        </div>`

module.exports = PlayerSummary
