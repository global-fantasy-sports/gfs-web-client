_ = require('underscore')

PlayerGeneralInfo = require('./PlayerGeneralInfo')
CommonDialog = require('../dialogs/CommonDialog')
TabPanel = require('../TabPanel')
PlayerPhoto = require('../PlayerPhoto')
PlayerGameLogTable = require('./PlayerGameLogTable')
PlayerStatsChart = require('./PlayerStatsChart')
TinyLineChart = require('../charts/TinyLineChart')
PlayerComparisonChart = require('./PlayerComparisonChart')
PlayerSummary = require('./PlayerSummary')
InjuryStatusBadge = require('../InjuryStatusBadge')
{Btn, Icon} = require('../../lib/ui')
Close = require('../../lib/ui/Close')
{cx} = require('../../lib/utils')
{POSITION_NAMES} = require('../const')
{COMPARE_PLAYERS_MAX} = require('../../lib/const')


class PlayerStatsDialog extends React.Component

    @propTypes: {
        participant: React.PropTypes.object.isRequired
        button: React.PropTypes.oneOf(['add', 'remove', 'disabled'])
        compareData: React.PropTypes.array
        compareMode: React.PropTypes.oneOf(['add', 'remove', 'disabled']).isRequired
        comparePlayers: React.PropTypes.object.isRequired
        activeSeason: React.PropTypes.number.isRequired
        currentSeason: React.PropTypes.number.isRequired
        onCompare: React.PropTypes.func.isRequired
        onHide: React.PropTypes.func.isRequired
    }

    @defaultProps: {
        button: 'disabled'
    }

    constructor: (props) ->
        super(props)
        @state = {
            activeTab: 0
        }

    componentDidMount: ->
        document.addEventListener('keydown', @handleKeyboard)

    componentWillUnmount: ->
        document.removeEventListener('keydown', @handleKeyboard)

    handleKeyboard: (e) =>
        if e.keyCode is 27
            @props.onHide()

    handleButton: (e) =>
        e.preventDefault()
        if @props.button isnt 'disabled'
            @props.onClick(@props.participant)

    handleCompare: (e) =>
        e.preventDefault()
        {compareMode} = @props
        if compareMode isnt 'disabled'
            @props.onCompare(@props.participant)
            @handleTab(2) if compareMode is 'add'

    handleTab: (activeTab) =>
        @setState({activeTab})

    onStarClick: (id) ->
        ->
            flux.actions.data.toggleFavorite(id)

    render: ->
        {participant, button, compareMode, onClick, currentSeason, activeSeason} = @props
        salaryrank =  (participant.formatSalary || participant.formatRank).call(participant)
        points = (participant.formatFPPG || participant.formatFPPR).call(participant)
        chartTitle = if SPORT == 'golf' then 'FPPR' else 'Salary & FPPG'

        rosterButtonMods = cx({
            'draft': button is 'add'
            'undraft': button is 'remove'
            'draft not-active': button is 'disabled'
        })

        compareButtonMods = cx({
            'blue-base': compareMode is 'add'
            'blue-base-active': compareMode is 'remove'
            'blue-base-disabled': compareMode is 'disabled'
        })

        clsStar = cx(
            "star-blue-big": participant.isFavorite
            "star-gray-big": !participant.isFavorite
        )

        `<CommonDialog className="player-info-dialog" ref="dialog">
            <Close mod="light"
                   className="player-info-dialog__close"
                   onClose={this.props.onHide}
            />
            <div className="player-info-dialog__star-icon">
                <Icon i={clsStar} onTap={this.onStarClick(participant.playerId)} />
            </div>
            <header className="player-info-dialog__top">
                <div className="player-info-dialog__column">
                    <PlayerGeneralInfo participant={participant} />

                    <div className="player-info__short-stats">
                        <div className="player-info__fppg-graph">
                            <TinyLineChart
                                height={40}
                                width={150}
                                color="#6c71c4"
                                data={participant.getFPPGHistory(currentSeason)}
                            />
                        </div>
                        <div className="player-info__fppg">
                            {points.value}
                            <span className="stroke lato-medium extra-small black">{points.label}</span>
                        </div>
                    </div>
                </div>

                <div className="player-info-dialog__column">
                    <PlayerPhoto player={participant} size={150} />
                </div>
            </header>

            <div className="player-info-dialog__row">
                <div className="player-info__salary">
                    {salaryrank}
                </div>
                <div>
                    <Btn mod={compareButtonMods} disabled={compareMode === 'disabled'} onTap={this.handleCompare}>
                        {compareMode === 'remove' ? 'Remove from comparison' : 'Add to comparison'}
                    </Btn>
                    {onClick && <Btn mod={rosterButtonMods} disabled={button === 'disabled'} onTap={this.handleButton}>
                        {button === 'remove' ? 'Undraft' : 'Draft'}
                    </Btn>}
                </div>
            </div>
            <div className="player-info-dialog__summary">
                <PlayerSummary summary={participant.getStatSummary(activeSeason)} />
            </div>
                {participant.statInfoHistory.count() > 0  &&
                <TabPanel activeIndex={this.state.activeTab} onClick={this.handleTab}>
                    <PlayerStatsChart
                        key={chartTitle}
                        player={participant}
                        season={currentSeason}
                        height={250}
                    />
                    <PlayerGameLogTable
                        key="Game log"
                        player={participant}
                        currentSeason={currentSeason}
                        activeSeason={activeSeason}
                        height={250}
                    />
                    <PlayerComparisonChart
                        key="Comparison"
                        players={this.props.comparePlayers}
                        data={this.props.compareData}
                        onRemove={this.props.onCompare}
                        height={250}
                    />
                </TabPanel>
                }
        </CommonDialog>`


module.exports = React.create(
    displayName: 'PlayerStatsDialogContainer'

    mixins: [
        flux.Connect('compare', (store) -> store.getNormalizedData())
        flux.Connect('participants', (store, props) -> {
            participant: store.getParticipant(props.participantId)
        })
        flux.Connect('time', (store) -> {
            activeSeason: store.getShownSeason()
            currentSeason: store.getSeason()
        })
    ]

    getCompareMode: () ->
        {players, position, participant} = @state

        if players.has(participant.id)
            'remove'
        else if position and position isnt (participant.position || 1)
            'disabled'
        else if players.size is COMPARE_PLAYERS_MAX
            'disabled'
        else
            'add'

    render: ({}, {data, participant, players, currentSeason, activeSeason}) ->

        `<PlayerStatsDialog
            {...this.props}
            participant={participant}
            currentSeason={currentSeason}
            activeSeason={activeSeason}
            compareMode={this.getCompareMode()}
            comparePlayers={players.toList()}
            compareData={data}
            onCompare={flux.actions.compare.toggle}
        />`
)
