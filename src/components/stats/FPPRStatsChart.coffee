{CartesianGrid, Legend, Line, LineChart, Tooltip, XAxis, YAxis, ReferenceLine} = require('recharts')


class CustomTooltip extends React.Component
    render: () ->
        return null unless @props.active
        {payload, label} = @props

        `<div className="player-chart-custom-tooltip">
            <p>Game Info</p>
            <p>{label}</p>
            <p>FPPR: {payload[0].formatter(payload[0].value)}</p>
        </div>`

formatPoints = (value) ->
    if typeof value is 'number'
        value.toFixed(2)
    else
        '0'



class FPPRStatsChart extends React.Component
    render: () ->
        {player, width, height} = @props

        data = player.statInfoHistory.map((entry) ->
            return {
                date: entry.tourStartDate.format('DD MMM, YY'),
                points: entry.avgFPPR
            }
        )

        `<LineChart
            width={width}
            height={height}
            data={data.toArray()}
        >
            <XAxis
                dataKey="date"
                tick={{fontSize: 12}}
            />
            <YAxis
                yAxisId="points"
                orientation="left"
                stroke="#6c71c4"
                tick={{fontSize: 12}}
            />
            <CartesianGrid
                strokeDasharray="3 3"
            />
            <Tooltip
                content={<CustomTooltip />}
            />
            <Line
                type="linear"
                yAxisId="points"
                dataKey="points"
                stroke="#6c71c4"
                formatter={formatPoints}
                isAnimationActive={false}
            />
            <ReferenceLine yAxisId="points" y={0} stroke="#cccccc"  />
        </LineChart>`

module.exports = FPPRStatsChart
