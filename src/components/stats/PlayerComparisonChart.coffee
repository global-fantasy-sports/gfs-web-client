{Legend, PolarAngleAxis, PolarGrid, PolarRadiusAxis, Radar, RadarChart} = require('recharts')
{prevent} = require('../../lib/utils')
{ROLE_STATS, STAT_NAMES} = require('../const')
Close = require('../../lib/ui/Close')
PlayerPhoto = require('../PlayerPhoto')

colors = [
    '#5DA5DA'
    '#F15854'
    '#DECF3F'
    '#60BD68'
]


class PlayerComparisonLegend extends React.Component

    handleClick: (player) => () =>
        @props.onRemove(player)

    renderItem: ({color, value}, index) =>
        player = @props.players.get(index)
        cls = (name) -> "player-comparison-legend__#{name}"

        `<li key={index} className={cls("item")}>
            <div
                className={cls("item-color")}
                style={{backgroundColor: color, color: color}}
            />

            <PlayerPhoto
                className={cls("item-photo")}
                player={player} size={32}
            />

            <div className={cls("item-value")}>
                <div className={cls('item-value-name')}>{player.getName()}</div>
                {SPORT !== 'golf' && (player.position !== 'DST') &&
                    <div className={cls('item-value-team')}>{player.getTeam().getFullName()}</div>
                }
            </div>

            <Close
                mod="dark"
                className={cls('item-remove')}
                onClose={this.handleClick(player)}
            />
        </li>`

    render: ->
        `<ul className="player-comparison-legend">
            {this.props.payload.map(this.renderItem)}
        </ul>`


getStatName = (name) ->
    STAT_NAMES[name] ? name

class PlayerComparisonChart extends React.Component

    renderPlayerRadar: (player, index) =>
        color = colors[index]
        `<Radar
            key={index}
            name={player.getName()}
            dataKey={'' + player.id}
            stroke={color}
            strokeWidth={2}
            strokeOpacity={0.8}
            fill={color}
            fillOpacity={0.3}
            isAnimationActive={false}
        />`

    render: () ->
        {players} = @props
        return null unless players.size

        {height, width, data, onRemove} = @props

        `<section>
            <RadarChart
                height={height}
                width={width}
                cx="60%" cy="50%"
                data={data}
            >
                <PolarAngleAxis
                    dataKey="stat"
                    tick={{fill: '#333'}}
                    tickFormatter={getStatName}
                />
                <PolarRadiusAxis tick={false} />
                <PolarGrid />
                <Legend
                    layout="vertical"
                    align="left"
                    verticalAlign="top"
                    content={<PlayerComparisonLegend
                        players={players}
                        onRemove={onRemove}
                    />}
                />
                {players.map(this.renderPlayerRadar)}
            </RadarChart>
        </section>`


module.exports = PlayerComparisonChart
