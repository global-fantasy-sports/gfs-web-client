{Btn} = require('../../lib/ui')
{cx} = require('../../lib/utils')


class SeasonSwitcher extends React.Component

    onLastSeasonTap: =>
        flux.actions.time.setShownSeason(@props.currentSeason - 1)

    onCurrentSeasonTap: =>
        flux.actions.time.setShownSeason(@props.currentSeason)

    render: () ->
        {player, activeSeason, currentSeason} = @props
        currentCount = player.getStatsHistory(currentSeason).count()
        prevCount = player.getStatsHistory(currentSeason - 1).count()

        modLast = cx('small', {
            'green': activeSeason != currentSeason && prevCount
            'gray': activeSeason == currentSeason
            'disabled': !prevCount
        })

        modCurrent = cx('small', {
            'green': activeSeason == currentSeason && currentCount
            'gray': activeSeason != currentSeason
            'disabled': !currentCount
        })

        `<section className='season-switcher'>
            <Btn mod={modLast} onTap={this.onLastSeasonTap} disabled={!prevCount}>Last season</Btn>
            <Btn mod={modCurrent} onTap={this.onCurrentSeasonTap} disabled={!currentCount}>Current season</Btn>
        </section>`


module.exports = SeasonSwitcher

