_ = require('underscore')
moment = require('moment')
{CartesianGrid, Legend, Line, LineChart, Tooltip, XAxis, YAxis, ReferenceLine} = require('recharts')
SeasonSwitcher = require('./SeasonSwitcher')


class CustomTooltip extends React.Component
    render: () ->
        return null unless @props.active
        {payload, label} = @props
        {game} = payload[0].payload
        `<div className="player-chart-custom-tooltip">
            <p><strong>{game.homeTeam}@{game.awayTeam}</strong></p>
            <p>{moment(game.startTimeUtc).format('ll')}</p>
            <p>&nbsp;</p>
            <p>Salary: <strong>{formatSalary(payload[0].value)}</strong></p>
            <p>FPPG: <strong>{formatPoints(payload[1].value)}</strong></p>
        </div>`


formatSalary = (value) ->
    if value
        '$' + value
    else
        '0'

formatPoints = (value) ->
    if typeof value is 'number'
        value.toFixed(2)
    else
        '0'

formatDate = (value) ->
    moment(value).format('MMM DD')

class SalaryFPPGStatsChart extends React.Component
    render: () ->
        {player, season, width, height} = @props
        history = player.getStatsHistory(season).cacheResult()
        salaries = history.map((p) -> p.salary or 0).toList()
        points = history.map((p) -> p.points or 0).toList()

        data = history.map((entry) ->
            game = entry.getGame()
            return {
                game
                date: +game.startTimeUtc
                salary: entry.salary
                points: entry.points
            }
        ).toArray()

        rightMin = Math.floor(points.min() / 10) * 10
        rightMax = Math.ceil(points.max() / 10) * 10
        leftMin = 0
        leftMax = Math.ceil(salaries.max() / 1000) * 1000

        if rightMin < 0
            leftMin = -(leftMax / (rightMax / Math.abs(rightMin)))
        else
            rightMin = 0

        `<LineChart
            width={width}
            height={height}
            data={data}
            margin={{top: 15, right: 0, bottom: 0, left: 5}}
        >
            <XAxis
                dataKey="date"
                tick={{fontSize: 12}}
                tickFormatter={formatDate}
            />
            <YAxis
                yAxisId="salary"
                orientation="left"
                stroke="green"
                tick={{fontSize: 12}}
                tickFormatter={formatSalary}
                domain={[leftMin, leftMax]}
            />
            <YAxis
                yAxisId="points"
                orientation="right"
                stroke="#6c71c4"
                tick={{fontSize: 12}}
                domain={[rightMin, rightMax]}
            />
            <CartesianGrid
                strokeDasharray="3 3"
            />
            <Tooltip
                content={<CustomTooltip />}
            />
            <Line
                type="linear"
                yAxisId="salary"
                dataKey="salary"
                stroke="green"
                strokeWidth={2}
                formatter={formatSalary}
                isAnimationActive={false}
            />
            <Line
                type="linear"
                yAxisId="points"
                dataKey="points"
                stroke="#6c71c4"
                strokeWidth={2}
                formatter={formatPoints}
                isAnimationActive={false}
            />
            <ReferenceLine
                yAxisId="points"
                y={0}
                stroke="#cccccc"
            />
        </LineChart>`

module.exports = SalaryFPPGStatsChart
