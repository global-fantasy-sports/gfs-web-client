{formatWeight, formatHeight} = require('../../lib/format')


GolfPlayerGeneralInfo = React.create
    displayName: 'GolfPlayerGeneralInfo'

    render: ({participant}) ->
        tournament = flux.stores.tournaments.getTournament(participant.tournamentId)


        `<section className="player-info__general">
            <div className="player-info__game">
                {tournament.name}
            </div>

            <h3 className="player-info__name">
                {participant.getName()}
            </h3>

            <div className="player-info__data">
                H:&nbsp;<strong>{formatHeight(participant.height)}</strong>,{' '}
                W:&nbsp;<strong>{formatWeight(participant.weight)}</strong>,{' '}
                Age:&nbsp;<strong>{participant.getAge()}</strong>,{' '}
                Country:&nbsp;<strong>{participant.getFullCountry()}</strong>
            </div>
        </section>`


module.exports = GolfPlayerGeneralInfo
