_ = require('underscore')
{Table, Column, Cell} = require('fixed-data-table')
{cx} = require('../../lib/utils')
{ROLE_STATS} = require('../const')
StatLabel = require('./StatLabel')
SeasonSwitcher = require('./SeasonSwitcher')


class HeaderCell extends React.Component

    @defaultProps: {
        align: 'center'
    }

    render: () ->
        `<Cell {...this.props} className="game-log-table__header-cell">
            {this.props.children}
        </Cell>`

class DataCell extends React.Component

    @defaultProps: {
        align: 'center'
    }

    render: () ->
        data = @props.data.get(@props.rowIndex).getStat(@props.field, '—', @props.context)
        props = _.omit(@props, 'rowIndex', 'field', 'data', 'context')
        `<Cell {...props} className="game-log-table__data-cell">
            {data}
        </Cell>`


class PlayerGameLogTable extends React.Component

    @propTypes: {
        player: React.PropTypes.object.isRequired
    }

    render: () ->
        {player, activeSeason, currentSeason, width, height} = @props

        stats = ['points', ROLE_STATS[player.position || 0]...]
        if SPORT == 'nba' || SPORT == 'golf'
            stats = stats.slice(1)

        totalWidth = if SPORT == 'golf' then width else width - 100 - 80 - 80

        if player.getCalculatedStats?
            data = player.getCalculatedStats()
        else
            data = player.getStatsHistory(activeSeason).toList()

        roleStats = stats.map((stat, index) =>
            `<Column
                key={index}
                header={<HeaderCell><StatLabel type={stat} /></HeaderCell>}
                cell={<DataCell field={stat} data={data} />}
                width={totalWidth / stats.length}
            />`
        )

        `<div>
            <SeasonSwitcher
                player={player}
                activeSeason={activeSeason}
                currentSeason={currentSeason}
            />
            <div className="game-log-table">
                <Table
                    headerHeight={50}
                    rowHeight={50}
                    rowsCount={data.size}
                    width={width}
                    height={height}
                >
                    {SPORT !== 'golf' &&
                    <Column
                        header={<HeaderCell>Date</HeaderCell>}
                        cell={<DataCell field="date" data={data} />}
                        width={100}
                    />
                    }
                    {SPORT !== 'golf' &&
                    <Column
                        header={<HeaderCell>Opponent</HeaderCell>}
                        cell={<DataCell field="opponent" data={data} context={{teamId: player.team}} />}
                        width={80}
                    />
                    }
                    {SPORT !== 'golf' &&
                    <Column
                        header={<HeaderCell>Result</HeaderCell>}
                        cell={<DataCell field="result" data={data} />}
                        width={80}
                    />
                    }

                    {roleStats}
                </Table>
            </div>
        </div>`


module.exports = PlayerGameLogTable
