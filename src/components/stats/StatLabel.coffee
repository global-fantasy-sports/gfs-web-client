{cx} = require('../../lib/utils')
{STAT_NAMES, STAT_NAME_ABBRS} = require('../const')


class StatLabel extends React.Component

    @propTypes: {
        type: React.PropTypes.string.isRequired
    }

    render: () ->
        {type} = @props
        className = cx('statistics-label', @props.className)
        `<abbr title={STAT_NAMES[type]} className={className}>
            {STAT_NAME_ABBRS[type]}
        </abbr>`


module.exports = StatLabel
