_ = require('underscore')
strftime = require('strftime')

{numFmt} = require('lib/ui')
{numberEnding} = require('lib/utils')


module.exports = React.create
    displayName: 'CandidateStats'

    mixins: [
        flux.Connect('participants', (store, {participant})-> {
            stats: store.getStats(participant.golferId)
        })
    ]

    render: ({participant, info, onClose, className, showSelect, mod}, {stats}) ->

        averageToParEl = 0
        totalToPar = 0

        lastPosition = participant.get("lastPosition")
        rank = participant.get("rank")
        height = participant.get("height")
        weight = participant.get("weight")
        country = participant.get("country")
        name = participant.get("name")
        birthday = participant.get("birthday")

        if stats.size
            totalToPar   = stats.reduce(((sum, m) -> sum + m.score), 0)
            averageToPar = (totalToPar / stats.size).toFixed(2)
            averageToParEl = `<span>{averageToPar}</span>`

        if @props.children
            info = @props.children
        else
            info = [
                `<div>
                    <small>Last position:</small>{' '}{lastPosition}
                </div>`,
                `<div>
                    <small>Rank:</small>{' '}{rank}
                </div>`
            ]

        inches = height
        feet = Math.floor(inches / 12)
        inches %= 12

        if inches != 0
            inchesEl = `<span>{inches} in</span>`

        cls = 'statistics '
        `<div className={mod ? cls + mod : cls}>
            <div className="game-table game-table--header">
                <div className="statistics__title">
                    <span>Statistics of</span>
                    <b className="statistics__person">{name}</b>
                </div>
            </div>

            <div className="statistics__header">
                <div className="statistics__el">
                    <span>Height:</span>
                    <b>{feet} ft&nbsp; {inchesEl}</b>
                </div>
                <div className="statistics__el">
                    <span>Birthday:</span>
                    {birthday.getFullYear() ?
                        <b>
                            {strftime('%d', birthday)}
                            {numberEnding(strftime('%d', birthday))},&nbsp;
                            {strftime('%b %Y', birthday)}
                        </b>
                    :
                        <b>&mdash;</b>
                    }
                </div>
                <div className="statistics__el">
                    <span>World Golf Ranking: </span>
                    <b>{rank || 'N/A'}</b>
                </div>
                <div className="statistics__el">
                    <span>FedEx Cup Points: </span>
                    <b>&mdash;</b>
                </div>
                <div className="statistics__el">
                    <span>Weight:</span>
                    <b>{weight} lb</b>
                </div>
                <div className="statistics__el">
                    <span>From:</span>
                    <b>{country}</b>
                </div>
                <div className="statistics__el">
                    <span>FedEx Cup Rank: </span>
                    <b>&mdash;</b>
                </div>
                <div className="statistics__el">
                    <span>Scoring Average: </span>
                    <b>{averageToParEl}</b>
                </div>
            </div>

            <div className="statistics__content">
                <CandidateStatsTable stats={stats} />
            </div>
        </div>`


CandidateStatsTable = React.create
    displayName: 'CandidateStatsTable'
    args:
        stats: 'object'

    render: ({stats}) ->
        if not stats.size
            return `<div className="statistics__bottom">
                <p className="stroke white medium">
                    There is no statistics for this golfer yet.
                </p>
            </div>`

        totalMoney = stats.reduce(((sum, m) -> sum + m.get("moneyPrize")), 0)


        els = stats.map (stat, index) ->
            tournament = flux.stores.tournaments.getTournament(stat.tournamentId)
            startDate  = strftime('%d.%m.%y', tournament.get("startDate"))
            endDate = strftime('%d.%m.%y', tournament.get("endDate"))

            `<tr key={index}>
                <td className="stats-table__tour">
                    <p className="stats-table__tour-name">
                        {tournament.get("name")}
                    </p>
                    <p className="stats-table__date">
                        {startDate} &mdash; {endDate}
                    </p>
                </td>
                <td>{stat.position}</td>
                <td>{stat.strokes[0] || '-'}</td>
                <td>{stat.strokes[1] || '-'}</td>
                <td>{stat.strokes[2] || '-'}</td>
                <td>{stat.strokes[3] || '-'}</td>
                <td>{stat.strokes[4] || '-'}</td>
                <td>{stat.score}</td>
                <td>
                    ${numFmt(stat.moneyPrize)}
                </td>
            </tr>`

        `<table className="stats-table">
            <thead className="stats-table__head">
                <tr>
                    <th>Tournament</th>
                    <th>pos</th>
                    <th>R1</th>
                    <th>R2</th>
                    <th>R3</th>
                    <th>R4</th>
                    <th>R5</th>
                    <th>To par</th>
                    <th>Money prize</th>
                </tr>
            </thead>

            <tbody className="stats-table__body">
                {els}
            </tbody>

            <tfoot className="stats-table__foot">
                <tr>
                    <td>Total</td>
                    <td colSpan="7">&nbsp;</td>
                    <td>${numFmt(totalMoney)}</td>
                </tr>
            </tfoot>
        </table>`
