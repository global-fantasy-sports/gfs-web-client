_ = require 'underscore'
{Btn} = require 'lib/ui'
{cx} = require 'lib/utils'


module.exports = React.create
    displayName: "ActiveRoundSwitcher"

    mixins: [
        flux.Connect("scorecards", (store, props) -> {
            game: store.getGame()
            activeRoundNumber: store.getActiveRoundNumber()
        })
    ]

    toggleActiveRound: (roundNumber) ->
        ->
            flux.actions.tap.toggleActiveRound(roundNumber)

    componentDidMount: ->
        gameId = @state.game.id
        if not gameId then return

    render: ({}, {game, activeRoundNumber}) ->
        cs1 = cx(
            "round": true
            "active": activeRoundNumber == game.roundNumber
        )

        cs2 = cx(
            "round": true
            "active": activeRoundNumber == game.roundNumber + 1
        )

        `<section className="rounds-switcher">
            <div className="rounds-switcher__buttons">
                <Btn mod={cs1} onTap={this.toggleActiveRound(game.roundNumber)}>
                    <span className="rounds-switcher__btn-text">Round</span>
                    <span className="rounds-switcher__num">{game.roundNumber}</span>
                </Btn>
                <Btn mod={cs2} onTap={this.toggleActiveRound(game.roundNumber + 1)}>
                    <span className="rounds-switcher__btn-text">Round</span>
                    <span className="rounds-switcher__num">{game.roundNumber + 1}</span>
                </Btn>
            </div>
        </section>`
