{cx} = require('../../lib/utils')
{Btn} = require('../../lib/ui')

AppliedFilter = require('./AppliedFilters')
ParticipantFilter = require('../ParticipantFilter')
ParticipantSearch = require('../ParticipantSearch')
CardsSortingMenu = require('./CardsSortingMenu')
PlayersFilterMenu = require('./PlayersFilterMenu')
{STATS_FILTER_POSITIONS} = require('../const')


class PlayerStatsFilters extends React.Component

    @propTypes: {
        search: React.PropTypes.string.isRequired
        position: React.PropTypes.string.isRequired
        showFavorites: React.PropTypes.bool.isRequired
        showSorting: React.PropTypes.bool.isRequired
    }

    constructor: (props)->
        super(props)
        @state = {
            filterMenuOpen: false
            sortMenuOpen: false
            cardsView: false
            activeFilterName: null
        }

    handlePositionFilter: (value) =>
        flux.actions.statistics.filterPosition(value)

    handleSearch: (value) =>
        flux.actions.statistics.search(value)

    handleFavoritesToggle: (e) =>
        e.preventDefault()
        flux.actions.statistics.toggleFavorites()

    handleViewModeToggle: (e) =>
        e.preventDefault()
        flux.actions.statistics.toggleViewMode()

    handleToggleMenu: (e) =>
        e.preventDefault()
        @setState({
            sortMenuOpen: false
            filterMenuOpen: not @state.filterMenuOpen
            activeFilterName: null
        })

    handleToggleSortMenu: (e) =>
        e.preventDefault()
        @setState({
            filterMenuOpen: false
            sortMenuOpen: not @state.sortMenuOpen
        })

    handleCloseMenu: (e) =>
        e.preventDefault()
        @setState({
            sortMenuOpen: false
            filterMenuOpen: false
        })

    handleAddFilter: (filter) =>
        flux.actions.statistics.addFilter(filter)

        @setState({
            filterMenuOpen: false
            activeFilterName: filter.column.name
        })

    handleRemoveFilter: (filter) =>
        flux.actions.statistics.removeFilter(filter)

    handleClearFilters: (e) =>
        e.preventDefault()
        flux.actions.statistics.clearFilters()

    handleOpenFilter: (filter) =>
        @setState({activeFilterName: filter.column.name})

    handleCloseFilter: () =>
        @setState({activeFilterName: null})

    handleChangeFilter: (name, value) =>
        flux.actions.statistics.filter(name, value)

    render: () ->
        {activeFilterName, filterMenuOpen, sortMenuOpen} = @state
        {available, active, sorting, showFavorites, showSorting, viewMode, position} = @props

        showClear = active and active.size > 0

        {
            handleChangeFilter
            handleRemoveFilter
            handleOpenFilter
            handleCloseFilter
        } = this

        activeFilters = active.toArray().map((item, index) =>
            open = activeFilterName is item.column.name
            `<AppliedFilter
                key={index}
                filter={item}
                open={open}
                onChange={handleChangeFilter}
                onRemove={handleRemoveFilter}
                onClick={open ? handleCloseFilter : handleOpenFilter}
                onClose={handleCloseFilter}
            />`
        )

        `<div className="players-list-filters">
            <div className="players-list-filters__top">
                <div className="players-list-filters__search w-70">
                    <ParticipantFilter
                        active={this.props.position}
                        values={STATS_FILTER_POSITIONS}
                        onChange={this.handlePositionFilter}
                    />
                    <ParticipantSearch
                        value={this.props.search}
                        onChange={this.handleSearch}
                    />
                </div>
                <div className="players-list-filters__mode-buttons">
                    <Btn
                        mod="square gray"
                        iconBg={true}
                        icon={showFavorites ? 'star-blue-big' : 'star-fav'}
                        onTap={this.handleFavoritesToggle}
                    />
                    <Btn
                        mod="square gray"
                        iconBg={true}
                        icon={viewMode === 'cards' ? 'table' : 'cards'}
                        onTap={this.handleViewModeToggle}
                    />
                </div>
            </div>
            <div className="players-list-filters__bottom">
                <div className="players-list-filters__applied-filters">
                    {activeFilters}
                </div>

                <div className="players-list-filters__add-buttons">
                    {showClear &&
                        <Btn
                            mod="blue-base"
                            onClick={this.handleClearFilters}
                        >
                            Clear All
                        </Btn>}

                    <Btn
                        mod="blue-dropmenu"
                        iconBg={true}
                        icon="head-drop-arrow"
                        onClick={this.handleToggleMenu}
                    >
                        Add Filter
                    </Btn>

                    {filterMenuOpen &&
                        <PlayersFilterMenu
                            available={available}
                            active={active}
                            onAddFilter={this.handleAddFilter}
                            onRemoveFilter={this.handleRemoveFilter}
                            onClose={this.handleCloseMenu}
                        />}
                </div>
            </div>
            <div className="players-list-filters__sorting">
                {showSorting && <div className="cards-sorting">
                    <Btn
                        mod="transparent-dropdown"
                        iconBg={true}
                        icon="head-drop-arrow"
                        onClick={this.handleToggleSortMenu}
                    >
                        Sort by {sorting.getDisplayLabel()}
                    </Btn>
                    {sortMenuOpen &&
                    <CardsSortingMenu
                        active={sorting}
                        onClose={this.handleCloseMenu}
                    />}
                </div>}
                <p className="players-list-filters__players-count">
                    Players count!!!
                </p>
                <div className="players-list-filters__sorting-underline"/>
            </div>
        </div>`


module.exports = PlayerStatsFilters
