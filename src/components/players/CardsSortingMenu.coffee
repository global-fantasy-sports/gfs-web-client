{prevent} = require('../../lib/utils')


class CardsSortingMenu extends React.Component

    componentDidMount: () ->
        document.addEventListener('click', @handleClickOutside)

    componentWillUnmount: () ->
        document.removeEventListener('click', @handleClickOutside)

    handleClickOutside: (e) =>
        if not React.findDOMNode(this).contains(e.target)
            @props.onClose(e)

    handleClick: (fieldName) =>
        return (e) =>
            e.preventDefault()
            @props.onClose(e)

            setTimeout((() -> flux.actions.statistics.sort(fieldName)), 100)


    render: () ->
        `<div className="filter-menu">
            <div className="filter-menu__content">
                <ul className="filter-menu__list">
                    <li><a href="#" onClick={this.handleClick('player/name')} className="filter-menu__link">Name</a></li>
                    <li><a href="#" onClick={this.handleClick('player/team')} className="filter-menu__link">Team</a></li>
                    <li><a href="#" onClick={this.handleClick('player/salary')} className="filter-menu__link">Salary</a></li>
                    <li><a href="#" onClick={this.handleClick('player/fppg')} className="filter-menu__link">FPPG</a></li>
                </ul>
            </div>
        </div>`


module.exports = CardsSortingMenu
