{cx} = require('../../lib/utils')
{Btn} = require('../../lib/ui')

Slider = require('../../lib/ui/Slider')


class AppliedFilter extends React.Component

    componentDidMount: () ->
        @toggleClickListener(@props.open)

    componentDidUpdate: () ->
        @toggleClickListener(@props.open)

    componentWillUnmount: () ->
        @toggleClickListener(false)

    toggleClickListener: (open) ->
        if open
            document.addEventListener('click', @handleClickOutside)
        else
            document.removeEventListener('click', @handleClickOutside)

    handleClickOutside: (e) =>
        if not React.findDOMNode(this).contains(e.target)
            @props.onClose()

    handleClick: (e) =>
        if not e.isDefaultPrevented()
            e.preventDefault()
            @props.onClick(@props.filter)

    handleChange: (value) =>
        @props.onChange(@props.filter, value)

    handleRemove: (e) =>
        e.preventDefault()
        @props.onRemove(@props.filter)

    render: () ->
        {filter, open} = @props
        {column, max, min, start, end} = filter

        value =
            if column.name is 'player/salary'
                "$#{start} — $#{end}"
            else
                "#{start} — #{end}"

        clsTooltip = cx({
            "applied-filter__tooltip": true,
            "applied-filter__tooltip--visible": open
        })

        `<div className="applied-filter">
            <div className="applied-filter__in" onClick={this.handleClick}>
                <span className="applied-filter__type">{column.label}:</span>
                <span className="applied-filter__value">{value}</span>
            </div>
            <div className={clsTooltip}>
                <Btn mod="blue-base" onClick={this.handleRemove}>Remove</Btn>
                <Slider
                    label={column.label}
                    min={min} max={max}
                    defaultValue={[start, end]}
                    onChange={this.handleChange}
                />
            </div>
        </div>`


module.exports = AppliedFilter
