{Grid} = require 'react-virtualized'
CssTransitionGroup = require('react-addons-css-transition-group')

{cx} = require('../../lib/utils')
{formatFPPG, formatSalary} = require('../../lib/format')
{Icon} = require('../../lib/ui')
PlaceholderImage = require('../../lib/ui/PlaceholderImage')
FavoriteBtn = require('./FavoriteBtn')


StatsItem = ({label, value, mod}) ->
    `<div className="card-view__stats-item">
        <div className={cx("stroke lato-medium", mod)}>{value}</div>
        <div className="stroke extra-small text-up gray">{label}</div>
    </div>`


FavoritePlayer = ({player}) ->
    {largePhoto, nameFull, playerId, position, team, FPPG, salary} = player

    salFPPG = (salary / FPPG)
    salFPPG = if isNaN(salFPPG) or salFPPG is Infinity then 0 else Math.round(salFPPG)

    teamLogo = "url(#{flux.stores.participants.getTeam(team).logo})"

    `<div className="favorite-player">
        <div className="favorite-player__head ">
            <div
                className="card-view__team-photo"
                style={{backgroundImage: teamLogo}}
            />
            <PlaceholderImage
                className="card-view__player-photo"
                src={largePhoto}
                placeholder="/static/img/nfl-player-placeholder.png"
                width="151"
                height="110"
            />
            <FavoriteBtn
                className="card-view__star-icon"
                playerId={playerId}
            />
        </div>
        <div className="favorite-player__name">
            <div className="card-view__name">{nameFull}</div>
            <div className="card-view__pos">{position} | {team}</div>
        </div>
        <div className="favorite-player__stats">
            <StatsItem mod="x-medium" label="FPPG" value={formatFPPG(FPPG)} />
            <StatsItem mod="x-medium" label="SAL/FPPG" value={salFPPG} />
            <StatsItem mod="x-medium green" label="PRICE" value={formatSalary(salary)} />
        </div>
    </div>`


class FavoritesView extends React.Component

    @propTypes: {
        open: React.PropTypes.bool.isRequired
        width: React.PropTypes.number.isRequired
        players: React.PropTypes.object.isRequired
    }

    renderCell: ({columnIndex}) =>
        `<FavoritePlayer
            player={this.props.players.get(columnIndex)}
        />`

    renderGrid: () ->
        {players, width} = @props

        if not players.size
            `<div className="favorite-players__empty-text">
                Click on the star icon on a player's card or next to his
                name in the list view to add him to your favorites
            </div>`
        else
            `<Grid
                width={width}
                height={320}
                rowCount={1}
                rowHeight={320}
                columnWidth={290}
                columnCount={players.size}
                cellRenderer={this.renderCell}
            />`

    render: () ->
        {players, width, open} = @props

        `<CssTransitionGroup
            className="favorite-players"
            component="div"
            transitionName="favorite-players"
            transitionEnterTimeout={300}
            transitionLeaveTimeout={300}
        >
            {open && this.renderGrid(players)}
        </CssTransitionGroup>`


module.exports = FavoritesView
