_ = require('underscore')
{Table, Column, ColumnGroup, Cell} = require('fixed-data-table')
{Icon} = require('../../lib/ui')
{cx} = require('../../lib/utils')
{formatSalary, formatPoints, formatName} = require('../../lib/format')
FavoriteBtn = require('./FavoriteBtn')

defaultFormatter = (value) -> value

DataCell = (props) ->
    {column, fixed, format, alignment} = props

    format or= defaultFormatter
    column or= props.columns.get(props.columnKey)

    className = cx({
        'players-list__cell': fixed
        'players-list__cell--right': not fixed and not column.highlight
        'players-list__cell--highlight': not fixed and column.highlight
        'players-list__cell--left-align': alignment == 'left'
        'players-list__cell--right-align': alignment == 'right'
    })

    `<Cell
        height={props.height}
        width={props.width}
        className={className}
    >
        {format(column.getCellValue(props.data.get(props.rowIndex)))}
    </Cell>`


FavoriteCell = ({height, width, data, rowIndex}) ->
    `<Cell height={height} width={width} className="players-list__cell">
        <FavoriteBtn playerId={data.get(rowIndex).playerId} />
    </Cell>`


onHeaderTap = _.memoize((column) -> (e) ->
    e.preventDefault()
    flux.actions.statistics.sort(column)
)


HeaderCell = (props) ->
    {column, fixed, sorting, alignment} = props

    column or= props.columns.get(props.columnKey)

    className = cx({
        'players-list__head-cell': fixed
        'players-list__head-cell-right': not fixed and not column.highlight
        'players-list__head-cell-highlight': not fixed and column.highlight
        'players-list__head-cell--left-align': alignment == 'left'
        'players-list__head-cell--right-align': alignment == 'right'
    })

    if sorting and sorting.field is column.name
        sort = switch sorting.getDirection()
            when 'desc' then 'arrow-sort-rev'
            else 'arrow-sort'

    `<Cell
        align="center"
        title={column.label}
        onTap={onHeaderTap(column)}
        className={className}
    >
        {sort && <Icon i={sort} className="sorting-icon" />}{column.abbr}
    </Cell>`


onCategoryTap = _.memoize((category) -> (e) ->
    e.preventDefault()
    flux.actions.statistics.toggleCategory(category)
)

GroupHeaderCell = ({group}) ->
    `<Cell
        onTap={onCategoryTap(group.get('name'))}
        className="players-list__group-head"
    >
        {group.get('label')} <Icon i='table-arrow-red' className={group.get('expanded') ? 'rotate-90' : ""} />
    </Cell>`


class PlayerStatsTable extends React.Component

    @defaultProps: {
        maxHeight: 600
        width: 900
    }

    getRowClassName: (rowIndex) =>
        'players-list__row'

    renderColumnGroup: (group, groupIndex) =>
        {sorting, players} = @props

        children = group.getItems().toArray().map((item) =>
            `<Column
                key={item.name}
                columnKey={item.name}
                allowCellsRecycling={true}
                width={95}
                header={
                    <HeaderCell
                        column={item}
                        sorting={sorting}
                    />}
                cell={
                    <DataCell
                        column={item}
                        data={players}
                    />}
            />`
        )

        `<ColumnGroup
            key={groupIndex}
            header={<GroupHeaderCell group={group} />}
        >
            {children}
        </ColumnGroup>`


    render: () ->
        {columns, players, sorting, columnGroups, width, maxHeight} = @props

        `<Table
            groupHeaderHeight={40}
            headerHeight={40}
            rowHeight={50}
            rowsCount={players.size}
            width={width}
            maxHeight={maxHeight}
            rowClassNameGetter={this.getRowClassName}
        >
            <ColumnGroup fixed header={<Cell className="players-list__head-cell-left"/>}>
                <Column
                    columnKey="player/position" fixed
                    header={<HeaderCell fixed sorting={sorting} columns={columns} alignment="left"/>}
                    cell={<DataCell fixed data={players} columns={columns} alignment="left"/>}
                    width={50}
                />
                <Column
                    columnKey="player/favorite" fixed
                    header={<HeaderCell fixed sorting={sorting} columns={columns} />}
                    cell={<FavoriteCell data={players} columns={columns} />}
                    width={50}
                />
                <Column
                    columnKey="player/name" fixed
                    header={<HeaderCell fixed sorting={sorting} columns={columns} alignment="left"/>}
                    cell={<DataCell fixed data={players} columns={columns} alignment="left" format={formatName}/>}
                    width={120}
                />
                <Column
                    columnKey="player/team" fixed
                    header={<HeaderCell fixed sorting={sorting} columns={columns} alignment="left"/>}
                    cell={<DataCell fixed data={players} columns={columns} alignment="left"/>}
                    width={55}
                />
                <Column
                    columnKey="player/salary" fixed
                    header={<HeaderCell fixed sorting={sorting} columns={columns} alignment="right"/>}
                    cell={<DataCell fixed data={players} columns={columns} format={formatSalary} alignment="right"/>}
                    width={60}
                />
                <Column
                    columnKey="player/fppg" fixed
                    header={<HeaderCell fixed sorting={sorting} columns={columns} alignment="right"/>}
                    cell={<DataCell fixed data={players} columns={columns} format={formatPoints} alignment="right"/>}
                    width={70}
                />
            </ColumnGroup>

            {columnGroups.toArray().map(this.renderColumnGroup)}
        </Table>`


module.exports = PlayerStatsTable
