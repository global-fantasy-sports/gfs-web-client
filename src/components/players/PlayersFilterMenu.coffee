_ = require('underscore')
{bem, cx, prevent} = require('lib/utils')


clsLink = bem("filter-menu__link")


Filter = ({filter, selected, onClick}) ->
    className = clsLink('drop-link', {
        'selected': selected
    })

    handleClick = (e) ->
        e.preventDefault()
        onClick(filter)

    `<li>
        <a href="#" className={className} onClick={handleClick}>
            <span>{filter.column.label}</span>
            {selected ? <span className="filter-menu__check-icon">{'\u2713'}️</span> : null}
        </a>
    </li>`



FilterCategory = ({name, items, expanded, onToggle, onItemClick}) ->
    clsDropList = cx("filter-menu__drop-list", {
        "filter-menu__drop-list--visible": expanded
    })

    filters = items.map((item, index) ->
        `<Filter
            key={index}
            filter={item}
            onClick={onItemClick}
        />`
    )

    handleClick = (e) ->
        e.preventDefault()
        onToggle(name)

    `<li>
        <a href="#" className={clsLink("head")} onClick={handleClick}>{name}</a>
        <ul className={clsDropList}>
            {filters}
        </ul>
    </li>`


module.exports = React.create(
    displayName: "PlayersFilterMenu"

    getInitialState: ->
        expanded: null

    componentDidMount: () ->
        document.addEventListener('click', @handleClickOutside)

    componentWillUnmount: () ->
        document.removeEventListener('click', @handleClickOutside)

    handleClickOutside: (e) ->
        if not React.findDOMNode(this).contains(e.target)
            @props.onClose(e)

    handleCategoryClick: (category) ->
        expanded = if @state.expanded is category then null else category
        @setState({expanded})

    render: ({active, available, onAddFilter, onRemoveFilter}, {expanded}) ->

        activeItems = Array.from(active).map((filter, index) ->
            `<Filter
                key={index}
                filter={filter}
                selected={true}
            />`
        )

        categories = available.toIndexedSeq()
            .filterNot((item) -> active.has(item))
            .groupBy((item) -> item.column.category)

        groupedItems = Array.from(categories.entries()).map(([category, items], index) =>
            onTap = @handleCategoryClick.bind(this, category)

            `<FilterCategory
                key={index}
                name={category}
                expanded={expanded === category}
                onToggle={onTap}
                items={items.toList()}
                onItemClick={onAddFilter}
            />`
        )

        `<div className="filter-menu">
            <div className="filter-menu__content">
                <ul className="filter-menu__list">
                    {activeItems}
                    {groupedItems}
                </ul>
            </div>
        </div>`
)
