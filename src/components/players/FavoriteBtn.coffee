Connect = require('../../flux/connectMixin')
{Icon} = require('../../lib/ui')

module.exports = React.create(
    displayName: 'FavoriteBtn'

    mixins: [
        Connect('participants', (store, props) ->
            isFavorite: store.getPlayer(props.playerId).isFavorite
        )
    ]

    handleClick: (e) ->
        e.preventDefault()
        @setState({isFavorite: not @state.isFavorite}, () =>
            flux.actions.data.toggleFavorite(@props.playerId)
        )

    render: ({playerId, className}, {isFavorite}) ->
        `<div className={className} onTap={this.handleClick}>
            <Icon i={isFavorite ? 'star-blue-big' : 'star-gray-big'} />
        </div>`
)
