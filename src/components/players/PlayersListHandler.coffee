_ = require('underscore')

Connect = require('../../flux/connectMixin')
Preloader = require('../../lib/ui/Preloader')
PlayerStatsTable = require('./PlayerStatsTable')
PlayerStatsFilters = require('./PlayerStatsFilters')
FavoritesView = require('./FavoritesView')
CardsList = require('./CardsList')


module.exports = React.create(
    displayName: 'PlayersListHandler'

    mixins: [
        Connect('statistics', (store) -> store.getPlayersList())
        Connect('participants', (store) -> {
            favorites: store.getFavorites()
        })
    ]

    getInitialState: () -> {
        width: 1200
        height: Math.ceil(window.screen.availHeight * 0.6)
    }

    componentDidMount: () ->
        _.defer(flux.actions.statistics.fetchPlayers)

    handlePositionFilter: (value) ->
        flux.actions.statistics.filterPosition(value)

    renderView: (mode) ->
        {columns, columnGroups, players, sorting, width, height, maxFPPG, minFPPG} = @state

        if mode is 'list'
            `<PlayerStatsTable
                columns={columns}
                columnGroups={columnGroups}
                players={players}
                sorting={sorting}
                width={width}
                maxHeight={height}
            />`
        else if mode is 'cards'
            `<CardsList
                columns={columns}
                columnGroups={columnGroups}
                players={players}
                sorting={sorting}
                width={width}
                maxFPPG={maxFPPG}
                minFPPG={minFPPG}
            />`
        else
            `<Preloader />`

    render: (P, {availableFilters, filters, error, inProgress, players, position, search, sorting, viewMode,  showFavorites, favorites, width}) ->
        `<section className="players-list">
            <FavoritesView
                open={showFavorites}
                width={width}
                players={favorites}
            />

            <PlayerStatsFilters
                players={players}
                position={position}
                search={search}
                available={availableFilters}
                active={filters}
                sorting={sorting}
                showFavorites={showFavorites}
                showSorting={viewMode === 'cards'}
                viewMode={viewMode}
            />

            {error && <div>{error}</div>}

            {this.renderView(inProgress ? 'loading' : viewMode)}
        </section>`
)
