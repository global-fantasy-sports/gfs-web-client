Immutable = require('immutable')
{Grid} = require('react-virtualized')

CardView = require('./CardView')


CARD_WIDTH = 290
CARD_HEIGHT = 950
DATA_ROW_HEIGHT = 64

class CardsList extends React.Component

    constructor: (props, context)->
        super(props, context)

        group = props.columnGroups.first()

        @state = {
            expanded: false
            activeGroup: group
            displayedItems: group.getItems(false)
            activeItem: 'PASSING'
        }

    handleToggleExpanded: (e) =>
        e.preventDefault()
        expanded = not @state.expanded

        @setState({
            expanded
            displayedItems: @state.activeGroup.getItems(expanded)
        })

    handleSelectGroup: (group) =>
        @setState({
            activeItem: group.label
            activeGroup: group
            displayedItems: group.getItems(@state.expanded)
        })

    renderCell: ({columnIndex}) =>
        {players, columns, maxFPPG, minFPPG} = @props
        playerData = players.get(columnIndex)

        `<CardView
            player={playerData.getPlayer()}
            playerData={playerData}
            columns={columns}
            maxFPPG={maxFPPG}
            minFPPG={minFPPG}
            extraRows={this.state.displayedItems}

            active={this.state.activeGroup}
            items={this.state.displayedItems}
            groups={this.props.columnGroups}
            onSelect={this.handleSelectGroup}
            expanded={this.state.expanded}
            onClick={this.handleToggleExpanded}
            activeItem={this.state.activeItem}
        />`

    render: () ->
        {columnGroups, players, sorting, width} = @props
        {activeGroup, expanded, displayedItems} = @state

        height = CARD_HEIGHT + DATA_ROW_HEIGHT * displayedItems.size

        `<div className="players-cards-wrapper">

            <Grid
                width={width}
                height={height}
                columnWidth={CARD_WIDTH}
                rowHeight={height}
                rowCount={1}
                columnCount={players.count()}
                cellRenderer={this.renderCell}
                sorting={sorting}
            />
        </div>`


module.exports = CardsList
