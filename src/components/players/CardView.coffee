_ = require('underscore')
RadialProgressChart = require('../charts/RadialProgressChart')
DynamicBarChart = require('../charts/DynamicBarChart')
BarChart = require('../charts/BarChart')
DoubleAreaChart = require('../charts/DoubleAreaChart')
VolumeChart = require('../charts/VolumeChart')
SimpleLineChart = require('../charts/SimpleLineChart')
{Btn, Icon} = require('lib/ui')
PlaceholderImage = require('../../lib/ui/PlaceholderImage')
{CHART_LABELS, CHART_MAX_CONSTS} = require('../const/nfl')
{cx} = require('../../lib/utils')
{formatFPPG, formatHeight, formatSalary, formatWeight} = require('../../lib/format')
FavoriteBtn = require('./FavoriteBtn')


CardViewTable = ({rows, data, items}) ->

    cells = for cell in [0...rows.size]
        `<div className="card-view__table-cell" key={cell}>
            <p className="card-view__cell-title">{items.get(cell).label}</p>
            <p className="card-view__cell-value">{rows.get(cell).getCellValue(data)}</p>
            <p className="card-view__cell-empty"/>
        </div>`

    `<div className="card-view__table">
        {cells}
    </div>`


StatsItem = ({label, value, mod}) ->
    `<div className="card-view__stats-item">
        <div className={cx("stroke lato-heavy", mod)}>{value}</div>
        <div className="card-view__stats-label">{label}</div>
    </div>`


ChartLabel = ({value, label}) ->
    `<div className="card-view__chart-label">
        <p className="card-view__text-value">{value}&nbsp;</p>
        <p className="card-view__text-label">{label}</p>
    </div>`


StatsDropMenu = ({active, groups, onSelect, openList, dropMenu, activeItem}) ->
    items = groups.toArray().map((item) =>
        onClick = (e) ->
            e.preventDefault()
            onSelect(item)

        `<a className="stats-tabs__item" key={item.name} onClick={onClick}>
            {item.label}
        </a>`
    )

    listCls = cx(
        "stats-tabs__list": true
        "stats-tabs__list--open": dropMenu
    )
    `<div className="stats-tabs">
        <a className="stats-tabs__item stats-tabs__item--top" onClick={openList}>
            <span>{activeItem}</span>
            <Icon i ="arrow-red-down"/>
        </a>
        <div className={listCls}>{items}</div>
    </div>`


CardView = React.create
    displayName: 'CardView'

    getInitialState: ->
        dropMenu: false

    getValue: (name) ->
        {playerData, columns} = @props
        return null unless playerData and columns
        columns.get(name).getCellValue(playerData)

    getRadialSectionValue: (position) ->
        switch position
            when 'QB'
                value = @getValue('passing/cmp')
            when 'WR'
                value = @getValue('receiving/rec')
            when 'TE'
                value = @getValue('receiving/rec')
            when 'RB'
                value = @getValue('rushing/fd')

        value

    getMaxCompareSectionValues: () ->
        ownData = @getValue('charts/compare_yds')
        data = @getValue('charts/compare_yds_aggregated')
        [ownData.player_average_yds || 0, data.league_average_yds || 0, data.best_average_yds || 0]

    getRadialChartProps: () ->
        value = @getValue('charts/round_chart_value')
        {
            value
            max: 100
            labelTop: `<span style={{color:'#333'}}>{value}%</span>`
        }

    getOwnPassChartProps: (data) ->
        max = _(data).max()
        value = data[0]

        {
            height: 10
            maxWidth: 160
            maxValue: max
            data: {
                value
                fill: "#64c285"
            }
        }

    getAveragePassChartProps: (data) ->
        max = _(data).max()
        value1 = data[1]
        value2 = data[2]

        {
            height: 10
            maxWidth: 160
            maxValue: max
            data: {
                value: value2
                fill: "#ccc"
            }
            startLabel: "#{value1.toFixed(2)} AVG"
            endLabel: "#{value2.toFixed(2)} BEST"
        }

    getRushYardsChartProps: (position) ->
        data = @getValue('charts/compare_types_of_yds')
        values = [data.first_average_yds, data.second_average_yds]
        max = _(values).max()
        maxIndex = _(values).indexOf(max)
        label = CHART_LABELS.yardsCompareMaxLabel[position][maxIndex]

        {
            height: 10
            maxWidth: 160
            maxValue: max
            endLabel: "#{max} #{label}"
            data: {
                value: _(values).min()
                fill: "#64c285"
            }
        }

    getRushYardsChartLabel: (position) ->
        data = @getValue('charts/compare_types_of_yds')
        values = [data.first_average_yds, data.second_average_yds]
        min = _(values).min()
        minIndex = _(values).indexOf(min)
        CHART_LABELS.yardsCompare[position][minIndex]

    getLastPointsChartProps: (position, data) ->
        pointsAvg = data['average']
        points = data['player']

        convert = (values = []) ->
            _.object(values.map((value, index) -> ["T#{index + 1}", value]))

        {
            width: 230
            height: 120
            max: @props.maxFPPG
            min: @props.minFPPG
            data: [{
                fill: "#ddf2e4"
                points: convert(pointsAvg)
            }, {
                fill: "#19ee18"
                points: convert(points)
            }]
        }

    handleToggleFavorite: (e) ->
        e.preventDefault()
        flux.actions.data.toggleFavorite(@props.playerData.playerId)

    toggleDropList: ->
        @setState({dropMenu: not @state.dropMenu})

    render: ({player, playerData, maxFPPG, extraRows,
            active, groups, onSelect, items, onClick, expanded, activeItem}, {dropMenu}) ->
        position = @getValue('player/position')
        team = @getValue('player/team')
        name = @getValue('player/name')
        fppg = @getValue('player/fppg')
        salary = @getValue('player/salary')

        salFPPG = (salary / fppg)
        salFPPG = if isNaN(salFPPG) or salFPPG is Infinity then 0 else Math.round(salFPPG)

        radialSectionValue = @getRadialSectionValue(position)

        games = @getValue('player/games_played')
        radialChartProps = @getRadialChartProps()
        radialLabel = CHART_LABELS.radial[position]

        maxCompareLabel = CHART_LABELS.maxCompare[position]
        maxCompareData = @getMaxCompareSectionValues()

        ownPass = @getOwnPassChartProps(maxCompareData)
        avgPass = @getAveragePassChartProps(maxCompareData)

        yardsCompareLabel = @getRushYardsChartLabel(position)
        rushYards = @getRushYardsChartProps(position)
        lastPoints = @getLastPointsChartProps(position, @getValue('charts/compare_fppg'))

        teamLogo = "url(#{flux.stores.participants.getTeam(team).logo})"

        progress = Math.round(games / 16 * 100)

        `<section className="card-view">
            <div className="card-view__head">
                <div className="card-view__team-photo" style={{backgroundImage: teamLogo}}/>
                <PlaceholderImage
                    className="card-view__player-photo"
                    src={player.largePhoto}
                    placeholder="/static/img/nfl-player-placeholder.png"
                    width="151"
                    height="110"
                />
                <FavoriteBtn className="card-view__star-icon" playerId={player.id} />
            </div>
            <div className="card-view__row card-view__row--name">
                <div className="card-view__name">
                    <span className="card-view__pos">{position}</span>&nbsp;
                    <span className="card-view__player-name"> {name}</span>
                </div>
                <div className="card-view__progress">
                    <span className="card-view__team">{team}</span>&nbsp;
                    <span className="card-view__games"> {games}/16 games</span>
                </div>
            </div>

            <div className="card-view__progress-bar">
                <div className="bar" style={{width: progress + '%'}}></div>
            </div>

            <div className="card-view__row card-view__row--characteristics flex-row-center">
                <StatsItem mod="medium black" label="AGE" value={player.age} />
                <StatsItem mod="medium black" label="HEIGHT" value={formatHeight(player.height)} />
                <StatsItem mod="medium black" label="WEIGHT" value={formatWeight(player.weight)} />
            </div>

            <div className="card-view__stats">
                <StatsItem mod="x-medium" label="FPPG" value={formatFPPG(fppg)} />
                <StatsItem mod="x-medium" label="SAL/FPPG" value={salFPPG} />
                <StatsItem mod="x-medium" label="PRICE" value={formatSalary(salary)} />
            </div>

            <div className="card-view__row flex-row-center">
                <RadialProgressChart
                    label="Successful passes"
                    barColor="green"
                    {...radialChartProps}
                />
            </div>

            <div className="card-view__row">
                <ChartLabel
                    label={maxCompareLabel}
                    value={maxCompareData[0].toFixed(2)}
                />
                <div className="card-view__dynamic">
                    <div className="card-view__dynamic-item">
                        <DynamicBarChart {...ownPass} />
                    </div>
                    <div className="card-view__dynamic-item">
                        <BarChart {...avgPass} />
                    </div>
                </div>
            </div>

            <div className="card-view__row">
                <ChartLabel
                    label={yardsCompareLabel}
                    value={rushYards.data.value.toFixed(2)}
                />
                <div>
                    <DynamicBarChart {...rushYards}/>
                </div>
            </div>

            <div className="card-view__row">
                <ChartLabel label="last fppg" value={maxFPPG}/>
                <SimpleLineChart {...lastPoints}/>
            </div>

            <div className="card-view__drop-down">
                <StatsDropMenu
                    active={active}
                    groups={groups}
                    onSelect={onSelect}
                    openList={this.toggleDropList}
                    dropMenu={dropMenu}
                    activeItem={activeItem}
                />
            </div>

            <CardViewTable rows={extraRows} data={playerData} items={items}/>
            <div className="players-cards-btn">
                <Btn mod="blue-base" onClick={onClick}>
                    <Icon
                        i="nfl-arrow-right"
                        className={expanded ? 'rotateBack-90' : 'rotate-90'}
                    />
                </Btn>
            </div>
        </section>`


module.exports = CardView
