{Input, Checkbox} = require "../../lib/forms"
{SelectBox} = require "../../lib/ui/SelectBox"
{prevent} = require "lib/utils"
countries = require '../../lib/constants/countries'
states = require '../../lib/constants/states'
{Btn, Link} = require "lib/ui"


module.exports = React.create
    displayName: "FinishRegistrationDialog"

    mixins: [flux.Connect("forms", (store) ->
        isAccepted: store.getValue("ACCEPT_TERMS_AND_CONDITIONS")
        error: store.getError("FINISH_REGISTRATION")
    ), flux.Connect("users", (store) ->
        country: store.getCurrentUser().get("country")
        state: store.getCurrentUser().get("state")
    )]

    termsClicked: (e) ->
        flux.actions.forms.setValue("ACCEPT_TERMS_AND_CONDITIONS", e.target.checked)

    onCancel: ->
        flux.actions.connection.logout()

    onContinue: ->
        email = flux.stores.forms.getValue("FINISH_REGISTRATION_EMAIL")

        if @state.country
            country = @state.country
            state = @state.state
            terms = true
        else
            country = flux.stores.forms.getValue("country")
            state = flux.stores.forms.getValue("state")
            terms = @state.isAccepted

        flux.actions.tap.finishRegistration({email, country, state, terms})

    render: ({}, {error, country, state}) ->
        `<section className="finish-registration-dialog">
            <Input placeholder="Email"
                className="finish-registration-dialog__element"
                theme="black"
                validationId="FINISH_REGISTRATION_EMAIL"
                label="Check your email:"/>

            {!country &&
                <SelectBox options={countries}
                    name="country"
                    validationId="country" />
            }

            {!country &&
                <SelectBox options={states}
                    name="state"
                    validationId="state"/>
            }

            {!country &&
                <Checkbox
                    className="finish-registration-dialog__label"
                    name="terms"
                    onChange={this.termsClicked}>
                        I agree with&nbsp;
                        <Link to="/terms/" className="finish-registration-dialog__link">
                            <span onTap={prevent} className="stroke small blue">
                                Terms &amp; Conditions
                            </span>
                        </Link>
                </Checkbox>
            }

            {error &&
                <div className="error-message">
                    <span className="error-message__text">
                        {error.message}
                    </span>
                </div>
            }
            <div className="flex-row-between">
                <Btn mod="red"
                     onTap={this.onCancel}>
                    Cancel
                </Btn>
                <Btn mod="green"
                     onTap={this.onContinue}>
                    Continue
                </Btn>
            </div>
        </section>`
