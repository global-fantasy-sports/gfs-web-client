{cx} = require('../../lib/utils')


module.exports = React.create
    displayName: "CommonDialog"

    propTypes: {
        title: React.PropTypes.string
        className: React.PropTypes.string
        children: React.PropTypes.node.isRequired
    }

    render: ({title, children, className, mod}) ->
        if mod
            mods = mod.split(' ').map((m) ->
                "common-dialog--#{m}"
            ).join(' ')
        else mods = ''

        `<section className={cx('common-dialog', mods,  className)}>
            {title &&
                <span className="common-dialog__title" name="common-dialog-title">{title}</span>
            }

            <section>
                {children}
            </section>
        </section>`
