{Input} = require "lib/forms"
{Btn} = require "lib/ui"


module.exports = React.create
    displayName: "AccountExistsDialog"

    mixins: [flux.Connect("forms", (store) ->
        error: store.getError("PASSWORD_RESET_ERROR") || store.getError("LINK_ACCOUNTS_ERROR")
    ), flux.Connect("users", (store) ->
        email: store.getCurrentUser().get("email") || store.getCurrentUser().get("fb_email")
    )]

    onResetPassword: ->
        flux.actions.tap.resetPassword({email: @state.email})

    onContinue: ->
        email = @state.email
        password = flux.stores.forms.getValue("EXISTING_PASSWORD")
        flux.actions.tap.linkAccounts({email, password})

    render: ({}, {email, error}) ->
        `<section className="account-exists-dialog">
            <div className="label">
                An account with email address {email} already exists.<br />
                Please provide the password for this account.<br />
                &nbsp;Or you may <span className="link" onTap={this.onResetPassword}>reset password</span>
                &nbsp;if you forgot it.
            </div>

            <Input className="account-exists-dialog__password"
                   placeholder="Password"
                   name="password"
                   validationId="EXISTING_PASSWORD"
                   type="password" />

            {error &&
                <section className="error-message">
                    <span className="error-message__text">{error.message}</span>
                </section>
            }
            <section className="common-dialog__action-bar">
                <Btn mod="green-linear medium"
                     onTap={this.onContinue}>
                    Continue
                </Btn>
            </section>
        </section>`
