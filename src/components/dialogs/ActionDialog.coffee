{Icon} = require '../../lib/ui'


ActionDialog = React.create
    displayName: 'ActionDialog'

    handleCloseButton: ->
        flux.actions.tap.closeDialog()

    render: ({title, content, actionContent}) ->
        `<section className="action-dialog">
            <div className="action-dialog__header">
                {title}
                <Icon
                    className="action-dialog__close-button"
                    i="cross-white-small"
                    onTap={this.handleCloseButton}
                />
            </div>
            <div className="action-dialog__body">
                <div className="action-dialog__content">
                    {content}
                </div>
                <div className="action-dialog__action-bar">
                    {actionContent}
                </div>
            </div>
        </section>`


module.exports = ActionDialog
