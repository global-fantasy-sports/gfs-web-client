ActionDialog = require './ActionDialog'
{Btn} = require '../../lib/ui'


DeleteLineupDialog = React.create
    displayName: 'DeleteLineupDialog'

    handleDeleteLineup: ->
        id = @props.lineup.id
        flux.actions.lineup.remove(id)
        flux.actions.tap.closeDialog()

    handleCancel: ->
        flux.actions.tap.closeDialog()

    renderContent: ->
        name = @props.lineup.name

        `<div className="delete-lineup">
            <div className="delete-lineup__text">ARE YOU SURE YOU WANT DELETE YOUR LINE-UP?</div>
            <div className="delete-lineup__name">{name}</div>
        </div>`

    renderButtons: ->
        [
            `<Btn
                mod="blue-base"
                onTap={this.handleCancel}
            >
                Cancel
            </Btn>`
            `<div>
                &nbsp;&nbsp;
            </div>`
            `<Btn
                mod="gray"
                onTap={this.handleDeleteLineup}
            >
                Delete
            </Btn>`
        ]

    render: ({lineupName}) ->
        `<ActionDialog
            title="Delete line-up"
            content={this.renderContent()}
            actionContent={this.renderButtons()}
        />`


module.exports = DeleteLineupDialog
