ActionDialog = require './ActionDialog'
{Btn} = require '../../lib/ui'
{Input} = require '../../lib/forms'


SaveLineupDialog = React.create
    displayName: 'SaveLineupDialog'

    getInitialState: ->
        editedName: @props.lineup.name
        error: null

    handleNameChange: (e) ->
        @setState({editedName: e.target.value})

    handleSaveLineup: ->
        if @refs.name.value().trim()
            lineup = @props.lineup.set('name', @refs.name.value())
            flux.actions.lineup.saveLineup(lineup)
        else
            @setState({error: 'Line-up name cannot be empty'})

    handleCancel: ->
        flux.actions.tap.closeDialog()

    renderContent: ->
        lineup = @props.lineup

        `<div className="save-lineup">
            <div className="save-lineup__row">
                <div className="save-lineup__cell">
                    <div className="save-lineup__title">
                        LINE-UP BUDGET
                    </div>
                    <div className="save-lineup__value">
                        ${lineup.getBudget()}
                    </div>
                </div>
                <div className="save-lineup__cell">
                    <div className="save-lineup__title">
                        LINE-UP COST
                    </div>
                    <div className="save-lineup__value">
                        ${lineup.getCost()}
                    </div>
                </div>
                <div className="save-lineup__cell">
                    <div className="save-lineup__title">
                        AVG. FPPG
                    </div>
                    <div className="save-lineup__value">
                        {lineup.getFPPG().toFixed(2)}
                    </div>
                </div>
            </div>
            <div className="save-lineup__input-wrapper">
                <Input
                    ref="name"
                    placeholder="GIVE YOUR LINE-UP A NAME"
                    className="save-lineup__input"
                    value={this.state.editedName}
                    onChange={this.handleNameChange}
                />
                {this.state.error &&
                <div className="save-lineup__error">
                    {this.state.error}
                </div>
                }
            </div>
        </div>`

    renderButtons: ->
        [
            `<Btn
                mod="blue-base"
                onTap={this.handleCancel}
            >
                Cancel
            </Btn>`
            `<div>
                &nbsp;&nbsp;
            </div>`
            `<Btn
                mod="blue-base"
                onTap={this.handleSaveLineup}
            >
                SAVE LINE-UP
            </Btn>`
        ]

    render: ({lineupName}) ->
        `<ActionDialog
            title="Save line-up"
            content={this.renderContent()}
            actionContent={this.renderButtons()}
        />`


module.exports = SaveLineupDialog
