{cx} = require('../../lib/utils')
{Icon} = require('../../lib/ui')
Close = require('../../lib/ui/Close')


NflRulesDialog = React.create
    getInitialState: ->
        tabIndex: 0

    onMenuTap: (index) ->
        =>
            @setState({tabIndex: index})

    renderGameFormatTab: ->
        `<div>
            <p>In a salary cap game, the fantasy player acts as a virtual owner with a virtual bank
            account to draft athletes whose daily/weekly game performance translates into points.
            These points are awarded for different categories(e.g touchdowns, rushing yards, field goals and sacks for American Football)
             - more info about scoring can be found in the  scoring rules section.</p>
            <br />
            <p>Players' salaries rise and fall according to their daily/weekly performance,
            and the goal of the user, as a virtual general manager of a team, is to build
            the strongest lineup possible without exhausting his payroll (also known as a Salary Cap).
             GFS’ standard Salary Cap is 50,000 virtual USD.</p>
        </div>`

    renderRoomTypes: ->
        `<div>
            <p>
                GFS provides multiple competition rooms where users are playing against other users
                for prizes. The competition room defines the prize payout structure, the type of game,
                the size of the entry fee to play, and how many players can play in the competition room.
            </p>
            <br />
            <br />

            <span className="rules-dialog__inner-title">
                Money and token rooms
            </span>
            <br />
            <br />
            <p>
                All games must be paid for using an Entry Fee. GFS offers two types of entry fee
                payments: a) real money and b) token games. So, a user can choose what he or she likes
                more: play for real cash or start from playing virtual currency (tokens) that he/she
                can earn from different activities on the platform.
            </p>
            <br />
            <br />

            <span className="rules-dialog__inner-title">
                Labels of rooms based on difficulty and prize distribution
            </span>
            <br />
            <br />
            <p>
                GFS provides two labels for rooms in terms of difficulty - Money rooms and Casual rooms.
            </p>

            <div className="rules-dialog__indent">
                <p>
                    <span className="rules-dialog__inner-title">
                        Money rooms:&nbsp;
                    </span>
                    you pay to enter using real money, and you can also win real money that you can withdraw
                    into your personal bank account after. So, if you are skilled fantasy player - you’ll
                    eventually start to pay in these types of competition rooms!
                </p>
                <br />
                <p>
                    <span className="rules-dialog__inner-title">
                        Casual rooms:&nbsp;
                    </span>
                    are the rooms use virtual tokens. You pay and win virtual tokens that be exchanged
                    for new games or other prizes depending on the individual competition room.
                </p>
            </div>
            <br />

            <p>
                GFS can from time to time change the labels for each room type because of the popularity
                change or difficulty for some room types.
            </p>
            <br />
            <br />

            <span className="rules-dialog__inner-title">
                Prize distribution
            </span>
            <br />
            <br />

            <p>
                Some rooms have equal prize distribution for each winner. These rooms are multiplier rooms.
            </p>
            <br />

            <p>
                Some rooms have a varying prize distribution depending on where a user finishes
                against the competition. Such distribution follows a descending curve (where the
                higher positions win more than lower ones). These rooms are Top X rooms.
            </p>

            <br />
            <br />
            <span className="rules-dialog__inner-title">
                Labels of rooms to provide better experience for players
            </span>
            <br />
            <br />

            <p>
                Also GFS is trying to provide the best experience for players, so GFS gives additional
                rooms labels that are based on historical info about player’s activity or skill level.
            </p>

            <div className="rules-dialog__indent">
                <span className="rules-dialog__inner-title">
                    Beginner:&nbsp;
                </span>
                only for players who have played less than 50 games.
                <br />
                <span className="rules-dialog__inner-title">
                    Guaranteed:&nbsp;
                </span>
                no matter how many users enter a competition room, this competition will run unless real
                world events dictates otherwise!
                <br />

                <span className="rules-dialog__inner-title">
                    Featured:&nbsp;
                </span>
                exciting and popular competition rooms.
            </div>
            <br />
            <br />

            <span className="rules-dialog__inner-title">
                Competition rooms types
            </span>
            <br />
            <br />

            <span className="rules-dialog__inner-title">
                Multiplier rooms
            </span>
            <br />
            <br />

            <p>
                The main idea of multiplier - is to multiply your entry fee with some value
                (x2,x3,x4). All winners are rewarded with an equal prize size. So there is no
                prize size difference e.g. between number 1 and number 2, or number 1 and number 3.
            </p>

            <div className="rules-dialog__indent">
                <span className="rules-dialog__inner-title">
                    Double:&nbsp;
                </span>
                fantasy player doubles his money.
                <br />

                <span className="rules-dialog__inner-title">
                    Triple:&nbsp;
                </span>
                fantasy player triples his money.
                <br />

                <span className="rules-dialog__inner-title">
                    Quadruple:&nbsp;
                </span>
                fantasy player quadruples money.
                <br />

            </div>

            <br />
            <br />

            <span className="rules-dialog__inner-title">
                Top X-rooms
            </span>
            <br />
            <br />
            <p>
                These competition rooms award prizes to only a fixed percentage of the players. Prizes are
                distributed following a distribution curve (see below for more info).
            </p>
            <div className="rules-dialog__indent">
                <span className="rules-dialog__inner-title">
                    50/50:&nbsp;
                </span>
                means that 50% of the users win.
                <br />

                <span className="rules-dialog__inner-title">
                    Top 15:&nbsp;
                </span>
                means that top 15 % of the users win.
                <br />

                <span className="rules-dialog__inner-title">
                    Top 20:&nbsp;
                </span>
                means that 20 % of the users win.
                <br />
            </div>
            <br />
            <br />

            <span className="rules-dialog__inner-title">
                Head 2 Head
            </span>
            <br />
            <br />
            <p>
                Fantasy player plays against another only one other fantasy player and the winner takes all.
            </p>
            <br />
            <br />

            <span className="rules-dialog__inner-title">
                Additional rooms types
            </span>
            <br />
            <br />

            <span className="rules-dialog__inner-title">
                Free roll
            </span>
            <br />
            <br />

            <p>
                Freerolls are free money rooms (users don’t pay a real money entry fee) but winners with
                can win real money, virtual tokens, tournament tickets, or other such prizes.
            </p>
            <br />

            <span className="rules-dialog__inner-title">
                Qualifiers
            </span>
            <br />
            <br />

            <p>
                The general idea of a qualifier is that you are trying to win your way into a big
                time game. So, for example, if there is a $150 buy-in daily fantasy game happening
                the following weekend, there may be a tournament that has just a $10 buy-in, and you’ll
                compete against the rest of the field for a spot in the action.
            </p>
            <br />

            <span className="rules-dialog__inner-title">
                Satellites
            </span>
            <br />
            <br />

            <p>
                Satellites are similar to qualifiers. They give the winner – or winners – entry into more
                expensive contests. One of the differences is that you’ll typically see satellites at the
                lower-stakes end of the chain.
            </p>
        </div>`


    renderNflRules: ->
        `<div>
            <span className="rules-dialog__inner-title">
                Glossary
            </span>
            <div className="rules-dialog__indent">
                <span className="rules-dialog__inner-title">
                    Games pool:&nbsp;
                </span>
                is a set of games used for competition rooms; each competition room is tied to one Games Pool.
                Competition rooms tied to the same Games Pool can be created at different times.
                Therefore, game policies are based on the timing of a Games Pool  being made available,
                not each individual competition room.
                <br />

                <span className="rules-dialog__inner-title">
                    Pool of players:&nbsp;
                </span>
                all NFL athletes that are available for drafting in the current week and selected competition
                room. NOTE - for rooms with Games Poll the set of players will be different.
                <br />
            </div>
            <br />

            <span className="rules-dialog__inner-title">
                Rules
            </span>
            <div className="rules-dialog__indent">
                <ul className="rules-dialog__list">
                    <li>
                        In each competition room, participants will create a lineup by selecting players listed in
                        the Pool of Players. Each player listed has an salary and a valid lineup must not exceed
                        the salary cap of $50,000.
                    </li>
                    <br />

                    <li>
                        Competition room results will be determined by the total points accumulated by
                         each individual lineup entry (scoring rules summarized below).
                    </li>
                    <br />

                    <li>
                        The Pool of Players will consist of all NFL players expected to be on the
                        active roster for any team scheduled to play in particular Games Pool  (rooms
                        with different period of time and Pool of Players are available). Occasionally
                         a player may be missing from the Pool of Players due to trades or other
                         unforeseen circumstances.
                    </li>
                    <br />

                    <li>
                        In most circumstances, once the Pool of Players is established for a Games Pool,
                        including positions and salaries, the Pool of Players will not be adjusted.
                        In the rare event that there is a mistake within the Pool of Players that would
                        significantly impact game quality, GFS reserves the right to correct the mistake
                        after competiton rooms for that Games Pool have become available. This may result
                        in valid lineups becoming invalid. This would most likely occur shortly after
                        the Pool of Players becomes available, far in advance of the competition room
                        start time, and participants who have entered competition rooms for the Games
                        Pool prior to the adjustment will be notified via email.
                    </li>
                    <br />

                    <li>
                        Participation in each competition room must be made only as specified in the
                        Terms of Use. Failure to comply with these Terms of Use will result in
                        disqualification and, if applicable, prize forfeiture.
                    </li>
                </ul>
            </div>
        </div>`

    renderRosterRequirements: ->
        `<div>
            <p>
                Rosters will consist of 9 players and must include players. In addition to the salary cap,
                there are two restrictions governing the mix of players allowed in your lineup for GFS
                competiton rooms. The first is that you must pick players from at least three different teams.
                The second is that you may not pick more than four players from the same team. The system
                will prevent you from saving a lineup that would violate either of these restrictions.
            </p>
            <br />
            <br />

            <p>
                The 9 roster positions are: QB, RB1, RB2, WR1, WR2, WR3, TE, FLEX (RB/WR/TE), and DST.
            </p>
        </div>`

    renderScoringRules: ->
        `<div className="rules-dialog__table">
            <div className="rules-dialog__table-row">
                <span className="rules-dialog__table-cell rules-dialog__table-cell--caption">
                    Offense
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--caption">
                    Defense
                </span>
            </div>
            <div className="rules-dialog__table-row">
                <span className="rules-dialog__table-cell rules-dialog__table-cell--inner-caption">
                    Statistical Category
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--inner-caption">
                    Fantasy Points
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--inner-caption">
                    Statistical Category
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--inner-caption">
                    Fantasy Points
                </span>
            </div>
            <div className="rules-dialog__table-row">
                <span className="rules-dialog__table-cell">
                    Passing TD
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +4
                </span>
                <span className="rules-dialog__table-cell">
                    Sack
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +1
                </span>
            </div>
            <div className="rules-dialog__table-row">
                <span className="rules-dialog__table-cell">
                    25 Passing Yards
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    <span>+1</span>
                    <span>(+0.04PT/ per yard )</span>
                </span>
                <span className="rules-dialog__table-cell">
                    Interception
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +2
                </span>
            </div>
            <div className="rules-dialog__table-row">
                <span className="rules-dialog__table-cell">
                    300+ Yard Passing Game
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +3
                </span>
                <span className="rules-dialog__table-cell">
                    Fumble Recovery
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +2
                </span>
            </div>
            <div className="rules-dialog__table-row">
                <span className="rules-dialog__table-cell">
                    Interception
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    -1
                </span>
                <span className="rules-dialog__table-cell">
                    Kickoff Return TD
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +6
                </span>
            </div>
            <div className="rules-dialog__table-row">
                <span className="rules-dialog__table-cell">
                    10 Rushing Yards
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    <span>+1PT</span>
                    <span>(+0.1PT per yard )</span>
                </span>
                <span className="rules-dialog__table-cell">
                    Punt Return TD
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +6
                </span>
            </div>
            <div className="rules-dialog__table-row">
                <span className="rules-dialog__table-cell">
                    Rushing TD
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +6
                </span>
                <span className="rules-dialog__table-cell">
                    Interception Return TD
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +6
                </span>
            </div>
            <div className="rules-dialog__table-row">
                <span className="rules-dialog__table-cell">
                    100+ Yard Rushing Game
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +3
                </span>
                <span className="rules-dialog__table-cell">
                    Fumble Recovery TD
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +6
                </span>
            </div>
            <div className="rules-dialog__table-row">
                <span className="rules-dialog__table-cell">
                    10 Receiving Yards
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    <span>+1</span>
                    <span>(+0.1PT per yard)</span>
                </span>
                <span className="rules-dialog__table-cell">
                    Blocked Punt or FG Return TD
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +6
                </span>
            </div>
            <div className="rules-dialog__table-row">
                <span className="rules-dialog__table-cell">
                    Reception
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +1
                </span>
                <span className="rules-dialog__table-cell">
                    Safety
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +2
                </span>
            </div>
            <div className="rules-dialog__table-row">
                <span className="rules-dialog__table-cell">
                    Receiving TD
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +6
                </span>
                <span className="rules-dialog__table-cell">
                    Blocked Kick
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +2
                </span>
            </div>
            <div className="rules-dialog__table-row">
                <span className="rules-dialog__table-cell">
                    100+ Yard Receiving Game
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +3
                </span>
                <span className="rules-dialog__table-cell">
                    0 Points Allowed
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +10
                </span>
            </div>
            <div className="rules-dialog__table-row">
                <span className="rules-dialog__table-cell">
                    Punt/Kickoff Return for TD
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +6
                </span>
                <span className="rules-dialog__table-cell">
                    1-6 Points Allowed
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +7
                </span>
            </div>
            <div className="rules-dialog__table-row">
                <span className="rules-dialog__table-cell">
                    Fumble Lost
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    -1
                </span>
                <span className="rules-dialog__table-cell">
                    7-13 Points Allowed
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +4
                </span>
            </div>
            <div className="rules-dialog__table-row">
                <span className="rules-dialog__table-cell">
                    2 Point Conversion (Pass, Run, or Catch)
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +2
                </span>
                <span className="rules-dialog__table-cell">
                    14-20 Points Allowed
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +1
                </span>
            </div>
            <div className="rules-dialog__table-row">
                <span className="rules-dialog__table-cell">
                    Offensive Fumble Recovery TD
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    +6
                </span>
                <span className="rules-dialog__table-cell">
                    21-27 Points Allowed
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    0
                </span>
            </div>
            <div className="rules-dialog__table-row">
                <span className="rules-dialog__table-cell">
                    &nbsp;
                </span>
                <span className="rules-dialog__table-cell">
                    &nbsp;
                </span>
                <span className="rules-dialog__table-cell">
                    28-34 Points Allowed
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    -1
                </span>
            </div>
            <div className="rules-dialog__table-row">
                <span className="rules-dialog__table-cell">
                    &nbsp;
                </span>
                <span className="rules-dialog__table-cell">
                    &nbsp;
                </span>
                <span className="rules-dialog__table-cell">
                    35+ Points Allowed
                </span>
                <span className="rules-dialog__table-cell rules-dialog__table-cell--value">
                    -4
                </span>
            </div>

            <div className="rules-dialog__table-footer">
                <p>
                    Notes: For purposes of GFS defensive scoring, points allowed are calculated as:
                </p>
                <br />
                <p>
                    6 * (Rushing TD + Receiving TD + Own fumbles recovered for TD ) + 2 *
                    (Two point conversions) + Extra points + 3 * (Field Goals)
                </p>
            </div>
        </div>`

    renderTab: (index) ->
        switch index
            when 0
                @renderGameFormatTab()
            when 1
                @renderRoomTypes()
            when 2
                @renderNflRules()
            when 3
                @renderRosterRequirements()
            when 4
                @renderScoringRules()
            else null

    render: ({}, {tabIndex}) ->
        onClose = -> flux.actions.tap.closeDialog()
        cs = (for i in [0..4]
            cx('rules-dialog__caption', {
                'rules-dialog__caption--active': tabIndex == i
            })
        )

        contentCs = cx("rules-dialog__content", {
            "rules-dialog__content--table": tabIndex == 4
        })

        tab = @renderTab(tabIndex)

        `<section className="rules-dialog">
            <Close onClose={onClose} className="rules-dialog__close-button" />
            <div className="rules-dialog__menu">
                <div className={cs[0]} onTap={this.onMenuTap(0)}>SALARY CAP GAME FORMAT</div>
                <div className={cs[1]} onTap={this.onMenuTap(1)}>COMPETITION ROOM TYPES<br /> AND RULES</div>
                <div className={cs[2]} onTap={this.onMenuTap(2)}>NFL RULES</div>
                <div className={cs[3]} onTap={this.onMenuTap(3)}>ROSTER REQUIREMENTS</div>
                <div className={cs[4]} onTap={this.onMenuTap(4)}>SCORING RULES</div>
            </div>
            <div className={contentCs} ref="dialog">
                {tab}
            </div>
        </section>`


module.exports = NflRulesDialog
