{Btn, Icon} = require '../../lib/ui'


LineupSavedDialog = React.create
    displayName: 'LineupSavedDialog'

    handleClose: ->
        flux.actions.tap.closeDialog()

    render: ({lineup}) ->
        `<section className="lineup-saved">
            <Icon i="user-check-green" className="lineup-saved__icon" />
            <div>YOUR LINE-UP</div>
            <div className="lineup-saved__name">{lineup.name}</div>
            <div>Has been saved!</div>
            <div>You can find it in the "My Line-ups" section</div>
            <Btn mod="blue-base"
                className="lineup-saved__btn"
                onTap={this.handleClose}
            >
                OK
            </Btn>
        </section>`


module.exports = LineupSavedDialog
