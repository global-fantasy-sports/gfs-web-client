_ = require('underscore')
flash = require('../lib/flash')
{eventTracker} = require('../lib/utils')
Immutable = require('immutable')

GameLayout = require('../start/GameLayout')


getSegments = (pathname) ->
    pathname = pathname[1..] if pathname[0] is '/'
    pathname.split('/')


module.exports = React.create(
    displayName: "GameWrapper"

    contextTypes:
        router: React.PropTypes.object

    mixins: [
        flux.Connect("users", (store) ->
            user = store.getCurrentUser()

            if user.count() and (not user.get("country") or not user.get("email"))
                _.defer(flux.actions.state.openFinishRegistrationDialog)

            {user}
        )
        flux.Connect('wizard', (store) ->
            currentSubstep: store.getCurrentSubstep()
            gameType: store.getWizardGameType()
        )
    ]

    componentWillMount: ->
        {aff} = @props.location.query ? {}
        flux.actions.connection.saveAffiliateCode(aff) if aff

    update: ->
        _.defer(=> @forceUpdate())

    componentDidMount: ->
#        if @state.user.get('first_login')
#
#            challengeRequests.once "sync", =>
#                return if not challengeRequests.length
#                req = challengeRequests.first()
#                return @context.router.transitionTo("/challenge/#{req.id}/")
        @checkInitialData()

    componentDidUpdate: ->
        @checkInitialData() unless @state.isTransitioning

    checkInitialData: ->
        if @props.initial
            @handleInitial(@props.initial)
        else if "hfg-init" of localStorage
            logger.log(localStorage["hfg-init"], "Initial data found", "InitialData")
            data = JSON.parse(localStorage["hfg-init"])
            delete localStorage["hfg-init"]
            @handleInitial(data)
        else
            logger.log(null, "No initial data found", "InitialData")

    handleInitial: ({action, prediction, roundNumber, participant}) ->
        switch action
            when "start-hole-by-hole"
                @setState(initial: {prediction, roundNumber, participant})
                _.defer(=> @context.router.transitionTo("/#{SPORT}/hole-by-hole/"))

    onEditGame: (game, gameType) ->
        flux.actions.tap.editGame(game)
        if gameType in['nfl', 'nba']
            gameType = 'salarycap'

        _.defer(=> @context.router.transitionTo("/#{SPORT}/#{gameType}/"))

    onChallenge: (friendsIds, gameType) ->
        @setState(initial: {friendsIds})
        flash(
            text: "Create a game now to challenge your friends"
            alwaysKeep: true
        )
        _.defer(=> @context.router.transitionTo("/#{SPORT}/#{gameType}/"))

    onPlay: (room, opts) ->
        if room
            isBeginner = @state.user.get('isBeginner', true)
            if !isBeginner and room.isForBeginners
                flux.actions.state.openInfoMessage("You can't join this competition since your experience level is too high")
                return
            else
                gameType = room.gameType
                @setState({initial: {roomId: room.id}})
                eventTracker("stakeroom", "#{gameType}-join-game")

                if opts?.fromChallenge
                    text = "You just accepted a challenge request from your friend.
                            Now please proceed and create your game."
                else
                    text = "Competition selected, please select candidate"
                flash(text: text, timeout: 3000)

        else
            gameType = opts.gameType
            @setState({initial: {roomId: null}})

        flux.actions.tap.newGame()
        initialData = {}
        if room
            if SPORT in ['nfl', 'nba']
                initialData = {
                    selectedRoomIds: Immutable.Set([room.id])
                    sportEventId: room.sportEventId
                    isInitialRoom: true
                }
        else
            if SPORT == 'golf'
                flux.actions.state.clearActiveRounds()

        if opts?.fromChallenge
            initialData.challengeRequestRoomId = room.id

        flux.actions.wizard.init(initialData)

        if SPORT == 'golf'
            flux.actions.wizard.setEvent(room.tournamentId, room.roundNumber)

        _.defer(=> @context.router.transitionTo("/#{SPORT}/#{gameType}/"))

    render: ({games, routeParams}, {initial, user, game, guideActive, currentSubstep}) ->
        logger.log(arguments, "GameWrapper render arguments", "GameWrapper")

        currentState = "default"
        segment = getSegments(@props.location.pathname)[1]

        view = React.cloneElement(@props.children, {
            user, games, initial
            onChallenge: @onChallenge
            onEditGame: @onEditGame
            onPlay: @onPlay
        })

        currentState =
            if segment is 'competition' then 'f-competition'
            else if segment in ['tournaments', 'games', 'competitions'] then ''
            else segment

        `<GameLayout {...this.props}
                     currentState={currentState}
                     segment={segment}>
            {view}
        </GameLayout>`
)
