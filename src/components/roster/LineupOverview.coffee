{history} = require('world')
{Titlebar} = require 'lib/ui/Titlebar'
{Btn, Icon, BtnExpand} = require('lib/ui')
PlaceholderImage = require('../../lib/ui/PlaceholderImage')
{cx} = require('lib/utils')
Connect = require('../../flux/connectMixin')
{Input} = require('../../lib/forms')


LineupOverview = React.create
    dislpayName: 'LineupOverview'

    mixins: [
        Connect('lineups', (store) ->
            hoveredId: store.getHoveredLineupId()
            editedId: store.getEditedLineupId()
        )
    ]

    getInitialState: ->
        expanded: false
        editedName: null
        error: null

    componentDidMount: ->
        flux.actions.lineup.finishEditLineupName()

    handleEditLineupHover: ->
        flux.actions.lineup.hoverLineupName(@props.lineup.id)

    handleEditLineupBlur: ->
        flux.actions.lineup.blurLineupName()

    handleEditLineup: ->
        id = @props.lineup.id
        flux.actions.lineup.goToLineupEdit(id)

    handleCreateGame: ->
        if !flux.stores.rooms.isAllStarted()
            flux.actions.roster.cloneLineup(@props.lineup)
            history.pushState(null, "/#{SPORT}/salarycap/select/")

    handleRemoveLineup: ->
        flux.actions.state.openDeleteLineupDialog(@props.lineup)

    handleLineupExpand: ->
        @setState({expanded: !@state.expanded})

    handleTitleClick: ->
        @setState({editedName: @props.lineup.name, error: null})
        flux.actions.lineup.startEditLineupName(@props.lineup.id)

    handleNameChange: (e) ->
        @setState({editedName: e.target.value})

    handleKeyDown: (e) ->
        if e.which == 13
            if e.target.value.trim()
                flux.actions.lineup.editName(@props.lineup.id, e.target.value)
            else
                @setState({error: 'Line-up name cannot be empty'})

        if e.which == 27
            flux.actions.lineup.finishEditLineupName()

    renderPhotos: ->
        @props.lineup.positions.map( (p, i) ->
            cs = "competition-players__img competition-players__img-#{i}"
            photo = p.participant?.smallPhoto || ''
            name = p.participant?.getName?() || ''

            `<div className={cs} key={i}>
                <PlaceholderImage
                    src={photo}
                    placeholder="/static/img/nfl-player-placeholder-small.png"
                    name={name}
                />
            </div>`
        )

    renderPositions: ->
        @props.lineup.positions.map( (p, i) ->
            mod = if i == 8 then 'competition-players__pos--lineup' else ''
            `<div className={"competition-players__pos " + mod} key={i}>
                {p.position}
            </div>`
        )

    renderTitleBlock: ->
        lineup = @props.lineup

        if @state.editedId == lineup.id
            `<div>
                <Input className="lineup-overview__input"
                    value={this.state.editedName}
                    onChange={this.handleNameChange}
                    onKeyDown={this.handleKeyDown}
                />
                {this.state.error &&
                <div className="lineup-overview__error">
                    {this.state.error}
                </div>
                }
            </div>`
        else
            showPencil = @state.hoveredId == lineup.id

            `<div className="lineup-overview__title"
                  onMouseEnter={this.handleEditLineupHover}
                  onMouseLeave={this.handleEditLineupBlur}
                  onTap={this.handleTitleClick}
            >
                {lineup.name || 'NEW LINEUP'}
                &nbsp;&nbsp;&nbsp;
                {showPencil &&
                <Icon i="pencil-blue-small" />
                }
            </div>`

    renderLineupInfo: ->
        {lineup} = @props

        cs = cx('lineup-overview__info', {
            'w-35': !@state.expanded,
            'w-15 lineup-overview__info--expanded': @state.expanded
        })

        if !@state.expanded
            `<div className={cs}>
                {this.renderTitleBlock()}
                <div className="lineup-overview__info-line">
                    <div className="lineup-overview__info-item">
                        <div className="lineup-overview__item-title">
                            LINE-UP BUDGET
                        </div>
                        <div className="lineup-overview__item-value">
                            ${lineup.getBudget()}
                        </div>
                    </div>
                    <div className="lineup-overview__info-item">
                        <div className="lineup-overview__item-title">
                            LINE-UP COST
                        </div>
                        <div className="lineup-overview__item-value">
                            ${lineup.getCost()}
                        </div>
                    </div>
                    <div className="lineup-overview__info-item">
                        <div className="lineup-overview__item-title">
                            AVG.FPPG
                        </div>
                        <div className="lineup-overview__item-value">
                            {lineup.getFPPG().toFixed(2)}
                        </div>
                    </div>
                </div>
            </div>`
        else
            `<div className={cs}>
                <div className="lineup-overview__info-item">
                    <div className="lineup-overview__item-title">
                        AVG.FPPG
                    </div>
                    <div className="lineup-overview__item-value">
                        {lineup.getFPPG().toFixed(2)}
                    </div>
                </div>
            </div>`

    renderAthletes: ->
        `<div className="game-table__players flex-row-stretch w-55">
            <div className="competition-players competition-players--lineup">
                <div className="competition-players__top">
                    <div className="competition-players__img-wrapper">
                        {this.renderPhotos()}
                    </div>
                </div>
                <div className="competition-players__bottom">
                    <div className="competition-players__pos-wrapper">{this.renderPositions()}</div>
                </div>
            </div>
            <BtnExpand onTap={this.handleLineupExpand} />
        </div>`

    renderActionButtons: ->
        cs = cx('lineup-overview__buttons', {
            'w-10': !@state.expanded,
            'w-30': @state.expanded
        })

        roomEnabled = !flux.stores.rooms.isAllStarted()

        `<div className={cs}>
            <Btn mod={"square "}
                 iconBg={true}
                 icon="pencil"
                 onTap={this.handleEditLineup}>
                Edit
            </Btn>

            {this.state.expanded &&
            <Btn mod={"square "}
                 iconBg={true}
                 icon="duplicate"
                 onTap={null}>
                Duplicate
            </Btn>
            }

            {this.state.expanded &&
            <Btn mod={"square "}
                 iconBg={true}
                 icon={roomEnabled ? 'nfl-ball-blue' : 'nfl-ball-gray'}
                 onTap={this.handleCreateGame}>
                Create game
            </Btn>
            }

            {this.state.expanded &&
            <Btn mod={"square text-red"}
                 iconBg={true}
                 icon="cross-circle-red-medium"
                 onTap={this.handleRemoveLineup}>
                Delete
            </Btn>
            }

        </div>`

    render: () ->
        `<section className="lineup-overview">
            {this.renderLineupInfo()}
            {this.renderAthletes()}
            {this.renderActionButtons()}
        </section>`


module.exports = LineupOverview
