{bem} = require('lib/utils')
{POSITIONS, POSITION_NAMES} = require('../const')


EmptyRosterParticipant = React.create
    displayName: 'EmptyRosterParticipant'

    propTypes: {
        position: React.PropTypes.oneOf(POSITIONS).isRequired
    }

    handleTap: (e) ->
        e.preventDefault()
        flux.actions.roster.filterByPosition(@props.position)

    render: ({position}) ->
        itemCls = bem('participant-item__el')

        `<div className='participant-item participant-item--empty' onTap={this.handleTap}>
            <div className={itemCls('empty') + " w-10"}>
                {position}
            </div>
            <div className={itemCls('img', 'empty')  + " w-10"}>
                <img src='/static/img/nfl-anonym.png'/>
            </div>
            <div className={itemCls('role', 'empty')  + " w-30"}>
                Select {POSITION_NAMES[position]}
            </div>
        </div>`


module.exports = EmptyRosterParticipant
