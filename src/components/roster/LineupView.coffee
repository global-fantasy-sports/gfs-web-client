{FlexTable, FlexColumn, SortDirection} = require('react-virtualized')
bem = require('bem-classname')

{Icon} = require('../../lib/ui')
PlayerPhoto = require('../PlayerPhoto')
{formatFPPG, formatSalary} = require('../../lib/format')
{POSITION_NAMES} = require('../const')


class LineupView extends React.Component

    @bem: bem.bind(null, 'wizard-table')

    rowClassName: ({index}) =>
        if index is -1
            LineupView.bem('head')
        else
            participant = @props.lineup.getIn(['positions', index, 'participant'])
            LineupView.bem('row', {
                'empty': not participant
                'filled': participant
            })

    rowGetter: ({index}) =>
        @props.lineup.getIn(['positions', index])

    cellDataGetter: ({rowData, dataKey}) =>
        rowData.getIn(dataKey.split('.'))

    handleRowClick: ({index}) =>
        {onRowClick} = @props
        if onRowClick
            onRowClick(@props.lineup.getIn(['positions', index]))

    renderPhoto: ({rowData}) =>
        if rowData.isOpen()
            `<img src="/static/img/nfl-anonym.png" />`
        else
            `<PlayerPhoto player={rowData.participant} size={60} />`

    renderName: ({rowData, cellData}) ->
        if rowData.isOpen()
            "Select #{POSITION_NAMES[rowData.position]}"
        else
            cellData.getName()

    renderFPPG: ({rowData, cellData}) ->
        formatFPPG(cellData) unless rowData.isOpen()

    renderOPRK: ({rowData, cellData}) ->
        cellData or 'N/A'

    renderSalary: ({rowData, cellData}) ->
        formatSalary(cellData) unless rowData.isOpen()

    renderFavorite: ({cellData, rowData}) =>
        return null if rowData.isOpen()

        handleClick = (e) ->
            e.stopPropagation()
            flux.actions.data.toggleFavorite(rowData.participant.playerId)

        `<Icon
            className="clickable"
            i={cellData ? 'star-blue-big' : 'star-gray-big'}
            onClick={handleClick}
        />`

    renderSelected: ({cellData}) =>
        return null unless cellData

        handleClick = (e) ->
            e.stopPropagation()
            flux.actions.roster.toggleParticipant(cellData)

        `<Icon className="clickable" i="minus" onClick={handleClick} />`

    render: () ->
        {lineup, width, height} = @props

        `<FlexTable
            width={width}
            height={height}
            className="wizard-table"
            headerClassName={LineupView.bem('head-cell')}
            headerHeight={40}
            rowHeight={60}
            rowClassName={this.rowClassName}
            rowCount={lineup.positions.size}
            rowGetter={this.rowGetter}
            onRowClick={this.handleRowClick}
        >
            <FlexColumn
                disableSorting
                label="POS"
                dataKey="position"
                width={30}
                flexGrow={0}
                flexShrink={0}
                className={LineupView.bem('row-cell')}
                cellDataGetter={this.cellDataGetter}
            />
            <FlexColumn
                dataKey="participant.isFavorite"
                disableSorting
                width={20}
                flexGrow={0}
                flexShrink={0}
                className={LineupView.bem('row-cell')}
                cellRenderer={this.renderFavorite}
                cellDataGetter={this.cellDataGetter}
            />
            <FlexColumn
                dataKey="participant.photo"
                disableSorting
                width={70}
                flexGrow={0}
                flexShrink={0}
                headerClassName={LineupView.bem('head-cell', ['no-margin'])}
                className={LineupView.bem('row-cell', ['no-margin'])}
                cellRenderer={this.renderPhoto}
                cellDataGetter={this.cellDataGetter}
            />
            <FlexColumn
                label="NAME"
                disableSorting
                dataKey="participant"
                cellRenderer={this.renderName}
                width={90}
                flexGrow={2}
                className={LineupView.bem('row-cell')}
                cellDataGetter={this.cellDataGetter}
            />
            <FlexColumn
                label="OPRK"
                disableSorting
                dataKey="participant.oprk"
                width={60}
                cellRenderer={this.renderOPRK}
                className={LineupView.bem('row-cell')}
                cellDataGetter={this.cellDataGetter}
            />
            <FlexColumn
                label="FPPG"
                disableSorting
                dataKey="participant.FPPG"
                width={60}
                cellRenderer={this.renderFPPG}
                className={LineupView.bem('row-cell')}
                cellDataGetter={this.cellDataGetter}
            />
            <FlexColumn
                label="SALARY"
                disableSorting
                dataKey="participant.salary"
                width={60}
                cellRenderer={this.renderSalary}
                className={LineupView.bem('row-cell')}
                cellDataGetter={this.cellDataGetter}
            />
            <FlexColumn
                dataKey="participant.id"
                width={32}
                flexGrow={0}
                flexShrink={0}
                disableSorting
                cellRenderer={this.renderSelected}
                className={LineupView.bem('row-cell')}
                cellDataGetter={this.cellDataGetter}
            />
        </FlexTable>`


module.exports = LineupView
