{Motion, spring} = require('react-motion')
bem = require('bem-classname')

{Btn} = require('../../lib/ui')
{cx} = require('../../lib/utils')
{formatSalary, formatFPPG} = require('../../lib/format')

RadialProgressChart = require('../charts/RadialProgressChart')


Value = ({label, children, error, small}) ->
    mods = {error, small}
    `<div className={Value.bem(mods)}>
        <span className={Value.bem('value', mods)}>{children}</span>
        <span className={Value.bem('label', mods)}>{label}</span>
    </div>`

Value.bem = bem.bind(null, 'roster-info-item')


class RosterInfo extends React.Component
    render: () ->
        {lineup, isSaved, onImport, onSave, toolbarContent} = @props

        isValid = lineup.isValid()
        totalCount = lineup.positions.size
        count = lineup.positions.count((p) -> not p.isOpen())
        maxBudget = flux.stores.roster.getRosterBudget()

        style = {
            avgFPPG: spring(lineup.getFPPG(), {precision: 0.1})
            budget: spring(lineup.getBudget(), {precision: 1})
        }

        React.createElement(Motion, {style}, ({avgFPPG, budget}) ->
            budget = Math.round(budget)

            `<div className="roster__info">
                <RadialProgressChart
                    value={budget}
                    max={maxBudget}
                    label={
                        <Value label="BUDGET" small error={budget < 0}>
                            {formatSalary(budget)}
                        </Value>
                    }
                />
                <Value label="PLAYERS">
                    {count}<em className="total">/{totalCount}</em>
                </Value>
                <Value label="AVG.FPPG">
                    {formatFPPG(avgFPPG)}
                </Value>
                <div className="roster__toolbar">
                    {toolbarContent}
                </div>
            </div>`
        )


module.exports = RosterInfo
