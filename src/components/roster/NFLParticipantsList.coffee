_ = require('underscore')
bem = require('bem-classname')
{FlexColumn} = require('react-virtualized')

{Icon} = require('../../lib/ui')
{cx} = require('../../lib/utils')
{formatFPPG, formatSalary} = require('../../lib/format')

ParticipantsList = require('./ParticipantsList')
Tooltip = require('../tooltip/Tooltip')


TeamCountBadge = ({count}) ->
    return `<script />` unless count > 0
    `<Tooltip viewMod="texted" text="You can draft max 4 players from one team">
        <div className={bem('team-count-badge', {'full': count === 4})}>
            {count}/4
        </div>
    </Tooltip>`


class NFLParticipantsList extends React.Component

    @propTypes: {
        selected: React.PropTypes.object.isRequired
        openPositions: React.PropTypes.object.isRequired
    }

    rowGetter: ({index}) =>
        {selected, openPositions, participants, budget, disabledTeams} = @props
        participant = participants.get(index)

        return {
            'id': participant.id
            'playerId': participant.playerId
            'team': participant.team
            'name': participant.getName()
            'position': participant.position
            'isFavorite': participant.isFavorite
            'salary': participant.salary
            'FPPG': formatFPPG(participant.FPPG)
            'oprk': participant.oprk or 'N/A'
            'isSelected': selected.has(participant.playerId)
            'isOpen': openPositions.has(participant.position) and !disabledTeams.contains(participant.team)
        }

    rowClassName: (index) =>
        {selected, participants, disabledTeams, openPositions} = @props
        unless index is -1
            participant = participants.get(index)
            if selected.has(participant?.playerId)
                return "selected"
            unless openPositions.has(participant.position) and !disabledTeams.contains(participant.team)
                "disabled"

    renderFavorite: ({cellData, rowData}) =>
        return null if rowData.position == 'DST'

        handleClick = (e) ->
            e.stopPropagation()
            flux.actions.data.toggleFavorite(rowData.playerId)

        `<Icon
            className="clickable"
            i={cellData ? 'star-blue-big' : 'star-gray-big'}
            onClick={handleClick}
        />`

    renderSelected: ({cellData, rowData}) =>
        {isSelected, isOpen} = rowData
        handleClick = (e) ->
            e.stopPropagation()
            if isSelected or isOpen
                flux.actions.roster.toggleParticipant(rowData.id)

        `<Icon
            className={cx({'clickable': isOpen || isSelected})}
            i={cellData ? 'checked' : (isOpen ? 'plus' : 'plus-gray')}
            onClick={handleClick}
        />`

    renderSalary: ({cellData}) =>
        className = if cellData > @props.budget then 'wizard-table__salary-red' else 'black'
        `<span className={className}>{formatSalary(cellData)}</span>`

    renderTeam: ({cellData}) =>
        count = @props.selectedTeams.get(cellData)?.count()
        `<div>{cellData}<TeamCountBadge count={count} /></div>`

    render: () ->
        `<ParticipantsList
            {...this.props}
            rowClassName={this.rowClassName}
            rowGetter={this.rowGetter}
        >
            <FlexColumn
                label="POS"
                dataKey="position"
                width={30}
            />
            <FlexColumn
                disableSorting
                dataKey="isFavorite"
                width={20}
                cellRenderer={this.renderFavorite}
            />
            <FlexColumn
                label="NAME"
                dataKey="name"
                width={80}
                flexGrow={2}
            />
            <FlexColumn
                label="TEAM"
                dataKey="team"
                width={80}
                cellRenderer={this.renderTeam}
            />
            <FlexColumn
                label="OPRK"
                dataKey="oprk"
                width={60}
            />
            <FlexColumn
                label="FPPG"
                dataKey="FPPG"
                width={60}
            />
            <FlexColumn
                label="SALARY"
                dataKey="salary"
                width={70}
                cellRenderer={this.renderSalary}
            />
            <FlexColumn
                disableSorting
                dataKey="isSelected"
                width={32}
                cellRenderer={this.renderSelected}
            />
        </ParticipantsList>`


module.exports = NFLParticipantsList
