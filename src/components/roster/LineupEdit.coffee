{history} = require('world')
bemClassname = require('bem-classname')
{SortDirection, AutoSizer} = require('react-virtualized')

Connect = require('../../flux/connectMixin')
ParticipantFilter = require('../ParticipantFilter')
ParticipantSearch = require('../ParticipantSearch')

NFLParticipantsList = require('./NFLParticipantsList')
RosterInfo = require('./RosterInfo')
LineupView = require('./LineupView')
{Btn, Icon} = require('../../lib/ui')

{POSITIONS} = require '../const'


bem = bemClassname.bind(null, 'roster')


LineupEdit = React.create
    displayName: 'LineupEdit'

    mixins: [
        Connect('roster', (store) -> store.getRoster(true))
    ]

    componentDidMount: ->
        flux.actions.lineup.finishEditLineupName()

    handlePositionChange: (value) ->
        flux.actions.roster.filterByPosition(value)

    handleSearchChange: (value) ->
        flux.actions.roster.search(value)

    handleFiltersReset: ->
        flux.actions.roster.clearFilters()

    handleImportLineup: ->

    handleLineupRowClick: (rowRecord) ->
        flux.actions.roster.filterByPosition(rowRecord.position)

    handleRemoveLineup: ->
        if @state.lineup.id
            flux.actions.state.openDeleteLineupDialog(@state.lineup)

    handleCreateGame: ->
        if !flux.stores.rooms.isAllStarted()
            flux.actions.roster.cloneLineup(@state.lineup)
            history.pushState(null, "/#{SPORT}/salarycap/select/")

    renderActionButtons: ->
        roomEnabled = !flux.stores.rooms.isAllStarted()
        notNew = !!@state.lineup.id

        [
            `<Btn
                mod={"tiny " + (notNew ? "transparent-black" : "transparent-gray")}
                showIcon={true}
                icon={notNew ? "cross-circle-black" : "cross-circle-gray"}
                onTap={this.handleRemoveLineup}
            >
                Delete
            </Btn>`
            `<Btn
                mod={"tiny " + (roomEnabled ? "transparent-black" : "transparent-gray")}
                showIcon={true}
                icon={roomEnabled ? "nfl-ball-black" : "nfl-ball-gray"}
                onTap={this.handleCreateGame}
            >
                Create game
            </Btn>`
        ]

    render: ({}, {position, search, budget, lineup, sorting, reversed, selected, open, participants,
            selectedTeams, disabledTeams}) ->
        lineupHeight = lineup.positions.size * 60 + 40


        `<section className={bem()}>
            <div className={bem('row')}>
                <div className={bem('left')}>
                    <div className="wizard-filter wizard-filter--tall">
                        <ParticipantFilter
                            className="wizard-filter__buttons"
                            active={position}
                            values={POSITIONS}
                            onChange={this.handlePositionChange}
                        />
                        <ParticipantSearch
                            className="wizard-filter__search"
                            value={search}
                            onChange={this.handleSearchChange}
                        />
                    </div>

                    <NFLParticipantsList
                        width={588}
                        height={lineupHeight}
                        sortBy={sorting}
                        sortDirection={SortDirection[reversed ? 'DESC' : 'ASC']}
                        selected={selected}
                        openPositions={open}
                        participants={participants}
                        disabledTeams={disabledTeams}
                        selectedTeams={selectedTeams}
                        budget={budget}
                        onResetFilters={this.handleFiltersReset}
                    />
                </div>
                <div className={bem('right')}>
                    <RosterInfo
                        lineup={lineup}
                        onSave={this.handleSaveLineup}
                        onImport={this.handleImportLineup}
                        toolbarContent={this.renderActionButtons()}
                    />
                    <LineupView
                        width={588}
                        height={lineupHeight}
                        lineup={lineup}
                        onRowClick={this.handleLineupRowClick}
                    />
                </div>
            </div>
        </section>`


module.exports = LineupEdit
