{history} = require('world')
LineupEdit = require('./LineupEdit')
{Btn, Icon} = require('../../lib/ui')
Connect = require '../../flux/connectMixin'
{Input} = require('../../lib/forms')


RosterContainer = React.create
    displayName: 'RosterContainer'

    mixins: [
        Connect('lineups', (store, props) ->
            lineup: store.getLineup(+props.id)
            hoveredId: store.getHoveredLineupId()
            editedId: store.getEditedLineupId()
        )
        Connect('roster', (store) ->
            rosterLineup: store.getLineup()
        )
    ]

    getInitialState: ->
        editedName: null
        error: null

    componentWillMount: ->
        flux.actions.data.fetchLineups()

    componentDidMount: ->
        flux.actions.lineup.finishEditLineupName()
        if @state.lineup && !@state.lineup.isEmpty()
            flux.actions.roster.cloneLineup(@state.lineup, {edit: true, isAll: true})

    componentWillUpdate: (props, state) ->
        if state.lineup.id && state.lineup != @state.lineup && !state.lineup.isEmpty()
            flux.actions.roster.cloneLineup(state.lineup, {edit: true, isAll: true})

    handleSaveLineup: ->
        flux.actions.lineup.onSaveLineup(@state.rosterLineup)

    handleExit: ->
        flux.actions.roster.clear()
        history.pushState(null, "/#{SPORT}/lineups/")

    handleEditLineupHover: ->
        flux.actions.lineup.hoverLineupName(@state.lineup.id || 'new')

    handleEditLineupBlur: ->
        flux.actions.lineup.blurLineupName()

    handleTitleClick: ->
        @setState({editedName: @state.rosterLineup.name, error: null})
        flux.actions.lineup.startEditLineupName(@state.lineup.id || 'new')

    handleNameChange: (e) ->
        @setState({editedName: e.target.value})

    handleKeyDown: (e) ->
        if e.which == 13
            if e.target.value.trim()
                lineup = @state.rosterLineup.set('name', e.target.value)
                flux.actions.roster.cloneLineup(lineup, {edit: true, isAll: true})
                flux.actions.lineup.finishEditLineupName()
            else
                @setState({error: "Line-up name cannot be empty"})

        if e.which == 27
            flux.actions.lineup.finishEditLineupName()

    renderTitleBlock: ->
        lineup = @state.lineup
        roster = @state.rosterLineup

        if @state.editedId == 'new' || (lineup.id && @state.editedId == lineup.id)
            `<div>
                <Input className="roster-container__input"
                       value={this.state.editedName}
                       onChange={this.handleNameChange}
                       onKeyDown={this.handleKeyDown}
                />
                {this.state.error &&
                <div className="roster-container__error">
                    {this.state.error}
                </div>
                }
            </div>`
        else
            showPencil = @state.hoveredId =='new' || (lineup.id && @state.hoveredId == lineup.id)

            `<div className="roster-container__title"
                  onMouseEnter={this.handleEditLineupHover}
                  onMouseLeave={this.handleEditLineupBlur}
                  onTap={this.handleTitleClick}
            >
                {roster.name}
                &nbsp;&nbsp;&nbsp;
                {showPencil &&
                <Icon i="pencil-blue-small" />
                }
            </div>`

    render: ({}, {lineup}) ->
        `<section className="roster-container">
            <div className="roster-container__header">
                {this.renderTitleBlock()}
                <div className="roster-container__buttons">
                    <Btn mod="white"
                        onTap={this.handleExit}>
                        EXIT
                    </Btn>
                    <Btn mod="white"
                        onTap={this.handleSaveLineup}>
                        SAVE LINE-UP
                    </Btn>
                </div>
            </div>
            <LineupEdit />
        </section>`


module.exports = RosterContainer
