_ = require('underscore')
{FlexColumn} = require('react-virtualized')

{Icon} = require('../../lib/ui')
{Flag} = require('../../lib/ui/Flag')
{cx} = require('../../lib/utils')
{formatFPPG, formatSalary} = require('../../lib/format')

ParticipantsList = require('./ParticipantsList')


class GolfParticipantsList extends React.Component

    @propTypes: {
        selected: React.PropTypes.object.isRequired
        open: React.PropTypes.bool.isRequired
    }

    rowGetter: ({index}) =>
        {selected, open, participants} = @props
        participant = participants.get(index)

        return {
            'id': participant.id
            'playerId': participant.golferId
            'name': participant.getName()
            'country': participant.country
            'rank': participant.rank
            'toPar': participant.lastScore
            'avgFPPR': formatFPPG(participant.avgFPPR)
            'isSelected': selected.has(participant.id)
            'isOpen': open
        }

    renderCountry: ({cellData}) ->
        `<Flag country={cellData} />`

    renderSelected: ({cellData, rowData}) =>
        {isSelected, isOpen} = rowData
        handleClick = (e) ->
            e.stopPropagation()
            if isSelected or isOpen
                flux.actions.roster.toggleParticipant(rowData.id)

        `<Icon
            className={cx({'clickable': isOpen || isSelected})}
            i={cellData ? 'checked' : (isOpen ? 'plus' : 'plus-gray')}
            onClick={handleClick}
        />`

    render: () ->
        `<ParticipantsList
            {...this.props}
            rowGetter={this.rowGetter}
        >
            <FlexColumn
                disableSorting
                dataKey="country"
                width={70}
                cellRenderer={this.renderCountry}
            />
            <FlexColumn label="NAME" dataKey="name" width={120} flexGrow={1} />
            <FlexColumn label="WORLD RANK" dataKey="rank" width={90} />
            <FlexColumn label="LAST TO-PAR" dataKey="toPar" width={90} />
            <FlexColumn label="FPPR" dataKey="avgFPPR" width={60} />
            <FlexColumn
                disableSorting
                dataKey="isSelected"
                width={32}
                cellRenderer={this.renderSelected}
            />
        </ParticipantsList>`


module.exports = GolfParticipantsList
