{Btn, Icon} = require('lib/ui')
{cx} = require('lib/utils')


module.exports = React.create(
    displayName: "ImportLineups"

    onDownloadTemplate: ->
        flux.actions.lineup.goToDownloadTemplate()

    onUpload: ->
        fileInput = @refs.upload

        fileInput.addEventListener "change", (e) ->
            file = @files[0]
            flux.actions.lineup.uploadLineupsFile(file)

        fileInput.click()
        

    render: ->
        `<div className="import-lineup">
          <div className="empty-block-notice">
              <Btn mod="red-linear x-small offset-sides"
                   onTap={this.onDownloadTemplate}>Download Template</Btn>
              <Btn mod="red-linear x-small offset-sides"
                   onTap={this.onUpload}>Upload lineups in csv</Btn>
                <input
                    ref="upload"
                    accept="text/csv"
                    type="file"
                    style={{display: "none"}}/>
            </div>
        </div>`
)