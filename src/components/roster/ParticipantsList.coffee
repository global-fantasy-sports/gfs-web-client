{FlexTable, FlexColumn, SortDirection} = require('react-virtualized')
bem = require('bem-classname')
_ = require('underscore')

{cx} = require('../../lib/utils')
{Btn} = require('../../lib/ui')



class ParticipantsList extends React.Component

    @propTypes: {
        participants: React.PropTypes.object.isRequired
        children: React.PropTypes.array.isRequired
        width: React.PropTypes.number.isRequired
        height: React.PropTypes.number.isRequired
        headerHeight: React.PropTypes.number
        rowHeight: React.PropTypes.number
        rowClassName: React.PropTypes.func
        rowGetter: React.PropTypes.func.isRequired
        sortBy: React.PropTypes.string.isRequired
        sortDirection: React.PropTypes.any.isRequired
        onResetFilters: React.PropTypes.func
    }

    @defaultProps: {
        headerHeight: 40
        rowHeight: 60
        rowClassName: _.noop
        oResetFilters: _.noop
    }

    @bem: bem.bind(null, 'wizard-table')

    rowClassName: ({index}) =>
        block = if index is -1 then 'head' else 'row'
        ParticipantsList.bem(block, @props.rowClassName(index))

    handleSort: ({sortBy}) ->
        flux.actions.roster.sort(sortBy)

    handleRowClick: ({index}) =>
        id = @props.participants.getIn([index, 'id'])
        if id
            flux.actions.state.openPlayerDetailsDialog(id)

    renderNoRows: () =>
        {onResetFilters} = @props
        `<div>
            <p>No participants match filter criteria</p>
            <Btn mod="small gray" onClick={onResetFilters}>Reset filters</Btn>
        </div>`


    render: () ->
        {children, participants, sortBy, sortDirection, width, height, headerHeight, rowHeight, rowGetter} = @props

        renderColumn = (column) ->
            React.cloneElement(column, {
                className: ParticipantsList.bem('row-cell')
            })

        `<FlexTable
            width={width}
            height={height}
            className={ParticipantsList.bem()}
            headerClassName={ParticipantsList.bem('head-cell')}
            headerHeight={headerHeight}
            rowClassName={this.rowClassName}
            rowCount={participants.size}
            rowGetter={rowGetter}
            rowHeight={rowHeight}
            sort={this.handleSort}
            sortBy={sortBy}
            sortDirection={sortDirection}
            onRowClick={this.handleRowClick}
            noRowsRenderer={this.renderNoRows}
        >
            {React.Children.map(children, renderColumn)}
        </FlexTable>`


module.exports = ParticipantsList
