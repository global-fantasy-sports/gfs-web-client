immutable = require('immutable')
{Btn} = require('lib/ui')
{cx} = require('lib/utils')
{Input} = require('lib/forms')
LineupOverview = require('./LineupOverview')

module.exports = React.create(
    displayName: "Lineups"

    mixins: [flux.Connect('lineups', (store) ->
        lineups: store.getLineups()
        editedLineupId: store.getEditedLineupId()
    )]

    getInitialState: ->
        hovered: 0

    componentWillMount: ->
        flux.actions.data.fetchLineups()

    handleEditLineupName: (id) ->
        ->
            flux.actions.lineup.startEditLineupName(id)

    onLineupNameEdited: (id) ->
        (newName) ->
            flux.actions.lineup.editName(id, newName)

    onCreateLineup: ->
        flux.actions.lineup.goToLineupEdit()

    onImportLineups: ->
        flux.actions.lineup.goToImportLineups()

    onEditLineup: (id) ->
        flux.actions.lineup.goToLineupEdit(id)

    onHover: (i) ->
        =>
            @setState({hovered: i})

    onBlur: ->
        @setState({hovered: 0})

    renderButtons: () ->
        `<div className="lineups__buttons">
            <Btn
                mod="small white inline-content"
                showIcon={true}
                icon={'plus-nocircle'}
                onTap={this.onCreateLineup}
            >
                &nbsp;CREATE
            </Btn>
            <Btn
                mod="small white inline-content"
                showIcon={true}
                icon={'import'}
                onTap={this.onImportLineups}
            >
                &nbsp;IMPORT
            </Btn>
        </div>`

    renderEmptyBlocks: (lineupBlocks = immutable.List()) ->
        blocks = [lineupBlocks.toArray()...]
        onHover = @onHover
        onBlur = @onBlur
        onCreateLineup = @onCreateLineup
        hovered = @state.hovered

        if lineupBlocks.count() < 7
            for i in [lineupBlocks.count()..6]
                blocks.push(`
                    <div className="lineup-overview lineup-overview--empty"
                         onMouseEnter={onHover(i)}
                         onMouseLeave={onBlur}
                         onClick={onCreateLineup}>
                        {hovered === i && !lineupBlocks.count() &&
                            <span className="lineup-overview__empty-text">
                                CREATE YOUR FIRST LINE-UP!
                            </span>
                        }
                    </div>
                `)

        blocks.push(`<br />`)

        blocks

    render: ({}, {styleLineupContent, lineups, hoveredId, editedLineupId}) ->
        if lineups.count()
            lineupBlocks = lineups.map (lineup, i) ->
                `<LineupOverview key={i}
                    lineup={lineup}
                />`

        lineupBlocks = @renderEmptyBlocks(lineupBlocks)

        `<div className="lineups">
            {this.renderButtons()}
            {lineupBlocks}
        </div>`
)
