Immutable = require('immutable')
_ = require('underscore')
{cx} = require('../../lib/utils')
Connect = require('../../flux/connectMixin')
{Btn, Icon} = require('../../lib/ui')
SearchView = require('./SearchView')
{eventTracker} = require('lib/utils')
FriendView = require('./FriendView')


RENDER_BATCH_SIZE = 50


class SortingCell extends React.Component

    handleClick: (e) =>
        e.preventDefault()
        @props.onClick(@props.name)

    render: () ->
        {name, label, sorting, reversed} = @props
        active = name is sorting
        `<div className="friends__head-item" onClick={this.handleClick}>
            <span>{label}</span>
            {active && <Icon i={reversed ? 'arrow-sort-rev' : 'arrow-sort'} />}
        </div>`


class FilterBtn extends React.Component

    handleClick: (e) =>
        e.preventDefault()
        @props.onClick(@props.name)

    render: () ->
        {name, label, filter} = @props
        mods = cx('offset-sides blue-base', {'blue-base-active': name is filter})
        `<Btn mod={mods} onClick={this.handleClick}>{label}</Btn>`

module.exports = React.create(
    displayName: "FriendsList"

    mixins: [
        Connect('wizard', (store) -> {
            challenged: store.getListOfFriendsToChallenge()
        })
    ]

    getInitialState: ->
        invited: new Immutable.Set()
        rendered: 0

    componentDidMount: ->
        @checkRendered()

    componentDidUpdate: ->
        @checkRendered()

    componentDidReceiveProps: ({friends}) ->
        if @props.friends isnt friends
            @setState({rendered: 0})

    checkRendered: ->
        {rendered} = @state
        {friends} = @props
        if rendered < friends.size
            rendered += RENDER_BATCH_SIZE
            _.defer(() =>
                @setState({rendered: Math.min(friends.size, rendered)})
            )

    handleSearch: (search) ->
        flux.actions.facebook.search(search)

    handleSort: (sortingField) ->
        flux.actions.facebook.sort(sortingField)

    handleFilter: (filter) ->
        flux.actions.facebook.filter(filter)

    toggleChallenge: (id) ->
        (e) =>
            eventTracker('buttons', 'challenged')
            flux.actions.wizard.toggleFriendInChallengeList(id)

    invite: (id) ->
        (e) =>
            eventTracker('buttons', 'invited')
            flux.actions.facebook.invite(id).then(
                () => @setState({invited: @state.invited.add(id)})
                (err) => logger.log(err, 'user invite', 'FriendsListInvite')
            )

    renderFriends: ({friends}, {challenged, invited, rendered}) ->
        friends.slice(0, rendered).toArray().map((friend) =>
            selectedInvited = invited.has(friend.id)
            selectedChallenged = challenged.has(friend.id)

            onTapInvite = if selectedInvited then _.noop else @invite(friend.id)
            onTapChallenge = @toggleChallenge(friend.id)

            if friend.registered
                `<FriendView key={friend.id} info={friend} onTap={onTapChallenge}>
                    <div className="friends__button">
                        {selectedChallenged
                            ? <Icon i="checkbox-checked" onTap={onTapChallenge} />
                            : <Icon i="checkbox" onTap={onTapChallenge} />
                        }
                    </div>
                </FriendView>`
            else
                `<FriendView key={friend.id} info={friend} onTap={onTapInvite}>
                    <div className="friends__button">
                        {selectedInvited
                            ? <span>Invitation sent</span>
                            : <Btn mod="red" onTap={onTapInvite}>Invite</Btn>
                        }
                    </div>
                </FriendView>`
        )

    render: () ->
        {filter, search, sorting, reversed} = @props

        `<div className="friends">
            <div className="friends__filters">
                <div className="w-40">
                    <FilterBtn
                        name={null}
                        filter={filter}
                        label="All Friends"
                        onClick={this.handleFilter}
                    />
                    <FilterBtn
                        name="registered"
                        filter={filter}
                        label="GFS Friends"
                        onClick={this.handleFilter}
                    />
                    <FilterBtn
                        name="notRegistered"
                        filter={filter}
                        label="FB Friends"
                        onClick={this.handleFilter}
                    />
                </div>
                <div className="friends__search w-30">
                    <SearchView
                        value={search}
                        onChange={this.handleSearch}
                    />
                </div>
            </div>
            <div className="friends__head">
                <div className="w-60">
                    <SortingCell
                        name="name"
                        label="Name"
                        sorting={sorting}
                        reversed={reversed}
                        onClick={this.handleSort}
                    />
                </div>
                <div className="w-40">
                    <SortingCell
                        name="registered"
                        label="Status"
                        sorting={sorting}
                        reversed={reversed}
                        onClick={this.handleSort}
                    />
                </div>
            </div>
            <div className="friends__row">
                {this.renderFriends(this.props, this.state)}
            </div>
        </div>`
)
