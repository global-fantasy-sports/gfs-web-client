_ = require 'underscore'

{Icon} = require '../../lib/ui'
{Input} = require '../../lib/forms'

module.exports = class SearchView extends React.Component

    @propTypes: {
        value: React.PropTypes.string.isRequired
        onChange: React.PropTypes.func.isRequired
    }

    constructor: (props)->
        super(props)
        @state = {value: props.value}

        @notifyChange = _.debounce(
            => @props.onChange(@state.value)
            200
        )

    componentDidUpdate: ({value})->
        if value isnt @props.value
            @setState({value: @props.value})

    handleChange: (event)=>
        @setState({value: event.target.value})
        @notifyChange(event.target.value)

    render: ->
        `<div>
            <Input placeholder="Search by Name"
                   value={this.state.value}
                   onChange={this.handleChange}/>
        </div>`
