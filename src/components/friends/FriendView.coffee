{Icon} = require('lib/ui')

module.exports = React.create
    displayName: 'FriendView'

    propTypes: {
        info: React.PropTypes.shape({
            registered: React.PropTypes.bool
            name: React.PropTypes.string
            picture: React.PropTypes.string
        }).isRequired
        children: React.PropTypes.node.isRequired
    }

    render: ({info, children})->
        `<section className="friends__item">
            <section className="f-63">
                <div className="friends__info">
                    <div className="friends__img">
                        <img width="80" height="80" src={info.picture}/>
                    </div>
                    <div className="friends__name">
                        {info.name}
                    </div>
                </div>
            </section>
            <section className="f-22">
                <div className="friends__register">
                    <span>
                        {info.registered ? 'Global Fantasy Sports' : <Icon i="fb-blue"/>}
                    </span>
                    {!info.registered &&
                    <span className="not-registered">
                        not yet registered
                    </span>
                    }
                </div>
            </section>
            <section className="f-15">
                {children}
            </section>
        </section>`
