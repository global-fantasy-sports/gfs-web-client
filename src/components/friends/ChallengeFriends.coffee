{Icon} = require('../../lib/ui')
Connect = require('../../flux/connectMixin')
{STATUS} = require('../../flux/common/stores/FacebookStore')
Preloader = require('../../lib/ui/Preloader')
FriendsList = require('./FriendsList')


module.exports = React.create(
    displayName: 'ChallengeFriends'

    mixins: [
        Connect('facebook', (store) -> store.getFilteredFriends())
    ]

    propTypes: {
        onNext: React.PropTypes.func.isRequired
    }

    getInitialState: ->
        fbIds: []
        challenged: null
        invited: null

    componentDidMount: () ->
        setTimeout(flux.actions.facebook.fetchFriends)

    handleLogin: ->
        flux.actions.facebook.login()

    onNext: ->
        @props.onNext()

    renderView: ->
        {status, friends, error, filter, search, sorting, reversed} = @state

        switch status
            when STATUS.ERROR
                `<section className="connect-facebook">
                    <div className="connect-facebook__row">
                        <p className="connect-facebook__text">
                            {error || 'Oops. Something went wrong.'}
                        </p>
                    </div>
                </section>`
            when STATUS.BUSY
                `<section className="connect-facebook">
                    <div className="connect-facebook__row">
                        <Preloader mod="dotted" />
                    </div>
                </section>`
            when STATUS.DECLINED
                `<section className="connect-facebook">
                    <div className="connect-facebook__row">
                        <p className="connect-facebook__text">
                            You have declined request so we cannot show you
                            anyone.
                        </p>
                    </div>
                </section>`
            when STATUS.NOT_LOGGED_IN
                `<section className="connect-facebook">
                    <div className="connect-facebook__row">
                        <p className="connect-facebook__text">
                            You're all alone here!
                        </p>
                        <p className="connect-facebook__text">
                            To get list of your friends please login to
                            Facebook.
                        </p>
                    </div>
                    <div className="connect-facebook__row">
                        <button
                            className="btn connect-facebook__button"
                            onTap={this.handleLogin}
                        >
                            <span className="icon-wrap">
                                <Icon i="fb-white" />
                            </span>
                            <span className="label">Login</span>
                        </button>
                    </div>
                </section>`
            else
                if not friends.size
                    `<section className="connect-facebook">
                        <div className="connect-facebook__row">
                            <p className="connect-facebook__text">
                                We could not fetch your friends from Facebook..
                            </p>
                        </div>
                    </section>`
                else
                    `<FriendsList
                        friends={friends}
                        filter={filter}
                        search={search}
                        sorting={sorting}
                        reversed={reversed}
                    />`

    render: ->
        `<section className="play-with-friends">
            <div className="play-with-friends__content">
                {this.renderView()}
            </div>
        </section>`
)
