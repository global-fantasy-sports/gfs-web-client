{Input} = require 'lib/forms.coffee'
{Btn} = require 'lib/ui.coffee'
xhr = require 'lib/xhr'
flash = require '../../lib/flash'
LinkedStateMixin = require('react-addons-linked-state-mixin')


exports.IdologyVerificationForm = React.create

    mixins: [LinkedStateMixin]

    getInitialState: ->
        user = flux.stores.users.getCurrentUser()
        nameArray = user.get('name').split(' ')
        firstName = nameArray[0]
        lastName = nameArray[nameArray.length - 1]
        {
        firstName
        lastName
        address: user.get('address', '')
        city: user.get('city', '')
        state: user.get('state', '')
        zip: user.get('zip', '')
        user
        error: ''
        }

    sendRequest: ->
        {firstName, lastName, address, city, state, zip} = @state
        xhr
            url: '/idology/'
            method: 'post'
            data: JSON.stringify({
                firstName: firstName or ''
                lastName: lastName or ''
                address: address or ''
                city: city or ''
                state: state or ''
                zip: zip or ''})
            responseType: 'json'
            callback: (err, data) =>
                if err
                    return flash
                        type: "error"
                        text: err
                if data.errors
                    text = _.map(data.errors, (error, field) -> "#{field}: #{error}")
                    return flash({
                        type: "error"
                        text: text
                    })
                if data.success == true
                    flux.actions.data.setCurrentUser({verified: true})
                    @setState({success: true})
                    setTimeout(
                        => @props.onCloseIdology(true)(),
                        1500)
                else if data.success == false
                    @setState(error: data.message)
                else if data.questions
                    @setState({questions: data.questions})

    render: ->
        onCancel = @props.onCloseIdology(false)
        if @state.success
            body = `<div className="idology__title">Congratulations! Your account is confirmed successfully.</div>`
        else if @state.questions
            body = `<div>
                <div className="idology__title">Please, answer additional {this.state.questions.length > 1 ? "questions" : "question"}.</div>
                <div className="idology__request">
                    Questions
                </div>
            </div>`
        else
            body = `<div>
            <div className="idology__title">Please, fill the form to confirm that you are a real person.</div>
            {this.state.error ? <div className="error">{this.state.error}</div> : null}
            <div className="idology__request">
                <div className="idology__group"><Input label="First name" name="firstName" valueLink={this.linkState('firstName')} /></div>
                <div className="idology__group"><Input label="Last name" name="lastName" valueLink={this.linkState('lastName')} /></div>
                <div className="idology__group"><Input label="Address" name="address" valueLink={this.linkState('address')} /></div>
                <div className="idology__group"><Input label="City" name="city" valueLink={this.linkState('city')} /></div>
                <div className="idology__group"><Input label="State" valueLink={this.linkState('state')} disabled="disabled" name="state" /></div>
                <div className="idology__group"><Input label="Zip" name="zip" valueLink={this.linkState('zip')} /></div>
                <div className="idology__button">
                  <Btn onTap={onCancel} mod="red-linear">Cancel</Btn>
                  <Btn onTap={this.sendRequest} mod="submit green-linear">Submit</Btn>
                </div>
            </div>
            </div>
            `

        `<div className="idology__form-container">
            <div className="idology__form-window">
                {body}
            </div>
        </div>`
