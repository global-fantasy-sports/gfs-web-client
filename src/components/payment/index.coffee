_ = require 'underscore'

{Btn, BtnLink} = require '../../lib/ui'
{getCookie} = require '../../lib/utils'
{Titlebar} = require '../../lib/ui/Titlebar'
{Input} = require '../../lib/forms'
{userCell} = require '../../world'
xhr = require '../../lib/xhr'
PaymentHistory = require './PaymentHistory'
{IdologyVerificationForm} = require './IdologyVerificationForm'
{WithdrawalBankDataForm} = require './WithdrawalBankDataForm'


exports.Deposit = React.create
    displayName: 'Deposit'
    mixins: [
        flux.Connect("users", (store) -> {currentUser: store.getCurrentUser()})
        flux.Connect("payments", (store) -> {history: store.getPaymentsHistory()})
    ]

    contextTypes:
        router: React.PropTypes.object

    getInitialState: ->
        {
        amount: 0
        customAmount: 0
        showIdology: false
        xsrf_token: getCookie('_xsrf')
        }

    componentDidMount: ->
        @scroll()
        flux.actions.data.loadPaymentHistory()

    componentDidUpdate: ({route}) ->
        if route.page isnt @props.route.page
            @scroll()

    scroll: ->
        page = @props.route.page || ''
        if page isnt ''
            setTimeout((=> document.getElementsByClassName('app')[0].scrollTop = document.getElementById(page)?.offsetTop or 0), 100)
        else
            setTimeout((=> document.getElementsByClassName('app')[0].scrollTop = 0), 100)

    onPay: (amount) ->
        (e)=>
            if amount > 0
                @setState({amount})
            else
                @setState({amount: @state.customAmount})
            setTimeout(=>
                if @state.amount > 0
                    if not @state.currentUser.get("verified", false)
                        @setState(showIdology: true)
                    else
                        @refs.form.getDOMNode().submit()
            , 1)

    onCloseIdology: (success) ->
        =>
            @setState(showIdology: false)
            if success
                @refs.form.getDOMNode().submit()

    onChangeCustomAmount: (e) ->
        @setState(customAmount: e.target.value)

    render: (P, {amount, customAmount, xsrf_token, showIdology, history}) ->
        onPay = @onPay
        deposits = for deposit in [10, 20, 100, 250]
            `<div className="deposit-money__el">
                    <p className="deposit-money__amount">{deposit}$</p>
                    <p className="stroke white x-medium">Claim: <span>+${deposit} BONUS</span></p>
                    <p className="stroke white x-medium bold">TOTAL OF ${deposit*2}</p>
                    <p className="deposit-money__select">
                        <Btn mod='orange-linear block' onTap={onPay(deposit)}>
                            Pay
                        </Btn>
                    </p>
            </div>`

        bonus = Math.min(customAmount, 500)

        `<section className="deposit-money">
            <div className="deposit-money__inner">
                {showIdology ? <IdologyVerificationForm onCloseIdology={this.onCloseIdology} /> : null}
                <div className="deposit-money__top">
                    <div className="deposit-money__table">
                        <Titlebar title="We'll double your deposit!" className="regular blue no-text-up"/>
                        <p className="stroke x-medium white">For new customers we double the deposit of whatever you put in, at least up until $500!</p>
                        <p className="stroke x-medium white">Add funds to your account, and start playing for money today.</p>
                    </div>
                </div>
                <div className="deposit-money__deposits">
                    {deposits}
                    <div className={"deposit-money__el custom-amount"}>
                        <div className="deposit-money__amount count">
                            <Input placeholder="0"
                                   onChange={this.onChangeCustomAmount}
                                   mod="dark big"/>
                        </div>
                        <p className="stroke white x-medium">Claim: <span>+${bonus} BONUS</span></p>
                        <p className="stroke white x-medium bold">TOTAL OF ${parseInt(customAmount) + bonus}</p>
                        <p className="deposit-money__select">
                            <Btn mod='orange-linear block' onTap={this.onPay(0)}>
                                Pay
                            </Btn>
                        </p>
                    </div>
                </div>
                <form action="/payment/deposit/" method="post" ref="form">
                    <input type="hidden" name="_xsrf" value={xsrf_token} />
                    <input type="hidden" name="amount" value={amount} />
                    <input type="hidden" name="sport" value={SPORT}/>
                </form>
                <div className="deposit-money__bottom">
                    <div className="deposit-money__left">
                        <img src="/static/img/guarantee.png" alt="Save and Secure"/>
                        <ul className="deposit-money__secure-text">
                            <li>Win real money</li>
                            <li>Hassle free cash out</li>
                            <li>No season-long requirement</li>
                            <li>100% Legal and based in U.S.A.</li>
                            <li><a href="#/terms" className="stroke blue x-medium">Learn more...</a></li>
                        </ul>
                    </div>
                    <div className="deposit-money__right">
                        <img src="/static/img/money-back.png" alt="Money back"/>
                        <div className="deposit-money__guarantee">
                            <Titlebar title="100% Money Back Guarantee" mod="h2" className="regular"/>
                            <p className="stroke x-medium white left">If you are not satisfied with your first
                                game on Global Fantasy Sports Golf, we will refund your deposit - no questions asked!&nbsp;
                                <a href="mailto:support@globalfantasysports.com" className="stroke blue x-medium">Contact support...</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div className="deposit-money__withdraw">
                    <div className="deposit-money__left">
                        <Withdrawal />
                    </div>
                    <div className="deposit-money__right column">
                        <Titlebar title="Funds status" mod="h2"/>
                        <PaymentHistory history={history} />
                        <p className="stroke x-medium white">For inqueries contact support <a href="mailto:support@globalfantasysports.com" className="stroke blue x-medium">here</a></p>
                    </div>
                </div>
                <div className="deposit-money__exit">
                    <BtnLink to="__back" mod='white-linear block x-medium'>
                        Exit
                    </BtnLink>
                </div>
            </div>
         </section>`


Withdrawal = React.create
    displayName: 'Withdrawal'
    mixins: [
        flux.Connect("payments", (store) ->
            totalUsd: store.getTotalUsd()
            availableUsd: store.getAvailableForWithdrawUsd()
        )
    ]
    contextTypes:
        router: React.PropTypes.object

    getInitialState: ->
        amount: null
        showForm: false

    componentDidMount: ->
        flux.actions.data.receiveBalance()

    onChangeAmount: (e) ->
        amount = parseInt(e.target.value or 0)
        amount = Math.min(@state.availableUsd, amount)
        @setState({amount})

    withdraw: ->
        if @state.availableUsd >= @state.amount and @state.amount > 0
            flux.actions.data.sendWithdrawRequest(@state.amount)
            @setState({amount: null})

    onWithdraw: ->
        if @state.availableUsd >= @state.amount and @state.amount > 0
            @setState {showForm: true}

    onCloseForm: (success) ->
        =>
            state = showForm: false
            if success
                state.amount = null

            @setState state

    render: (P, {amount, totalUsd, availableUsd, showForm}) ->
        unavailableUSD = (totalUsd - availableUsd).toFixed(2)

        `<div className="withdraw-money">
            {showForm ? <WithdrawalBankDataForm onCloseForm={this.onCloseForm} amount={amount} /> : null}
            <Titlebar id="withdraw" className="left" title="Withdraw" mod="h2"/>
            <div className="withdraw-money__content">
                <div className="withdraw-money__left">
                    <Input name='withdraw'
                           placeholder={"Up to $" + availableUsd}
                           onChange={this.onChangeAmount}
                           value={amount}
                           mod="offset dark big"/>
                    <p className="stroke x-medium white indents">Account balance:</p>
                    <p className="stroke x-big green bold">${totalUsd}</p>
                </div>
                <div className="withdraw-money__right">
                    <div className="withdraw-money__pay-btn">
                        <Btn mod='green-linear medium' onTap={this.onWithdraw}>Withdraw funds</Btn>
                    </div>
                    <p className="stroke x-medium white bold indents">
                        Unavailable for withdrawal: <span>${unavailableUSD}</span>
                    </p>
                    <p className="stroke medium white">
                        Unavailable funds are defined as
                        a) free cash bonuses during depositing, or
                        b) any pending deposits awaiting confirmation by the bank
                        for availability on account.
                    </p>
                </div>
            </div>
         </div>`



exports.PaymentSuccessHandler = PaymentSuccessHandler = React.create
    contextTypes:
        router: React.PropTypes.object

    getInitialState: ->
        error: ''
        success: false

    componentWillMount: ->
        xhr
            url: '/payment/redirect/success/'
            method: 'post'
            responseType: 'json'
            data: JSON.stringify({
                id: @props.location.query['id']
                status: 'success'
            })
            callback: (err, data) =>
                setTimeout(
                    => @context.router.transitionTo("/deposit/")
                    5000)
                if data.error
                    @setState {error: data.error}
                else
                    flux.actions.data.loadPaymentHistory()
                    @setState {success: true}

    render: ({location}, S) ->
        if @state.error
            `<div className="payment-redirect warning">
                <div>{this.state.error}</div>
                <div className="clear" />
            </div>`
        else if @state.success
            amount = (parseFloat(location.query['transaction.amount']) / 100).toFixed(2)
            `<div className="payment-redirect success">
                <div>Your payment (${amount}) is deposited to your account</div>
                <div className="clear" />
            </div>`
        else
            `<div className="payment-redirect success">
                <div>Processing... Wait a while</div>
                <div className="preloader">
                    <img className="preloader__img" src="/static/img/preloader.svg" />
                </div>
            </div>`


exports.PaymentErrorHandler = PaymentErrorHandler = React.create
    contextTypes:
        router: React.PropTypes.object
    componentDidMount: ->
        setTimeout(
            => @context.router.transitionTo("/deposit/")
            10000)
    render: ({location}, S) ->
        errorMessage = location.query['transaction.errorMessage']

        `<div className="payment-redirect warning">
            <div>We are sorry. Something goes wrong.</div>
            <div>Reason: {errorMessage}</div>
            <div className="clear" />
        </div>`


exports.PaymentTimeoutHandler = PaymentTimeoutHandler = React.create
    contextTypes:
        router: React.PropTypes.object
    componentDidMount: ->
        setTimeout(
            => @context.router.transitionTo("/deposit/")
            3000)
    render: ({location}, S) ->
        `<div className="payment-redirect warning">
            <div>Sorry, but you exceeded timeout of payment operation.</div>
            <div className="clear" />
        </div>`


exports.PaymentDeclineHandler = PaymentDeclineHandler = React.create
    contextTypes:
        router: React.PropTypes.object
    componentDidMount: ->
        setTimeout(
            => @context.router.transitionTo("/deposit/")
            10000)
    render: ({location}, S) ->
        errorMessage = location.query['transaction.errorMessage']

        `<div className="payment-redirect warning">
            <div>Sorry, but your payment was declined.</div>
            <div className="clear" />
        </div>`
