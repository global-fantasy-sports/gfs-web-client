moment = require 'moment'
{Iterable} = require 'immutable'


module.exports = React.create
    displayName: "PaymentHistory"

    propTypes: {
        history: React.PropTypes.instanceOf(Iterable).isRequired
    }

    render: ({history}) ->
        if not history or history.isEmpty()
            rows = `<tr><td colSpan="5">No operations</td></tr>`
        else
            rows = Array.from(history).map((row, i) ->
                `<tr key={i} className="payment-history__row">
                    <td>{row.get('id').toLocaleString()}</td>
                    <td>{moment(row.get('date')).fromNow()}</td>
                    <td>{row.get('type')}</td>
                    <td>{row.get('status')}</td>
                    <td>${row.get('amount')}</td>
                </tr>`
            )

        `<table className="payment-history">
            <thead>
            <tr>
                <th className="payment-history__head">ID</th>
                <th className="payment-history__head">Date</th>
                <th className="payment-history__head">Type</th>
                <th className="payment-history__head">Status</th>
                <th className="payment-history__head">Amount</th>
            </tr>
            </thead>
            <tbody>{rows}</tbody>
        </table>`
