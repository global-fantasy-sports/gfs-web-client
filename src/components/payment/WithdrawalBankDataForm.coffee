LinkedStateMixin = require('react-addons-linked-state-mixin')
{Input} = require('../../lib/forms')
{Btn} = require('../../lib/ui')
{SelectBox} = require('../../lib/ui/SelectBox')
countries = require('../../lib/constants/countries')
states = require('../../lib/constants/states')
{EMAIL_RE} = require('../../lib/const')

exports.WithdrawalBankDataForm = React.create
    mixins: [
        LinkedStateMixin,
        flux.Connect("users", (store, props, state) ->
            bankData = store.getBankData()
            user = store.getCurrentUser()
            nameArray = user.get('name').split(' ')
            firstName = nameArray[0] or ''
            lastName = nameArray[(nameArray.length - 1) or -1] or ''
            setTimeout((->
                flux.actions.forms.setValue('firstName', bankData.get('firstname') or firstName)
                flux.actions.forms.setValue('lastName', bankData.get('lastname') or lastName)
                flux.actions.forms.setValue('email', bankData.get('email', user.get('email', '')))
                flux.actions.forms.setValue('phone', bankData.get('phone', ''))),
            1)
            {}
        )
    ]

    componentWillMount: ->
        flux.actions.forms.init()
        if flux.stores.users.getBankData().isEmpty()
            flux.actions.data.receiveBankData()

    validateField: (name) ->
        value = flux.stores.forms.getValue(name)
        switch name
            when "firstName"
                unless value
                    flux.actions.forms.setError(name, "This field is required")
                    return false
            when "lastName"
                unless value
                    flux.actions.forms.setError(name, "This field is required")
                    return false
            when "email"
                unless value
                    flux.actions.forms.setError(name, "This field is required")
                    return false
                if not value.match(EMAIL_RE)
                    flux.actions.forms.setError(name, "Incorrect email")
                    return false
            when "phone"
                unless value
                    flux.actions.forms.setError(name, "This field is required")
                    return false
                val = value.replace('(\(|\)|\s+)', '')
                if not val.match(/^(?:1-?)?(\d{3})[-\.]?(\d{3})[-\.]?(\d{4})$/)
                    flux.actions.forms.setError(name, "Incorrect Phone number")
                    return false
        flux.actions.forms.setSuccess(name)
        return true

    sendRequest: ->
        formValid = true
        for field in ['firstName', 'lastName', 'phone', 'email']
            if not @validateField(field)
                formValid = false

        if formValid
            data =
                amount: @props.amount
                firstname: flux.stores.forms.getValue('firstName')
                lastname: flux.stores.forms.getValue('lastName')
                phone: flux.stores.forms.getValue('phone')
                email: flux.stores.forms.getValue('email')


            flux.actions.data.sendWithdrawRequest(data)
            @props.onCloseForm(true)()

    render: ->
        onCancel = @props.onCloseForm(false)
        body = `<div>
            <div className="withdraw__title">Please, fill the form with your bank account data.</div>
            {this.state.error ? <div className="error">{this.state.error}</div> : null}
            <div className="withdraw__request">
                <div className="withdraw__group"><Input label="First name" validationId="firstName" /></div>
                <div className="withdraw__group"><Input label="Last name" validationId="lastName" /></div>
                <div className="withdraw__group"><Input label="Email" validationId="email" /></div>
                <div className="withdraw__group"><Input label="Phone" validationId="phone" /></div>
                <div className="withdraw__button">
                    <Btn onTap={onCancel} mod="red-linear">Cancel</Btn>
                    <Btn onTap={this.sendRequest} mod="submit green-linear">Submit</Btn>
                </div>
            </div>
        </div>
        `

        `<div className="withdraw__form-container">
          <div className="withdraw__form-window">
            {body}
          </div>
        </div>`
