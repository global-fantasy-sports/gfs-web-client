{AutoSizer} = require('react-virtualized')
{cx, bem} = require('../lib/utils')


class TabPanel extends React.Component

    @propTypes: {
        activeIndex: React.PropTypes.number
        children: React.PropTypes.arrayOf(React.PropTypes.node).isRequired
        onClick: React.PropTypes.func.isRequired
    }

    @defaultProps: {
        activeIndex: 0
    }

    constructor: (props) ->
        super(props)
        @state = {activeIndex: props.activeIndex ? 0}
        @_tabCls = bem('tab-panel__tab')

    renderTab: (name, index) =>
        handleClick = (e) =>
            e.preventDefault()
            @props.onClick(index)

        cls = @_tabCls(if index is @props.activeIndex then 'active' else null)

        `<a key={index} className={cls} href="#" onClick={handleClick}>
            <span role="label">{name}</span>
            <span role="underline" />
        </a>`

    render: () ->
        {children, activeIndex, className} = @props
        tabs = children.map((tab) -> tab.key)

        renderChild = ({width}) =>
            React.cloneElement(children[activeIndex], {width})

        `<section className={cx("tab-panel", className)}>
            <nav className="tab-panel__controls">
                {tabs.map(this.renderTab)}
            </nav>

            <div className="tab-panel__content">
                <AutoSizer disableHeight>{renderChild}</AutoSizer>
            </div>
        </section>`


module.exports = TabPanel
