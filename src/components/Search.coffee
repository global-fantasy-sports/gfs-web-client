_ = require('underscore')
{countryByCode} = require('lib/utils')


{SelectBox} = require('../lib/ui/SelectBox')
{Input} = require('../lib/forms')



getCountries = ()->
    countries = flux.stores.participants.getCountryCodes().toSeq()
        .map((value)-> {value, name: countryByCode(value)})
        .sortBy((c)-> c.name)
        .toArray()

    [{value: '', name: 'All countries'}, countries...]


exports.Search = React.create
    displayName: 'Search'

    mixins: [flux.Connect('search', (store) ->
        country: store.getCountry()
        name: store.getName()
    )]

    handleChangeFilter: (e) ->
        flux.actions.search.setName(e.target.value)

    handleChangeCountry: (e) ->
        flux.actions.search.setCountry(e.target.value)

    render: ({country, name}) ->
        countries = getCountries()

        countryButton = `<div className="ui-form">
                <label className="ui-label inline left text-up">
                    Select country
                </label>
                <select className="ui-select"
                        value={country}
                        onChange={this.handleChangeCountry}>
                    {countries.map(function(c) {
                        return <option key={c.name} value={c.value}>{c.name}</option>
                    })}
                </select>
            </div>`

        nameSearch = `<div className="ui-form">
                <label className="ui-label inline left text-up">
                    By name
                </label>
                <Input name="filter-by-name" type="text"
                       value={name}
                       onChange={this.handleChangeFilter}/>
            </div>`

        topClass = "f-48"

        `<nav className="filters-nav">
            <div className="filters-nav__top">
                <div className={topClass}>
                    {countryButton}
                </div>
                <div className={topClass}>
                    {nameSearch}
                </div>
            </div>
        </nav>`
