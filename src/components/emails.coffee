config = require('../config')
xhr = require('../lib/xhr')
Popup = require('../lib/ui/Popup')
GameEmbed = require('../lib/ui/GameEmbed')


exports.unsubscribe = ({token}, callback) ->
    return callback('fail') unless token
    xhr({
        url: "/account/unsubscribe/"
        method: "post"
        responseType: "json"
        data: JSON.stringify({token})
        callback: (err, data) =>
            status = if err or (data and data.error) then 'fail' else 'success'
            callback(status)
    })
    
    
exports.checkEmailConfirmation = ({status}, callback) ->
    callback(status)


class exports.StatusPopupHandler extends React.Component

    @propTypes:
        routeParams: React.PropTypes.object.isRequired
        route: React.PropTypes.shape(
            func: React.PropTypes.func.isRequired
            messages: React.PropTypes.object.isRequired
        ).isRequired

    @contextTypes:
        router: React.PropTypes.object

    constructor: (args...) ->
        super(args...)
        @state = status: null

    componentDidMount: ->
        {routeParams, route} = @props
        route.func(routeParams or {}, (status) => @setState({status}))

    onClose: =>
        @context.router.transitionTo("/#{SPORT}/")

    render: ->
        return null unless @state.status?
        text = @props.route.messages[@state.status]
        `<section className="app app__pre-content">
            <Popup>
                <GameEmbed mod="flash-popup" onTap={this.onClose} key={text}>
                    <p className="flash-popup__msg">{text}</p>
                </GameEmbed>
            </Popup>
        </section>`
