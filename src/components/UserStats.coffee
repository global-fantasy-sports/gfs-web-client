{Btn, BtnLink, Tokens, Dollars} = require('../lib/ui')
GuideHint = require('./guide/GuideHint')
Notifications = require('./notifications/Notifications')


module.exports = React.create
    displayName: 'UserStats'

    menuClick: (event)->
        event.stopPropagation()
        flux.actions.state.toggleSideMenu()

    render: ({balance, name, avatar}) ->
        `<div className="user-stats">
            <div className="user-stats__person">
                <img src={avatar} height="35" width="35"/>
            </div>
            <ul className="user-stats__list">
                <Notifications className="user-stats__item" />

                <li className="user-stats__item">
                    <Tokens m={balance} icon={true} mod="white"/>
                </li>
                <li className="user-stats__item">
                    <Dollars m={balance} icon={true} mod="white"/>
                </li>
                <li className="user-stats__item clickable">
                    <BtnLink to="/deposit/" mod="green small">
                        Add funds
                    </BtnLink>
                </li>
            </ul>
            <div className="user-stats__buttons">
                <Btn onTap={this.menuClick}
                    mod="sidemenu"
                    name="menu"
                    icon="white-lines">
                    &nbsp;
                </Btn>
            </div>
        </div>`

