{GAMEDEF} = require '../games'
CompetitionRoom = require 'components/competitionRoom'

ChallengeRoom = React.create

    mixins: [
        flux.Connect('challenges', (store, {challengeId}, {timerId})->
            challenge = store.getChallenge(challengeId)
            if not challenge.count()
                timerId = setTimeout =>
                    window.location = '/'
                , 1000
            else
                clearTimeout(timerId)
                timerId = null

            {challenge, timerId}
        )
    ]

    render: ({}, {challenge})->
        id = challenge.get("competitionRoomId")

        if id
            `<CompetitionRoom
                roomId={id}
                fromChallenge={true}
                onPlay={this.props.onPlay}
                user={this.props.user} />`
        else
            `<div className="wrong-room" />`

module.exports = ChallengeRoom
