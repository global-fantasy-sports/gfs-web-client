UserStats = require('./../UserStats')
AppLogo = require('./../AppLogo')

module.exports = React.create
    displayName: 'Header'

    propTypes: {
        top: React.PropTypes.node
        bottom: React.PropTypes.node
    }

    mixins: [
        flux.Connect("users", (store) ->
            userName: store.getUserName()
            userAvatar: store.getUserAvatar()
            userBalance: store.getUserBalance()
        )
    ]


    render: ({top, bottom, sport}, {userAvatar, userBalance, userName}) ->
        `<header className="head">
            {top}

            <div className="head__top">
                <div className="head__row">
                    <div className="head__logo">
                        <AppLogo sport={sport} />
                    </div>

                    <div className="head__menu">
                        <a href="/" className="head__link">GAMES</a>
                        <a href="http://news.globalfantasysports.com" className="head__link">NEWS</a>
                        <a href="http://globalfantasysports.com" className="head__link" target="_blank">ABOUT</a>
                        <a href="http://globalfantasysports.com" className="head__link" target="_blank">HELP</a>
                    </div>

                    <div className="head__user-info">
                        <UserStats
                            name={this.state.userName}
                            avatar={this.state.userAvatar}
                            balance={this.state.userBalance}
                        />
                    </div>
                </div>
            </div>

            {bottom}
        </header>`
