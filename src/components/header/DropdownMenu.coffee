{Icon} = require('lib/ui')
{cx} = require('lib/utils')

DropdownMenu = React.create
    displayName: 'DropdownMenu'

    getInitialState: ->
        hovered: false

    onHover: ->
        @setState hovered: true

    onBlur: ->
        @setState hovered: false

    render: ({title, className, children}, {hovered}) ->
        els = children.map((c, i) ->
            `<li key={i} className="dropdown-menu__item">{c}</li>`
        )
        iconClass = cx(
            "rotateBack-180": hovered
        )

        `<section className="dropdown-menu" onMouseEnter={this.onHover}
            onMouseLeave={this.onBlur}>
            <a href="javascript:void(0);" className="head__drop-link">
                {title}
                <Icon i="head-drop-arrow" className={iconClass}/></a>
            {hovered && <div className="dropdown-menu__submenu">
                <ul>
                    {els}
                </ul>
            </div>}
        </section>`


module.exports = DropdownMenu
