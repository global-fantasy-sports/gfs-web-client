Connect = require('../../flux/connectMixin')
PredictionCardHoleByHole = require('./PredictionCardHoleByHole')
PredictionParticipant = require('./PredictionParticipant')
PredictionProgressBar = require('./PredictionProgressBar')


module.exports = React.create(
    displayName: 'PredictionStep'

    mixins: [
        Connect('handicap', (store) -> store.getHandicapStep())
    ]

    render: (P, {lineup, progress}) ->
        golfers = lineup.map((p, index) =>
            handleClick = () ->
                alert("OHAI, #{index}")

            `<PredictionParticipant
                key={index}
                active={index === 0}
                photo={p.largePhoto}
                name={p.getFullName()}
                country={p.country}
                onClick={handleClick}
            />`
        )

        `<section className="handicap-card">
            <div className="handicap-card__top">
                <div className="flex-row-start">
                    <span className="handicap-card__title">Prediction Card</span>
                    <span className="handicap-card__title-small">{progress}% complete</span>
                </div>
                <PredictionProgressBar progress={progress} />
            </div>
            <div className="handicap-card__golfer-items">
                {golfers}
            </div>
            <div className="handicap-card__table">
                <PredictionCardHoleByHole />
            </div>
        </section>`
)
