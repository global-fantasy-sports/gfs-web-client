bem = require('bem-classname')
{Icon} = require('lib/ui')
{cx} = require('lib/utils')


cardCls = bem.bind(null, "prediction-card")

CountIcon = ({value, max}) ->
    finished = value is max
    `<div className={cardCls('round-count', {finished: finished})}>
        {finished
            ? <Icon i="round-finished" />
            : value + '/' + max}
    </div>`


Cell = ({disabled, filled, index}) ->
    handleClick = (e) ->
        e.preventDefault()
        flux.actions.handicap.toggleHandicap(index)

    mod =
        if filled then 'filled'
        else if disabled then 'disabled'
        else 'empty'

    className = "prediction-cell prediction-cell--#{mod}"

    `<a href="#" onClick={handleClick} className={className}>
        {mod === 'filled' ? -1 : null}
    </a>`

module.exports = React.create(
    displayName: 'HandicapSelection'

    render: ({yardages, pars, participant, handicap}) ->
        isOpen = handicap.size < participant.handicap
        column = for i in [0..17]
            `<div className={cardCls("column")} key={i}>
                <div className={cardCls("column-top")}>
                    <div className={cardCls("cell-hole")}>{i + 1}</div>
                    <div>{yardages[i]}</div>
                    <div>{pars[i]}</div>
                </div>
                <div className={cardCls("column-bottom")}>
                    <Cell filled={handicap.has(i)} disabled={!isOpen} index={i} />
                    <Cell filled={handicap.has(i)} disabled={!isOpen} index={i} />
                </div>
            </div>`

        `<section className="prediction-card">
            <section className="prediction-card__left">
                <div className="prediction-card__top-table">
                    <div className={cardCls("column", "name")}>
                        <p>Hole</p>
                        <p>Yards</p>
                        <p>Par</p>
                    </div>
                </div>
                <div className="prediction-card__bottom-table">
                    <div className={cardCls("column", "legend")}>
                        <div className={cardCls("cell-legend")}>
                            <CountIcon
                                value={handicap.size}
                                max={participant.handicap}
                            />
                            <p>Round 1</p>
                        </div>
                        <div className={cardCls("cell-legend")}>
                            <CountIcon
                                value={handicap.size}
                                max={participant.handicap}
                            />
                            <p>Round 2</p>
                        </div>
                    </div>
                </div>
            </section>
            <section className="prediction-card__right">
                <div className="prediction-card__table">
                    {column}
                </div>
            </section>
        </section>`
)
