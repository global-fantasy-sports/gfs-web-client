FilterGroup = require('../../lib/ui/FilterGroup')
ParticipantSearch = require('../ParticipantSearch')


HANDICAPS = ['All', 'H1', 'H2', 'H3', 'H4']


class HandicapParticipantFilters extends React.Component

    handleHandicap: (index) ->
        flux.actions.roster.filterHandicap(HANDICAPS[index])

    handleSearch: (value) ->
        flux.actions.roster.search(value)

    render: () ->
        {className, search} = @props
        `<div className={className}>
            <FilterGroup
                values={HANDICAPS}
                activeIndex={0}
                onChange={this.handleHandicap}
            />
            <ParticipantSearch
                value={search}
                onChange={this.handleSearch}
            />
        </div>`


module.exports = HandicapParticipantFilters
