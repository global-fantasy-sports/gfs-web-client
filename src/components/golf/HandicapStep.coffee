Connect = require('../../flux/connectMixin')
HandicapLegend = require('./HandicapLegend')
HandicapSelection = require('./HandicapSelection')
PredictionParticipant = require('./PredictionParticipant')
PredictionProgressBar = require('./PredictionProgressBar')


module.exports = React.create(
    displayName: 'HandicapStep'

    mixins: [
        Connect('handicap', (store) -> store.getHandicapStep())
    ]

    render: (P, {activeIndex, lineup, progress, participant, handicap, finished, yardages, pars}) ->
        golfers = lineup.map((p, index) =>
            `<PredictionParticipant
                key={index}
                active={index === activeIndex}
                isDone={finished.get(index)}
                photo={p.largePhoto}
                name={p.getFullName()}
                country={p.country}
                onClick={flux.actions.handicap.selectParticipant.bind(null, index)}
            />`
        )

        `<section className="handicap-card">
            <div className="handicap-card__top">
                <div className="flex-row-start">
                    <span className="handicap-card__title">Handicap Card</span>
                    <span className="handicap-card__title-small">{progress}% complete</span>
                </div>
                <PredictionProgressBar progress={progress} />
            </div>
            <div className="handicap-card__golfer-items">
                {golfers}
            </div>
            <div className="handicap-card__table">
                <HandicapSelection
                    yardages={yardages}
                    pars={pars}
                    handicap={handicap}
                    participant={participant}
                />
            </div>
            <div className="handicap-card__legend">
                <HandicapLegend />
            </div>
        </section>`
)
