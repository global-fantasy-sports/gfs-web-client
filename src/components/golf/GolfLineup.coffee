_ = require('underscore')
{Icon} = require('../../lib/ui')
{Flag} = require('../../lib/ui/Flag')
{countryByCode} = require('../../lib/utils')
PlayerPhoto = require('../PlayerPhoto')


EmptyParticipantItem = ({position, label, onClick}) ->
    `<div className="selected-participants__empty" onClick={onClick || _.noop}>
        <div className="selected-participants__empty-inner">
            {label} {position + 1}
        </div>
    </div>`


class SelectedParticipantItem extends React.Component

    handleClick: (e) =>
        if not e.isDefaultPrevented()
            e.preventDefault()
            @props.onSelect(@props.position)

    handleRemove: (e) =>
        if not e.isDefaultPrevented()
            e.preventDefault()
            flux.actions.roster.removeParticipant(@props.participant.id)

    render: () ->
        {participant, position} = @props
        `<div className="selected-participants__filled" onClick={this.handleClick}>
            <div className="selected-participants__info">
               <div className="selected-participants__photo">
                   <PlayerPhoto player={participant} size={110}/>
               </div>
                <div className="selected-participants__name">
                    <p className="stroke medium black lato-black text-up">{participant.getName()}</p>
                    <div className="selected-participants__flag">
                        <Flag country={participant.country} />
                        <span className="selected-participants__country">{countryByCode(participant.country)}</span>
                    </div>
                </div>
            </div>
            <div className="selected-participants__btn" onClick={this.handleRemove}>
                <Icon i="minus-blue" />
            </div>
        </div>`


module.exports = React.create(
    displayName: 'GolfLineup'

    render: ({lineup, onSelect, onRemove}) ->
        participants = lineup.toSeq().map((p, index) ->
            if p
                `<SelectedParticipantItem
                    key={index}
                    position={index}
                    participant={p}
                    onSelect={onSelect}
                    onRemove={onRemove}
                />`
            else
                label = if lineup.size is 5 then 'SELECT Golfer from TIER' else 'SELECT Golfer'
                if lineup.size is 5
                    handleClick = () -> flux.actions.roster.filterByTier(index)
                `<EmptyParticipantItem
                    key={index}
                    label={label}
                    position={index}
                    onClick={handleClick}
                />`
        )

        `<div className="selected-participants">
            <div className="selected-participants__top">
                <span className="selected-participants__title">Candidates</span>&nbsp;
                <span className="stroke size-24 white lato-black">{lineup.count(Boolean)}</span>
                <span className="stroke size-24 black lato-black">/{lineup.size}</span>
            </div>
            <div className="selected-participants__list">
                {participants}
            </div>
        </div>`
)
