_ = require('underscore')
{SortDirection} = require('react-virtualized')

{cx} = require('../../lib/utils')
Connect = require('../../flux/connectMixin')

GolfParticipantsList = require('../roster/GolfParticipantsList')
GolfLineup = require('./GolfLineup')


module.exports = React.create(
    displayName: 'ParticipantSelectionStep'

    mixins: [
        Connect('roster', (store) -> store.getRoster())
    ]

    label: (m) ->
        if @state.selectedIds.includes(m.id)
            return `<Icon i="ok green 05x label" />`

    onNext: ->
        return unless @state.isDone
        if flux.stores.state.getMode() == "EDIT"
            flux.actions.wizard.saveGame()
        @props.onNext()

    handleRemoveParticipant: (participant) ->
        flux.actions.roster.toggleParticipant(participant.id)

    render: ({gameDef}, {position, participants, lineup, sorting, reversed, open, selected}) ->
        {ParticipantFilters} = gameDef.select

        `<section className="flex-row-start">
            <div>
                <ParticipantFilters
                    {...this.state}
                    className="wizard-filter"
                />
                <GolfParticipantsList
                    width={700}
                    height={640}
                    participants={participants}
                    open={open}
                    selected={selected}
                    sortBy={sorting}
                    sortDirection={SortDirection[reversed ? 'DESC' : 'ASC']}
                />
            </div>
            <GolfLineup
                lineup={lineup}
                onRemove={this.handleRemoveParticipant}
            />
        </section>`
)
