{cx, countryByCode} = require('../../lib/utils')
{Flag} = require('../../lib/ui/Flag')
{Icon} = require('../../lib/ui')


PredictionParticipant = ({active, isDone, photo, name, country, onClick}) ->
    cls = cx('handicap-card__item', {
        "handicap-card__item--active": active
    })

    `<div className={cls} onClick={onClick}>
        <div className="handicap-card__golfer">
            <img src={photo} alt={name} height={70} />
            <div className="handicap-card__golfer-info">
                <div className="handicap-card__name">{name}</div>
                <div className="handicap-card__country">
                    <Flag country={country} />
                    <span>{countryByCode(country)}</span>
                </div>
            </div>
        </div>
        <div className="handicap-card__btn">
            {isDone ? <Icon i="checked" /> : null}
        </div>
    </div>`


module.exports = PredictionParticipant
