bem = require('bem-classname')
{Icon, Btn} = require('lib/ui')
{cx} = require('lib/utils')

module.exports = React.create(
    displayName: "PredictionCardHoleByHole"

    getInitialState: ->
#        TODO: temporary state for prediction cells. Delete it!
        cellState: "played"
        resultTable: true

    render: ({}, {cellState, resultTable}) ->

        cardCls = bem.bind(null, "prediction-card")

        cell =
            switch cellState
                when "empty"
                    `<a href="#" className="prediction-cell prediction-cell--empty"></a>`
                when "filled"
                    `<a href="#" className="prediction-cell prediction-cell--icon">
                        <Icon i="checked"/>
                    </a>`
                when "disabled"
                    `<a href="#" className="prediction-cell prediction-cell--disabled"></a>`
                when "wrong"
                    `<a href="#" className="prediction-cell prediction-cell--icon">
                        <Icon i="wrong-predict"/>
                    </a>`
                when "played"
                    `<a href="#" className="prediction-cell prediction-cell--icon">
                        <Icon i="played-ball"/>
                    </a>`

        column = for i in [1..18]
            `<div className={cardCls("column", {result: resultTable})} key={i}>
                <div className={cardCls("column-top")}>
                    <div className={cardCls("cell-hole")}>{i}</div>
                    <div>450</div>
                    <div>4</div>
                </div>
                <div className={cardCls("column-bottom", "hole-by-hole")}>
                    {cell}
                    {cell}
                    {cell}
                </div>
            </div>`

        totalSection = if resultTable
            `<section className="prediction-card__total">
                <div className="prediction-card__top-table">
                    <div className={cardCls("column", "total")}>
                        <p>TOTAL</p>
                    </div>
                </div>
                <div className="prediction-card__bottom-table">
                    <div className={cardCls("column", "total-hole-by-hole")}>
                        <div className={cardCls("cell-total")}>
                            <span className="stroke lato-heavy extra-small white">10</span>
                            <span className="stroke lato-heavy extra-small blue-light">/24</span>
                        </div>
                        <div className={cardCls("cell-total")}>
                            <span className="stroke lato-heavy extra-small white">12</span>
                            <span className="stroke lato-heavy extra-small blue-light">/26</span>
                        </div>
                        <p className="stroke lato-heavy small white">0</p>
                    </div>
                </div>
            </section>`
        else null

        `<section className="prediction-card">
            <div className="prediction-card__top">
                <div className="prediction-card__round-btn">
                    <Btn mod="blue-base">Round 1</Btn>
                    <Btn mod="blue-base offset-sides">Round 2</Btn>
                </div>
                <Btn mod="copy-rounds">Copy from round 1</Btn>
            </div>
            <section className="prediction-card__inner">
                <section className="prediction-card__left">
                    <div className="prediction-card__top-table">
                        <div className={cardCls("column", "name")}>
                            <p>Hole</p>
                            <p>Yards</p>
                            <p>Par</p>
                        </div>
                    </div>
                    <div className="prediction-card__bottom-table">
                        <div className={cardCls("column", "hole-by-hole")}>
                            <p>Under par</p>
                            <p>Par</p>
                            <p>Over Par</p>
                        </div>
                    </div>
                </section>
                <section className="prediction-card__right">
                    <div className="prediction-card__table">
                        {column}
                    </div>
                </section>
                {totalSection}
            </section>
        </section>`
)
