FilterGroup = require('../../lib/ui/FilterGroup')

POSITIONS = [1..5].map((n) -> "Tier #{n}")


class FiveBallParticipantFilters extends React.Component

    handleTierSelect: (value) ->
        flux.actions.roster.filterByTier(value)

    render: () ->
        {className, tier} = @props
        `<div className={className}>
            <FilterGroup
                values={POSITIONS}
                activeIndex={tier}
                onChange={this.handleTierSelect}
            />
        </div>`


module.exports = FiveBallParticipantFilters
