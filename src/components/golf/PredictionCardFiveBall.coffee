bem = require('bem-classname')
{Icon, Btn} = require('lib/ui')
{cx} = require('lib/utils')

module.exports = React.create(
    displayName: "PredictionCardFiveBall"

    getInitialState: ->
#        TODO: temporary state for prediction cells. Delete it!
        resultTable: true

    render: ({}, {cellState, resultTable}) ->

        cardCls = bem.bind(null, "prediction-card")

        cell = `<a href="#" className="prediction-cell prediction-cell--small"></a>`

        column = for i in [1..18]
            `<div className={cardCls("column", "result")} key={i}>
                <div className={cardCls("column-top", "five-ball")}>
                    <div className={cardCls("cell-hole")}>{i}</div>
                    <div>4</div>
                </div>
                <div className={cardCls("column-bottom", "five-ball")}>
                    {cell}
                    {cell}
                </div>
            </div>`

        totalSection = if resultTable
            `<section className="prediction-card__total">
                <div className="prediction-card__top-table">
                    <div className={cardCls("column", ["total", "total-top-five-ball"])}>
                        <p>TOTAL</p>
                        <p className="prediction-card__par-total">0</p>
                    </div>
                </div>
                <div className="prediction-card__bottom-table">
                    <div className={cardCls("column", ["total-hole-by-hole", "total-five-ball"])}>
                        <p className="stroke lato-heavy small white">0</p>
                        <p className="stroke lato-heavy small white">0</p>
                    </div>
                </div>
            </section>`
        else null

        `<section className="prediction-card">
            <div className="prediction-card__top">
                <div className="prediction-card__round-btn">
                    <Btn mod="blue-base">Round 1</Btn>
                    <Btn mod="blue-base offset-sides">Round 2</Btn>
                </div>
            </div>
            <section className="prediction-card__inner">
                <section className="prediction-card__left">
                    <div className="prediction-card__top-table">
                        <div className={cardCls("column", ["name", "name-five-ball"])}>
                            <p>Hole</p>
                            <p>Par</p>
                        </div>
                    </div>
                    <div className="prediction-card__bottom-table">
                        <div className={cardCls("column", ["hole-by-hole", "five-ball"])}>
                            <p>STROKE</p>
                            <p>FANTASY POINTS</p>
                        </div>
                    </div>
                </section>
                <section className="prediction-card__right">
                    <div className="prediction-card__table">
                        {column}
                    </div>
                </section>
                {totalSection}
            </section>
        </section>`
)
