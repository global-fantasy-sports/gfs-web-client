PredictionProgressBar = ({progress = 0}) ->
    `<div className="handicap-card__line">
        <div className="handicap-card__progress" style={{width: progress + '%'}}>
            <div className="handicap-card__circle" />
        </div>
    </div>`

module.exports = PredictionProgressBar
