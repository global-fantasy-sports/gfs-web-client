LegendItem = ({type, points, description}) ->
    `<div className="handicap-legend__item">
        <i className={'legend-icon legend-icon--' + type} />
        <div className="handicap-legend__info">
            <p className="handicap-legend__stroke">{points}</p>
            <p className="handicap-legend__type">{type}</p>
            <p className="handicap-legend__desc">{description}</p>
        </div>
    </div>`


HandicapLegend = () ->
    `<section className="handicap-legend">
        <div className="handicap-legend__left">Legend</div>
        <div className="handicap-legend__right">
            <LegendItem
                type="handicap"
                points="-1 stroke"
                description="Subtract 1 stroke from hole"
            />
            <LegendItem
                type="albatross"
                points="+8 points"
                description="3 strokes under par"
            />
            <LegendItem
                type="eagle"
                points="+5 points"
                description="2 strokes under par"
            />
            <LegendItem
                type="birdie"
                points="+2 points"
                description="1 strokes under par"
            />
            <LegendItem
                type="par"
                points="0 points"
                description=""
            />
            <LegendItem
                type="bogey"
                points="-1 points"
                description="1 stroke over par"
            />
            <LegendItem
                type="double-bogey"
                points="-3 points"
                description="or worse 2 strokes or moreover par"
            />
        </div>
    </section>`


module.exports = HandicapLegend
