ParticipantSearch = require('../ParticipantSearch')


class PredictionParticipantFilters extends React.Component

    handleSearch: (value) ->
        flux.actions.roster.search(value)

    render: () ->
        {className, search} = @props
        `<div className={className}>
            <ParticipantSearch
                value={search}
                onChange={this.handleSearch}
            />
        </div>`


module.exports = PredictionParticipantFilters
