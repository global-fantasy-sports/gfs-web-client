bem = require('bem-classname')
{Icon} = require('lib/ui')
{cx} = require('lib/utils')

module.exports = React.create(
    displayName: "PredictionCard"

    getInitialState: ->
#        TODO: temporary state for prediction cells. Delete it!
        cellState: "handicap"
        resultTable: true

    render: ({}, {cellState, resultTable}) ->

        cardCls = bem.bind(null, "prediction-card")

        cell =
            switch cellState
                when "empty"
                    `<a href="#" className="prediction-cell prediction-cell--empty"></a>`
                when "filled"
                    `<a href="#" className="prediction-cell prediction-cell--filled">-1</a>`
                when "disabled"
                    `<a href="#" className="prediction-cell prediction-cell--disabled"></a>`
                when "small"
                    `<a href="#" className="prediction-cell prediction-cell--small"></a>`
                when "handicap"
                    `<a href="#" className="prediction-cell prediction-cell--handicap">-1</a>`
                when "albatross"
                    `<a href="#" className="prediction-cell prediction-cell--albatross">+8</a>`
                when "eagle"
                    `<a href="#" className="prediction-cell prediction-cell--eagle">+5</a>`
                when "birdie"
                    `<a href="#" className="prediction-cell prediction-cell--birdie">+2</a>`
                when "par"
                    `<a href="#" className="prediction-cell prediction-cell--par">0</a>`
                when "bogey"
                    `<a href="#" className="prediction-cell prediction-cell--bogey">-1</a>`
                when "double-bogey"
                    `<a href="#" className="prediction-cell prediction-cell--double-bogey">-3</a>`

        column = for i in [1..18]
            `<div className={cardCls("column", {result: resultTable})} key={i}>
                <div className={cardCls("column-top")}>
                    <div className={cardCls("cell-hole")}>{i}</div>
                    <div>450</div>
                    <div>4</div>
                </div>
                <div className={cardCls("column-bottom", {handicap: resultTable})}>
                    {cell}
                    {cell}
                    {resultTable ? cell : null}
                </div>
            </div>`
        roundsIcon = `<div className={cardCls("round-count")}>2/4</div>`
        roundsIconFinished = `<div className={cardCls("round-count", "finished")}>
            <Icon i="round-finished"/>
        </div>`

        totalSection = if resultTable
            `<section className="prediction-card__total">
                <div className="prediction-card__top-table">
                    <div className={cardCls("column", "total")}>
                        <p>TOTAL</p>
                    </div>
                </div>
                <div className="prediction-card__bottom-table">
                    <div className={cardCls("column", "total-hole-by-hole")}>
                        <div className={cardCls("cell-total")}>
                            <span className="stroke lato-heavy extra-small white">10</span>
                            <span className="stroke lato-heavy extra-small blue-light">/24</span>
                        </div>
                        <div className={cardCls("cell-total")}>
                            <span className="stroke lato-heavy extra-small white">12</span>
                            <span className="stroke lato-heavy extra-small blue-light">/26</span>
                        </div>
                        <p className="stroke lato-heavy small white">0</p>
                    </div>
                </div>
            </section>`
        else null

        legend = if resultTable
            `<div className={cardCls("column", "hole-by-hole")}>
                <p>Stroke</p>
                <p>Handicap</p>
                <p>Fantasy Points</p>
            </div>`
        else
            `<div className={cardCls("column", "legend")}>
                <div className={cardCls("cell-legend")}>
                    {roundsIconFinished}
                    <p>Round 1</p>
                </div>
                <div className={cardCls("cell-legend")}>
                    {roundsIcon}
                    <p>Round 2</p>
                </div>
            </div>`

        `<section className="prediction-card">
            <section className="prediction-card__left">
                <div className="prediction-card__top-table">
                    <div className={cardCls("column", "name")}>
                        <p>Hole</p>
                        <p>Yards</p>
                        <p>Par</p>
                    </div>
                </div>
                <div className="prediction-card__bottom-table">
                    {legend}
                </div>
            </section>
            <section className="prediction-card__right">
                <div className="prediction-card__table">
                    {column}
                </div>
            </section>
            {totalSection}
        </section>`
)
