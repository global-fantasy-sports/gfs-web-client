_ = require('underscore')
{Btn, BtnLink, Link} = require('../lib/ui')
Logo = require('../lib/ui/Logo')
{playButtonNameByType} = require('../lib/const')

ReactCSSTransitionGroup = require('react-addons-css-transition-group')

exports.Rules = Rules = React.create
    displayName: 'Rules'

    getInitialState: ->
        pick: false
        hole: false
        five: false
        scoring: false

    componentDidMount: ->
        {path} = _.last(@props.branch) if @props.branch
        @setState({
            pick: path is 'hole-by-hole'
            hole: path is 'handicap-the-pros'
            five: path is 'five-ball'
        })

        @scrollToSection(path, false)

    openPick18: ->
        @setState(pick: not @state.pick)

    openHoleInOne: ->
        @setState(hole: not @state.hole)

    openFiveBall: ->
        @setState(five: not @state.five)

    openScoring: ->
        @setState(scoring: not @state.scoring)

    scrollToSection: (rulesSection, animate=true) ->
        if rulesSection and rulesSection of @refs
            el = React.findDOMNode(@refs[rulesSection])

        duration = 0 unless animate

        _.defer(->
            flux.actions.state.scrollToTop({el, duration})
        )


    render: ({game, type}, {segment, pick, hole, five, scoring}) ->

        sfSystemContent = `
            <table className="table table-bordered text-centered">
                <thead className="bold">
                    <tr>
                        <th>Strokes used in relation to par</th>
                        <th>Points</th>
                    </tr>
                </thead>
                <tbody>
                     <tr>
                        <td className="text-left">
                            Albatros (3 strokes under par)
                        </td>
                        <td>+8</td>
                    </tr>
                    <tr>
                        <td className="text-left">
                            Eagle (2 strokes under par)
                        </td>
                        <td>+5</td>
                    </tr>
                    <tr>
                        <td className="text-left">
                            Birdie (1 stroke under par)
                        </td>
                        <td>+2</td>
                    </tr>
                    <tr>
                        <td className="text-left">Par</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td className="text-left">
                            Bogey (1 stroke over par)
                        </td>
                        <td>-1</td>
                    </tr>
                    <tr>
                        <td className="text-left">
                            Double bogey or worse
                            (2 strokes or more over par)
                        </td>
                        <td>-3</td>
                    </tr>
                </tbody>
            </table>`

        sfSystem = `<div className="article__section">
            <div className="rules__scoring">
                <h1 className="title">Stableford scoring</h1>
                <Btn mod="blue-linear x-small" onTap={this.openScoring}>
                View the scoring</Btn>
            </div>
            <ReactCSSTransitionGroup transitionName="slide"
                                     transitionEnterTimeout={500}
                                     transitionLeaveTimeout={500}>
                {scoring ? sfSystemContent : null}
            </ReactCSSTransitionGroup>
        </div>`



        pick18Content =
            `<div className="rules__content">
                <p>
                    <b> Capitalized terms not defined herein shall have the meanings set forth in
                    the Global Fantasy Sports Golf™ Contest Rules
                    &nbsp;(&ldquo;<Link to="/contest-rules/"><a className="stroke x2-medium bold blue cursor">Contest Rules</a></Link>&rdquo;).
                    To the extent that anything in the
                    Contest summary below is inconsistent with the Contest Rules, the Contest Rules shall
                    take precedence and govern in all respects.</b>
                </p>
                <p>
                    <b>The aim of the Contest: </b>
                    A {playButtonNameByType['hole-by-hole']} Contest lasts for two(2) Rounds of a Tournament, You
                    predict if the three (3) Golfers that you select on your Team
                    will play &ldquo;under par&rdquo;,
                    &ldquo;to par&rdquo; or &ldquo;over par&rdquo; for
                    each of the eighteen(18) holes of each of the two(2) selected Rounds.
                    The aim of the Contest is to predict the
                    outcome of each hole for the Golfers you have selected for the chosen Rounds.
                    You will not necessarily win by choosing the best Golfers, but instead you will win by
                    predicting the most number of holes correctly.

                </p>

                <p className="bottom-0"><b>How to play:</b></p>
                <ul className="disc-list">
                    <li>
                        Choose the Tournament that you want to play and two(2) Rounds in that Tournament.
                    </li>
                    <li>
                        Select three (3) Golfers from the list of participants for theat Tournament.
                    </li>
                    <li>Fill in the prediction card for each of the selected Golfers:
                        select &ldquo;under par&rdquo;, &ldquo;on par&rdquo;
                        or &ldquo;over par&rdquo; for each hole.
                        The prediction that you submit for each hole will be applied
                        for both of the Rounds that you have chosen.
                        To clarify, you cannot choose a different result in relation
                        to par for the same hole for each Round.
                    </li>
                    <li>
                        Pay the Entry Fee, which must be paid in dollars or, in the case of a Contest
                        that is free to enter, can be paid with Virtual Tokens.
                    </li>
                    <li>
                        Challenge friends to join the Contest.
                    </li>
                </ul>
                <p>
                    <b>Select when: </b>
                    First select two (2) Rounds in the upcoming Tournament. In cases where there
                    is more than one (1) Tournament available, you must choose the Tournament as well.
                </p>
                <p>
                    <b>Select Golfers: </b>
                    Select three (3) Golfers from the list of participants. Only Golfers who have
                    registered and qualified for the Tournament and its associated
                    Rounds are available for selection.
                </p>
                <p>
                    <b>Filling in the prediction card: </b>
                    For each hole you must predict whether each chosen Golfers plays
                    the applicable hole &ldquo;under par&rdquo;, &ldquo;to par&rdquo; or
                    &ldquo;over par&rdquo;. Birdies, Eagles and
                    Albatrosses all count as &ldquo;under par&rdquo;, pars count as
                    &ldquo;on-par&rdquo;, and bogeys,
                    double-bogeys and triple-bogeys, etc. all count as &ldquo;over par&rdquo;.

                </p>
                <p>
                    <b>Entry Fee: </b>
                    You can choose to play Contests with different Entry Fees. Remember that
                    Contests with higher Entry Fees have bigger Prizes and the Contests with
                    fewer participants will increase your odds of winning.
                </p>

                <p>
                    <b>Prizes: </b>
                    Prizes will be given to the Entrants who predict the highest number of holes correctly.
                </p>
                <div className="rules__play">
                    <BtnLink to="/golf/hole-by-hole/" mod="red-linear x-medium">Play</BtnLink>
                </div>
            </div>`

        pick18 =
            `<div className="article__section" ref="hole-by-hole">
                <div className="rules__top">
                    <div className="rules__logo">
                        <Logo logo='hole-by-hole-small' mod="small"/>
                    </div>
                    <div className="rules__title">
                        <h1 className="title">{playButtonNameByType['hole-by-hole']}:</h1>
                        {pick ?
                            <Btn mod="white-linear x-small" onTap={this.openPick18}>
                            Hide rules</Btn>
                        :
                            <Btn mod="blue-linear x-small" onTap={this.openPick18}>
                            Read the rules</Btn>
                        }
                    </div>
                </div>
                <ReactCSSTransitionGroup transitionName="slide"
                                         transitionEnterTimeout={500}
                                         transitionLeaveTimeout={500}>
                    {pick ? pick18Content : null}
                </ReactCSSTransitionGroup>
            </div>`



        holeInOneContent =`
            <div className="rules__content">
                <p>
                    <b> Capitalized terms not defined herein shall have the meanings set forth in
                    the Global Fantasy Sports Golf™ Contest Rules
                    &nbsp;(&ldquo;<Link to="/contest-rules/"><a className="stroke x2-medium bold blue cursor">Contest Rules</a></Link>&rdquo;).
                    To the extent that anything in the
                    Contest summary below is inconsistent with the Contest Rules, the Contest Rules shall
                    take precedence and govern in all respects.</b>
                </p>
                <p>
                    <b>The aim of the Contest: </b>
                    Choose three (3) Golfers, choose two (2) rounds of an applicable Tournament and score as
                    many &ldquo;modified stableford&rdquo; points as possible. Each Golfer in the
                    Contest will be awarded extra &ldquo;handicap&rdquo; strokes to spend on some of the eighteen (18)
                    holes applicable to each of the Rounds.  The Golfers will be awarded more or less
                    strokes as per the amateur handicap system.  Apply these extra strokes and you may
                    watch your Golfer’s Par become a Birdie, or a Birdie become an Eagle.
                </p>
                <p className="bottom-0"><b>Remember: </b>
                    Unskilled Golfers will be awarded more handicap strokes than skilled Golfers,
                    giving you the chance to pick more holes to apply extra strokes.  When you use a
                    handicap stroke on a hole, a Birdie that would normally be worth +2 Points will be
                    converted into an Eagle a which will earn you +5 Points instead.
                </p>

                <p className="bottom-0"><b>How to play:</b></p>
                <ul className="disc-list">
                    <li>Choose the Tournament that you want to play and select two (2) Rounds in that Tournament.</li>
                    <li>Select three (3) Golfers from the list of participants for that Tournament.</li>
                    <li>Apply handicap strokes to certain holes, as determined by you.</li>
                    <li>Pay the Entry Fee, which must be paid in dollars or, in the case of a Contest that is free to enter, can be paid with Virtual Tokens.</li>
                </ul>

                <p>
                    <b> Select when: </b>
                    First select two (2) Rounds in the upcoming Tournament. In cases
                    where there is more than one (1) Tournament available, you must choose the Tournament as well.

                </p>
                <p>
                    <b> Select Golfers: </b>
                    Select three (3) Golfers from the list of participants.
                    Pay attention to how many handicap strokes they have been awarded.
                    Only Golfers who have registered and qualified for the Tournament and
                    its associated Round are available for selection.
                </p>
                <p>
                    <b> Choose handicap holes: </b>
                    Each Golfer will be awarded with a certain number of handicap strokes.
                    This means that you can choose certain holes and subtract one (1) stroke from the Golfer’s
                    actual stroke count for that hole.  Choose which holes you would like to use
                    the handicap strokes on.
                </p>
                <p>
                    Remember that the Points system is based on the &ldquo;modified stableford&rdquo;
                    points system which rewards good scores more than it punishes bad scores.
                </p>

                <p className="bottom-0"><b>Examples:</b></p>
                <ul className="disc-list">
                    <li>
                        If the Golfer makes a Birdie on the first hole, you will score two (2)
                        Points on this hole, but if you chose to use one (1) of the handicap strokes available
                        for that Golfer on that hole, the score will count as an Eagle, and you will get five (5) Points.
                    </li>
                    <li>
                        If the Golfer makes a bogey on the second hole, you will score -1 Point
                        on that hole, but if you chose to use one (1) of the handicap strokes available for
                        that Golfer on that hole, the score will count as par, and you will get 0 Points.
                    </li>
                </ul>


                <p>
                    <b>Entry Fee: </b>
                    You can choose to play Contests with different Entry Fees.  Remember that
                    Contests with higher Entry Fees have bigger Prizes and the Contests with fewer
                    participants will increase your odds of winning.
                </p>

                <p>
                    <b>Prizes: </b>
                    Prizes are given to the Entrants who accumulate the most
                    &ldquo;modified stableford&rdquo; points from  the chosen Golfers.
                </p>
                <div className="rules__play">
                    <BtnLink to="/golf/handicap-the-pros/" mod="red-linear x-medium">Play</BtnLink>
                </div>
            </div>`

        holeInOne = `<div className="article__section" ref="handicap-the-pros">
            <div className="rules__top">
                <div className="rules__logo">
                    <Logo logo='handicap-the-pros-small' mod="small"/>
                </div>
                <div className="rules__title">
                    <h1 className="title">{playButtonNameByType['handicap-the-pros']}:</h1>
                    {hole ?
                        <Btn mod="white-linear x-small" onTap={this.openHoleInOne}>
                        Hide rules</Btn>
                    :
                        <Btn mod="blue-linear x-small" onTap={this.openHoleInOne}>
                        Read the rules</Btn>
                    }
                </div>
            </div>
            <ReactCSSTransitionGroup transitionName="slide"
                                     transitionEnterTimeout={500}
                                     transitionLeaveTimeout={500}>
                {hole ? holeInOneContent : null}
            </ReactCSSTransitionGroup>
        </div>`



        fiveballContent = `
            <div className="rules__content">
                <p>
                    <b> Capitalized terms not defined herein shall have the meanings set forth in
                    the Global Fantasy Sports Golf™ Contest Rules
                    &nbsp;(&ldquo;<Link to="/contest-rules/"><a className="stroke x2-medium blue bold cursor">Contest Rules</a></Link>&rdquo;).
                    To the extent that anything in the
                    Contest summary below is inconsistent with the Contest Rules, the Contest Rules shall
                    take precedence and govern in all respects.</b>
                </p>
                <p>
                    <b>The aim of the Contest: </b>
                    {playButtonNameByType['five-ball']} is a Contest where the collective score will be the sum
                    of the Points of the five (5) Golfers you choose.  The Contest takes
                    place over two (2) Rounds of a given Tournament.  The Golfers will score
                    Points according to the &ldquo;modified stableford&rdquo; points system.

                </p>

                <p className="bottom-0"><b>How to play:</b></p>
                <ul className="disc-list">
                    <li>Choose the Tournament that you want to play and select two (2) Rounds in that Tournament.</li>
                    <li>Select five (5) Golfers from the list of participants for that Tournament.</li>
                    <li>Pay the Entry Fee, which must be paid in dollars or, in the case of a Contest that is free to enter, can be paid with Virtual Tokens.</li>
                </ul>

                <p>
                    <b>Select when: </b>
                    First select two (2) Rounds in the upcoming Tournament.
                    In cases where there is more than one (1) Tournament available,
                    you must choose the Tournament as well.
                </p>

                <p>
                    <b>Remember: </b>
                    All Golfers in the applicable Tournament will be separated into tiers consisting of
                    ten (10) Golfers, depending on their relative world rankings (Tier 1, Tier 2, Tier 3, etc.).
                    You may only select one (1) Golfer from Tier 1, one (1) Golfer from Tier 2, one (1) Golfer
                    from Tier 3, one (1) Golfer from Tier 4 and one (1) Golfer from Tier 5; provided, however, that
                    where you do not select a Golfer from a higher Tier, you may select one (1) additional Golfer
                    from a lower Tier.  For example, if you do not select a Golfer from Tier 1, you may select two
                    (2) Golfers from Tier 2, or a second Golfer from any other lower Tier..
                </p>

                <p>
                    <b>Select Golfers: </b>
                    Choose five (5) Golfers from the list of participants.
                    Only Golfers who have registered and qualified for the Tournament
                    are available for selection.
                </p>

                <p>
                    <b>Entry Fee: </b>
                    You can choose to play Contests with different Entry Fees.  Remember that Contests with higher
                    Entry Fees have bigger Prizes and the Contests with fewer
                    participants will increase your odds of winning.
                </p>

                <p>
                    <b>Prizes: </b>
                    Prizes are given to the Entrants who accumulate the most &ldquo;modified stableford&rdquo;
                    points from the five (5) chosen Golfers.  The Prizes will be established in advance,
                    and may depend on the amount of the Entry Fee and Contest room size.
                </p>
                <div className="rules__play">
                    <BtnLink to="/golf/five-ball/" mod="red-linear x-medium">Play</BtnLink>
                </div>
            </div>`

        fiveball = `<div className="article__section" ref="five-ball">
            <div className="rules__top">
                <div className="rules__logo">
                    <Logo logo='five-ball-small' mod="small"/>
                </div>
                <div className="rules__title">
                    <h1 className="title">{playButtonNameByType['five-ball']}:</h1>
                    {five ?
                        <Btn mod="white-linear x-small" onTap={this.openFiveBall}>
                        Hide rules</Btn>
                    :
                        <Btn mod="blue-linear x-small" onTap={this.openFiveBall}>
                        Read the rules</Btn>
                    }
                </div>
            </div>
            <ReactCSSTransitionGroup transitionName="slide"
                                     transitionEnterTimeout={500}
                                     transitionLeaveTimeout={500}>
                {five ? fiveballContent : null}
            </ReactCSSTransitionGroup>
        </div>`

        `<section className="rules-wrap">
            <article className="article rules">
                {pick18}
                {holeInOne}
                {fiveball}
                {sfSystem}
                <div className="rules__bottom">
                    <BtnLink to="__back" mod="white-linear wide">Exit</BtnLink>
                </div>
            </article>
        </section>`
