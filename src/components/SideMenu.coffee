_ = require('underscore')
moment = require('moment')

{MenuLink, Money, Btn, BtnLink, Link} = require('../lib/ui')
FakeTourButtons = require('../lib/ui/FakeTourButtons')
CurrentWeekInfo = require('../lib/ui/CurrentWeekInfo')
{STATE} = require('../lib/const')
{cx} = require('lib/utils')

ReactCSSTransitionGroup = require('react-addons-css-transition-group')


DateTime = React.create(
    displayName: 'DateTime'

    mixins: [
        flux.Connect('connection', (store, props, state, self) ->
            counter = state.counter or 0
            isTimeRunning = store.getState().state == STATE.PROCESSING
            if isTimeRunning and isTimeRunning != state.isTimeRunning and self
                self.timerRunning = setInterval(=>
                    counter = counter + 60 * 1000
                , 30)
            else if not isTimeRunning and isTimeRunning != state.isTimeRunning and self
                clearInterval(self.timerRunning)
                counter = 0
            {isTimeRunning, counter}
        )
        flux.Connect('time', (store, props, state, self) =>
            if self
                self.timerSemicolon = setTimeout((=> self.setState({semicolon: '\u00a0'})), 500)

            now = +store.getNow()
            datetime = moment(now + (state.counter or 0))
            date = datetime.format('ddd, MMM D, YYYY')
            if date != state.date and self
                self.startDateMotion()
            ampm = datetime.format('a')

            {date, datetime, ampm, semicolon: ':'}
        )
    ]

    getInitialState: ->
        isDateChanged: false
        animationStarted: false

    startDateMotion: ->
        @setState({isDateChanged: true})
        setTimeout((=> @setState({animationStarted: true})), 1)
        setTimeout(@removeMotion, 5000)

    removeMotion: ->
        @setState({isDateChanged: false, animationStarted: false})

    componentWillUnmount: ->
        @stopSemicolon()

    stopSemicolon: ->
        clearInterval(@timerSemicolon)

    render: ({}, {semicolon, counter, date, datetime, ampm, isDateChanged, animationStarted}) ->
        time = datetime.format('hh' + semicolon + 'mm')

        dateClassNames = cx({
            'pageslide-slide-date__date': true,
            'to-start': isDateChanged,
            'go': animationStarted
        })

        `<div className="pageslide-slide-date">
          <p className={dateClassNames}>{date}</p>
          <p className="pageslide-slide-date__time">{time}
            <span className="pageslide-slide-date__time-small">{ampm}</span>
          </p>
        </div>`
)


SideMenu = React.create(
    displayName: 'SideMenu'

    mixins: [
        flux.Connect("users", (store) ->
            user: store.getCurrentUser()
            avatar: store.getUserAvatar()
            userName: store.getUserName()
            balance: store.getUserBalance()
        )
        flux.Connect("games", (store) ->
            gamesCount: store.getActiveGamesCount()
        )
        flux.Connect('state', (store) -> {
            open: store.getMenuState()
        })
    ]

    handleTap: ->
        flux.actions.state.toggleSideMenu(false)

    handleLogout: (e) ->
        e.preventDefault()
        flux.actions.connection.logout()

    render: (P, {avatar, balance, gamesCount, myGames, open, user, userName}) ->

        `<ReactCSSTransitionGroup
            id="pageslide-slide-wrap"
            component="section"
            transitionName="appear"
            transitionEnterTimeout={700}
            transitionLeaveTimeout={700}
        >{open ?
            <div className="pageslide-slide-content" onTap={this.handleTap}>
                <div className="pageslide-slide-option">
                    <div className="pageslide-slide-text">
                        <span className="stroke x3-medium white bold">Options</span>
                    </div>
                    <Btn mod="sidebar close-aside-menu" icon="cancel-white" onTap={this.handleTap}>&nbsp;</Btn>
                </div>
                <div className="pageslide-slide-header">
                    <div className="pageslide-slide-top">
                        <img src={avatar} height="50" width="50" />
                        <span name="username">{userName || "Mister Manager"}</span>
                    </div>

                    <DateTime />

                    <FakeTourButtons />

                    <div className="aside-nav">
                        <MenuLink to="/useraccount/">Account Settings</MenuLink>
                        <MenuLink to="/useraccount/">Subscription Settings</MenuLink>
                    </div>

                    <div className="pageslide-slide-button">
                        <BtnLink mod="green" to="/deposit/">Add funds</BtnLink>
                        <BtnLink mod="green" to="/withdraw/">Withdraw funds</BtnLink>
                    </div>
                </div>

                <div className="aside-menu">
                    <div className="aside-menu__tab active" id="tab4">
                        <div className="account-info">
                            {gamesCount > 0 &&
                            <div className="account-info__item">
                                <span className="stroke medium bold">
                                    My current games:&nbsp;
                                </span>
                                <span className="stroke x3-medium white bold">
                                    {gamesCount}
                                </span>
                            </div>}
                            <div className="account-info__item">
                                <span className="stroke medium bold">
                                    Account:&nbsp;
                                </span>
                                <Money m={balance} mod="medium" symb="medium" />
                            </div>
                        </div>

                        <div className="aside-menu__dates">

                            <Btn onTap={this.handleLogout} mod="logout">Logout</Btn>

                            <MenuLink to="/policy/">Privacy Policy</MenuLink>
                            <MenuLink to="/terms/">Terms and Conditions</MenuLink>
                        </div>

                    </div>
                </div>
            </div>
        : null}</ReactCSSTransitionGroup>`
)

module.exports = SideMenu
