{Pie, PieChart, Sector} = require('recharts')


RadialChart = React.create
    displayName: 'RadialChart'

    propTypes: {
        value: React.PropTypes.number.isRequired
        max: React.PropTypes.number.isRequired
        width: React.PropTypes.number.isRequired
        formatter: React.PropTypes.func
        activeFill: React.PropTypes.string
        emptyFill: React.PropTypes.string
        textColor: React.PropTypes.string
    }

    defaultProps: {
        formatter: (value) -> value
        textColor: "#ffffff"
    }

    renderActiveShape: (props) ->
        {formatter, textColor} = @props

        fontSize = props.innerRadius / 2
        value = formatter(props.payload.value)

        `<g>
            <text x={props.cx} y={props.cy} dy={fontSize / 2.5}
                textAnchor="middle" fill={textColor}
                style={{fontSize: fontSize + "px"}}>{value}</text>
            <Sector {...props} />
        </g>`

    render: ({ value, max, width, formatter, activeFill, emptyFill }) ->
        activeFill = activeFill || "#456990"
        emptyFill = emptyFill || "#f45b69"
        width = width || 100
        angle = Math.round((value / max) * 360) + 90

        data = [{value}, {value: 100}]
        `<section className="radial-chart">
            <PieChart width={width} height={width}>
                <Pie
                    formatter={formatter}
                    activeIndex={0}
                    activeShape={this.renderActiveShape}
                    data={data}
                    startAngle={90}
                    endAngle={angle}
                    innerRadius="65%"
                    outerRadius="95%"
                    fill={activeFill}
                    stroke={activeFill}
                    isAnimationActive={false} />

                <Pie
                    activeIndex={0}
                    activeShape={this.renderActiveShape}
                    data={data}
                    startAngle={angle - 360}
                    endAngle={90}
                    innerRadius="65%"
                    outerRadius="95%"
                    fill={emptyFill}
                    stroke={emptyFill}
                    isAnimationActive={false} />
            </PieChart>
        </section>`


module.exports = RadialChart
