_ = require('underscore')
{ComposedChart, Area, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend} = require('recharts')

SimpleLineChart = React.create(
    displayName: 'SimpleLineChart'

    render: ({data, width, height}) ->
        points = []
        keys = _(data[0].points).keys()
        for key in keys
            points.push({
                label: key
                value1: data[0].points[key]
                value2: data[1].points[key]
            })

        `<ComposedChart width={width} height={height} data={points}
            margin={{top: 10, right: 0, left: 0, bottom: 10}}
            isAnimationActive={false}
        >
            <XAxis dataKey="name" />
            <YAxis hide />
            <CartesianGrid strokeWidth="1" />
            <Line type="linear"
                  isAnimationActive={false}
                  dataKey="value1"
                  stroke={data[0].fill}
                  strokeWidth="2"
                  dot={{stroke: '#e4e4e4', fill: '#e4e4e4', strokeWidth: 6}} />
            <Area type="linear"
                  isAnimationActive={false}
                  dataKey="value2"
                  stroke={data[1].fill} />
        </ComposedChart>`
)

module.exports = SimpleLineChart
