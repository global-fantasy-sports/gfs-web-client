{RadialBarChart, RadialBar, Legend} = require('recharts')


START_ANGLE = 115
END_ANGLE = 425

SIZE = 120

BAR_WIDTH = 6

BG_STYLE = {
    fill: '#333'
    stroke: '#333'
    opacity: 0.3
}


RadialProgressLegend = ({labelTop, label}) ->
    `<div className="radial-progress-legend">
        <div className="radial-progress-legend__top">{labelTop}</div>
        <div className="radial-progress-legend__center">{label}</div>
        <div className="radial-progress-legend__bottom" />
    </div>`


class RadialProgressChart extends React.Component

    @propTypes: {
        barColor: React.PropTypes.string
        value: React.PropTypes.number.isRequired
        max: React.PropTypes.number.isRequired
        label: React.PropTypes.node
        labelTop: React.PropTypes.node
    }

    @defaultProps: {
        barColor: 'white'
    }

    getData: () =>
        {value, max, barColor} = @props

        return [
            {name: 'Budget', value: Math.max(0, value), fill: barColor, stroke: barColor}
            {name: 'max', value: max, stroke: 'transparent', fill: 'transparent', style: {display: 'none'}}
        ]

    render: () ->
        {label, labelTop} = @props

        `<RadialBarChart
            className="radial-progress-chart"
            data={this.getData()}
            width={SIZE}
            height={SIZE}
            barSize={7}
            innerRadius={SIZE / 2 - 22}
        >
            <RadialBar
                startAngle={START_ANGLE}
                endAngle={END_ANGLE}
                maxAngle={Math.abs(END_ANGLE - START_ANGLE)}
                label={false}
                background={BG_STYLE}
                dataKey="value"
                isAnimationActive={false}
            />

            <Legend
                width={SIZE}
                height={SIZE}
                layout="vertical"
                verticalAlign="middle"
                content={<RadialProgressLegend labelTop={labelTop} label={label} />}
            />
        </RadialBarChart>`


module.exports = RadialProgressChart
