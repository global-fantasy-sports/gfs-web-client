shader = require('shader')


`/**
 * @fill Color of active part of the chart
 * @opacity Opacity of active part of the chart
 * @emptyFill Color of background (not active) part of the chart
 * @emptyOpacity Opacity of background (not active) part of the chart
 */`
VolumeChart = React.create
    displayName: 'VolumeChart'

    propTypes: {
        width: React.PropTypes.number.isRequired
        height: React.PropTypes.number.isRequired
        value: React.PropTypes.number.isRequired
        max: React.PropTypes.number.isRequired
        fill: React.PropTypes.string
        emptyFill: React.PropTypes.string
        opacity: React.PropTypes.number
        emptyOpacity: React.PropTypes.number
    }

    render: ({width, height, value, max, fill, emptyFill, opacity, emptyOpacity}) ->
        opacity = opacity || 1
        emptyOpacity = emptyOpacity || opacity
        fill = fill || "#333"
        emptyFill = emptyFill || shader(fill, 0.5)

        tick = height / 20
        ticks = [0..(max - 1)].map((i) ->
            top = tick * i
            bottom = (tick * (i + 1))
            tickHeight = (bottom - top) / 2
            color = emptyFill
            op = emptyOpacity

            if value > i
                color = fill
                op = opacity

            `<rect key={i}
                width={width}
                height={tickHeight}
                x={0}
                y={top}
                fill={color}
                fillOpacity={op}
            />`
        )


        `<section className="volume-chart">
            <svg width={width} height={height}>
                {ticks}
            </svg>
        </section>`


module.exports = VolumeChart
