module.exports = React.create(
    displayName: "BarChart"

    render: ({ data, maxValue, maxWidth}) ->
        {fill, value} = data
        width = (value / maxValue) * maxWidth


        `<section className="barchart" style={{width: maxWidth}}>
            <div className="barchart__line" style={{width: width}}></div>
        </section>`
)
