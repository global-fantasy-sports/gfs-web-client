{AreaChart, Area} = require('recharts')


class TinyLineChart extends React.Component

    @defaultProps: {
        width: 150
        height: 50
        color: '#00ff00'
    }

    render: () ->
        {data, width, height, color} = @props

        data = data.map((value) -> {value})

        `<AreaChart
            width={width}
            height={height}
            data={data}
        >
            <Area
                type="linear"
                dataKey="value"
                dot={false}
                stroke={color}
                strokeWidth={2}
                fill={color}
                fillOpacity={0.3}
                isAnimationActive={false}
            />
        </AreaChart>`


module.exports = TinyLineChart
