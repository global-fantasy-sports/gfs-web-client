_ = require('underscore')
{cx} = require('lib/utils')

DynamicBarChart = React.create
    displayName: 'DynamicBarChart'

    propTypes: {
        data: React.PropTypes.object.isRequired
        maxWidth: React.PropTypes.number.isRequired
        maxValue: React.PropTypes.number.isRequired
        height: React.PropTypes.number.isRequired
        startLabel: React.PropTypes.string
        endLabel: React.PropTypes.string
        modifier: React.PropTypes.string
    }

    render: ({ data, maxValue, maxWidth}) ->
        {fill, value} = data
        width = ((value / maxValue) * maxWidth) + "%"

        bar = `<div className="dynamic-barchart" style={{width: maxWidth}}>
            <div className="dynamic-barchart__bar" style={{width: width, backgroundColor: fill}}/>
        </div>`

        `<section className="dynamic-barchart">
            {bar}
        </section>`


module.exports = DynamicBarChart
