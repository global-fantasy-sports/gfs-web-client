_ = require('underscore')
shader = require('shader')
{Area, AreaChart, CartesianAxis, CartesianGrid, Line, ReferenceLine, Tooltip, XAxis, YAxis} = require('recharts')


renderDot = (data, width) ->
    lastProps = null
    (props) ->
        { cx, cy, fill } = props
        if cx == width
            prevCx = lastProps.cx
            prevCy = lastProps.cy
            prevX = -(cx - prevCx)
            prevY = -(cy - prevCy)
            color = shader(fill, -0.5)
            max = Math.max(Math.abs(prevX), Math.abs(prevY))
            firstStop = {stopColor: color, stopOpacity: 1}
            lastStop = {stopColor: fill, stopOpacity: 0}

            BC = Math.abs(cy - prevCy)
            AC = Math.abs(cx - prevCx)
            AB = Math.round(Math.sqrt(BC * BC + AC * AC))

            angle = Math.atan(AC / BC) * (180 / Math.PI)
            if cy > prevCy
                angle = 180 - angle

            transform = "rotate(#{angle + 90}, #{max}, #{max})"

            `<svg x={cx-max} y={cy-max}>
                <defs>
                    <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
                        <stop offset="0%" style={firstStop} />
                        <stop offset="100%" style={lastStop} />
                    </linearGradient>
                </defs>
                <circle cx={max} cy={max} r={5} fill={color} />
                <rect
                    x={max-1} y={max-1}
                    width={AB} height={3}
                    fill="url(#grad1)"
                    transform={transform}
                />
            </svg>`
        else
            lastProps = Object.assign({}, props)
            null


`/**
* @data Array of two element containing data of each area
* @data[n].fill Color of the nth area
* @data[n].opacity Opacity of the nth area
* @data[n].points Collection of chart points, where key is X axis label, value is Y axis value
* @max Maximum value of Y axis
* @tick Color of X axis label
*/`
DoubleAreaChart = React.create
    displayName: 'DoubleAreaChart'

    propsTypes: {
        data: React.PropTypes.array.isRequired
        max: React.PropTypes.number.isRequired
        min: React.PropTypes.number.isRequired
        width: React.PropTypes.number.isRequired
        height: React.PropTypes.number.isRequired
    }

    render: ({ data, max, min, width, height, tick }) ->
        points = []
        keys = _(data[0].points).keys()
        for key in keys
            points.push({
                label: key
                value1: data[0].points[key]
                value2: data[1].points[key]
            })

        if tick
            tick = {stroke: 'none', fill: tick}

        `<section className="double-area-chart">
            <AreaChart width={width} height={height} data={points}>
                <CartesianGrid
                    strokeDasharray="3 3"
                    vertical={false}
                    horizontalPoints={[10, 20, 30]}
                />

                <XAxis dataKey="label" tick={tick} />
                <YAxis hide domain={[min, max]} />

                <Area
                    type="linear"
                    dataKey="value1"
                    stroke="none"
                    fill={data[0].fill}
                    fillOpacity={data[0].opacity}
                    isAnimationActive={false}
                />

                <Area
                    type="linear"
                    dataKey="value2"
                    stroke="none"
                    fill={data[1].fill}
                    fillOpacity={data[1].opacity}
                    dot={renderDot(data, width - 10)}
                    isAnimationActive={false}
                />

            </AreaChart>
        </section>`

module.exports = DoubleAreaChart
