{Link} = require('lib/ui')


if SPORT is "golf"
    HeaderNav = () ->
        `<div className="head__bottom">
            <div className="head__row">
                <div className="head__left" />
                <nav className="head__games-list">
                    <Link to={"/" + SPORT + "/"} className="head__games-item" tag="a">Home</Link>
                    <Link to={"/" + SPORT + "/games/"} className="head__games-item" tag="a">My Games</Link>
                    <Link to={"/" + SPORT + "/schedule/"} className="head__games-item" tag="a">Tournament Info</Link>
                    <Link to={"/" + SPORT + "/competitions/"} className="head__games-item" tag="a">Competition Rooms</Link>
                    <Link to={"/" + SPORT + "/rules/"} className="head__games-item" tag="a">Game Rules</Link>
                </nav>
            </div>
        </div>`

else if SPORT in ['nfl', 'nba']
    HeaderNav = () ->
        `<div className="head__bottom">
            <div className="head__row">
                <div className="head__left" />
                <nav className="head__games-list">
                    <Link to={"/" + SPORT + "/games/"} className="head__games-item" tag="a">My Games</Link>
                    <Link to={"/" + SPORT + "/lineups/"} className="head__games-item" tag="a">My Line-ups</Link>
                    <Link to={"/" + SPORT + "/players/"} className="head__games-item" tag="a">Players list</Link>
                    <Link to={"/" + SPORT + "/schedule/"} className="head__games-item" tag="a">Schedule</Link>
                    <Link to={"/" + SPORT + "/competitions/"} className="head__games-item" tag="a">Competition Rooms</Link>
                    <Link to={"/" + SPORT + "/rules/"} className="head__games-item" tag="a">Game Rules</Link>
                </nav>
            </div>
        </div>`

HeaderNav.displayName = 'HeaderNav'


module.exports = HeaderNav
