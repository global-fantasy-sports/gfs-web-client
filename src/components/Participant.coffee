{Icon} = require('../lib/ui')
{cx, bem} = require('../lib/utils')

Popup = require('../lib/ui/Popup')
PlayerStatsDialog = require('./stats/PlayerStatsDialog')
InjuryStatusBadge = require('./InjuryStatusBadge')


module.exports = React.create
    displayName: "Participant"

    propTypes: {
        disabled: React.PropTypes.bool.isRequired
        selected: React.PropTypes.bool.isRequired
        participant: React.PropTypes.object.isRequired
        overpriced: React.PropTypes.bool.isRequired
    }

    mixins: [
        flux.Connect('participants', (store, props) -> isFavorite: store.getParticipant(props.participant.id)?.isFavorite)
    ]

    getInitialState: -> {
        stats: false
    }

    handleToggle: (e) ->
        e.preventDefault()
        if not @props.disabled
            flux.actions.roster.toggleParticipant(@props.participant.id)

    handleTap: (e) ->
        if not e.isDefaultPrevented() && not @props.selected
            @setState({stats: not @state.stats})

    handleHideStats: () ->
        @setState({stats: false})

    handleDialogButton: () ->
        if @props.selected or not @props.disabled
            flux.actions.roster.toggleParticipant(@props.participant.id)
            @setState({stats: false})

    render: ({participant, selected, disabled, overpriced}) ->
        rootCls = cx("participant-item", {
            "participant-item--active": selected
            "participant-item--disabled": disabled
        })

        itemCls = bem('participant-item__el')

        clsStar = cx(
            "star-orange disabled": @state.isFavorite
        )

        buttonState =
            if selected then 'remove'
            else if disabled then 'disabled'
            else 'add'

        `<div className={rootCls} onTap={this.handleTap}>
            <div className={itemCls('fav') + " w-3"}>
                <Icon i={clsStar} />
            </div>
            <div className={itemCls() + " w-10"}>
                {participant.position}
            </div>
            <div className={itemCls('name') + " w-35"}>
                {participant.getName()}
                <InjuryStatusBadge player={participant} />
            </div>
            <div className={itemCls() + " w-10"}>
                {participant.opponent}
            </div>
            <div className={itemCls() + " w-10"}>
                {participant.formatRank()}
            </div>
            <div className={itemCls() + " w-10"}>
                {participant.formatFPPGstr()}
            </div>
            <div className={itemCls(selected ? '' : (overpriced ? 'red' : 'green')) + " w-15"}>
                {participant.formatSalary()}
            </div>
            <div className={itemCls(disabled ? 'disabled' : '') + " w-10"}>
                <Icon i={selected ? 'minus' : 'plus'} onTap={this.handleToggle} />
            </div>

            {this.state.stats && participant.id &&
                <Popup>
                    <PlayerStatsDialog
                        participantId={participant.id}
                        button={buttonState}
                        onClick={this.handleDialogButton}
                        onHide={this.handleHideStats}
                    />
                </Popup>
            }
        </div>`
