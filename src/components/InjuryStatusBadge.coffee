{cx} = require('../lib/utils')
{INJURY} = require('./const')


class InjuryStatusBadge extends React.Component

    render: () ->
        {player} = @props
        return null unless player.isInjured()
        color = player.getInjuredColor()
        injureCss = cx('badge', "badge--#{color}")

        `<abbr className={injureCss} title={INJURY[player.injured]}>
            INJ
        </abbr>`


module.exports = InjuryStatusBadge
