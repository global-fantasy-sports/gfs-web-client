{BtnLink} = require('../lib/ui')
{Titlebar} = require('../lib/ui/Titlebar')
{cx} = require('lib/utils')

module.exports = React.create
	displayName: 'ContestRules'

	render: () ->
		cls = cx({"regular medium", "green"})

		`<section className="underlay-wrap">
            <article className="article">
                <div className="article__section">
                    <div className="article__head">
                        <div className="article__title">
                            <Titlebar title="Global Fantasy Sports Golf™ Contest Rules"/>
                        </div>
                        <div className="article__close">
                            <BtnLink to="__back" mod={cls}>Back</BtnLink>
                        </div>
                    </div>
                    <p>
                        The Global Fantasy Sports Golf™ contests (collectively, the &ldquo;Contests&rdquo;), made available by and
                        through the Global Fantasy Sports Golf™ website located at <a href="http://golf.globalfantasysports.com" target="_blank"> golf.globalfantasysports.com
                    </a> (the &ldquo;Site&rdquo;),
                        are operated by Global Fantasy Sports, Inc. (&ldquo;Global Fantasy Sports Golf™,&rdquo; &ldquo;we,&rdquo; &ldquo;our&rdquo; or &ldquo;us&rdquo;).
                        These Global Fantasy Sports Golf™ Contest Rules (the &ldquo;Contest Rules&rdquo;), as well as the terms, conditions
                        and Contest features that apply to each Global Fantasy Sports Golf™ Contest individually as made available
                        on the registration page applicable to each Contest (the &ldquo;Competition Room Rules&rdquo; and together
                        with these Contest Rules, the &ldquo;Rules&rdquo;), shall govern your ability to earn Prizes (as defined below)
                        in connection with your participation in the Contests as a contestant (&ldquo;Entrant&rdquo;).  Capitalized
                        terms that are used but not defined herein shall have the meaning set forth in the Global Fantasy Sports Golf™
                        Terms and Conditions (&ldquo;Terms&rdquo;).
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">Global Fantasy Sports Golf™ Agreements</h3>
                    <p>In addition to the Rules, the Contests, and each Entrant’s participation
                        in the Contests, shall be governed by all applicable Global Fantasy Sports Golf™ agreements including,
                        without limitation, the Terms and the Global Fantasy Sports Golf™ Privacy Policy made available on
                        the Site (collectively, &ldquo;Global Fantasy Sports Golf™ Agreements&rdquo;).  The Rules are hereby incorporated
                        into the Global Fantasy Sports Golf™ Agreements, and any and all terms and conditions contained therein
                        shall apply to the Rules.  Where there is a conflict between the terms and conditions of
                        the Rules and those of the Global Fantasy Sports Golf™ Agreements, to the extent that they apply to a
                        Contest, the Rules shall govern in all respects.
                    </p>
                    <p>Please review the Rules, and all applicable Global Fantasy Sports Golf™ Agreements, carefully, prior
                        to participating in a Contest.  If you do not agree to all of the terms contained
                        in the Rules, and all applicable Global Fantasy Sports Golf™ Agreements, in their respective
                        entirety, you are not authorized to become an Entrant in any Contest.
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">Eligibility/Necessary Equipment</h3>
                    <p>The Contests are available only to individuals that: (a) are legal
                        residents of, and that physically reside in, the United States (with restrictions
                        applying to residents of the <b>States of Arizona, Hawaii, Illinois, Iowa, Louisiana, Montana, Nevada,
                            North Dakota, New York, Tennessee, Texas, Vermont and Washington, as well as Puerto Rico
                            and where otherwise prohibited by law)</b>; (b) are at least: (i) eighteen (18)
                        years of age; or (ii) the age of majority in their respective jurisdictions
                        at the time of entry, if the age of majority is greater than eighteen (18)
                        years of age; (c) can enter into legally binding contracts under applicable
                        law; and (d) are not associated with a professional athlete and/or Association
                        (as defined below) that is/are involved in any Contest in a way that creates,
                        or could create, an unfair advantage in any Contest including, without
                        limitation, Association officials, referees, owners and/or employees, as
                        well as a professional athlete’s management, agent(s), caddies and/or medical
                        advisors.  Residents of the <b>States of Arizona, Haawaii, Illinois, Iowa, Louisiana, Montana, Nevada, North Dakota,
                            New York, Tennessee, Texas, Vermont and Washington, as well as Puerto Rico, and where otherwise
                            restricted by law</b>, may only participate in the Free Contests (as defined below).
                    </p>
                    <p>In order to become an Entrant in a Contest, you must either be a registered User,
                        or submit a registration form by and through the Site in order to apply to become a User.
                        Global Fantasy Sports Golf™ reserves the right, in its sole discretion, to deny access to the Contests
                        to anyone at any time and for any reason, whatsoever.  The registration data that you must
                        supply in order to become a registered User, and then an Entrant in a Contest, may include,
                        without limitation, your: (a) full name; (b) state and country of residence; (c) e-mail
                        address (including the e-mail address associated with your PayPal® account, where applicable);
                        (d) password; (e) billing/mailing address (where entering a Paid Contest, as defined below);
                        (f) credit card information (where selected as your preferred payment method); (g) Social
                        Security Number (for potential Prize winners); (h) picture proof of identification, which
                        may include a driver’s license, passport, voting card or similar government issued
                        identification (for potential Prize winners); and/or (i) any other information requested
                        on the applicable Form.  In addition, Users may register for an Account by and through
                        their Facebook® accounts, Google+® account or any other means designated by Global Fantasy Sports Golf™.
                        Where you register via your Facebook® account, Global Fantasy Sports Golf™ will have access to your: (i)
                        Facebook® public profile; (ii) the e-mail address associated  with your Facebook® account;
                        and (iii) any and all other information made available to Global Fantasy Sports Golf™ by and through your
                        Facebook® account.  Where you register via your Google+®  account, Global Fantasy Sports Golf™ will have
                        access to your: (A) basic Google+®   profile information; (B) the e-mail address associated
                        with your Google+®  account; and (C) any and all other information made available to
                        Global Fantasy Sports Golf™ by and through your Google+®  account.
                    </p>
                    <p>Only one (1) Account, and one (1) entry in each Contest, is allowed per person.
                        You shall be responsible, at all times, for ensuring that you have an Internet
                        connection, computer/mobile device and/or other equipment necessary to access the Contests.
                    </p>
                    <p><b>HOW FAR GOLF™ IS NOT RESPONSIBLE FOR ATTEMPTED CONTEST REGISTRATIONS
                        THAT ARE LOST, LATE, ILLEGIBLE, MISDIRECTED, DAMAGED, INCOMPLETE OR INCORRECT, OR FOR
                        YOUR FAILURE TO PARTICIPATE IN, OR COMPLETE, A CONTEST.  IF YOU FAIL TO ENTER
                        A CONTEST INCLUDING, WITHOUT LIMITATION, WHERE YOU CANNOT ACCESS THE SITE FOR
                        ANY REASON WHATSOEVER, YOU WILL NOT BE ABLE TO PARTICIPATE IN THAT PARTICULAR CONTEST.</b></p>
                    <p>Facebook® is a registered trademark of Facebook, Inc. (&ldquo;Facebook&rdquo;).
                        Google+® is a registered trademark of Google, Inc. (&ldquo;Google&rdquo;).
                        Please be advised that Global Fantasy Sports Golf™ is not in any way affiliated
                        with either Facebook or Google, nor are the Global Fantasy Sports Golf™ Offerings or
                        Contests endorsed, administered or sponsored by either Facebook or Google.
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">Contests</h3>
                    <p><b>General:</b></p>
                    <p>Each Contest may contain certain terms, conditions and restrictions,
                        including the minimum/maximum number of Entrants who may participate, the
                        types of Prizes (Virtual Tokens, as defined below, or monetary prizes), number
                        of Prize winners and the applicable scoring system associated with each
                        Contest, as set forth in the applicable Competition Room Rules.
                        In addition, Entrants may join Contests with random opponents,
                        or invite friends to compete one-on-one or in larger groups.  Users
                        can invite friends on their own, or utilize Global Fantasy Sports Golf’s™ social media
                        invitation functionality that enables Users to invite friends via their
                        Facebook® accounts, Google+® accounts, Twitter® accounts or other designated
                        social media accounts and/or methods.  The Entrants shall be accepted in
                        Contests for which they are eligible on a first-come, first-served basis.
                    </p>
                    <p>Twitter® is a registered trademark of Twitter, Inc. (&ldquo;Twitter&rdquo;).
                        Please be advised that Global Fantasy Sports Golf™ is not in any way affiliated with Twitter,
                        nor are the Global Fantasy Sports Golf™ Offerings or Contests endorsed,
                        administered or sponsored by Twitter.</p>
                    <p>All Contests shall be posted on the Site as they
                        become available for participation.  Posting times may vary
                        depending on the time and day of the first tee time associated
                        with each Round (as defined below) applicable to the Contest</p>
                    <p>Each Contest will involve a set number of individual golfers
                        (each, a &ldquo;Golfer&rdquo;), as set forth in the Competition Room Rules, to be selected by the Entrant.
                        In connection with each Contest, Entrants must choose, at a minimum, three (3) Golfers to
                        join their team (&ldquo;Team&rdquo;), and at least two (2) different Rounds in which their Team will compete,
                        in a given tournament (&ldquo;Tournament&rdquo;) sponsored by or associated with the Professional
                        Golfers Association of America (&ldquo;PGA®&rdquo;) or any other professional golf organization
                        designated by Global Fantasy Sports Golf™ (each, an &ldquo;Association&rdquo;).  Depending on the Competition
                        Room Rules, Entrants may be able to change their selected Golfers up until the applicable
                        Contest’s start time (&ldquo;Start Time&rdquo;).  The exact roster size of Golfers for each Team
                        in any given Contest will be predetermined and set forth in the applicable Competition
                        Room Rules.  If you have not selected the requisite number of Golfers as of the
                        applicable Start Time, you will not be entered into that Contest.
                    </p>
                    <p><b>IT IS PROHIBITED FOR TWO (2) OR MORE ENTRANTS TO COLLABORATE
                        DURING A CONTEST OR TO ADOPT A STRATEGY (BEFORE OR DURING A CONTEST)
                        IN ORDER TO GAIN AN ADVANTAGE OVER, AND/OR TO DISADVANTAGE,
                        OTHER ENTRANTS.</b>
                    </p>
                    <p>The Professional Golfers Association of America and PGA®
                        are registered trademarks of the Professional Golfers Association of America.
                        Please be advised that Global Fantasy Sports Golf™ is not in any way affiliated with the
                        Professional Golfers Association of America or any other Association, and
                        neither Global Fantasy Sports Golf™ nor the Contests are endorsed or sponsored by the Professional
                        Golfers Association of America or any other Association.
                    </p>

                    <p><b>Entry Fees/Free Contests:</b></p>
                    <p>
                        <ul className="disc-list">
                            <li>In order to enter a paid Contest (&ldquo;Paid Contest&rdquo;), you must have
                                enough money in your Account to pay the entry fee required to enter the applicable
                                Contest (&ldquo;Entry Fee&rdquo;).  You can enter as many Paid Contests as you want, provided
                                that you have sufficient funds in your Account for the applicable Entry Fees.
                                The Entry Fee for each Contest shall be set forth in the Competition Room Rules.
                                Each Entrant’s Account will be debited the applicable Entry Fee when he/she
                                completes his/her Golfer selections. You can deposit money into your Account via
                                either PayPal® or a credit/debit card.  Charges will appear on your PayPal®
                                statement or credit/debit card statement, as applicable, through the identifier
                                &ldquo;Global Fantasy Sports.&rdquo;  <b>OTHER THAN AS EXPRESSLY SET FORTH HEREIN, ALL ENTRY FEES ARE NON-REFUNDABLE.</b>
                            </li>
                            <li>The Entry Fees are quoted in U.S. Dollars and are payable in U.S. Dollars.
                                Subject to the conditions set forth herein, you agree to be bound by the Billing Provisions
                                of Global Fantasy Sports Golf™ in effect at any given time.  Upon reasonable prior written notice to you
                                (with e-mail sufficing), Global Fantasy Sports Golf™ reserves the right to change its Billing Provisions
                                whenever necessary, in its sole discretion.  Continued participation in the Contests after
                                receipt of such notice shall constitute consent to any and all such changes; provided,
                                however, that any amendment or modification to the Billing Provisions shall not
                                apply to any charges incurred prior to the applicable amendment or modification.
                            </li>
                            <li>Global Fantasy Sports Golf’s™ authorization to bill for Account deposits and Entry
                                Fees is obtained by way of your electronic signature.  Once an electronic signature is submitted,
                                the electronic order constitutes an electronic letter of agency.  Global Fantasy Sports Golf’s™ reliance
                                upon your electronic signature was specifically sanctioned and written into law when the
                                Uniform Electronic Transactions Act and the Electronic Signatures in Global and National
                                Transactions Act were enacted in 1999 and 2000, respectively.  Both laws specifically
                                preempt all state laws that recognize only paper and handwritten signatures.
                            </li>
                            <li>Global Fantasy Sports Golf™ may offer free Contests from time-to-time which do
                                not require any Entry Fee to enter and which may, or may not, offer an associated
                                monetary Prize and/or Global Fantasy Sports Golf™ virtual tokens (&ldquo;Virtual Tokens&rdquo;) to the
                                applicable winner(s) (&ldquo;Free Contests&rdquo;).  You can enter as many Free Contests
                                as you want; provided, however, that some Free Contests may require Virtual
                                Tokens to enter and, therefore, your ability to enter certain Free Contests
                                may be limited by the number of Virtual Tokens in your Account.  <b>Please
                                    note that the Virtual Tokens have no monetary or other value outside
                                    of entry into certain Free Contests, and the Virtual Tokens cannot be
                                    exchanged for cash, products and/or merchandise.</b>
                            </li>
                        </ul>
                    </p>
                    <p><b>Contest Start Time/Contest Duration/Withdrawal/Points:</b></p>
                    <p><ul className="disc-list">
                        <li>Contests may vary in duration from start to finish and may last for two
                            (2) rounds in a Tournament (each, a &ldquo;Round&rdquo;), one (1) entire Tournament or
                            such other interval, as determined by Global Fantasy Sports Golf™ in its sole discretion (&ldquo;Contest Duration&rdquo;).
                        </li>
                        <li>Each Contest’s start time (&ldquo;Start Time&rdquo;) shall be set forth in the applicable Competition Room Rules, and shall generally correspond to when the first official tee time in the applicable Tournament for that Contest is scheduled to commence.</li>
                        <li>Contests are designed to close, and Teams are designed to lock, at the scheduled Start Time of the Contest.</li>
                        <li>You may leave a Contest at any point up until the scheduled Start Time.  If you leave a Contest prior to the scheduled Start Time, the Entry Fee, if any, will be credited back to your Account.</li>
                        <li>Contests that do not get filled with the required minimum number of Entrants prior to the scheduled Start Time will be cancelled and Global Fantasy Sports Golf™ will credit Entry Fees back to the Accounts of the Entrants that had already joined the Contests.  Contests that are filled with the required minimum number of Entrants prior to the scheduled Start Time will be conducted.</li>
                    </ul>
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">Contest Types</h3>
                    <h3 className="title">5-Ball Contest</h3>
                    <p>The following terms apply to &ldquo;5-Ball Contests&rdquo;:</p>
                    <p>
                        <ul className="disc-list">
                            <li>Each Entrant must select two (2) Rounds of the applicable Tournament for which the Golfers on her/his Team will earn Points (as defined below).  Golfers on an Entrant’s Team will not earn Points for Rounds of a Tournament that were not selected by that Entrant.</li>
                            <li>All Golfers in the applicable Tournament will be separated into tiers consisting of ten (10) Golfers, depending on their relative world rankings (Tier 1, Tier 2, Tier 3, etc.).</li>
                            <li>Depending on the number of Golfers required for each Team in the applicable 5-Ball Contest, each Entrant may only select one (1) Golfer from Tier 1, one (1) Golfer from Tier 2, one (1) Golfer from Tier 3, etc.; provided, however, that where an Entrant does not select a Golfer from a higher Tier, that Entrant may select one additional Golfer from a lower Tier.  For example, if an Entrant does not select a Golfer from Tier 1, that Entrant may select two (2) Golfers from Tier 2, or a second Golfer from any other lower Tier.</li>
                            <li>Multiple Entrants are permitted to select the same Golfer on their respective Teams.</li>
                            <li>Each Golfer on an Entrant’s Team will earn Points based on the &ldquo;Modified Stableford System&rdquo; which awards Points for each Golfer’s result on each hole in an active Round of a given Contest according to the following chart:</li>
                        </ul>
                    </p>
                    <p>
                        Double Eagle: 8 Points<br/>
                        Eagle: 5 Points<br/>
                        Birdie: 2 Points<br/>
                        Par: 0 Points<br/>
                        Bogey: -1 Points<br/>
                        Double Bogey or Worse: -3 Points<br/>
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">Pick-18 Contest</h3>
                    <p>The following terms apply to &ldquo;Pick-18 Contests&rdquo;:</p>
                    <p>
                        <ul className="disc-list">
                            <li>Each Entrant must select two (2) Rounds of the applicable Tournament for which the Golfers on her/his Team will earn Points (as defined below).  Golfers on an Entrant’s Team will not earn Points for Rounds of a Tournament that were not selected by that Entrant.</li>
                            <li>Depending on the number of Golfers required for each Team in the applicable Pick-18 Contest, each Entrant may select the requisite number from the entire list of Golfers competing in the applicable Tournament, without restriction by Tier or otherwise.</li>
                            <li>Multiple Entrants are permitted to select the same Golfer on their respective Teams.</li>
                            <li>Each Golfer on an Entrant’s Team will earn Points based on the predictions of the Entrant with respect to each such Golfer’s performance on each hole in an active Round of a given Contest.  For each hole in a given Contest, the Entrant must predict how each Golfer on her/his Team will perform in relation to par for that hole, either: &ldquo;Under,&rdquo; &ldquo;On&rdquo; or &ldquo;Over.&rdquo;  For each correct prediction, the Entrant will earn one (1) Point.  For each incorrect prediction, the Entrant will receive zero Points.</li>
                            <li>In order to assist the prediction process, Global Fantasy Sports Golf™ may make available certain statistics regarding each Golfer’s recorded performance in previous tournaments (including hole-by-hole statistics), the length of each hole in the applicable Tournament, each Golfer’s world ranking, and the par stroke for each hole in the applicable Tournament.</li>
                        </ul>
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">Hole-in-1</h3>
                    <p>The following terms apply to &ldquo;Hole-in-1 Contests&rdquo;:</p>
                    <p>
                        <ul className="disc-list">
                            <li>Each Entrant must select two (2) Rounds of the applicable Tournament for which the Golfers on her/his Team will earn Points (as defined below).  Golfers on an Entrant’s Team will not earn Points for Rounds of a Tournament that were not selected by that Entrant.</li>
                            <li>Depending on the number of Golfers required for each Team in the applicable Hole-in-1 Contest, each Entrant may select the requisite number from the entire list of Golfers competing in the applicable Tournament, without restriction by Tier or otherwise.</li>
                            <li>Multiple Entrants are permitted to select the same Golfer on their respective Teams.</li>
                            <li>Each Golfer in a Contest shall be assigned &ldquo;Handicap&rdquo; bonus strokes (&ldquo;Bonus Strokes&rdquo;) based on a proprietary method of calculation developed Global Fantasy Sports Golf™. The calculation is based on the world rank of each Golfer.  Each Golfer’s Handicap will be set forth in the applicable Competition Room Rules prior to the Contest’s Start Time.</li>
                            <li>Each Bonus Stroke will lower a Golfer’s score on a given hole by one (1) stroke.  Each Entrant must select, in advance, which holes in an active Round of a given Contest that it will apply its Bonus Strokes to. There is a limit of one (1) Bonus Stroke per hole.</li>
                            <li>Each Golfer on an Entrant’s Team will earn Points based on the Modified Stableford System which awards Points for each Golfer’s result on each hole (including where Bonus Strokes are applied) in an active Round of a given Contest according to the following chart:</li>
                        </ul>
                    </p>
                    <p>
                        Double Eagle: 8 Points<br/>
                        Eagle: 5 Points<br/>
                        Birdie: 2 Points<br/>
                        Par: 0 Points<br/>
                        Bogey: -1 Points<br/>
                        Double Bogey or Worse: -3 Points<br/>
                    </p>
                    <p><b>Tallying Points/Statistics:</b></p>
                    <p>Each Entrant will earn Contest points based on the performance,
                        during the active Rounds of the applicable Contest, of the Golfers that
                        were on his/her Team (&ldquo;Points&rdquo;).  Global Fantasy Sports Golf™ will be responsible for
                        tallying the Points for each Contest based on the reporting of Golfer
                        performance provided by the applicable Association during the applicable
                        Contest Duration.  Please be advised that during the Contests, Global Fantasy Sports Golf™
                        receives live scoring from its third-party statistics providers.  Live scoring
                        statistics that appear during a Contest are subject to change by the applicable
                        Association after the conclusion of the applicable Round and/or Tournament.
                        Please be advised that, even in the event of a change to the statistics
                        by the applicable Association after the conclusion of the applicable Round
                        and/or Tournament, the applicable Golfer’s Points on the Site will not be
                        updated and the Contest outcomes will not be revised.  Please note that where
                        a correction needs to be made after the conclusion of a given Round
                        and/or Tournament, as applicable, due to a problem with the statistics feed,
                        the Contest outcome (including payment of Prizes) may be reversed and
                        Contests resettled correctly. <b> ALL DETERMINATIONS WITH RESPECT
                            TO POINTS, PROJECTED POINTS, WINNERS AND PRIZES WILL BE MADE IN HOW FAR GOLF’S™
                            SOLE AND ABSOLUTE DISCRETION, AND WILL BE FINAL AND BINDING ON ALL PARTIES INVOLVED.</b>
                    </p>
                    <p><b>Single Winner Contests:</b></p>
                    <p>Where the Competition Room Rules indicate that the Contest
                        will have a single potential winner (subject to eligibility verification and/or
                        other potential disqualification, as set forth herein), the Entrant with the
                        most Points at the end of the Contest Duration will be deemed the potential
                        winner of that Contest.  The winner of the Contest will receive the Virtual
                        Tokens and/or cash prize specified in the applicable Competition Room Rules
                        (the &ldquo;Prize&rdquo;).  If two (2) or more Entrants finish in a tie with the top
                        Point total at the end of the Contest Duration, the Prize amount shall
                        be divided equally amongst all winners.
                    </p>
                    <p><b>Contests with Multiple Winners: </b></p>
                    <p>Where the Competition Room Rules indicate that the Contest will have
                        multiple potential winners, the designated number of Entrants with the most Points
                        at the end of the Contest Duration will be deemed the potential winners of that
                        Contest, and the Prize totals shall be assigned to each potential winner depending
                        on what place she/he finished.  In the event that two (2) or more Entrants finish
                        in a tie for any of the winning Point totals at the end of the Contest Duration,
                        the Prize amounts shall be divided equally amongst all potential winners with
                        the same Point total as if each were a potential winner of that tier’s Prize,
                        as well as the Prize(s) for the next tier or tiers.  For example, if 1st, 2nd
                        and 3rd place in a given Contest includes Prizes of $500, $300 and $100, respectively,
                        and two (2) Entrants tie for first place, then those two (2) Entrants shall receive
                        $400 each ($500 + $300 divided by 2), and the Entrant that finished in 3rd place shall
                        receive $100.
                    </p>
                    <p><b>Withdrawals/Taxes:</b></p>
                    <p>
                        <ul className="disc-list">
                            <li>Subject to the requirements set forth herein, the Prizes will be credited
                                to the winners’ Accounts.  Monetary Prize winnings may be withdrawn from your Account,
                                at your option, and will be paid via the same method that the Entrant used to deposit
                                money to her/his Account; provided, however, that certain bonus Prizes, registration
                                bonuses and other free money giveaways, as offered by Global Fantasy Sports Golf™ from time-to-time,
                                may be subject to certain withdrawal restrictions.
                            </li>
                            <li>All taxes associated with winning a Contest and/or receipt
                                of a Prize are the sole responsibility of the applicable winner/Prize recipient.
                                Global Fantasy Sports Golf™ may contact you to obtain your Social Security Number or other
                                personal information for tax purposes.  Without limiting the foregoing, any request
                                for withdrawal from your Account that brings your total earnings (withdrawals less deposits)
                                to an amount over Six Hundred Dollars ($600.00) for any calendar year will require that
                                you provide us with a valid street address and Social Security Number prior to processing.
                                This information will be used by us to file a form 1099-MISC at the end of the year.
                            </li>
                        </ul>
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title"></h3>
                </div>

                <div className="article__section">
                    <h3 className="title">Multiple Accounts</h3>
                    <p>Each Entrant is allowed one (1) Account.  Penalties for utilizing more than one (1)
                        Account without prior authorization from Global Fantasy Sports Golf™ will be assessed in
                        Global Fantasy Sports Golf’s™ sole discretion, and may include closure of all applicable
                        Accounts and forfeiture of any Prizes that we determine to have been
                        fraudulently obtained. </p>
                </div>

                <div className="article__section">
                    <h3 className="title">Suspended/Terminated Accounts</h3>
                    <p>There are a variety of behaviors that may be deemed by Global Fantasy Sports Golf™ to be detrimental
                        to Global Fantasy Sports Golf™ Offerings and other Entrants utilizing the Global Fantasy Sports Golf™ Offerings including,
                        without limitation, where Global Fantasy Sports Golf™ determines that you have abused, misused or
                        violated the terms of the Rules, the Global Fantasy Sports Golf™ Agreements or any aspect of the Contests.
                        Engaging in those behaviors may result in suspension and/or termination of some or all
                        functions associated with your Account, as well as forfeiture of any Prizes previously won,
                        as determined by Global Fantasy Sports Golf™ in its sole discretion.  Suspended players are expected
                        to respect the disciplinary actions imposed by Global Fantasy Sports Golf™.  </p>
                </div>

                <div className="article__section">
                    <h3 className="title">User Names</h3>
                    <p>Global Fantasy Sports Golf™ may require Users to change their user names in cases
                        where the subject user name is offensive or promotes a commercial venture.
                        Any determination that a user name must be changed shall be made in How
                        Far Golf’s™ sole discretion, and if requests are ignored, Global Fantasy Sports Golf™ may
                        unilaterally change a User’s user name.</p>
                </div>

                <div className="article__section">
                    <h3 className="title">Postponed Tournaments</h3>
                    <p>If a Tournament is canceled prior to the Tournament’s commencement,
                        the associated Contest will be terminated, and all Entrants
                        will be refunded their Entry Fees. </p>
                </div>

                <div className="article__section">
                    <h3 className="title">Suspended Rounds/Tournaments</h3>
                    <p>A Round will be considered suspended if it is unfinished,
                        due to weather, time considerations or other intervening factor,
                        but is scheduled to be completed in the future.  If a Round is suspended
                        for any reason, the Golfer statistics compiled for that Round will count toward
                        the applicable Contests, but only where the Round is officially completed prior
                        to 7:00pm Current Local Time (UTC), the following day. </p>
                </div>

                <div className="article__section">
                    <h3 className="title">Indemnification</h3>
                    <p>You agree to indemnify and hold Global Fantasy Sports Golf™, its parents and
                        subsidiaries, and each of their respective members, officers, directors,
                        shareholders, employees, agents, co-branders, content licensors and/or
                        other partners, harmless from and against any and all claims, expenses
                        (including reasonable attorneys’ fees), damages, suits, costs, demands and/or
                        judgments whatsoever, made by any third party related to or arising out of: (a)
                        your participation in a Contest in any way, whatsoever; (b) your breach of these
                        Contest Rules or any other Global Fantasy Sports Golf™ Agreement; (c) any dispute between you
                        and any other Entrants, Associations, Users and/or third parties; (d) any claim
                        that Global Fantasy Sports Golf™ owes any taxes in connection with your participation in a
                        Contest; and/or (e) your violation of any rights of another individual and/or
                        entity.  The provisions of this paragraph are for the benefit of Global Fantasy Sports Golf™,
                        its parents and subsidiaries, and each of their respective members, officers,
                        directors, shareholders, employees, agents, co-branders, content licensors
                        and/or other partners.  Each of these individuals and entities shall have the
                        right to assert and enforce these provisions directly against you on its own behalf.</p>
                </div>

                <div className="article__section">
                    <h3 className="title">Access to the Global Fantasy Sports Golf™ Offerings</h3>
                    <p>While we try to ensure that the Global Fantasy Sports Golf™ Offerings are
                        functioning smoothly at all times, like any online service, we may periodically
                        experience periods of outage or slow performance.  These can sometimes result in,
                        among other things, an inability to access the Global Fantasy Sports Golf™ Offerings, problems
                        editing Teams and/or problems entering new Contests.  If you are unable to access the
                        Global Fantasy Sports Golf™ Offerings, please report the problem by emailing us at: &nbsp;<a href="mailto:support@globalfantasysports.com"
                                                                                                                     target="_blank">support@globalfantasysports.com</a>.
                        If there are sustained periods where Users are unable to access the Global Fantasy Sports Golf™ Offerings,
                        or are otherwise prevented from editing Teams, we may provide instructions on how to cancel
                        any affected Contest entries.</p>
                </div>

                <div className="article__section">
                    <h3 className="title">Contest Cancellation</h3>
                    <p>Global Fantasy Sports Golf™ reserves that right to cancel any Contests in our sole discretion,
                        for any reason and without restriction.  These reasons may include, without limitation:
                        (a) where Global Fantasy Sports Golf™ believes that, due to problems with the Global Fantasy Sports Golf™ Offerings
                        or due to events affecting the actual Tournaments, there would be a negative impact on the
                        integrity of the applicable Contest(s); and/or (b) where Global Fantasy Sports Golf™ determines, in its
                        sole and absolute discretion, that such cancellation is necessary or advisable due to any
                        change in or to any law, regulation or legal interpretation thereof affecting the Contests in any way.</p>
                </div>

                <div className="article__section">
                    <h3 className="title">Modification of the Contest Rules</h3>
                    <p>Global Fantasy Sports Golf™ may modify these Contest Rules, in its sole discretion,
                        by posting amended Contest Rules on the Site, or by sending an e-mail to the
                        e-mail addresses associated with registered Users.  Such notice shall be effective
                        immediately after it is posted or e-mailed, as applicable; provided, however, that no
                        such modification or termination shall affect Prizes that are pending, or that have already
                        been awarded, as of the date of the modification or termination notice, as applicable.
                        If any such modification is unacceptable to you, your only recourse is to cease
                        participating as a Contest Entrant.</p>
                </div>

                <div className="article__actions">
                    <BtnLink to="__back" mod={cls}>
                        Back
                    </BtnLink>
                </div>
            </article>
        </section>`
