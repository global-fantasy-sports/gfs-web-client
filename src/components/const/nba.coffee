Immutable = require('immutable')


exports.POSITION_NAMES = {
    'PG': 'Point Guard'
    'SG': 'Shooting Guard'
    'PF': 'Power Forward'
    'SF': 'Small Forward'
    'C': 'Center'
    'F': 'Forward'
    'G': 'Guard'
    'UTIL': 'UTIL'
}

exports.POSITIONS = ['PG', 'SG', 'PF', 'SF', 'C', 'G', 'F', 'UTIL']

exports.STATS_FILTER_POSITIONS = ['PG', 'SG', 'PF', 'SF', 'C']

exports.POSITION_VALUES = {
    'PG': Immutable.Set.of('PG')
    'SG': Immutable.Set.of('SG')
    'PF': Immutable.Set.of('PF')
    'SF': Immutable.Set.of('SF')
    'C': Immutable.Set.of('C')
    'F': Immutable.Set.of('PF', 'SF')
    'G': Immutable.Set.of('PG', 'SG')
    'UTIL': Immutable.Set.of('C', 'PG', 'SG', 'PF', 'SF')
}


exports.ROLE_STATS = {
    PG: ['points', 'assists', 'rebounds', 'blocks', 'steals']
    SG: ['points', 'assists', 'rebounds', 'blocks', 'steals']
    PF: ['points', 'assists', 'rebounds', 'blocks', 'steals']
    SF: ['points', 'assists', 'rebounds', 'blocks', 'steals']
    C: ['points', 'assists', 'rebounds', 'blocks', 'steals']
}


exports.STAT_NAMES = {
    games: 'Games'
    points: 'Points'
    assists: 'Assists'
    rebounds: 'Rebounds'
    blocks: 'Blocks'
    steals: 'Steals'
}


exports.STAT_NAME_ABBRS = {
    games: 'Games'
    points: 'Points'
    assists: 'Assists'
    rebounds: 'Rebounds'
    blocks: 'Blocks'
    steals: 'Steals'
}

exports.INJURY = {
    'DOU': 'Doubtful'
    'OUT': 'Out of game'
    'PRO': 'Probable'
    'QST': 'Questionable'
    'UNK': 'Unknown'
}

exports.ROOM_TYPES = [
    'Top 15%'
    'Quadruple'
    'Head 2 head'
    'Double Up'
    'Top 20%'
    '50/50'
]

