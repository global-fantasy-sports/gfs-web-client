date = require('lib/date')

if SPORT == 'nfl'
    constants = require('./nfl')
else if SPORT == 'nba'
    constants = require('./nba')
else if SPORT == 'golf'
    constants = require('./golf')
else
    constants = {}

module.exports = Object.assign({}, constants, {
    RoomsComparators:
        game: (room) ->
            -((room.pot * 1000) +
                    (room.gamesCount * 10))

        entries: (room) ->
            n = -(room.gamesCount / room.maxEntries)
            dt = date.now() / room.startDate
            return n + dt

        entry: (room) ->
            -room.entryFee

        prizes: (room) ->
            -room.pot

        starts: (room) ->
            percent = -(room.gamesCount / room.maxEntries)

            +room.startDate * 100 + percent
#            percent = Math.round((room.gamesCount / room.maxEntries) * 100, 0)
#
#            (+room.startDate * 1000000).toPrecision() + (10000 - 100 * percent) + (10000000 - room.pot)

    COMPETITION_ROOM_TEXTS:
        gameTypes:
            salarycap: 'The game is a Salary-Cap game format. Players are given a limited budget from which to draft their perfect line-up. The winner is the player who drafts the best collectively scoring team and has the  most fantasy points. All athletes score points according to the fantasy points scoring system.'
            'handicap-the-pros': 'HANDICAP-THE-PROS: All golfers are ranked and given a handicap. You on which holes to apply the extra strokes. Turn a birdie into an eagle and supercharge the score of your golfer!'
            'hole-by-hole': 'HOLE-BY-HOLE: Predict the outcome of each hole for your golfers. Will it be birdie? A par? A double-bogey?'
            'five-ball': '5 BALL: Build a team of 5 golfers and win by scoring the most points collectively. Points system in use: Modified Stableford Scoring'
        featured:
            button: 'Exciting and popular competition rooms'
        beginner:
            button: 'Only for players with less than 50 played games'
        guaranteed:
            button: 'No matter how many sign-up, this competition will run!'
        'Head 2 head':
                button: 'There can be only one!'
                description: -> 'This is a Head-2-Head competition between 2 players. You square off with another fan to find out who\'s the best man!'
                distribution: 'Winner takes all - The loser takes nothing!'

        'Double Up':
            button: 'Double your entry'
            description: (players, winners) -> "In this competition with #{players} players there are #{winners} winners. A friendly competition for people who like to win, Win, WIN!"
            distribution: 'Winners double their money. There are only two types of players: Winners and Losers - and nothing inbetween. Loser walk away with nothing, but if you\'re a winner; you (almost) double your money.'

        'Triple Up':
            button: 'Triple your entry'
            description: (players, winners) -> "In this competition with #{players} players there are #{winners} winners. A friendly competition for people who like to win, Win, WIN! ",
            distribution: 'Winners triple their money. There are only two types of players: winners and loosers. Losers don\'t get anything, and if you\'re a winner you 3x your money (or as close as can be).',

        'Free roll':
            button: 'Take it for a spin free of charge and emerge a winner'
            description: (players, winners) -> "In this competition with #{players} players there are #{winners} winners. Although you enter by playing tokens you're competing for money! If you win you can take your winnings and play in competition rooms where more money is at stake!"
            distribution: 'Winners convert their free tokens into real money that can be only be spent on entring into further competition rooms. In this competition room there are only two types of players: winners, loosers and nothing inbetween. Losers don\'t get anything it\'s sad we know it!'

        'Top 20%':
            button: 'The top 20% win'
            description: (players, winners) -> "In this competition with #{players} players there are #{winners} winners. In this competition it is all about THE MONEY! Lots of players and big prizes!"
            distribution: 'There are only two types of players: winners, loosers and nothing inbetween. Losers don\'t get anything, and winners split the spoils equally.'

        '50/50':
            button: '50% win and share the spoils equally'
            description: (players, winners) -> "In this competition with #{players} players there are #{winners} winners. A friendly competition for people who like to win, Win, WIN! "
            distribution: 'There are only two types of players: winners, loosers and nothing inbetween. Losers don\'t get anything, and winners partake in the spoils equally.'

        'Quadruple':
            button: '4x your money back!'
            description: (players, winners) -> "In this competition with #{players} players there are #{winners} winners. In this competition it is all about THE MONEY! Lots of players and big prizes!"
            distribution: 'Winners quadruple their money. There are only two types of players: winners and loosers. Losers don\'t get anything, and if you\'re a winner you 4x your money (or as close as can be).'

        'Quintuple':
            button: '5x your money back!'
            description: (players, winners) -> "In this competition with #{players} players there are #{winners} winners. In this competition it is all about THE MONEY! Lots of players and big prizes!"
            distribution: 'Winners quintuple their money. There are only two types of players: winners and losers. Losers don\'t get anything, and if you\'re a winner you 5x your money (or as close as can be).'

        'Top 15%':
            button: 'The top 15% win'
            description: (players, winners) -> "In this competition with #{players} players there are #{winners} winners. In this competition it is all about THE MONEY! Lots of players and big prizes!"
            distribution: 'The 15% best win money as per detailed payout structure.'

})
