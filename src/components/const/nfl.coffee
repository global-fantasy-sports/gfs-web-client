Immutable = require('immutable')


exports.POSITION_NAMES = {
    'QB': 'Quarterback'
    'RB': 'Running Back'
    'WR': 'Wide Receiver'
    'TE': 'Tight End'
    'FLEX': 'RB/WR/TE'
    'DST': 'Defense/Special Teams'
}

exports.POSITIONS = ['QB', 'RB', 'WR', 'TE', 'FLEX', 'DST']

exports.STATS_FILTER_POSITIONS = ['QB', 'RB', 'WR', 'TE']

exports.POSITION_VALUES = {
    'QB': Immutable.Set.of('QB')
    'RB': Immutable.Set.of('RB')
    'WR': Immutable.Set.of('WR')
    'TE': Immutable.Set.of('TE')
    'FLEX': Immutable.Set.of('RB', 'WR', 'TE')
    'DST': Immutable.Set.of('DST')
}


exports.ROLE_STATS = {
    'QB': ['rushing:yds', 'rushing:td', 'passing:yds', 'passing:td', 'passing:cmp_pct']
    'RB': ['rushing:yds', 'rushing:td', 'receiving:yds', 'receiving:td', 'receiving:rec']
    'WR': ['receiving:yds', 'receiving:td', 'receiving:rec', 'fumbles:lost']
    'TE': ['receiving:yds', 'receiving:td', 'receiving:rec', 'fumbles:lost']
    'DST': ['defense:sack', 'defense:tackle', 'defense:int', 'defense:opp_rec', 'team:points']
}


exports.STAT_NAMES = {
    'games': 'Number of games played'
    'points': 'Fantasy points'

    'defense:int': 'Interceptions'
    'defense:opp_rec': 'Fumbles recovered'
    'defense:sack': 'Sacks'
    'defense:tackle': 'Tackles'
    'fumbles:lost': 'Fumbles lost'
    'passing:cmp_pct': 'Completion %%'
    'passing:td': 'Passing touchdowns per game'
    'passing:yds': 'Passing yards per game'
    'receiving:rec': 'Receptions per game'
    'receiving:td': 'Receiving touchdowns per game'
    'receiving:yds': 'Receiving yards per game'
    'rushing:td': 'Rushing touchdowns'
    'rushing:yds': 'Rushing yards per game'
    'team:points': 'Average points gained by another team'
}


exports.STAT_NAME_ABBRS = {
    'games': 'Games'
    'points': 'Points'

    'defense:int': 'Int.'
    'defense:opp_rec': 'Fum. recovered'
    'defense:sack': 'Sacks'
    'defense:tackle': 'Tackles'
    'fumbles:lost': 'Lost'
    'passing:cmp_pct': 'Cmp %%'
    'passing:td': 'Pass. TD'
    'passing:yds': 'Pass. YDS'
    'receiving:rec': 'Reception YDS'
    'receiving:td': 'Receiving TD'
    'receiving:yds': 'Receiving YDS'
    'rushing:td': 'Rush. TD'
    'rushing:yds': 'Rush. YDS'
    'team:points': 'Avg. opp points'
}

exports.INJURY = {
    'DOU': 'Doubtful'
    'OUT': 'Out of game'
    'PRO': 'Probable'
    'QST': 'Questionable'
    'UNK': 'Unknown'
}

exports.ROOM_TYPES = [
    'Top 15%'
    'Quadruple'
    'Head 2 head'
    'Double Up'
    'Top 20%'
    '50/50'
]


exports.CHART_LABELS = {
    radial:
        QB: 'Successful passes'
        WR: 'Successful receptions'
        TE: 'Successful receptions'
        RB: 'First downs'
    maxCompare:
        QB: 'Passing yards per game'
        WR: 'Receive yards per game'
        TE: 'Receive yards per game'
        RB: 'Rushing yards per game'
    yardsCompare:
        QB: ['Passing yards per game', 'Rushing yards per game']
        WR: ['Receiving yards per game', 'Rushing yards per game']
        TE: ['Receiving yards per game', 'Rushing yards per game']
        RB: ['Receiving yards per game', 'Rushing yards per game']
    yardsCompareMaxLabel:
        QB: ['Pass yds', 'Rush yds']
        WR: ['Rec yds', 'Rush yds']
        TE: ['Rec yds', 'Rush yds']
        RB: ['Rec yds', 'Rush yds']
}


exports.CHART_MAX_CONSTS = {
    maxCompare:
        QB: 342.31
        WR: 122.75
        TE: 84.57
        RB: 84.57
    yardsCompare:
        QB: 342.31
        WR: 122.75
        TE: 84.57
        RB: 131.06

}

