


exports.STAT_NAMES = {
    'events_played': 'Events',
    'cuts_made': 'Cuts made'
    'drive_avg': 'Average Drive Distance'
    'drive_acc': 'Drive Accuracy Percentage'
    'gir_pct': 'Greens in Regulation'
    'hole_proximity_avg': 'Proximity to Hole Average'
    'scrambling_pct': 'Scrambling Percentage'
    'strokes_gained': 'Strokes Gained Putting'
    'dateRange': 'Dates Range'
    'tourName': 'Tournament name'
    'position': 'Player\'s place'
    'score': 'Total fantasy points'
    'avgFPPR': 'FPPR'
    'birdies': 'Birdies'
    'pars': 'Pars'
    'bogeys': 'Bogeys'
}

exports.STAT_NAME_ABBRS = {
    'events_played': 'Events',
    'cuts_made': 'Cuts'
    'drive_avg': 'Avr. Drive'
    'drive_acc': 'Drive Acc.'
    'gir_pct': 'Greens'
    'hole_proximity_avg': 'Prox. to Hole'
    'scrambling_pct': 'Scrambl. Per.'
    'strokes_gained': 'Strokes Gained'
    'dateRange': 'Dates'
    'tourName': 'Tournament'
    'position': 'Place'
    'score': 'Total points'
    'avgFPPR': 'FPPR'
    'birdies': 'Birdies'
    'pars': 'Pars'
    'bogeys': 'Bogeys'
}

exports.ROLE_STATS = {
    0: ['tourName', 'dateRange', 'position', 'score', 'avgFPPR', 'birdies', 'pars', 'bogeys']
    1: ['drive_avg', 'drive_acc', 'gir_pct', 'hole_proximity_avg', 'scrambling_pct', 'strokes_gained']

}

exports.SUMMARY_STATS = ['events_played', 'cuts_made', 'drive_avg', 'drive_acc', 'gir_pct', 'hole_proximity_avg', 'scrambling_pct', 'strokes_gained']

exports.GAME_TYPES = {
    'five-ball': '5-BALL'
    'hole-by-hole': 'HOLE-BY-HOLE'
    'handicap-the-pros': 'HANDICAP-THE-PROS'
}
