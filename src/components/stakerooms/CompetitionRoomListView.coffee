_ = require('underscore')
{Titlebar} = require('../../lib/ui/Titlebar')
StickyBlock = require('../../lib/ui/StickyBlock')
GuideHint = require('../guide/GuideHint')
CompetitionsFilterBase = require('./CompetitionsFilterBase')
CompetitionsFilterExtend = require('./CompetitionsFilterExtend')
CompetitionsSortingHeader = require('./CompetitionsSortingHeader')
{Btn} = require('../../lib/ui')
{scrollToTop} = require('../../lib/scroll')


if SPORT == 'golf'
    CompetitionRoomItem = require('./GolfCompetitionRoomItem')
if SPORT in ['nfl', 'nba']
    CompetitionRoomItem = require('./CompetitionRoomItem')


CompetitionRoomListView = React.create(
    displayName: 'CompetitionRoomListView'

    propTypes:
        rooms: React.PropTypes.object.isRequired
        types: React.PropTypes.object.isRequired
        actionRenderer: React.PropTypes.func
        selectedRoomIds: React.PropTypes.object
        isInitialRoom: React.PropTypes.bool

    mixins: [flux.Connect('search', (store) ->
        sortingField: store.getSortingField()
        sortingDirection: store.getSortingDirection().toLowerCase()
        numberToShow: store.getNumberToShow()
    )]

    getInitialState: ->
        expanded: false

    componentDidMount: ->
        if @props.isInitialRoom
            _.defer(=>
                el = document.getElementById('competitionroomitem_' + @props.selectedRoomIds.first())
                scrollToTop(el, {offset: -200})
            )

    onExpandFilter: ->
        @setState(expanded: not @state.expanded)

    orderBy: (order) ->
        ->
            flux.actions.search.setSortingField(order)

    onShowMore: ->
        flux.actions.search.extendNumberToShow()

    renderRooms: (rooms) ->
        self = this
        rooms.map((room, i) ->
            `<CompetitionRoomItem
                key={room.id}
                room={room}
                actionComponent={self.props.actionRenderer(room.id)}
                challengeRequestRoomId={self.props.challengeRequestRoomId}
                selectedRoomIds={self.props.selectedRoomIds} />`
        ).toArray()

    render: ({rooms, types, actionRenderer}, {sortingField, sortingDirection, expanded, numberToShow}) ->
        roomList = @renderRooms(rooms.take(numberToShow))
        orderBy = @orderBy

        `<section className="competition-room-list-view">
            <div className="app__row app__row--competition">

                <CompetitionsFilterBase onExpand={this.onExpandFilter} expanded={expanded}/>
                {expanded && <CompetitionsFilterExtend/>}

                {!rooms.count() &&
                    <div className="competitions">
                        <div className="competitions__title">
                            <Titlebar title="Competitions" className="app__title">
                                <div className="competitions__no-rooms">
                                    There are no rooms
                                </div>
                            </Titlebar>
                        </div>
                    </div>
                }

                {!!rooms.count() &&
                    <div className="competitions">
                        <div className="competitions">
                            <StickyBlock className="competitions__head" ref="header">
                                <CompetitionsSortingHeader orderBy={orderBy}
                                                           sortingDirection={sortingDirection}
                                                           sortingField={sortingField}/>
                            </StickyBlock>
                            <div className="competitions__list">
                                {roomList}
                            </div>
                        </div>
                    </div>
                }
                {numberToShow < rooms.count() &&
                    <div className="competition-room-list-view__show-more">
                        <Btn mod="blue-base-active" onTap={this.onShowMore}>Show 50 more rooms</Btn>
                    </div>
                }
            </div>
        </section>`
)

module.exports = CompetitionRoomListView
