_ = require('underscore')
{cx} = require('../../lib/utils')
{gameNameByType} = require('../../lib/const')
{BtnLink} = require('../../lib/ui')
{Titlebar} = require('../../lib/ui/Titlebar')
CompetitionRoomListView = require('./CompetitionRoomListView')


LandingCompetitionRoomList = React.create(
    displayName: 'LandingCompetitionRoomList'

    mixins: [
        flux.Connect('rooms', (store) ->
            totalRoomCount: store.getTotalAvailableRooms()
            rooms: store.getFilteredRooms()
            types: store.getRoomTypes()
            allRoomsStarted: store.isAllStarted()
        )
    ]

    actionRenderer: (id) ->
        link = "/#{SPORT}/competition/#{id}/"

        `<BtnLink to={link} mod="red middle">
            ENTER
        </BtnLink>`


    render: (P, {allRoomsStarted, types, rooms, totalRoomCount}) ->
        return null if allRoomsStarted

        `<section className="app__row app__row--competition">
            <Titlebar>Competitions</Titlebar>
            {totalRoomCount > 0
                ? <CompetitionRoomListView
                      rooms={rooms}
                      types={types}
                      challengeRequestRoomId={null}
                      actionRenderer={this.actionRenderer}
                  />
                : <h1 className="title white size-30 text-centered" style={{marginBottom: 0}}>
                      There are no competition rooms available at the moment
                  </h1>
            }
        </section>`
)


module.exports = LandingCompetitionRoomList
