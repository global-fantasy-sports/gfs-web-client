moment = require('moment')

Connect = require('../../flux/connectMixin')
{pad} = require('../../lib/utils')


module.exports = React.create(
    displayName: 'RoomStartCounter'

    mixins: [
        Connect('time', (store) -> {now: store.getNow()})
    ]

    render: ({room, text, mod, subtext}, {now}) ->
        date = moment(room.startDate)

        if date.isValid() and date.isAfter(now)
            diff = moment.duration(date.diff(now))
            if diff.days() > 0
                return `<div className="countdown big">{date.format('MMMM Do')}</div>`

        diff or= moment.duration(0)

        if subtext
            `<div className="countdown">
                <div className="countdown__content">
                    <div className="countdown__item">
                        <p className="countdown__num">{pad(diff.get('hours'))}</p>
                        <p className="countdown__subtext">HOURS</p>
                    </div>
                    <div className="countdown__item">
                        <p className="countdown__num">{pad(diff.get('minutes'))}</p>
                        <p className="countdown__subtext">MIN</p>
                    </div>
                    <div className="countdown__item">
                        <p className="countdown__num">{pad(diff.get('seconds'))}</p>
                        <p className="countdown__subtext">SEC</p>
                    </div>
                </div>
            </div>`
        else
            `<div className="countdown">
                <div className="countdown__counter">
                    {text ?
                    <span className="countdown__text">starts in: </span>
                    : null}
                    <span className="countdown__els">
                        {diff.get('days') ?
                        <span className="countdown__el">
                            {diff.get('days')}
                            <span>d</span>
                        </span>
                        : null}
                        <span className="countdown__el">
                            {pad(diff.get('hours'))}
                        </span>
                        <span> : </span>
                        <span className="countdown__el">
                            {pad(diff.get('minutes'))}
                        </span>
                        <span> : </span>
                        <span className="countdown__el">
                            {pad(diff.get('seconds'))}
                        </span>
                    </span>
                </div>
            </div>`
)
