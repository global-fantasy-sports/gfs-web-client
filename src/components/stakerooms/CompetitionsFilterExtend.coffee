_ = require('underscore')
{Btn, Icon} = require('lib/ui')
{cx} = require('lib/utils')
bem = require('bem-classname')
Tooltip = require('../tooltip/Tooltip')
{COMPETITION_ROOM_TEXTS, GAME_TYPES} = require('../const/index')


module.exports = React.create(
    displayName: "CompetitionsFilterExtend"

    mixins: [flux.Connect('search', (store) ->
        paymentType: store.getPaymentType()
        roomTypeFilters: store.getRoomTypeFilters()
        gameTypeFilters: store.getGameTypeFilters()
    )]

    onGameTypeFilter: (gameType) ->
        ->
            flux.actions.search.toggleGameTypeFilter(gameType)

    onRoomTypeFilter: (roomType) ->
        ->
            flux.actions.search.toggleRoomTypeFilter(roomType)

    onPaymentTypeFilter: (payment) ->
        (e) ->
            e.preventDefault()
            flux.actions.search.setPaymentTypeFilter(payment)

    renderGameTypes: ->
        return null unless GAME_TYPES
        onGameTypeFilter = @onGameTypeFilter
        gameTypeFilters = @state.gameTypeFilters

        gameTypeButtons = _(GAME_TYPES).map((text, gameType) ->
            `<Btn mod={cx('blue-base', {'blue-base-active': gameTypeFilters.has(gameType)})}
                  onTap={onGameTypeFilter(gameType)}>
                {text}
            </Btn>`
        )

        `<div className="filter-extend__type">
            <div className="filter-extend__title">
                Game type:
            </div>
            <nav className="filter-extend__buttons">
                <Btn mod="blue-base-active" onTap={onGameTypeFilter()}>
                    All
                </Btn>

                {gameTypeButtons}
            </nav>
        </div>`

    render: ({}, {paymentType, roomTypeFilters}) ->
        linkCx = bem.bind(null, 'filter-extend', 'link')
        onRoomTypeFilter = @onRoomTypeFilter
        onPaymentTypeFilter = @onPaymentTypeFilter

        `<div className="filter-extend">
            <div className="filter-extend__row">
                <div className="filter-extend__type">
                    <div className="filter-extend__title">
                        Pro rooms:
                    </div>
                    <nav className="filter-extend__buttons">
                        <Btn mod={cx('blue-base', {'blue-base-active': roomTypeFilters.count() == 0})}
                             onTap={onRoomTypeFilter()}>
                            All
                        </Btn>
                        <Tooltip text={COMPETITION_ROOM_TEXTS['Top 15%'].button} viewMod="texted">
                            <Btn mod={cx('blue-base', {'blue-base-active': roomTypeFilters.has('Top 15%')})}
                                 onTap={onRoomTypeFilter('Top 15%')}>
                                Top 15%
                            </Btn>
                        </Tooltip>
                        <Tooltip text={COMPETITION_ROOM_TEXTS['Quadruple'].button} viewMod="texted">
                            <Btn mod={cx('blue-base', {'blue-base-active': roomTypeFilters.has('Quadruple')})}
                                 onTap={onRoomTypeFilter('Quadruple')}>
                                Quadruple
                            </Btn>
                        </Tooltip>
                    </nav>
                </div>
                <div className="filter-extend__type">
                    <div className="filter-extend__title">
                        Payment:
                    </div>
                    <nav className="filter-extend__buttons">
                        <Btn mod={cx('blue-base', {'blue-base-active': paymentType == null})}
                             onTap={onPaymentTypeFilter()}>
                            All
                        </Btn>
                        <Btn mod={cx('blue-base', {'blue-base-active': paymentType == 'token'})}
                             onTap={onPaymentTypeFilter('token')}>
                            Tokens
                        </Btn>
                        <Btn mod={cx('blue-base', {'blue-base-active': paymentType == 'usd'})}
                             onTap={onPaymentTypeFilter('usd')}>
                            Dollars
                        </Btn>
                    </nav>
                </div>
            </div>
            <div className="filter-extend__row">
                <div className="filter-extend__type">
                    <div className="filter-extend__title">
                        Casual rooms:
                    </div>
                    <nav className="filter-extend__buttons">
                        <Tooltip text={COMPETITION_ROOM_TEXTS['Head 2 head'].button} viewMod="texted">
                            <Btn mod={cx('blue-base', {'blue-base-active': roomTypeFilters.has('Head 2 head')})}
                                 onTap={onRoomTypeFilter('Head 2 head')}>
                                Head 2 Head
                            </Btn>
                        </Tooltip>
                        <Tooltip text={COMPETITION_ROOM_TEXTS['Double Up'].button} viewMod="texted">
                            <Btn mod={cx('blue-base', {'blue-base-active': roomTypeFilters.has('Double Up')})}
                                 onTap={onRoomTypeFilter('Double Up')}>
                                Double-up
                            </Btn>
                        </Tooltip>
                        <Tooltip text={COMPETITION_ROOM_TEXTS['Top 20%'].button} viewMod="texted">
                            <Btn mod={cx('blue-base', {'blue-base-active': roomTypeFilters.has('Top 20%')})}
                                 onTap={onRoomTypeFilter('Top 20%')}>
                                Top 20%
                            </Btn>
                        </Tooltip>
                        <Tooltip text={COMPETITION_ROOM_TEXTS['50/50'].button} viewMod="texted">
                            <Btn mod={cx('blue-base', {'blue-base-active': roomTypeFilters.has('50/50')})}
                                 onTap={onRoomTypeFilter('50/50')}>
                                50/50
                            </Btn>
                        </Tooltip>
                    </nav>
                </div>

                {this.renderGameTypes()}
            </div>
        </div>`
)
