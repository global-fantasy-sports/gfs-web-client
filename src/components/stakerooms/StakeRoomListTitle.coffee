{Titlebar} = require('lib/ui/Titlebar')
{prevent} = require('../../lib/utils')

if SPORT == 'golf'
    StakeRoomListTitle = React.create
        displayName: "StakeRoomListTitle"

        mixins: [
            flux.Connect('search', (store) => {roomType: store.getRoomType()})
        ]

        onTap: ->
            flux.actions.search.toggleRoomType()

        render: ->
            message =
                if @state.roomType is 'usd'
                    'Click here to see free competitions for tokens'
                else
                    'Click here to hide free competitions for tokens'

            `<div className="competitions__title">
                <Titlebar title="Competitions" className="app__title" mod="h2">
                    <a href="#" onClick={prevent} onTap={this.onTap} className="stroke blue">
                        {message}
                    </a>
                </Titlebar>
            </div>`

if SPORT in ['nfl', 'nba']
    StakeRoomListTitle = React.create
        displayName: "StakeRoomListTitle"

        render: ->
            `<div className="competitions__title">
                <Titlebar title="Competitions" className="app__title"/>
            </div>`

module.exports = StakeRoomListTitle
