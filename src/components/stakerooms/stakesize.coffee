{Btn, Dollars, Tokens, Icon} = require('lib/ui')
{cx} = require('lib/utils')
RoomStartCounter = require('./RoomStartCounter')
GuideHint = require('components/guide/GuideHint')
HintTrigger = require('components/guide/HintTrigger')

WizardActionBar = require('lib/ui/WizardActionBar')


exports.StakeSelect = React.create
    displayName: 'StakeSelect'

    mixins: [
        flux.Connect('rooms', (store) -> rooms: store.getRooms().toList())
        flux.Connect('wizard', (store) ->
            gameState: store.getGameState()
            selectedRoomId: store.getCompetitionRoomId()
            roundNumber: store.getRoundNumber()
            tournamentId: store.getTournamentId()
            gameType: store.getWizardGameType()
        )
    ]

    getInitialState: ->
        roomId = flux.stores.wizard.getCompetitionRoomId()

        if roomId is null
            preselected = false
            deactivateRooms = false
        else
            preselected = true
            deactivateRooms = true

        {
            deactivateRooms
            preselected
            entryFeeType: 'all'
        }

    componentWillUpdate: (nextProps, nextState) ->
        if nextState.gameState == 'saved'
            @props.onNext()

    onSelect: (roomId) ->
        flux.actions.wizard.setCompetitionRoomId(roomId)

    next: ->
        flux.actions.wizard.saveGame()

    toggleRooms: (t) ->
        (e) =>
            e.preventDefault()
            @setState(entryFeeType: t)

    render: (props,
             {selectedRoomId, roundNumber, mode, rooms, deactivateRooms, preselected,
             entryFeeType, tournamentId, gameType}) ->
        filter = (room) -> room.roundNumber == roundNumber and room.tournamentId == tournamentId and room.gameType == gameType
        onSelect = @onSelect
        publicRooms = getStakeRooms(rooms, [filter])
        tokenRooms = rooms.filter (room) -> room.mode == 'token'
        usdRooms = rooms.filter (room) -> room.mode == 'usd'

        cls = cx("stroke", "white x-medium")

        `<section className="stake-select">

            <section className="choose-stake">
                <section className="competitions stakesizes">
                    <section className="game-table game-table--header game-table--entryfee" ref="header">
                        <section className="f-30">GAME</section>
                        <section className="f-10">ENTRIES</section>
                        <section className="f-14">ENTRY FEE</section>
                        <section className="f-17">PRIZE POLL</section>
                        <section className="f-29">STARTS(ET)</section>
                    </section>
                    <GuideHint children="Choose the competition room with the stake
                                         size and entries that you want."
                               title="Select your entry fee"
                               pointer="bottom"
                               group="wizard"
                               id="entryfee"
                               top="-97px"
                               right="-97px"/>

                    <StakesList
                        rooms={publicRooms}
                        selectedRoomId={selectedRoomId}
                        onSelect={onSelect}
                        deactivateRooms={deactivateRooms}
                        entryFeeType={entryFeeType}/>

                    {tokenRooms.size && usdRooms.size ?
                    <div className="choose-stake__rooms-switcher">
                        {entryFeeType == 'usd' ?
                        <span onTap={this.toggleRooms('all')} className={cls}>
                            Click here to see free competitions for tokens
                        </span>
                        :
                        <span onTap={this.toggleRooms('usd')} className={cls}>
                            Click here to hide free competitions for tokens
                        </span>
                        }
                    </div>
                    : null}
                </section>
            </section>

            <WizardActionBar
                nextLabel="Pay"
                isDone={Boolean(selectedRoomId)}
                onNext={this.next} />
        </section>`


exports.StakesList = StakesList = React.create
    displayName: 'StakesList'

    render: ({rooms, deactivateRooms, selectedRoomId, onSelect, entryFeeType}, S) ->
        tokenRooms = rooms.filter (room) -> room.mode == 'token'
        usdRooms = rooms.filter (room) -> room.mode == 'usd'

        usd = usdRooms.map((room, i) ->
            `<StakeSize
                key={i} room={room}
                onSelect={onSelect}
                isSelected={room.id == selectedRoomId}
                deactivateRooms={deactivateRooms}/>`
        )
        tokens = tokenRooms.map((room, i) ->
            `<StakeSize
                key={i} room={room}
                onSelect={onSelect}
                isSelected={room.id == selectedRoomId}
                deactivateRooms={deactivateRooms}/>`
        )

        if entryFeeType == 'usd'
            els = `<div className="competitions__list-in">
                {usd}
            </div>`
        # FIXME: case under doesn't work
        else if entryFeeType == 'usd' && !usdRooms.size
            els = `<div className="competitions__list-in">
                {tokens}
            </div>`
        else if entryFeeType == 'all'
            els = `<div className="competitions__list-in">
                {usd}
                {tokens}
            </div>`
        else
            els = `<div className="competitions__list-sorry">
                Sorry, there are no competition rooms to choose from
            </div>`

        `<section className="competitions__list">
            {els}
        </section>`

exports.StakeSize = StakeSize = React.create
    displayName: 'StakeSize'

    onSelect: ->
        isBeginner = flux.stores.users.getCurrentUser().get('isBeginner', true)
        if !isBeginner and @props.room.isForBeginners
            flux.actions.state.openInfoMessage("You can't join this competition since your experience level is too high")
        else
            @props.onSelect(@props.room.id)

    render: ({isSelected, room, deactivateRooms}, S) ->

        roomSize = `<div>
            <span className="stakeroom-item__games-count">{room.gamesCount}</span>
            <span className="stakeroom-item__max-entries"> / {room.maxEntries}</span>
        </div>`
        entryFee = room.entryFee
        onSelect = @onSelect

        cls = 'stakeroom-item stakeroom-item--stake'

        if deactivateRooms and not isSelected
            cls = cls + ' disabled '

        `<section className={isSelected ? cls + ' active' : cls} onTap={onSelect}>
            <section className="f-25">
                <div className="stakeroom-item__name">
                    {room.fullName1()}
                </div>
            </section>
            <section className="f-5">
                {room.isForBeginners &&
                    <div className="stakeroom-item__icon"><Icon i="injured"/></div>
                }
                {room.isFeatured &&
                    <div className="stakeroom-item__icon"><Icon i="star"/></div>
                }
                {room.isGuaranteed &&
                    <div className="stakeroom-item__icon"><Icon i="cup"/></div>
                }
            </section>
            <section className="w-10">
                {roomSize}
            </section>
            <section className="f-14">
                {room.mode == 'token' ?
                <Tokens m={entryFee} mod="small" symb="small"/>
                :
                <Dollars m={entryFee} mod="small" symb="small"/>
                }
            </section>
            <section className="f-14">
                {room.mode == 'token' ?
                    <Tokens m={room.pot} mod="big" symb="big"/>
                    :
                    <Dollars m={room.pot} mod="big green" symb="big"/>
                }
            </section>
            <section className="f-17">
                <RoomStartCounter room={room}
                    mode={"stake_size"}/>
            </section>
            <section className="w-15">
                {isSelected ? <Icon i="checkbox-checked"/> : <Icon i="checkbox"/>}
            </section>
        </section>`
