_ = require('underscore')
{Icon} = require('lib/ui')
CompetitionRoomListView = require('./CompetitionRoomListView')
Immutable = require('immutable')


WizardCompetitionRoomList = React.create(
    displayName: 'WizardCompetitionRoomList'

    mixins: [flux.Connect('rooms', (store) ->
        rooms: store.getTournamentFilteredRooms()
        types: store.getRoomTypes()
    ), flux.Connect('wizard', (store) ->
        {challengeRequestRoomId} = store.getState()
        challengeRequestRoom = null
        if challengeRequestRoomId
            challengeRequestRoom = flux.stores.rooms.getRoom(challengeRequestRoomId)

        {
            selectedRoomIds: store.getSelectedRoomIds()
            challengeRequestRoom: challengeRequestRoom
            challengeRequestRoomId: challengeRequestRoomId
            isInitialRoom: store.isInitialRoom()
        }
    )]

    componentWillMount: ->
        if @state.selectedRoomIds.count() > 0
            roomId = @state.selectedRoomIds.first()
            startDate = flux.stores.rooms.getRoom(roomId)?.startDate
            flux.actions.search.setStartDateFilter(startDate)
        else
            flux.actions.search.setStartDateFilter('nearest')

    componentWillUnmount: ->
        flux.actions.search.setStartDateFilter(null)

    actionRenderer: (roomId) ->
        {challengeRequestRoomId, selectedRoomIds} = @state
        disabled = challengeRequestRoomId is roomId
        selected = selectedRoomIds.has(roomId) or disabled
        onTap = ->
            return if disabled
            flux.actions.wizard.toggleCompetitionRoom(roomId)

        `<Icon i={selected ? 'checkbox-checked' : 'checkbox'} mod={disabled ? 'gray' : ''} onTap={onTap} />`


    render: ({}, {rooms, types, selectedRoomIds, isInitialRoom, challengeRequestRoom, challengeRequestRoomId}) ->
        if challengeRequestRoom
            rooms = rooms.filterNot((r) -> r is challengeRequestRoom)
            rooms = Immutable.Seq([challengeRequestRoom]).concat(rooms).toList()

        `<CompetitionRoomListView
            rooms={rooms}
            isInitialRoom={isInitialRoom}
            types={types}
            selectedRoomIds={selectedRoomIds}
            challengeRequestRoomId={challengeRequestRoomId}
            actionRenderer={this.actionRenderer} />`
)

module.exports = WizardCompetitionRoomList
