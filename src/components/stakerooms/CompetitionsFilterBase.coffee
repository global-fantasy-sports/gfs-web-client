_ = require('underscore')
{Btn, Icon} = require('../../lib/ui')
{cx, prevent} = require('../../lib/utils')
Slider = require('../../lib/ui/Slider')
{COMPETITION_ROOM_TEXTS} = require('../const/index')
Tooltip = require('../tooltip/Tooltip')


module.exports = React.create(
    displayName: "CompetitionsFilterBase"

    mixins: [flux.Connect('search', (store) ->
        onlyBeginners: store.isOnlyBeginners()
        onlyFeatured: store.isOnlyFeatured()
        onlyGuaranteed: store.isOnlyGuaranteed()
        interval: store.getEntryFeeInterval()
    )]

    onSliderChange: ([start, end]) ->
        flux.actions.search.setEntryFeeInterval(start, end)

    onExpand: (e) ->
        e.preventDefault()
        @props.onExpand()

    onBeginnerTap: ->
        flux.actions.search.toggleBeginners()

    onFeaturedTap: ->
        flux.actions.search.toggleFeatured()

    render: ({expanded}, {onlyBeginners, onlyFeatured, onlyGuaranteed, interval}) ->
        onBeginnerTap = @onBeginnerTap
        onFeaturedTap = @onFeaturedTap
        onGuaranteedTap = @onGuaranteedTap

        beginnerCs = cx({
            'blue-base-active': onlyBeginners,
            'blue-base': not onlyBeginners
        })

        featuredCs = cx({
            'blue-base-active': onlyFeatured,
            'blue-base': not onlyFeatured
        })

        `<div className="filter-base">
            <div className="filter-base__item">
                <div className="filter-base__buttons">
                    <Tooltip text={COMPETITION_ROOM_TEXTS.beginner.button} viewMod="texted">
                        <Btn mod={beginnerCs} onTap={onBeginnerTap}>Beginner</Btn>
                    </Tooltip>
                    <Tooltip text={COMPETITION_ROOM_TEXTS.featured.button} viewMod="texted">
                        <Btn mod={featuredCs} onTap={onFeaturedTap}>Featured</Btn>
                    </Tooltip>
                    <Tooltip text={COMPETITION_ROOM_TEXTS.guaranteed.button} viewMod="texted">
                        <Btn mod="blue-base-disabled">Guaranteed to run</Btn>
                    </Tooltip>
                </div>
            </div>
            <div className="filter-base__slider">
                <p className="filter-base__title">Entry fee</p>
                <Slider
                    min={0} max={40}
                    onChange={this.onSliderChange}
                    value={interval}
                    defaultValue={[0, 40]}
                />
            </div>
            <a href="#" className="filter-base__more-btn" onTap={this.onExpand} onClick={prevent}>
                <Icon
                    i="nfl-arrow-right"
                    className={expanded ? 'rotateBack-90' : 'rotate-90'}
                />
            </a>
        </div>`
)
