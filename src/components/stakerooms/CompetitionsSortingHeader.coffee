{SortingHeader} = require('./stakerooms')

if SPORT in ['nfl', 'nba']
    module.exports = React.create(
        displayName: "CompetitionsSortingHeader"

        render: ({orderBy, sortingDirection, sortingField}) ->

            `<section className="game-table game-table--header">
                <div className="flex-row-start-stretch w-25">
                    <SortingHeader>
                        Game
                    </SortingHeader>
                </div>
                <div className="flex-row-start-stretch w-5"></div>
                <div className="flex-row-start-stretch w-10">
                    <SortingHeader onTap={orderBy('entries')}
                                   direction={sortingDirection}
                                   selected={sortingField == 'entries'}
                                   mod="right">
                        Entries
                    </SortingHeader>
                </div>
                <div className="flex-row-start-stretch w-14">
                    <SortingHeader onTap={orderBy('entry')}
                                   direction={sortingDirection}
                                   selected={sortingField == 'entry'}
                                   mod="right">
                        Entry fee
                    </SortingHeader>
                </div>
                <div className="flex-row-start-stretch w-14">
                    <SortingHeader onTap={orderBy('prizes')}
                                   direction={sortingDirection}
                                   selected={sortingField == 'prizes'}
                                   mod="right">
                        Prize pool
                    </SortingHeader>
                </div>
                <div className="flex-row-start-stretch w-17">
                    <SortingHeader onTap={orderBy('starts')}
                                   direction={sortingDirection}
                                   selected={sortingField == 'starts'}
                                   mod="right">
                        Starts
                    </SortingHeader>
                </div>
                <div className="flex-row-start-stretch w-15"></div>
            </section>`
    )

if SPORT == 'golf'
    module.exports = React.create(
        displayName: "CompetitionsSortingHeader"

        render: ({orderBy, sortingDirection, sortingField}) ->

            `<section className="game-table game-table--header game-table--golf">
                <div className="flex-row-start-stretch w-7">
                    <SortingHeader onTap={orderBy('type')}
                                   direction={sortingDirection}
                                   selected={sortingField == 'type'}>
                        Type
                    </SortingHeader>
                </div>
                <div className="flex-row-start-stretch w-20">
                    <SortingHeader onTap={orderBy('game')}
                                   direction={sortingDirection}
                                   selected={sortingField == 'game'}>
                        Game
                    </SortingHeader>
                </div>
                <div className="flex-row-start-stretch w-5"></div>
                <div className="flex-row-start-stretch w-10">
                    <SortingHeader onTap={orderBy('entries')}
                                   direction={sortingDirection}
                                   selected={sortingField == 'entries'}
                                   mod="right">
                        Entries
                    </SortingHeader>
                </div>
                <div className="flex-row-start-stretch w-14">
                    <SortingHeader onTap={orderBy('entry')}
                                   direction={sortingDirection}
                                   selected={sortingField == 'entry'}
                                   mod="right">
                        Entry fee
                    </SortingHeader>
                </div>
                <div className="flex-row-start-stretch w-14">
                    <SortingHeader onTap={orderBy('prizes')}
                                   direction={sortingDirection}
                                   selected={sortingField == 'prizes'}
                                   mod="right">
                        Prize pool
                    </SortingHeader>
                </div>
                <div className="flex-row-start-stretch w-15">
                    <SortingHeader onTap={orderBy('starts')}
                                   direction={sortingDirection}
                                   selected={sortingField == 'starts'}
                                   mod="right">
                        Starts (ET)
                    </SortingHeader>
                </div>
                <div className="flex-row-start-stretch w-15"></div>
            </section>`
    )
