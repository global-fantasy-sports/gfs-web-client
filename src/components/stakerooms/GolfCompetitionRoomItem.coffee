{Tokens, Dollars, BtnLink} = require('lib/ui')
{cx} = require('lib/utils')
RoomStartCounter = require('./RoomStartCounter')


GolfCompetitionRoomItem = React.create(
    displayName: 'GolfCompetitionRoomItem'

    render: ({room, actionComponent}) ->
        mode = room.mode
        entryFee = room.entryFee

        badgeClass = cx(
            "stakeroom-item__badge": true
            "stakeroom-item__badge--five-ball": room.gameType == "five-ball"
            "stakeroom-item__badge--handicap": room.gameType == "handicap-the-pros"
            "stakeroom-item__badge--hole-by-hole": room.gameType == "hole-by-hole"
        )
        switch room.gameType
            when "five-ball"
                badgeText = "5 BALL"
            when "handicap-the-pros"
                badgeText = "HANDICAP-THE-PROS"
            when "hole-by-hole"
                badgeText = "HOLE-BY-HOLE"

        `<section className="stakeroom-item stakeroom-item--golf">
            <div className={badgeClass}>
                {badgeText}
            </div>
            <div className="stakeroom-item__name f-20">
                {room.fullName1()}
            </div>
            <div className="flex-row-center f-7">
                {room.isForBeginners &&
                    <span className="stakeroom-item__icon"><Icon i="injured"/></span>
                }
                {room.isFeatured &&
                    <span className="stakeroom-item__icon"><Icon i="star"/></span>
                }
                {room.isGuaranteed &&
                    <span className="stakeroom-item__icon"><Icon i="cup"/></span>
                }
            </div>
            <div className="flex-row-end w-8">
                <div>
                    <span className="stakeroom-item__games-count">{room.gamesCount}</span>
                    <span className="stakeroom-item__max-entries"> / {room.maxEntries}</span>
                </div>
            </div>
            <div className="flex-row-end w-14">
                {mode == 'token' ?
                <Tokens m={entryFee} mod="small" symb="small"/>
                    :
                <Dollars m={entryFee} mod="small" symb="small"/>
                    }
            </div>
            <div className="flex-row-end w-14">
                {mode == 'token' ?
                <Tokens m={room.pot} mod="big" symb="big"/>
                    :
                <Dollars m={room.pot} mod="green big" symb="big"/>
                    }
            </div>
            <div className="flex-row-end f-17">
                <RoomStartCounter room={room}/>
            </div>
            <div className="flex-row-end w-15">
                {actionComponent}
            </div>
        </section>`
)

module.exports = GolfCompetitionRoomItem
