module.exports = React.create
    displayName: 'ItemWrapper'

    getInitialState: ->
        {}

    render: ({game, children, mod, active, indication}, S) ->
        cls = 'game-table '
        gameId = game?.id
        `<section id={"game__" + gameId} {...this.props}
                  className={!mod ? cls : cls + mod}
                  key={gameId}>
            {children}
        </section>`

