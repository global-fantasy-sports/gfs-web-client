{Icon, Tokens, Dollars} = require('lib/ui')
{cx} = require('lib/utils')
{now} = require('lib/date')

RoomStartCounter = require('./RoomStartCounter')
GuideHint = require('../guide/GuideHint')



exports.StakeRoomInfo = StakeRoomInfo = React.create
    displayName: 'StakeRoomInfo'

    render: () ->

        roomId = null
        if @props.room
            roomId = @props.room.id
        else if @props.game
            roomId = @props.game.competitionRoomId

        if roomId
            room = flux.stores.rooms.getRoom(roomId)
            mode = room.mode
            entryFee = room.entryFee
            gamesCount = room.gamesCount
            maxEntries = room.maxEntries

            `<ul className="data-items data-items--row">
                <li className="data-item">
                    {'Entries: '}
                    <b>
                        <Icon i="user"/>
                        {gamesCount + ' / ' + maxEntries}
                    </b>
                </li>
                <li className="data-item">
                    {'Entry size: '}
                    <b>
                        {mode == 'token' ?
                        <Tokens m={entryFee}/>
                        :
                        <Dollars m={entryFee}/>
                        }
                    </b>
                </li>
            </ul>`
        else
            return `<ul></ul>`


exports.PrizesDistribution = PrizesDistribution = React.create
    displayName: 'PrizesDistribution'
    args:
        room: 'object'

    render: ({room}) ->
        distribution = room.prizeList.toOrderedMap().map((prize, place) ->
            `<tr key={place}>
                <td>{place} place</td>
                <td>
                    {room.mode == 'token' ? <Tokens m={prize}/> : <Dollars m={prize}/>}
                </td>
            </tr>`
        ).toArray()

        `<table className="prizes-distribution">
            <thead className="prizes-distribution__head">
                <tr>
                    <th colSpan="3">Prizes distribution</th>
                </tr>
            </thead>
            <tbody className="prizes-distribution__content">
                {distribution}
            </tbody>
        </table>`


exports.MiniStakeRoomList = MiniStakeRoomList = React.create
    displayName: 'MiniStakeRoomList'

    render: ({rooms}, S) ->
        if rooms.size
            els = rooms.map (room, index) =>
                `<MiniStakeRoomItem room={room} key={index} />`
            els = els.toArray()
        else
            els = `<tr className="prizes-table__row">
                       <td className="prizes-table__el sorry" colSpan="3">
                            <p className="stroke small">
                                There are no competitions
                            </p>
                       </td>
                   </tr>`

        `<table className="prizes-table centered">
            <thead className="prizes-table__head">
                <tr>
                    <th>Entry fee</th>
                    <th>Prize pool</th>
                    <th>Entries</th>
                </tr>
            </thead>
            <tbody className="prizes-table__body">
                {els}
            </tbody>
        </table>`



MiniStakeRoomItem = React.create
    displayName: 'MiniStakeRoomItem'
    args:
        room: 'object'

    render: ({room}, S) ->
        row = 'prizes-table__row'
        entryFee = room.entryFee
        pot = room.pot
        gamesCount = room.gamesCount
        maxEntries = room.maxEntries

        `<tr className={row}>
            <td className="prizes-table__el">
                <Dollars m={entryFee} mod="small green"/>
            </td>
            <td className="prizes-table__el">
                <Dollars m={pot} mod="small green"/>
            </td>
            <td className="prizes-table__el">
                <p className="stroke white bold">
                    {gamesCount} / {maxEntries}
                </p>
            </td>
        </tr>`


exports.SortingHeader = React.create
    displayName: 'SortingHeader'

    render: ({children, selected, mod, direction}) ->
        mod ?= ""
        cls = 'sorting-header ' + mod
        `<div {...this.props} className={selected ? cls + ' selected' : cls}>
            <Icon i= {direction == "asc" && selected ? "arrow-sort-rev" : "arrow-sort"}
                  className={direction == "asc" && selected ? "rotateBack-180" : ""}/>
            {children}
        </div>`


COMPARATORS =
    game: (room) ->
        -((room.pot * 1000) +
          (room.gamesCount * 10))

    entries: (room) ->
        n = -(room.gamesCount / room.maxEntries)
        dt = now() / room.startDate
        return n + dt

    entry: (room) ->
        -room.entryFee

    prizes: (room) ->
        -room.pot

    starts: (room) ->
        n = -(room.gamesCount / room.maxEntries)

        +room.startDate * 100 + n


exports.getStakeRooms = getStakeRooms = (rooms, filter, sortBy = 'starts', isDescending = false) ->
    rooms = rooms.toSeq()

    for filter in filter
        rooms = rooms.filter(filter) if filter

    comp = COMPARATORS[sortBy]
    rooms = rooms.sort((a, b)->
        if comp(a) > comp(b) then (if isDescending then 1 else -1)
        else if comp(a) < comp(b) then (if isDescending then -1 else 1)
        else 0
    )

    rooms.cacheResult()

