_ = require('underscore')


module.exports = React.create
    displayName: 'StakeRoomProfile'

    render: ({user, label, idx, children, mod}, S) ->
        profile = (if mod then "profile " + mod else "profile")
        avatarUrl = user.avatar
        name = user.name

        `<div className={profile}>
            {idx != null ?
            <div className="user-pos">
                {label && _.isString(label) ?
                <div className="profile__my-badge">
                    {label}
                </div>
                    : null}
                <div className="user-pos__square">
                    <span className="user-pos__num">{idx + 1}</span>
                </div>
            </div>
                : null }
            <div className="profile__avatar">
                <img src={avatarUrl} width="50" height="50" />
            </div>
            <div className="profile__person-info">
                <div className="person-info">
                    <div className="person-info__name">
                        {name}
                    </div>
                </div>
                {children}
            </div>
        </div>`

