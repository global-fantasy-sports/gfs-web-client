{Tokens, Dollars, BtnLink, Icon} = require('lib/ui')
RoomStartCounter = require('components/stakerooms/RoomStartCounter')
Tooltip = require('../tooltip/Tooltip')
{COMPETITION_ROOM_TEXTS} = require('../const/index')


CompetitionRoomItem = React.create(
    displayName: 'CompetitionRoomItem'

    getInitialState: ->
        hovered: false

    onMouseEnterHandler: ->
        @setState({hovered: true})

    onMouseLeaveHandler: ->
        @setState({hovered: false})

    render: ({room, actionComponent}, {hovered}) ->
        mode = room.mode
        entryFee = room.entryFee

        `<section className="stakeroom-item"
                  id={'competitionroomitem_' + room.id}
                  onMouseEnter={this.onMouseEnterHandler}
                  onMouseLeave={this.onMouseLeaveHandler}>
            <section className="stakeroom-item__name w-23">
                {room.fullName1()}
            </section>
            <section className="flex-row-center w-7">
                {room.isForBeginners &&
                    <Tooltip text={COMPETITION_ROOM_TEXTS.beginner.button} viewMod="texted">
                        <span className="stakeroom-item__icon">
                            {hovered ? <Icon i="injured-white"/> : <Icon i="injured"/>}
                        </span>
                    </Tooltip>
                }
                {room.isFeatured &&
                    <Tooltip text={COMPETITION_ROOM_TEXTS.featured.button} viewMod="texted">
                        <span className="stakeroom-item__icon">
                            {hovered ? <Icon i="star-white"/> : <Icon i="star"/>}
                        </span>
                    </Tooltip>
                }
                {room.isGuaranteed &&
                    <Tooltip text={COMPETITION_ROOM_TEXTS.guaranteed.button} viewMod="texted">
                        <span className="stakeroom-item__icon">
                            {hovered ? <Icon i="cup-white"/> : <Icon i="cup"/>}
                        </span>
                    </Tooltip>
                }
            </section>
            <section className="flex-row-end w-10">
                <div>
                    <span className="stakeroom-item__games-count">{room.gamesCount}</span>
                    <span className="stakeroom-item__max-entries"> / {room.maxEntries}</span>
                </div>
            </section>
            <section className="flex-row-end w-14">
                {mode == 'token' ?
                <Tokens m={entryFee} mod="small" symb="small"/>
                    :
                <Dollars m={entryFee} mod="small" symb="small"/>
                    }
            </section>
            <section className="flex-row-end w-14">
                {mode == 'token' ?
                <Tokens m={room.pot} mod="big" symb="big"/>
                    :
                <Dollars m={room.pot} mod="green big" symb="big"/>
                    }
            </section>
            <section className="flex-row-end w-17">
                <RoomStartCounter room={room}/>
            </section>
            <section className="flex-row-end w-15">
                {actionComponent}
            </section>
        </section>`
)

module.exports = CompetitionRoomItem
