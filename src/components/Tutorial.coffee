_ = require 'underscore'
{BtnLink} = require 'lib/ui'
{Titlebar} = require 'lib/ui/Titlebar'
{START_CONFIG} = require '../start/start-config'

module.exports = React.create
    displayName: 'Tutorial'

    render: ->
        {path} = _.last(@props.branch) if @props.branch
        CONFIG = START_CONFIG[path]
        t = CONFIG.tutorial
        section = _.map t.steps, (step) ->
            `<section className="tutorial__section">
                <div className="tutorial__section-name">
                    <Titlebar title={step.title} mod="h3"/>
                </div>
                <div className="tutorial__img">
                    <img src={'/static/img/' + step.img}/>
                </div>
                <div className="tutorial__text-box">
                    <p className="stroke white bold">
                        {step.text}
                    </p>
                </div>
            </section>`

        `<section className="underlay-wrap">
            <section className="tutorial">
                <header className="tutorial__head">
                    <Titlebar title={CONFIG.name + ' stories'}/>
                    <div className="tutorial__story">
                        <p className="stroke white bold">
                            {t.story}
                        </p>
                    </div>
                </header>
                <section className={path == "five-ball" ?
                        "tutorial__sections small" :
                        "tutorial__sections"}>
                    {section}
                </section>
                <footer className="tutorial__back">
                    <BtnLink to={"/" + SPORT + "/"} mod="white-linear wide">
                        Exit
                    </BtnLink>
                    <BtnLink to={"/" + SPORT + "/" + path + "/"} mod="red-linear wide">
                        Play
                    </BtnLink>
                </footer>
            </section>
        </section>`
