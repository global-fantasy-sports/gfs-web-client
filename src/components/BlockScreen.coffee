Logo = require('../lib/ui/Logo')
{Btn} = require('../lib/ui')
{Input} = require('../lib/forms')
xhr = require('../lib/xhr')

class EmailCollectForm extends React.Component
    constructor: (props) ->
        super(props)
        @state = {
            email: ''
            done: false
            submitting: false
            error: null
        }

    handleEmailChange: (e) =>
        @setState({
            email: e.target.value
            error: null
        })

    handleSubmit: (e) =>
        e.preventDefault()
        {email, submitting} = @state
        if submitting
            return

        email = email.trim()
        if not email
            @setState({error: 'Please enter an email'})
        else
            @setState({submitting: true})
            xhr.post('/user/contact-email/', {email}).then(
                () => @setState({done: true, submitting: false})
                (error) => @setState({error, submitting: false})
            )

    render: () ->
        {email, error, submitting, done} = @state
        if done
            `<div className="contact-email-form">
                <p>Subscribed successfully!</p>
            </div>`
        else
            `<form onSubmit={this.handleSubmit} className="contact-email-form">
                <Input
                    type="email"
                    placeholder="Your Email"
                    value={email}
                    error={error}
                    disabled={submitting}
                    onChange={this.handleEmailChange}
                />
                <Btn mod="blue-base" disabled={submitting}>
                    Send
                </Btn>
            </form>`


class BlockScreen extends React.Component

    render: ->
        `<section className="block-screen">
            <header className="block-screen__header">
                <img
                    src="/static/img/404/logo.png"
                    alt="Global Fantasy Sports"
                    width="160" height="48"
                />
            </header>
            <article className="block-screen__content">
                <h1>
                    We&apos;re really sorry, but unfortunately<br />
                    we&apos;re not able to offer our fantasy<br />
                    games in your area.
                </h1>
                <p>
                    However, enter your email below and we&apos;ll inform you
                    as soon as they&apos;re available. Thank you.
                </p>

                <EmailCollectForm />
            </article>
            <footer className="block-screen__footer">
                <p>
                    Residents of the States of Arizona, Iowa, Illinois,
                    Louisiana, Montana, Nevada, North Dakota, New York,
                    Tennessee, Vermont and Washington, as well as Puerto Rico,
                    and where otherwise prohibited by law, are not eligible to
                    enter Global Fantasy Sports Inc pay to play contest
                    (as defined in the Contest Rules).
                </p>
                <nav className="links">
                    <a href="/policy/" target="_blank">Privacy policy</a>
                    <a href="/terms/">Terms and conditions</a>
                    <a href="/contest-rules/">Contest rules</a>
                </nav>
                <p>
                    &copy; 2014-2016 Global Fantasy Sports Inc.
                    All Rights Reserved.
                </p>
            </footer>
        </section>`


module.exports = BlockScreen
