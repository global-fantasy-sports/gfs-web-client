{history} = require('../world')


class Page404 extends React.Component

    handleBack: (e) ->
        e.preventDefault()
        history.go(-1)

    render: () ->
        `<div className="error404-wrapper">
            <div className="error404">
                <h1 className="error404__header">This page does not exist</h1>
                <div className="error404__content">
                    <p>Something went wrong. This page has been deleted or has
                        not been created.</p>
                    <div className="error404-buttons">
                        <a className="btn" href="#" onClick={this.handleBack}>
                            Back
                        </a>
                        <a className="btn btn--primary" href="/">Homepage</a>
                    </div>
                </div>
            </div>
            <div className="error404-logo">
                <img src="/static/img/404/logo.png" width="160" height="48"
                     alt="Global Fantasy Sports" />
            </div>
        </div>`

module.exports = Page404
