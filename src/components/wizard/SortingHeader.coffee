_ = require 'underscore'
{MenuButton} = require 'lib/ui'


module.exports = React.create
    displayName: 'SortingTable'

    mixins: [flux.Connect('search', (store) ->
        {
            orderName: store.getOrderName()
            sortOrders: store.getSortOrders()
        }
    )]

    direction: (key) ->
        if @state.sortOrders.get(key) == 'DESC' then '-rev' else ''

    sortBy: (orderName) ->
        (e) =>
            e.preventDefault()
            flux.actions.search.toggleSortOrder(orderName)

    render: ({type}, {orderName, sort, sortOrders}) ->
        handicap = flux.stores.state.getState().gameType == 'hole-in-one'

        nameButton = `<MenuButton
            key='golfer'
            i={"arrow-sort" + this.direction('golfer')}
            active={orderName == 'golfer'}
            onTap={this.sortBy('golfer')}>
                Golfer
        </MenuButton>`

        handicapButton = `<MenuButton
            key='handicap'
            i={"arrow-sort" + this.direction('handicap')}
            active={orderName == 'handicap'}
            onTap={this.sortBy('handicap')}>
                Handicap
        </MenuButton>`

        rankButton = `<MenuButton
            key='rank'
            i={"arrow-sort" + this.direction('rank')}
            active={orderName == 'rank'}
            onTap={this.sortBy('rank')}
        >
            World Rank
        </MenuButton>`

        scoreButton = `<MenuButton
            key='lastScore'
            i={"arrow-sort" + this.direction('lastScore')}
            active={orderName == 'lastScore'}
            onTap={this.sortBy('lastScore')}>
                Last to-Par
        </MenuButton>`

        countryClass = "f-30"
        nameClass = "f-35"
        handicapClass = "f-12"
        selectClass = "f-12"

        if not @props.type
            `<nav className="sorting-table">
                <div className="sorting-table__el f-27 centered">Country</div>
                <div className="sorting-table__el f-30 left">
                    Golfer
                </div>
                <div className="sorting-table__el f-15 centered">
                    World Rank
                </div>
                <div className="sorting-table__el f-20 left">
                    Last to-Par
                </div>
                <div className="sorting-table__el f-8 centered">
                    Select
                </div>
            </nav>`
        else
            `<div className="sorting-table">
                <div className={"sorting-table__el centered " + countryClass}>
                    Country
                </div>
                <div className={"sorting-table__el " + nameClass}>
                    {nameButton}
                </div>
                <div className={"sorting-table__el " + handicapClass}>
                    {handicap ? handicapButton : rankButton}
                </div>
                <div className="sorting-table__el f-12">
                    {handicap ? rankButton : scoreButton}
                    </div>
                <div className="sorting-table__el centered f-12">
                    Statistics
                    </div>
                <div className={"sorting-table__el centered " + selectClass}>
                    Select
                </div>
            </div>`
