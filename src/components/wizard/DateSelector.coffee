_ = require('underscore')
{Btn, BtnLink} = require('lib/ui')
{TournamentSwitch} = require('lib/ui/TournamentSwitch')
{RoundSwitch} = require('lib/ui/RoundSwitch')
DateSelectorItem = require('./DateSelectorItem')


module.exports = React.create(
    displayName: "DateSelector"

    mixins: [
        flux.Connect("games", (store)=>
            availableEvents: store.getAvailableEvents()
        )
        flux.Connect("wizard", (store)=>
            selectedEvent: store.getSelectedEvent()
        )
    ]

    onNext: ->
        @props.onNext()

    onTap: (date, id) ->
        ->
            flux.actions.wizard.selectEvent(date, id)

    render: ({}, {availableEvents, selectedEvent}) ->
        gameType = @props.gameDef.name

        onTap = @onTap
        dates = availableEvents.map(({date, id}) ->
            `<DateSelectorItem selected={date == selectedEvent.date} date={date} onTap={onTap(date, id)} />`
        ).toList()

        `<section className="wizard__row">
          <div>{dates}</div>
        </section>`
)
