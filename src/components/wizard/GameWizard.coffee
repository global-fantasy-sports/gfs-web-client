_ = require('underscore')
flash = require('lib/flash')
WizardHead = require('./WizardHead')


module.exports = React.create(
    displayName: 'GameWizard'

    mixins: [
        flux.Connect('wizard', (store, props) -> store.getState(props))
        flux.Connect('roster', (store) -> {isRosterValid: store.isValid()})
        flux.Connect('state', (store) -> {isEdit: store.getMode() is "EDIT"})
    ]

    contextTypes:
        router: React.PropTypes.object

    componentDidMount: () ->
        setTimeout(=> flux.actions.wizard.setWizardGameType(@props.gameType))

    componentWillUpdate: ({step, gameDef}, {isEdit}) ->
        if isEdit and step in gameDef.disableEdit
            i = gameDef.steps.indexOf(step)
            @setUrl(gameDef.steps[i + 1])

    backFromChild: ->
        if @props.step == 'select'
            @context.router.transitionTo("/#{SPORT}/")
        else
            @context.router.goBack()

    currentStep: ->
        @props.gameDef[@props.step]

    nextStep: (current) ->
        i = @props.gameDef.steps.indexOf(current)
        @props.gameDef.steps[i + 1]

    previousStep: (current) ->
        i = @props.gameDef.steps.indexOf(current)
        @props.gameDef.steps[i - 1]

    applyStep: (name, args) ->
        step = @props.gameDef[name]
        func = step.next
        if func
            if func.call(this) == 'finish'
                return

        nextName = @nextStep(name)
        nextStep = @props.gameDef[nextName]

        if not nextStep
            return flash(
                type: 'error'
                text: 'Wizard logic error, please contact developers'
            )

        if not nextStep.predicate
            return @setUrl(nextName)

        result = nextStep.predicate.call(this)
        if not result
            return @setUrl(nextName)

        @applyStep(nextName, _.flatten([result]))

    goNextStep: (args...) ->
        @applyStep(@props.step, args)

    setUrl: (path) ->
        url = "/#{SPORT}/#{@props.gameType}/#{path}/"
        router = @context.router
        _.defer(-> router.transitionTo(url))

    renderStep: ({gameDef}, S) ->
        View = @currentStep().view

        `<View
            gameDef={gameDef}
            onNext={this.goNextStep}
            wizard={true}
        />`

    urlGuard: (segment) ->
        if not segment
            step = @props.gameDef.steps[0]
            predicate = @props.gameDef[step].predicate?.call(this)
            if predicate
                @applyStep(step, _.flatten([predicate]))
            else
                @setUrl(step)
            return `<div/>`

        step = @currentStep()
        if step.isValid and not step.isValid.call(this)
            @setUrl(@previousStep(segment))
            return `<div/>`

    handleExit: () ->
        flux.actions.roster.clear()
        @context.router.transitionTo("/#{SPORT}/")

    render: ({gameDef, gameType, title, step}, {isEdit, isStepDone, nextStepLabel, selectedRoomIds, errorCode}) ->
        logger.log(arguments, "GameWizard render arguments", "GameWizard")

        if isEdit and gameDef.disableEdit.includes(step)
            return null

        if ret = @urlGuard(step)
            return ret

        mainView = @renderStep(@props, @state)

        `<section className="wizard">
            <div className="wizard__inner">
                <div className="wizard__content">
                    {mainView}
                </div>
            </div>
            <div className="wizard__head">
                <div className="wizard__inner">
                    <WizardHead
                        title={title}
                        nextStepLabel={nextStepLabel}
                        gameDef={gameDef}
                        errorCode={errorCode}
                        currentStep={step}
                        editMode={isEdit}
                        isNextActive={isStepDone}
                        onNext={this.goNextStep}
                        onExit={this.handleExit}
                        onSelectStep={this.setUrl}
                    />
                </div>
            </div>
        </section>`
)
