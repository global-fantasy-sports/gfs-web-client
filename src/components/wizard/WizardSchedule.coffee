bemClassName = require('bem-classname')
{Icon} = require('../../lib/ui')

WizardScheduleDay = ({games, activeTeamIds, onSelect}) ->
    bem = bemClassName.bind(null, 'wizard-schedule-day')

    els = games.map((game, index) ->
        homeTeamClass = bem('team', {active: activeTeamIds.has(game.homeTeam), "left"})
        awayTeamClass = bem('team', {active: activeTeamIds.has(game.awayTeam), "right"})

        `<div key={index} className={bem('game')}>
            <span className={homeTeamClass} onTap={onSelect.bind(null, game.homeTeam)}>{game.homeTeam}</span>
            <span className={bem('divider')}>&nbsp;</span>
            <span className={awayTeamClass} onTap={onSelect.bind(null, game.awayTeam)}>{game.awayTeam}</span>
        </div>`
    )

    `<div className={bem()}>
        <div className={bem('top')}>{games.first().formatTime('ddd, hA')}</div>
        <div className={bem('content')}>{els}</div>
    </div>`



module.exports = React.create(
    displayName: "WizardSchedule"

    render: () ->
        {games, activeId, activeTeamIds, onSelect, onSelectAll} = @props

        days = games.groupBy((game) -> game.startTimeUtc).valueSeq().map((games, index) ->
            `<WizardScheduleDay
                key={index}
                games={games}
                activeTeamIds={activeTeamIds}
                onSelect={onSelect}
            />`
        )

        bem = bemClassName.bind(null, 'wizard-schedule')

        `<div className={bem()}>
            <div className={bem('all', {active: !activeId})} onTap={onSelectAll}>
                <div className={bem('all-top')}>
                    <p className={bem('games-size')}>{games.size}</p>
                    <p>GAMES</p>
                </div>
                <div className={bem('all-bottom')}>
                    <div onTap={onSelectAll} className={bem('all-button', {active: activeTeamIds.isEmpty()})}>Select All</div>
                </div>
            </div>
            <div className={bem('games')}>
                <div className={bem('icon-left')}>
                    <Icon i="nfl-arrow-left"/>
                </div>
                <div className={bem('games-wrap')}>
                    {days}
                </div>
                <div className={bem('icon-right')}>
                    <Icon i="nfl-arrow-right"/>
                </div>
            </div>
        </div>`
)
