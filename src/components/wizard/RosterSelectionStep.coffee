{SortDirection, AutoSizer} = require('react-virtualized')
bemClassname = require('bem-classname')

Connect = require('../../flux/connectMixin')
{Btn} = require('../../lib/ui')

{POSITIONS} = require('../const')
ParticipantFilter = require('../ParticipantFilter')
ParticipantSearch = require('../ParticipantSearch')

WizardSchedule = require('./WizardSchedule')
NFLParticipantsList = require('../roster/NFLParticipantsList')
LineupView = require('../roster/LineupView')
RosterInfo = require('../roster/RosterInfo')

{formatFPPG, formatSalary} = require('../../lib/format')


POSITIONS or= []

bem = bemClassname.bind(null, 'roster')


module.exports = React.create(
    displayName: 'RosterSelectionStep'

    mixins: [
        Connect('roster', (store) -> store.getRoster())
        Connect('lineups', (store) -> {
            currentLineupId: store.getCurrentLineupId()
        })
    ]

    componentDidMount: ->
        flux.actions.roster.init()

    handleSearchChange: (value) ->
        flux.actions.roster.search(value)

    handlePositionChange: (value) ->
        flux.actions.roster.filterByPosition(value)

    handleLineupRowClick: (rowRecord) ->
        flux.actions.roster.filterByPosition(rowRecord.position)

    handleSaveLineup: () ->
        flux.actions.state.openSaveLineupDialog(@state.lineup)

    handleImportLineup: () ->
        # TODO
        throw new Error('not implemented')

    handleTeamSelect: (teamId) ->
        flux.actions.roster.filterByTeam(teamId)

    selectAllTeams: () ->
        flux.actions.roster.selectAllTeams()

    handleFiltersReset: () ->
        flux.actions.roster.clearFilters()

    autosize: ({height, width}) ->
        `<div style={{border: '10px solid red', width: width + 'px', height: height + 'px' }}>norm</div>`

    renderActionButtons: ->
        saveLabel = if @state.lineup.id && @state.lineup.id == @state.currentLineupId then 'Update line-up' else 'Save line-up'

        [
            `<Btn
                mod="transparent-gray tiny bold"
                showIcon={true}
                icon={'import-medium'}
                onTap={null}
            >
                Import
            </Btn>`
            `<Btn
                mod="transparent-gray tiny bold o-70"
                showIcon={true}
                icon={'user-check'}
                onTap={this.handleSaveLineup}
            >
                {saveLabel}
            </Btn>`
        ]

    render: ({}, {budget, gameId, teamIds, games, isCurrentRosterAdded, lineup, open, participants, position, reversed, search, selected, sorting, disabledTeams, selectedTeams}) ->
        lineupHeight = lineup.positions.size * 60 + 40
        budget = lineup.getBudget()

        `<div className={bem()}>
            <div className={bem('row')}>
                <WizardSchedule
                    games={games}
                    activeId={gameId}
                    activeTeamIds={teamIds}
                    onSelect={this.handleTeamSelect}
                    onSelectAll={this.selectAllTeams}
                />
            </div>
            <div className={bem('row')}>
                <div className={bem('left')}>
                    <div className="wizard-filter wizard-filter--tall">
                        <ParticipantFilter
                            className="wizard-filter__buttons"
                            active={position}
                            values={POSITIONS}
                            onChange={this.handlePositionChange}
                        />
                        <ParticipantSearch
                            className="wizard-filter__search"
                            value={search}
                            onChange={this.handleSearchChange}
                        />
                    </div>
                    <NFLParticipantsList
                        width={588}
                        height={lineupHeight}
                        sortBy={sorting}
                        sortDirection={SortDirection[reversed ? 'DESC' : 'ASC']}
                        selected={selected}
                        openPositions={open}
                        participants={participants}
                        disabledTeams={disabledTeams}
                        selectedTeams={selectedTeams}
                        onResetFilters={this.handleFiltersReset}
                        budget={budget}
                    />
                </div>
                <div className={bem('right')}>
                    <RosterInfo
                        lineup={lineup}
                        isSaved={isCurrentRosterAdded}
                        onSave={this.handleSaveLineup}
                        onImport={this.handleImportLineup}
                        toolbarContent={this.renderActionButtons()}
                    />
                    <LineupView
                        width={588}
                        height={lineupHeight}
                        lineup={lineup}
                        onRowClick={this.handleLineupRowClick}
                    />
                </div>
            </div>
        </div>`
)
