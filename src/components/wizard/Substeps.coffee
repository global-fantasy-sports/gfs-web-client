_ = require('underscore')

WizardActionBar = require('lib/ui/WizardActionBar')


module.exports = React.create
    displayName: "Substeps"

    mixins: [flux.Connect("wizard", (store) ->
        isSubstepComplete: store.isSubstepComplete()
        currentSubstep: store.getCurrentSubstep()
        SubstepView: store.getSubstepView()
    )]

    getDefaultProps: ->
        stepsCount: 3

    componentDidMount: ->
        flux.actions.wizard.setCurrentSubstep(1)
        @updateScrollPosition()

    componentDidUpdate: (prevProps, {currentSubstep})->
        if currentSubstep isnt @state.currentSubstep
            _.defer(=> @updateScrollPosition())

    updateScrollPosition: ->
        flux.actions.state.scrollToTop({
            el: React.findDOMNode(@refs['substep'])
            offset: -100
        })

    onNext: ->
        {currentSubstep} = @state
        if @props.stepsCount == currentSubstep
            if flux.stores.state.getMode() == "EDIT"
                flux.actions.wizard.saveGame()
            @props.onNext()
        else
            flux.actions.wizard.setCurrentSubstep(currentSubstep + 1)

    render: (props, {isSubstepComplete, SubstepView, currentSubstep}) ->
        `<section className="substeps">
            <SubstepView {...props}
                ref="substep"
                index={currentSubstep} />

            <WizardActionBar
                isDone={isSubstepComplete}
                onNext={this.onNext} />
        </section>`
