_ = require('underscore')
{TournamentSwitch} = require('../../lib/ui/TournamentSwitch')
{RoundSwitch} = require('../../lib/ui/RoundSwitch')


module.exports = React.create(
    displayName: 'SelectTournamentRound'

    mixins: [
        flux.Connect('wizard', (store) ->
            tournamentId: store.getTournamentId()
            roundNumber: store.getRoundNumber()
        )
        flux.Connect('tournaments', (store, {gameType}) ->
            tournaments: store.getTournamentsAllowedToStart(gameType)
        )
    ]

    onNext: ->
        flux.actions.wizard.setEvent(@state.tournamentId, @state.roundNumber)
        @props.onNext()

    render: ({gameDef}, {tournaments, tournamentId, roundNumber}) ->
        `<section className="wizard-rounds">
            <div className="w-58">
                <TournamentSwitch
                    gameType={gameDef.name}
                    tournaments={tournaments}
                    selected={tournamentId}
                />
            </div>
            <div className="w-41">
                <RoundSwitch
                    selectedTournamentId={tournamentId}
                    selectedRound={roundNumber}
                />
            </div>
        </section>`
)
