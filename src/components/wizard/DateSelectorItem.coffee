{Btn} = require 'lib/ui.coffee'
{cx} = require 'lib/utils'

module.exports = React.create
    displayName: 'DateSelectorItem'

    render: ({selected, date, onTap}) ->
#        hint = `<GuideHint children="Click to select date"
#                           title="Select date"
#                           pointer="bottom"
#                           group="wizard"
#                           id="round"
#                           top="-142px"
#                           pos="centered"/>`
        classes = cx
            "round-switch__el": true
            "active": selected
            "inactive": false

        selectButton = `<Btn mod="regular small select inherit"
                             icon={selected ? 'ball-selected' : 'ball-select-empty'}
                             btnText={selected ? true : false}>
                            {selected ? 'Selected' : 'Select'}
        </Btn>`

        `<div className={classes} onTap={onTap}>
          <div className="round-switch__btn">
            <div className="round-switch__content">
              <span className="round-switch__text">{date.toString()}</span>
            </div>
          </div>
        </div>`
