_ = require('underscore')
bem = require('bem-classname')
{cx} = require('../../lib/utils')
{Btn} = require('../../lib/ui')
Tooltip = require('../tooltip/Tooltip')


class Step extends React.Component

    @bem: bem.bind(null, 'wizard-head')

    getIconSrc: () ->
        {icon, active, disabled} = @props

        iconType =
            if disabled then 'disabled'
            else if active then 'active'
            else 'inactive'

        "/static/img/wizard/#{icon}_#{iconType}.png"

    handleClick: (e) =>
        if not @props.disabled
            e.preventDefault()
            @props.onTap(@props.name)

    render: () ->
        {icon, label, active, disabled} = @props

        className = cx(Step.bem('step'), {'clickable': not disabled})

        iconClassName = Step.bem('step-img', {active, disabled})
        labelClassName = Step.bem('step-name', {active, disabled})

        `<div className={className} onTap={this.handleClick}>
            <div className={iconClassName}>
                <img src={this.getIconSrc()} alt={label} />
            </div>
            <p className={labelClassName}>{label}</p>
        </div>`


SplitLine = ({distance}) ->
    `<div
        className={Step.bem('line', {
            'fill': distance <= 0,
            'half': distance === 1
        })}
    />`


class WizardHead extends React.Component

    @bem: bem.bind(null, 'wizard-head')

    @contextTypes: {
        router: React.PropTypes.object
    }

    renderSteps: () ->
        {gameDef, currentStep, editMode, onSelectStep} = @props
        {steps, disableEdit} = gameDef

        activeIndex = steps.indexOf(currentStep)

        steps.reduce(((els, stepName, stepIndex) =>
            els.push(
                `<SplitLine
                    key={'l-' + stepIndex}
                    distance={stepIndex - activeIndex}
                />`) if stepIndex > 0

            activeStep = steps[activeIndex]
            {wizardIcon, title} = gameDef[stepName]
            {disabledSteps} = gameDef

            isDisabled =
                if editMode then disableEdit.includes(stepName)
                else if disabledSteps?[activeStep]
                    _(disabledSteps[activeStep]).includes(stepName)
                else stepIndex > activeIndex

            els.push(
                `<Step
                    key={'s-' + stepName}
                    name={stepName}
                    icon={wizardIcon}
                    label={title}
                    active={stepIndex === activeIndex}
                    disabled={isDisabled}
                    onTap={onSelectStep}
                />`)

            return els
        ), [])

    onNext: ->
        document.getElementById('info-tooltip')?.remove()
        @props.onNext()

    renderErrorContent: ->
        flux.stores.dialogs.getErrorTooltipContent(@props.errorCode)

    render: () ->
        {gameDef, title, isNextActive, onExit, currentStep, nextStepLabel, errorCode} = @props
        InfoTooltip = gameDef[currentStep].tooltip
        onNext = @onNext.bind(this)

        `<div className="wizard-head">
            <div className={WizardHead.bem('left')}>
                <p className={WizardHead.bem('name')}>{title}</p>
                <p className={WizardHead.bem('name-small')}>{gameDef.title}</p>
            </div>

            <div className={WizardHead.bem('center')}>
                {this.renderSteps()}
            </div>

            <div className={WizardHead.bem('right')}>
                <Btn
                    mod='gray offset-sides'
                    onTap={onExit}
                >
                    Exit
                </Btn>
                <Tooltip
                    content={this.renderErrorContent()}
                    mod='light-gray'
                    position='bottomLeft'
                    openned={!!errorCode}
                    disabled={!errorCode}
                    closeButton={true}
                >
                    <Btn
                        mod={cx('offset-sides', isNextActive ? 'green' : 'not-active')}
                        disabled={!isNextActive}
                        onTap={onNext}
                    >
                        {nextStepLabel}
                    </Btn>
                </Tooltip>
                {!!InfoTooltip && <InfoTooltip />}
            </div>
        </div>`


module.exports = WizardHead
