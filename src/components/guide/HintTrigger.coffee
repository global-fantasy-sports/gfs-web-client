module.exports = React.create
    displayName: 'HintTrigger'

    propTypes: {
        group: React.PropTypes.string.isRequired
        id: React.PropTypes.string.isRequired
        children: React.PropTypes.element.isRequired
    }

    onTap: (event) ->
        flux.actions.tap.guideHintTap(@props.group, @props.id)
        @props.children.props?.onTap?(event)

    render: ({group, id})->
        React.cloneElement(@props.children, {onTap: @onTap})
