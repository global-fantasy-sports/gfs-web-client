_ = require "underscore"
{Btn} = require 'lib/ui.coffee'
{getPosition} = require 'lib/utils.coffee'

module.exports = React.create
    displayName: 'GuideHint'

    mixins: [flux.Connect("guide", (store) ->
        guideActive: store.isGuideActive()
        store.getState()
    )]

    propTypes: {
        title: React.PropTypes.string.isRequired
        children: React.PropTypes.node.isRequired
        id: React.PropTypes.string.isRequired
        group: React.PropTypes.string.isRequired
        arrow: React.PropTypes.oneOf(['up', 'down'])
        pointer: React.PropTypes.oneOf(['top', 'bottom', 'left', 'right'])
        scrollTo: React.PropTypes.string
    }

    getDefaultProps: -> {
        arrow: null
        pointer: null
    }

    getInitialState: ->
        offset = {
            top: @props.top
            bottom: @props.bottom
            left: @props.left
            right: @props.right
            position: @props.pos
        }
        {offset: offset}


    componentDidMount: ->
        @updatePosition()
        window.addEventListener "resize", @updatePosition

    componentDidUpdate: ->
        @updatePosition()

    componentWillUnmount: ->
        window.removeEventListener "resize", @updatePosition

    updatePosition: ->
        if flux.stores.guide.isGuideHintActive(@props.group, @props.id) and @props.pos == "centered"
            {offset} = @state
            @setState _.extend offset, left: getPosition(React.findDOMNode(this)).centered
        else if flux.stores.guide.isGuideHintActive(@props.group, @props.id) and @props.pos == "absolute"
            {offset} = @state
            {top, centered} = getPosition(React.findDOMNode(this), "absolute")
            @setState _.extend offset,
                top: top
                left: centered
                position: @props.pos

    gotIt: ->
        {group, id, scrollTo} = @props
        flux.actions.tap.guideHintTap(group, id, scrollTo)

    render: ({children, title, id, group, pointer, arrow, scrollTo}, {guideActive, offset}) ->
        if flux.stores.guide.isGuideHintActive(group, id)
            btnIcon = "hint-arrow-#{arrow}" if arrow
            `<div className="guide-hint" style={offset}>
                <div className="guide-hint__inner">
                    <div className="guide-hint__title">{title}</div>
                    <div className="guide-hint__content">
                        {children}
                    </div>
                    <div className="guide-hint__foot">
                        <Btn onTap={this.gotIt}
                             mod={arrow ? "blue square regular small" : "blue-linear x-small"}
                             icon={btnIcon}>
                            {arrow ? null : 'Got it!'}
                        </Btn>
                    </div>
                    <span className={"guide-hint__pointer " + (pointer || '')} />
                </div>
            </div>`
        else null
