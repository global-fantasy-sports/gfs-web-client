{Btn} = require 'lib/ui.coffee'
{Titlebar} = require "lib/ui/Titlebar.coffee"


module.exports = React.create
    displayName: 'GuideHeader'

    mixins: [flux.Connect("guide", (store) ->
        guideActive: store.isGuideActive()
    )]

    close: ->
        flux.actions.tap.closeGuide()

    render: ({}, {guideActive}) ->

        if guideActive == true
            `<div className="guide-header">
                <div className="guide-header__title">
                    <Titlebar className="black" title="Interactive guide is active" mod="h3"></Titlebar>
                </div>
                <div className="guide-header__close">
                    <Btn mod="red-linear x-small" onTap={this.close}>Close guide</Btn>
                </div>
            </div>`
        else null
