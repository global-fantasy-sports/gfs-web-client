{Motion, spring} = require 'react-motion'
{cx} = require '../lib/utils'
{Icon} = require '../lib/ui'


translate = ({x}) -> {
    transform: "translate3d(#{x}px,0,0)"
    MozTransform: "translate3d(#{x}px,0,0)"
    WebkitTransform: "translate3d(#{x}px,0,0)"
}


class GamesFilterItem extends React.Component

    handleClick: (e)=>
        e.preventDefault()
        @props.onClick(@props.value)

    render: ->
        {time, children} = @props

        className = cx('team-filter__item', {
            'team-filter__item--active': @props.active
        })

        `<div className={className} onTap={this.handleClick}>
            <div className="team-filter__teams">
                {this.props.children}
            </div>
            <p className="team-filter__time">
                {time}
            </p>
        </div>`


module.exports = React.create
    displayName: "GamesFilter"

    getInitialState: ->
        selectedId: @props.selectedId
        offset: 0

    componentWillReceiveProps: (nextProps)->
        if nextProps.selectedId isnt @state.selectedId
            @setState({selectedId: nextProps.selectedId})

    scrollLeft: ->
        @setState(offset: Math.min(@state.offset + 100, 0))

    scrollRight: ->
        {scrollWidth, offsetWidth} = React.findDOMNode(@_scroller)
        limit = -1 * (scrollWidth - offsetWidth)

        @setState(offset: Math.max(@state.offset - 100, limit))

    handleGameSelect: (selectedId)->
        @setState({selectedId})
        setTimeout((-> flux.actions.roster.filterByGame(selectedId)), 10)

    renderGames: ->
        selectedId = @state.selectedId
        @props.contests.map((contest)=>
            `<GamesFilterItem
                key={contest.id}
                value={contest.id}
                time={contest.formatTime()}
                active={selectedId === contest.id}
                onClick={_this.handleGameSelect}>
                <p>{contest.homeTeam}</p>
                <p>{contest.awayTeam}</p>
            </GamesFilterItem>`
        )

    render: ({contests}, {selectedId, offset}) ->
        self = this

        allGamesCls = cx('team-filter__item team-filter__item--first', {
            'team-filter__item--active': not selectedId,
        })

        handleSelectAll = ()=>
            @handleGameSelect(null)

        React.createElement(Motion, {style: {x: spring(offset)}}, (style) =>
            `<div className="team-filter">
                <div className={allGamesCls} onClick={handleSelectAll}>
                    <p className="team-filter__title">GAMES</p>
                    <p className="team-filter__count">{contests.count()}</p>
                </div>

                <div className="team-filter__item team-filter__item--arrow"
                     onClick={self.scrollLeft}>
                    <Icon i="nfl-arrow-left"/>
                </div>

                <div className="team-filter__games">
                    <div className="team-filter__inner"
                         style={translate(style)}
                         ref={function(ref) { self._scroller = ref }}>

                        {self.renderGames()}

                    </div>
                </div>

                <div className="team-filter__item team-filter__item--arrow"
                     onClick={self.scrollRight}>
                    <Icon i="nfl-arrow-right" />
                </div>
            </div>`
        )
