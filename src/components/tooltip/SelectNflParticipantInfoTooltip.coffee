InfoTooltip = require './InfoTooltip'


SelectNflParticipantInfoTooltip = React.create
    render: ->
        content = flux.stores.dialogs.getText('nfl', 'select')()
        `<InfoTooltip settings="nfl-participants" content={content} />`


module.exports = SelectNflParticipantInfoTooltip
