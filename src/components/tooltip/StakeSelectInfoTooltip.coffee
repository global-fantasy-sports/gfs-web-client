InfoTooltip = require './InfoTooltip'


StakeSelectInfoTooltip = React.create
    render: ->
        content = flux.stores.dialogs.getText('hole-by-hole', 'stake')
        `<InfoTooltip settings="five-ball-participants" content={content} />`


module.exports = StakeSelectInfoTooltip
