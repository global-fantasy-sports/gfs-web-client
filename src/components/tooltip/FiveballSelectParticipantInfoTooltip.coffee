InfoTooltip = require './InfoTooltip'


FiveballSelectParticipantInfoTooltip = React.create
    render: ->
        content = flux.stores.dialogs.getText('five-ball', 'select')

        `<InfoTooltip settings="five-ball-participants" content={content} />`


module.exports = FiveballSelectParticipantInfoTooltip
