_ = require 'underscore'
{cx} = require '../../lib/utils'
{Icon} = require '../../lib/ui'


Tooltip = React.create
    displayName: 'Tooltip'

    propTypes: {
        text: React.PropTypes.string
        content: React.PropTypes.any
        position: React.PropTypes.string.isRequired
        closable: React.PropTypes.bool
        openned: React.PropTypes.bool
        settings: React.PropTypes.string
        onTap: React.PropTypes.func
    }


    getDefaultProps: ->
        position: 'top'

    getInitialState: ->
        tooltip: null

    componentDidMount: ->
        if @props.openned
            @openTooltip()

    componentWillUnmount: ->
        @closeTooltip()

    componentWillReceiveProps: (props) ->
        if props.openned != @props.openned
            if props.openned
                @openTooltip(props)
            else
                @closeTooltip()

    updatePosition: ->
        div = @state.tooltip
        @setPosition(div, @props.position)

    setPosition: (div, position) ->
        rect = @refs.node.getBoundingClientRect()
        height = document.documentElement.clientHeight
        width = document.documentElement.clientWidth
        center = rect.left + rect.width / 2

        divRect = div.getBoundingClientRect()

        if position == 'top'
            div.style.bottom = "#{height - rect.bottom + rect.height + 10}px"
            div.style.left = "#{center - divRect.width / 2}px"

        if position == 'bottomLeft'
            div.style.top = "#{rect.top + rect.height + 10}px"
            div.style.right = "#{width - center - 15}px"

    closeTooltip: (isTracked) ->
        @props.onClose?(isTracked)
        document.removeEventListener("scroll", @updatePosition)
        document.removeEventListener("resize", @updatePosition)
        @state.tooltip?.remove()
        @setState({tooltip: null})

    onTap: ->
        if @props.closable
            isOpen = if @state.tooltip then true else false

            if @props.onTap
                @props.onTap(!isOpen)
            else
                if isOpen
                    @closeTooltip(true)
                else
                    @openTooltip()

    onMouseEnter: ->
        if not @props.closable then @openTooltip()

    openTooltip: (props) ->
        props = props || @props
        return if props.disabled

        @state.tooltip?.remove()
        mods = props.mod?.split(' ') || []
        csMods = mods.map((m) -> "tooltip-view--#{m}")

        div = document.createElement('div')
        cs = cx('tooltip-view', csMods, {
            "tooltip-view--#{props.position}": !!props.position
        })

        div.setAttribute('class', cs)
        div.setAttribute('id', 'info-tooltip')

        text = props.text
        content = props.content
        closeTooltip = @closeTooltip.bind(@, true)

        mods = ''
        if props.viewMod
            mods = props.viewMod.split(' ').map((m) -> "tooltip-content--#{m}")

        tooltipContent = `<div className={"tooltip-content " + mods}>
            {this.props.closeButton ? <Icon i="cross-gray" className="tooltip-content__close-button" onTap={closeTooltip} /> : null }
            {!!text && <span className="tooltip-content__text-only">{text}</span>}
            {content}
        </div>`
        inner = document.createElement('div')
        React.render(tooltipContent, inner)
        div.appendChild(inner)

        @setState tooltip: div
        document.body.appendChild(div)
        @setPosition(div, props.position)

        document.addEventListener("scroll", @updatePosition)
        document.addEventListener("resize", @updatePosition)

    onMouseLeave: ->
        if not @props.closable
            document.removeEventListener("scroll", @updatePosition)
            document.removeEventListener("resize", @updatePosition)
            @state.tooltip?.remove()

    render: ({children, position, mod}) ->
        cs = cx('tooltip-container', {
            "tooltip-container--#{mod}": !!mod
        })

        `<section className={cs} ref='node'
            onMouseEnter={this.onMouseEnter}
            onTap={this.onTap}
            onMouseLeave={this.onMouseLeave}>
            {children}
        </section>`



module.exports = Tooltip
