InfoTooltip = require './InfoTooltip'


SelectRoundInfoTooltip = React.create
    render: ->
        content = flux.stores.dialogs.getText('hole-by-hole', 'round')
        `<InfoTooltip settings="select-round" content={content} />`


module.exports = SelectRoundInfoTooltip
