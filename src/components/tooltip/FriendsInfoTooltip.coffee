InfoTooltip = require './InfoTooltip'


FriendsInfoTooltip = React.create
    render: ->
        content = flux.stores.dialogs.getStepText('friends')()
        `<InfoTooltip settings="five-ball-participants" content={content} />`


module.exports = FriendsInfoTooltip
