InfoTooltip = require './InfoTooltip'


HandicapCardInfoTooltip = React.create
    render: ->
        content = flux.stores.dialogs.getText('handicap-the-pros', 'handicap')
        `<InfoTooltip settings="five-ball-participants" content={content} />`


module.exports = HandicapCardInfoTooltip
