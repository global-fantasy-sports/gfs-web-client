_ = require 'underscore'
{cx} = require '../../lib/utils'
{Icon} = require '../../lib/ui'


NotificationTooltip = React.create
    displayName: 'NotificationTooltip'

    propTypes: {
        content: React.PropTypes.any
        panelOpen: React.PropTypes.bool.isRequired
        showOnlyImportant: React.PropTypes.bool.isRequired
    }

    getInitialState: ->
        tooltip: null

    componentDidMount: ->
        if @props.panelOpen
            @openTooltip()

    componentWillUnmount: ->
        @onClose()

    componentWillReceiveProps: (props) ->
        if props.panelOpen != @props.panelOpen
            if props.panelOpen
                @openTooltip(props)
            else
                @onClose()

    updatePosition: ->
        div = @state.tooltip
        @setPosition(div)

    setPosition: (div) ->
        rect = @refs.node.getBoundingClientRect()
        height = document.documentElement.clientHeight
        width = document.documentElement.clientWidth
        center = rect.left + rect.width / 2

        divRect = div.getBoundingClientRect()

        div.style.top = "#{rect.top + rect.height + 10}px"
        div.style.right = "#{width - center - 15}px"

    onClose: () ->
        document.removeEventListener("scroll", @updatePosition)
        document.removeEventListener("resize", @updatePosition)
        @state.tooltip?.remove()
        @props.onClose?()
        @setState({tooltip: null})

    openTooltip: (props) ->
        props = props || @props
        content = props.content

        @state.tooltip?.remove()
        div = document.createElement('div')
        cs = cx('tooltip-view', 'tooltip-view--bottomLeft', 'tooltip-view--notification',  {
            "tooltip-view--#{props.mod}": !!props.mod
        })

        div.setAttribute('class', cs)
        div.setAttribute('id', 'info-tooltip')

        {unread, showOnlyImportant} = props
        onClose = @onClose

        tooltipContent = `<div className="tooltip-content">
            <div className="tooltip-content__title">
                {unread == 1 ?
                <span>New notification</span>
                    :
                <span>{unread} new notifications</span>
                }
                <Icon i="cross-gray" className="tooltip-content__close-button" onTap={onClose} />
            </div>

            {content}
        </div>`
        inner = document.createElement('div')
        React.render(tooltipContent, inner)
        div.appendChild(inner)

        @setState tooltip: div
        document.body.appendChild(div)
        @setPosition(div)

        document.addEventListener("scroll", @updatePosition)
        document.addEventListener("resize", @updatePosition)

    render: ({children, mod}) ->
        cs = cx('tooltip-container', {
            "tooltip-container--#{mod}": !!mod
        })

        `<section
            className={cs}
            ref='node'>
            {children}
        </section>`



module.exports = NotificationTooltip
