InfoTooltip = require './InfoTooltip'


SelectParticipantInfoTooltip = React.create
    render: ->
        content = flux.stores.dialogs.getText('hole-by-hole', 'select')
        `<InfoTooltip settings="five-ball-participants" content={content} />`


module.exports = SelectParticipantInfoTooltip
