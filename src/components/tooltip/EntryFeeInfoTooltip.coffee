InfoTooltip = require './InfoTooltip'

EntryFeeInfoTooltip = React.create
    render: ->
        content = flux.stores.dialogs.getStepText('entry-fee')()
        `<InfoTooltip settings="entry-fee" content={content} />`


module.exports = EntryFeeInfoTooltip
