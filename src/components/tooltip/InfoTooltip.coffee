Tooltip = require './Tooltip'
{Icon} = require '../../lib/ui'


TooltipContent = React.create
    render: ({content}) ->
        `<div className="tooltip-content">
            {content}
        </div>`


InfoTooltip = React.create
    mixins: [flux.Connect('wizard', (store, props) ->
        isVisible: store.isInfoTooltipVisible(props.settings)
    )]

    onClose: (isTracked) ->
        if isTracked
            flux.actions.wizard.setInfoTooltipVisibility(@props.settings, false)

    onTap: (isOpen) ->
        flux.actions.wizard.setInfoTooltipVisibility(@props.settings, isOpen)

    render: ({content, settings}, {isVisible}) ->
        onClose = @onClose

        `<Tooltip position="bottomLeft"
                  mod="light-gray"
                  closable={true}
                  closeButton={true}
                  onClose={this.onClose}
                  onTap={this.onTap}
                  openned={isVisible}
                  settings={settings}
                  content={<TooltipContent content={content} />}
        >
            <Icon i="info-small" className="tooltip-icon" />
        </Tooltip>`


module.exports = InfoTooltip
