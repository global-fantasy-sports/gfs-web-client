{cx} = require('../lib/utils')
PlaceholderImage = require('../lib/ui/PlaceholderImage')

placeholders = {
    'small': '/static/img/nfl-player-placeholder-small.png'
    'large': '/static/img/nfl-player-placeholder.png'
}

PlayerPhoto = ({player, size, className}) ->
    photoSize = if size > 65 then 'large' else 'small'

    `<div className={cx('player-photo', className)} style={{height: size}}>
        <PlaceholderImage
            className="player-photo__image"
            src={player.getPhoto(photoSize)}
            placeholder={placeholders[photoSize]}
            alt={player.getName()}
        />
    </div>`


module.exports = PlayerPhoto
