{Titlebar} = require "lib/ui/Titlebar"
{Btn} = require "lib/ui"

module.exports.SocialNetwork = React.create
    displayName: "SocialNetwork"

    render: ({}) ->
        `<section className="social-network">
            <Titlebar title="Social network" className="user-account-title" mod="h2"/>
            <div className="social-network__item">
                <Btn mod="blue-linear-facebook x-medium" icon="fb-white">
                    Connect your
                </Btn>
            </div>
            <div className="social-network__item">
                <Btn mod="red-linear-google x-medium" icon="google-white">
                    Disconnect your
                </Btn>
            </div>
            <div className="social-network__item">
                <Btn mod="grey-linear-twitter x-medium" icon="twitter">
                    Connect your
                </Btn>
            </div>
        </section>`
