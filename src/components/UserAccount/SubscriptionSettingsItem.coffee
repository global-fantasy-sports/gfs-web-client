{Titlebar} = require "lib/ui/Titlebar"
SubscriptionSettingItemTop = require "./SubscriptionSettingItemTop"

{Checkbox} = require "lib/forms"

module.exports = React.create
    displayName: "SubscriptionSettingsItem"

    mixins: [flux.Connect("subscription", (store, props) ->
        subscriptions: store.getSubscriptions(props.section)
    )]

    onChange: (option) ->
        (e) ->
            flux.actions.userAccount.changeSubscriptionOption(option, e.target.checked)

    render: ({section}, {visible, subscriptions}) ->
        self = this

        content = subscriptions?.map(({key, value, description}) ->
            `<Checkbox checked={value} onChange={self.onChange(key)} bigLabel={true}>
                {description}
            </Checkbox>`
        )

        return `<div className="subscription__item">
            <Titlebar title={section} mod="h4" />
            <div className="subscription__content">
                {content}
            </div>
        </div>`
