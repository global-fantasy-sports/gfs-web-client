{Btn} = require "lib/ui"

module.exports.DeleteAccount = React.create
    displayName: "DeleteAccount"

    render: ({}) ->
        `<div className="delete-account">
            <div className="delete-account__btn-wrap">
                <Btn mod="delete x-medium">Delete account</Btn>
            </div>
            <p className="delete-account__info stroke x-medium">Account will be deleted
                after a few days, and can be restored
                if you contact support within 3 months.
            </p>
        </div>`
