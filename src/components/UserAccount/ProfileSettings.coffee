{Titlebar} = require "../../lib/ui/Titlebar"
{Input} = require "../../lib/forms"
{SelectBox} = require "../../lib/ui/SelectBox"

countries = require("../../lib/constants/countries")[1..]
states = require("../../lib/constants/states")[1..]

module.exports = React.create
    displayName: "ProfileSettings"
    mixins: [flux.Connect("users", (store) ->
        user: store.getCurrentUser()
    ), flux.Connect("forms", (store) ->
        formUsername: store.getValue("name")
    )]

    onCountryChanged: (country) ->
        flux.actions.userAccount.changeCountry(country)

    onStateChanged: (state) ->
        flux.actions.userAccount.changeState(state)

    onNameSave: (value) ->
        flux.actions.userAccount.changeName(value)

    onAddressSave: (value) ->
        flux.actions.userAccount.changeAddress(value)

    onUpload: ->
        fileInput = @refs.upload.getDOMNode()

        fileInput.addEventListener "change", (e) ->
            file = @files[0]
            flux.actions.data.uploadAvatar(file)

        fileInput.click()


    render: ({}) ->
        name = @state.user.get("name")
        email = @state.user.get("email")
        address = @state.user.get("address")
        avatar = @state.user.get("avatar")
        country = @state.user.get("country")
        state = @state.user.get("state")

        `<section className="account-settings">
            <Titlebar title="Profile settings" className="user-account-title" mod="h2"/>
            <div className="account-settings__content">
                <div className="account-settings__upload">
                    <div className="account-settings__img">
                        <img width={50} height={50} src={avatar} />
                    </div>
                    <a href="javascript:void(0);" onTap={this.onUpload} className="stroke x2-medium blue">
                        Upload
                        <input
                            ref="upload"
                            accept="image/*"
                            type="file"
                            style={{display: "none"}} />
                    </a>
                    <span className="stroke white small">
                        <ul>
                            <li>50x50px recommended</li>
                            <li>Max 500kb</li>
                        </ul>
                    </span>
                </div>
                <div className="account-settings__info">
                    <div className="account-settings__input">
                        <Input placeholder="Name"
                            validationId="name"
                            defaultValue={name}
                            onSave={this.onNameSave} />
                        <Input placeholder="E-mail"
                            type="text"
                            value={email}
                            disabled="disabled" />
                        <Input placeholder="Address"
                            validationId="address"
                            defaultValue={address}
                            onSave={this.onAddressSave} />
                    </div>
                    <div className="account-settings__select">
                        <SelectBox options={countries}
                                   value={country}
                                   onChange={this.onCountryChanged} />

                        {country === "US" &&
                            <SelectBox options={states}
                                       value={state}
                                       onChange={this.onStateChanged} />
                        }
                    </div>
                </div>
            </div>
        </section>`
