{Btn} = require "lib/ui"
{Titlebar} = require "lib/ui/Titlebar.coffee"
ProfileSettings = require "./ProfileSettings"
AnonymousAccount = require "./AnonymousAccount"
AccountSecurity = require "./AccountSecurity"
SubscriptionSettings = require "./SubscriptionSettings"

exports.UserAccount = React.create
    displayName: "UserAccount"

    mixins: [flux.Connect("forms", (store) ->
        inProgress: store.isInProgress("save_user")
        isError: store.isError("save_user")
        isSuccess: store.isSuccess("save_user")
        error: store.getError("save_user")
    )]

    onSave: ->
        flux.actions.forms.startProgress("save_user")
        flux.actions.userAccount.save()

    render: ({}, {inProgress, isError, isSuccess, error}) ->
        `<section className="user-account-wrap">
            <div className="app__title-wrap">
                <Titlebar title="Account settings" className="title app__title" mod="h2"/>
            </div>
            <div className="user-account">
                <div className="user-account__left">
                    <ProfileSettings/>
                    <AnonymousAccount/>
                    <AccountSecurity/>
                </div>
                <div className="user-account__right">
                    <SubscriptionSettings/>
                </div>
            </div>
            <div className="user-account__save">
                <Btn mod="orange-linear x-medium" onTap={this.onSave}>Save</Btn>
                {inProgress &&
                    <h3>in progress</h3>
                }
                {isSuccess &&
                    <h3>Success</h3>
                }
                {isError &&
                    <h3>Error: {error}</h3>
                }
            </div>
        </section>`
