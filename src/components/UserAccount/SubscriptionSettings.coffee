{Titlebar} = require "lib/ui/Titlebar"
SubscriptionSettingsItem = require "./SubscriptionSettingsItem"

module.exports = React.create
    displayName: "SubscriptionSettings"

    render: ({}) ->
        `<section className="subscription">
            <div className="subscription__top-block">
                <Titlebar title="Subscription settings" className="user-account-title" mod="h2"/>
            </div>
            <SubscriptionSettingsItem section="Challenges"/>
            <SubscriptionSettingsItem section="Financial events" />
            <SubscriptionSettingsItem section="Game events" />
        </section>`
