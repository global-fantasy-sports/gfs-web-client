{Input, Checkbox} = require "lib/forms"


module.exports = React.create
    displayName: "AnonymousAccount"

    mixins: [flux.Connect("users", (store) ->
        user: store.getCurrentUser()
        activeUrl: store.getAnonymAvatarUrl()
        avatars: store.getAvatarUrls()
    ), flux.Connect("forms", (store) ->
        formNickname: store.getValue("nickname")
    )]

    onNicknameChanged: () ->
        flux.actions.userAccount.changeNickname(@state.formNickname)

    onAnonymStatusChanged: (e) ->
        flux.actions.userAccount.changeAnonymousStatus(e.target.checked)

    onAnonymAvatarChanged: (url) ->
        ->
            flux.actions.userAccount.changeAnonymAvatar(url)

    render: ({}, {activeUrl, error, user, avatars}) ->
        self = @
        nickname = user.get("nickname")
        isAnonym = user.get("anonymous")

        `<section className="anonymous">
            <div className="user-account-title">Anonymous account</div>
            <section className="anonymous__content">
                <p className="anonymous__info stroke x-medium">If you wish to remain anonymous you can
                    choose a username and an avatar instead.
                </p>
                <div className="anonymous__name">
                    <Checkbox
                        name="received"
                        bigLabel={true}
                        onChange={this.onAnonymStatusChanged}
                        checked={isAnonym}>Play anonymously</Checkbox>
                    <Input
                        placeholder="Nickname"
                        validationId="nickname"
                        onSave={this.onNicknameChanged}
                        defaultValue={nickname} />
                </div>
                <div className="anonymous__choose-img">
                    {avatars.map(function(url) {
                        var isActive = url == activeUrl;

                        return <div className={"anonymous__photo" + (isActive ? " active" : "")}
                                    onTap={self.onAnonymAvatarChanged(url)}>
                                    <img src={url}
                                        width={65}
                                        height={65} />
                                </div>
                    })}
                </div>
            </section>
        </section>`
