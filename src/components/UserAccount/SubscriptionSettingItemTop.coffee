{cx} = require 'lib/utils'


module.exports = React.create
    displayName: "SubscriptionSettingItemTop"

    render: ({arrow, title}) ->

        cls = cx(
            "subscription__item-arrow": true
            "right": this.props.arrow == false
            "down": this.props.arrow == true
        )

        `<div className="subscription__item-head" onTap={this.props.onItemClick}>
            <span className="subscription__item-name">{this.props.title}</span>
            <i className={cls}></i>
        </div>`
