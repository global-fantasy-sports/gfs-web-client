Popup = require('../../lib/ui/Popup')
GameEmbed = require('../../lib/ui/GameEmbed')
{Btn} = require 'lib/ui'
{PasswordResetPopup} = require '../auth'
{cx} = require 'lib/utils'
{Input} = require 'lib/forms'

module.exports = React.create
    displayName: "AccountSecurity"

    mixins:[flux.Connect("state", (store) ->
        showPasswordResetPopup: store.getOpenDialogCode() == "FORGOT_PASSWORD"
        error: store.getError("CHANGE_PASSWORD")
    ), flux.Connect("forms", (store) ->
        oldPassword: store.getValue("old_password")
        newPassword: store.getValue("password")
        confirmPassword: store.getValue("confirmPassword")
        validationComplete: store.isValidationComplete()
    )]

    componentWillUpdate: (_, state) ->
        if state.validationComplete
            flux.actions.userAccount.changePassword()

    onChangePasswordClick: ->
        flux.actions.forms.validatePassword()

    onForgotPassword: ->
        flux.actions.tap.forgotPassword()

    onPasswordReset: ->
        flux.actions.tap.closeDialog()

    render: ({}, {showPasswordResetPopup, error}) ->
        cls = cx('orange-linear x-medium')

        `<section className="security">
            <div className="user-account-title">Account security</div>
            <div className="security__controls security__controls--vertical">
                <div>Password must be at least 6 characters long and must contain at least 1 digit</div>
                <br />

                <Input
                    type="password"
                    validationId="old_password"
                    placeholder="Password"
                    label="Enter you password:"
                    mod="offset"/>

                <Input
                    type="password"
                    validationId="password"
                    placeholder="New password"
                    label="Enter new password:"
                    mod="offset"/>

                <Input
                    type="password"
                    validationId="confirmPassword"
                    placeholder="New password"
                    label="Confirm new password:"
                    mod="offset"/>

                <br />
                {error}

                <Btn mod={cls}
                    onTap={this.onChangePasswordClick}
                >
                    Change password
                </Btn>
                <div className="ui-input__pass-forgot" onTap={this.onForgotPassword}>
                    Forgot password?
                </div>
                {showPasswordResetPopup ?
                    <Popup>
                    <GameEmbed noClose={true}>
                        <PasswordResetPopup onDone={this.onPasswordReset}/>
                    </GameEmbed>
                    </Popup>
                : null
                }
            </div>
        </section>`
