{Flag} = require "lib/ui/Flag"
{Btn} = require "lib/ui"
{cx} = require "lib/utils"
CandidateStats = require './CandidateStats'
HintTrigger = require './guide/HintTrigger'


Candidate = React.create
    displayName: "Candidate"

    getDefaultProps: ->
        selectLimit: 3

    mixins: [flux.Connect("state", (store, props) ->
        expandedStatistics: store.isExpandedStatistics(props.participant.id)
    ), flux.Connect("wizard", (store) ->
        selectedIds: store.getSelectedParticipantIds()
    )]

    onTap: (event)->
        event.preventDefault()
        if @state.selectedIds.contains(@props.participant.id)
            flux.actions.tap.unselectParticipant(@props.participant.id)
        else if typeof(@props.checkCount) == 'function'
            if @props.checkCount()
                flux.actions.tap.selectParticipant(@props.participant.id)
        else if @state.selectedIds.count() < @props.selectLimit
            flux.actions.tap.selectParticipant(@props.participant.id)

    onStatisticTap: (e) ->
        e.stopPropagation()
        flux.actions.tap.participantStatistics(@props.participant.id)

    render: ({participant}, {expandedStatistics, selectedIds}) ->
        active = selectedIds.contains(participant.id)
        status = participant.status
        name = participant.getFullName()
        countryCode = participant.country
        handicapValue = participant.handicap
        rank = participant.rank
        lastScore = participant.lastScore

        handicap = flux.stores.state.getState().gameType == 'handicap-the-pros'

        cs = cx(
            "dark candidate game-table": true
            "active": active
            "open-statistic": expandedStatistics
            "inactive": status == "cut" or status == "withdrawn"
        )

        avatarClass = "f-30"
        nameClass = "f-35"

        `<div className={cs}>
            <div className="golfer" onTap={this.onTap}>
                <div className={"golfer__el " + avatarClass}>
                    <div className="avatar">
                        <Flag country={countryCode}/>
                    </div>
                </div>
                <div className={"golfer__el " + nameClass + " left"}>
                    {name}
                </div>
                {handicap ?
                    <div className="golfer__el f-12">
                        {handicapValue}
                    </div>
                :
                    <div className="golfer__el f-12">
                        {rank}
                    </div>
                }
                {handicap ?
                    <div className="golfer__el f-12">
                        {rank}
                    </div>
                :
                    <div className="golfer__el f-12">
                        {(lastScore > 0 ? '+' : '') + lastScore}
                    </div>
                }
                <div className="golfer__el f-12">
                    {expandedStatistics ?
                        <Btn onTap={this.onStatisticTap}
                            mod="white square regular small"
                            iconBg={true} icon="close-red">
                            Close
                        </Btn>
                    :
                        <Btn onTap={this.onStatisticTap}
                             mod="blue square regular small"
                             iconBg={true} icon="stats">
                             Statistics
                        </Btn>
                    }
                </div>
                <div className="golfer__el f-12">
                    <HintTrigger group="wizard" id="golfer">
                        <Btn mod="regular small select"
                            icon={active ? "ball-selected" : "ball-select-empty"}>
                              {active ? "Deselect" : "Select"}
                        </Btn>
                     </HintTrigger>
                </div>
            </div>
            {expandedStatistics ?
                <div className="frog">
                    <CandidateStats participant={participant} />
                </div>
            : null}
        </div>`

module.exports = Candidate
