{cx} = require 'lib/utils'
{Icon} = require 'lib/ui'
NotificationsList = require './NotificationsList'
NotificationTooltip = require('../tooltip/NotificationTooltip')

ReactCSSTransitionGroup = require('react-addons-css-transition-group')


{togglePanel} = flux.actions.notifications


module.exports = React.create
    displayName: 'Notifications'

    propTypes: {
        className: React.PropTypes.string
    }

    mixins: [
        flux.Connect('notifications', (store)-> store.getState())
    ]

    componentDidMount: ->
        document.addEventListener('click', @checkOutsideClick, false)

    componentWillUnmount: ->
        document.removeEventListener('click', @checkOutsideClick, false)

    checkOutsideClick: (event) ->
        return unless @state.open
        if not React.findDOMNode(this).contains(event.target)
            @setState(open: false)

    onToggle: () ->
        flux.actions.notifications.togglePanel()

    onClose: () ->
        flux.actions.notifications.closePanel()

    renderNotificationList: () ->
        {notifications, important, showOnlyImportant} = @state

        if important.count() && showOnlyImportant
            return `<NotificationsList
                notifications={important} />`
        else
            return `<NotificationsList
                notifications={notifications} />`

    render: ->
        {unreadCount, panelOpen, showOnlyImportant} = @state
        className = cx('clickable notifications-box notification', @props.className)
        onToggle = @onToggle
        onClose = @onClose

        content = @renderNotificationList()

        `<li className={className} onTap={onToggle}>
            <NotificationTooltip
                mod="white"
                unread={unreadCount}
                panelOpen={panelOpen}
                showOnlyImportant={showOnlyImportant}
                onClose={onClose}
                content={content}>
                <Icon i="bell" />
            </NotificationTooltip>

            {unreadCount > 0 &&
                <span className="item-counter">{unreadCount}</span>
            }
        </li>
        `
