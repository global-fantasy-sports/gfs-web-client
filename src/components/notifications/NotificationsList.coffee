Immutable = require('immutable')
{history} = require('../../world')
{NotificationItem, EmptyNotificationList} = require('./items')
ReactDOM = require('react-dom');


module.exports = React.create(
    displayName: 'NotificationsList'

    propTypes: {
        notifications: React.PropTypes.instanceOf(Immutable.Iterable).isRequired
    }

    componentDidMount: () ->
        ReactDOM.findDOMNode(this).addEventListener('click', @handleLink)

    componentWillUnmount: () ->
        ReactDOM.findDOMNode(this).removeEventListener('click', @handleLink)

    handleLink: (e) ->
        unless e.defaultPrevented
            if e.target.nodeName is 'A' and e.target.hash is ''
                if (SPORT and e.target.pathname.startsWith('/' + SPORT)) or
                        (SPORT is '' and
                                not e.target.pathname.startsWith('/nfl') and
                                not e.target.pathname.startsWith('/nba') and
                                not e.target.pathname.startsWith('/golf'))
                    e.preventDefault()
                    history.pushState(null, e.target.pathname)

    render: ({notifications}) ->
        els = for item in Array.from(notifications)
            `<NotificationItem title={item.title} date={item.date}
                               read={item.read} key={item.id}>
              {item.message}
            </NotificationItem>`


        if not els.length
            els = `<EmptyNotificationList />`

        `<div>
          <ul>{els}</ul>
        </div>`
)