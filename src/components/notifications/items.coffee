_ = require "underscore"

{Link, Money, Icon} = require '../../lib/ui'
{cx, dateTimeFmt} = require '../../lib/utils'
{gameNameByType} = require '../../lib/const'


exports.NotificationItem = React.create
    displayName: 'NotificationItem'

    propTypes: {
        title: React.PropTypes.string.isRequired
        children: React.PropTypes.string.isRequired
        date: React.PropTypes.any.isRequired
    }

    render: ({title, children, date, read}) ->
        cls = cx('notification-item', {'notification-item--unread': !read})

        `<li {...this.props} className={cls}>
            <h4 className="notification-item__title">
                {!read ? <i className="notification-item__red-icon"/> : null}
                {title}
            </h4>
            <p className="notification-item__text" dangerouslySetInnerHTML={{__html: children}}></p>
            <span className="notification-item__time">{dateTimeFmt(date)}</span>
        </li>`


exports.EmptyNotificationList = React.create
    displayName: 'EmptyNotificationList'
    render: ->
        `<div className="notification-item">
            No inbox messages.
        </div>`
