Candidate = require "components/Candidate"
_ = require 'underscore'


module.exports = React.create
    displayName: "ParticipantSelectionList"

    mixins: [flux.Connect("participants", (store, props) =>
        participants: store.getParticipants(props)
    ), flux.Connect('search', (store) ->
        sortOrders = store.getSortOrders()
        orderName = store.getOrderName()
        mainOrder = sortOrders.get(orderName)
        mapping = store.getMapping()
        accessor = mapping[orderName] || orderName

        {
            name: store.getName()
            country: store.getCountry()
            orderName
            sortOrders
            mainOrder
            accessor
        }
    )]

    render: ({}, {participants, name, country, mainOrder, orderName, accessor}) ->
        cut = participants
            .toIndexedSeq()
            .filter((p) ->
                status = p.get("status")
                status == "cut" or status == "withdrawn"
            )

        notCut = participants
            .toIndexedSeq()
            .filter((p) ->
                status = p.get("status")
                !(status == "cut" or status == "withdrawn")
            )

        list = notCut.concat(cut).filter((p) ->
            if name and not ~p.getFullName().toUpperCase().indexOf(name.toUpperCase()) then return false
            if country and p.country != country then return false
            true
        ).sort( (p1, p2) =>
            if _(p1[accessor]).isFunction()
                v1 = p1[accessor]() || 0
                v2 = p2[accessor]() || 0
            else
                v1 = p1[accessor] || 0
                v2 = p2[accessor] || 0

            if v1 == v2 then return 0

            if mainOrder == 'ASC'
                if v2 > v1 then return 1 else return -1
            else
                if v1 > v2 then return 1 else return -1
        ).toArray().map((p) =>
            `<Candidate key={p.get('id')} participant={p} />`
        )

        `<div className="candidate-list">
            {list}
         </div>`
