{Link} = require('../lib/ui')
Logo = require('../lib/ui/Logo')


Logos = {
    'golf': {
        link: '/golf/'
        logo: 'gfg-logo'
        title: 'Global Fantasy Sports Golf'
    }
    'nfl': {
        link: '/nfl/'
        logo: 'gfs-white'
        title: 'Global Fantasy Sports Football'
    }
    'nba': {
        link: '/nba/'
        logo: 'gfs-white'
        title: 'Global Fantasy Sports Basketball'
    }
}

DefaultLogo = {
    logo: 'gfs-white'
    title: 'Global Fantasy Sports'
}


AppLogo = ({sport}) ->
    {link, logo, title} = Logos[sport] ? DefaultLogo

    `<Link to={link || '/'}>
        <Logo logo={logo} alt={title} />
    </Link>`


module.exports = AppLogo
