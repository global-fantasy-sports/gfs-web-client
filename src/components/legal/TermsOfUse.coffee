{BtnLink, Link} = require('lib/ui')
{Titlebar} = require('lib/ui/Titlebar')
{cx} = require('lib/utils')

module.exports = React.create
    displayName: 'TermsOfUse'

    render: ->
        cls = cx("regular medium", "green")

        `<section className="rules-wrap">
            <article className="article">
                <div className="article__section">
                    <div className="article__head">
                        <div className="article__title">
                            <Titlebar title="Global Fantasy Sports Golf™ Website Terms and Conditions"/>
                        </div>
                        <div className="article__close">
                            <BtnLink to="__back" mod={cls}>Back</BtnLink>
                        </div>
                    </div>

                    <p>The Global Fantasy Sports Golf™ website located at golf.globalfantasysports.com (the &ldquo;Site&rdquo;),
                        is operated by Global Fantasy Sports Inc. (&ldquo;Global Fantasy Sports Golf™,&rdquo; &ldquo;we,&rdquo; &ldquo;our&rdquo; or &ldquo;us&rdquo;).
                        You agree to the following Global Fantasy Sports Golf™ Website Terms and Conditions
                        (&ldquo;Terms and Conditions&rdquo;), in their entirety, when you: (a) access or
                        use the Site; and/or (b) register as a user (&ldquo;User&rdquo;) of the Site,
                        which provides you with access to: (i) the Site’s viewable Content
                        (as defined below) and other material; (ii) interactive areas of
                        the Site (where and when available), such as blog and comment
                        sections, that enable Users to interact with other Users, as well
                        as Global Fantasy Sports Golf™ personnel (&ldquo;Interactive Services&rdquo;); and (iii)
                        an array of daily and weekly golf-themed fantasy sports-based games
                        (collectively, &ldquo;Contests&rdquo;) involving golfers associated with the
                        Professional Golfers Association of America (&ldquo;PGA®&rdquo;) or any other
                        professional golf organization designated by Global Fantasy Sports Golf™ (each, an &ldquo;Association&rdquo;).
                        The Site, Content, Interactive Services and Contests are referred
                        to, collectively herein, as the &ldquo;Global Fantasy Sports Golf™ Offerings.&rdquo;
                    </p>
                    <p>
                        These Terms and Conditions are inclusive of the Global Fantasy Sports Golf™
                        Privacy Policy, the rules applicable to all Contests (&ldquo;Contest Rules&rdquo;),
                        the Competition Room Rules (as defined below) and any and all other
                        applicable Global Fantasy Sports Golf™ operating rules, policies, price schedules
                        and other supplemental terms and conditions or documents that may be
                        published from time to time, which are expressly incorporated herein
                        by reference (collectively, the &ldquo;Agreement&rdquo;).  Please review the
                        Agreement carefully before accessing the Global Fantasy Sports Golf™ Offerings.
                        The Agreement constitutes the entire and only agreement between you
                        and Global Fantasy Sports Golf™ with respect to your use of the Global Fantasy Sports Golf™
                        Offerings and supersedes all prior or contemporaneous agreements,
                        representations, warranties and/or understandings with respect
                        to your use of the Global Fantasy Sports Golf™ Offerings and/or the products,
                        services and/or programs provided by and/or through same.  Unless
                        explicitly stated otherwise, any future offer(s) made available
                        to you on the Site that augment(s) or otherwise enhance(s) the
                        current features of the Global Fantasy Sports Golf™ Offerings shall be subject
                        to the Agreement.<b> If you do not agree to the terms and conditions
                        contained within the Agreement in its entirety, you are not
                        authorized to become a User and/or access or use the Global Fantasy Sports Golf™
                        Offerings in any manner or form whatsoever.</b>
                    </p>
                    <p>
                        The Professional Golfers Association of America and PGA® are
                        registered trademarks of the Professional Golfers Association of America.
                        Please be advised that Global Fantasy Sports Golf™ is not in any way affiliated with
                        the Professional Golfers Association of America or any other Association,
                        and neither Global Fantasy Sports Golf™ nor the Contests are endorsed or sponsored by
                        the Professional Golfers Association of America or any other Association.
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">1. Requirements.</h3>
                    <p>
                        The Contests are available only to individuals that: (a) are legal
                        residents of, and that physically reside in, the United States
                        (with restrictions applying to residents of the <b>States of Arizona, Hawaii,
                        Iowa, Louisiana, Montana, Nevada, North Dakota, Illinois, New York, Tennessee,
                        Texas, Vermont and Washington,
                        as well as Puerto Rico and where otherwise prohibited by law, collectively,
                        the &ldquo;Restricted Territories&rdquo;)</b>; (b) are at least: (i) eighteen (18) years
                        of age; or (ii) the age of majority in their respective jurisdictions
                        at the time of entry, if the age of majority is greater than eighteen
                        (18) years of age; (c) can enter into legally binding contracts under
                        applicable law; and (d) are not associated with a professional athlete
                        and/or Association (as defined below) that is/are involved in any Contest
                        in a way that creates, or could create, an unfair advantage in any
                        Contest including, without limitation, Association officials, referees,
                        owners and/or employees, as well as a professional athlete’s management,
                        agent(s), caddies and/or medical advisors.  Residents of the <b>States
                        of Arizona, Hawaii, Illinois, Iowa, Louisiana, Montana, Nevada, North Dakota, New York, Tennessee,
                        Texas, Vermont and Washington, as well as Puerto Rico, and where otherwise prohibited by law, are not
                        eligble to enter Global Fantasy Sports Inc pay to play</b>,
                        (as defined below and in the Contest Rules).
                    </p>
                    <p>
                        WITHOUT LIMITING THE FOREGOING, AND WITHOUT EXCEPTION, ALL PROSPECTIVE
                        USERS ARE SOLELY AND EXCLUSIVELY RESPONSIBLE FOR ENSURING THAT USE OF
                        A HOW FAR GOLF™ OFFERING IS NOT PROHIBITED BY ANY LAW APPLICABLE TO THAT
                        PROSPECTIVE USER’S RESIDENT JURISDICTION.
                    </p>
                    <p>
                        In seeking to register a User account on the Site (&ldquo;Account&rdquo;),
                        you are representing to us that you are of sufficient age and not
                        legally resident, or physically located in, a Restricted Territory
                        and you agree to provide us, upon request, with such documentation
                        as we may require from time-to-time to verify your age, identity
                        and residency status. You acknowledge and agree that failure to
                        provide any such documentation within a timely fashion may result
                        in the suspension or termination of your Account, as well as
                        forfeiture of any applicable Prizes (as defined below) awarded
                        to you, without notice.
                    </p>

                </div>

                <div className="article__section">
                    <h3 className="title">2. Modification.</h3>

                    <p>
                        To the extent permissible by applicable law, we may amend the Agreement
                        from time to time in our sole discretion, without specific notice
                        to you; provided, however, that: (a) any amendment or modification
                        to the arbitration provisions, prohibition on class action provisions
                        or any other provisions applicable to dispute resolution
                        (collectively, &ldquo;Dispute Resolution Provisions&rdquo;) shall not apply
                        to any disputes incurred prior to the applicable amendment or
                        modification; and (b) any amendment or modification to pricing
                        and/or billing provisions (&ldquo;Billing Provisions&rdquo;) shall not apply
                        to any charges incurred prior to the applicable amendment or
                        modification.  The latest Agreement will be posted on the Site,
                        and you should review the Agreement, in its entirety, prior to
                        using the Global Fantasy Sports Golf™ Offerings.  By your continued use of the
                        Global Fantasy Sports Golf™ Offerings, you hereby agree to comply with, and be
                        bound by, all of the terms and conditions contained within the
                        Agreement effective at that time (other than with respect to
                        disputes arising prior to the amendment or modification of
                        the Dispute Resolution Provisions, or charges incurred prior to
                        the amendment or modification of the Billing Provisions, which
                        shall be governed by the Dispute Resolution Provisions and/or
                        Billing Provisions then in effect at the time of the subject
                        dispute or incurred charges, as applicable).
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">3. Registration.</h3>

                    <p>
                        In order to obtain access to the Global Fantasy Sports Golf™ Offerings including,
                        without limitation, the Contests as a User, you must first submit
                        the applicable registration form to Global Fantasy Sports Golf™ for review and
                        approval (&ldquo;Form&rdquo;).  Global Fantasy Sports Golf™ reserves the right, in its sole
                        discretion, to deny access to the Global Fantasy Sports Golf™ Offerings to
                        anyone at any time and for any reason, whatsoever.
                        The registration data that you must supply in order to become
                        a registered User, and then an Entrant (as defined in the Contest Rules)
                        in a Contest, may include, without limitation, your: (a) full name;
                        (b) state and country of residence; (c) e-mail address (including
                        the e-mail address associated with your PayPal® account, where
                        applicable); (d) password; (e) billing/mailing address
                        (where entering a Paid Contest, as defined below); (f) credit
                        card information (where selected as your preferred payment method);
                        (g) Social Security Number (for potential Prize winners);
                        (h) picture proof of identification, which may include a driver’s
                        license, passport, voting card or similar government issued
                        identification (for potential Prize winners); and/or (i) any other
                        information requested on the applicable Form (&ldquo;Form Registration Data&rdquo;).
                        In addition, Users may register for an Account by and through their
                        Facebook® accounts, Google+® accounts or any other means designated
                        by Global Fantasy Sports Golf™.  Where you register via your Facebook® account,
                        Global Fantasy Sports Golf™ will have access to your: (i) Facebook® public profile;
                        (ii) the e-mail address associated  with your Facebook® account; and
                        (iii) any and all other information made available to Global Fantasy Sports Golf™
                        by and through your Facebook® account (&ldquo;Facebook® Registration Data&rdquo;).
                        Where you register via your Google+®  account, Global Fantasy Sports Golf™ will
                        have access to your: (A) basic Google+®   profile information; (B)
                        the e-mail address associated with your Google+®  account; and (C)
                        any and all other information made available to Global Fantasy Sports Golf™ by and
                        through your Google+®  account (&ldquo;Google+® Registration Data,&rdquo; and together
                        with the Form Registration Data and Facebook® Registration Data,
                        the &ldquo;Registration Data&rdquo;).
                    </p>
                    <p>
                        You agree to provide true, accurate, current and complete
                        Registration Data and to update your Registration Data as
                        necessary in order to maintain it in an up to date and accurate
                        fashion.  Global Fantasy Sports Golf™ will verify and approve all registrants
                        in accordance with its standard verification procedures.
                        If Global Fantasy Sports Golf™ approves your Form, Global Fantasy Sports Golf™ will set up
                        your specific Account.
                        <b>Only one (1) Account is permitted per person.</b>
                    </p>
                    <p>
                        During registration, you will be asked to provide a user name
                        and password to use as your Account log-in identification,
                        where applicable (&ldquo;Log-In&rdquo;).  If the Log-In that you request
                        is not available, you will be asked to supply another Log-In.
                        You can change the user name and/or password that you selected
                        during registration at any time through your Account settings.
                        You are responsible for maintaining the confidentiality of your
                        Account and Log-In, and for restricting access to your computer,
                        and you agree to accept responsibility for all activities that occur
                        through use of your Account and Log-In,
                        including any Entry Fees paid therethrough
                    </p>
                    <p>
                        Facebook® is a registered trademark of Facebook, Inc. (&ldquo;Facebook&rdquo;).
                        Google+® is a registered trademark of Google, Inc. (&ldquo;Google&rdquo;).  PayPal®
                        is a registered trademark of PayPal, Inc. (&ldquo;PayPal&rdquo;).  Please be advised
                        that Global Fantasy Sports Golf™ is not in any way affiliated with Facebook, Google
                        or PayPal, nor are the Global Fantasy Sports Golf™ Offerings or Contests endorsed,
                        administered or sponsored by Facebook, Google or PayPal.
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">4. Global Fantasy Sports Golf™ Offerings.</h3>

                    <p>
                        A User Account will enable you to utilize the Global Fantasy Sports Golf™ Offerings
                        and other features including, but not limited to: (a) viewable Content;
                        (b) the Contests; and (c) Interactive Services.
                    </p>
                    <p><b>4.1 Contests</b></p>
                    <p>
                        Capitalized terms used in this Section 4(i) but not defined in
                        these Terms and Conditions shall have the meaning set forth in the Contest Rules.
                    </p>
                    <p>
                        Competition Room Rules:  Periodically, Global Fantasy Sports Golf™ will make
                        Contests available to Users.  Each Contest shall be subject to the
                        Contest Rules, and may include certain specific terms, conditions
                        and restrictions, as predetermined and set forth in the rules
                        appearing on the &ldquo;Competition Room&rdquo; page associated with each
                        applicable Contest (&ldquo;Competition Room Rules,&rdquo; and together with
                        the Contest Rules, the &ldquo;Rules&rdquo;), including, without limitation:
                        (A) the minimum/maximum number of Entrants who may participate;
                        (B) the applicable Entry Fee, Prize amount, and number of winners;
                        and (C) the applicable Contest type (5-Ball Contest, Pick-18 Contest,
                        Hole-in-1 Contest, etc.).  In addition, Entrants may join Contests
                        with random opponents, or invite friends to compete one-on-one or in
                        larger groups.  Users can invite friends on their own, or utilize
                        Global Fantasy Sports Golf’s™ social media invitation functionality
                        that enables Users to invite friends via their Facebook® accounts,
                        Google+® accounts, Twitter® accounts or other designated social
                        media accounts and/or methods.  The Entrants shall be accepted in
                        Contests for which they are eligible on a first-come, first-served basis.
                    </p>
                    <p>
                        Twitter® is a registered trademark of Twitter, Inc. (&ldquo;Twitter&rdquo;).
                        Please be advised that Global Fantasy Sports Golf™ is not in any way affiliated
                        with Twitter, nor are the Global Fantasy Sports Golf™ Offerings or Contests endorsed,
                        administered or sponsored by Twitter.
                    </p>
                    <p>
                        <b>IT IS PROHIBITED FOR TWO (2) OR MORE ENTRANTS TO COLLABORATE
                            DURING ANY CONTEST OR TO ADOPT A STRATEGY (BEFORE OR DURING A
                            CONTEST) IN ORDER TO MUTUALLY GAIN AN ADVANTAGE AND/OR HARM OTHER
                            ENTRANTS (COLLECTIVELY, &ldquo;COLLUSION&rdquo;).
                        </b>
                    </p>
                    <p>
                        <b>
                            TO THE EXTENT PERMISSIBLE BY APPLICABLE LAW, HOW FAR GOLF™
                            IS NOT RESPONSIBLE FOR ATTEMPTED CONTEST REGISTRATIONS
                            THAT ARE LOST, LATE, ILLEGIBLE, MISDIRECTED, DAMAGED,
                            INCOMPLETE OR INCORRECT, OR FOR YOUR FAILURE TO PARTICIPATE
                            IN, OR COMPLETE, A SCHEDULED CONTEST.  IF YOU FAIL TO ENTER
                            A CONTEST INCLUDING, WITHOUT LIMITATION, WHERE YOU CANNOT
                            ACCESS THE SITE FOR ANY REASON WHATSOEVER, YOU WILL NOT BE
                            ABLE TO PARTICIPATE IN THAT CONTEST.
                        </b>
                    </p>
                    <p>
                        Entry Fees; Account Deposits:  Subject to Section 1 above,
                        the Contests can either be played: (I) for a fee, with a chance
                        to win cash and/or merchandise (&ldquo;Pay Contests&rdquo;); and (II) for
                        free and/or for Virtual Tokens, with a chance to win cash,
                        merchandise and/or more Virtual Tokens (&ldquo;Free Contests&rdquo;).  In
                        connection with Pay Contests, Users must pay the applicable
                        Entry Fee from their Account in advance of that Contest’s Start Time.
                        In connection with Free Contests that require Virtual Tokens
                        to play, Users must pay the applicable Virtual Token entry fee
                        from their Account in advance of that Contest’s Start Time.
                        The Entry Fee and/or Virtual Token entry fee associated with
                        each Contest will be predetermined and set forth in the applicable
                        Competition Room Rules.  Contests that do not attract the requisite
                        number of Entrants will be cancelled prior to commencement, and any
                        and all Entry Fees and Virtual Tokens fully refunded.
                    </p>
                    <p>
                        Where you opt to deposit funds into your Account, your PayPal® account,
                        or the credit or debit card that you provided during registration or
                        updated at a later date (&ldquo;Active Credit Card&rdquo;), will be charged the
                        applicable amount.  All Entry Fees are payable in United States currency.
                        Subject to the conditions set forth herein, you agree to be bound by
                        the Billing Provisions of Global Fantasy Sports Golf™ in effect at any given time.
                        Upon reasonable prior written notice to you (with e-mail sufficing),
                        Global Fantasy Sports Golf™ reserves the right to change its Billing Provisions
                        whenever necessary, in its sole discretion.  Continued use of the
                        Global Fantasy Sports Golf™ Offerings after receipt of such notice shall constitute
                        consent to any and all such changes; provided, however, that any
                        amendment or modification to the Billing Provisions shall not apply
                        to any charges incurred prior to the applicable amendment or modification.
                        All Entry Fee charges will appear on your Active Credit Card statement,
                        or PayPal® account statement, as applicable, as &ldquo;Global Fantasy Sports.&rdquo;
                    </p>
                    <p>
                        Global Fantasy Sports Golf’s™ authorization to provide and bill for the Global Fantasy Sports
                        Golf™ Offerings is obtained by way of your electronic signature or,
                        where applicable, via physical signature and/or voice affirmation.
                        Once an electronic signature is submitted, this electronic order
                        constitutes an electronic letter of agency.  Global Fantasy Sports Golf’s™ reliance
                        upon your electronic signature was specifically sanctioned and written
                        into law when the Uniform Electronic Transactions Act and the Electronic
                        Signatures in Global and National Transactions Act were
                        enacted in 1999 and 2000, respectively.
                        Both laws specifically preempt all state laws that recognize only paper
                        and handwritten signatures.
                    </p>
                    <p>
                        Prizes; Withdrawals; and Taxes:  The potential winner(s) of the
                        Pay Contests (subject to eligibility verification and/or other potential
                        disqualification, as set forth herein) will receive the applicable
                        monetary Prize.  The potential winners of the Free Contests may
                        receive Virtual Token Prizes or, in some select circumstances, monetary Prizes.
                        Monetary Prize amounts may be withdrawn by a User at any time, and
                        will be paid to that User’s Active Credit Card, PayPal® account or
                        by check sent directly by Global Fantasy Sports Golf™; provided, however, that
                        certain bonus Prizes, registration bonuses and other free money
                        giveaways, as offered by Global Fantasy Sports Golf™ from time-to-time, may be
                        subject to certain withdrawal restrictions.  Checks for withdrawal
                        requests are processed within fourteen (14) business days, and are
                        sent via U.S. Mail
                        <b>Please note that the Virtual Tokens have no monetary or other value
                        outside of entry into certain Free Contests, and the Virtual Tokens
                        cannot be exchanged for cash, products and/or merchandise.</b>
                    </p>
                    <p>
                        <b>
                            You are responsible for paying any sales, use or other taxes
                            related to any Prizes awarded to you in connection with the
                            Contests.  Without limiting the foregoing, any request for
                            withdrawal from your Account that brings your total earnings
                            (withdrawals less deposits) to an amount over Six Hundred
                            Dollars ($600.00) for any calendar year will require that
                            you provide us with a valid street address and Social Security
                            Number prior to processing.  This information will be used by
                            us to file a form 1099-MISC at the end of the year.  In addition,
                            Entrants may be requested to complete an affidavit of eligibility
                            and a liability/publicity release (unless prohibited by law) and
                            provide forms of identification including, but not limited to, a
                            driver's license, proof of residence and/or any information
                            relating to payment/deposit accounts as reasonably requested
                            by Global Fantasy Sports Golf™ in order to complete the payment process.
                            Failure to comply with the above requirements may result in
                            disqualification and forfeiture of any Prizes.
                        </b>
                    </p>
                    <p>
                        <b>
                            ALL DETERMINATIONS WITH RESPECT TO WINNERS AND PRIZES
                            WILL BE MADE IN HOW FAR GOLF’S™ SOLE AND ABSOLUTE DISCRETION,
                            AND WILL BE FINAL AND BINDING ON YOU.
                        </b>
                    </p>
                    <p>
                        <b>
                            HOW FAR GOLF™ RESERVES THE RIGHT TO DISQUALIFY AND/OR INVALIDATE
                            ANY PRIZES AWARDED WHERE THE APPLICABLE PRIZE WINNER FAILED TO
                            COMPLY WITH THE AGREEMENT, IN ANY MANNER.
                        </b>
                    </p>
                    <p>
                        Cancellation/Invalidation of Contests:  Without limiting any
                        provision set forth in the Rules, Global Fantasy Sports Golf™ reserves the right
                        to terminate and/or invalidate the results of any and all Contests
                        (including associated Prizes) where Global Fantasy Sports Golf™ determines, in
                        its sole and absolute discretion, that such cancellation/invalidation:
                        (x) is necessary or advisable due to any change in or to any law,
                        regulation or legal interpretation thereof affecting the Contests
                        in any way; and/or (y) for purposes of preventing abusive, fraudulent,
                        unfair or potentially unlawful activity, or in the event that
                        there is a risk of any such abusive, fraudulent, unfair or
                        potentially unlawful activity.
                    </p>
                    <p>
                        <b>
                            YOU UNDERSTAND AND AGREE THAT HOW FAR GOLF™ IS NOT RESPONSIBLE
                            OR LIABLE IN ANY MANNER WHATSOEVER FOR ANY CONTEST CANCELLATION
                            OR FOR YOUR INABILITY TO PARTICIPATE IN THE CONTESTS
                            FOR ANY OTHER REASON, WHATSOEVER.
                        </b>
                    </p>

                    <p><b>4.2 Content</b></p>
                    <p>
                        Users that possess the requisite technology shall have the
                        opportunity to view certain sports-related and other media
                        (downloaded, streaming or samples), leaderboards, statistics,
                        software, text, images, graphics, user interfaces, audio,
                        trademarks, logos, artwork, Feedback (as defined below) and other
                        content made available by and through the Site (collectively, &ldquo;Content&rdquo;)
                        as compiled, distributed and displayed by Global Fantasy Sports Golf™, as well
                        as Users and third party statistics providers and/or other entities
                        (collectively, &ldquo;Third Party Providers&rdquo;) that provide sports-related
                        news and information by and through the Site via updated news feeds.
                        The Content will be organized by theme and genre to enable Users
                        to locate areas of interest.  The Content should not necessarily
                        be relied upon.  The Third Party Providers are solely responsible
                        for the accuracy, completeness, appropriateness and/or usefulness
                        of such Content.  You may find certain Content to be harmful,
                        inaccurate and/or deceptive.  Please use caution, common sense
                        and safety when using the Content.
                    </p>
                    <p>
                        <b>
                            To the extent permissible by applicable law, you
                            understand and agree that Global Fantasy Sports Golf™ will not be responsible
                            for, and Global Fantasy Sports Golf™ undertakes no responsibility to monitor,
                            or otherwise police, the Content.  To the extent permissible by
                            applicable law, you agree that Global Fantasy Sports Golf™ shall have no
                            obligation and incur no liability to you in connection with any
                            Content.
                        </b>
                    </p>

                    <p><b>4.3 Interactive Services/Feedback.</b></p>
                    <p>
                        Subject to the restrictions set forth herein, the Interactive
                        Services (where and when available) will allow Users to participate
                        in comment sections and other interactive areas of the Site.
                        Each User agrees to use the Interactive Services in full compliance
                        with all applicable laws, rules and regulations.  Each User shall
                        be solely responsible for her/his comments, opinions, statements,
                        feedback and other content posted by and through the Interactive
                        Services (&ldquo;Feedback&rdquo;).  <b>You understand and agree that Global Fantasy Sports Golf™
                        shall not be liable to you, any other User or any third party for
                        any claim in connection with your use of, or inability to use, the
                        Interactive Services.  Global Fantasy Sports Golf™ does not monitor the Feedback
                        submitted by Users, and operates the Interactive Services as a
                        neutral host.  The Interactive Services contain Feedback that is
                        provided directly by Users.  You agree that Global Fantasy Sports Golf™ shall
                        have no obligation and incur no liability to you in connection with
                        any Feedback appearing in or through the Interactive Services.
                        Global Fantasy Sports Golf™ does not represent or warrant that the Feedback
                        posted through the Interactive Services is accurate, complete
                        or appropriate.</b> Global Fantasy Sports Golf™ reserves the right to remove any
                        Feedback from the Site at any time and for any reason,
                        in Global Fantasy Sports Golf’s™ sole discretion.
                    </p>
                    <p>
                        In connection with your Feedback, you agree not to: (A) display
                        any telephone numbers, street addresses, last names, URLs, e-mail
                        addresses or any confidential information of any third party; (B)
                        display any audio files, text, photographs, videos or other images
                        containing confidential information; (C) display any audio files,
                        text, photographs, videos or other images that may be deemed indecent
                        or obscene, as defined under applicable law; (D) impersonate any person
                        or entity; (E) &ldquo;stalk&rdquo; or otherwise harass any person; (F) engage in
                        advertising to, or commercial solicitation of, Users or other third
                        parties; (G) transmit any chain letters, spam or junk e-mail to any
                        Users or other third parties; (H) express or imply that any statements
                        that you make are endorsed by Global Fantasy Sports Golf™; (I) harvest or collect
                        personal information of Users or other third parties whether or not
                        for commercial purposes, without their express consent; (J) use any
                        robot, spider, site search/retrieval application, or other manual
                        or automatic device or process to retrieve, index, &ldquo;data mine,&rdquo; or
                        in any way reproduce or circumvent the navigational structure or
                        presentation of the Global Fantasy Sports Golf™ Offerings or related content; (K) post,
                        distribute or reproduce in any way any copyrighted material, trademarks
                        or other proprietary information without obtaining the prior consent
                        of the owner of such proprietary rights; (L) remove any copyright,
                        trademark or other proprietary rights notices contained within the
                        Global Fantasy Sports Golf™ Offerings; (M) interfere with or disrupt any of the
                        Global Fantasy Sports Golf™ Offerings and/or the servers or networks connected
                        to same; (N) post, offer for download, e-mail or otherwise transmit
                        any material that contains software viruses or any other computer
                        code, files or programs designed to interrupt, destroy or limit
                        the functionality of any computer software or hardware or
                        telecommunications equipment; (O) post, offer for download,
                        transmit, promote or otherwise make available any software,
                        product or service that is illegal or that violates the rights
                        of a third party including, but not limited to, spyware,
                        adware, programs designed to send unsolicited advertisements
                        (i.e. &ldquo;spamware&rdquo;), services that send unsolicited advertisements,
                        programs designed to initiate &ldquo;denial of service&rdquo; attacks,
                        mail bomb programs and programs designed to gain unauthorized access
                        to networks on the Internet; (P) &ldquo;frame&rdquo; or &ldquo;mirror&rdquo; any part
                        of the Site; (Q) use metatags or code or other devices containing
                        any reference to the Global Fantasy Sports Golf™ Offerings in order to direct
                        any person to any other website for any purpose; and/or (R) modify,
                        adapt, sublicense, translate, sell, reverse engineer, decipher,
                        decompile or otherwise disassemble any portion of the Global Fantasy Sports Golf™
                        Offerings or any software used on or in connection with same.
                        Engaging in any of the aforementioned prohibited practices shall
                        be deemed a breach of the Agreement and may result in the immediate
                        termination of, or access to, some or all of the Global Fantasy Sports Golf™
                        Offerings without notice, in the sole discretion of Global Fantasy Sports Golf™.
                        <b>Global Fantasy Sports Golf™ reserves the right to pursue any and all legal remedies
                        against Users that engage in the aforementioned prohibited conduct.</b>
                    </p>
                </div>


                <div className="article__section">
                    <h3 className="title">
                        5. Cancellation of Account.
                    </h3>

                    <p>
                        You may cancel your Account at any time if you are not completely
                        satisfied.  To cancel your Account, simply e-mail us at:
                        &nbsp;<a href="mailto:support@globalfantasysports.com"
                            target="_blank"> support@globalfantasysports.com</a>&nbsp;,
                        or cease using the Global Fantasy Sports Golf™ Offerings.
                        In addition, Global Fantasy Sports Golf™ reserves the right to cancel the Account
                        of any User for any reason or no reason, including where Global Fantasy Sports Golf™
                        believes that such User is in breach of the Agreement,
                        in Global Fantasy Sports Golf’s™ sole and absolute discretion.  You understand
                        and agree that cancellation of your Account is your sole right
                        and remedy with respect to any dispute with Global Fantasy Sports Golf™.  Upon
                        any termination and/or cancellation of your Account, your license
                        grant, as set forth in Section 6 hereinbelow shall immediately
                        terminate.  You shall not receive any refund for Entry Fees previously
                        paid for, up to the date of cancellation or termination.
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">6. License Grant.</h3>

                    <p>
                        As a User of the Site, you are granted a non-exclusive,
                        non-transferable, revocable and limited license to access and use
                        the Global Fantasy Sports Golf™ Offerings and all other material, products and/or
                        services posted or made available by and through same
                        (collectively, &ldquo;Global Fantasy Sports Golf™ Material&rdquo;) in accordance with the
                        Agreement.  Global Fantasy Sports Golf™ may terminate this license at any
                        time for any reason.  Unless otherwise expressly authorized by
                        Global Fantasy Sports Golf™, you may only use the Global Fantasy Sports Golf™ Material
                        for your own personal, non-commercial use.  No part of the How
                        Far Golf™ Material may be reproduced in any form or incorporated
                        into any information retrieval system, electronic or mechanical.
                        You may not use, copy, emulate, clone, rent, lease, sell, modify,
                        decompile, disassemble, reverse engineer or transfer the Global Fantasy Sports
                        Golf™ Material and/or any portion thereof.  You may not create any
                        &ldquo;derivative works&rdquo; by altering any aspect of the Global Fantasy Sports Golf™ Material.
                        You may not use Global Fantasy Sports Golf™ Material in conjunction with any other
                        third-party content (e.g., to provide sound for a film).
                        You may not exploit any aspect of the Global Fantasy Sports Golf™ Material
                        for any commercial purposes not expressly permitted by
                        Global Fantasy Sports Golf™ (including the bundled sale of such
                        Global Fantasy Sports Golf™ Material).  Systematic retrieval of the Global Fantasy Sports Golf™
                        Material by automated means or any other form of scraping or
                        data extraction in order to create or compile, directly or indirectly,
                        a collection, compilation, database or directory without written
                        permission from Global Fantasy Sports Golf™ is strictly prohibited.  You further
                        agree to indemnify and hold harmless Global Fantasy Sports Golf™ for your failure
                        to comply with this Section 6.  Global Fantasy Sports Golf™ reserves any rights
                        not explicitly granted in the Agreement.
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">7. Proprietary Rights.</h3>

                    <p>
                        All Global Fantasy Sports Golf™ Material is owned or licensed by or to Global Fantasy Sports Golf™,
                        and is protected under applicable copyrights, trademarks and other
                        proprietary (including, but not limited to, intellectual property)
                        rights.  Except as expressly provided in the Agreement, no part of
                        Global Fantasy Sports Golf™ Material may be reproduced, recorded, retransmitted,
                        sold, rented, broadcast, distributed, published, uploaded, posted,
                        publicly displayed, altered to make new works, performed, digitized,
                        compiled, translated or transmitted in any way to any other computer,
                        website or other medium or for any commercial purpose, without
                        Global Fantasy Sports Golf’s™ prior express written consent.  Except as expressly
                        provided herein, you are not granted any rights or license to
                        patents, copyrights, trade secrets, rights of publicity or trademarks
                        with respect to any of the Global Fantasy Sports Golf™ Material.  The posting
                        of information or material at the Site by Global Fantasy Sports Golf™ does not
                        constitute a waiver of any right in or to such information and/or
                        materials.  Global Fantasy Sports Golf™ reserves all rights not expressly
                        granted hereunder.  The &ldquo;Global Fantasy Sports Golf&rdquo; name and logo are trademarks
                        of Global Fantasy Sports Golf, Inc.  All custom graphics, icons and service
                        names are trademarks of their rightful owners.  All other trademarks
                        are the property of their respective owners.  The use of any
                        Global Fantasy Sports Golf™ trademark without Global Fantasy Sports Golf’s™ express written
                        consent is strictly prohibited.
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">8. Indemnification.</h3>

                    <p>
                        To the fullest extent permissible by applicable law, you agree
                        to indemnify and hold Global Fantasy Sports Golf™, its parents and subsidiaries,
                        and each of their respective members, officers, directors, employees,
                        agents, co-branders, content licensors and/or other partners,
                        harmless from and against any and all claims, expenses (including
                        reasonable attorneys’ fees), damages, suits, costs, demands
                        and/or judgments whatsoever, made by any third party due to or
                        arising out of: (a) your use of the Global Fantasy Sports Golf™ Offerings in
                        any way, whatsoever; (b) your breach of the Agreement; (c) any
                        dispute between you and any other Users, Third Party Providers
                        and/or other third parties; (d) any claim that Global Fantasy Sports Golf™ owes
                        any taxes in connection with your use of the Global Fantasy Sports Golf™
                        Offerings; and/or (e) your violation of any rights of another
                        individual and/or entity.  The provisions of this Section 8 are
                        for the benefit of Global Fantasy Sports Golf™, its parents, subsidiaries
                        and/or affiliates, and each of their respective officers,
                        directors, members, employees, agents, shareholders, licensors,
                        suppliers and/or attorneys.  To the fullest extent permissible
                        by applicable law, each of these individuals and entities shall
                        have the right to assert and enforce these provisions directly
                        against you on its own behalf.
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">9. Disclaimer of Warranties.</h3>

                    <p>
                        THE HOW FAR GOLF™ OFFERINGS, HOW FAR GOLF™ MATERIAL AND/OR ANY
                        OTHER PRODUCTS AND/OR SERVICES OFFERED IN CONNECTION WITH SAME,
                        ARE PROVIDED TO YOU ON AN &ldquo;AS IS&rdquo; AND &ldquo;AS AVAILABLE&rdquo; BASIS AND
                        ALL WARRANTIES, EXPRESS AND IMPLIED, ARE DISCLAIMED TO THE FULLEST
                        EXTENT PERMISSIBLE PURSUANT TO APPLICABLE LAW (INCLUDING, BUT NOT
                        LIMITED TO, THE DISCLAIMER OF ANY WARRANTIES OF MERCHANTABILITY,
                        NON-INFRINGEMENT OF INTELLECTUAL PROPERTY AND/OR FITNESS FOR A
                        PARTICULAR PURPOSE).  IN PARTICULAR, BUT NOT AS A LIMITATION
                        THEREOF, HOW FAR GOLF™ MAKES NO WARRANTY THAT THE HOW FAR GOLF™
                        OFFERINGS, HOW FAR GOLF™ MATERIAL AND/OR ANY OTHER PRODUCTS AND/OR
                        SERVICES OFFERED AND/OR ACCESSED IN CONNECTION WITH SAME: (A)
                        WILL MEET YOUR REQUIREMENTS; (B) WILL BE UNINTERRUPTED, TIMELY,
                        SECURE, ERROR-FREE OR THAT DEFECTS WILL BE CORRECTED; (C) WILL
                        BE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS; (D) WILL FEATURE
                        STATISTICS, CONTENT AND/OR LEADERBOARDS THAT ARE CURRENT, ACCURATE
                        OR RELIABLE; (E) WILL HAVE SECURITY METHODS EMPLOYED THAT WILL BE
                        SUFFICIENT AGAINST INTERFERENCE WITH YOUR ENJOYMENT OF SAME, OR
                        AGAINST INFRINGEMENT; (F) WILL RESULT IN ANY ECONOMIC BENEFIT OR
                        GAIN; AND/OR (G) WILL BE ACCURATE OR RELIABLE.  THE HOW FAR GOLF™
                        OFFERINGS, HOW FAR GOLF™ MATERIAL AND/OR ANY OTHER PRODUCTS AND/OR
                        SERVICES OFFERED IN CONNECTION WITH SAME MAY CONTAIN BUGS, ERRORS,
                        PROBLEMS OR OTHER LIMITATIONS.  HOW FAR GOLF™ WILL NOT BE LIABLE
                        FOR THE AVAILABILITY OF THE UNDERLYING INTERNET CONNECTION ASSOCIATED
                        WITH THE SITE.  NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN,
                        OBTAINED BY YOU FROM HOW FAR GOLF™, ANY USERS, THIRD PARTY CONTENT
                        PROVIDERS OR OTHERWISE SHALL CREATE ANY WARRANTY NOT EXPRESSLY
                        STATED IN THE AGREEMENT.
                    </p>
                </div>

                <div className="article__section">
                    <Titlebar title="Game Rules"/>
                    <h3 className="title">10. Limitation of Liability</h3>

                    <p>
                        YOU EXPRESSLY UNDERSTAND AND AGREE THAT HOW FAR GOLF™ SHALL
                        NOT BE LIABLE TO YOU OR ANY THIRD PARTY FOR ANY DIRECT, INDIRECT,
                        INCIDENTAL, SPECIAL, CONSEQUENTIAL AND/OR EXEMPLARY DAMAGES
                        INCLUDING, BUT NOT LIMITED TO, DAMAGES FOR LOSS OF PROFITS,
                        GOODWILL, USE, DATA OR OTHER INTANGIBLE LOSSES (EVEN IF HOW FAR GOLF™
                        HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES), TO THE
                        FULLEST EXTENT PERMISSIBLE BY LAW FOR: (A) THE USE OR THE INABILITY
                        TO USE THE HOW FAR GOLF™ OFFERINGS, HOW FAR GOLF™ MATERIAL AND/OR
                        ANY OTHER PRODUCTS AND/OR SERVICES OFFERED IN CONNECTION WITH SAME;
                        (B) THE COST OF PROCUREMENT OF SUBSTITUTE GOODS AND/OR SERVICES
                        RESULTING FROM ANY GOODS, DATA, INFORMATION, CONTENT AND/OR ANY
                        OTHER PRODUCTS PURCHASED OR OBTAINED FROM OR THROUGH THE SITE; (C)
                        THE UNAUTHORIZED ACCESS TO, OR ALTERATION OF, YOUR REGISTRATION
                        DATA; (D) ANY ISSUE RELATED TO THE TIMELINESS, ACCURACY OR RELIABILITY
                        OF STATISTICS, CONTENT AND/OR LEADERBOARDS THAT ARE MADE AVAILABLE
                        IN CONNECTION WITH THE HOW FAR GOLF™ OFFERINGS; (E) YOUR FAILURE TO
                        REALIZE ANY ECONOMIC BENEFIT OR GAIN; (F) THE CANCELLATION OF ANY
                        CONTEST(S) AND/OR PRIZES, AS PERMITTED HEREUNDER OR UNDER THE RULES;
                        AND (G) ANY OTHER MATTER RELATING TO THE HOW FAR GOLF™ OFFERINGS,
                        HOW FAR GOLF™ MATERIAL AND/OR ANY OTHER PRODUCTS AND/OR SERVICES
                        OFFERED AND/OR ACCESSED IN CONNECTION WITH SAME.  TO THE FULLEST
                        EXTENT PERMISSIBLE BY LAW, THIS LIMITATION APPLIES TO ALL CAUSES
                        OF ACTION, IN THE AGGREGATE INCLUDING, BUT NOT LIMITED TO, BREACH
                        OF CONTRACT, BREACH OF WARRANTY, NEGLIGENCE, STRICT LIABILITY,
                        MISREPRESENTATIONS AND ANY AND ALL OTHER TORTS.  TO THE FULLEST
                        EXTENT PERMISSIBLE BY LAW, YOU HEREBY RELEASE HOW FAR GOLF™ FROM
                        ANY AND ALL OBLIGATIONS, LIABILITIES AND CLAIMS IN EXCESS OF THE
                        LIMITATIONS STATED HEREIN.  THE NEGATION OF DAMAGES SET FORTH
                        ABOVE IS A FUNDAMENTAL ELEMENT OF THE BASIS OF THE BARGAIN BETWEEN
                        YOU AND HOW FAR GOLF™.  ACCESS TO THE HOW FAR GOLF™ OFFERINGS,
                        HOW FAR GOLF™ MATERIAL AND/OR ANY OTHER PRODUCTS AND/OR SERVICES
                        OFFERED IN CONNECTION WITH SAME WOULD NOT BE PROVIDED TO YOU
                        WITHOUT SUCH LIMITATIONS.  SOME JURISDICTIONS DO NOT ALLOW CERTAIN
                        LIMITATIONS ON LIABILITY AND, IN SUCH JURISDICTIONS, HOW FAR GOLF’S™
                        LIABILITY SHALL BE LIMITED TO THE MAXIMUM EXTENT PERMITTED BY LAW.
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">11. Copyright Policy/DMCA Compliance.</h3>

                    <p>
                        Global Fantasy Sports Golf™ reserves the right to terminate the Account of any
                        User who infringes upon third-party copyrights.  If you believe
                        that a copyrighted work has been copied and posted via the Site
                        in a way that constitutes copyright infringement, you should
                        provide Global Fantasy Sports Golf™ with the following information: (a) an
                        electronic or physical signature of the person authorized to
                        act on behalf of the owner of the copyrighted work; (b) an
                        identification and location within the Site of the copyrighted
                        work that you claim has been infringed; (c) a written statement
                        by you that you have a good faith belief that the disputed use
                        is not authorized by the owner, its agent or the law; (d) your
                        name and contact information, such as telephone number or e-mail
                        address; and (e) a statement by you that the above information
                        in your notice is accurate and, under penalty of perjury, that
                        you are the copyright owner or authorized to act on the copyright
                        owner’s behalf. Contact information for Global Fantasy Sports Golf’s™ Copyright
                        Agent for notice of claims of copyright infringement is as follows:
                    </p>
                    <p>
                        Klein Moynihan Turco LLP <br/>
                        Attn: Jonathan Turco, Esq. <br/>
                        450 Seventh Avenue, 40th Floor <br/>
                        New York, NY 10123 <br/>
                        jturco@kleinmoynihan.com <br/>
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">12. Legal Warning.</h3>

                    <p>
                        If you bypass or disable any portion of the Global Fantasy Sports Golf™ Offerings
                        or associated software including, without limitation, the operation
                        of Global Fantasy Sports Golf™ systems, or you attempt to circumvent or tamper
                        with the Contests or billing methods in any way, you are in
                        violation of the Agreement and Global Fantasy Sports Golf™ may suspend or
                        terminate your Account without notice.  Termination of your
                        Account will not excuse you from any criminal or other civil
                        liabilities that may result from your actions.
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">13. Accessing the Global Fantasy Sports Golf™ Offerings</h3>

                    <p>
                        You are responsible for obtaining and maintaining, at your own cost
                        and expense, all input/output devices or equipment (such as modems,
                        terminal equipment, computer equipment and software) and communications
                        services (including, without limitation, long distance or local telephone
                        services) necessary to access the Global Fantasy Sports Golf™ Offerings and for ensuring
                        that such equipment and services are compatible with Global Fantasy Sports Golf’s™
                        requirements.
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">14. Third-Party Websites.</h3>

                    <p>
                        The Global Fantasy Sports Golf™ Offerings may provide links to other
                        Internet websites and/or resources including, without
                        limitation, websites owned and/or operated by Third Party
                        Providers.  Because Global Fantasy Sports Golf™ has no control over such third
                        party websites and/or resources, you hereby acknowledge and agree
                        that Global Fantasy Sports Golf™ is not responsible for the availability or
                        content of such third party websites and/or associated resources.
                        Furthermore, Global Fantasy Sports Golf™ does not endorse, and is not responsible or
                        liable for, any terms and conditions, privacy policies, content,
                        advertising, services, products and/or other materials available
                        at or from such third party websites or resources, or for any
                        damages and/or losses arising therefrom or associated therewith.
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">15. User Information.</h3>

                    <p>
                        All Feedback, comments, information, Registration Data and/or materials
                        that you submit through or in association with the Global Fantasy Sports Golf™
                        Offerings shall be subject to the Global Fantasy Sports Golf™ Privacy Policy.
                        For a copy of the Global Fantasy Sports Golf™ Privacy Policy, please
                        &nbsp;<Link to="/policy/"><a target="_blank">Click here</a></Link>&nbsp;.
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">16. Dispute Resolution Provisions.</h3>

                    <p>
                        The Agreement shall be treated as though it were executed and performed
                        in New York, NY and shall be governed by and construed in accordance
                        with the laws of the State of New York (without regard to conflict
                        of law principles). <b>Should a dispute arise concerning the Global Fantasy Sports Golf™ Offerings,
                        the terms and conditions of the Agreement or the breach of
                        same by any party hereto: (a) the parties agree to submit
                        their dispute for resolution before a reputable arbitration
                        organization as mutually agreed upon by the parties; and (b) you
                        agree to first commence a formal dispute proceeding by completing
                        and submitting an
                        Initial Dispute Notice which can be found
                        &nbsp;<a href="Global Fantasy Sports Golf Initial Dispute Notice.doc" target="_blank">Here</a>&nbsp;.
                        We may choose to provide you with a final written settlement offer
                        after receiving your Initial Dispute Notice (&ldquo;Final Settlement Offer&rdquo;).
                        If we provide you with a Final Settlement Offer and you do not accept
                        it, or we cannot otherwise satisfactorily resolve your dispute,
                        you can submit your dispute for resolution by arbitration before
                        a reputable arbitration organization as mutually agreed upon by
                        the parties, in your county of residence, by filing a separate
                        Demand for Arbitration, which is available
                        &nbsp;<a href="Global Fantasy Sports Golf Demand for Arbitration.doc" target="_blank">Here</a>&nbsp;.
                        Any award rendered shall be final and conclusive to the parties
                        and a judgment thereon may be entered in any court of competent
                        jurisdiction.  Nothing contained herein shall be construed to
                        preclude any party from: (i) seeking injunctive relief in order
                        to protect its rights pending an outcome in arbitration; and/or
                        (ii) pursuing the matter in small claims court rather than arbitration.
                        Although we may have a right to an award of attorneys’ fees and
                        expenses if we prevail in arbitration, we will not seek such an
                        award from you unless the arbitrator determines that your claim
                        was frivolous.</b>
                    </p>
                    <p>
                        <b>
                            To the extent permitted by law, you agree that you will
                            not bring, join or participate in any class action lawsuit
                            as to any claim, dispute or controversy that you may have
                            against Global Fantasy Sports Golf™ and/or its employees, officers, directors,
                            representatives and/or assigns.  You agree to the entry of
                            injunctive relief to stop such a lawsuit or to remove you as
                            a participant in the suit.  You agree to pay the attorney’s
                            fees and court costs that Global Fantasy Sports Golf™ incurs in seeking
                            such relief.  This provision preventing you from bringing,
                            joining or participating in class action lawsuits: (A) does
                            not constitute a waiver of any of your rights or remedies
                            to pursue a claim individually in binding arbitration as
                            provided above; and (B) is an independent agreement.
                            You may opt-out of these dispute resolution provisions
                            by providing written notice of your decision within thirty
                            (30) days of the date that you first access the Site.
                        </b>
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">17. Miscellaneous.</h3>

                    <p>
                        Should any part of the Agreement be held invalid or unenforceable,
                        that portion shall be construed consistent with applicable law and
                        the remaining portions shall remain in full force and effect.
                        The Agreement is personal between you and Global Fantasy Sports Golf™ and it
                        governs your use of the Global Fantasy Sports Golf™ Offerings and Global Fantasy Sports Golf™
                        Material.  To the extent that anything in or associated with the
                        Global Fantasy Sports Golf™ Offerings is in conflict or inconsistent with these
                        Terms and Conditions, these Terms and Conditions shall take precedence;
                        provided, however, that with respect to any Contests, to the extent
                        that anything in these Terms and Conditions is inconsistent with the
                        applicable Rules, those Rules shall take precedence.  Global Fantasy Sports Golf’s™
                        failure to enforce any provision of the Agreement shall not be deemed
                        a waiver of such provision nor of the right to enforce such provision.
                        The parties do not intend that any agency or partnership relationship be
                        created through operation of the Agreement.
                    </p>
                </div>


                <div className="article__section">
                    <h3 className="title">18. Contact Us</h3>
                    <p>
                        If you have any questions about the Agreement or the practices
                        of Global Fantasy Sports Golf™, please feel free to e-mail us at:
                        &nbsp;<a href="mailto:support@globalfantasysports.com"
                        target="_blank">support@globalfantasysports.com</a>.
                    </p>
                </div>

                <div className="article__actions">
                    <BtnLink to="__back" mod={cls}>
                         Back
                    </BtnLink>
                </div>
            </article>
        </section>`
