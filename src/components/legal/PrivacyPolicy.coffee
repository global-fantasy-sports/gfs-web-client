{BtnLink} = require('lib/ui')
{Titlebar} = require('lib/ui/Titlebar')
{cx} = require('lib/utils')


module.exports = React.create
    displayName: 'PrivacyPolicy'

    render: ->
        cls = cx("regular medium", "green")

        `<section className="underlay-wrap">
            <article className="article">
                <div className="article__section">
                    <div className="article__head">
                        <div className="article__title">
                            <Titlebar title="Global Fantasy Sports Golf™ Website Privacy Policy"/>
                        </div>
                        <div className="article__close">
                            <BtnLink to="__back" mod={cls}>Back</BtnLink>
                        </div>
                    </div>
                    <p>
                        <b>Last updated: August 11, 2015.</b>
                    </p>
                    <p>
                        The Global Fantasy Sports Golf™ website located at <a href="http://golf.globalfantasysports.com" target="_blank"> golf.globalfantasysports.com
                    </a> (the &ldquo;Site&rdquo;),
                        is operated by Global Fantasy Sports Inc.
                        (&ldquo;Global Fantasy Sports Golf™,&rdquo; &ldquo;we,&rdquo; &ldquo;our&rdquo; or &ldquo;us&rdquo;).
                        This Global Fantasy Sports Golf™ Website Privacy Policy
                        (&ldquo;Privacy Policy&rdquo;) covers our treatment of
                        personal information and other information that we collect when an end-user:
                        (a) accesses or use the Site; and/or (b) registers as a user (&ldquo;User&rdquo;)
                        of the Site, which provides you with access to: (i) the Site’s viewable
                        Content and other material; (ii) interactive areas of the Site
                        (where and when available), such as blog and comment sections, that
                        enable Users to interact with other Users, as well as Global Fantasy Sports Golf™
                        personnel (&ldquo;Interactive Services&rdquo;); and (iii) an array of daily and
                        weekly golf-themed fantasy sports-based games (collectively, &ldquo;Contests&rdquo;)
                        involving golfers associated with the Professional Golfers Association of
                        America (&ldquo;PGA®&rdquo;) or any other professional golf organization designated
                        by Global Fantasy Sports Golf™ (each, an &ldquo;Association&rdquo;).  The Site, Content, Interactive
                        Services and Contests are referred to, collectively herein, as
                        the &ldquo;Global Fantasy Sports Golf™ Offerings.&rdquo;
                    </p>
                    <p>
                        Capitalized terms not defined herein shall have the meanings set forth
                        in the Global Fantasy Sports Golf™ Website Terms and Conditions (&ldquo;Terms and Conditions&rdquo;).
                        <b> IF YOU DO NOT AGREE TO TERMS OF THIS PRIVACY POLICY IN THEIR ENTIRETY,
                            YOU MAY NOT ACCESS OR OTHERWISE USE THE HOW FAR GOLF™ OFFERINGS </b>
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">Your California Privacy Rights</h3>
                    <p>
                        If you are a resident of the State of California and would like to
                        learn how your &ldquo;personal information&rdquo; (as defined in the Shine the
                        Light Law, Cal. Civ. Code § 1798.83) is shared with third parties,
                        what categories of personal information that we have shared with third
                        parties in the preceding year, as well as the names and addresses of
                        those third parties, please e-mail us at: <a href="mailto:support@globalfantasysports.com"
                                                                     target="_blank"> support@globalfantasysports.com</a>
                    </p>
                    <p>
                        Further, if you are a resident of the State of California and
                        would like to opt-out from the disclosure of your personal information
                        to any third party for marketing purposes, please e-mail us at: <a href="mailto:support@globalfantasysports.com"
                                                                                           target="_blank">  support@globalfantasysports.com</a>
                        Please be advised that where California State residents opt-out
                        from permitting their personal information to be shared, such
                        individuals may still receive selected offers directly from us,
                        in accordance with applicable law.
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">Personal Information Collected</h3>

                    <p>
                        For the purposes of this Privacy Policy, &ldquo;personal information&rdquo;
                        shall mean individually identifiable information from or about an
                        individual.  We collect personal information when you access certain
                        of the Global Fantasy Sports Golf™ Offerings and complete the required registration
                        form (collectively, &ldquo;Form&rdquo;).  The information that you must supply on
                        the Form may include, without limitation, your: (a) full name; (b)
                        state and country of residence; (c) e-mail address
                        (including the e-mail address associated with your PayPal® account,
                        where applicable); (d) password; (e) billing/mailing address
                        (where entering a Paid Contest, as defined below); (f) credit card
                        information (where selected as your preferred payment method); (g)
                        Social Security Number (for potential Prize winners); (h) picture
                        proof of identification, which may include a driver’s license,
                        passport, voting card or similar government issued identification
                        (for potential Prize winners); and/or (i) any other information
                        requested on the applicable Form.  In addition, Users may register
                        for an Account by and through their Facebook® accounts, Google+®
                        accounts or any other means designated by Global Fantasy Sports Golf™.
                        Where you register via your Facebook® account, Global Fantasy Sports Golf™ will
                        have access to your: (i) Facebook® public profile; (ii) the e-mail
                        address associated  with your Facebook® account; and (iii) any and
                        all other information made available to Global Fantasy Sports Golf™ by and through
                        your Facebook® account.  Where you register via your Google+®  account,
                        Global Fantasy Sports Golf™ will have access to your: (A) basic Google+®   profile
                        information; (B) the e-mail address associated with your Google+®
                        account; and (C) any and all other information made available to
                        Global Fantasy Sports Golf™ by and through your Google+®  account.
                        For purposes of this Privacy Policy, items (f), (g) and (h) in
                        the preceding sentence shall be considered &ldquo;Sensitive Information.&rdquo;
                    </p>
                </div>



                <div className="article__section">
                    <h3 className="title">Use of Personal Information</h3>
                    <p>
                        By submitting your personal information by and through the
                        Global Fantasy Sports Golf™ Offerings, you agree that we may share, sell, rent,
                        lease or otherwise provide that personal information
                        (other than Sensitive Information) to any third party for any
                        purpose permitted by law, and we may work with other businesses
                        to bring selected retail opportunities to our Users.
                        These businesses and third parties may include, but are not
                        limited to: (a) providers of direct marketing services and applications,
                        including lookup and reference, data enhancement, suppression and
                        validation; (b) e-mail marketers; (c) telemarketers (where permitted
                        by applicable law); and (d) direct marketers.  We will also use your
                        personal information to send you promotional messages regarding
                        various Global Fantasy Sports Golf™ products and/or services, as well as third
                        party products and/or services that we think may be of interest to you.
                    </p>
                    <p>
                        We may also employ other companies and individuals to perform certain
                        functions on our behalf.  Examples include sending direct and electronic
                        mail, removing duplicate information from User lists, analyzing data and
                        providing marketing analysis.  The agents performing these limited
                        functions on our behalf shall have access to our Users’ personal
                        information as needed to perform these functions for us, but we do
                        not permit them to use User personal information for other purposes.
                    </p>
                    <p>
                        We will also use your personal information for customer service,
                        to provide you with information that you may request, to customize
                        your experience with the Global Fantasy Sports Golf™ Offerings and/or to contact
                        you when necessary in connection with your use of the Global Fantasy Sports Golf™ Offerings.
                        We may also use your personal information for internal business purposes,
                        such as analyzing and managing our business including, without limitation,
                        the Global Fantasy Sports Golf™ Offerings.  We may also combine the information we have
                        gathered about you with information from other sources.
                    </p>
                    <p>
                        By submitting your personal information by and through the Global Fantasy Sports Golf™
                        Offerings, you agree that such act constitutes an inquiry and/or application
                        for purposes of the Amended Telemarketing Sales Rule (16 CFR §310 et seq.),
                        as amended from time to time (the &ldquo;Rule&rdquo;) and applicable state do-not-call
                        regulations.  As such, notwithstanding that your telephone number may be
                        listed on the Federal Trade Commission’s Do-Not-Call List, and/or on
                        applicable state do-not-call lists, we retain the right to contact you
                        via telemarketing in accordance with the Rule and applicable state
                        do-not-call regulations.
                    </p>
                    <p>
                        Where you provide &ldquo;prior express written consent&rdquo; within the meaning of
                        the Telephone Consumer Protection Act (47 USC § 227), and its
                        implementing regulations adopted by the Federal Communications Commission
                        47 CFR § 64.1200), as amended from time-to-time (&ldquo;TCPA&rdquo;), you consent
                        to receive telephone calls, including artificial voice calls,
                        pre-recorded messages and/or calls (including SMS text messages)
                        delivered via automated technology, to the telephone number(s)
                        that you provided.  Please note that you are not required to
                        provide this consent in order to obtain access to the Global Fantasy Sports Golf™ Offerings,
                        and your consent simply allows Global Fantasy Sports Golf™ to contact you via these means.
                        Further, you agree that we reserve the right to share, sell, rent, lease
                        and/or otherwise distribute your mobile telephone and other mobile data
                        with/to any third-party for any and all non-marketing uses permitted by
                        this Privacy Policy and applicable law. <b> Please be advised that by
                        agreeing to this Privacy Policy, you are obligated to immediately
                        inform us if and when the telephone number that you have previously
                        provided to us changes.</b>
                    </p>
                    <p>
                        We reserve the right to release current or past personal
                        information (including Sensitive Information): (i) in the event
                        that we believe that the Global Fantasy Sports Golf™ Offerings are being or
                        have been used in violation of the Terms and Conditions or to
                        commit unlawful acts; (ii) if the information is subpoenaed;
                        provided, however, that, where permitted by applicable law, we
                        shall provide you with e-mail notice, and opportunity to challenge
                        the subpoena, prior to disclosure of any personal information
                        pursuant to a subpoena; or (iii) if we are sold, merge with a
                        third party, are acquired or are the subject of bankruptcy proceedings;
                        provided, however, that if Global Fantasy Sports Golf™ is involved in a bankruptcy
                        proceeding, merger, acquisition or sale of all or a portion of its assets,
                        you will be notified via email and/or a prominent notice on the
                        Site of any change in ownership or uses of your personal information,
                        as well as any choices that you may have regarding your personal information.
                    </p>
                    <p>
                        You hereby consent to the disclosure of any record or communication
                        to any third-party when we, in our sole discretion, determine the
                        disclosure to be appropriate including, without limitation,
                        sharing your e-mail address with third-parties for suppression
                        purposes in compliance with applicable law including the
                        CAN-SPAM Act of 2003, as amended from time to time.
                        Users should also be aware that courts of equity, such as U.S.
                        Bankruptcy Courts, might have the authority under certain
                        circumstances to permit personal information to be shared or
                        transferred to third parties without permission.
                    </p>
                </div>



                <div className="article__section">
                    <h3 className="title">Credit Card and PayPal® Transactions</h3>
                    <p>
                        Global Fantasy Sports Golf™ utilizes third party service providers to provide
                        credit card and PayPal® payment processing services.
                        If you choose to fund your Account by and through the Site, the
                        Active Credit Card information provided, or PayPal® account
                        registration information, depending on your preferred payment
                        method, will either be collected directly by such third party
                        service provider or PayPal®, as applicable, or we will share
                        that payment information with our contracted third party service
                        provider(s) or PayPal®, as applicable.  Global Fantasy Sports Golf™ requires
                        that its third party payment processing service provider(s)
                        has/have in place privacy policies and practices consistent with
                        this Privacy Policy; provided, however, that we cannot guarantee
                        the privacy practices of our third party payment processing service
                        provider(s) including, without limitation, PayPal®.
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">Non-Personal Information Collection and Use</h3>
                    <p>
                        <b>IP Addresses/Browser Type</b>
                    </p>
                    <p>
                        We may collect certain non-personally identifiable information about
                        you when you visit many of the pages of the Site.  This non-personally
                        identifiable information includes, without limitation, the type of
                        browser that you use (e.g., Safari, Chrome, Internet Explorer), your
                        IP address, the type of operating system that you use (e.g., Windows or iOS)
                        and the domain name of your Internet service provider (e.g., Verizon, AT&amp;T).
                        We use the non-personally identifiable information that we collect to
                        improve the design and content of the Global Fantasy Sports Golf™ Offerings and to enable
                        us to personalize your Internet experience.  We also may use this information
                        in the aggregate to analyze usage of the Global Fantasy Sports Golf™ Offerings.
                    </p>
                    <p>
                        <b>Cookies </b>
                    </p>
                    <p>
                        When a User visits the Site, we send one (1) or more cookies and/or
                        gif files (collectively, &ldquo;Cookies&rdquo;) to assign an anonymous, unique
                        identifier to the applicable User’s computer or other device.
                        A Cookie is a piece of data stored on your hard drive containing
                        non-personally identifiable information about you.  Cookies have
                        many benefits to enhance your experience at the Site.
                        To find out more about Cookies, please visit www.cookiecentral.com.
                        We use Cookies to improve the quality of the Global Fantasy Sports Golf™ Offerings,
                        including for storing user preferences and tracking Site-user trends
                        (such as pages opened and length of stay at the Site).
                    </p>
                    <p>
                        Most Internet browsers are initially set up to accept Cookies,
                        but you can reset your browser to refuse all Cookies or to indicate
                        when a Cookie is being sent.  To disable and reject certain Cookies,
                        follow the instructions associated with your Internet browser.
                        Even in the case where a user rejects a Cookie, he or she may still
                        use the Global Fantasy Sports Golf™ Offerings; provided, however, that certain
                        functions of the Global Fantasy Sports Golf™ Offerings may be impaired or rendered
                        inoperable if the use of Cookies is disabled.  We reserve the right
                        to retain Cookie data indefinitely.
                    </p>
                    <p>
                        <b>Behavioral Tracking/Tracking Cookies </b>
                    </p>
                    <p>
                        Global Fantasy Sports Golf™ uses Cookies and other technology on the Site that
                        tracks end-users’ activities after they leave the Site for advertisement
                        retargeting purposes, and otherwise.  Global Fantasy Sports Golf™ uses this tracking
                        technology to target the applicable end-user with advertisements regarding
                        the Global Fantasy Sports Golf™ Offerings and other products and/or services that
                        Global Fantasy Sports Golf™ thinks might be of interest to the applicable end-user.
                        In addition, third parties such as those set forth below, may engage
                        in a similar form of behavioral tracking of end-users that visit the Site.
                    </p>
                    <p>
                        End-users may be able to disable some or all of this tracking activity
                        by utilizing the &ldquo;Do Not Track&rdquo; setting or similar options within most
                        major Internet browsers.  To the greatest extent permissible under applicable law,
                        we are not responsible for the tracking practices of third parties
                        in connection with the Site.  To opt-out of behavioral tracking
                        conducted by Global Fantasy Sports Golf™, please Click Here
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">Google Inc. (&ldquo;Google&rdquo;)</h3>

                    <p>
                        Google, as a third party vendor, uses Cookies to serve ads on our
                        Site and other online venues, based on Users’ visits to our Site
                        and/or other websites on the Internet.  You may opt-out of Google’s
                        tracking as set forth below:
                    </p>
                    <p><b>Google’s Use of the DoubleClick Cookie</b></p>
                    <p>
                        You may opt out of the use of the DoubleClick Cookie for interest-based
                        advertising by visiting the Google Ads Settings
                        &nbsp;<a href="http://www.google.com/ads/preferences" target="_blank">
                        (http://www.google.com/ads/preferences)</a>&nbsp;
                    </p>
                    <p><b>Google AdWords </b></p>
                    <p>
                        You can opt-out of Google Analytics for Display Advertising and
                        customize the Google Display Network ads by visiting the Google
                        Ads Settings
                        &nbsp;<a href="http://www.google.com/settings/ads" target="_blank">
                        (http://www.google.com/settings/ads)</a> page.
                        Google also recommends installing the Google Analytics Opt-out
                        Browser Add-on
                        &nbsp;<a href="https://tools.google.com/dlpage/gaoptout" target="_blank">
                        (https://tools.google.com/dlpage/gaoptout)</a> for
                        your web browser. Google Analytics Opt-Out Browser Add-on provides
                        Users with the ability to prevent their data from being collected
                        and used by Google Analytics.
                    </p>
                    <p>
                        For more information on the privacy practices of Google, please
                        visit the Google Privacy &amp; Terms
                        &nbsp;<a href="http://www.google.com/intl/en/policies/privacy/"
                                 target="_blank">(http://www.google.com/intl/en/policies/privacy/)</a>&nbsp; web page.
                    </p>
                </div>


                <div className="article__section">
                    <h3 className="title">AdRoll</h3>
                    <p>
                        The AdRoll behavioral tracking/remarketing service is provided by
                        Semantic Sugar, Inc. (&ldquo;AdRoll&rdquo;).  You can opt-out of AdRoll
                        remarketing by visiting this AdRoll Advertising Preferences
                        &nbsp;<a href="http://info.evidon.com/pub_info/573?v=1&nt=1&nw=false"
                                 target="_blank">(http://info.evidon.com/pub_info/)</a>&nbsp; web page.
                    </p>
                </div>


                <div className="article__section">
                    <h3 className="title">Perfect Audience</h3>
                    <p>
                        The Perfect Audience behavioral tracking/remarketing service is
                        provided by NowSpots Inc. (&ldquo;Perfect Audience&rdquo;).  You can opt-out of
                        Perfect Audience remarketing by visiting these pages:
                        Platform Opt-out
                        &nbsp;<a href="http://pixel.prfct.co/coo"
                                 target="_blank">(http://pixel.prfct.co/coo)</a>&nbsp;
                        and Partner Opt-out <a href="http://ib.adnxs.com/optout"
                                               target="_blank">(http://ib.adnxs.com/optout)</a>.
                    </p>
                    <p>
                        For more information on the privacy practices of Perfect Audience,
                        please visit the Perfect Audience Privacy Policy &amp; Opt-Out
                        &nbsp;<a href="https://www.perfectaudience.com/privacy/index.html"
                                 target="_blank">
                        (https://www.perfectaudience.com/privacy/index.html)</a>&nbsp;
                        web page.
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">AppNexus</h3>
                    <p>
                        The AppNexus remarketing service is provided by AppNexus Inc. (&ldquo;AppNexus&rdquo;).
                        You can opt-out of AppNexus remarketing by visiting
                        the Privacy &amp; the AppNexus Platform
                        &nbsp;<a href="http://www.appnexus.com/platform-policy#choices"
                                 target="_blank">(http://www.appnexus.com/platform-policy-choices)</a>&nbsp;
                        web page.
                    </p>
                    <p>
                        For more information on the privacy practices of
                        AppNexus, please visit the AppNexus Platform Privacy
                        Policy
                        &nbsp;<a href="http://www.appnexus.com/platform-policy"
                                 target="_blank">(http://www.appnexus.com/platform-policy)</a>&nbsp;
                        web page.
                    </p>
                </div>


                <div className="article__section">
                    <h3 className="title">Aggregate Data</h3>
                    <p>
                        Global Fantasy Sports Golf™ reserves the right to transfer and/or sell aggregate
                        or group data about Users of the Global Fantasy Sports Golf™ Offerings for lawful purposes.
                        Aggregate or group data is data that describes the demographics, usage or other
                        characteristics of Global Fantasy Sports Golf™ Offerings Users as a group, without
                        disclosing personally identifiable information.
                    </p>
                </div>


                <div className="article__section">
                    <h3 className="title">Third-Party Websites</h3>

                    <p>
                        The Site may contain links to third-party owned and/or
                        operated websites including, without limitation, the Third Party
                        Provider websites.  Global Fantasy Sports Golf™ is not responsible for the privacy
                        practices or the content of such websites.  In some cases, you may
                        be able to make a purchase through one of these third-party websites.
                        In these instances, you may be required to provide certain information,
                        such as a credit card number, to register or complete a transaction
                        at such website.  These third-party websites have separate privacy and
                        data collection practices and Global Fantasy Sports Golf™ has no responsibility or
                        liability relating to them.
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">Communications</h3>

                    <p>
                        We may use your Personal Information to contact you
                        with newsletters, marketing or promotional materials
                        and other information that may be of interest to you.
                        You may opt out of receiving any, or all, of these
                        communications from us by following the unsubscribe
                        link or instructions provided in any email we send.
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">Security</h3>

                    <p>We endeavor to safeguard and protect our Users’ personal information.
                        When Users make personal information available to us, their personal
                        information is protected both online and offline (to the extent that
                        we maintain any personal information offline).
                        Where our registration/application process prompts Users to enter
                        Sensitive Information, and when we store and transmit such Sensitive
                        Information, that information is encrypted with advanced TLS
                        (Transport Layer Security).
                    </p>
                    <p>Access to your personal information is strictly limited, and we take
                        reasonable measures to ensure that your personal information is not
                        accessible to the public.  All of our Users’ personal information is
                        restricted in our offices, as well as the offices of our third party
                        service providers.  Only employees or third party agents who need
                        the personal information to perform a specific job are granted access
                        to personal information.  Our employees are dedicated to ensuring
                        the security and privacy of all User personal information.
                        Employees not adhering to our firm policies are subject to
                        disciplinary action.  The servers that we store personal
                        information on are kept in a secure physical environment.
                        We also have security measures in place to protect the loss, misuse
                        and alteration of personal information under our control.
                    </p>
                    <p>Please be advised, however, that while we take every reasonable
                        precaution available to protect your data, no storage facility,
                        technology, software, security protocols or data transmission
                        over the Internet or via wireless networks can be guaranteed
                        to be 100% secure.  Computer hackers that circumvent our security
                        measures may gain access to certain portions of your personal
                        information, and technological bugs, errors and glitches may cause
                        inadvertent disclosures of your personal information; provided,
                        however, that any attempt to breach the security of the network,
                        our servers, databases or other hardware or software may constitute
                        a crime punishable by law.  For the reasons mentioned above, we
                        cannot warrant that your personal information will be absolutely
                        secure.  Any transmission of data at or through the Site, other
                        Global Fantasy Sports Golf™ Offerings, or otherwise via the Internet or wireless
                        network, is done at your own risk.
                    </p>
                    <p>In compliance with applicable federal and state laws, we shall
                        notify you and any applicable regulatory agencies in the event
                        that we learn of an information security breach with respect to
                        your personal information.  You will be notified via e-mail in
                        the event of such a breach.  Please be advised that notice may
                        be delayed in order to address the needs of law enforcement,
                        determine the scope of network damage, and to engage in remedial
                        measures.
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">Minors</h3>
                    <p>
                        Visitors under eighteen (18) years of age are not permitted to
                        use and/or submit their personal information at the Site.
                        Global Fantasy Sports Golf™ does not knowingly solicit or collect information
                        from visitors under eighteen (18) years of age.  Global Fantasy Sports Golf™
                        encourages parents and guardians to spend time online with their
                        children and to participate and monitor the interactive activities
                        of their children.
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">Opt-Out/Unsubscribe</h3>
                    <p>
                        To opt-out of receiving e-mail communications from us, you can
                        follow the instructions at the end of the applicable e-mail message or e-mail us at:
                        &nbsp;<a href="mailto:support@globalfantasysports.com" target="_blank">support@globalfantasysports.com
                    </a>&nbsp;
                    </p>
                    <p>Notwithstanding the foregoing, we may continue to contact you for
                        the purpose of communicating information relating to your request
                        for Global Fantasy Sports Golf™ Offerings, your participation in the Contests, as
                        well as to respond to any inquiry or request made by you.
                        To opt-out of receiving Global Fantasy Sports Golf™ Offerings-related,
                        Contest-related and/or inquiry response-related messages from
                        Global Fantasy Sports Golf™, you must cease requesting and/or utilizing the
                        Global Fantasy Sports Golf™ Offerings, cease participating in the Contests
                        and/or cease submitting inquiries to Global Fantasy Sports Golf™, as applicable.
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">Deleting, Modifying and Updating Your Information</h3>
                    <p>
                        At your request, we will: (a) inform you of what personal information
                        we have on file for you; (b) amend the personal information that we
                        have on file for you; and/or (c) remove personal information that
                        you have provided to us, or that we have collected.  You may do so by e-mailing us at:
                        &nbsp;<a href="mailto:support@globalfantasysports.com" target="_blank">support@globalfantasysports.com
                    </a>&nbsp;.
                        We ask individual Users to identify themselves and the information
                        requested to be accessed, corrected or removed before processing
                        such requests, and, to the extent permitted by applicable law,
                        we may decline to process requests that are unreasonably repetitive
                        or systematic, require disproportionate technical effort,
                        jeopardize the privacy of others or would be extremely impractical
                        (for instance, requests concerning information residing on backup tapes).
                    </p>
                    <p>
                        Please be advised that deleting your personal information may
                        terminate your access to certain of the Global Fantasy Sports Golf™ Offerings,
                        including potentially terminating your eligibility in any Contest(s)
                        that you have entered.  If you wish to continue using the full complement
                        of Global Fantasy Sports Golf™ Offerings, you may not be able to delete all of the
                        personal information that we have on file for you.
                    </p>
                    <p>
                        Please be further advised that, after you delete your personal
                        information, residual copies may take a period of time before they
                        are deleted from our active servers and may remain in our backup
                        systems.
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">Changes to this Privacy Policy</h3>

                    <p>
                        Global Fantasy Sports Golf™ reserves the right to change or update this Privacy
                        Policy at any time by posting a notice on the Site that we are
                        changing our Privacy Policy.  If the manner in which we use
                        personal information changes, Global Fantasy Sports Golf™ will notify Users
                        by: (a) sending the modified policy to our Users via email;
                        and/or (b) by any other reasonable means acceptable under
                        applicable state and federal law.  You will have a choice
                        as to whether or not we use your information in this different
                        manner and we will only use your information in this different
                        manner where you opt-in to such use.
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">Contact Us</h3>

                    <p>
                        If you have any questions about this Privacy Policy,
                        please e-mail us <a href="mailto:support@globalfantasysports.com"
                                            target="_blank">support@globalfantasysports.com</a>
                    </p>
                </div>

                <div className="article__section">
                    <h3 className="title">Filing a Complaint with the Federal Trade Commission</h3>

                    <p>
                        To file a complaint regarding our privacy practices, please
                        &nbsp;<a href="https://www.ftccomplaintassistant.gov/FTC_Wizard.aspx?Lang=en"
                                 target="_blank">Click here</a>&nbsp;
                    </p>

                </div>

                <div className="article__actions">
                    <BtnLink to="__back" mod={cls}>
                        Back
                    </BtnLink>
                </div>
            </article>
        </section>`

