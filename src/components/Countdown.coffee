moment = require('moment')
{now} = require('../lib/date')


zeroed = (val) ->
    if val >= 10 then "#{val}" else "0#{val}"


module.exports = React.create(
    displayName: "Countdown"


    getDefaultProps: -> {
        blockName: 'countdown'
    }

    getInitialState: -> {
        now: now()
    }

    componentDidMount: ->
        @isMounted = true
        @updateTime()

    componentWillUnmount: ->
        @isMounted = false

    updateTime: ->
        if @isMounted
            @setState({now: now()})
            setTimeout(@updateTime, 1000)

    render: ({target, blockName}, {now}) ->
        countdown = if now >= target
            "Live"
        else
            diff = moment.duration(target - now)
            "#{diff.days()}D #{zeroed(diff.hours())}:#{zeroed(diff.minutes())}:#{zeroed(diff.seconds())}"

        `<section className={blockName}>
            <span className={blockName + "__text"}>{countdown}</span>
        </section>`
)
