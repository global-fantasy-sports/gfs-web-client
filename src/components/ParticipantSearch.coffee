_ = require('underscore')

{cx} = require('../lib/utils')
{Input} = require('../lib/forms')


class ParticipantSearch extends React.Component

    @propTypes: {
        value: React.PropTypes.string.isRequired
        onChange: React.PropTypes.func.isRequired
    }

    constructor: (props) ->
        super(props)
        @notifyChange = _.throttle(@notifyChange, 150, {leading: false})
        @state = {
            value: props.value or ''
        }

    componentWillReceiveProps: (nextProps) ->
        if nextProps.value isnt @state.value
            @setState({value: nextProps.value})

    notifyChange: =>
        @props.onChange(@state.value)

    handleChange: (e) =>
        @setState({value: e.target.value}, @notifyChange)

    render: ->
        `<Input
            {...this.props}
            className={cx('players-list-filters__search-input', this.props.className)}
            placeholder="Search Players"
            value={this.state.value}
            onChange={this.handleChange}
        />`


module.exports = ParticipantSearch
