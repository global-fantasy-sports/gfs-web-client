{Icon} = require('../../lib/ui')


GamesScheduleDay = ({month, value}) ->
    items = value.map((tour, i) ->
        date = tour.getTournamentPeriod().date

        `<div className="games-schedule__item games-schedule__item--wide" key={i}>
            <p className="games-schedule__tour-date">{date}</p>
            <p className="games-schedule__tour">{tour.name}</p>
            <p className="games-schedule__money">${tour.purse}</p>
            <p className="games-schedule__purse">purse</p>
            <div className="games-schedule__win">
                <div className="games-schedule__win-info">
                    <p className="games-schedule__win-money">${tour.winningShare}</p>
                    <p className="games-schedule__win-text">Winning Share</p>
                </div>
                <div className="games-schedule__win-info">
                    <p className="games-schedule__win-money">{tour.fedexPoints}</p>
                    <p className="games-schedule__win-text">Fedexcup points</p>
                </div>
                <div className="games-schedule__weather">
                    <div className="games-schedule__weather-info">76F</div>
                    <div className="games-schedule__weather-info">
                        <img src="/static/img/lobby/cloud-small.png" alt="Party cloudy" />
                    </div>
                    <div className="games-schedule__weather-info">0
                        <span className="games-schedule__temp"> mph/sec</span>
                    </div>
                    <div className="games-schedule__weather-info">75%</div>
                </div>
            </div>
        </div>`
    )

    `<div className="games-schedule__day">
        <div className="games-schedule__top">
            <div>
                <p className="games-schedule__date">{month}</p>
            </div>
        </div>
        <div className="games-schedule__items-wrap">
            {items}
        </div>
    </div>`


GolfGamesSchedule = React.create(
    displayName: "GolfGamesSchedule"

    mixins: [flux.Connect("tournaments", (store) ->
        tournaments: store.getTournamentsByMonth()
    )]

    renderContests: ->
        @state.tournaments.entrySeq().map(([month, value], i) ->
            `<GamesScheduleDay key={i} value={value} month={month}/>`
        ).toArray()


    render: ({}, {tournaments}) ->
        contest = @renderContests()

        `<div className="games-schedule">
            <div className="games-schedule__content">
                <div className="games-schedule__icon-left">
                    <Icon i="nfl-arrow-left" />
                </div>
                <div className="games-schedule__inner">
                    {contest}
                </div>
                <div className="games-schedule__icon-right">
                    <Icon i="nfl-arrow-right" />
                </div>
            </div>
        </div>`

)

module.exports = GolfGamesSchedule
