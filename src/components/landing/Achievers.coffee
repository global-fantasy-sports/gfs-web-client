_ = require 'underscore'
{Titlebar} = require 'lib/ui/Titlebar'
{Icon} = require 'lib/ui'
PlaceholderImage = require('../../lib/ui/PlaceholderImage')
{cx} = require 'lib/utils'

Achiever = React.create(
    displayName: "Achiever"

    onStarClick: (id) ->
        (e) ->
            e.stopPropagation()
            flux.actions.data.toggleFavorite(id)

    onClick: ->
        flux.actions.state.openPlayerDetailsDialog(@props.player.participantId)

    render: ({key, player = {}, achiever, underachiever}) ->
        clsRank = cx(
            "achiever__rank": true
            "achiever__rank--red": underachiever
            "achiever__rank--green": !underachiever
        )
        clsStar = cx(
            "star-orange": player.isFavorite
            "star-gray": !player.isFavorite
        )

        team = achiever.get('teamId')
        teamLogo = "url(#{flux.stores.participants.getTeam(team).logo})"

        `<div className="achiever__item" onTap={this.onClick}>
            <div className="achiever__img">
                <div className="achiever__team-photo" style={{backgroundImage: teamLogo}}/>
                <PlaceholderImage
                    src={player.smallPhoto}
                    placeholder="/static/img/nfl-player-placeholder-small.png"
                />
                <div className="achiever__star" onTap={this.onStarClick(player.id)}>
                    <Icon i={clsStar} />
                </div>
            </div>
            <div className="achiever__data">
                <p className="achiever__name">{player.nameFull}</p>
                <div className="achiever__opponent">
                    <p>{player.currentPosition}</p>
                    <p>{achiever.get('teamId')}</p>
                </div>
            </div>
            <div className="achiever__points">
                <div className={clsRank}>
                    <Icon i="rank-arrow" className={underachiever ? "rotateBack-180" : ""}/>
                    <span title={achiever.get('deviation')}>{achiever.get('points')}</span>
                </div>
                <div className="achiever__points-devider"></div>
                <div className="achiever__fppg">{player.FPPG} <span className="achiever__fppg-small">FPPG</span></div>
            </div>
        </div>`
)


module.exports = React.create(
    displayName: "Achievers"

    mixins: [
        flux.Connect('participants', (store, props) ->
            overachievers = store.getOverachievers()
            underachievers = store.getUnderachievers()

            return {
                overachievers, underachievers,
                overachieverParticipants: overachievers.map((p) -> store.getPlayer(p.get('id')))
                underachieverParticipants: underachievers.map((p) -> store.getPlayer(p.get('id')))
            }
        )
    ]

    render: (P, {overachievers, underachievers, underachieverParticipants, overachieverParticipants}) ->
        achiever = overachievers.concat(underachievers).first()
        return null unless achiever

        week = achiever.get('week')
        year = achiever.get('year')

        itemsOver = overachieverParticipants.map((overachiever, i) ->
            `<Achiever player={overachiever} achiever={overachievers.get(i)} key={i} />`
        )

        itemsUnder = underachieverParticipants.map((underachiever, i) ->
            `<Achiever player={underachiever} achiever={underachievers.get(i)} key={i} underachiever={true} />`
        )


        `<div className="achiever">
            <div className="achiever__left">
                <div className="achiever__top">
                    <Titlebar className="white">Overachievers</Titlebar>
                </div>
                <div className="achiever__content">
                    {itemsOver}
                </div>
            </div>
            <div className="achiever__week">Week {week}</div>
            <div className="achiever__right">
                <div className="achiever__top achiever__top--right">
                    <Titlebar className="white">Underachievers</Titlebar>
                </div>
                <div className="achiever__content">
                    {itemsUnder}
                </div>
            </div>
        </div>`
)
