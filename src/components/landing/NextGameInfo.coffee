moment = require('moment')
{pad} = require('../../lib/utils')
Connect = require('../../flux/connectMixin')


Item = ({label, value, optional}) ->
    if not value and optional
        return `<script />`

    `<div className="game-countdown__item">
        <p className="game-countdown__num">{pad(value)}</p>
        <p className="game-countdown__text">{label}</p>
    </div>`


NextGameInfo = React.create(

    mixins: [
        Connect('time', (store, {date}) -> {
            diff: store.getDiffToNow(date)
        })
    ]

    render: ({date}, {diff}) ->
        diff = moment.duration(diff)
        if diff < 0
            return null

        `<div className="next-game-info">
            <div className="next-game-info__item next-game-info__item--left w-15">
                <p className="stroke lato-heavy black medium text-up">Next game begins in</p>
            </div>
            <div className="next-game-info__item w-40">
                <div className="game-countdown" title={String(date)}>
                    <Item label="DAYS" value={diff.get('days')} optional />
                    <Item label="HOURS" value={diff.get('hours')} />
                    <Item label="MINUTES" value={diff.get('minutes')} />
                    <Item label="SECONDS" value={diff.get('seconds')} />
                </div>
            </div>
            <div className="next-game-info__item w-20" />
        </div>`
)

module.exports = NextGameInfo
