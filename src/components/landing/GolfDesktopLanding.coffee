_ = require('underscore')
{STATE} = require('../../lib/const')
{Titlebar} = require('../../lib/ui/Titlebar')
GolfGamesSchedule = require('../landing/GolfGamesSchedule')
LandingCompetitionRoomList = require('../stakerooms/LandingCompetitionRoomList')
Achievers = require('../landing/Achievers')
MyGamesOverview = require('./MyGamesOverview')
GolfGameSelector = require('./GolfGameSelector')
GolfLeaderboard = require('./GolfLeaderboard')


GolfDesktopLanding = React.create(
    displayName: 'GolfDesktopLanding'

    mixins: [
        flux.Connect('connection', (store) ->
            games: store.getState().games
            connectionState: store.getState().state
        )
        flux.Connect('rooms', (store) ->
            allRoomsStarted: store.isAllStarted()
        )
    ]

    componentDidMount: ->
        _.defer(=>
            flux.actions.state.scrollToTop({
                el: document.getElementById(@props.page)
                duration: 0
            })
        )

    componentDidUpdate: ({route}) ->
        if route.page isnt @props.route.page
            @scrollTo(@props.route.page)

    scrollTo: (name) ->
        el = document.getElementById(name or 'home')
        flux.actions.state.scrollToTop({el})

    render: ({user, onChallenge, onEditGame, onPlay}, {connectionState}) ->
        if connectionState != STATE.CONNECTED
            return null


        `<section ref="main" className="app__in">
            <div className="app__content">
                <a id="schedule" />
                <div className="app__row">
                    <Titlebar title="Tournament">schedule</Titlebar>
                    <GolfGamesSchedule />
                </div>
                <div className="app__row">
                    <GolfGameSelector onPlay={onPlay} />
                </div>
                <div className="app__row">
                    <Titlebar>Leaderboard</Titlebar>
                    <GolfLeaderboard />
                </div>

                <a id="mygames" />
                <div className="app__row">
                    <Titlebar title="My">games</Titlebar>
                    <MyGamesOverview
                        onPlay={onPlay}
                        onEditGame={onEditGame}
                    />
                </div>

                <a id="competition-rooms" />
                <LandingCompetitionRoomList />
            </div>
        </section>`
)


module.exports = GolfDesktopLanding
