_ = require('underscore')
moment = require('moment')
bemClassName = require('bem-classname')
Connect = require('../../flux/connectMixin')
{cx, pad, dateFmt, timeFmt} = require('../../lib/utils')
{Btn, BtnExpand, BtnLink, Icon, Tokens, Dollars} = require('../../lib/ui')
{GAMEDEF} = require('../../games')
ItemWrapper = require('../../components/stakerooms/ItemWrapper')
GamePointsAndPosition = require('../games/GamePointsAndPosition')
CompetitionPlayers = require('../competitionRoom/CompetitionPlayers')
RoomStatusIcon = require('../competitionRoom/RoomStatusIcon')
GameCountdown = require('./GameCountdown')


GameOverviewItem = React.create(
    displayName: 'GameOverviewItem'

    contextTypes:
        router: React.PropTypes.object

    mixins: [
        Connect('rooms', (store, {game}) -> {
            room: store.getRoom(game.competitionRoomId)
        })
    ]

    getInitialState: ->
        open: false

    onEditGame: ->
        @props.onEditGame(@props.game, @props.game.gameType)

    onClone: ->
        flux.actions.roster.cloneGame(@props.game)
        @context.router.transitionTo("/#{SPORT}/salarycap/select/")

    onRemove: ->
        flux.actions.tap.openRemoveGameDialog( () =>
            @props.game.remove()
        )

    toggleControlButtons: ->
        @setState(open: not @state.open)

    render: ({game, status}, {room, open}) ->
        return null unless game and room.id

        isDisabled = if not game.isPending() then " disabled" else ""

        Money = if room.mode is 'token' then Tokens else Dollars

        iconBg =
            if room.isFinished() then 'finished'
            else if room.isLive() then 'started'
            else 'not-started'
        openCls = if open then 'open' else ''

        prize = game.getPrize()

        `<ItemWrapper mod={SPORT == 'golf' ? 'golf' : ""} game={game}>
            <div className={bemClassName('room-status-icon', [iconBg])}>
                <RoomStatusIcon room={room} />
                <p>WEEK {room.week}</p>
            </div>
            <section className={SPORT == 'nfl' ? "w-15" : "w-25"}>
                {room.isPending()
                    ? <div className="data-items"><GameCountdown startDate={room.startDate} /></div>
                    : <GamePointsAndPosition game={game} />
                }
            </section>

            <section className="w-25">
                <div className="data-items data-items--row">
                    <div className="data-item">
                        <p className="data-item__text">Entries</p>
                        <div className="data-item__num">
                            {room.gamesCount}/{room.maxEntries}
                        </div>
                    </div>
                    <div className="data-item">
                        <p className="data-item__text">Entry fee</p>
                        <div className="data-item__num">
                            <Money m={room.entryFee}/>
                        </div>
                    </div>
                    <div className="data-item">
                        <p className="data-item__text">
                            {prize ? 'Prize' : 'Prize pool'}
                        </p>
                        <div className="data-item__num">
                            <Money m={prize || room.pot} mod="green"/>
                        </div>
                    </div>
                </div>
            </section>

            <section className={SPORT == 'nfl' ? "game-table__players w-45 flex-row-stretch" : "w-30 flex-row-stretch"}>
                <CompetitionPlayers
                    game={game}
                    overlay={prize}
                />
                <BtnExpand onTap={this.toggleControlButtons}/>
            </section>

            <section className={bemClassName("game-table", "buttons", [openCls])}>
                {SPORT == 'nfl' && open ?
                    <Btn mod={"square" + isDisabled}
                        iconBg={true}
                        icon="clone"
                        onTap={!isDisabled ? this.onClone : null}>
                        Clone
                    </Btn>
                : null}
                {open ?
                    <Btn mod={"square " + isDisabled}
                        iconBg={true}
                        icon="pencil"
                        onTap={!isDisabled ? this.onEditGame : null}>
                        Edit
                    </Btn>
                : null}
                <BtnLink to={"/" + SPORT + "/competition/" + room.id + "/"}
                    mod={"square"}
                    iconBg={true}
                    icon="view-games">
                    View Games
                </BtnLink>
                {open ?
                    <Btn mod={"square text-red " + isDisabled}
                        iconBg={true}
                        icon="trash"
                        onTap={!isDisabled ? this.onRemove : null}>
                        Delete
                    </Btn>
                : null}
            </section>
        </ItemWrapper>`
)

module.exports = GameOverviewItem
