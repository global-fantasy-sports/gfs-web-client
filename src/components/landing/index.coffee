_ = require 'underscore'
strftime = require 'strftime'

{Link} = require 'lib/ui'
{eventTracker} = require 'lib/utils'
{Titlebar} = require 'lib/ui/Titlebar'
{now} = require 'lib/date'

{STATE} = require 'lib/const'

Logo = require 'lib/ui/Logo'
ToursInfo = require './ToursInfo'


exports.SeasonProgress = SeasonProgress = React.create
    displayName: 'SeasonProgress'
    render: ->
        week = parseInt(strftime('%W', now()), 10)
        style = {width: ((week + 5) * 100 / 54) + '%'}

        `<div className="season-progress">
            <Titlebar title=" Season progress" className="app__title"/>
            <div className="season-progress__bar">
                <div className="season-progress__bar-slider" style={style}>
                    <i className="season-progress__bar-stripes">
                        <span className="season-progress__current-week">
                            {week+1}
                            <i className="season-progress__week">week</i>
                        </span>
                    </i>
                    <i className="season-progress__bar-indicator"></i>
                </div>
                <div className="season-progress__el">
                    <span className="season-progress__season">
                        Winter
                    </span>
                </div>
                <div className="season-progress__el">
                    <span className="season-progress__season">
                        Spring
                    </span>
                </div>
                <div className="season-progress__el">
                    <span className="season-progress__season">
                        Summer
                    </span>
                </div>
                <div className="season-progress__el">
                    <span className="season-progress__season">
                        Autumn
                    </span>
                </div>
            </div>
        </div>`


exports.Footer = Footer = React.create
    displayName: 'Footer'

    about: (e) ->

    render: ({logo}) ->
        `<footer className="app__foot">
            <section className="foot">
                <div className="foot__col">
                    <p className="eligible">
                        Residents of the States of Arizona, Hawaii, Illinois, Iowa, Louisiana, Montana, Nevada, North Dakota,
                        New York, Tennessee, Texas, Vermont and Washington, as well as Puerto Rico, and where otherwise
                        prohibited by law, are not eligible to enter Global Fantasy Sports Inc pay to play
                        contest. (as defined in the Contest Rules).
                    </p>
                </div>
                <div className="foot__col">
                    <Link to="/policy/"><a className="foot__link">Privacy Policy/Your California Privacy Rights</a></Link>&nbsp;
                    <Link to="/terms/"><a className="foot__link">Terms and conditions</a></Link>&nbsp;
                    <Link to="/contest-rules/"><a className="foot__link">Contest Rules</a></Link>
                </div>
                <div className="foot__col">
                    <p className="copyright">
                        <span>Copyright</span>
                        <span>&copy;</span>
                        <span>2016 Global Fantasy Sports Inc. All Rights Reserved</span>
                    </p>
                </div>
                <div className="foot__col foot__col--logos">
                    <p className="ssl">
                        <a href="https://www.instantssl.com/ssl-certificate.html" className="ssl__link">
                            <img alt="SSL Certificate"
                                src="https://www.instantssl.com/ssl-certificate-images/support/comodo_secure_100x85_transp.png"
                                className="ssl__img"/>
                            <span>SSL Certificate</span>
                        </a>
                    </p>
                    <a href="http://www.sportsdatallc.com/" className="sports-data-api-logo" target="_blank">
                        <img src="/static/logos/SportsData.png"
                            alt="Sports Data"/>
                    </a>
                    <div className="foot__fsta">
                        <img src="/static/logos/FSTA-logo.png"
                             alt="FSTA"/>
                    </div>
                </div>
            </section>
        </footer>`

