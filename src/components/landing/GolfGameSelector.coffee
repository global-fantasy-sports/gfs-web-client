{Btn, BtnLink} = require('lib/ui')
{eventTracker} = require('lib/utils')


module.exports = React.create(
    displayName: "GolfGameSelector"

    play: (gameType) ->
        () =>
            eventTracker('buttons', gameType + "-play")
            @props.onPlay(null, {gameType: gameType})


    render: ({onPlay}) ->
        `<div className="golf-game-selector">
            <div className="golf-game-selector__item">
                <div className="golf-game-selector__img">
                    <img src="/static/img/lobby/5-ball.png" alt=""/>
                </div>
                <div className="golf-game-selector__title golf-game-selector__title--blue">5 BALL</div>
                <div className="golf-game-selector__diff">Difficulty : Medium</div>
                <div className="golf-game-selector__desc">
                    Build a team of 5 golfers and win by scoring the most points collectively. Points
                    system in use: Modified Stableford Scoring
                </div>
                <div className="golf-game-selector__btn">
                    <BtnLink to={'/golf/rules/five-ball/'}
                             mod="rules">
                        Rules
                    </BtnLink>
                </div>
                <div className="golf-game-selector__btn">
                    <Btn mod="red text-12" onTap={this.play('five-ball')}>Play</Btn>
                </div>
            </div>

            <div className="golf-game-selector__item">
                <div className="golf-game-selector__img">
                    <img src="/static/img/lobby/hole-in-one.png" alt=""/>
                </div>
                <div className="golf-game-selector__title golf-game-selector__title--green">handicap-the-pros</div>
                <div className="golf-game-selector__diff">Difficulty : HARD</div>
                <div className="golf-game-selector__desc">
                    All golfers are ranked and given a handicap. You on which holes to apply the extra
                    strokes. Turn a birdie into an eagle and supercharge the score of your golfer!
                </div>
                <div className="golf-game-selector__btn">
                    <BtnLink to={'/golf/rules/handicap-the-pros/'}
                             mod="rules">
                        Rules
                    </BtnLink>
                </div>
                <div className="golf-game-selector__btn">
                    <Btn mod="red text-12" onTap={this.play('handicap-the-pros')}>Play</Btn>
                </div>
            </div>

            <div className="golf-game-selector__item">
                <div className="golf-game-selector__img">
                    <img src="/static/img/lobby/pick-18.png" alt=""/>
                </div>
                <div className="golf-game-selector__title golf-game-selector__title--orange">hole-by-hole</div>
                <div className="golf-game-selector__diff">Difficulty : Easy</div>
                <div className="golf-game-selector__desc">
                    Predict the outcome of each hole for your golfers. Will it be birdie?
                    A par? <br/> A double-bogey?
                </div>
                <div className="golf-game-selector__btn">
                    <BtnLink to={'/golf/rules/hole-by-hole/'}
                             mod="rules">
                        Rules
                    </BtnLink>
                </div>
                <div className="golf-game-selector__btn">
                    <Btn mod="red text-12" onTap={this.play('hole-by-hole')}>Play</Btn>
                </div>
            </div>
        </div>`
)
