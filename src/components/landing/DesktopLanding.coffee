_ = require('underscore')
{STATE} = require('../../lib/const')
{Titlebar} = require('../../lib/ui/Titlebar')
GamesSchedule = require('../landing/GamesSchedule')
LandingCompetitionRoomList = require('../stakerooms/LandingCompetitionRoomList')
Achievers = require('../landing/Achievers')
MyGamesOverview = require('./MyGamesOverview')


DesktopLanding = React.create(
    displayName: 'DesktopLanding'

    mixins: [
        flux.Connect('connection', (store) ->
            games: store.getState().games
            connectionState: store.getState().state
        )
        flux.Connect('games', (store) ->
            contestsByDay: store.getContestsByDay()
        )
    ]

    componentDidMount: ->
        _.defer(=>
            flux.actions.state.scrollToTop({
                el: document.getElementById(@props.page)
                duration: 0
            })
        )

    componentDidUpdate: ({route}) ->
        if route.page isnt @props.route.page
            @scrollTo(@props.route.page)

    scrollTo: (name) ->
        el = document.getElementById(name or 'home')
        flux.actions.state.scrollToTop({el})

    render: ({user, onChallenge, onEditGame, onPlay}, {contestsByDay, connectionState}) ->

        unless connectionState in [STATE.CONNECTED, STATE.PROCESSING]
            return null

        `<section ref="main" className="app__in">
            <section className="app__content">
                <a id="schedule" />
                <div className="app__row">
                    <Titlebar title="Games">schedule</Titlebar>
                    <GamesSchedule contestsByDay={contestsByDay} />
                </div>
                <div className="app__row">
                    <Achievers />
                </div>

                <a id="mygames" />
                <div className="app__row">
                    <Titlebar title="My">games</Titlebar>
                    <MyGamesOverview
                        onPlay={onPlay}
                        onEditGame={onEditGame}
                    />
                </div>

                <a id="competition-rooms" />
                <LandingCompetitionRoomList />
            </section>
        </section>`
)

module.exports = DesktopLanding
