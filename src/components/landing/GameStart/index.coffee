{getRoundTour, getNextTour} = require('lib/weektours')
{GameStartLogos} = require('./GameStartLogos')
{GameStartBadges} = require('./GameStartBadges')
{GameStartActions} = require('./GameStartActions')
{GameStartTourInfo} = require('./GameStartTourInfo')
{MiniStakeRoomList} = require('../../stakerooms/stakerooms')
TournamentInfo = require('./TournamentInfo')
{START_CONFIG} = require('../../../start/start-config')
{eventTracker} = require('lib/utils')


module.exports = React.create
    displayName: 'GameStart'
    args:
        gameType: 'string'
        user: 'object'
        mod: 'string?'

    contextTypes:
        router: React.PropTypes.object

    mixins: [
        flux.Connect('rooms', (store, props) ->
            rooms: flux.stores.rooms.getRooms().filter((room) ->
                    room.roundNumber > 0 and room.gameType == props.gameType and room.isPending())
            allRooms: store.getRooms({gameType: props.gameType})
        )
        flux.Connect('tournaments', (store) ->
            tournaments: store.getTournaments()
        )
    ]

    play: ->
        eventTracker('buttons', "#{@props.gameType}-play")
        @props.onPlay(null, {gameType: @props.gameType})

    getInfo: (gameType) ->
        mapped = @state.rooms
            .map((room) ->
                gamesCount: room.gamesCount
                pot: room.pot
                maxEntries: room.maxEntries
                room: room
            )
            .toList()

        sortBySize = mapped.sort (a, b) ->
            res = b.gamesCount - a.gamesCount
            return res if res

            b.pot - a.pot

        sortByPot = mapped.sort (a, b) ->
            res = b.pot - a.pot
            return res if res

            b.gamesCount - a.gamesCount

        oneOnOneRoom = mapped.sort((a, b) -> a.maxEntries - b.maxEntries).first()?.room or immutable.Map()
        filledRoom = sortBySize.first()?.room or immutable.Map()
        bigPrizeRoom = sortByPot.find((a) -> a.room != filledRoom)?.room or immutable.Map()

        rooms = immutable.List()
        if not filledRoom.isEmpty()
            rooms = rooms.push(filledRoom)
        if not bigPrizeRoom.isEmpty()
            rooms = rooms.push(bigPrizeRoom)
        if not oneOnOneRoom.isEmpty()
            rooms = rooms.push(oneOnOneRoom)

        rooms

    render: ({gameType, friends, user, mod}, {allRooms, tournaments}) ->
        allowToPlay = tournaments.reduce(((reduction, tour)->
            tourRooms = allRooms.filter((room) -> room.isPending() and room.tournamentId == tour.id)
            reduction or not tourRooms.isEmpty()
        ), false)

        CONFIG = START_CONFIG[gameType]
        tour = getRoundTour()
        nextTour = getNextTour()

        rooms = @getInfo()


        cls = 'game '

        `<section className={mod ? cls + mod : cls}>
            {mod == 'vertical' ?
            <div className="game__in">
                <div className="game__top">
                    <GameStartLogos orientation="vertical"
                                    config={START_CONFIG}
                                    allowToPlay={allowToPlay}
                                    gameType={gameType}
                                    play={this.play}/>

                    <GameStartBadges gameType={gameType}
                                     config={START_CONFIG} />
                    <GameStartActions play={this.play}
                                      config={START_CONFIG}
                                      allowToPlay={allowToPlay}
                                      gameType={gameType}/>
                    <GameStartTourInfo tour={tour}
                                       nextTour={nextTour}
                                       config={START_CONFIG}
                                       gameType={gameType}/>
                </div>
            </div>
            :
            <div className="game__in">
                <div className="game__top">
                    <div className="game__l">
                        <GameStartLogos tour={tour}
                                        orientation="horizontal"
                                        gameType={gameType}
                                        allowToPlay={allowToPlay}
                                        config={START_CONFIG}
                                        play={this.play}/>
                    </div>

                    <div className="game__r">
                        <div className="game__desc">
                            <p>
                                {CONFIG.desc}
                            </p>
                        </div>
                        <div className="game__info-table">
                            <MiniStakeRoomList rooms={rooms}/>
                        </div>
                    </div>
                </div>

                <div className="game__mid">
                    <GameStartBadges gameType={gameType}
                                     config={START_CONFIG} />
                </div>

                <div className="game__bottom">
                    <GameStartTourInfo tour={tour}
                        nextTour={nextTour}
                        config={START_CONFIG}
                        gameType={gameType}
                        mod="horizontal"/>
                </div>
            </div>
            }
        </section>`
