_ = require('underscore')
Logo = require('lib/ui/Logo')

exports.GameStartLogos = React.create(
    displayName: 'GameStartLogos'

    render: ({orientation, gameType, play, allowToPlay}, S) ->
        CONFIG = @props.config[gameType]
        logo = CONFIG.logo
        disabled = logo + '-disabled'
        mod = '-big'
        `<div className="game__logo">
            {allowToPlay ?
            <Logo onTap={allowToPlay ? play : _.noop} alt={gameType}
                  mod={CONFIG.name + '-promo'}
                  logo={orientation == 'vertical' ? logo : logo + mod}/>
            :
            <Logo onTap={allowToPlay ? play : _.noop} alt={gameType}
                  mod={CONFIG.name + '-promo'}
                  logo={orientation == 'vertical' ? disabled : disabled + mod}/>
            }
        </div>`
)
