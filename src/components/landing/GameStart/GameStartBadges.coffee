exports.GameStartBadges = React.create
    displayName: 'GameStartBadges'
    args:
        gameType: 'string'

    render: ({gameType, config}, S)->
        CONFIG = config[gameType]
        `<div className="game__badges">
            <div className="game__badge">
                <span className={'game__diff stroke white kelson text-up '
                                + CONFIG.difficultyStyle}>
                    Difficulty
                </span>
                <span className={'badge ' + CONFIG.difficultyColor}>
                    {CONFIG.difficulty}
                </span>
            </div>
            <div className="game__badge">
                <span className={'game__type stroke white kelson text-up '
                                + CONFIG.gameTypeStyle}>
                    Type of game
                </span>
                <span
                    className={'badge ' + CONFIG.gameTypeColor}>
                    {CONFIG.gameType}
                </span>
            </div>
        </div>`
