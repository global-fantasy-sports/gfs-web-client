TournamentCounter = require '../../tournament/TournamentCounter'
{cx} = require '../../../lib/utils'


exports.GameStartTourInfo = React.create
    displayName: 'GameStartTourInfo'
    args:
        tour: 'object?'
        nextTour: 'object?'
        gameType: 'string'

    render: ({tour, nextTour, gameType, mod}, S)->
        CONFIG = @props.config[gameType]
        classSet = cx(
            stroke: true
            "blue": true
            kelson: true
            'x3-medium': !!mod
        )

        isCurrentTourAvailable = tour?.isAllowedToStart() or null
        if isCurrentTourAvailable?
            children = `<div className="game__announce">
                   <p className={classSet}>
                       This games starts in:
                   </p>
                   <TournamentCounter tournament={tour} mod="small"
                                      mode={"round"}
                                      position={mod}/>
                </div>`
        else if nextTour?
            children = `<div className="game__announce">
               <p className="stroke white kelson">
                    Unavailable, new games for
                    &nbsp;{nextTour.name} are up soon
                </p>
            </div>`
        else
            children = `<div className="game__announce">
                <p className="stroke white kelson">
                    There is no information about next tournament.
                </p>
            </div>`

        `<div className="game__tour-info">
          {children}
        </div>`
