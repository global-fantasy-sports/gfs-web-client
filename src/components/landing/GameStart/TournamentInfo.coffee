strftime = require('strftime')
{now} = require('lib/date')

module.exports = React.create
    "displayName": "TournamentInfo"

    ###
    # timeLeft: number. Number of milliseconds to the start of tournament
    ###
    getInitialState: ->
        timeLeft: null

    componentDidMount: ->
        if @props.isNext and @props.startTimestamp
            @timer = setInterval =>
                @setState timeLeft: new Date(@props.startTimestamp) - now()
            , 1000

    ###
    # isNext: boolean. Tournament type
    # isOpen: boolean. Tournament type
    # name: string. Tournament name
    # place: string. Tournament place
    # timeString: string. Formated time string of the tournament
    # startTimestamp: Date. Datetime of the tournament start moment. Need for countdown
    ###
    render: ({isNext, isOpen, name, place, timeString, startTimestamp}) ->
        formatedCountdown = strftime("%A %B %H %M", new Date(@state.timeLeft))
        `<span>{formatedCountdown}</span>`
