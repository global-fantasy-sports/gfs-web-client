{Btn, BtnLink} = require 'lib/ui.coffee'
GuideHint = require 'components/guide/GuideHint'
HintTrigger = require 'components/guide/HintTrigger'


exports.GameStartActions = React.create
    displayName: 'GameStartActions'

    render: ({play, gameType, allowToPlay})->
        `<div className="game__actions">
            <HintTrigger group="home" id="play">
                <Btn onTap={play}
                     mod={allowToPlay ? "red-linear x-medium" : "not-active x-medium"}>
                     Play
                </Btn>
            </HintTrigger>
            <GuideHint children="Click on Play button to start new game."
                       title="Click Play"
                       pointer="top"
                       group="home"
                       id="play"
                       top="68px"
                       right="-10px"/>
            <BtnLink to={"/rules/" + gameType + "/"}
                     mod="green-linear x-medium">
                Rules
            </BtnLink>
        </div>`
