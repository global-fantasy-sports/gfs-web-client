strftime = require('strftime')
{now, calculateDateInterval} = require('lib/date')
{cx} = require('lib/utils')


module.exports = React.create
    displayName: "TournamentPromo"
    args:
        tour: "object?"
        mod: "string?"

    componentDidMount: ->
        if @props.tour
            clearInterval(@timer)
            @timer = setInterval((=> @forceUpdate()), 1000)

    componentWillReceiveProps: (nextProps)->
        if nextProps.tour
            clearInterval(@timer)
            @timer = setInterval((=> @forceUpdate()), 1000)

    componentWillUnmount: ->
        if @props.tour
            clearInterval(@timer)

    calculateTimeLeft: ->
        if @props.tour?.isPending() and @props.tour?.startDate
            startDate = @props.tour.startDate
            timeLeft = new Date(startDate) - now()
            if timeLeft < 0
                return "00D:00H:00M"
            interval = calculateDateInterval timeLeft
            "#{interval.d}D:#{interval.h}H:#{interval.m}M"
        else
            clearInterval(@timer)
            if @props.tour?.isLive()
                ""
            else
                "00D:00H:00M"

    render: ({tour, mod}, S) ->
        logger.log tour, "tournament", "TournamentPromo"
        cls = "tournament-promo "
        stroke = "stroke white kelson"

        timeLeft = @calculateTimeLeft()

        clsTime = cx(
            "tournament-promo__time": true
            "live" : mod == "under-games" and tour and tour.isLive()
            "live-right" : mod == "right-oriented" and tour and tour.isLive()
        )

        if tour and (tour.isThisWeek() or tour.isLive())
            if mod == "under-games"
                caption = `<p className={stroke + " bold big"}>
                        {tour.courseName}
                    </p>`
            else if tour.isPending()
                caption = `<p className={stroke + " bold medium"}>
                        Next tournament starts
                    </p>`
            else
                caption = `<p className={stroke + " bold medium"}>
                        &nbsp;
                    </p>`

            `<section className={mod ? cls + mod : cls}>
                <div className={stroke + " big tournament-promo__name bold"}>
                    <p>{mod == "under-games" ? "Next tournament starts" : tour.name}</p>
                </div>
                <div className="tournament-promo__in">
                    <div className="tournament-promo__content">
                        <div className="tournament-promo__l">
                            {caption}

                            <p className={stroke + " bold medium"}>
                                {tour.courseName},&nbsp;{tour.country}
                            </p>
                        </div>
                        <div className="tournament-promo__r">
                            <p className={stroke + " medium bold offset nowrap"}>
                                {strftime("%I:%M %p %Z %A", tour.startDate)}
                            </p>
                            <p className={stroke + " medium bold nowrap"}>
                                {strftime("%B %d", tour.startDate)}
                                &nbsp;&mdash;&nbsp;
                                {strftime("%d, %Y", tour.endDate)}
                            </p>
                        </div>
                    </div>
                </div>
                <div className={clsTime}>
                    <p>{timeLeft}</p>
                </div>
            </section>`
        else if tour
            `<section className={mod ? cls + mod : cls}>
                <div className={stroke + " big tournament-promo__name offset bold"}>
                    <p>There are no tournaments this week </p>
                </div>
                <div className="tournament-promo__in">
                    <div className="tournament-promo__content">
                        <div className="tournament-promo__l">
                            <p className={stroke}>
                                Next tournament starts
                            </p>
                            <p className={stroke + " medium nowrap"}>
                                {strftime("%B %d", tour.startDate)}
                                &nbsp;&mdash;&nbsp;
                                {strftime("%d, %Y", tour.endDate)}
                            </p>
                        </div>
                        <div className="tournament-promo__r next-tour">
                            <p className={stroke + " medium big bold"}>
                                {tour.name}
                            </p>
                            <p className={stroke + " medium"}>
                                {tour.courseName},&nbsp;{tour.country}
                            </p>
                        </div>
                    </div>
                </div>
                <div className={clsTime}>
                    <p>{timeLeft}</p>
                </div>
            </section>`
        else
            clearInterval(@timer)
            `<section className={mod ? cls + mod : cls}>
                <div className={stroke + " big tournament-promo__name offset bold"}>
                    <p>There are no tournaments in the nearest time </p>
                </div>
                <div className="tournament-promo__in">
                    <div className="tournament-promo__content">
                        <div className="tournament-promo__l">
                            <p className={stroke}>
                                &nbsp;
                            </p>
                            <p className={stroke + " medium"}>
                                &nbsp;
                            </p>
                        </div>
                        <div className="tournament-promo__r">
                            <p className={stroke + " medium big bold"}>
                                &nbsp;
                            </p>
                            <p className={stroke + " medium"}>
                                &nbsp;
                            </p>
                        </div>
                    </div>
                </div>
            </section>`
