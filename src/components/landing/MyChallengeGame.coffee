ItemWrapper = require('../stakerooms/ItemWrapper')
bemClassName = require('bem-classname')
{Btn, BtnExpand, BtnLink, Tokens, Dollars} = require('../../lib/ui')
GameCountdown = require('./GameCountdown')
{cx} = require('lib/utils')
Immutable = require('immutable')

MyChallengeGame = React.create(
    displayName: "MyChallengeGame"

    mixins: [
        flux.Connect('rooms', (store, props) ->
            room: store.getRoom(props.challengeRequest.get('competitionRoomId'))
        )
        flux.Connect('games', (store, props, state) ->
            games: state.room?.getGames() or Immutable.List()
        )
    ]

    componentWillMount: ->
        @state.room.fetchGames({force: true})

    contextTypes:
        router: React.PropTypes.object

    getInitialState: ->
        open: false

    toggleControlButtons: ->
        @setState(open: not @state.open)

    onRemove: ->
        flux.actions.tap.openRemoveChallengeRequestDialog( () =>
            data = {uuid: @props.challengeRequest.get('uuid')}
            connAuth.send('delete', 'remove-challenge-request', data)
        )

    onView: ->
        roomId = @state.room.id
        @context.router.transitionTo("/#{SPORT}/competition/#{roomId}/")

    onAccept: ->
        @props.onPlay(@state.room, {fromChallenge: true})

    render: ({challengeRequest}, {open, room}) ->

        openCls = if open then 'open' else ''

        Money = if room.mode is 'token' then Tokens else Dollars

        expired = false
        if room.started
            expired = true
        if room.maxEntries == room.gamesCount
            expired = expired or @state.games.reduce(((reduction, game) -> reduction and not game.mechanical), true)

        clsOpponent = cx('game-table__opponent', {
            expired: expired
        })

        `<ItemWrapper>
            <section className="game-table__challenge">
                <img src={challengeRequest.get('challengerUserAvatar')} alt="" height="100" width="100"/>
                <div className={clsOpponent}>
                    <div className="data-item data-item--left">
                        <p className="data-item__text data-item__text--white">Challenge from</p>
                        <div className="data-item__name">
                          {challengeRequest.get('challengerUserName')}
                        </div>
                    </div>
                    <img src="/static/img/landing/football-ball-img.png" alt=""/>
                </div>
            </section>

            {!open ?
                <section className="w-23">
                    <div className="data-items data-items--row">
                        <div className="data-item">
                            <p className="data-item__text">Competition</p>
                            <div className="data-item__comp-name">
                              {room.fullName1()}
                            </div>
                        </div>
                    </div>
                </section>
            :   <section className="w-10"/>
            }

            <section className="game-table__data w-40">
                <div className="data-items data-items--row data-items--border-right">
                  {expired ?
                      <div className="data-item">
                        <p className="data-item__text"></p>
                        <div className="data-item__num">
                          Expired
                        </div>
                      </div>
                      : <GameCountdown startDate={room.startDate} /> }
                    <div className="data-item">
                        <p className="data-item__text">Entries</p>
                        <div className="data-item__num">
                          {room.gamesCount}/{room.maxEntries}
                        </div>
                    </div>
                    <div className="data-item">
                        <p className="data-item__text">Entry fee</p>
                        <div className="data-item__num">
                            <Money m={room.entryFee}/>
                        </div>
                    </div>
                    <div className="data-item">
                        <p className="data-item__text">
                            Prize pool
                        </p>
                        <div className="data-item__num">
                            <Money m={room.pot} mod="green"/>
                        </div>
                    </div>
                </div>
                <BtnExpand onTap={this.toggleControlButtons} mod="challenge"/>
            </section>

            <section className={bemClassName("game-table", "buttons", [openCls])}>
                {open || expired ?
                    <Btn mod="square text-red"
                         iconBg={true}
                         icon="trash"
                         onTap={this.onRemove}>
                        Delete
                    </Btn>
                : null}
               {open && !expired ?
                   <Btn mod={"square"}
                         iconBg={true}
                         icon="view-games"
                         onTap={this.onView}>
                       View Games
                   </Btn> : null }
                {!expired ?
                    <Btn mod="square"
                         iconBg={true}
                         icon="green-check"
                         onTap={this.onAccept}>
                        Accept
                    </Btn>
                : null}
            </section>
        </ItemWrapper>`
)

module.exports = MyChallengeGame