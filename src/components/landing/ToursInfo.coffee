_ = require('underscore')
strftime = require('strftime')

{weekStart, weekEnd} = require('lib/date')
{Flag} = require('lib/ui/Flag')
{TourNames} = require('../tournament/TourName')
{TourDates} = require('../tournament/TourDates')
{MiniLeaderboard} = require('../leaderboard/MiniLeaderboard')
TourData = require('../tournament/TourData')
TournamentCounter = require('../tournament/TournamentCounter')
GuideHint = require('../guide/GuideHint')


module.exports = React.create
    displayName: 'ToursInfo'
    args:
        onPlay: 'func'

    getInitialState: ->
        week: 'current'

    selectWeek: (week) ->
        (e) =>
            @setState({week})

    isActiveFilter: (name) ->
        klass = "filter-tabs__item"
        if @state.week == name
            klass += ' active'
        klass

    render: ({onPlay}, {week}) ->
        tournaments = flux.stores.tournaments.getTournaments()
        curWeekStart = weekStart()
        curWeekEnd = weekEnd()
        prevWeekStart = weekStart(curWeekStart - 86400000)
        prevWeekEnd = curWeekStart
        nextWeekStart = curWeekEnd
        nextWeekEnd = weekEnd(curWeekEnd)

        switch week
            when 'prev'
                tournaments = tournaments.filter (t) ->
                    prevWeekStart < +t.startDate < prevWeekEnd and t.isFinished()
            when 'current'
                tournaments = tournaments.filter (t) ->
                    t.isThisWeek()
            when 'next'
                tournaments = tournaments.filter (t) ->
                    nextWeekStart < +t.startDate < nextWeekEnd

        els = tournaments.map (tour) ->
            `<div key={tour.id}>
                <div className="tournament__data">
                    <div className="tournament__data-el f-9">
                        <span className="tour-names__country">
                            <Flag country={tour.country}/>
                        </span>
                    </div>
                    <div className="tournament__data-el f-32">
                        <TourNames tournament={tour}/>
                        <TourDates tournament={tour}/>
                    </div>
                    <div className="tournament__data-el f-32">
                        <TourData tournament={tour} mod="tour-column"/>
                    </div>
                    <div className="tournament__data-el f-27">
                        <TournamentCounter tournament={tour} mode='tour'/>
                    </div>
                </div>
                <div className="tournament__leaderboard">
                    <MiniLeaderboard tournament={tour}/>
                </div>
            </div>`


        `<div className="tournament">
            <div className="tournament__tabs">
                <ul className="filter-tabs">
                    <li className={this.isActiveFilter('prev')}
                        onTap={this.selectWeek('prev')}>Prev week</li>
                    <li className={this.isActiveFilter('current')}
                        onTap={this.selectWeek('current')}>This week</li>
                    <li className={this.isActiveFilter('next')}
                        onTap={this.selectWeek('next')}>Next week</li>
                </ul>
            </div>
            <div className="tournament__content">
                {!els.isEmpty() ?
                    els.toArray()
                    :
                <div className="tournament__data sorry">
                    <p className="stroke white x-big bold">
                        There are no tournaments
                    </p>
                </div>
                    }
            </div>
            <GuideHint children="Here you can see information about the tournament."
                       title="Tournament information"
                       group="home"
                       id="tour"
                       scrollTo="competitions"
                       bottom="-170px"
                       left="0"
                       pointer="top"/>
        </div>`

