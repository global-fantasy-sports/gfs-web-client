{Icon, Btn} = require('../../lib/ui')
GameOverviewItem = require('../landing/GameOverviewItem')
NextGameInfo = require('../landing/NextGameInfo')
MyChallengeGame = require('../landing/MyChallengeGame')
{cx, eventTracker} = require('../../lib/utils')


PLAY_BUTTON_LABEL =
    if SPORT is 'golf'
        'Play Golf'
    else if SPORT is 'nfl'
        'Play Football'
    else if SPORT is 'nba'
        'Play Basketball'
    else
        'Play'


MyGamesFilters = ({value, onFilter}) ->
    isActive = (name) ->
        if value is name
            'blue-base-active'
        else
            'blue-base'

    `<div className="my-games__filter-buttons">
        <Btn mod={isActive('all')} onTap={onFilter.bind(null, 'all')}>All games</Btn>
        <Btn mod={isActive('soon')} onTap={onFilter.bind(null, 'soon')}>Soon</Btn>
        <Btn mod={isActive('live')} onTap={onFilter.bind(null, 'live')}>Live</Btn>
        <Btn mod={isActive('archived')} onTap={onFilter.bind(null, 'archived')}>Archived</Btn>
    </div>`


MyGamesLegend = () ->
    if SPORT is 'nfl'
        `<div className="wizard-legend">
            <div className="wizard-legend__step">
                <span className="wizard-legend__num">1</span>
                <span className="wizard-legend__text">Draft <br/> players</span>
            </div>
            <div className="wizard-legend__arrow">
                <Icon i="legend-arrow" />
            </div>
            <div className="wizard-legend__step">
                <span className="wizard-legend__num">2</span>
                <span className="wizard-legend__text">Entry <br/> fee</span>
            </div>
            <div className="wizard-legend__arrow">
                <Icon i="legend-arrow" />
            </div>
            <div className="wizard-legend__step">
                <span className="wizard-legend__num">3</span>
                <span className="wizard-legend__text">Invite <br/> friends</span>
            </div>
        </div>`
    else if SPORT is 'nba'
        `<div className="wizard-legend">
            <div className="wizard-legend__step">
                <span className="wizard-legend__num">1</span>
                <span className="wizard-legend__text">Draft <br/> players</span>
            </div>
            <div className="wizard-legend__arrow">
                <Icon i="legend-arrow" />
            </div>
            <div className="wizard-legend__step">
                <span className="wizard-legend__num">2</span>
                <span className="wizard-legend__text">Entry <br/> fee</span>
            </div>
            <div className="wizard-legend__arrow">
                <Icon i="legend-arrow" />
            </div>
            <div className="wizard-legend__step">
                <span className="wizard-legend__num">3</span>
                <span className="wizard-legend__text">Invite <br/> friends</span>
            </div>
        </div>`
    else if SPORT is 'golf'
        `<div className="wizard-legend">
            <div className="wizard-legend__step">
                <span className="wizard-legend__num">1</span>
                <span className="wizard-legend__text">Choose <br/> round</span>
            </div>
            <div className="wizard-legend__step">
                <span className="wizard-legend__num">2</span>
                <span className="wizard-legend__text">Pick <br/> golfers</span>
            </div>
            <div className="wizard-legend__step">
                <span className="wizard-legend__num">3</span>
                <span className="wizard-legend__text">Build <br/> strategy</span>
            </div>
            <div className="wizard-legend__step">
                <span className="wizard-legend__num">4</span>
                <span className="wizard-legend__text">Pay entry <br/> fee</span>
            </div>
            <div className="wizard-legend__step">
                <span className="wizard-legend__num">5</span>
                <span className="wizard-legend__text">Challenge <br/> friends</span>
            </div>
        </div>`
    else
        `<div />`


class MyGamesList extends React.Component
    constructor: () ->
        super()

        @state = {
            showMore: false
        }

    handleMoreClick: () =>
        @setState({showMore: not @state.showMore})

    render: () ->
        {games, onEditGame, filter} = @props
        {showMore} = @state

        totalGames = games.size
        games = games.toIndexedSeq()

        if not showMore
            games = games.take(10)

        content = games.map((game) ->
            `<GameOverviewItem
                key={game.id}
                game={game}
                status={filter}
                onEditGame={onEditGame}
            />`
        )

        `<div className="my-games__tables">
            {content}
            {totalGames > 10 &&
                <div className="my-games__more">
                    <Btn
                        mod="small green"
                        onTap={this.handleMoreClick}
                    >
                        {showMore ? 'Show less…' : 'Show more…'}
                    </Btn>
                </div>
            }
        </div>`


MyChallengeGameList = React.create(
    displayName: "MyChallengeGameList"

    mixins: [
        flux.Connect('challenges', (store) -> challengeRequests: store.getChallenges())
    ]

    render: ({}, {challengeRequests}) ->

        this_ = @
        requests = challengeRequests.map((c, i) ->
            `<MyChallengeGame key={i} challengeRequest={c} onPlay={this_.props.onPlay} />`
        ).toArray()

        `<section>
          {requests}
        </section>`
)


MyGamesOverview = React.create(
    displayName: 'MyGamesOverview'

    mixins: [
        flux.Connect('games', (store) -> games: store.getMyGames())
        flux.Connect('state', (store) -> filter: store.getGamesFilter())
        flux.Connect('rooms', (store, props, state) ->
            roomIds = state.games?.map((g) -> g.competitionRoomId).toArray() or []
            filter = (r) ->
                r.id in roomIds

            {
                isAllStarted: store.isAllStarted()
                nextGame: store.getNextGame()
                rooms: store.getRooms([], [filter])
            }
        )
    ]

    selectFilter: (filter) ->
        flux.actions.state.setGamesFilter(filter)

    playGame: () ->
        gameType = if SPORT is 'golf' then 'five-ball' else 'salarycap'
        eventTracker('buttons', "#{gameType}-play")
        @props.onPlay(null, {gameType})

    render: ({onEditGame}, {filter, games, isAllStarted, nextGame}) ->
        filtered = {
            all: games.filter((game) -> game.isRecent())
            archived: games.filter((game) -> game.isFinished())
            live: games.filter((game) -> game.isLive())
            soon: games.filter((game) -> game.isPending())
        }

        activeGames = filtered[filter].size

        playButtonMods = cx({
            disabled: isAllStarted
            red: !isAllStarted
            gray: isAllStarted
        })


        `<div className="my-games">
            <div className={'my-games__top my-games__top--' + SPORT}>
                <div className="my-games__filters">
                    <MyGamesFilters
                        value={filter}
                        onPlayGame={this.playGame}
                        onFilter={this.selectFilter}
                    />
                    <Btn mod={playButtonMods} onTap={this.playGame}>
                        {PLAY_BUTTON_LABEL}
                    </Btn>
                </div>

                {activeGames === 0 && <MyGamesLegend />}
            </div>

            {activeGames === 0
                ? <NextGameInfo date={nextGame ? nextGame.startDate : 0} />
                : null}
            <div>
                <MyChallengeGameList onPlay={this.props.onPlay} />
                {activeGames != 0 ?
                    <MyGamesList
                        games={filtered[filter]}
                        status={filter}
                        onEditGame={onEditGame} />
                    : null}
            </div>
        </div>`
)

module.exports = MyGamesOverview
