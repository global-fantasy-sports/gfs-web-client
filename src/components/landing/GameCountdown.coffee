moment = require('moment')
Connect = require('../../flux/connectMixin')
{pad, dateFmt, timeFmt} = require('../../lib/utils')

module.exports = React.create(
    displayName: "GameCountdown"

    mixins: [
        Connect('time', (store, {startDate}) -> {
            diff: store.getDiffToNow(startDate)
        })
    ]

    render: ({startDate}, {diff}) ->
        diff = moment.duration(diff)
        isSoon = diff.days() < 1

        `<div className="data-item" title={moment(startDate).utc().format('LLL')}>
            <div className="data-item__text">{isSoon ? 'Starts in' : 'Starts at'}</div>
            {isSoon
                ? <div className="data-item__time">{pad(diff.hours())}:{pad(diff.minutes())}:{pad(diff.seconds())}</div>
                : <div className="data-item__date">
                <span className="nowrap">{dateFmt(startDate)}</span> <span className="nowrap">{timeFmt(startDate)}</span>
            </div>
            }
        </div>`
)
