{Icon} = require('../../lib/ui')


GamesScheduleDay = ({day, date}) ->
    items = day.map((game) ->
        if game.status == 'scheduled'
            panel = `<p className="games-schedule__time">
              <span>{game.formatTime('h:mm A z')}</span>
            </p>`
        else
            panel = `<div className="games-schedule__result">
              <div className="games-schedule__scores"><p>{game.homeScore}</p><p>{game.awayScore}</p></div>
              <div className="games-schedule__status">FINAL</div>
            </div>`
        `<div className="games-schedule__item" key={game.id}>
            <div className="games-schedule__teams">
                <div className="games-schedule__team-item">
                    <img src={game.homeTeamPhoto} className="games-schedule__img"/>
                    <p className="games-schedule__team-abbr">{game.homeTeam}</p>
                </div>
                <div className="games-schedule__team-item">
                    <img src={game.awayTeamPhoto} className="games-schedule__img"/>
                    <p className="games-schedule__team-abbr">{game.awayTeam}</p>
                </div>
            </div>
          {panel}
        </div>`
    )

    gamesCounts = if day.size == 1 then day.size + " game" else day.size + " games"

    `<div className="games-schedule__day">
        <div className="games-schedule__top">
            <div>
                <p className="games-schedule__date">{date}</p>
                <p className="games-schedule__game-count">({gamesCounts})</p>
            </div>
        </div>
        <div className="games-schedule__items-wrap">
            {items}
        </div>
    </div>`


GamesSchedule = React.create(
    displayName: "GamesSchedule"

    renderContests: ->
        @props.contestsByDay.toIndexedSeq().map((day) ->
            date = day.first().getDay()
            `<GamesScheduleDay
                key={date}
                day={day}
                date={date}
            />`
        ).toArray()

    render: ({contestsByDay}) ->

        contest = @renderContests()
        if contest.length == 0
            return `<div className="games-schedule__no-games">There are no games at this time</div>`

        `<div className="games-schedule">
            <div className="games-schedule__content">
                <div className="games-schedule__icon-left"><Icon i="nfl-arrow-left"/></div>
                <div className="games-schedule__inner">
                    {contest}
                </div>
                <div className="games-schedule__icon-right"><Icon i="nfl-arrow-right"/></div>
            </div>
        </div>`
)

module.exports = GamesSchedule
