{Icon} = require('lib/ui')
{bem, cx} = require('lib/utils')
{Flag} = require('lib/ui/Flag')


GolfLeaderboardRow = React.create(
    displayName: "GolfLeaderboardRow"

    mixins: [flux.Connect("participantRoundResults", (store, props) ->
        participant_id = props.participant.id

        score1: store.getResult({participant_id, round_number: 1}).getScore()
        score2: store.getResult({participant_id, round_number: 2}).getScore()
        score3: store.getResult({participant_id, round_number: 3}).getScore()
        score4: store.getResult({participant_id, round_number: 4}).getScore()
    )]

    getScore: ->
        v = @props.participant.getScore()
        if v > 0
            "+#{v}"
        else
            "#{v}"

    onTap: ->
        flux.actions.state.openPlayerDetailsDialog(@props.participant.id)

    render: ({participant, idx}, {score1, score2, score3, score4}) ->
        itemCell = bem("leaderboard__item-cell")

        `<div className="leaderboard__item" onTap={this.onTap}>
            <div className={itemCell("pos") + " w-5"}>{idx + 1}</div>
            <div className={itemCell() + " w-10"}><Flag country={participant.country}/></div>
            <div className={itemCell("name") + " w-20"}>{participant.getFullName()}</div>
            <div className={itemCell() + " w-10"}> {participant.rank}</div>
            <div className={itemCell() + " w-5"}>7:20 <span>AM</span></div>
            <div className={itemCell() + " w-5"}>{score1}</div>
            <div className={itemCell() + " w-5"}>{score2}</div>
            <div className={itemCell() + " w-5"}>{score3}</div>
            <div className={itemCell() + " w-5"}>{score4}</div>
            <div className={itemCell() + " w-10"}>{this.getScore()}</div>
            <div className={itemCell() + " w-10"}>N/A</div>
            <div className={itemCell() + " w-10"}>N/A</div>
        </div>`
)

module.exports = React.create(
    displayName: "GolfLeaderboard"

    mixins: [
        flux.Connect("participants", (store, props, state) ->
            participants: store.getLeaderboard(state.tournamentId)
        ),
        flux.Connect("tournaments", (store) ->
            tournaments: store.getTournamentsAllowedToStart()
        )]

    getInitialState: ->
        tournamentId: null

    componentDidMount: ->
        tour = @state.tournaments.first()
        if @state.tournamentId == null and tour
            @setState(tournamentId: tour.id)

    selectTournamentBoard: (id) ->
        =>
            @setState(tournamentId: id)

    render: ({}, {participants, tournaments, tournamentId}) ->

        tournament = flux.stores.tournaments.getTournament(+tournamentId)
        currentRound = tournament.getCurrentRoundNumber()
        sortedParticipants = participants.sortBy((p) -> p.getScore())


        self = this
        tour = tournaments.map((tour, i) =>
            tourClass = cx({
                "leaderboard__tournament": true
                "leaderboard__tournament--active": tour.id == @state.tournamentId
            })

            `<div className={tourClass} key={i} onTap={self.selectTournamentBoard(tour.id)}>
                <p className="leaderboard__tour-name">{tour.name}</p>
                <p className="leaderboard__tour-info">{tour.courseName}</p>
                <p>
                    <span className="leaderboard__purse">PURSE:  </span>
                    <span className="leaderboard__purse-count">${tour.purse}</span>
                </p>
            </div>`
        )

        golfer = sortedParticipants.toList().take(20).map((p, i) ->
            `<GolfLeaderboardRow participant={p} key={p.id} idx={i}
                                 currentRound={currentRound}/>`
        )


        `<section className="leaderboard">
            <section className="leaderboard__left">
                <div className="game-table game-table--header">
                    <div className="flex-row-center w-5">POS</div>
                    <div className="flex-row-center w-10">COUNTRY</div>
                    <div className="flex-row-start-center in-20 w-20">NAME</div>
                    <div className="flex-row-center w-10">TOTAL</div>
                    <div className="flex-row-center w-5">THRU</div>
                    <div className="flex-row-center w-5">R1</div>
                    <div className="flex-row-center w-5">R2</div>
                    <div className="flex-row-center w-5">R3</div>
                    <div className="flex-row-center w-5">R4</div>
                    <div className="flex-row-center w-10">STROKES</div>
                    <div className="flex-row-center w-10">FEDEXCUP</div>
                    <div className="flex-row-center w-10">STARTING</div>
                </div>
                <div className="leaderboard__golfers-list">
                    {golfer}
                </div>
            </section>
            <section className="leaderboard__right">
                <div className="leaderboard__tours-list">
                    {tour}
                </div>
            </section>
        </section>`
)
