_ = require 'underscore'
GameStart = require "./GameStart"
{Btn} = require 'lib/ui'
TournamentPromo = require './TournamentPromo'
GamesBanners = require './GamesBanners'
{getRoundTour, getNextTour} = require 'lib/weektours'
GuideHint = require 'components/guide/GuideHint'
HintTrigger = require 'components/guide/HintTrigger'
{GAMES, playButtonNameByType} = require 'lib/const'


module.exports = React.create
    displayName: 'PlayerDashboard'
    args:
        user: 'object'

    mixins: [
        flux.Connect("tournaments", (store) =>
            tournaments: store.getTournaments()
        )
    ]

    getInitialState: ->
        game: null
        gamesMod: 'horizontal'
        gameType: 'five-ball'

    contextTypes:
        router: React.PropTypes.object

    toggleDash: (game, url) ->
        (e) =>
            @setState(game: game)
            if @state.dashMod == 'select-game'
                @setState(dashMod: 'tutorial')
            else
                @setState(dashMod: 'select-game')
            @context.router.transitionTo("/#{SPORT}/#{url}/#{game}/")
            @forceUpdate()

    toggleGamesMod: (mod) ->
        (e) =>
            @setState(gamesMod: mod)

    selectGame: (game) ->
        (e) =>
            @setState(gameType: game)

    nextGame: ->
        if @state.gameType == 'five-ball'
            @setState(gameType: "handicap-the-pros")
        else if @state.gameType == "handicap-the-pros"
            @setState(gameType: "hole-by-hole")
        else if @state.gameType == "hole-by-hole"
            @setState(gameType: 'five-ball')

    prevGame: ->
        if @state.gameType == 'five-ball'
            @setState(gameType: "hole-by-hole")
        else if @state.gameType == "handicap-the-pros"
            @setState(gameType: 'five-ball')
        else if @state.gameType == "hole-by-hole"
            @setState(gameType: "handicap-the-pros")

    isActiveFilter: (name) ->
        cls = "filter-tabs__item"
        if @state.gameType == name
            cls += ' active'
        cls

    render: ({user, onPlay}, {game, gamesMod, gameType}) ->

        allGames = for g in GAMES
            `<GameStart gameType={g} mod="vertical"
                        user={user} onPlay={onPlay} />`
        oneGame = `<GameStart gameType={gameType} mod="horizontal"
                              user ={user} onPlay={onPlay} />`

        nextTour = getRoundTour() or getNextTour()
        logger.log nextTour, "nextTour", "PlayerDashboard"

        # wrappers classes
        dashCls = 'player-dash '
        gamesCls = 'games '

        # partials
        switcher = `<div className="games__switcher-wrap">
            <div className="games__orient-switcher">
                <Btn mod="games-horizontal" onTap={this.toggleGamesMod('vertical')}
                    activeState={gamesMod == 'vertical' ? 'active' : null}/>
                <Btn mod="games-vertical" onTap={this.toggleGamesMod('horizontal')}
                    activeState={gamesMod == 'horizontal' ? 'active' : null}/>
            </div>
        </div>`

        filters = `<ul className="filter-tabs narrow dark kelson">
            <li className={this.isActiveFilter('five-ball')}
                onTap={this.selectGame('five-ball')}>{playButtonNameByType['five-ball']}</li>
            <li className={this.isActiveFilter("handicap-the-pros")}
                onTap={this.selectGame("handicap-the-pros")}>{playButtonNameByType['handicap-the-pros']}</li>
            <GuideHint children="Click on tab to choose type of game."
                       title="Click on tab"
                       pointer="left"
                       group="home"
                       id="tabs"
                       top="-13px"
                       right="-50px"/>
            <HintTrigger group="home" id="tabs">
                <li className={this.isActiveFilter("hole-by-hole")}
                    onTap={this.selectGame("hole-by-hole")}>{playButtonNameByType['hole-by-hole']}</li>
            </HintTrigger>
        </ul>`

        # different layout and content positioning for horizontal
        # and vertical orientation of game selector
        if gamesMod == 'vertical'
            topContent = `<div className="games__top">
                   {allGames}
            </div>`
            bottomContent = `<div className="games__bottom">
                {nextTour ?
                <TournamentPromo mod="under-games" tour={nextTour}/>
                : null}
                <GamesBanners  />
            </div>`
        else if gamesMod == 'horizontal'
            topContent = `<div className="games__top">
                <div className="games__in">
                    <div onTap={this.prevGame}
                         className="games__left-arrow">
                         <span></span>
                     </div>
                    <div className="games__l">
                        <div className="games__filters">
                            {filters}
                        </div>
                        {oneGame}
                    </div>
                    <div className="games__r">
                    <TournamentPromo mod="right-oriented" tour={nextTour}/>
                    </div>
                    <div onTap={this.nextGame}
                         className="games__right-arrow">
                         <span></span>
                     </div>
                </div>
            </div>`
            bottomContent = `<div className="games__bottom">
                <GamesBanners />
            </div>`

        `<section className={dashCls}>
            <section className={gamesCls + gamesMod}>
                {switcher}
                {topContent}
                {bottomContent}
            </section>
        </section>`
