GuideHint = require('components/guide/GuideHint')
Portal = require('react-overlays/lib/Portal')
{Link} = require('../../lib/ui')

module.exports = React.create
    displayName: 'GamesBanners'

    render: ->
        `<section className="games-banners">
            <span className="games-banners__el">
                <Link to={"/" + SPORT + "/competitions/"}>
                    <img src="/static/img/b-invite-friends.png"
                         alt="Play against your friends in one of our exciting competition rooms"/>
                </Link>
            </span>
            <span className="games-banners__el">
                <Link to={"/" + SPORT + "/competitions/"}>
                    <img src="/static/img/b-500.png"
                         alt="Play this week for a share of the prize pool"/>
                </Link>
            </span>
            <span className="games-banners__el">
                <Link to={"/" + SPORT + "/competitions/"}>
                    <img src="/static/img/b-hole-by-hole.png"
                         alt="You have a good chance with prizes for at least 7 participants"/>
                </Link>
            </span>
            <Portal container={document.body}>
                <GuideHint children="Click here to see more."
                           title="See more"
                           arrow="down"
                           scrollTo="games"
                           group="home"
                           id="seemore1"
                           bottom="0px"
                           pos="absolute"
                />
            </Portal>
        </section>`

GamesBannerFriend = GamesBannerFriend = React.create
    displayName: 'GamesBannerFriend'

    render: ->
        `<section className="banner">
            <img src="/static/img/b-invite-friends.png"
                 alt="Play against your friends in one of
                 our exciting competition rooms"/>
        </section>`

GamesBannerPrize = GamesBannerPrize = React.create
    displayName: 'GamesBannerPrize'

    render: ->
        `<section className="banner">
            <img src="/static/img/b-500-000.png"
                     alt="Play this week for a share of the prize pool"/>
        </section>`

GamesBannerParticipant = GamesBannerParticipant = React.create
    displayName: 'GamesBannerParticipant'

    render: ->
        `<section className="banner">
            <img src="/static/img/b-hole-by-hole.png"
                     alt="You have a good chance with prizes
                     for at least 7 participants"/>
        </section>`
