_ = require('underscore')
{cx} = require('../lib/utils')
{Btn} = require('../lib/ui')


ToggleButton = ({value, label, onClick, active}) ->
    mod = if active then 'blue-base-active' else 'blue-base'
    handleClick = () -> onClick(value)
    `<Btn mod={mod} onClick={handleClick}>
        {label || value}
    </Btn>`


module.exports = React.create(
    displayName: 'ParticipantFilter'

    propTypes: {
        values: React.PropTypes.arrayOf(React.PropTypes.string).isRequired
        count: React.PropTypes.number
        active: React.PropTypes.string.isRequired
        onChange: React.PropTypes.func.isRequired
    }

    getDefaultProps: -> {
        active: 'All'
    }

    render: () ->
        {active, className, count, values, onChange} = @props
        isFavorite = active == 'FAV'

        filters = values.map((item, index) =>
            `<ToggleButton
                key={index}
                value={item}
                active={active === item}
                onClick={onChange}
            />`
        )

        `<nav className={cx('participant-filter', className)}>
            <ToggleButton
                value="All"
                onClick={onChange}
                active={!active || active === 'All'}
            />
            {filters}
            <ToggleButton
                label={(isFavorite ? '★' : '☆') + ' FAV'}
                value={isFavorite ? 'All' : 'FAV'}
                onClick={onChange}
                active={isFavorite}
            />
        </nav>`
)
