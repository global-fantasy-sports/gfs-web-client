strftime = require('strftime')
{cx} = require('lib/utils')


module.exports = React.create
    displayName: 'TourData'
    args:
        tournament: 'object'

    render: ({tournament, mod}) ->
        cls = cx(
            "data-items data-items--column": true
            "tour-column": mod == "tour-column"
        )

        `<div className="tour-data">
            <div className={cls}>
                <p className="data-item">
                    Purse: <b>${tournament.purse}</b>
                </p>
                <p className="data-item">
                    Par: <b>{tournament.courseRating}</b>
                </p>
                <p className="data-item">
                    Deadline:&nbsp;
                    <b>{strftime('%a, %B %d %H:%M', tournament.startDate)}</b>
                </p>
            </div>
        </div>`
