exports.TournamentResults = React.create
    displayName: 'TournamentResults'

    componentDidUpdate: (node) ->
        for k, ref of @refs when k.match(/current/)
            node = ref.getDOMNode()
            # why offsetParent is table, not fixedTable?
            table = node.offsetParent.parentNode
            if node.offsetLeft >= table.offsetWidth
                table.scrollLeft = node.offsetLeft - 1

    render: ({children}) ->
        `<div className="tour-results">
            {children}
         </div>`
