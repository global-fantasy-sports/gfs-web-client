_ = require('underscore')
Immutable = require('immutable')
moment = require('moment')

Connect = require('../../flux/connectMixin')
{cx, pad, parseDec} = require('../../lib/utils')

MIN = 60
HOUR = 3600
DAY = HOUR * 24


module.exports = React.create(
    displayName: 'TournamentCounter'

    mixins: [
        Connect('time', (store) -> {now: store.getNow()})
    ]

    render: ({tournament, text, type, mode, mod, position}, {now}) ->
        tour_state = ''

        date = tournament.roundDate if tournament.roundDate

        if mode in ["stake_size", "stake_room"] and date
            date = moment(date)
            if date.diff(now, 'days') > 0
                nextDate = moment(date).add(1, 'day')
                return `<div className="countdown big">
                  {date.format('DD MMM')} - {nextDate.format('DD MMM')}
                </div>`
        else if mode == "round"
            date = null

            rounds = tournament.rounds
            for n in [1..4]
                round = rounds.get(n, Immutable.Map())
                if round.get("started", false) == false
                    date = round.get("start_time_utc")
                    break
        else
            if tournament
                if tournament.isLive()
                    tour_state = 'live'
                else if tournament.isPending()
                    tour_state = 'pending'
                    date = tournament.startDate
            else
                tour_state = 'no next tournament'

        date = moment(date)
        diff = moment.duration(if date.isAfter(now) then date.diff(now) else 0)
        days = diff.get('days')

        modify = {
            "small": true
            "countdown": true
        }
        modify[mod] = !!mod
        classes = cx(modify)

        showCountDown = tour_state is 'pending' or mode in ["round", "stake_room"]

        `<div className={classes}>
            { showCountDown ?
            <span className="countdown__counter">
                    {text ?
                    <span className="countdown__text">starts in: </span>
                        : null}
                    <span className="countdown__els">
                        {days ?
                        <span className="countdown__el">
                            {days}
                            <small>d</small>
                        </span>
                            : null}
                        <span className="countdown__el">
                            {pad(diff.get('hours'))}
                            <small>:</small>
                        </span>
                        <span className="countdown__el">
                            {pad(diff.get('min'))}
                            <small>:</small>
                        </span>
                        <span className="countdown__el">
                            {pad(diff.get('sec'))}
                        </span>
                    </span>
                </span>
                : null }
            { tour_state == 'live' ?
            <img src="/static/img/Live-on-tournament.png"/>
                : null }
        </div>`
)
