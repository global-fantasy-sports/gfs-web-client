_ = require('underscore')
{cx} = require('lib/utils')
{strokeType} = require('lib/const')
TournamentLegend = require('./TournamentLegend')

getCurrentStep = (participantRoundResult) ->
    participantRoundResult.get("strokes")?.size or -1

exports.MemberTable = React.create
    displayName: "MemberTable"

    render: ({game, participantRoundResult, points, strokeClass, pointClass, bonus}, S) ->
        {pars, yardages, strokes} = participantRoundResult

        strokeClass or= _.noop

        if not pointClass # why???
            pointClass = (i) ->
                return "" if not strokes[i]
                strokeType(strokes[i] - pars[i])

        currentStep = getCurrentStep(participantRoundResult)

        index = for i in [0..17]
            if i == currentStep
                `<td key={i} className="data-table__el current">
                     <span className="data-table__i">
                        {i + 1}
                     </span>
                </td>`
            else
                `<td className={"data-table__el " +
                                 cx({past: i < currentStep})}>
                    <span className="data-table__i">
                        {i + 1}
                    </span>
                </td>`
        index.push `<td key={"total"} className="data-table__total">Total</td>`

        parsTotal = 0
        parsRow = for i in [0..17]
            parsTotal += pars[i]
            `<td key={i} className={"data-table__el " +
                            cx({current: i == currentStep,
                                past: i < currentStep})}>
                <span className="data-table__i">
                    {pars[i]}
                </span>
            </td>`
        parsRow.push `<td key={"total"} className="data-table__total">{parsTotal}</td>`

        strokesTotal = 0
        strokesRow = for i in [0..17]
            strokesTotal += strokes[i] if strokes[i]
            className = "data-table__el " + strokeType(strokes[i] - pars[i]) + " " +
                            cx
                                current: i == currentStep
                                past: i < currentStep
            `<td key={i} className={className}>
                <span className="data-table__i">
                    {strokes[i] || "-"}
                </span>
            </td>`
        strokesRow.push `<td key={"total"} className="data-table__total">{strokesTotal}</td>`

        if bonus
            handicaps = for i in [0..17]
                className = "data-table__el " + strokeClass(i) + " " +
                    cx
                        current: i == currentStep
                        past: i < currentStep
                `<td key={i} className={className}>
                    <span className="data-table__i">
                        {+bonus[i]? -1 : "-"}
                    </span>
                </td>`
            handicaps.push `<td key={"total"} className="data-table__total">-</td>`

        if points
            pointsTotal = 0
            pointsRow = for i in [0..17]
                p = points[i]
                pointsTotal += p if p
                if p == undefined
                    p = "-"
                if p > 0
                    p = "+#{p}"
                cls = pointClass(i)
                `<td key={i} className={"data-table__el " +  cls + " " +
                                cx({current: i == currentStep})}>
                    <span className="data-table__i">
                        {p}
                    </span>
                </td>`
            pointsRow.push `<td key={"total"} className="data-table__total">{pointsTotal}</td>`

        `<div className="scorecard-table">
            <div className="scorecard-table__prop">
                <table className="data-table blue-gray text-up color-white">
                    <tr className="data-table__row">
                        <td className="data-table__el">
                            <span className="data-table__i">
                                hole
                            </span>
                        </td>
                    </tr>
                    <tr className="data-table__row">
                        <td className="data-table__el">
                        <span className="data-table__i">
                            par
                        </span>
                        </td>
                    </tr>
                    <tr className="data-table__row">
                        <td className="data-table__el">
                            <span className="data-table__i">
                                stroke
                            </span>
                        </td>
                    </tr>

                    {game && game.gameType == 'handicap-the-pros' ?
                    <tr className="data-table__row">
                        <td className="data-table__el">
                            <span className="data-table__i">
                                handicap
                            </span>
                        </td>
                    </tr>
                    : null }

                    {pointsRow ?
                    <tr className="data-table__row">
                        <td className="data-table__el">
                            <span className="data-table__i">
                                m.stf. points
                            </span>
                        </td>
                    </tr>
                    : null}

                    <tr className="data-table__row">
                        <td className="data-table__el legend-height">
                            <span className="data-table__i">
                                legend
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div className="scorecard-table__val">
                <table className="data-table">
                    <tr className="data-table__row">
                        {index}
                    </tr>
                    <tr className="data-table__row pars">
                        {parsRow}
                    </tr>
                    <tr className="data-table__row strokes">
                        {strokesRow}
                    </tr>
                    {game && game.gameType == 'handicap-the-pros' ?
                    <tr className="data-table__row">
                            {handicaps}
                    </tr>
                    : null}
                    {pointsRow ?
                    <tr className="data-table__row points">
                        {pointsRow}
                    </tr>
                    : null}
                 </table>
                 <TournamentLegend/>
            </div>
        </div>`
