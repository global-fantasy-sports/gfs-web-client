exports.TournamentLive = React.create
    displayName: 'TournamentLive'
    mixins: [flux.Connect("state", (store) ->
        roundNumber: store.getActiveRoundNumber()
    )]

    render: ({game, gameProperties, participant}, {roundNumber}) ->
        {ResultsView} = gameProperties

        `<section className="team-results">
            <div className="team-results__tables">
                <ResultsView roundNumber={roundNumber} game={game}
                             participant={participant}  {...this.props} />
            </div>
        </section>`
