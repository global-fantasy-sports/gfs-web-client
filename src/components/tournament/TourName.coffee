{cx} = require('lib/utils')
{dateFmt} = require('lib/utils')

exports.TourName = React.create
    displayName: 'TourName'

    render: ({tournament, active}) ->
        if not tournament
            `<div/>`

        datesCls = cx(
            "tour-dates": true
            "tour-dates--active": active
        )
        nameCls = cx(
            "tour-name"
            "tour-name--active": active
        )

        `<div>
            <div className={datesCls}>
                <span>{dateFmt(tournament.startDate)}</span>
                <span> &mdash; </span>
                <span>{dateFmt(tournament.endDate)}</span>
            </div>
            <div className={nameCls}>
                <p className="tour-name__name">
                    {tournament.name}
                </p>
                <p className="tour-name__course">
                    {tournament.courseName}
                </p>
            </div>
        </div>`
