module.exports = React.create(
    displayName: "TourInfo"

    render: ({tournament, mod}) ->
        cls = "tour-info "
        `<div className={mod ? cls + mod : cls}>
            <div>
                <p className="tour-info__count">${tournament.purse}</p>
                <p className="tour-info__title">PURSE</p>
            </div>
            <div>
                <p className="tour-info__count">${tournament.winningShare}</p>
                <p className="tour-info__title">WINNING SHARE</p>
            </div>
            <div>
                <p className="tour-info__count">{tournament.fedexPoints}</p>
                <p className="tour-info__title">FEDEX POINTS</p>
            </div>
        </div>`
)
