exports.TourCourseRating = React.create
    displayName: 'TourCourseRating'
    args:
        m: 'object'
        mod: 'string?'

    render: ({m, mod}) ->
        cls = 'tour-course-rating '
        `<div className={mod ? cls + mod : cls}>
            <p className="tour-course-rating__num">{m.courseRating()}</p>
            <p className="tour-course-rating__text">Course rating</p>
        </div>`
