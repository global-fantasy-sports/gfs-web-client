{MemberTable} = require('./MemberTable')

getCurrentStep = (participantRoundResult) ->
    participantRoundResult.get("strokes")?.size or -1


exports.TournamentMember = React.create
    displayName: 'TournamentMember'

    render: ({roundNumber, participantRoundResult, game, bonus, points,
              strokeClass, pointClass}, S) ->

        currentStep = getCurrentStep(participantRoundResult)

        `<div className="scorecard team-results">
                <MemberTable roundNumber={roundNumber}
                             participantRoundResult={participantRoundResult}
                             game={game}
                             points={points}
                             bonus={bonus}
                             currentStep={currentStep}
                             strokeClass={strokeClass}
                             pointClass={pointClass} />
         </div>`
