{dateFmt} = require('lib/utils')

exports.TourDates = React.create
    displayName: 'TourDates'

    render: ({tournament, mod}) ->
        if not tournament
            `<div/>`

        cls = 'tour-dates '
        `<div className={mod ? cls + mod : cls}>
            <span>{dateFmt(tournament.startDate)}</span>
            <span> &mdash; </span>
            <span>{dateFmt(tournament.endDate)}</span>
        </div>`
