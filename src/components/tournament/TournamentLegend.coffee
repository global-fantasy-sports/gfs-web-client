module.exports = React.create
    displayName: 'TournamentLegend'

    render: ({showHandicap, mod}) ->
        cls = "legend"
        if mod
            cls += " " + mod.split(" ").map((x) -> ("legend_" + x)).join(" ")

        `<div className={cls}>
            <div className="legend__item">
                <div className="legend__color handicap"></div>
                <div className="legend__info">Handicap -1 stroke
                    (subtracts 1 stroke from hole)
                </div>
            </div>
            <div className="legend__item">
                <div className="legend__color albatross"></div>
                <div className="legend__info">+8 points  Albatross
                    (3 strokes under par)
                </div>
             </div>
            <div className="legend__item">
                <div className="legend__color eagle"></div>
                <div className="legend__info">+5 points Eagle
                    (2 strokes under par)
                </div>
            </div>
            <div className="legend__item">
                <div className="legend__color birdie"></div>
                <div className="legend__info">+2 points  Birdie
                    (1 stroke under par)
                </div>
            </div>
            <div className="legend__item">
                <div className="legend__color par"></div>
                <div className="legend__info">0 points Par</div>
            </div>
            <div className="legend__item">
                <div className="legend__color bogey"></div>
                <div className="legend__info"> −1 points  Bogey
                    (1 stroke over par)
                </div>
            </div>
            <div className="legend__item">
                <div className="legend__color two-bogey"></div>
                <div className="legend__info"> −3  points Double bogey
                    or worse (2 strokes or more over par)
                </div>
            </div>
        </div>`
