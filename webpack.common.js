require('core-js')

const _ = require('underscore')
const path = require('path')
const webpack = require('webpack')


function entry(entries) {
    if (Array.isArray(entries)) {
        return _.object(entries.map(function (entry) {
            return [entry, 'main.' + entry + '.coffee']
        }))
    }

    return entries
}

const defaults = {
    debug: false,
    devtool: false,
    entries: [],
    globals: {},
    path: path.resolve('./_out'),
    plugins: [],
    watch: false,
}


/**
 *
 * @param options
 * @param options.entry
 * @param options.debug
 * @param options.watch
 * @param options.devtool
 * @param options.globals
 * @param options.path
 * @param options.filename
 * @param options.plugins
 *
 * @returns object
 */
module.exports = function (options) {
    options = Object.assign({}, defaults, options || {})

    var plugins = [
        new webpack.DefinePlugin(options.globals),
    ]

    return {
        context: path.resolve('./src'),

        output: {
            path: options.path,
            filename: options.filename,
        },

        entry: entry(options.entry),

        debug: options.debug,
        watch: options.debug && options.watch,
        devtool: options.devtool,

        plugins: plugins.concat(options.plugins),

        module: {
            preLoaders: [
                {test: /\.json$/, loader: 'json'},
            ],
            loaders: [
                {
                    test: /\.coffee$/,
                    loader: 'jsx!coffee',
                    include: path.resolve('./src'),
                    happy: {id: 'coffee'},
                },
                {
                    test: /\.css$/,
                    loader: 'style!css',
                    happy: {id: 'css'},
                },
            ],
            noParse: [
                /underscore\/underscore.js/,
                /vendor\/.+\.js/,
            ],
        },

        resolve: {
            root: [path.resolve('./src')],
            alias: {
                'minimal-flux': 'minimal-flux-2',
                'moment-timezone': 'moment-timezone/builds/moment-timezone-with-data-2012-2022.js',
            },
            extensions: ['', '.coffee', '.js', '.css'],
            unsafeCache: /node_modules/,
        },

        cache: {},

        colors: true,

        info: {
            assets: true,
        },
    }
}
