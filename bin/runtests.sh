#!/bin/sh

SERVER=" ./.venv/bin/python ./.venv/bin/howapi run --dev -r ../webclient/_out "
make dev

cd ../howapi
make frontend-tests args="-c /etc/howapi-frontendtests.yaml --nomad=.venv/bin/nomad"
nohup $SERVER -p 9999 -c /etc/howapi-frontendtests.yaml &
PID=$!

sleep 10
cd ../webclient
make test
STATUS=$?

kill -9 $PID

exit $STATUS

