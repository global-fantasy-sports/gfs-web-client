const webpack = require('webpack')
const configure = require('./webpack.common.js')


const GEOIP_BLOCK = process.env.GEOIP_BLOCK || 'ignore'
const NODE_ENV = process.env.NODE_ENV || 'development'
const WEBPACK_OPTIMIZE = Boolean(process.env.WEBPACK_OPTIMIZE)

function build(entry, target) {
    var plugins = []

    if (WEBPACK_OPTIMIZE) {
        plugins = plugins.concat([
            new webpack.optimize.DedupePlugin(),
            new webpack.optimize.UglifyJsPlugin({
                sourceMap: true,
                mangle: {
                    'keep_fnames': true,
                    'keep_fargs': true,
                },
                compress: {
                    'keep_fnames': true,
                    'keep_fargs': true,
                    'warnings': false,
                },
            }),
        ])
    }

    return configure({
        entry: [entry],
        filename: entry + '-' + target + '.js',

        devtool: '#source-map',

        globals: {
            'SPORT': JSON.stringify(entry === 'selector' ? '' : entry),
            'process.env': {
                NODE_ENV: JSON.stringify(NODE_ENV),
                GEOIP_BLOCK: JSON.stringify(GEOIP_BLOCK),
            },
        },

        plugins: plugins,
    })
}


module.exports = [
    build('selector', 'web'),
    build('nfl', 'web'),
    build('nba', 'web'),
    build('golf', 'web'),
]
